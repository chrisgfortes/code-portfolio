(function () {
	'use strict';

	angular
		.module('app')
		.factory('API', function($http, albumsService){
			var HOST = 'https://iws-recruiting-bands.herokuapp.com/api';

			return {
				get: {
					bands: _getBands,
					albums: _getAlbuns
				}
			}

			function _getBands(id){
				var hasID = (id ? id : '');
				return $http.get(HOST + '/bands/' + hasID);
			}

			function _getAlbuns(bandId){
				var promise = $http.get(HOST + '/albums');
				return albumsService.get.albums(bandId, promise);
			}
		})
})();