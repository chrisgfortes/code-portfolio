(function () {
	'use strict';

	angular
		.module('app')
		.controller('appController', _appController)
		.directive('onError', _srcError)

	function _appController(API, $timeout){
		var vm = this;
		vm.filters = {}
		vm.activeOrder = {}
		vm.albums = [];
		vm.bandsFiltered = [];

		_init();

		vm.setOrderBy = function(field, reverse){
			_removeFilterOrderActive(vm.activeOrder);
			vm.activeOrder[field] = true;
			vm.orderBy = (reverse ? ('-' + field) : field);
		}

		vm.bandDetail = function(band){
			vm.albums = [];
			vm.bandDetailToggle(true);
			vm.band = band;

			API.get
				.albums(band.id)
				.then(function(res) {
					vm.albums = res;

				});
		}

		vm.bandDetailToggle = function(hiddenContent){
			if(hiddenContent) {
				$timeout(function(){
					vm.contentHide = !vm.contentHide;
				}, 400);
			}else{
				vm.contentHide = !vm.contentHide;
			}

			vm.openedBandDetail = !vm.openedBandDetail;
		}

		vm.showMore = function(){
			vm.showAllBiography = !vm.showAllBiography;
		}

		function _removeFilterOrderActive(obj){
			for (var key in obj) {
				obj[key] = false;
			}
		}

		function _init(){
			API.get.bands()
				.then(function(res){
					vm.bands = res.data;
				})
				.catch(function(error){
					console.log(error);
				});
		}
	}

	function _srcError() {
		return {
			restrict:'A',
			link: function(scope, element, attr) {
				element.on('error', function() {
					element.attr('src', attr.onError);
				});
			}
		}
	};
})();