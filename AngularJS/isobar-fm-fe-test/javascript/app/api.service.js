(function () {
	'use strict';

	angular
		.module('app')
		.service('albumsService', function($http, $q){
			this.get = {
				albums: _getAlbums
			}

			function _getAlbums(bandId, getAlbuns){
				return getAlbuns
					.then(function(res){
						var albums = res.data.filter(function(item) {
							return (bandId == item.band);
						});

						return $q.resolve(albums);
					})
					.catch(function(error){
						console.log(error);
					});
			}
		})
})();