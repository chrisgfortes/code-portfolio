(function () {
    'use strict';

    angular
        .module('app')
        .directive('uiSelectMax', uiSelectMax);

    function uiSelectMax() {
        return {
            restrict: 'A',
            link: link
        };

        function link(scope, element, attr) {
            var $uiSelect = angular.element(element[0].querySelector('.ui-select-search'));
            $uiSelect.attr("maxlength", attr['uiSelectMax']);
        }
    }
}());