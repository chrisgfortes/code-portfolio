(function(app) {
    'use strict';

    app.directive('uppercase', _toUpperCase);

    function _toUpperCase() {
    	return {
    		require: 'ngModel',
    		link: function (scope, iElement, iAttrs, modelCtrl) {
    			modelCtrl.$parsers.push(function(input) {
                    return (input ? input.toUpperCase() : "");
	            });
                iElement.css("text-transform","uppercase");
    		}
    	};
    }
})(angular.module('app'));
