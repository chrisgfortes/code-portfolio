(function () {
    'use strict';

    angular
        .module('app')
        .directive('validaData', function (moment) {
            return {
                link: link,
                restrict: 'A',
                require: 'ngModel'
            }
        });

    function link(scope, element, attr, model) {
        element.bind('blur', function(e) {
            if(!moment(model.$modelValue).isValid()) {
                model.$modelValue = undefined
            }
        });
    }

})();