(function () {
    'use strict';

    angular
        .module('app')
        .factory('avaliacaoGeral', avaliacaoGeral)

    /** @ngInject */
    function avaliacaoGeral($http, HOST) {

        return {
            buscar: buscar
        }

        function buscar() {
            return $http.get(urlBase() + '/avaliacoes-gerenciais', { cache: true });
        }

        function urlBase() {
            return HOST.pessoa + '/analise-cadastral';
        }
    }
}());