(function () {
    'use strict';

    angular
        .module('app')
        .factory('focus', function ($timeout, $window) {
            return function (id) {
                $timeout(function () {
                    var document = $window.document;
                    var element = document.getElementById(id) || document.getElementsByName(id)[0];

                    if (element) {
                        element.focus();
                    }
                });
            };
        });
}());