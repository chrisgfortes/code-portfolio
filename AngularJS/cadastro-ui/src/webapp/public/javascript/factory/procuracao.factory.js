(function () {
    'use strict';

    angular
        .module('app')
        .factory('procuracao', procuracao)

    function procuracao($http, HOST) {
        var urlBase = (HOST.pessoa + '/fisica')

        return {
            tipos: tipos
        }

        function tipos() {
            return $http.get(urlBase + '/procurador/procuracao/tipos');
        }
    }
}());