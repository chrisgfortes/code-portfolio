(function () {
    'use strict';

    angular.module('app')
        .factory('cepFactory', cepFactory);

    function cepFactory($http, HOST) {
        return {
            cep: cep,
            estados: estados,
            cidades: cidades,
            paises: paises
        }

        function cep(cep) {
            return $http.get(HOST.localidade + '/localidades/' + cep, { cache: true });
        };

        function estados() {
            return $http.get(HOST.localidade + '/estados', { cache: true });
        };

        function cidades(uf) {
            return $http.get(HOST.localidade + '/estados/' + uf + '/cidades', { cache: true });
        };

        function paises(uf) {
            return $http.get(HOST.localidade + '/paises/', { cache: true });
        };
    }
})();
