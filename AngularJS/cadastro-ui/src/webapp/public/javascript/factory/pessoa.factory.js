(function() {
    'use strict';

    angular
        .module('app')
        .factory('pessoa', pessoa)

    function pessoa($http, HOST, $q) {

        return {
            estadoCivil: estadoCivil,
            regimeCasamento: regimeCasamento,
            tiposDePessoa: tiposDePessoa,
            buscar: buscar,
            status: status,
            buscarCadastrado: buscarCadastrado,
            dadosCadastrado: dadosCadastrado,
            buscarTipos: buscarTipos
        };

        function estadoCivil() {
            return $http.get(urlCadastro() + '/estados-civis');
        }

        function regimeCasamento() {
            return $http.get(urlCadastro() + '/regimes-casamento');
        }

        function tiposDePessoa() {
            return $http.get(urlSAU() + '/tipos');
        }

        function buscar(matricula) {
            return $http.get(urlTerceiro() + '/' + matricula + '/associados');
        }

        function buscarCadastrado(cpf, tipo) {
            return $http.get(HOST.dadosPessoais + '/' + cpf + '/' + tipo);
        }

        function dadosCadastrado(tipo, valor) {
            return $http.get(HOST.terceiro + 'terceiro?tipo=' + tipo + '&valor=' + valor);
        }

        function urlCadastro() {
            return HOST.pessoa + '/fisica';
        }

        function urlSAU() {
            return HOST.sau + '/pessoa/fisica';
        }

        function urlTerceiro() {
            return HOST.sau + '/pessoa/terceiro';
        }

        function status(cpf, tipo) {
            var url = HOST[tipo] + cpf + '/status';

            return $http
                .get(url)
                .then(transformaRetorno);

        }

        function buscarTipos(cpf) {
            var url = HOST.terceiro + 'terceiro/' + cpf + '/tipos-cadastro';

            return $http
                .get(url)
                .then(transformaRetorno);
        }

        function transformaRetorno(res) {
            return res.data;
        }


    }

}());
