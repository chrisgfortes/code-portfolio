(function () {
    'use strict';

    angular
        .module('app')
        .factory('associado', associadoFactory)

    function associadoFactory($http, HOST, $q) {

        return {
            buscar: buscar,
            situacoes: situacoes,
            buscarMatricula: buscarMatricula
        }

        function buscar(matricula) {
            var url = HOST.terceiro + 'terceiro?tipo=MATRICULA&valor=' + matricula;

            return $http.get(url)
                .then(function (res) {
                    return res.data;
                })
        }

        function situacoes() {
            var url = HOST.associado + 'situacoes-associacao'

            return $http.get(url)
                .then(function (res) {
                    return res.data;
                });
        }

        function buscarMatricula(cpf) {
            var url = HOST.associado + '?cpfCnpj=' + cpf;

            return $http.get(url)
                .then(function (res) {
                    return res.data;
                });
        }
    }

}());
