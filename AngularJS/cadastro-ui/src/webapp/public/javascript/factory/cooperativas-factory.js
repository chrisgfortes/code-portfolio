(function (app) {
    'use strict';

    angular.module('app')
        .factory('cooperativasFactory', cooperativasFactory);

    function cooperativasFactory($http, HOST) {
        return {
            all: all,
            postos: postos
        }

        function all() {
            var url = (HOST.auth + "/autenticacao/cooperativas");
            return $http.get(url);
        };

        function postos(codigoCooperativa) {
            return $http.get(HOST.cooperativa + '/' + codigoCooperativa + '/postos');
        }
    }

})();
