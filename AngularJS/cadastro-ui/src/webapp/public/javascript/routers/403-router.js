(function(app) {
    'use strict';

    app.config(function($routeProvider) {
        $routeProvider.when('/403', {
            templateUrl: 'views/pages/403.html',
            controller: '403Controller',
            controllerAs: '$ctrl',
            resolve: {}
        });
    });
})(angular.module('app'));
