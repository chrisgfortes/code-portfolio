(function (app) {
    'use strict';

    app.config(function ($routeProvider) {
        $routeProvider.when('/alteracao/:cpf', {
            templateUrl: 'views/pages/alteracao.html',
            controller: 'alteracaoController',
            controllerAs: '$ctrl',
            resolve: {
                isNotTerceiro: function ($route, rotaService) {
                    var cpf = $route.current.params.cpf;
                    return rotaService.isNotTerceiro(cpf);
                },
                section: function (secao) {
                    secao.nome = 'Alteracao de Cadastro';
                },
                temPermissao: function (controleDeAcesso) {
                    return controleDeAcesso.temAcesso('Cadastro Atualizar');
                }
            }
        });
    });
})(angular.module('app'));
