(function(app) {
    'use strict';

    app.config(function($routeProvider) {
        $routeProvider.when('/cadastro', {
            templateUrl: 'views/pages/cadastro.html',
            controller: 'cadastroController',
            controllerAs: '$ctrl',
            resolve: {
                section: function (secao) {
                    secao.nome = 'Cadastro de Pessoa';
                },
                temPermissao: function (controleDeAcesso) {
                    return controleDeAcesso.temAcesso('Cadastro Inserir');
                }
            }
        });
    });
})(angular.module('app'));
