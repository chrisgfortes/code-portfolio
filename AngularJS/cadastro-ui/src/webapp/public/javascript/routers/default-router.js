(function (app) {
    'use strict';

    app.config(function ($routeProvider) {
        $routeProvider.when('/', {
            resolve: {
                salvarInformacoes: salvarInformacoes
            }
        });

        $routeProvider.when('/login', {
            resolve: {
                salvarInformacoes: salvarInformacoes
            }
        });

        $routeProvider.otherwise({
            redirectTo: '/404'
        });
    });
    function salvarInformacoes(usuarioFactory, AuthFactory, $q, $location) {
        var promises = [
            usuarioFactory.token(), 
            usuarioFactory.tokenLegacy(), 
            usuarioFactory.cooperativa()
        ];

        $q.all(promises)
            .then(function (valores) {

                AuthFactory.salvarToken(valores[0].data.value);
                AuthFactory.salvarTokenLegacy(valores[1].data.value);
                AuthFactory.salvarCooperativa(valores[2].data.value);

                return valores[1].data.value;
            })
            .then(function () {
                usuarioFactory
                    .autorizacao()
                    .then(function (res) {
                        AuthFactory.salvarPermissoes(res.data.perfis);
                        $location.path('/home')
                    });
            })
            .catch(function () {
                // console.log('ERRO AO SALVAR TOKEN OU COOPERATIVA');
            });
    }
})(angular.module('app'));
