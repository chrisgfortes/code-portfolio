(function (app) {
    'use strict';

    app.config(function ($routeProvider) {
        $routeProvider.when('/404', {
            templateUrl: 'views/pages/404.html',
            controller: 'notFoundController',
            controllerAs: '$ctrl',
            resolve: {}
        });
    });
})(angular.module('app'));
