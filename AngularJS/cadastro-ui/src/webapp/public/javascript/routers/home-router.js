(function(app) {
    'use strict';

    app.config(function($routeProvider) {
        $routeProvider.when('/home', {
            templateUrl: 'views/pages/home.html',
            controller: 'homeController',
            controllerAs: '$ctrl',
            resolve: {
                section: function(secao){
                    secao.nome = 'Home Page';
                }
            }
        });
    });
})(angular.module('app'));
