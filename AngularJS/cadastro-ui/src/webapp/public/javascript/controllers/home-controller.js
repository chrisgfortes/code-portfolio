(function(app) {
    'use strict';

    app.controller('homeController', homeController);

    function homeController($rootScope) {
        $rootScope.sectionName = 'Cadastro Unicred';
    }

})(angular.module('app'));
