(function (app) {
    'use strict';

    app.controller('alteracaoController', alteracaoController);

    function alteracaoController(updateLinks, $routeParams) {
        var vm = this;
        vm.menus = updateLinks;
        vm.cpf = $routeParams.cpf;
    }
})(angular.module('app'));