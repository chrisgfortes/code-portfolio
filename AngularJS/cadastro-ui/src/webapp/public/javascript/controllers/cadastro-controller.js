(function (app) {
    'use strict';

    app.controller('cadastroController', cadastroController);

    function cadastroController($rootScope, urlService, controleDeAcesso) {
        var vm = this;

        vm.openModal = function (type, titulo) {
            var obj = urlService.getUrl(type);

            var modal = {
                enable: true,
                url: obj.url,
                titulo: titulo,
                isAlteracao: type == 'alteracoes',
                isContaCorrente: type == 'conta-corrente'
            }

            $rootScope.$emit('open-modal-cpf', modal);
        }

        vm.isAllowedToUpdate = function() {
            return controleDeAcesso.temPermissao('Cadastro Atualizar');
        }
    }
})(angular.module('app'));
