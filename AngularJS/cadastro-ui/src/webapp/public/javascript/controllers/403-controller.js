(function(app) {
    'use strict';

    app.controller('403Controller', unauthorizedController);

    function unauthorizedController($rootScope, AuthFactory) {
        $rootScope.isLogged = false;
    }


})(angular.module('app'));
