(function(app) {
    'use strict';

    angular
        .module('app')
        .controller('sidebarController', sidebarController);

    function sidebarController($rootScope, $location, AuthFactory, VERSION, pessoa, urlService, $window, controleDeAcesso) {
        var baseURI = '#/home';
        var vm = this;

        vm.app = {
            version: VERSION,
            user: AuthFactory.getUsuario().nome
        };

        $rootScope.$on('sidebar-active', function (e, isActive) {
            vm.isActive = isActive;
        });

        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams) {
            vm.isBaseUrl = ($window.location.hash == baseURI);
        });

        vm.menuAction = function (event, action) {
            if(!action){
                event.preventDefault();
            }
        };

        vm.isMenuActive = function(listRouter){
            return _currentMenuIsActive(listRouter);
        }

        function _currentMenuIsActive(listRouter){
            var currentLocation = $location.path();

            if (angular.isArray(listRouter)) {
                for (var i = 0; i < listRouter.length; i++) {
                    if(listRouter[i] === currentLocation){
                        return true;
                    }else{
                        var search = currentLocation.search(new RegExp(listRouter[i]));
                        return (search > -1 ? true : false);
                    }
                }
            }else{
                return (listRouter === currentLocation);
            }
        }

        vm.isAllowedToInsert = function() {
            return controleDeAcesso.temPermissao('Cadastro Inserir');
        }
    }
})();
