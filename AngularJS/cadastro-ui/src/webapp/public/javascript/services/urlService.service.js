(function () {
    'use strict';

    angular
        .module('app')
        .service('urlService', urlService);

    function urlService(redirectUrls) {
        var patternDelimiter = /^\:/g;

        this.parse = _parseObject;
        this.getUrl = _getUrlRedirect;

        function _parseObject(uri, mapper) {
            var params = Object.keys(mapper);
            var url = '';

            params.forEach(function (param) {
                var valor = ':' + param;
                uri = uri.replace(valor, mapper[param])
            });

            return uri;
        }

        function _getUrlRedirect(type) {
            var obj = redirectUrls.filter(function (item, index) {
                var tipoMap = item.type.toLowerCase();
                var tipoUrl = type.toLowerCase();

                return tipoMap == tipoUrl;
            });

            var objDefault = {
                type: undefined,
                url: undefined
            }

            return obj.length > 0 ? obj[0] : objDefault;
        }
    }

}());