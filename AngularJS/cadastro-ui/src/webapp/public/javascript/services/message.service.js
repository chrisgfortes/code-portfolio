(function () {
    'use strict';

    angular
        .module('app')
        .service('message', message)

    function message() {

        this.concat = _concatMessage;

        function _concatMessage() {
            var args = Array.prototype.slice.call(arguments);
            var string = '';

            if (args.length > 0) {
                args.forEach(function (text) {
                    string = string.concat(' ', text);
                });
            }

            return string;
        }
    }

}());