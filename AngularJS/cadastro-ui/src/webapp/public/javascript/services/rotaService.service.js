(function() {
    'use strict';

    angular
        .module('app')
        .service('rotaService', rotaService)

    function rotaService($rootScope, pessoa, $location, $q) {

        this.isTerceiro = isTerceiro;
        this.isNotTerceiro = isNotTerceiro;
        this.isAssociado = isAssociado;
        this.isNotAssociado = isNotAssociado;

        function isTerceiro(cpf) {
            return pessoa
                .status(cpf, 'terceiro')
                .then(function(terceiro) {
                    $rootScope.$emit('mostrar-mensagem', {
                        titulo: 'Terceiro já cadastrado',
                        descricao: 'O cadastro do CPF ' + terceiro.cpfCnpj + ' já foi inserido anteriormente.'
                    });

                    $location.path('cadastro');
                })
                .catch(function() {
                    return;
                });
        }

        function isNotTerceiro(cpf) {
            return pessoa
                .status(cpf, 'terceiro')
                .catch(function(erro) {
                    if (erro.status == 404) {
                        $rootScope.$emit('mostrar-mensagem', {
                            titulo: 'Terceiro não cadastrado',
                            descricao: 'O CPF pesquisado ainda não é terceiro.'
                        });

                        $location.path('cadastro');
                    }
                    return;
                });
        }

        function isAssociado(cpf, titulo) {
            return pessoa
                .status(cpf, 'associado')
                .then(function(associado) {
                    $rootScope.$emit('mostrar-mensagem', {
                        titulo: 'Associado já cadastrado',
                        descricao: 'O cadastro do CPF ' + associado.cpfCnpj + ' já foi confirmado como associado anteriormente.'
                    });

                    $location.path('cadastro');
                })
                .catch(function() {
                    return;
                });
        }

        function isNotAssociado(cpf, titulo) {
            return pessoa
                .status(cpf, 'associado')
                .catch(function(erro) {
                    if (erro.status == 404) {
                        $rootScope.$emit('mostrar-mensagem', {
                            titulo: 'Associado não cadastrado',
                            descricao: 'O CPF pesquisado ainda não é associado.'
                        });

                        $location.path('cadastro');
                    }
                    return;
                });
        }
    }

}());
