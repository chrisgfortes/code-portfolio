(function () {
    'use strict';

    angular
        .module('app')
        .service('cpfService', cpfService)

    function cpfService() {

        this.validarCPF = validarCPF;

        function validarCPF(cpf) {
            return CPF.validate(cpf);
        }
    }

}());
