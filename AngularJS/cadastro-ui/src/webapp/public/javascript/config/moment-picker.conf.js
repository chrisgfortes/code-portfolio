(function () {
    'use strict';

    angular
        .module('app')
        .config(MomentPickerConfig)

    function MomentPickerConfig(momentPickerProvider) {
        momentPickerProvider.options({
            /* Picker properties */
            locale: 'pt-br',
            format: 'DD/MM/YYYY',
            autoclose: true,
            today: true,
            keyboard: true
        });
    }
}());
