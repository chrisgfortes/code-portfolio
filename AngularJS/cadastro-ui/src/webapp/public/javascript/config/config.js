(function(app) {
    'use strict';

    app.config(config);

    function config($httpProvider, $locationProvider) {
        // Interceptors
        $httpProvider.interceptors.push('httpInterceptor');
        $httpProvider.interceptors.push('loadingInterceptor');
        $httpProvider.interceptors.push('errorInterceptor');
        $locationProvider.html5Mode(false);
        $locationProvider.hashPrefix('');
    }

})(angular.module('app'));
