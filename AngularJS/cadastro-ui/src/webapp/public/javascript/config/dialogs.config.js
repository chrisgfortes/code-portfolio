(function () {
    'use strict';

    angular
        .module('app')
        .config(DialogsConfig)

    /** @ngInject */
    function DialogsConfig(dialogsProvider, $translateProvider) {
        dialogsProvider.setSize('sm');
        dialogsProvider.useFontAwesome();

        $translateProvider.translations('pt-BR', {
            DIALOGS_OK: "",
            DIALOGS_YES: "",
            DIALOGS_NO: ""
        });

        $translateProvider.preferredLanguage('pt-BR');
        $translateProvider.useSanitizeValueStrategy(null);
    }
}());
