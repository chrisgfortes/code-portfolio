(function(app) {
    'use strict';

    app.run(appRun);

    function appRun($rootScope, AuthFactory, $location, $routeParams, secao, cpfService, $route, HttpCodes) {
        $rootScope.message = null;

        $rootScope.$on('$routeChangeSuccess', function (event, next, prev) {
            $rootScope.$emit('cpf-alterado', $routeParams.cpf);
            $rootScope.$emit('section-name', secao);
            $rootScope.isLogged = AuthFactory.isAuthenticated();

        });

        $rootScope.$on('$routeChangeError', function (event, current, previous, rejection) {
            if (rejection == HttpCodes.FORBIDDEN) {
                $location.path('403');
            }
        });
    }

})(angular.module('app'));
