(function() {
    'use strict';

    angular
        .module('app')
        .factory('controleDeAcesso', controleDeAcesso);

    function controleDeAcesso($q, AuthFactory, HttpCodes) {

        return {
            temAcesso: temAcesso,
            temPermissao: temPermissao
        };

        function temAcesso(perfis) {
            var deferred = $q.defer();

            temPermissao(perfis)
                ? deferred.resolve(HttpCodes.OK)
                : deferred.reject(HttpCodes.FORBIDDEN);

            return deferred.promise;
        }

        function temPermissao(permissoes) {
            if (!angular.isArray(permissoes)) {
                return AuthFactory.buscarPermissoes().some(tem(permissoes));
            }

            return permissoes.some(function(permissao) {
                return AuthFactory.buscarPermissoes().some(tem(permissao));
            });
        }

        function tem(perfil) {
            return function(permissao) {
                return permissao.nmPerfil == perfil;
            };
        }
    }
}());
