(function(app) {
    'use strict';

    app.constant('HOST', {
        "cooperativa": "../../../cooperativa-us/cooperativa/v1/",
        "userCooperativa": "/user/cooperativa",
        "localidade": "../../../localidade-us/cadastro/localidade/v1",
        "conta": "../../../conta-us/cadastro/conta/v1/",
        "sau": "../../../sau-us/cadastro/cooperado/v1",
        "pessoa": "../../../pessoa-us/cadastro/pessoa/v1",
        "cadastro": "../../../cadastro-web-bff/cadastro/cadastro-bff/v1",
        "terceiro": "../../../cadastro-web-bff/cadastro/cadastro-bff/v1/terceiros/",
        "associado": "../../../cadastro-web-bff/cadastro/cadastro-bff/v1/associados/",
        "contaCorrente": "../../../cadastro-web-bff/cadastro/cadastro-bff/v1/contas-correntes/",
        "agendamentoCapital": "../../../cadastro-web-bff/cadastro/cadastro-bff/v1/agendamentos-capitais",
        "dadosPessoais": "../../../cadastro-web-bff/cadastro/cadastro-bff/v1/dados-pessoais",
        "linhaCredito": "../../../linha-credito-us/credito/linhasCredito/v1/"
    });

})(angular.module('app'));
