(function () {
    'use strict';

    angular
        .module('app')
        .value('security', [
            {
                chave: 'Cadastro Aberto',
                telas: [
                    '/login',
                    '/home',
                    '/403',
                    '/404'
                ]
            }
        ]);
}());