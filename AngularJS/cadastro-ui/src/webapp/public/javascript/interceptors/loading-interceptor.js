(function(app) {
    'use strict';

    app.factory('loadingInterceptor', loadingInterceptor);

    function loadingInterceptor($q, $rootScope) {
	    $rootScope.loading = false;

        return {
            'requestError': function(rejection) {
                $rootScope.loading = false;
                return $q.reject(rejection);
            },
            'responseError': function(rejection) {
                $rootScope.loading = false;
                return $q.reject(rejection);
            }
        }
    }

})(angular.module('app'));
