(function () {
    'use strict';

    angular
        .module('app')
        .factory('errorInterceptor', errorInterceptor);

    function errorInterceptor($q, $rootScope) {
        $rootScope.displayToast = false;

        return {
            responseError: responseError
        };

        function responseError(rejection) {
            $rootScope.displayToast = (rejection.status == 500);
            return $q.reject(rejection);
        }
    }

})();
