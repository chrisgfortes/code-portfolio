(function () {
    'use strict';

    angular
        .module('app')
        .factory('httpInterceptor', httpInterceptor);

    function httpInterceptor(AuthFactory) {
        return {
            request: request
        };

        function request($config) {
            var tokenLegacy = AuthFactory.buscarTokenLegacy();
            var authorization = AuthFactory.buscarToken();
            var cooperativa = AuthFactory.buscarActionCooperativa() || AuthFactory.buscarCooperativa();

            if (tokenLegacy && cooperativa && authorization) {
                $config.headers.token = tokenLegacy;
                $config.headers.Authorization = 'Bearer ' + authorization;
                $config.headers.cooperativa = cooperativa;
                $config.headers['X-Forwarded-Host'] = 'cadastro.e-unicred.com.br';

                var userPermissions = AuthFactory.buscarPermissoes();

                if (userPermissions) {
                    var permissoes = userPermissions.map(function(item) {
                        return item.nmPerfil;
                    });

                    $config.headers['X-Unicred-Permissoes'] = permissoes;
                }
            }

            return $config;
        }
    }

})();
