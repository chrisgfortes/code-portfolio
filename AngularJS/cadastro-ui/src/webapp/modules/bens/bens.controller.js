(function (app) {
    'use strict';

    app.controller('bensController', bensController);

    function bensController($routeParams, urls, bensInfo, bensFactory) {
        var vm = this;

        vm.cpf = $routeParams.cpf;
        vm.links = urls;
        vm.bens = bensInfo;

        vm.salvar = salvar;

        function salvar(obj) {
            var data = angular.copy(obj);

            return bensFactory
                .salvar(vm.cpf, data)
                .then(function (res) {
                    vm.sucessos = [{ message: 'Os dados de Bens foram salvos com sucesso.' }];
                })
                .catch(function (erro) {
                    if (erro.status == 422) {
                        vm.erros = erro.data.body || erro.data.content;
                    }
                });
        }

    }
})(angular.module('app'));
