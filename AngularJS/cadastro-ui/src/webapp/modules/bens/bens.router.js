(function(app) {
    'use strict';

    app.config(function($routeProvider) {
        $routeProvider.when('/cadastro/terceiro/:cpf/bens', {
            templateUrl: './modules/bens/bens.html',
            controller: 'bensController',
            controllerAs: '$ctrl',
            resolve: {
                isTerceiro: function ($route, rotaService) {
                    var cpf = $route.current.params.cpf;
                    return rotaService.isTerceiro(cpf);
                },
                bensInfo: function(bens, $route) {
                    var factory = bens('terceiro');
                    return factory
                        .buscar($route.current.params.cpf)
                        .catch(function () {
                            return {};
                        });
                },
                urls: function (linksFactory) {
                    return linksFactory('terceiro');
                },
                bensFactory: function (bens){
                    return bens('terceiro');
                },
                temPermissao: function (controleDeAcesso) {
                    return controleDeAcesso.temAcesso('Cadastro Inserir');
                }
            }
        });
    });
})(angular.module('app'));
