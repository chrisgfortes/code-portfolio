(function() {
    'use strict';

    angular
        .module('app')
        .factory('bens', bens)

    function bens($http, HOST, moment, veiculoConvert, imovelConvert) {

        return function (urlBase) {
            return {
                salvar: salvar,
                buscar: buscar
            }

            function salvar(cpf, bens) {
                var url = urlBuild(cpf);

                bens.veiculos = veiculoConvert.convertPost(bens.veiculos);
                bens.imoveis = imovelConvert.convertPost(bens.imoveis);

                return $http.put(url, bens);
            }

            function buscar(cpf) {
                var url = urlBuild(cpf);

                return $http
                    .get(url)
                    .then(function(res) {
                        res.data.veiculos = veiculoConvert.convertGet(res.data.veiculos || []);
                        res.data.imoveis = imovelConvert.convertGet(res.data.imoveis || []);

                        return res.data;
                    });
            }

            function urlBuild(cpf) {
                return HOST[urlBase] + cpf + '/rascunho/bens';
            }
        }
    }

}());
