(function () {
    'use strict';

    limiteChequeEspecialController.$inject = ['chequeEspecial']

    angular
        .module('app')
        .component('dadosLimiteChequeEspecial', {
            bindings: {
                limiteChequeEspecial: '<'
            },
            controller: limiteChequeEspecialController,
            templateUrl: './modules/cheque-especial/limite-cheque-especial/dados-limite-cheque-especial.html'
        });

    function limiteChequeEspecialController(chequeEspecial) {
        var vm = this;

        vm.$onInit = onInit;

        function onInit() {
            vm.linhasDeCredito = [];

            chequeEspecial
                .linhasDeCredito()
                .then(carregarLinhasDeCredito)

            chequeEspecial
                .tiposVencimento()
                .then(carregarTiposVencimento)
        }

        function carregarLinhasDeCredito(linhasDeCredito) {
            vm.linhasDeCredito = linhasDeCredito
        }

        function carregarTiposVencimento(tiposVencimento) {
           vm.tiposVencimento = tiposVencimento
        }
    }
}());