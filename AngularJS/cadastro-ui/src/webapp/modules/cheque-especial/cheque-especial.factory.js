(function () {
    'use strict';

    angular
        .module('app')
        .factory('chequeEspecial', chequeEspecial)

    function chequeEspecial($http, HOST, $q) {

        return {
            linhasDeCredito: linhasDeCredito,
            tiposVencimento: tiposVencimento,
            salvar: salvar
        }

        function linhasDeCredito() {
            return $http
                .get(HOST.linhaCredito + '?modalidade=CHEQUE_ESPECIAL&ativo=true&tipo-credito=EMPRESTIMO')
                .then(function (res) {
                    return res.data
                })
        }

        function tiposVencimento() {
            return $http
                .get(HOST.contaCorrente + 'tipos-vencimento-proposta')
                .then(function (res) {
                    return res.data
                })
        }

        function salvar(limiteChequeEspecial) {
            return $http.post(HOST.contaCorrente + 'limite-cheque-especial', limiteChequeEspecial);
        }
    }

}());