(function () {
    'use strict';

    angular
        .module('app')
        .controller('chequeEspecialController', chequeEspecialController)

    function chequeEspecialController(chequeEspecial) {
        var vm = this;
        vm.limiteChequeEspecial = {};
        vm.salvar = salvar;

        function salvar(limiteChequeEspecial) {
            return chequeEspecial
                .salvar(limiteChequeEspecial)
                .then(notificarSucesso)
                .catch(notificarErro);
        }

        function notificarSucesso() {
            vm.sucessos = [{ message: 'Os dados Limite de Credito foram salvos com sucesso.' }];
        }

        function notificarErro(erro) {
            if (erro.status == 422) {
                vm.erros = erro.details || [erro.data];

                return;
            }

            vm.erros = [{ message: 'Ocorreu um erro no servidor, tente novamente.' }];
        }

    }

}());