(function (app) {
    'use strict';

    app.config(function ($routeProvider) {
        $routeProvider.when('/cadastro/associado/:cpf/limite-cheque-especial', {
            templateUrl: './modules/cheque-especial/cheque-especial.html',
            controller: 'chequeEspecialController',
            controllerAs: 'ctrl',
            params: {
                isContaCorrente: true
            },
            resolve: {
                temPermissao: function (controleDeAcesso) {
                    return controleDeAcesso.temAcesso('Cadastro Inserir');
                }
            }
        });
    });
})(angular.module('app'));