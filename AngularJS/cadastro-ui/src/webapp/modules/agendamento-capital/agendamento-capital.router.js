(function (app) {
    'use strict';

    app.config(function ($routeProvider) {
        $routeProvider.when('/cadastro/correntista/:cpf/agendamento-capital', {
            templateUrl: './modules/agendamento-capital/agendamento-capital.html',
            controller: 'agendamentoCapitalController',
            controllerAs: 'ctrl',
            resolve: {
                isNotAssociado: function($route, rotaService) {
                    var cpf = $route.current.params.cpf;
                    return rotaService.isNotAssociado(cpf);
                },
                temPermissao: function (controleDeAcesso) {
                    return controleDeAcesso.temAcesso('Cadastro Inserir');
                }
            }
        });
    });
})(angular.module('app'));
