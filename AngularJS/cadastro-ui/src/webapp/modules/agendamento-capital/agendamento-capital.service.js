(function () {
    'use strict';

    angular
        .module('app')
        .service('agendamentoCapitalConverter', agendamentoCapitalConverter)

    function agendamentoCapitalConverter(moment) {
        this.get = get;
        this.post = post;

        function get(agendamento) {
            var dataIntegralizacoes = angular.copy(agendamento);

            if(dataIntegralizacoes.hasOwnProperty('anoMesPrimeiroVencimento')){
                var date = angular.copy(dataIntegralizacoes.anoMesPrimeiroVencimento);
                dataIntegralizacoes.anoMesPrimeiroVencimento = moment(date).format('YYYY-MM');
            }

            if (dataIntegralizacoes.hasOwnProperty('diaPrimeiroVencimento')) {
                var date = angular.copy(dataIntegralizacoes.diaPrimeiroVencimento);
                dataIntegralizacoes.diaPrimeiroVencimento = moment(date).format('DD');
            }

            if (dataIntegralizacoes.hasOwnProperty('dataEntrada')) {
                dataIntegralizacoes.dataEntrada = moment(date).format('YYYY-MM-DD');
            }

            if (dataIntegralizacoes.hasOwnProperty('contaDebito')) {
                dataIntegralizacoes.contaDebito = dataIntegralizacoes.contaDebito;
            }

            if (dataIntegralizacoes.hasOwnProperty('renovacaoAutomatica')) {
                var bool = dataIntegralizacoes.renovacaoAutomatica;
                dataIntegralizacoes.renovacaoAutomatica = !!bool;
            }

            return dataIntegralizacoes;
        }

        function post(agendamento) {
            var dataIntegralizacoes = angular.copy(agendamento);

            if(!!dataIntegralizacoes.anoMesPrimeiroVencimento){
                var date = angular.copy(dataIntegralizacoes.anoMesPrimeiroVencimento);
                dataIntegralizacoes.anoMesPrimeiroVencimento = moment(date).format('YYYY-MM');
            }

            if (!!dataIntegralizacoes.diaPrimeiroVencimento) {
                var date = angular.copy(dataIntegralizacoes.diaPrimeiroVencimento);
                dataIntegralizacoes.diaPrimeiroVencimento = moment(date).format('DD');
            }

            return dataIntegralizacoes;
        }
    }

}());
