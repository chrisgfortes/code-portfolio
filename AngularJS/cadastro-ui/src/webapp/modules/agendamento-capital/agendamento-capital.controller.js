(function (app) {
    'use strict';

    app.controller('agendamentoCapitalController', agendamentoCapitalController);

    function agendamentoCapitalController($rootScope, $routeParams, agendamentoCapital, $timeout, pessoa) {
        var vm = this;

        vm.cpf = $routeParams.cpf;
        vm.salvar = salvar;

        function salvar(_agendamentoCapital) {
            var agendamento = angular.copy(_agendamentoCapital);
            salvandoNoSau();

            agendamentoCapital
                .salvar(agendamento.agendamentoIntegralizacoes)
                .then(notificarSucesso)
                .catch(notificarErro)
                .finally(salvandoNoSau)
        }

        function notificarSucesso(res) {
            vm.salvoComSucesso = true;
            vm.erros = [];
            vm.sucessos = [{ message: 'Os dados de agendamento foram salvos com sucesso.' }];

            pessoa.status(vm.cpf, 'terceiro')
                .then(function(response) {
                    var dadosPessoa = response;
                    var modal = {
                        enable: true,
                        cpf: dadosPessoa.cpfCnpj,
                        nome: dadosPessoa.nome,
                        titulo: 'Agendamento Capital',
                        tipo: 'correntista'
                    };
                    $rootScope.$emit('usuario-pendente', modal);
                });

        }

        function salvandoNoSau() {
            vm.savingProgress = !vm.savingProgress;
        }

        function notificarErro(erro) {
            vm.sucessos = [];
            if (erro.status == 422) {
                vm.erros = erro.data.body || erro.data.content || [{ message: erro.data.message }];

                return;
            }

            vm.erros = [{
                message: 'Ocorreu um erro no servidor, tente novamente.'
            }];
        }
    }
})(angular.module('app'));
