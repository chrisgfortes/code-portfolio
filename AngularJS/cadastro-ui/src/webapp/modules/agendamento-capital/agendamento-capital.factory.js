(function () {
    'use strict';

    angular
        .module('app')
        .factory('agendamentoCapital', agendamentoCapital)

    function agendamentoCapital($http, HOST, $q, agendamentoCapitalConverter) {

        return {
            salvar: salvar
        };

        function salvar(_agendamentoCapital) {
            var data = agendamentoCapitalConverter.post(_agendamentoCapital);
            return $http.post(HOST.agendamentoCapital + "/", data);
        }
    }

}());
