(function () {
    'use strict';

    angular
        .module('app')
        .controller('dadosComplementaresController', dadosComplementaresController)

    function dadosComplementaresController($routeParams, urls, dadosComplementaresInfo, dadosComplementaresFactory) {
        var vm = this;

        vm.cpf = $routeParams.cpf;
        vm.links = urls;
        vm.dadosComplementares = dadosComplementaresInfo;
        vm.salvar = salvar;

        function salvar(form) {
            var dados = angular.copy(form);

            return dadosComplementaresFactory
                .salvar(vm.cpf, dados)
                .then(function (res) {
                    vm.sucessos = [{ message: 'Os dados de complementares foram salvos com sucesso.' }];
                }).catch(function (erro) {
                    if (erro.status == 422) {
                        vm.erros = erro.data.body || erro.data.content;
                    }
                });

        }
    }
}());
