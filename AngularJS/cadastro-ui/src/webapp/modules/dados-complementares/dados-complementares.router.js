(function (app) {
    'use strict';

    app.config(function ($routeProvider) {
        $routeProvider.when('/cadastro/terceiro/:cpf/dados-complementares', {
            templateUrl: './modules/dados-complementares/dados-complementares.html',
            controller: 'dadosComplementaresController',
            controllerAs: '$ctrl',
            resolve: {
                isTerceiro: function ($route, rotaService) {
                    var cpf = $route.current.params.cpf;
                    return rotaService.isTerceiro(cpf);
                },
                dadosComplementaresInfo: function (dadosComplementares, $route) {
                    var factory = dadosComplementares('terceiro');
                    return factory
                        .buscar($route.current.params.cpf)
                        .catch(function () {
                            return {};
                        })
                },
                urls: function (linksFactory) {
                    return linksFactory('terceiro');
                },
                dadosComplementaresFactory: function (dadosComplementares){
                    return dadosComplementares('terceiro');
                },
                temPermissao: function (controleDeAcesso) {
                    return controleDeAcesso.temAcesso('Cadastro Inserir');
                }
            }
        });
    });
})(angular.module('app'));
