(function() {
    'use strict';

    angular
        .module('app')
        .factory('dadosComplementares', dadosComplementares)

    function dadosComplementares($http, HOST, moment, procuradorConvert, planoDeSaudeConvert, referenciaConvert, seguroConvert, previdenciaConvert) {

        return function (urlBase) {
            return {
                salvar: salvar,
                buscar: buscar
            }

            function salvar(cpf, dadosComplementares) {
                var url = urlBuild(cpf);

                dadosComplementares.procuradores = procuradorConvert.convertPost(dadosComplementares.procuradores);
                dadosComplementares.planosSaude = planoDeSaudeConvert.convertPost(dadosComplementares.planosSaude);
                dadosComplementares.referencias = referenciaConvert.convertPost(dadosComplementares.referencias);
                dadosComplementares.seguros = seguroConvert.convertPost(dadosComplementares.seguros);
                dadosComplementares.previdencias = previdenciaConvert.convertPost(dadosComplementares.previdencias);

                return $http.put(url, dadosComplementares);
            }

            function buscar(cpf) {
                var url = urlBuild(cpf);

                return $http
                    .get(url)
                    .then(function(res) {
                        res.data.procuradores = procuradorConvert.convertGet(res.data.procuradores || []);
                        res.data.referencias = referenciaConvert.convertGet(res.data.referencias || []);
                        res.data.planosSaude = planoDeSaudeConvert.convertGet(res.data.planosSaude || []);
                        res.data.seguros = seguroConvert.convertGet(res.data.seguros || []);
                        res.data.previdencias = previdenciaConvert.convertGet(res.data.previdencias || []);
                        res.data.participacoesSocietarias = res.data.participacoesSocietarias || [];

                        return res.data;
                    });
            }

            function urlBuild(cpf) {
                return HOST[urlBase] + cpf + '/rascunho/dadoscomplementares';
            }
        }
    }

}());
