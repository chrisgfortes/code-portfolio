(function() {
    'use strict';

    angular
        .module('app')
        .factory('analiseCadastral', analiseCadastral)

    function analiseCadastral($http, HOST, moment, pessoaResponsavelConvert, informacoesComplementaresConvert) {
        return function (urlBase) {
            return {
                salvar: salvar,
                buscar: buscar
            }

            function salvar(cpf, analiseCadastral) {
                var url = urlBuild(cpf);
                analiseCadastral.cpf = cpf;
                analiseCadastral.pessoaResponsavelFATCA = pessoaResponsavelConvert.convertPost(analiseCadastral.pessoaResponsavelFATCA);
                analiseCadastral.informacoesComplementares = informacoesComplementaresConvert.convertPost(analiseCadastral.informacoesComplementares);

                return $http.put(url, analiseCadastral);
            }

            function buscar(cpf) {
                var url = urlBuild(cpf);

                return $http
                    .get(url)
                    .then(function(res) {
                        return res.data;
                    });
            }

            function urlBuild(cpf) {
                return HOST[urlBase] + cpf + '/rascunho/analisecadastral';
            }
        }
    }

}());
