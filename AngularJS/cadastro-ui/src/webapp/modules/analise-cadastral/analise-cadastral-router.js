(function (app) {
    'use strict';

    app.config(function ($routeProvider) {
        $routeProvider.when('/cadastro/terceiro/:cpf/analise-cadastral', {
            templateUrl: './modules/analise-cadastral/analise-cadastral.html',
            controller: 'analiseCadastralController',
            controllerAs: '$ctrl',
            resolve: {
                analiseCadastralInfo: function (analiseCadastral, $route) {
                    var factory = analiseCadastral('terceiro');
                    return factory
                        .buscar($route.current.params.cpf)
                        .catch(function () {
                            return {};
                        });
                },
                urls: function (linksFactory) {
                    return linksFactory('terceiro');
                },
                analiseCadastralFactory: function (analiseCadastral) {
                    return analiseCadastral('terceiro');
                },
                temPermissao: function (controleDeAcesso) {
                    return controleDeAcesso.temAcesso('Cadastro Inserir');
                }
            }
        });
    });
})(angular.module('app'));
