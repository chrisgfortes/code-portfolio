(function (app) {
    'use strict';

    app.controller('analiseCadastralController', analiseCadastralController);

    function analiseCadastralController($routeParams, urls, analiseCadastralInfo, analiseCadastralFactory, analiseCadastralConverter) {
        var vm = this;

        vm.cpf = $routeParams.cpf;
        vm.links = urls;
        vm.analiseCadastral = analiseCadastralInfo;
        vm.salvar = salvar;

        function salvar(form) {
           var dados = analiseCadastralConverter.get(form);

           return analiseCadastralFactory
               .salvar(vm.cpf, dados)
               .then(function (res) {
                   vm.sucessos = [{ message: 'Os dados da Análise Cadastral foram salvos com sucesso.' }];
               }).catch(function (erro) {
                   if (erro.status == 422) {
                       vm.erros = erro.data.body || erro.data.content;
                   }
               });
       }


    }
})(angular.module('app'));
