(function(){
    'use strict';

    angular
        .module('app')
        .service('analiseCadastralConverter', analiseCadastralConverter)

    function analiseCadastralConverter(){
        this.get = get;

        function get(analise){
            if (analise.restricoesAcatadas && analise.restricoesAcatadas.length < 1) {
                analise.restricoesAcatadas = null;
            }

            if (analise.matriculaVinculada && Object.keys(analise.matriculaVinculada).length < 1) {
                analise.matriculaVinculada = null;
            }

            return analise;
        }
    }

}());
