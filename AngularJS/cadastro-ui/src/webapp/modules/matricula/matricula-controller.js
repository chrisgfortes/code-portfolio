(function(app) {
    'use strict';

    app.controller('matriculaController', matriculaController);

    function matriculaController($rootScope, $routeParams, matriculaFactory, AuthFactory, pessoa) {
        var vm = this;

        vm.cpf = $routeParams.cpf;
        vm.salvar = salvar;

        vm.associado = {
            matricula: {
                dadosCadastrais: {}
            }
        };
        vm.associado.matricula.cooperativa = AuthFactory.buscarActionCooperativa() || AuthFactory.buscarCooperativa();

        function salvar(form) {
            var dados = angular.copy(form);
            salvandoNoSau();

            matriculaFactory
                .salvar(vm.cpf, dados)
                .then(notificarSucesso)
                .catch(notificarErro)
                .finally(salvandoNoSau);
        }

        function salvandoNoSau() {
            vm.savingProgress = !vm.savingProgress;
        }

        function notificarSucesso(res) {
            vm.salvoComSucesso = true;
            vm.erros = [];
            vm.sucessos = [{
                message: 'Associado cadastrado com sucesso.'
            }];

            pessoa.status(vm.cpf, 'terceiro')
                .then(function(response) {
                    var dadosPessoa = response;
                    var modal = {
                        enable: true,
                        cpf: dadosPessoa.cpfCnpj,
                        nome: dadosPessoa.nome,
                        titulo: 'Associado',
                        tipo: 'associado'
                    };

                    $rootScope.$emit('usuario-pendente', modal);
                });

        }

        function notificarErro(erro) {
            vm.salvoComSucesso = false;
            vm.sucessos = [];
            if (erro.status == 422) {
                vm.erros = erro.data.body || erro.data.content || [{
                    message: erro.data.message
                }];
            }
        }

    }
})(angular.module('app'));
