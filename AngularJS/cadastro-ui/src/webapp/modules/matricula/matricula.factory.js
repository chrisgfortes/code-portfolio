(function() {
    'use strict';

    angular
        .module('app')
        .factory('matricula', matricula)

    function matricula($http, HOST) {
        return function(urlBase) {

            return {
                salvar: salvar
            }

            function salvar(cpf, matricula) {
                var _matricula = angular.copy(matricula)
                var url = urlBuild(cpf);

                return $http.post(url, _matricula);
            }

            function urlBuild(cpf) {
                return HOST[urlBase] + cpf;
            }
        }
    }

}());
