(function () {
    'use strict';

    dadosCadastraisMatriculaController.$inject = ['associado', 'moment']

    angular
        .module('app')
        .component('dadosCadastraisMatricula', {
            bindings: {
                dadosCadastrais: '<'
            },
            controller: dadosCadastraisMatriculaController,
            templateUrl: './modules/matricula/dados-cadastrais-matricula/dados-cadastrais-matricula.html'
        });

    function dadosCadastraisMatriculaController(associado, moment) {
        var vm = this;

        vm.$onInit = onInit;
        vm.validaData = validaData;
        vm.buscarMatricula = buscarMatricula;

        function onInit() {
            vm.dadosCadastrais.dataAssociacao = vm.dadosCadastrais.dataAssociacao || moment()
            vm.dadosCadastrais.situacaoAssociacao = vm.dadosCadastrais.situacaoAssociacao || 'ATIVO'


            associado
                .situacoes()
                .then(carregarSituacoes)
        }

        function carregarSituacoes(situacoes) {
            vm.situacoes = situacoes;
        }

        function validaData(dataAssociacao) {
            if (!dataAssociacao.isValid()) {
                vm.dadosCadastrais.dataAssociacao = undefined;
            }
        }

        function buscarMatricula(matricula) {
            if(!matricula) return;

            associado
                .buscar(matricula)
                .then(function (_associado) {
                    vm.dadosCadastrais.nomeProponente = _associado.nome;
                })
                .catch(function (erro) {
                    vm.dadosCadastrais.nomeProponente = '';
                })
        }
    }
}());