(function(app) {
    'use strict';

    app.config(function($routeProvider) {
        $routeProvider.when('/cadastro/associado/:cpf/matricula', {
            templateUrl: './modules/matricula/matricula.html',
            controller: 'matriculaController',
            controllerAs: '$ctrl',
            resolve: {
                isNotTerceiro: function ($route, rotaService) {
                    var cpf = $route.current.params.cpf;
                    return rotaService.isNotTerceiro(cpf);
                },
                isAssociado: function ($route, rotaService) {
                    var cpf = $route.current.params.cpf;
                    return rotaService.isAssociado(cpf);
                },
                matriculaFactory: function (matricula){
                    return matricula('associado');
                },
                temPermissao: function (controleDeAcesso) {
                    return controleDeAcesso.temAcesso('Cadastro Inserir');
                }
            }
        });
    });
})(angular.module('app'));
