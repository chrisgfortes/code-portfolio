(function(app) {
    'use strict';

    app.config(function($routeProvider) {
        $routeProvider.when('/cadastro/terceiro/:cpf/cartao-autografo', {
            templateUrl: './modules/cartoes-autografo/cartoes-autografo.html',
            controller: 'cartoesAutografoController',
            controllerAs: '$ctrl',
            resolve: {
                isTerceiro: function ($route, rotaService) {
                    var cpf = $route.current.params.cpf;
                    return rotaService.isTerceiro(cpf);
                },
                cartoesAutografoInfo: function (cartaoAutografoFactory, $route, HOST) {
                    var factory = cartaoAutografoFactory(HOST.terceiro);
                    return factory
                       .buscar($route.current.params.cpf)
                       .catch(function () {
                           return { data: [] };
                       })
                },
                hostFactory: function (HOST) {
                    return HOST.terceiro;
                },
                urls: function (linksFactory) {
                    return linksFactory('terceiro');
                },
                temPermissao: function (controleDeAcesso) {
                    return controleDeAcesso.temAcesso('Cadastro Inserir');
                }
            }
        });
    });
})(angular.module('app'));
