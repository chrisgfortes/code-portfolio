(function (app) {
    'use strict';

    app.controller('cartoesAutografoController', cartoesAutografoController);

    function cartoesAutografoController($routeParams, urls, cartoesAutografoInfo, hostFactory) {
        var vm = this;

        vm.cpf = $routeParams.cpf;
        vm.links = urls;
        vm.cartoesAutografo = cartoesAutografoInfo.data;
        vm.host = hostFactory;
        vm.feedbackSucesso = feedbackSucesso;

        function feedbackSucesso(status, mensagem) {
            vm.salvoComSucesso = status;
        }
    }
})(angular.module('app'));
