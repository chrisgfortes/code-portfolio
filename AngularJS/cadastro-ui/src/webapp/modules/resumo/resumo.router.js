(function (app) {
    'use strict';

    app.config(function ($routeProvider) {
        $routeProvider.when('/cadastro/terceiro/:cpf/resumo', {
            templateUrl: './modules/resumo/resumo.html',
            controller: 'resumoController',
            controllerAs: 'ctrl',
            resolve: {
                isTerceiro: function ($route, rotaService) {
                    var cpf = $route.current.params.cpf;
                    return rotaService.isTerceiro(cpf);
                },
                resumoInfo: function (resumo, $route) {
                    var factory = resumo('terceiro');
                    return factory
                        .resumoGeral($route.current.params.cpf)
                        .catch(function (erro) {
                            return {};
                        });
                },
                resumoFactory: function (resumo){
                    return resumo('terceiro');
                },
                urls: function (linksFactory) {
                    return linksFactory('terceiro');
                },
                tituloPagina: function () {
                    return 'Pessoa Física';
                },
                temPermissao: function (controleDeAcesso) {
                    return controleDeAcesso.temAcesso('Cadastro Inserir');
                }
            }
        });
    });
})(angular.module('app'));
