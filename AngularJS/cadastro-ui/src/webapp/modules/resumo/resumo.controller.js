(function (app) {
    'use strict';

    app.controller('resumoController', dadosResumoController);

    function dadosResumoController($rootScope, $routeParams, resumoFactory, resumoInfo, urls, secoesResumo, tituloPagina) {
        var vm = this;
        vm.cpf = $routeParams.cpf;
        vm.links = urls;
        vm.resumo = resumoInfo;
        vm.secoes = desativarSecoes(secoesResumo);
        vm.representanteLegalSelecionado = representanteLegalSelecionado;
        vm.savingProgress = false;
        vm.salvar = salvar;
        vm.readonly = true;

        function desativarSecoes(secoes){
            secoes.matricula.ativo = false;
            return secoes;
        }

        function representanteLegalSelecionado(representante, mostrarRepresentante) {
            vm.mostrarRepresentante = mostrarRepresentante;
        }

        function salvar() {
            salvandoNoSau();

            resumoFactory
                .salvarCadastroNoSAU(vm.cpf)
                .then(notificarSucesso)
                .catch(notificarErro)
                .finally(salvandoNoSau);
        }

        function salvandoNoSau() {
            vm.savingProgress = !vm.savingProgress;
        }

        function notificarSucesso(res) {
            vm.sucessos = [{ message: 'Cadastro salvo com sucesso.' }];

            var modal = {
                enable: true,
                cpf: vm.cpf,
                nome: vm.resumo.dadosPessoais.nomeCompleto,
                titulo: tituloPagina,
                tipo: 'terceiro'
            };

            $rootScope.$emit('usuario-pendente', modal);
        }

        function notificarErro(erro) {
            if (erro.status == 422) {
                vm.erros = erro.data.body || erro.data.content || [{ message: erro.data.message }];
            }
        }
    }
})(angular.module('app'));
