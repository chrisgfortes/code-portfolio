(function () {
    'use strict';

    angular
        .module('app')
        .value('secoesResumo', {
            dadosPessoais:{
                ativo: true,
                descricao: 'Dados Pessoais'
            },
            dadosComplementares:{
                ativo: true,
                descricao: 'Dados Complementares'
            },
            bens:{
                ativo: true,
                descricao: 'Bens'
            },
            cartaoAutografo:{
                ativo: true,
                descricao: 'Cartão Autógrafo'
            },
            renda:{
                ativo: true,
                descricao: 'Renda'
            },
            matricula:{
                ativo: true,
                descricao: 'Matrícula'
            },
            analiseCadastral:{
                ativo: true,
                descricao: 'Análise Cadastral'
            }
        });
}());
