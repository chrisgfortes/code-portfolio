(function (app) {
    'use strict';

    app.factory('resumo', resumo);

    function resumo($http, HOST) {
        return function (urlBase) {
            return {
                resumoGeral: resumoGeral,
                salvarCadastroNoSAU: salvarCadastroNoSAU
            };

            function resumoGeral(cpf) {
                var hostAssociado = HOST[urlBase] + cpf + '/rascunho';
                return $http.get(hostAssociado)
                    .then(function (res) {
                        return res.data;
                    });
            }

            function salvarCadastroNoSAU(cpf) {
                var hostAssociado = HOST[urlBase] + cpf;
                return $http.post(hostAssociado);
            }
        }
    }

})(angular.module('app'));
