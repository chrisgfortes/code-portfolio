(function () {
    'use strict';

    angular
        .module('app')
        .config(function ($routeProvider) {
            $routeProvider.when('/cadastro/terceiro/:cpf/renda', {
                templateUrl: './modules/rendas/rendas.html',
                controller: 'rendasController',
                controllerAs: '$ctrl',
                resolve: {
                    isTerceiro: function ($route, rotaService) {
                        var cpf = $route.current.params.cpf;
                        return rotaService.isTerceiro(cpf);
                    },
                    rendasInfo: function (rendas, $route) {
                        var factory = rendas('terceiro');
                        return factory
                            .buscar($route.current.params.cpf)
                            .catch(function () {
                                return { rendas:[] };
                            })
                    },
                    urls: function (linksFactory) {
                        return linksFactory('terceiro');
                    },
                    rendaFactory: function (rendas) {
                        return rendas('terceiro');
                    },
                    temPermissao: function (controleDeAcesso) {
                        return controleDeAcesso.temAcesso('Cadastro Inserir');
                    }
                }
            });
        });
})();
