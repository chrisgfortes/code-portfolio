(function (app) {
    'use strict';

    app.controller('rendasController', rendasController);

    function rendasController($routeParams, urls, rendasInfo, rendaFactory) {
        var vm = this;

        vm.cpf = $routeParams.cpf;
        vm.links = urls;
        vm.rendasObj = rendasInfo;
        vm.salvar = salvar;

        function salvar(dados) {

            return rendaFactory
                .salvar(vm.cpf, dados)
                .then(function (res) {
                    vm.erros = [];
                    vm.sucessos = [{ message: 'Os dados renda foram salvos com sucesso.' }];
                }).catch(function (erro) {
                    vm.sucessos = [];
                    if (erro.status == 422) {
                        vm.erros = erro.data.body || erro.data.content;
                    }
                });
        }

    }
})(angular.module('app'));
