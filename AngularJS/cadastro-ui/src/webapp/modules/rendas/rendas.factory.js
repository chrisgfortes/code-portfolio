(function () {
    'use strict';

    angular
        .module('app')
        .factory('rendas', renda)

    function renda($http, HOST, rendaConverter) {

        return function (urlBase) {
            return {
                buscar: buscar,
                salvar: salvar
            }

            function buscar(cpf) {
                var url = urlBuild(cpf);

                return $http
                    .get(url)
                    .then(transformaRetorno)
                    .then(function (objRetorno) {
                        objRetorno.rendas = objRetorno.rendas.map(rendaConverter.get);

                        return objRetorno;
                    });
            }

            function salvar(cpf, dados) {
                var url = urlBuild(cpf);

                return $http.put(url, dados);
            }

            function urlBuild(cpf) {
                return HOST[urlBase] + cpf + '/rascunho/renda';
            }

            function transformaRetorno(res) {
                return res.data;
            }
        }
    }
}());
