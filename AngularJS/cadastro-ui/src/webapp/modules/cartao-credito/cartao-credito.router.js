(function (app) {
    'use strict';

    app.config(function ($routeProvider) {
        $routeProvider.when('/cadastro/associado/:cpf/limite-cartao-credito', {
            templateUrl: './modules/cartao-credito/cartao-credito.html',
            controller: 'cartaoCreditoController',
            controllerAs: 'ctrl',
            params: {
                isContaCorrente: true
            },
            resolve: {
                temPermissao: function (controleDeAcesso) {
                    return controleDeAcesso.temAcesso('Cadastro Inserir');
                }
            }
        });
    });
})(angular.module('app'));