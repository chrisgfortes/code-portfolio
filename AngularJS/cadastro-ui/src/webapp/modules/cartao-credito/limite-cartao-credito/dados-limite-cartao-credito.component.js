(function () {
    'use strict';

    limiteCartaoCreditoController.inject = ['cartaoCredito']

    angular
        .module('app')
        .component('dadosLimiteCartaoCredito', {
            bindings: {
                limiteCartaoCredito: '<'
            },
            controller: limiteCartaoCreditoController,
            templateUrl: './modules/cartao-credito/limite-cartao-credito/dados-limite-cartao-credito.html'
        });

    function limiteCartaoCreditoController(cartaoCredito) {
        var vm = this;

        vm.$onInit = onInit;

        function onInit() {
            vm.linhasDeCredito = [];

            cartaoCredito
                .linhasDeCredito()
                .then(carregarLinhasDeCredito)

            cartaoCredito
                .tiposVencimento()
                .then(carregarTiposVencimento)
        }

        function carregarLinhasDeCredito(linhasDeCredito) {
            vm.linhasDeCredito = linhasDeCredito
        }

        function carregarTiposVencimento(tiposVencimento) {
           vm.tiposVencimento = tiposVencimento
        }
    }
}());