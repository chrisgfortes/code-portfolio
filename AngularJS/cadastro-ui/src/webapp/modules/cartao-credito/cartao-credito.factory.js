(function () {
    'use strict';

    angular
        .module('app')
        .factory('cartaoCredito', cartaoCredito)

    function cartaoCredito($http, HOST, $q) {

        return {
            salvar: salvar,
            tiposVencimento: tiposVencimento,
            linhasDeCredito: linhasDeCredito
        }

        function salvar(linhaCartaoCredito) {
            return $http.post(HOST.contaCorrente + 'limite-cartao-credito', linhaCartaoCredito)
        }

        function tiposVencimento() {
            return $http
                .get(HOST.contaCorrente + 'tipos-vencimento-proposta')
                .then(function (res) {
                    return res.data
                })
        }

        function linhasDeCredito() {
            return $http
                .get(HOST.linhaCredito + '?modalidade=CARTAO_ROTATIVO&ativo=true&tipo-credito=EMPRESTIMO')
                .then(function (res) {
                    return res.data
                })
        }
    }

}());