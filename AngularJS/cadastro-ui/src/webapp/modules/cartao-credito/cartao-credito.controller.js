(function () {
    'use strict';

    angular
        .module('app')
        .controller('cartaoCreditoController', cartaoCreditoController)

    function cartaoCreditoController(cartaoCredito) {
        var vm = this;
        vm.limiteCartaoCredito = {};
        vm.salvar = salvar;

        function salvar(limiteCartaoCredito) {
            return cartaoCredito
                .salvar(limiteCartaoCredito)
                .then(notificarSucesso)
                .catch(notificarErro);
        }

        function notificarSucesso() {
            vm.sucessos = [{ message: 'Os dados Limite de Credito foram salvos com sucesso.' }];
        }

        function notificarErro(erro) {
            if (erro.status == 422) {
                vm.erros = erro.details || [erro.data];

                return;
            }

            vm.erros = [{ message: 'Ocorreu um erro no servidor, tente novamente.' }];
        }
    }

}());