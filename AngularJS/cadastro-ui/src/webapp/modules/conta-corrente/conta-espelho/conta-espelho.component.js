(function () {
    'use strict';

    dadosContaEspelhoController.$inject = ['$rootScope', 'contaCorrente', '$routeParams'];

    angular
        .module('app')
        .component('dadosContaEspelho', {
            bindings: {
                contaEspelho: '='
            },
            controller: dadosContaEspelhoController,
            templateUrl: './modules/conta-corrente/conta-espelho/conta-espelho.html'
        });

    function dadosContaEspelhoController($rootScope, contaCorrente, $routeParams) {
        var vm = this;
        vm.readonly = true;
        vm.tiposCalculoLimiteContaEspelho = []

        vm.$onInit = init;

        $rootScope.$on('conta-corrente-endereco', function (e, endereco) {
            vm.contaEspelho.enderecoCorrespondencia = endereco.descricao;
        });

        function init() {
            vm.contaEspelho = vm.contaEspelho || { enderecoCorrespondencia: '' };

            contaCorrente
                .situacoesContaEspelho()
                .then(carregarSituacoesContaEspelho)

            contaCorrente
                .pacotesTarifaBB()
                .then(carregarPacotesTarifasBB);

            contaCorrente
                .tiposCalculoLimiteContaEspelho()
                .then(carregartiposCalculoLimiteContaEspelho);
        }

        function carregarSituacoesContaEspelho(situacoesContaEspelho) {
            vm.situacoesContaEspelho = situacoesContaEspelho;
        }

        function carregarPacotesTarifasBB(pacotesTarifas) {
            vm.pacotesTarifas = pacotesTarifas;
        }

        function carregartiposCalculoLimiteContaEspelho(tiposCalculoLimiteContaEspelho) {
            vm.tiposCalculoLimiteContaEspelho = tiposCalculoLimiteContaEspelho;
        }
    }

}());
