(function () {
    'use strict';

    angular
        .module('app')
        .factory('contaCorrenteConverter', contaCorrenteConverter)

    function contaCorrenteConverter() {
        return {
            salvar: _salvar
        };

        function _salvar(dados){
            if (!!dados.clienteDesde){
                dados.clienteDesde = (moment(dados.clienteDesde).isValid() || undefined);
            }

            return dados;
        }
    }

}());
