(function () { 
    'use strict';
    
    dadosTarifasController.$inject = ['contaCorrente']
    
    angular
        .module ('app')
        .component ('dadosTarifas', {
            bindings: {
                tarifas: '='
            },
            controller: dadosTarifasController,
            templateUrl: './modules/conta-corrente/tarifas/dados-tarifas.html'
        });
    
        function dadosTarifasController(contaCorrente){
            var vm = this;
            vm.utilizaPacoteTarifa = utilizaPacoteTarifa;
    
            vm.$onInit = onInit;
    
            function onInit() {
                contaCorrente
                    .pacoteServicos()
                    .then(carregarPacotesServicos)
            }

            function utilizaPacoteTarifa(utilizaPacote) {
               if(!utilizaPacote) {
                   vm.tarifas.utilizaPacoteTarifas = false;
                   vm.tarifas.codigoPacote = undefined;
                   vm.tarifas.diaCobranca = undefined;
               }
            }
            
            function carregarPacotesServicos(pacotesServicos) {
            	vm.pacotesServicos = pacotesServicos;
            }
        }
} ());