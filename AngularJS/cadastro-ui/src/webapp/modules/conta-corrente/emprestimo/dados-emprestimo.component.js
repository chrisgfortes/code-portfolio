(function () { 
    'use strict';
    
    emprestimoController.$inject = ['contaCorrente']
    
    angular
        .module ('app')
        .component ('dadosEmprestimo', {
            bindings: {
                indicadoresEmprestimo: '='
            },
            controller: emprestimoController,
            templateUrl: './modules/conta-corrente/emprestimo/dados-emprestimo.html'
        });
    
        function emprestimoController(contaCorrente){
            var vm = this;
    
            vm.$onInit = onInit;
    
            function onInit() {
                
            }
        }
} ());