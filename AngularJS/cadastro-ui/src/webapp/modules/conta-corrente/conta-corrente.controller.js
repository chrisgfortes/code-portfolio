(function(app) {
    'use strict';

    app.controller('contaCorrenteController', contaCorrenteController);

    function contaCorrenteController($routeParams, $rootScope, contaCorrente, $timeout, posto, $location, pessoa) {
        var vm = this;

        vm.salvoComSucesso = false;
        vm.contaCorrente = {
            contaEspelho: {},
            posto: posto
        };
        vm.cpf = $routeParams.cpf;
        vm.salvar = salvar;

        $rootScope.$on('alterado-compe-propria', function(e, compePropria) {
            vm.mostrarContaEspelho = !compePropria;
        });

        function salvar(_contaCorrente) {
            salvandoNoSau();
            var conta = angular.copy(_contaCorrente);

            var endereco = JSON.parse(conta.endereco);

            conta.cpfCnpjPrimeiroTitular = vm.cpf;
            conta.empostamento = endereco.id;
            conta.tipoEnderecoEmpostamento = endereco.tipoEndereco;

            validar(conta);

            contaCorrente
                .salvar(conta)
                .then(notificarSucesso)
                .catch(notificarErro)
                .finally(salvandoNoSau);
        }

        function salvandoNoSau() {
            vm.savingProgress = !vm.savingProgress;
        }

        function validar(conta) {
            if (conta.compePropria) {
                delete conta.contaEspelho;
            }

            if (conta.tarifas.naoUtilizaPacoteTarifas) {
                delete conta.tarifas
            }
        }

        function notificarSucesso(res) {
            vm.salvoComSucesso = true;
            vm.erros = [];
            vm.sucessos = [{
                message: 'Os dados de Conta Corrente foram salvos com sucesso.'
            }];

            pessoa.status(vm.cpf, 'terceiro')
                .then(function(response) {
                    var dadosPessoa = response;
                    var modal = {
                        enable: true,
                        cpf: dadosPessoa.cpfCnpj,
                        nome: dadosPessoa.nome,
                        titulo: 'Correntista',
                        tipo: 'correntista'
                    };
                    $rootScope.$emit('usuario-pendente', modal);
                });

        }

        function notificarErro(erro) {
            vm.sucessos = [];
            if (erro.status == 422) {
                vm.erros = erro.data.details;

                return;
            }

            vm.erros = [{
                message: 'Ocorreu um erro no servidor, tente novamente.'
            }];
        }

    }
})(angular.module('app'));
