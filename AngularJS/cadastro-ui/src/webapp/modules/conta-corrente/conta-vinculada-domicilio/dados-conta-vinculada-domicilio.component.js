(function () {
    'use strict';

    contaVinculadaDomicilioController.$inject = ['contaCorrente', 'moment']

    angular
        .module('app')
        .component('dadosContaVinculadaDomicilio', {
            bindings: {
                contaVinculadaDomicilio: '='
            },
            controller: contaVinculadaDomicilioController,
            templateUrl: './modules/conta-corrente/conta-vinculada-domicilio/dados-conta-vinculada-domicilio.html'
        });

    function contaVinculadaDomicilioController(contaCorrente, moment) {
        var vm = this;
        vm.validaData = validaData;
        vm.mostarVisa = mostarVisa;
        vm.mostarMaster = mostarMaster;

        vm.$onInit = onInit;

        function onInit() {
            vm.contaVinculadaDomicilio = vm.contaVinculadaDomicilio || {};

            contaCorrente
                .domiciliosBancarios()
                .then(carregarDomiciliosBancarios)
        }

        function carregarDomiciliosBancarios(domiciliosBancarios) {
            vm.domiciliosBancarios = domiciliosBancarios;
            vm.contaVinculadaDomicilio.tipoTravaDomicilioBancario = vm.contaVinculadaDomicilio.tipoTravaDomicilioBancario || 'NAO';
        }

        function validaData(data, campo) {
            if (!moment(data).isValid()) {
                vm.contaVinculadaDomicilio[campo] = undefined;
            }
        }

        function mostarVisa(domicilioBancario) {
           return ['AMBAS', 'VISA'].some(function (item) {
              return item == domicilioBancario;
           })
        }

        function mostarMaster(domicilioBancario) {
           return ['AMBAS', 'MASTER'].some(function (item) {
              return item == domicilioBancario;
           })
        }
    }
}());