(function () { 
    'use strict';
    
    aplicacaoFinanceiraController.$inject = []
    
    angular
        .module ('app')
        .component ('dadosAplicacaoFinanceira', {
            bindings: {
                aplicacaoFinanceira: '='
            },
            controller: aplicacaoFinanceiraController,
            templateUrl: './modules/conta-corrente/aplicacao-financeira/dados-aplicacao-financeira.html'
        });
    
        function aplicacaoFinanceiraController(){
            var vm = this;
    
            vm.$onInit = onInit;
    
            function onInit() {
                
            }
        }
} ());