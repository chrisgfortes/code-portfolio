(function () {
    'use strict';

    angular
        .module('app')
        .factory('contaCorrente', contaCorrente)

    function contaCorrente($http, HOST, $q, $route, contaCorrenteConverter) {

        return {
            tiposConta: tiposConta,
            modalidades: modalidades,
            contatosUnicred: contatosUnicred,
            assinaturas: assinaturas,
            cpmf: cpmf,
            situacoes: situacoes,
            pacotesTarifaBB: pacotesTarifaBB,
            situacoesContaEspelho: situacoesContaEspelho,
            tiposCalculoLimiteContaEspelho: tiposCalculoLimiteContaEspelho,
            domiciliosBancarios: domiciliosBancarios,
            numerosFolhasSolicitadas: numerosFolhasSolicitadas,
            pacoteServicos: pacoteServicos,
            salvar: salvar,
            buscarPeloCpf: buscarPeloCpf
        };

        function tiposConta() {
            return $http.get(HOST.contaCorrente + 'tipos-conta')
                .then(function (res) {
                    return res.data;
                });
        }

        function modalidades() {
            return $http.get(HOST.contaCorrente + 'modalidades')
                .then(function (res) {
                    return res.data;
                });
        }

        function contatosUnicred() {
            return $http.get(HOST.contaCorrente + 'gerentes')
                .then(function (res) {
                    return res.data;
                });
        }

        function assinaturas() {
            return $http.get(HOST.contaCorrente + 'assinaturas')
                .then(function (res) {
                    return res.data;
                });
        }

        function cpmf() {
            return $http.get(HOST.contaCorrente + 'tipos-cpmf')
                .then(function (res) {
                    return res.data;
                });
        }

        function situacoes() {
            return $http.get(HOST.contaCorrente + 'situacoes')
                .then(function (res) {
                    return res.data;
                });
        }

        function situacoesContaEspelho() {
            return $http.get(HOST.contaCorrente + 'situacoes-conta-espelho')
                .then(function (res) {
                    return res.data;
                });
        }

        function pacotesTarifaBB() {
            return $http
                .get(HOST.contaCorrente + 'pacotes-tarifas-conta-espelho')
                .then(function (res) {
                   return res.data;
                });
        }

        function tiposCalculoLimiteContaEspelho() {
            return $http
                .get(HOST.contaCorrente + 'tipos-calculo-limite-conta-espelho')
                .then(function (res) {
                   return res.data;
                });
        }

        function domiciliosBancarios() {
        	return $http
	            .get(HOST.contaCorrente + 'tipos-travas-domicilio-bancario')
	            .then(function (res) {
	               return res.data;
	            });
        }

        function numerosFolhasSolicitadas() {
        	return $http
            .get(HOST.contaCorrente + 'numero-folhas-talao')
            .then(function (res) {
               return res.data;
            });
        }

        function pacoteServicos() {
            return $http
                .get(HOST.contaCorrente + 'pacote-servicos')
                .then(function (res) {
                   return res.data;
                });
        }

        function salvar(contaCorrente) {
            var dados = contaCorrenteConverter.salvar(contaCorrente);
            return $http.post(HOST.contaCorrente, dados);
        }

        function buscarPeloCpf(cpf) {
            return $http
                .get(HOST.contaCorrente + '?cpfCnpj=' + cpf)
                .then(function (res) {
                   return res.data;
                });
        }
    }

}());
