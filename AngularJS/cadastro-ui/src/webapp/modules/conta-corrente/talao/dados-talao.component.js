(function () {
    'use strict';

    talaoController.$inject = ['contaCorrente']

    angular
        .module('app')
        .component('dadosTalao', {
            bindings: {
                talao: '='
            },
            controller: talaoController,
            templateUrl: './modules/conta-corrente/talao/dados-talao.html'
        });

    function talaoController(contaCorrente) {
        var vm = this;

        vm.$onInit = onInit;

        function onInit() {
            vm.talao = vm.talao || {};
            vm.talao.numeroMinimoTaloes = vm.talao.numeroMinimoTaloes || "0";
            vm.talao.numeroMinimoTaloesTb = vm.talao.numeroMinimoTaloesTb || "0";

            contaCorrente
                .numerosFolhasSolicitadas()
                .then(carregarNumerosFolhasSolicitadas)
        }

        function carregarNumerosFolhasSolicitadas(numerosFolhasSolicitadas) {
            vm.numerosFolhasSolicitadas = numerosFolhasSolicitadas;
            vm.talao.numeroFolhasSolicitacaoAutomatica =  numerosFolhasSolicitadas[0] + '';
        }
    }
}());