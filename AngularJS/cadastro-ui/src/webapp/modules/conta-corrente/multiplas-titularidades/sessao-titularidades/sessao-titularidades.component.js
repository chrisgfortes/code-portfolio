(function () {
    'use strict';

    sessaoTitularidadesController.$inject = ['dialogs', 'focus', '$routeParams', 'associado'];

    angular
        .module('app')
        .component('sessaoTitularidades', {
            transclude: true,
            bindings: {
                titularidades: '=',
                readonly: '<',
                mostraSessao: '<'
            },
            controller: sessaoTitularidadesController,
            templateUrl: './modules/conta-corrente/multiplas-titularidades/sessao-titularidades/sessao-titularidades.html'
        });

    function sessaoTitularidadesController(dialogs, focus, $routeParams, associado) {
        var vm = this;
        
        vm.$onInit = onInit;
        vm.salvar = salvar;
        vm.cancelar = cancelar;
        vm.novo = novo;
        vm.remover = remover;
        vm.editar = editar;

        function onInit() {
            vm.mostrarTitularForm = false;
            vm.titularidades = vm.titularidades || [];

            associado
                .buscarMatricula($routeParams.cpf)
                .then(_thenBuscarMatricula);

            function _thenBuscarMatricula(res) {
                vm.matriculaUser = (!!res.matricula.matricula ? res.matricula.matricula : 0);
            }
        }

        function salvar(titular) {
            var _titular = angular.copy(titular);

            if (!_validarMatriculaMesmoTitular(_titular.matricula)){
                dialogs.notify('Atenção', 'Você não pode cadastrar como 2º titular o mesmo titular da conta corrente.');
                return false;
            }

            if (_titular.modoEdicao) {
                atualizarListaTitulares(_titular)
            } else {
                if (!_validarMatricula(_titular)){
                    dialogs.notify('Atenção', 'A matricula informada está incorreta.');
                    return false;
                }

                if (!_validarMatriculasExiste(_titular)){
                    vm.titularidades.push(_titular);
                }else{
                    dialogs.notify('Atenção', 'A matricula informada já está cadastrada.');
                    return false;
                }
            }

            vm.titular = {};
            vm.mostrarTitularForm = false;
        }

        function _validarMatriculaMesmoTitular(matricula){
            return (matricula != vm.matriculaUser);
        }

        function _validarMatricula(titular){
            var qtdDigitMatricula = (String(titular.matricula).length >= 2);
            var existCpfName = (titular.nome && titular.cpfCnpj);
            return (qtdDigitMatricula && existCpfName);
        }

        function _validarMatriculasExiste(titular){
            var validate = vm.titularidades.some(function(item){
                return titular.matricula == item.matricula;
            });
            
            return validate;
        }

        function atualizarListaTitulares(titular) {
            vm.titularidades = vm.titularidades.map(function (item) {
                if (item.modoEdicao) {
                    titular.modoEdicao = false;
                    return titular;
                }
                return item;
            });
        }

        function cancelar() {
            vm.mostrarTitularForm = false;

            vm.titularidades = vm.titularidades.map(function (titular) {
                titular.modoEdicao = false;
                return titular;
            })

            vm.titular = {};
        }

        function novo() {
            vm.mostrarTitularForm = true;
        }

        function remover(titular) {
            var confirmacao = dialogs.confirm('Confirmar', "Confirma a remoção do titular?");

            confirmacao
                .result
                .then(removerDaLista(titular))
                .catch(desfazerModoExclusao);
        }

        function removerDaLista(contato) {
            return function (btn) {
                vm.titularidades = vm.titularidades.filter(modoEclusao);
            }
        }

        function modoEclusao(titular) {
            return !titular.modoExclusao;
        }

        function desfazerModoExclusao() {
            vm.titularidades = vm.titularidades.filter(desabilitarModoExlucao);
        }

        function desabilitarModoExlucao(titular) {
            titular.modoExclusao = false;
            return titular;
        }

        function editar(titular) {
            vm.mostrarTitularForm = true;
            vm.titular = angular.copy(titular);
        }
    }
}());
