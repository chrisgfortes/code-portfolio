(function () {
    'use strict';

    listaTitularidadesController.$inject = [];

    angular
        .module('app')
        .component('listaTitularidades', {
            bindings: {
                titularidades: '<',
                readonly: '<',
                onEditar: '&',
                onNovo: '&',
                onRemover: '&',
                esconderBotaoNovo: '<'
            },
            controller: listaTitularidadesController,
            templateUrl: './modules/conta-corrente/multiplas-titularidades/lista-titularidades/lista-titularidades.html'
        });

    function listaTitularidadesController() {
        var vm = this;

        vm.$onInit = init;
        vm.remover = remover;
        vm.novo = novo;
        vm.editar = editar;

        function init() {}

        function remover(titular) {
            titular.modoExclusao = true;
            vm.onRemover({ titular: angular.copy(titular) });
        }

        function novo() {
            vm.onNovo();
        }

        function editar(titular) {
            vm.titularidades = vm.titularidades.map(function (item) {
                item.modoEdicao = false;
                return item;
            });

            titular.modoEdicao = true;

            vm.onEditar({ titular: angular.copy(titular) });
        }
    }
}());
