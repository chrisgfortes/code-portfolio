(function(){
    'use strict';

    angular
        .module('app')
        .factory('titularidades', titularidades)

    /** @ngInject */
    function titularidades($http, HOST){

        return {
            get: _get
        }

        function _get(matricula){
            return $http.get(HOST.cadastro + '/terceiros/terceiro?tipo=MATRICULA&valor=' + matricula, { cache: true });
        }
    }
}());
