(function () {
    dadosTitularController.$inject = [
        'titularidades', 
        'message', 
        'dialogs', 
        '$rootScope',
        '$routeParams'
    ];

    angular
        .module('app')
        .component('dadosTitular', {
            bindings: {
                titular: '<',
                readonly: '<',
                onSalvar: '&',
                onCancelar: '&'
            },
            controller: dadosTitularController,
            templateUrl: './modules/conta-corrente/multiplas-titularidades/dados-titular/dados-titular.template.html'
        });

    function dadosTitularController(titularidades, message, dialogs, $rootScope, $routeParams) {
        var vm = this;

        vm.$onInit = init;
        vm.salvar = salvar;
        vm.cancelar = cancelar;
        vm.getMatricula = _getMatricula;

        function init() {
            vm.titular = vm.titular || {};
        }

        function salvar(titular) {
            vm.onSalvar({ titular: angular.copy(titular) });
            vm.titular = {};
        }

        function cancelar() {
            vm.onCancelar();
        }

        function _getMatricula(matricula){
            if (matricula){
                titularidades
                .get(matricula)
                .then(_thenGetMatricula)
                .catch(_catchGetMatricula);
                
                function _thenGetMatricula(res){
                    var associado = res.data;

                    vm.titular.cpfCnpj = associado.cpfCnpj;
                    vm.titular.nome = associado.nome;
                }

                function _catchGetMatricula(error) {
                    vm.titular = {};
                    vm.titular.matricula = matricula;

                    var msg = message.concat(
                        'A matricula', 
                        matricula,
                        'não foi encontrada, tente novamente'
                    );

                    $rootScope.$emit('mostrar-mensagem', {
                        titulo: 'Vincular 2º Titular',
                        descricao: msg
                    });
                }
            }
        }
    };
}());
