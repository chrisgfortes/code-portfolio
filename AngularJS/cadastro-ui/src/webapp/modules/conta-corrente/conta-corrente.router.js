(function(app) {
    'use strict';

    app.config(function($routeProvider) {
        $routeProvider.when('/cadastro/correntista/:cpf/conta-corrente', {
            templateUrl: './modules/conta-corrente/conta-corrente.html',
            controller: 'contaCorrenteController',
            controllerAs: 'ctrl',
            resolve: {
                isNotAssociado: function($route, rotaService) {
                    var cpf = $route.current.params.cpf;
                    return rotaService.isNotAssociado(cpf);
                },
                temPermissao: function(controleDeAcesso) {
                    return controleDeAcesso.temAcesso('Cadastro Inserir');
                },
                posto: function(associado, $route) {
                    var cpf = $route.current.params.cpf;

                    return associado
                        .buscarMatricula(cpf)
                        .then(function(associado) {
                            return associado.matricula.posto;
                        })
                        .catch(function(erro) {
                            return 0;
                        })
                }
            }
        });
    });
})(angular.module('app'));
