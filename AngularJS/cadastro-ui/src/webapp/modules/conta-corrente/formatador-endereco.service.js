(function () {
    'use strict';

    angular
        .module('app')
        .service('formatadorEndereco', formatadorEndereco)

    function formatadorEndereco() {

        this.formatar = formatar;

        function formatar(endereco) {
            if(!endereco) throw('Endereço não informado.')

            var numero = endereco.enderecoSemNumero ? ', ' + endereco.numero : '';
            var cep = ', CEP - ' + endereco.cep;

            return endereco.logradouro + numero + cep;
        }
    }

}());