(function () {
    'use strict';

    contaCorrenteController.$inject = ['contaCorrente', 'moment', '$rootScope', '$routeParams', 'formatadorEndereco', 'enderecoUpdate', '$rootScope']

    angular
        .module('app')
        .component('dadosContaCorrente', {
            bindings: {
                contaCorrente: '='
            },
            controller: contaCorrenteController,
            templateUrl: './modules/conta-corrente/gestao-conta-corrente/dados-conta-corrente.html'
        });

    function contaCorrenteController(contaCorrente, moment, $rootScope, $routeParams, formatadorEndereco, enderecoUpdate) {
        var vm = this;

        vm.hoje = moment();

        vm.$onInit = onInit;
        vm.alteradoCompePropria = alteradoCompePropria;
        vm.alteradoEndereco = alteradoEndereco;

        function onInit() {
            vm.contaCorrente = vm.contaCorrente || {};
            vm.contaCorrente.centralizaSaldo = true;
            vm.contaCorrente.compePropria = true;

            contaCorrente
                .tiposConta()
                .then(carregarTiposConta);

            contaCorrente
                .modalidades()
                .then(carregarModalidades);

            contaCorrente
                .contatosUnicred()
                .then(carregarContatosUnicred);

            contaCorrente
                .assinaturas()
                .then(carregarAssinaturas);

            contaCorrente
                .cpmf()
                .then(carregarCPMF);

            contaCorrente
                .situacoes()
                .then(carregarSituacoes);

            enderecoUpdate
                .buscar($routeParams.cpf)
                .then(carregarEnderecos);

            alteradoCompePropria();
        }

        function carregarTiposConta(tiposContas) {
            vm.tiposConta = tiposContas;
        }

        function carregarModalidades(modalidades) {
            vm.modalidades = modalidades
        }

        function carregarContatosUnicred(contatosUnicred) {
            vm.contatosUnicred = contatosUnicred;
        }

        function carregarAssinaturas(assinaturas) {
            vm.assinaturas = assinaturas;
        }

        function carregarCPMF(cpmfLista) {
            vm.cpmfLista = cpmfLista;
        }

        function carregarSituacoes(situacoes) {
            vm.situacoes = situacoes;
        }

        function carregarEnderecos(enderecos) {
            vm.enderecos = enderecos.map(function (endereco) {
                return {
                    id: endereco.id,
                    descricao: formatadorEndereco.formatar(endereco),
                    tipoEndereco: endereco.tipoEndereco
                };
            });
        }

        function alteradoCompePropria() {
            $rootScope.$broadcast('alterado-compe-propria', vm.contaCorrente.compePropria);
        }

        function alteradoEndereco(endereco) {
            $rootScope.$broadcast('conta-corrente-endereco',  JSON.parse(endereco));
        }
    }
}());