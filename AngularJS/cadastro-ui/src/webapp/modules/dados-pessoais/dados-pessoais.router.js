(function(app) {
    'use strict';

    app.config(function($routeProvider) {
        $routeProvider.when('/cadastro/terceiro/:cpf/dados-pessoais/', {
            templateUrl: './modules/dados-pessoais/dados-pessoais.html',
            controller: 'dadosPessoaisController',
            controllerAs: '$ctrl',
            resolve: {
                isTerceiro: function ($route, rotaService) {
                    var cpf = $route.current.params.cpf;
                    return rotaService.isTerceiro(cpf);
                },
                dadosPessoaisInfo: function (dadosPessoais, $route) {
                    var factory = dadosPessoais('terceiro');
                    return factory
                      .buscar($route.current.params.cpf)
                      .then(function (res) {
                          return verificarRetorno(res, $route.current.params.cpf)
                      })
                      .catch(function () {
                          return { cpf: $route.current.params.cpf };
                      })
                },
                urls: function (linksFactory) {
                    return linksFactory('terceiro');
                },
                dadosPessoaisFactory: function (dadosPessoais){
                    return dadosPessoais('terceiro');
                },
                temPermissao: function (controleDeAcesso) {
                    return controleDeAcesso.temAcesso('Cadastro Inserir');
                }
            }
        });

        function verificarRetorno(response, cpf) {
            response.cpf = response.cpf || cpf;
            return response;
        }
    });
})(angular.module('app'));
