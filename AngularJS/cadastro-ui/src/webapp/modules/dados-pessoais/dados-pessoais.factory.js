(function () {
    'use strict';

    angular
        .module('app')
        .factory('dadosPessoais', dadosPessoais)

    /** @ngInject */
    function dadosPessoais($http, HOST, documentoConvert, dependenteConvert, dadosProfissionaisConvert, conjugeConvert) {

        return function (urlBase) {
            return {
                buscar: buscar,
                salvar: salvar
            }

            function buscar(cpf) {
                var url = urlBuild(cpf);

                return $http.get(url)
                    .then(function (res) {
                        dadosPessoais = res.data;

                        return dadosPessoais;
                    });
            }

            function salvar(dadosPessoais) {
                var _dadosPessoais = angular.copy(dadosPessoais)

                var url = urlBuild(dadosPessoais.cpf);

                _dadosPessoais.documento = documentoConvert.post(_dadosPessoais.documento);
                _dadosPessoais.dependentes = _dadosPessoais.dependentes.map(dependenteConvert.post);
                _dadosPessoais.dadosProfissionais = dadosProfissionaisConvert.post(_dadosPessoais.dadosProfissionais);
                _dadosPessoais.conjuge = conjugeConvert.post(_dadosPessoais.conjuge);

                return $http.put(url, _dadosPessoais);
            }

            function urlBuild(cpf) {
                return HOST[urlBase] + cpf + '/rascunho/dadospessoais';
            }
        }
    }
}());
