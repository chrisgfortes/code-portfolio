(function (app) {
    'use strict';

    app.controller('dadosPessoaisController', dadosPessoaisController);

    function dadosPessoaisController($routeParams, urls, dadosPessoaisFactory, dadosPessoaisInfo) {
        var vm = this;

        vm.links = urls;
        vm.dadosPessoais = dadosPessoaisInfo;
        vm.dadosPessoais.dependentes = vm.dadosPessoais.dependentes || [];
        vm.representanteLegalSelecionado = representanteLegalSelecionado;
        vm.mostrarRepresentante = false;

        vm.salvar = salvar;

        function representanteLegalSelecionado (representante, mostrarRepresentante) {
            vm.mostrarRepresentante = mostrarRepresentante
            vm.dadosPessoais.representanteLegal = vm.dadosPessoais.representanteLegal || { endereco: {} };

            if (!vm.mostrarRepresentante) {
                delete vm.dadosPessoais.representanteLegal;
            }
        }

        function salvar(dados) {
            return dadosPessoaisFactory
                .salvar(dados)
                .then(function (res) {
                    vm.sucessos = [{ message: 'Os dados pessoais foram salvos com sucesso.' }];
                })
                .catch(function (erro) {
                    if (erro.status == 422) {
                         vm.erros = erro.data.body || erro.data.content;
                    }
                });
        }
    }

})(angular.module('app'));