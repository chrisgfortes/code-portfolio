(function () {
    'use strict';

    angular
        .module('app')
        .factory('planoDeSaude', planoDeSaude)

    /** @ngInject */
    function planoDeSaude($http, HOST) {

        return {
            tiposPlanoSaude: tiposPlanoSaude,
            coberturasPlanoSaude: coberturasPlanoSaude,
            instituicoesPlanoSaude: instituicoesPlanoSaude
        }

        function tiposPlanoSaude() {
            return $http.get(ulrBase() + '/tipos', { cache: true });
        }

        function coberturasPlanoSaude() {
            return $http.get(ulrBase() + '/coberturas', { cache: true });
        }

        function instituicoesPlanoSaude() {
            return $http.get(ulrBase() + '/instituicoes', { cache: true });
        }

        function ulrBase() {
            return HOST.pessoa + '/fisica/plano-saude';
        }
    }

}());