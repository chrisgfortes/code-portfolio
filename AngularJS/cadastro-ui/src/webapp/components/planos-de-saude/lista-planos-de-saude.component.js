(function () {
    'use strict';

    listaPlanosDeSaudeController.$inject = ['$rootScope', 'dialogs'];

    angular
        .module('app')
        .component('listaPlanosDeSaude', {
            bindings: {
                planosSaude: '<',
                readonly: '<',
                onEditar: '&',
                onNovo: '&',
                onRemover: '&',
                escondeNovo: '<'
            },
            controller: listaPlanosDeSaudeController,
            templateUrl: './components/planos-de-saude/lista-planos-de-saude.html'
        });

    function listaPlanosDeSaudeController($rootScope, dialogs) {
        var vm = this;

        vm.remover = remover;
        vm.editar = editar;
        vm.novo = novo;
        vm.tiposPlanoSaude = [];
        vm.instituicoesPlanoSaude = [];
        vm.$onInit = onInit;

        function onInit() {
            vm.planosSaude = vm.planosSaude || [];
        }

        function novo () {
            vm.onNovo();
        }

        function remover(planoDeSaude) {
            planoDeSaude.modoExclusao = true;
            vm.onRemover({ planoDeSaude: angular.copy(planoDeSaude) });
        }

        function editar(index) {
            vm.planosSaude = vm.planosSaude.map(function (p) {
                p.modoEdicao = false;
                return p;
            })

            var plano = vm.planosSaude[index];
            plano.modoEdicao = true;

            vm.onEditar({ planoDeSaude: plano });
        }
    }
}());
