(function () {
    'use strict';

    sessaoPlanoDeSaudeController.$inject = ['dialogs', 'moment', 'planoDeSaude', 'focus'];


    angular
        .module('app')
        .component('sessaoPlanoDeSaude', {
            transclude: true,
            bindings: {
                planosSaude: '=',
                readonly: '<'
            },
            controller: sessaoPlanoDeSaudeController,
            templateUrl: './components/planos-de-saude/sessao-plano-de-saude.html'
        });

    function sessaoPlanoDeSaudeController(dialogs, moment, planoDeSaude, focus) {
        var vm = this;

        vm.salvarPlanoDeSaude = salvarPlanoDeSaude;
        vm.cancelarPlanoDeSaude = cancelarPlanoDeSaude;
        vm.editarPlanoDeSaude = editarPlanoDeSaude;
        vm.novoPlanoDeSaude = novoPlanoDeSaude;
        vm.removerPlanoDeSaude = removerPlanoDeSaude;
        vm.planoDeSaude = {};
        vm.planoDeSaudeEdicao = {};
        vm.mostrarPlanoDeSaudeForm = false;
        vm.$onInit = onInit;

        function onInit() {
            vm.planosSaude = vm.planosSaude || [];

            planoDeSaude
                .tiposPlanoSaude()
                .then(function (res) {
                    vm.tiposPlanoSaude = res.data;
                    vm.planosSaude = vm.planosSaude.map(adicionaDescricaoTipoDePlano);
                });


            planoDeSaude
                .instituicoesPlanoSaude()
                .then(function (res) {
                    vm.instituicoesPlanoSaude = res.data;
                    vm.planosSaude = vm.planosSaude.map(adicionaDescricaoTipoInstituicao);
                });
        }

        function adicionaDescricaoTipoDePlano (plano) {
            var descricaoTipoDePlano = vm.tiposPlanoSaude.filter(filtrarPlanoSelecionado(plano))[0];
            plano.descricaoTipoDePlano = descricaoTipoDePlano.descricao;
            return plano;
        }

        function filtrarPlanoSelecionado(plano) {
            return function (tipoPlano) {
                return tipoPlano.valor == plano.tipoPlanoSaude;
            }
        }

        function adicionaDescricaoTipoInstituicao(plano) {
            var descricaoInstituicao = vm.instituicoesPlanoSaude.filter(filtrarInstituicaoSelecionada(plano))[0];
            plano.descricaoInstituicao = descricaoInstituicao.descricao;
            return plano;
        }

        function filtrarInstituicaoSelecionada(plano) {
            return function (instituicao) {
                return instituicao.valor == plano.instituicaoSaude;
            }
        }

        function salvarPlanoDeSaude(planoDeSaude) {
            var plano = angular.copy(planoDeSaude);

            if (planoDeSaude.modoEdicao) {
                atualizarListaPlanosDeSaude(plano)
            } else {
                vm.planosSaude.push(plano);
            }

            vm.planoDeSaude = {};
            vm.mostrarPlanoDeSaudeForm = false;
            focus('novoPlanoDeSaude');
        }

        function cancelarPlanoDeSaude() {
            vm.planoDeSaude = {};

            vm.planosSaude = vm.planosSaude.map(function (p) {
                p.modoEdicao = false;
                return p;
            });

            vm.mostrarPlanoDeSaudeForm = false;
            focus('novoPlanoDeSaude');
        }

        function editarPlanoDeSaude(planoDeSaude) {
            vm.mostrarPlanoDeSaudeForm = true;
            vm.planoDeSaude = angular.copy(planoDeSaude);
            focus('tipoPlanoSaude');
        }

        function novoPlanoDeSaude() {
            vm.mostrarPlanoDeSaudeForm = true;
            focus('tipoPlanoSaude');
        }

        function atualizarListaPlanosDeSaude(plano) {
            vm.planosSaude = vm.planosSaude.map(function (item) {
                if (item.modoEdicao) {
                    plano.modoEdicao = false;
                    return plano;
                }
                return item;
            });
        }

        function removerPlanoDeSaude(planoDeSaude) {
            var confirmacao = dialogs.confirm('Confirmar', 'Confirma a remoção do plano de saúde?');
            confirmacao
                .result
                .then(function (btn) {
                    vm.planosSaude = vm.planosSaude.filter(function (plano) {
                        return !plano.modoExclusao;
                    });
                })
                .catch(function () {
                    vm.planosSaude = vm.planosSaude.map(function (plano) {
                        plano.modoExclusao = false;
                        return plano;
                    });
                });
            focus('novoPlanoDeSaude');
        }
    }
}());
