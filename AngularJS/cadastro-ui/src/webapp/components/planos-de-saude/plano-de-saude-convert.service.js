(function () {
    'use strict';

    angular
        .module('app')
        .service('planoDeSaudeConvert', planoDeSaudeConvert)

    function planoDeSaudeConvert(moment) {

        this.convertGet = convertGet;
        this.convertPost = convertPost;

        function convertGet(planosSaude) {
            return planosSaude.map(function (plano) {
                if (!!plano.dataVencimento) {
                    plano.dataVencimento = moment(plano.dataVencimento);
                }
                return plano;
            })
        }

        function convertPost(planosSaude) {
            return planosSaude.map(function (plano) {
                delete plano.descricaoTipoDePlano;
                delete plano.descricaoInstituicao;
                return plano;
            })
        }
    }

}());
