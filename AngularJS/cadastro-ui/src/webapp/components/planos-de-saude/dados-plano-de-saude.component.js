(function () {
    'use strict';

    planoDeSaudeController.$inject = ['moment', '$rootScope', 'planoDeSaude'];

    angular
        .module('app')
        .component('dadosPlanoDeSaude', {
            bindings: {
                planoDeSaude: '<',
                onSalvar: '&',
                onCancelar: '&'
            },
            controller: planoDeSaudeController,
            templateUrl: './components/planos-de-saude/dados-plano-de-saude.html'
        });

    function planoDeSaudeController(moment, $rootScope, planoDeSaude) {
        var vm = this;
        vm.vencimentoMinima = moment();
        vm.$onInit = onInit;
        vm.salvar = salvar;
        vm.cancelar = cancelar;
        vm.tiposPlanoSaude = [];
        vm.coberturasPlanoSaude = [];
        vm.instituicoesPlanoSaude = [];
        vm.validarData = validarData;

        function onInit() {

            planoDeSaude
                .tiposPlanoSaude()
                .then(carregar('tiposPlanoSaude'));

            planoDeSaude
                .instituicoesPlanoSaude()
                .then(carregar('instituicoesPlanoSaude'));

            planoDeSaude
                .coberturasPlanoSaude()
                .then(carregar('coberturasPlanoSaude'));
        }

        function salvar() {
            vm.planoDeSaude.descricaoTipoDePlano = vm.tiposPlanoSaude.filter(function (tipoPlano) {
                return tipoPlano.valor == vm.planoDeSaude.tipoPlanoSaude;
            })[0];

            vm.planoDeSaude.descricaoTipoDePlano = vm.planoDeSaude.descricaoTipoDePlano.descricao;

            vm.planoDeSaude.descricaoInstituicao = vm.instituicoesPlanoSaude.filter(function (instituicao) {
                return instituicao.valor == vm.planoDeSaude.instituicaoSaude;
            })[0];

            vm.planoDeSaude.descricaoInstituicao = vm.planoDeSaude.descricaoInstituicao.descricao;

            vm.onSalvar({ planoDeSaude: angular.copy(vm.planoDeSaude) });
        }

        function cancelar() {
            vm.onCancelar();
        }

        function carregar(itens) {
           return function (res) {
               vm[itens] = res.data;
           }
        }

        function validarData(dataVencimento) {
            if (!validar(dataVencimento)) {
                vm.planoDeSaude.dataVencimento = undefined;
            }
        }

        function validar(data) {
            return moment(data, 'DD/MM/YYYY', true).isValid();
        }
    }
}());
