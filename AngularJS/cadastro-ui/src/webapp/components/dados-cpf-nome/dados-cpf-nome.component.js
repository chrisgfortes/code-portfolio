(function () {
    'use strict';

    dadosCpfNomeController.$inject = [];

    angular
        .module('app')
        .component('dadosCpfNome', {
            bindings: {
                cpfNome: '=',
                readonly: '<'
            },
            controller: dadosCpfNomeController,
            templateUrl: './components/dados-cpf-nome/dados-cpf-nome.html'
        });

    function dadosCpfNomeController() {
        var vm = this;  

        vm.$onInit = function () {
            vm.cpfNome = vm.cpfNome || {};
        }

        vm.abreviarNome = function (nomeCompleto) {
            if (nomeCompleto) {
                var arr = nomeCompleto.split(/\s/g);

                if (arr.length == 1) {
                    vm.cpfNome.nomeSucinto = arr.slice(0, 1)[0];
                } else {
                    vm.cpfNome.nomeSucinto = arr.slice(0, 1) + ' ' + arr.slice(-1);
                }

                if (vm.cpfNome.nomeSucinto.length > 26) {
                    vm.cpfNome.nomeSucinto = vm.cpfNome.nomeSucinto.substring(0, 26);
                }
            }
        }
    }
}());
