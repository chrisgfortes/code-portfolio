(function () {
    'use strict';

    participacaoSocietariaController.$inject = [];

    angular
        .module('app')
        .component('listaParticipacaoSocietaria', {
            bindings: {
                participacoesSocietarias: '<',
                onEditar: '&',
                onNovo: '&',
                onRemover: '&',
                escondeNovo: '<',
                readonly: '<'
            },
            controller: participacaoSocietariaController,
            templateUrl: './components/participacoes-societarias/lista-participacao-societaria.html'
        });

    function participacaoSocietariaController() {
        var vm = this;

        vm.$onInit = onInit;
        vm.remover = remover;
        vm.editar = editar;
        vm.novo = novo;

        function onInit() {
        }

        function novo () {
            vm.onNovo();
        }

        function remover(participacaoSocietaria) {
            participacaoSocietaria.modoExclusao = true;
            vm.onRemover({ participacaoSocietaria: angular.copy(participacaoSocietaria) });
        }

        function editar(index) {
            vm.participacoesSocietarias = vm.participacoesSocietarias.map(function (p) {
                p.modoEdicao = false;

                return p;
            })

            var participacao = vm.participacoesSocietarias[index];
            participacao.modoEdicao = true;

            vm.onEditar({ participacaoSocietaria: participacao});
        }
    }
}());
