(function () {
    'use strict';

    participacaoSocietariaController.$inject = ['participacaoSocietaria', 'dialogs'];

    angular
        .module('app')
        .component('dadosParticipacaoSocietaria', {
            bindings: {
                participacaoSocietaria: '<',
                participacoesSocietarias: '<',
                onSalvar: '&',
                onCancelar: '&',

            },
            controller: participacaoSocietariaController,
            templateUrl: './components/participacoes-societarias/dados-participacao-societaria.html'
        });

    function participacaoSocietariaController(participacaoSocietaria, dialogs) {
        var vm = this;

        vm.$onInit = onInit;
        vm.salvar = salvar;
        vm.cancelar = cancelar;
        vm.funcoesCargos = [];

        function onInit() {
            participacaoSocietaria
                .funcoes()
                .then(function (res) {
                    vm.funcoesCargos = res.data;
                });
        }

        function salvar() {
            vm.participacaoSocietaria.descricaoFuncaoCargo = vm.funcoesCargos.filter(function (tipoPlano) {
                return tipoPlano.valor == vm.participacaoSocietaria.funcaoCargo;
            })[0];
            vm.participacaoSocietaria.descricaoFuncaoCargo = vm.participacaoSocietaria.descricaoFuncaoCargo.descricao;

            vm.onSalvar({ participacaoSocietaria: angular.copy(vm.participacaoSocietaria) });
        }

        function cancelar() {
            vm.onCancelar();
        }
    }
}());
