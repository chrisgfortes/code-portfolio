(function () {
    'use strict';

    sessaoParticipacaoSocietariaController.$inject = ['dialogs', 'focus', 'participacaoSocietaria'];

    angular
        .module('app')
        .component('sessaoParticipacaoSocietaria', {
            bindings: {
                participacoesSocietarias: '=',
                readonly: '<'
            },
            controller: sessaoParticipacaoSocietariaController,
            templateUrl: './components/participacoes-societarias/sessao-participacao-societaria.html'
        });

    function sessaoParticipacaoSocietariaController(dialogs, focus, participacaoSocietaria) {
        var vm = this;

        vm.salvarParticipacaoSocietaria = salvarParticipacaoSocietaria;
        vm.cancelarParticipacaoSocietaria = cancelarParticipacaoSocietaria;
        vm.editarParticipacaoSocietaria = editarParticipacaoSocietaria;
        vm.novoParticipacaoSocietaria = novoParticipacaoSocietaria;
        vm.removerParticipacaoSocietaria = removerParticipacaoSocietaria;
        vm.participacaoSocietaria = {};
        vm.mostrarParticipacaoSocietariaForm = false;
        vm.funcoesCargos = [];
        vm.$onInit = onInit;

        function onInit() {
            vm.participacoesSocietarias = vm.participacoesSocietarias || [];
            participacaoSocietaria
                .funcoes()
                .then(function (res) {
                    vm.funcoesCargos = res.data;
                    vm.participacoesSocietarias = vm.participacoesSocietarias.map(function (participacao) {
                        var funcaoCargo = vm.funcoesCargos.filter(function (funcaoCargo) {
                            return funcaoCargo.valor == participacao.funcaoCargo;
                        })[0];
                        participacao.descricaoFuncaoCargo = funcaoCargo.descricao;
                        return participacao;
                    });
                });
        }

        function salvarParticipacaoSocietaria(participacaoSocietaria) {
            var participacao = angular.copy(participacaoSocietaria);

            if (validarCNPJ(participacao.cnpj)) {
                var notificacao = dialogs.notify('Atenção', 'Esse CNPJ já foi inserido.');
                notificacao
                    .result
                    .then(function () {
                        focus('participacaoSocietariaCNPJ');
                    });

                return false;
            }

            if (participacaoSocietaria.modoEdicao) {
                atualizarListaParticipacaoSocietaria(participacao)
            } else {
                vm.participacoesSocietarias.push(participacao);
            }

            vm.participacaoSocietaria = {};
            vm.mostrarParticipacaoSocietariaForm = false;

            return true;
        }

        function atualizarListaParticipacaoSocietaria(participacao) {
            vm.participacoesSocietarias = vm.participacoesSocietarias.map(function (item) {
                if (item.modoEdicao) {
                    participacao.modoEdicao = false;
                    return participacao;
                }
                return item;
            });
        }

        function cancelarParticipacaoSocietaria() {
            vm.participacaoSocietaria = {};

            vm.participacoesSocietarias = vm.participacoesSocietarias.map(function (p) {
                p.modoEdicao = false;

                return p;
            });

            vm.mostrarParticipacaoSocietariaForm = false;
        }

        function editarParticipacaoSocietaria(participacaoSocietaria) {
            vm.mostrarParticipacaoSocietariaForm = true;
            vm.participacaoSocietaria = angular.copy(participacaoSocietaria);
        }

        function novoParticipacaoSocietaria() {
            vm.mostrarParticipacaoSocietariaForm = true;
            focus('participacaoSocietariaCNPJ');
        }

        function removerParticipacaoSocietaria(index) {
            var confirmacao = dialogs.confirm('Confirmar', 'Confirma a remoção da participação societária?');
            confirmacao
                .result
                .then(function (btn) {
                    vm.participacoesSocietarias = vm.participacoesSocietarias.filter(function (participacao) {
                        return !participacao.modoExclusao;
                    });
                })
                .catch(function () {
                    vm.participacoesSocietarias = vm.participacoesSocietarias.map(function (participacao) {
                        participacao.modoExclusao = false;
                        return participacao;
                    });
                });
        }

        function validarCNPJ(cnpj) {
            return vm.participacoesSocietarias.some(function (item) {
                return !item.modoEdicao && item.cnpj == cnpj;
            });
        }
    }
}());
