(function () {
    'use strict';

    angular
        .module('app')
        .factory('participacaoSocietaria', participacaoSocietaria)

    /** @ngInject */
    function participacaoSocietaria($http, HOST) {

        return {
            funcoes: funcoes
        }

        function funcoes() {
            return $http.get(urlBase() + '/funcoes', { cache: true });
        }

        function urlBase() {
            return HOST.pessoa + '/participacao-societaria';
        }
    }

}());