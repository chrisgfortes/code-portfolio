(function () {
    'use strict';

    angular
        .module('app')
        .service('dadosProfissionaisConvert', dadosProfissionaisConvert)

    /** @ngInject */
    function dadosProfissionaisConvert() {

        this.post = post;

        function post(dadosProfissionais) {
            var _dadosProfissionais = angular.copy(dadosProfissionais);

            if (!!_dadosProfissionais.cdOrgaoResponsavel) {
                _dadosProfissionais.cdOrgaoResponsavel = _dadosProfissionais.cdOrgaoResponsavel.id;
            }

            if (_dadosProfissionais.especialidade) {
                _dadosProfissionais.cdEspecialidade = _dadosProfissionais.especialidade.id;
                delete _dadosProfissionais.especialidade;
            }

            if (_dadosProfissionais.profissao) {
                _dadosProfissionais.cdProfissao = _dadosProfissionais.profissao.id;
                delete _dadosProfissionais.profissao;
            }

            if (_dadosProfissionais.inicioProfissional === true) {
                delete _dadosProfissionais.inicioProfissional;
            }

            return _dadosProfissionais;
        }
    }

}());
