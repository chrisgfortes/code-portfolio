(function () {
    'use strict';

    dadosProficionaisController.$inject = ['cepFactory', 'moment', 'dadosProfissional'];

    angular
        .module('app')
        .component('dadosProfissionais', {
            bindings: {
                profissional: '=',
                nomeComponente: '@',
                readonly: '<'
            },
            controller: dadosProficionaisController,
            templateUrl: './components/dados-profissionais/dados-profissionais.html'
        });


    function dadosProficionaisController(cepFactory, moment, dadosProfissional) {
        var vm = this;

        vm.calcularTempoDeProfissao = calcularTempoDeProfissao;

        vm.$onInit = init;

        function init() {
            vm.profissional = vm.profissional || {};
            vm.maxData = moment();

            dadosProfissional
                .especialidades()
                .then(function (res) {
                    vm.especialidades = res.data;
                    if (!!vm.profissional.cdEspecialidade) {
                        vm.profissional.especialidade = vm.especialidades.filter(function (especialidade) {
                            return especialidade.id == vm.profissional.cdEspecialidade;
                        })[0];
                    }
                });

            cepFactory
                .estados()
                .then(function (res) {
                    vm.estados = res.data;
                });

            dadosProfissional
                .profissoes()
                .then(function (res) {
                    vm.profissoes = res.data;
                    if (!!vm.profissional.cdProfissao) {
                        vm.profissional.profissao = vm.profissoes.filter(function (profissao) {
                            return profissao.id == vm.profissional.cdProfissao;
                        })[0];
                    }
                });

            dadosProfissional
                .grauInstrucao()
                .then(function (res) {
                    vm.instrucoes = res.data;
                });

            dadosProfissional
                .orgaosprofissionais()
                .then(function (res) {
                    vm.orgaoResponsavel = res.data;
                    if (!!vm.profissional.cdOrgaoResponsavel) {
                        vm.profissional.cdOrgaoResponsavel = vm.orgaoResponsavel.filter(function (orgao) {
                            return orgao.id == vm.profissional.cdOrgaoResponsavel;
                        })[0];
                    }
                });

            if (!!vm.profissional.inicioProfissional) {
                vm.profissional.inicioProfissional = moment(vm.profissional.inicioProfissional);
                calcularTempoDeProfissao(vm.profissional.inicioProfissional);
            }
        }
        vm.validarData = function (data) {
            if (!validar(data)) {
                vm.profissional.inicioProfissional = null;
            }
        }

        function calcularTempoDeProfissao(data) {

            if (!validar(data)) {
                vm.profissional.inicioProfissional = null;
                vm.profissional.mesesProfissao = undefined;
                return;
            }

            var dataConvertida = moment(data, 'DD/MM/YYYY').toDate();

            vm.profissional.mesesProfissao = moment().diff(dataConvertida, 'months');
        };


        function validar(data) {
            return moment(data, 'DD/MM/YYYY', true).isValid();
        }
    }
}());
