(function () {
    'use strict';

    angular
        .module('app')
        .factory('dadosProfissional', dadosProfissional)

    /** @ngInject */
    function dadosProfissional($http, HOST) {

        return {
            especialidades: especialidades,
            profissoes: profissoes,
            tipoVinculo: tipoVinculo,
            grauInstrucao: grauInstrucao,
            orgaosprofissionais: orgaosprofissionais
        }

        function especialidades() {
            return $http.get(HOST.sau + '/especialidades', {
                cache: true
            });
        }

        function profissoes() {
            return $http.get(HOST.sau + '/profissoes', { cache: true });
        }

        function tipoVinculo() {
            return $http.get(urlPessoa() + '/analise-cadastral/tipos-vinculo', { cache: true });
        }

        function grauInstrucao() {
            return $http.get(urlPessoa() + '/dados-profissionais/graus-instrucao', { cache: true });
        }

        function orgaosprofissionais() {
            return $http.get(urlSAU() + '/orgaosprofissionais', { cache: true });
        }

        function urlPessoa() {
            return HOST.pessoa;
        }

        function urlSAU() {
            return HOST.sau + '/pessoa/fisica';
        }
    }

}());
