(function () {
    'use strict';

    dadosAgendamentoIntegralizacoesController.$inject = [
        '$rootScope',
        'agendamento',
        'moment',
        'associado',
        '$routeParams'
    ];

    angular
        .module('app')
        .component('dadosAgendamentoIntegralizacoes', {
            bindings: {
                agendamentoIntegralizacoes: '=',
                nomeComponente: '@',
                readonly: '<'
            },
            controller: dadosAgendamentoIntegralizacoesController,
            templateUrl: './components/dados-agendamento-integralizacoes/dados-agendamento-integralizacoes.html'
        });

    function dadosAgendamentoIntegralizacoesController($rootScope, agendamento, moment, associado, $routeParams) {
        var vm = this;

        vm.showCapitalIntegralizar = false;
        vm.showIndicadores = false;

        vm.settedTypeVenc = false;

        vm.$onInit = _onInit;
        vm.setValorIndicador = _setValorIndicador;
        vm.buscarConta = _buscarConta;
        vm.setTypeVenc = _setTypeVenc;
        vm.setVerificaSaldo = _setVerificaSaldo;
        vm.checkValorEntrada = _checkValorEntrada;
        vm.calcularValores = _calcularValores;
        vm.calcularQtdParcelas = _calcQtdParcelas;
        vm.validarDataEntrada = validarDataEntrada;
        vm.dataMinima = moment().add(1,'days');

        function _onInit() {
            vm.maxData = moment();
            vm.diaPrimeiroVenc = true;
            vm.agendamentoIntegralizacoes = {
                renovacaoAutomatica: 'false'
            }

            agendamento
                .formasRecebimento()
                .then(function (res) {
                    vm.formasRecebimento = res.data;
                });

            agendamento
                .indicadores()
                .then(function (res) {
                    vm.indicadores = res.data;
                });

            agendamento
                .tiposAgendamento()
                .then(function (res) {
                    vm.tiposAgendamento = res.data;
                    vm.agendamentoIntegralizacoes.tipoAgendamento = 'F';
                });

            agendamento
                .tiposLancamento()
                .then(function (res) {
                    vm.tiposLancamento = res.data;
                });

            agendamento
                .valorIndicador()
                .then(function (res) {
                    vm.valorIndicador = res.data;
                });

            associado
                .buscarMatricula($routeParams.cpf)
                .then(function (res) {
                    vm.agendamentoIntegralizacoes.matricula = res.matricula.matricula;
                });
        }

        function _setValorIndicador(){
            if (vm.agendamentoIntegralizacoes.valorIndicador){
                var option = JSON.parse(vm.agendamentoIntegralizacoes.valorIndicador);

                if (option.valor == 'V') {
                    vm.showCapitalIntegralizar = true;
                    vm.showIndicadores = false;
                } else if (option.valor == 'I') {
                    vm.showIndicadores = true;
                    vm.showCapitalIntegralizar = false;
                }
            }else{
                vm.showIndicadores = false;
                vm.showCapitalIntegralizar = false;
            }
        }

        function _calcularValores() {

            var valCapital = angular.copy(vm.agendamentoIntegralizacoes.capitalAIntegralizar);
            var valEntrada = angular.copy(vm.agendamentoIntegralizacoes.valorEntrada);
            var qtdParcela = angular.copy(vm.agendamentoIntegralizacoes.quantidadeParcelas);

            if (!!valCapital && !isNaN(valEntrada) && !!qtdParcela){
                var valorParcelas = (valCapital-valEntrada) / qtdParcela;

                vm.agendamentoIntegralizacoes.valorParcela = valorParcelas;
            }
            return;

        }

        function _calcQtdParcelas() {

            var valCapital = angular.copy(vm.agendamentoIntegralizacoes.capitalAIntegralizar);
            var valEntrada = angular.copy(vm.agendamentoIntegralizacoes.valorEntrada);
            var valParcela = angular.copy(vm.agendamentoIntegralizacoes.valorParcela);

            if (!!valCapital && !isNaN(valEntrada) && !!valParcela){
                var total = (valCapital - valEntrada);

                if (valParcela > total) {
                    _calcularValores();
                } else{
                    var parcelas = (total / valParcela);
                    var qtdParcelaArredondado = (Number.isInteger(parcelas) ? parcelas : Math.ceil(parcelas));
                    vm.agendamentoIntegralizacoes.quantidadeParcelas = qtdParcelaArredondado;
                }
            }
            return;

        }

        function _buscarConta(model){
            !!model || (vm.nomeCorrentista = '');

            !model ||
            agendamento
                .getDadosConta(model)
                .then(function(res) {
                    var conta = res.data;
                    vm.nomeCorrentista = conta.cooperadoPessoa.nome || 'Correntista sem nome cadastrado.';
                })
                .catch(function(err){
                    vm.agendamentoIntegralizacoes.contaDebito = null;
                    vm.nomeCorrentista = 'Não encontrado.';
                });
        }

        function _setTypeVenc(){
            if (vm.agendamentoIntegralizacoes.hasOwnProperty('tipoAgendamento')){
                var tipoAgendamentoValor = vm.agendamentoIntegralizacoes.tipoAgendamento;

                if (tipoAgendamentoValor){
                    tipoAgendamentoValor = tipoAgendamentoValor;
                }else{
                    tipoAgendamentoValor = '';
                }

                if (tipoAgendamentoValor == 'F'){
                    vm.diaPrimeiroVenc = true;

                    if (vm.agendamentoIntegralizacoes.hasOwnProperty('anoMesPrimeiroVencimento')) {
                        delete vm.agendamentoIntegralizacoes.anoMesPrimeiroVencimento;
                    }

                }else{
                    vm.diaPrimeiroVenc = false;
                }

                if (tipoAgendamentoValor == 'U') {
                    vm.mesAnoVenc = true;

                    if(vm.agendamentoIntegralizacoes.hasOwnProperty('diaPrimeiroVencimento')){
                        delete vm.agendamentoIntegralizacoes.diaPrimeiroVencimento;
                    }
                }else{
                    vm.mesAnoVenc = false;
                }
            }else{
                vm.diaPrimeiroVenc = true;
                vm.mesAnoVenc = false;
                vm.agendamentoIntegralizacoes.diaPrimeiroVencimento = '';
            }
        }

        function _setVerificaSaldo(){
            var validate = false;

            if (vm.agendamentoIntegralizacoes.formaRecebimento){
                var model = angular.copy(vm.agendamentoIntegralizacoes.formaRecebimento);
                validate = (model == 'S');
            }

            vm.showVerificaSaldo = validate;
            vm.agendamentoIntegralizacoes.verificaSaldo = validate;
        }

        function _checkValorEntrada(){
            if(vm.agendamentoIntegralizacoes.valorEntrada > vm.agendamentoIntegralizacoes.capitalAIntegralizar){
                vm.agendamentoIntegralizacoes.valorEntrada = '';
                vm.agendamentoIntegralizacoes.quantidadeParcelas = '';
                vm.agendamentoIntegralizacoes.valorParcela = '';

                $rootScope.$emit('mostrar-mensagem', {
                    titulo: 'Agendamento Integralizações',
                    descricao: 'O valor de entrada deve ser menor que o de capitalização'
                });
            }
        }

        function validarDataEntrada(data) {
            if (!validar(data)) {
                vm.agendamentoIntegralizacoes.dataEntrada = null;
            }
        }

        function validar(data) {
            return moment(data, 'DD/MM/YYYY', true).isValid();
        }

        Number.isInteger = Number.isInteger || function (value) {
            return typeof value === 'number' &&
                isFinite(value) &&
                Math.floor(value) === value;
        };
    }
}());
