(function () {
    'use strict';

    angular
        .module('app')
        .factory('agendamento', agendamento);

    function agendamento($http, HOST) {
        return {
            formasRecebimento: _formasRecebimento,
            indicadores: _indicadores,
            tiposAgendamento: _tiposAgendamento,
            tiposLancamento: _tiposLancamento,
            valorIndicador: _valorIndicador,
            getDadosConta: _getDadosConta
        }

        function _formasRecebimento() {
            return $http.get(HOST.agendamentoCapital + '/formas-recebimento');
        }

        function _indicadores() {
            return $http.get(HOST.agendamentoCapital + '/indicadores');
        }

        function _tiposAgendamento() {
            return $http.get(HOST.agendamentoCapital + '/tipos-agendamento');
        }

        function _tiposLancamento() {
            return $http.get(HOST.agendamentoCapital + '/tipos-lancamento');
        }

        function _valorIndicador() {
            return $http.get(HOST.agendamentoCapital + '/valor-indicador');
        }

        function _getDadosConta(conta) {
            return $http.get(HOST.cadastro + '/contas-correntes/' + conta);
        }
    }
}());