(function(){
    'use strict';

    angular
        .module('app')
        .service('poderesService', poderesService)

    function poderesService(poderes){
        var listaPoderes = poderes;
        
        this.setarPoderesSelecionados = setarPoderesSelecionados;

        function setarPoderesSelecionados(poderes){
            for(var key in listaPoderes) {
                if(poderes.hasOwnProperty(key)) {
                    listaPoderes[key] = listaPoderes[key].map(function(value) {
                        value.selected = poderes[key][value.contrato];
                        return value;
                    });
                }
            }

            return listaPoderes;
        }
    }

}());
