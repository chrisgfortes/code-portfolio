(function () {
    'use strict';

    angular
        .module('app')
        .value('poderes', {
            cheques:[
                {
                    descricao: "Emitir",
                    contrato: "emitir",
                    selected: ""
                },
                {
                    descricao: "Endossar",
                    contrato: "endossar",
                    selected: ""
                },
                {
                    descricao: "Caucionar",
                    contrato: "caucionar",
                    selected: ""
                },
                {
                    descricao: "Avalizar",
                    contrato: "avalizar",
                    selected: ""
                }
            ],
            autorizacoes:[
                {
                    descricao: "Contrato de Empréstimo",
                    contrato: "contratoEmprestimo",
                    selected: ""
                },
                {
                    descricao: "Nota Promissória",
                    contrato: "notaPromissoria",
                    selected: ""
                },
                {
                    descricao: "Débitos",
                    contrato: "debitos",
                    selected: ""
                }
            ],
            outrasAutorizacoes:[
                {
                    descricao: "Assinar Contratos",
                    contrato: "assinarContratos",
                    selected: ""
                },
                {
                    descricao: "Saque",
                    contrato: "saque",
                    selected: ""
                },
                {
                    descricao: "Negociar Contratos",
                    contrato: "negociarContratos",
                    selected: ""
                },
                {
                    descricao: "Transferência",
                    contrato: "transferencia",
                    selected: ""
                },
                {
                    descricao: "Abrir Ou Encerrar Contas",
                    contrato: "abrirEncerrarContas",
                    selected: ""
                },
                {
                    descricao: "Pagamentos",
                    contrato: "pagamentos",
                    selected: ""
                },
                {
                    descricao: "Solicitar Extrato",
                    contrato: "solicitarExtrato",
                    selected: ""
                },
                {
                    descricao: "Entregar Doc. Para Pgtos",
                    contrato: "entregarDocumentoPagamento",
                    selected: ""
                },
                {
                    descricao: "Retirar Cheque Devolvido",
                    contrato: "retirarChequeDevolvido",
                    selected: ""
                }
            ]
        });
}());
