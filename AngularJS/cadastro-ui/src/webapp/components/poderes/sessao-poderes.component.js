(function() {
  'use strict';

  sessaoPoderesController.$inject = [];

  angular
    .module('app')
    .component('sessaoPoderes', {
      transclude: true,
      bindings: {
        poderes: '=',
        readonly: '<'
      },
      controller: sessaoPoderesController,
      templateUrl: './components/poderes/sessao-poderes.html'
    });

  function sessaoPoderesController() {
    var vm = this;

    vm.mostrarPoderesForm = true;
    vm.$onInit = onInit;

    function onInit() {}
  }
}());
