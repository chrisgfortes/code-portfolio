(function() {
  'use strict';

  poderesController.$inject = ['$route', 'poderes', 'poderesService', '$rootScope', 'cartaoAutografoFactory'];

  angular
    .module('app')
    .component('dadosPoderes', {
        bindings: {
            poderes: '=',
            host:'='
        },
        controller: poderesController,
        templateUrl: './components/poderes/dados-poderes.template.html'
    });

    function poderesController($route, poderes, poderesService, $rootScope, cartaoAutografoFactory) {
        var vm = this;

        vm.listaPoderes = poderes;
        vm.allSelected = '';
        vm.valoresPoderes = [
            'INDIVIDUAL',
            'CONJUNTA',
            'PREJUDICADO'
        ];

        vm.validar = validar;
        vm.selectAll = selectAll;
        vm.deselect = deselect;
        vm.$onInit = onInit;

        function onInit() {
            vm.factory = cartaoAutografoFactory(vm.host);

            vm.factory.tipoPoder().then(function(res) {
                vm.opcoes = res.data;
            });

            vm.listaPoderes = poderesService.setarPoderesSelecionados(vm.poderes);
            vm.validar();
        }

        $rootScope.$on('atualizar-lista-poderes', function (event, poderes) {
            vm.listaPoderes = poderesService.setarPoderesSelecionados(poderes);
            vm.allSelected = vm.marcarTodos = '';
        });

        function validar() {
            var cheques = clean(vm.poderes.cheques);
            vm.isRequired = !!cheques && !angular.equals({}, cheques);
        }

        function deselect($event, secao, poder, indexPoder) {
            var eventTarget = $event.target.value;

            var isSelected = eventTarget != vm.listaPoderes[secao][indexPoder]['selected'];
            var valor = isSelected ? eventTarget : '';

            !valor ? delete vm.poderes[secao][poder] : vm.poderes[secao][poder] = valor;
            vm.listaPoderes[secao][indexPoder]['selected'] = valor;

            secao != 'cheques' || vm.validar();
        }

        function selectAll(valor){

            valor = vm.allSelected == valor ? '' : valor;
            vm.allSelected = vm.marcarTodos = valor;

            montarObjetoPoderes('cheques', valor);
            vm.validar();
            montarObjetoPoderes('autorizacoes', valor);
            montarObjetoPoderes('outrasAutorizacoes', valor);

            vm.listaPoderes = poderesService.setarPoderesSelecionados(vm.poderes);
            valor != '' || (vm.poderes = {});

        }

        function montarObjetoPoderes(atributo, valor) {
            var obj = {};
            vm.listaPoderes[atributo].forEach(marcar(obj, valor));
            vm.poderes[atributo] = obj;
        }

        function marcar(obj, valor) {
            return function(item) {
                var chave = item.contrato;
                obj[chave] = valor;
            }
        }

        function clean(obj) {
            return angular.forEach(obj, function(value, key) {
                if (!value) delete obj[key];
            });
        }
    }
}());
