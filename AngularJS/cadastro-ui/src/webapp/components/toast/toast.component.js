(function() {
    'use strict';

    toastController.$inject = ['$route', '$rootScope'];

    angular
        .module('app')
        .component('toastMessage', {
			bindings: {
				showToast: '<',
				title: '@',
				message: '@',
				error: '<',
                warning: '<',
                dismiss: '&'
			},
            controller: toastController,
            templateUrl: './components/toast/toast.html'
        });

    function toastController($route, $rootScope) {
        var vm = this;

        vm.dismissToast = dismissToast;

        function dismissToast () {
            vm.dismiss();
        }
    }
}());
