(function () {
    'use strict';

    angular
        .module('app')
        .component('dadosFiliacao', {
            bindings: {
                filiacao: '=',
                nomeComponente: '@',
                readonly: '<'
            },
            controller: filiacaoController,
            templateUrl: './components/filiacao/dados-filiacao.html'
        });

    function filiacaoController() {
        var vm = this;

        vm.desabilitarNomeDoPai = desabilitarNomeDoPai;
        vm.desabilitarNomeDaMae = desabilitarNomeDaMae;

        vm.$onInit = init;

        function init() {
            vm.filiacao = vm.filiacao || { nomePaiNaoDeclarado: false, nomeMaeNaoDeclarado: false };
        }

        function desabilitarNomeDoPai() {
            if (vm.filiacao.nomePaiNaoDeclarado)
                vm.filiacao.nomePai = "";
        }

        function desabilitarNomeDaMae() {
            if (vm.filiacao.nomeMaeNaoDeclarado)
                vm.filiacao.nomeMae = "";
        }
    }
}());
