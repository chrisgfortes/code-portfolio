(function () {
    'use strict';

    listaCartaoAutografoController.inject = ['$routeParams', 'cartaoAutografoFactory']

    angular
        .module('app')
        .component('listaCartaoAutografo', {
            bindings: {
                cartoesAutografo: '=',
                onNovo: '&',
                onEditar: '&',
                onRemover: '&',
                esconderBotaoNovo: '<',
                readonly: '<',
                host: '<'
            },
            controller: listaCartaoAutografoController,
            templateUrl: './components/cartao-autografo/lista-cartao-autografo.html'
        });

    function listaCartaoAutografoController($routeParams, cartaoAutografoFactory) {
        var vm = this;

        vm.$onInit = onInit;
        vm.novo = novo;
        vm.editar = editar;
        vm.remover = remover;

        function onInit() {
            vm.factory = cartaoAutografoFactory(vm.host);
            vm.cartoesAutografo = buildDescricao(vm.cartoesAutografo || []);
        }

        function novo() {
            vm.onNovo()
        }

        function buildDescricao(cartoes) {
            return cartoes.map(function (cartao) {
                if (!cartao.pessoa.nome) {
                    cartao.pessoa.nome = "(não informado)";
                }

                cartao.pessoa.descricao = cartao.pessoa.tipo.descricao + " - " + cartao.pessoa.nome;

                return cartao;
            });
        }

        function editar(_cartaoAutografo) {
            vm.cartoesAutografo = vm.cartoesAutografo.map(function (item) {
                item.modoEdicao = false;
                return item;
            });

            _cartaoAutografo.modoEdicao = true;

            vm.factory
                .buscarImagemAssinatura($routeParams.cpf, _cartaoAutografo)
                .then(function (res) {
                    _cartaoAutografo.assinatura = res.data;
                })
                .finally(function () {
                    vm.onEditar({ cartaoAutografo: angular.copy(_cartaoAutografo) });
                })
        }

        function remover(cartaoAutografo) {
            cartaoAutografo.modoExclusao = true;

            vm.onRemover({
                cartaoAutografo: angular.copy(cartaoAutografo)
            })
        }
    }
}());
