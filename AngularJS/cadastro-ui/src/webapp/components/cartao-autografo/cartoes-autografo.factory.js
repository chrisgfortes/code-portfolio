(function() {
    'use strict';

    angular
        .module('app')
        .factory('cartaoAutografoFactory', cartaoAutografo)

    /** @ngInject */
    function cartaoAutografo($http, HOST) {

        return function (urlBase) {
            return {
              buscar: buscar,
              salvar: salvar,
              salvarImagemAssinatura: salvarImagemAssinatura,
              tipoPoder: tipoPoder,
              buscarPessoa: buscarPessoa,
              excluir: excluir,
              buscarImagemAssinatura: buscarImagemAssinatura
            }

            function buscar(cpf) {
                var url = urlBuild(cpf);
                return $http.get(url);
            }

            function buscarImagemAssinatura(cpf, cartaoAutografo) {
              var url = urlBuildComCartaoAutografo(cpf, cartaoAutografo.pessoa.cpfCnpj);
              return $http.get(url + '/imagem');
            }

            function salvar(cpf, cartaoAutografo) {
                var url = urlBuild(cpf);
                return $http.put(url, cartaoAutografo);
            }

            function salvarImagemAssinatura(cpf, cartaoAutografo, file) {
              var url = urlBuildComCartaoAutografo(cpf, cartaoAutografo.pessoa.cpfCnpj);
              return $http.post(url + '/imagem', file);
            }

            function buscarPessoa(cpf) {
              var url = urlBuild(cpf);
              return $http.get(url + '/pessoa');
            }

            function excluir(cpf, cartaoAutografo) {
              var url = urlBuildComCartaoAutografo(cpf, cartaoAutografo.pessoa.cpfCnpj);
              return $http.delete(url);
            }

            function tipoPoder() {
              return $http.get(HOST.pessoa + '/fisica/cartao-autografo/permissao-poder');
            }

            function urlBuildComCartaoAutografo(cpf, cpfCnpj) {
              return urlBase + cpf + '/rascunho/cartaoautografo/' + cpfCnpj;
            }

            function urlBuild(cpf) {
              return urlBase + cpf + '/rascunho/cartaoautografo';
            }
        }
    }

}());
