(function () {
    'use strict';

    sessaoCartaoAutografoController.$inject = ['$rootScope', '$routeParams', 'dialogs', 'focus', 'Upload', 'cartaoAutografoFactory', 'HOST'];

    angular
        .module('app')
        .component('sessaoCartaoAutografo', {
            bindings: {
                cartoesAutografo: '=',
                readonly: '<',
                onNotificacao: '&',
                host: '<',
                mostraSessao: '<'
            },
            controller: sessaoCartaoAutografoController,
            templateUrl: './components/cartao-autografo/sessao-cartao-autografo.html'
        });

    function sessaoCartaoAutografoController($rootScope, $routeParams, dialogs, focus, Upload, cartaoAutografoFactory, HOST) {
        var vm = this;

        vm.salvar = salvar;
        vm.cancelar = cancelar;
        vm.editar = editar;
        vm.novo = novo;
        vm.remover = remover;
        vm.mostrarCartaoAutografoForm = false;
        vm.$onInit = onInit;
        vm.cpf = $routeParams.cpf;

        function onInit() {
            vm.factory = cartaoAutografoFactory(vm.host);
            vm.cartaoAutografo = {
                poderes: {}
            };
        }

        function salvar(cartaoAutografo) {
            var _cartaoAutografo = angular.copy(cartaoAutografo);

            if (angular.equals({}, _cartaoAutografo.poderes)) delete _cartaoAutografo.poderes;

            if (!_cartaoAutografo.modoEdicao && validar(_cartaoAutografo)) {
                dialogs.notify("Atenção", "Essa pessoa já foi adicionada.");
                return;
            }

            salvarCartaoAutografo(_cartaoAutografo);
        }

        function validar(_cartaoAutografo) {
            return vm.cartoesAutografo.some(function (cartao) {
                return cartao.pessoa.cpfCnpj == _cartaoAutografo.pessoa.cpfCnpj;
            });
        }

        function salvarCartaoAutografo(dados) {
            vm.factory
                .salvar(vm.cpf, dados)
                .then(atualizaCartoes(dados))
                .then(salvarImage(dados))
                .then(notificarSucesso)
                .catch(notificarErro);
        }

        function atualizaCartoes(dados) {
            return function (res) {
                if (dados.modoEdicao) {
                    atualizarLista(dados);
                } else {
                    vm.cartoesAutografo.push(dados);
                }

                return res;
            }
        }

        function salvarImage(dados) {
            return function (res) {
                if (!(dados.assinatura instanceof Blob)) return res;

                dados.assinatura.name = dados.filename;

                return Upload.upload({
                    url: vm.host + vm.cpf + '/rascunho/cartaoautografo/' + dados.pessoa.cpfCnpj + '/imagem',
                    data: {
                        filename: dados.assinatura.filename,
                        file: dados.assinatura
                    }
                }).then(function() {}, function (err) {
                    vm.onNotificacao({
                        status: false,
                        mensagem: 'Erro ao fazer upload da assinatura.'
                    });
                });

            }
        }

        function notificarSucesso() {
            vm.cartaoAutografo = {poderes:{}};
            vm.mostrarCartaoAutografoForm = false;

            vm.onNotificacao({
                status: true
            });
        }

        function notificarErro(erro) {
            vm.onNotificacao({
                status: false,
                mensagem: erro.data.body
            });
        }

        function atualizarLista(cartaoAutografo) {
            vm.cartoesAutografo = vm.cartoesAutografo.map(function (item) {
                if (item.modoEdicao) {
                    cartaoAutografo.modoEdicao = false;
                    return cartaoAutografo;
                }
                return item;
            });
        }

        function cancelar() {
            vm.mostrarCartaoAutografoForm = false;

            vm.cartoesAutografo = vm.cartoesAutografo.map(function (item) {
                item.modoEdicao = false;

                return item;
            });

            vm.cartaoAutografo = {
                poderes: {}
            };
        }

        function editar(cartaoAutografo) {
            vm.mostrarCartaoAutografoForm = true;
            vm.cartaoAutografo = angular.copy(cartaoAutografo);
            if (!vm.cartaoAutografo.poderes) vm.cartaoAutografo.poderes = {};

            $rootScope.$broadcast('atualizar-lista-poderes', vm.cartaoAutografo.poderes);
        }

        function novo() {
            vm.mostrarCartaoAutografoForm = true;

            focus('nomeCartaoAutografo');
        }

        function remover(_cartaoAutografo) {
            var confirmacao = dialogs.confirm('Confirmar', 'Confirma a remoção do cartaoAutografo?');

            confirmacao
                .result
                .then(function (btn) {
                    vm.factory.excluir(vm.cpf, _cartaoAutografo).then(function (res) {
                        vm.cartoesAutografo = vm.cartoesAutografo.filter(function (item) {
                            return !item.modoExclusao;
                        });
                    });
                }).catch(function () {
                    vm.cartoesAutografo = vm.cartoesAutografo.map(function (item) {
                        item.modoExclusao = false;
                        return item;
                    });
                });
        }
    }
}());
