(function () {
    'use strict';

    cartaoAutografoController.inject = ['$routeParams', 'cartaoAutografoFactory']

    angular
        .module('app')
        .component('dadosCartaoAutografo', {
            bindings: {
                cartaoAutografo: '<',
                onCancelar: '&',
                onSalvar: '&',
                host: '<'
            },
            controller: cartaoAutografoController,
            templateUrl: './components/cartao-autografo/dados-cartao-autografo.html'
        });

    function cartaoAutografoController($rootScope, $routeParams, cartaoAutografoFactory) {
        var vm = this;

        vm.$onInit = onInit;
        vm.cancelar = cancelar;
        vm.salvar = salvar;
        vm.cpf = $routeParams.cpf;

        function onInit() {
            vm.factory = cartaoAutografoFactory(vm.host);

            if (!!vm.cartaoAutografo.pessoa) {
                vm.factory.buscarImagemAssinatura(vm.cpf, vm.cartaoAutografo)
                    .then(function (res) {
                        vm.cartaoAutografo.assinatura = res.data;
                    });
            }

            vm.factory.buscarPessoa(vm.cpf).then(function (res) {
                vm.pessoas = buildDescricao(res.data);
            });

        }

        function buildDescricao(pessoas) {
            return pessoas.map(function (pessoa) {
                if (!!pessoa.nome) {
                    pessoa.descricao = pessoa.tipo.descricao + ' - ' + pessoa.nome;
                } else {
                    pessoa.nome = '(não informado)';
                    pessoa.descricao = pessoa.tipo.descricao + ' - ' + pessoa.nome;
                }

                return pessoa;
            });
        }

        function cancelar() {
            vm.onCancelar();
        }

        function salvar(cartaoAutografo) {
            var _cartaoAutografo = angular.copy(cartaoAutografo);

            if (!!_cartaoAutografo.assinatura)
                _cartaoAutografo.filename = cartaoAutografo.assinatura.$ngfName;

            vm.onSalvar({
                cartaoAutografo: _cartaoAutografo
            });
        }
    }
}());
