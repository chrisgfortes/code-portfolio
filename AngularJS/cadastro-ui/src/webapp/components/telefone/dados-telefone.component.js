(function () {
    'use strict';

    participacaoSocietariaController.$inject = ['contato'];

    angular
        .module('app')
        .component('dadosTelefone', {
            bindings: {
                telefone: '=',
                esconder: '<',
                nome: '@'
            },
            controller: participacaoSocietariaController,
            templateUrl: './components/telefone/dados-telefone.html'
        });

    function participacaoSocietariaController(contato) {
        var vm = this;

        vm.$onInit = onInit;

        function onInit() {
            vm.nome = vm.nome || "";

            contato
                .referenciaContatos()
                .then(function (res) {
                    vm.referenciasTelefone = res.data;
                });
        }
    }
}());
