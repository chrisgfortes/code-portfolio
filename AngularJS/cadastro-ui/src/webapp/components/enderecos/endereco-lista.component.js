(function () {
    'use strict';

    enderecosController.$inject = ['dialogs'];

    angular
        .module('app')
        .component('enderecoLista', {
            bindings: {
                enderecos: '<',
                readonly: '<',
                onEditar: '&',
                onNovo: '&',
                onRemover: '&',
                esconderBotaoNovo: '<'
            },
            controller: enderecosController,
            templateUrl: './components/enderecos/endereco-lista.template.html'
        });

    function enderecosController(dialogs) {
        var vm = this;

        vm.$onInit = init;

        vm.remover = remover;
        vm.editar = editar;

        vm.novo = function () {
            vm.onNovo();
        }

        //////////////////////////////////////////////////

        function init() {
            vm.esconderBotaoNovo = false;
            vm.enderecos = vm.enderecos || [];
        };

        function remover(endereco) {
            endereco.modoExclusao = true;

            vm.onRemover({ endereco: angular.copy(endereco) });
        }

        function editar(endereco) {
            vm.enderecos.map(function (item) {
                item.modoEdicao = false;

                return item;
            });

            endereco.modoEdicao = true;

            vm.onEditar({ endereco: angular.copy(endereco) });
        }
    }
}());
