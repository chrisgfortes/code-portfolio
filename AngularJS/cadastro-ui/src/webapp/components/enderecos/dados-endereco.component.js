(function () {
    'use strict';

    enderecoController.$inject = ['endereco', 'cepFactory', 'moment', '$rootScope', 'focus', '$scope'];

    angular
        .module('app')
        .component('dadosEndereco', {
            bindings: {
                endereco: '<',
                ocultarCampos: '<',
                nomeComponente: '@',
                readonly: '<',
                onSalvar: '&',
                onCancelar: '&'
            },
            controller: enderecoController,
            templateUrl: './components/enderecos/dados-endereco.html'
        });

    function enderecoController(endereco, cepFactory, moment, $rootScope, focus, $scope) {
        var vm = this;
        vm.ocultar = {};
        vm.cidades = [];
        vm.maxData = moment();

        vm.$onInit = init;
        vm.cancelar = cancelar;
        vm.salvar = salvar;
        vm.situacaoEnderecoAlterado = situacaoEnderecoAlterado;
        vm.isNumeroRequired = isNumeroRequired;
        vm.semNumero = semNumero;
        vm.cityAutocomplete = cityAutocomplete;
        vm.buscarEnderecoPeloCEP = buscarEnderecoPeloCEP;
        vm.calcularTempoDeResidencia = calcularTempoDeResidencia;
        vm.validarResideDesde = validarResideDesde;
        vm.limparCamposPrincipal = limparCamposPrincipal;

        vm.endereco = {
            valorAluguel: 0.00
        };

        /////////////////////////////////////////////////////

        function init() {
            carregarListas();
            ocultarCampos();

            vm.isCepRequired = true;
            vm.numeroRequired = true;
            vm.mesesDeResidencia = '';

            situacaoEnderecoAlterado(vm.endereco);

            vm.endereco ? enderecoExiste() : setarEnderecoPadrao();
        }

        function limparCamposPrincipal() {
            vm.deveHabilitarValorAluguel = false;
            vm.endereco.resideDesde = null;
            vm.endereco.situacaoEndereco = null;
            vm.endereco.valorAluguel = null;
        }

        function enderecoExiste() {
            if (!!vm.endereco.estado) {
                cityAutocomplete(vm.endereco.estado);
            }
            if (!!vm.endereco.resideDesde) {
                vm.endereco.resideDesde = moment(vm.endereco.resideDesde);
                calcularTempoDeResidencia(vm.endereco.resideDesde);
            }
        }

        function setarEnderecoPadrao() {
            vm.endereco = {
                tipoEndereco: 'RESIDENCIAL',
                principal: true,
                empostamento: true
            }
        }

        function salvar(endereco) {
            var cidade = vm.cidades.filter(function (city) {
                return city.codigoCidade == endereco.codigoCidade;
            })[0];
            endereco.cidade = cidade.nomeCidade;

            vm.onSalvar({ endereco: angular.copy(endereco) });

            vm.endereco = {};
        }

        function cancelar() {
            vm.onCancelar();
        }

        function isNumeroRequired () {
            return !vm.endereco.enderecoSemNumero;
        }

        function semNumero () {
            vm.endereco.numero = null;
            vm.numeroRequired = !vm.numeroRequired;
        }

        vm.$onChanges = function (a) {
            if (vm.endereco) {
                init();
            }
        }

        function cityAutocomplete (uf) {
            carregarCidades(uf);
        }

        function buscarEnderecoPeloCEP (cep) {
            if (cep && cep.length == 8) {
                vm.cepIconLoading = true;

                cepFactory
                    .cep(cep)
                    .then(extrairEndereco);
            }
        };

        function extrairEndereco(response) {
            var data = response.data;
            if (data.tipoLogradouro && data.logradouro) {
                vm.endereco.logradouro = data.tipoLogradouro + ' ' + data.logradouro;
            }

            vm.endereco.codigoCidade = data.cidade.codigoCidade;
            vm.endereco.estado = data.uf.toUpperCase();
            vm.endereco.bairro = data.bairro;

            carregarCidades(data.uf);
        }

        function ocultarCampos() {
            var campos = vm.ocultarCampos || [];

            if (campos.length == 0) return;

            campos.forEach(function (campo) {
                vm.ocultar[campo] = true;
            });
        }

        function carregarCidades(uf) {
            if (!uf) return;

            cepFactory
                .cidades(uf)
                .then(function (res) {
                    vm.cidades = res.data;
                });
        }

        function carregarListas() {
            cepFactory
                .estados()
                .then(function (res) {
                    vm.estados = res.data;
                });

            endereco
                .tipos()
                .then(function (res) {
                    vm.tiposEndereco = res.data;
                });

            endereco
                .situacoes()
                .then(function (res) {
                    vm.situacoesEndereco = res.data;
                });
        }

        function calcularTempoDeResidencia(data) {
            vm.mesesDeResidencia = moment().diff(data, 'months');
        }

        function validarResideDesde (dataResideDesde) {
            if (!validar(dataResideDesde)) {
                vm.mesesDeResidencia = undefined;
            } else {
                calcularTempoDeResidencia(dataResideDesde);
            }
        }

        function validar(data) {
            return moment(data, 'DD/MM/YYYY', true).isValid();
        }

        function situacaoEnderecoAlterado(endereco) {
            if (!endereco || !endereco.situacaoEndereco) {
                vm.deveHabilitarValorAluguel = false;
                return;
            }

            var situacoesHabilitarValorAluguel = ['ALUGADA', 'FINANCIADA'];

            vm.deveHabilitarValorAluguel = situacoesHabilitarValorAluguel.some(function (item) {
                return item == endereco.situacaoEndereco;
            });
        }

    }
}());
