(function () {
    'use strict';

    sessaoEnderecoController.$inject = ['dialogs', 'focus', 'cepFactory'];

    angular
        .module('app')
        .component('sessaoEndereco', {
            transclude: true,
            bindings: {
                enderecos: '=',
                readonly: '<',
                mostraSessao: '<'
            },
            controller: sessaoEnderecoController,
            templateUrl: './components/enderecos/sessao-endereco.html'
        });

    function sessaoEnderecoController(dialogs, focus, cepFactory) {
        var vm = this;

        vm.$onInit = onInit;
        vm.salvar = salvar;
        vm.cancelar = cancelar;
        vm.novo = novo;
        vm.remover = remover;
        vm.editar = editar;

        function onInit() {
            vm.mostrarEnderecoForm = false;
            vm.enderecos = vm.enderecos || [];

            vm.enderecos.map(function (endereco) {
                cepFactory
                    .cidades(endereco.estado)
                    .then(function (res) {
                        var cidades = res.data;
                        var cidade = cidades.filter(function (cidade) {
                            return cidade.codigoCidade == endereco.codigoCidade;
                        })[0];
                        endereco.cidade = cidade.nomeCidade;
                        return endereco;
                    });
            });

        }

        function salvar(endereco) {
            var _endereco = angular.copy(endereco);

            if(_endereco.empostamento) {
                limpaEnderecosEmpostamentos();
            }

            if(_endereco.principal) {
                limpaEnderecosPrincipais();
            }

            if (_endereco.modoEdicao) {
                atualizarListaEndereco(_endereco)
            } else {
                vm.enderecos.push(_endereco);
            }

            vm.endereco = {};
            vm.mostrarEnderecoForm = false;

            focus('novoEndereco');
        }

        function atualizarListaEndereco(endereco) {
            vm.enderecos = vm.enderecos.map(function (item) {
                if (item.modoEdicao) {
                    endereco.modoEdicao = false;
                    return endereco;
                }
                return item;
            });
        }

        function cancelar() {
            vm.mostrarEnderecoForm = false;

            vm.enderecos = vm.enderecos.map(function (item) {
                item.modoEdicao = false;

                return item;
            })

            vm.endereco = {};

            focus('novoEndereco');
        }

        function novo() {
            vm.mostrarEnderecoForm = true;

            focus('residencialTipoEndereco');
        }

        function remover(endereco) {
            var confirmacao = dialogs.confirm('Confirmar', "Confirma a remoção do endereço?");

            confirmacao
                .result
                .then(removerDaLista(endereco))
                .catch(desfazerModoExclusao)
                .finally(function () {
                    focus('novoEndereco');
                });
        }

        function removerDaLista(endereco) {
            return function (btn) {
                vm.enderecos = vm.enderecos.filter(modoEclusao);
            }
        }

        function modoEclusao(item) {
            return !item.modoExclusao;
        }

        function desfazerModoExclusao() {
            vm.enderecos = vm.enderecos.filter(desabilitarModoExlucao);
        }

        function desabilitarModoExlucao(item) {
            item.modoExclusao = false

            return item;
        }

        function editar(endereco) {
            vm.mostrarEnderecoForm = true;

            vm.endereco = angular.copy(endereco);
        }

        function limpaEnderecosEmpostamentos () {
            vm.enderecos = vm.enderecos.map(function (item) {
                item.empostamento = false;

                return item;
            });
        }

        function limpaEnderecosPrincipais () {
            vm.enderecos = vm.enderecos.map(function (item) {
                item.principal = false;
                item.resideDesde = null;
                item.situacaoEndereco = null;
                return item;
            });
        }
    }
}());
