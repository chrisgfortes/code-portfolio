(function () {
    'use strict';

    angular
        .module('app')
        .factory('endereco', endereco)

    /** @ngInject */
    function endereco($http, HOST) {

        return {
            tipos: tipos,
            situacoes: situacoes
        }

        function tipos() {
            return $http.get(urlBase() + '/tipos', { cache: true });
        }

        function situacoes() {
            return $http.get(urlBase() + '/situacoes-endereco', { cache: true });
        }

        function urlBase() {
            return HOST.pessoa + '/endereco';
        }
    }
}());