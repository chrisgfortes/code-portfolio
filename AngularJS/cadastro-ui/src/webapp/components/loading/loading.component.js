(function () {
    'use strict';

    loadingController.$inject = [];

    angular
        .module('app')
        .component('loading', {
            bindings: {
                enable: '<'
            },
            controller: loadingController,
            templateUrl: './components/loading/loading.template.html'
        });

    function loadingController() {
        var vm = this;
    };
}());
