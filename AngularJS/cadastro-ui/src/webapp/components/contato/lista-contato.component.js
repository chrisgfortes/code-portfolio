(function () {
    'use strict';

    contatosController.$inject = [];

    angular
        .module('app')
        .component('listaContato', {
            bindings: {
                contatos: '=',
                readonly: '<',
                onNovo: '&',
                onRemover: '&',
                onEditar: '&',
                esconderBotaoNovo: '<',
                nome: '@'
            },
            controller: contatosController,
            templateUrl: './components/contato/lista-contato.html'
        });

    function contatosController() {
        var vm = this;

        vm.$onInit = init;
        vm.remover = remover;
        vm.novo = novo;
        vm.editar = editar;

        //////////////////////////////////////////////////

        function init() {
            vm.contatos = vm.contatos || [];
            var telefones = vm.contatos.telefones || [];
            var emails = vm.contatos.emails || [];

            vm.contatos = {
                telefones: telefones,
                emails: emails
            };
        }

        function remover(contato) {
            contato.modoExclusao = true;

            vm.onRemover({ contato: angular.copy(contato) });
        }

        function editar(contato) {
            vm.contatos.emails = vm.contatos.emails.map(alterarModoEdicaoParaFalse);

            vm.contatos.telefones = vm.contatos.telefones.map(alterarModoEdicaoParaFalse);

            contato.modoEdicao = true;
            contato.numero = contato.ddd + contato.numero;

            vm.onEditar({ contato: contato });
        }

        function alterarModoEdicaoParaFalse(c) {
            c.modoEdicao = false;
            return c;
        }

        function novo() {
            vm.contato = {};
            vm.onNovo();
        }
    }
}());
