(function () {
    'use strict';

    contatoController.$inject = ['contato'];

    angular
        .module('app')
        .component('dadosContato', {
            bindings: {
                contato: '<',
                nome: '@',
                onSalvar: '&',
                onCancelar: '&'
            },
            controller: contatoController,
            templateUrl: './components/contato/dados-contato.html'
        });

    function contatoController(contato) {
        var vm = this;

        vm.$onInit = init;
        vm.salvar = salvar;
        vm.cancelar = cancelar;

        function init() {
            if (!!vm.contato) vm.tipoSelecionado = vm.contato.tipoContato;
            
            vm.tipoSelecionado = vm.tipoSelecionado || 'telefone';

            contato
                .tiposEmail()
                .then(function (res) {
                    vm.tiposEmail = res.data;
                });
        }

        function salvar(contato) {
            delete contato.tipoContato;
            vm.onSalvar({ contato: angular.copy(contato) });

            vm.tipoSelecionado = null;
            vm.contato = {};
        }

        function cancelar() {
            vm.onCancelar();
        }
    }

}());
