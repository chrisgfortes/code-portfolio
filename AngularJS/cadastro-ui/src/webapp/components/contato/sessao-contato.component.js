(function () {
    'use strict';

    sessaoContatoController.$inject = ['dialogs', 'focus'];

    angular
        .module('app')
        .component('sessaoContato', {
            transclude: true,
            bindings: {
                contatos: '=',
                readonly: '<',
                nome: '@'
            },
            controller: sessaoContatoController,
            templateUrl: './components/contato/sessao-contato.html'
        });

    function sessaoContatoController(dialogs, focus) {
        var vm = this;

        vm.$onInit = onInit;
        vm.salvar = salvar;
        vm.cancelar = cancelar;
        vm.novo = novo;
        vm.remover = remover;
        vm.editar = editar;

        function onInit() {

        }

        function salvar(contato) {

            if (contato.modoEdicao) {
                atualizarModoEdicaoListaContatos(contato);
            } else {
                if (contato.hasOwnProperty('tipoTelefone')) {
                    if (!!contato.tipoTelefone) {
                        contato = separarDDDeTelefone(contato);
                        vm.contatos.telefones.push(angular.copy(contato));
                    }
                }
                if (contato.hasOwnProperty('tipoEmail')) {
                    if (!!contato.tipoEmail) {
                        vm.contatos.emails.push(angular.copy(contato));
                    }
                }
            }

            vm.contato = {};
            vm.mostrarContatoForm = false;
            focus('novoContato');
        }

        function separarDDDeTelefone(contato) {
            contato.ddd = contato.numero.substr(0, 2);
            contato.numero = contato.numero.substr(2);
            return contato;
        }

        function atualizarModoEdicaoListaContatos(contato) {
            vm.contatos.emails = vm.contatos.emails.map(mapModoEdicaoParaAtualizarContato(contato));
            vm.contatos.telefones = vm.contatos.telefones.map(mapModoEdicaoParaAtualizarContato(contato));
        }

        function mapModoEdicaoParaAtualizarContato(contato) {
            return function (c) {
                if (c.modoEdicao) {
                    contato.modoEdicao = false;
                    if (contato.hasOwnProperty('tipoTelefone')) {
                        contato = separarDDDeTelefone(contato);}
                    return contato;
                }
                return c;
            }
        }

        function cancelar() {
            vm.contato = {};
            mudarModoEdicaoContatosParaFalse();
            vm.mostrarContatoForm = false;
            focus('novoContato');
        }

        function mudarModoEdicaoContatosParaFalse() {
            vm.contatos.emails = vm.contatos.emails.map(mapModoEdicao);
            vm.contatos.telefones = vm.contatos.telefones.map(mapModoEdicao);
        }

        function mapModoEdicao(c) {
            c.modoEdicao = false;
            return c;
        }

        function novo() {
            vm.mostrarContatoForm = true;

            focus('contatoTipoContato');
        }

        function editar(contato) {
            vm.mostrarContatoForm = true;
            contato.tipoContato = descobreTipoDeContato(contato);
            vm.contato = angular.copy(contato);
        }

        function descobreTipoDeContato(contato) {
            if (contato.hasOwnProperty('tipoTelefone')) {
                return 'telefone'
            }
            if (contato.hasOwnProperty('tipoEmail')) {
                return 'email';
            }
        }

        function remover(contato) {
            var confirmacao = dialogs.confirm('Confirmar', "Confirma a remoção do contato");

            confirmacao
                .result
                .then(removerDaLista(contato))
                .catch(desfazerModoExclusao);
        }

        function removerDaLista(contato) {
            return function (btn) {
                if (contato.hasOwnProperty('tipoTelefone')) {
                    vm.contatos.telefones = vm.contatos
                        .telefones
                        .filter(modoEclusao);
                }
                if (contato.hasOwnProperty('tipoEmail')) {
                    vm.contatos.emails = vm.contatos
                        .emails
                        .filter(modoEclusao);
                }
            }
        }

        function modoEclusao (contato) {
            return !contato.modoExclusao;
        }

        function desfazerModoExclusao() {
            vm.contatos.telefones = vm.contatos
                .telefones
                .filter(desabilitarModoExlucao);

            vm.contatos.emails = vm.contatos
                .emails
                .filter(desabilitarModoExlucao);
        }

        function desabilitarModoExlucao(contato) {
            contato.modoExclusao = false

            return contato;
        }
    }
}());
