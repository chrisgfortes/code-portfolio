(function(){
    'use strict';

    angular
        .module('app')
        .factory('contato', contato)

    /** @ngInject */
    function contato($http, HOST){

        return {
            referenciaContatos: referenciaContatos,
            tiposEmail: tiposEmail
        }

        function referenciaContatos(){
            return $http.get(urlBase() + '/tipos-telefone', { cache: true });
        }

        function tiposEmail() {
            return $http.get(urlBase() + '/tipos-email', { cache: true });
        }

        function urlBase() {
            return HOST.pessoa + '/contato';
        }
    }
}());