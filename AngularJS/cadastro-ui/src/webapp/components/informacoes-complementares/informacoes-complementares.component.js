(function () {
    'use strict';

    infComplementaresController.$inject = [
        'avaliacaoGeral',
        'AuthFactory',
        'moment',
        'informacoesComplementaresConvert',
        'pessoa',
        'cooperativasFactory'
    ];


    angular
        .module('app')
        .component('informacoesComplementares', {
            bindings: {
                informacoesComplementares: '=',
                readonly: '<'
            },
            controller: infComplementaresController,
            templateUrl: './components/informacoes-complementares/informacoes-complementares.html'
        });

    function infComplementaresController(avaliacaoGeral, AuthFactory, moment, informacoesComplementaresConvert, pessoa, cooperativasFactory) {
        var vm = this;

        vm.$onInit = init;

        function init() {
            var coop = AuthFactory.buscarActionCooperativa() || AuthFactory.buscarCooperativa();

            vm.informacoesComplementares = vm.informacoesComplementares || { dataAnalise: moment() };

            vm.informacoesComplementares = informacoesComplementaresConvert.convertGet(vm.informacoesComplementares);

            pessoa
                .tiposDePessoa()
                .then(function (res) {
                    vm.codigoTipoPessoa = res.data;
                });
            cooperativasFactory
                .postos(coop)
                .then(function (res) {
                    vm.postosCoopSAU = res.data;
                });
            avaliacaoGeral
                .buscar()
                .then(function (res) {
                    vm.avaliacoes = res.data;
                });
        }

    }
}());
