(function () {
    'use strict';

    angular
        .module('app')
        .service('informacoesComplementaresConvert', informacoesComplementaresConvert)

    function informacoesComplementaresConvert() {

        this.convertGet = convertGet;
        this.convertPost = convertPost;

        function convertGet(informacoesComplementares) {
            if (!!informacoesComplementares.dataAnalise)
                informacoesComplementares.dataAnalise = moment(informacoesComplementares.dataAnalise);
            if (!!informacoesComplementares.codigoTipoPessoa)
                    informacoesComplementares.codigoTipoPessoa = informacoesComplementares.codigoTipoPessoa.toString();
            if (!!informacoesComplementares.codigoPosto)
                informacoesComplementares.codigoPosto = 'posto' + informacoesComplementares.codigoPosto.toString();
            if (informacoesComplementares.codigoPosto == 0)
                informacoesComplementares.codigoPosto = 'posto0';

            return informacoesComplementares;
        }

        function convertPost(informacoesComplementares) {
            if (!!informacoesComplementares.codigoPosto){
                informacoesComplementares.codigoPosto = informacoesComplementares.codigoPosto.split('posto')[1];
            }

            return informacoesComplementares;
        }
    }

}());
