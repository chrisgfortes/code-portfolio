(function() {
    'use strict';

    modalStepsController.$inject = ['$location', '$rootScope', '$interval', 'controleDeAcesso', 'associado', 'pessoa', 'contaCorrente'];

    angular
        .module('app')
        .component('modalSteps', {
            bindings: {
                open: '<',
                cpf: '<'
            },
            controller: modalStepsController,
            controllerAs: '$ctrl',
            templateUrl: './components/modal-steps/modal-steps.template.html'
        });

    function modalStepsController($location, $rootScope, $interval, controleDeAcesso, associado, pessoa, contaCorrente) {
        var vm = this;
        vm.criarAssociado = criarAssociado;
        vm.criarContaCorrente = criarContaCorrente;
        vm.criarAgendamentoCapital = criarAgendamentoCapital;
        vm.criarProdServ = criarProdServ;
        vm.alteracaoCadastral = alteracaoCadastral;
        vm.modal = {
            close: modalClose
        }

        $rootScope.$on('show-modal-steps', function(event, data) {
            inicializar(data);
            vm.cpf = data.cpf;
        });

        function inicializar(data) {
            limparVariaveis();
            buscarTipos(data.cpf)
                .then(function() {
                    carregarDadosCadastrado(data.cpf);
                    vm.open = data.enable;
                });
        }

        function buscarTipos(cpf) {
            return pessoa
                .buscarTipos(cpf)
                .then(function(response) {
                    vm.tipos = response;
                    !verificarTipos(vm.tipos, 'TERCEIRO') || (vm.tipo = 'terceiro');
                    !verificarTipos(vm.tipos, 'ASSOCIADO') || (vm.tipo = 'associado');
                    !verificarTipos(vm.tipos, 'CORRENTISTA') || (vm.tipo = 'correntista');
                    habilitarCards(response);
                });
        }

        function carregarDadosCadastrado(cpf) {
            var tipo = vm.tipo == 'correntista' ? 'terceiro' : vm.tipo;

            pessoa
                .status(cpf, tipo)
                .then(function(response) {
                    vm.nome = response.nome;
                });

            vm.cpf = cpf;
            !vm.isAssociado || buscarMatricula(vm.cpf);
            !vm.isCorrentista || buscarConta(vm.cpf);
        }

        function habilitarCards(tipos) {
            vm.isAssociado = verificarTipos(tipos, 'ASSOCIADO');
            vm.isCorrentista = verificarTipos(tipos, 'CORRENTISTA');
            vm.isAgCapital = verificarTipos(tipos, 'AGENDAMENTO_CAPITAL');
            vm.isProdServ = verificarTipos(tipos, 'PROD_SERV');
        }

        function verificarTipos(arrayTipos, tipo) {
            return arrayTipos.some(function(item) {
                return item == tipo;
            });
        }

        function buscarMatricula(cpf) {
            associado
                .buscarMatricula(cpf)
                .then(function(response) {
                    vm.matriculaAssociado = response.matricula.matricula;
                })
                .catch(function() {
                    vm.erroBuscarMatricula = true;
                });
        }

        function buscarConta(cpf) {
            contaCorrente
                .buscarPeloCpf(cpf)
                .then(function(contas) {
                    var conta = contas[0];
                    vm.contaCorrentista = conta.numero;
                })
                .catch(function() {
                    vm.erroBuscarConta = true;
                });
        }

        function limparVariaveis() {
            vm.tipo = undefined;
            vm.tipos = [];
            vm.cpf = undefined;
            vm.nome = undefined;
            vm.isAssociado = undefined;
            vm.isCorrentista = undefined;
            vm.isAgCapital = undefined;
            vm.isProdServ = undefined;
            vm.contaCorrentista = undefined;
            vm.erroBuscarConta = false;
            vm.matriculaAssociado = undefined;
            vm.erroBuscarMatricula = false;
        }

        function criarAssociado() {
            if (!vm.isAssociado){
                $location.path('/cadastro/associado/' + vm.cpf + '/matricula');
                vm.modal.close();
            }
        }

        function criarContaCorrente() {
            if (!vm.isCorrentista && vm.isAssociado){
                $location.path('/cadastro/correntista/' + vm.cpf + '/conta-corrente');
                vm.modal.close();
            }
        }

        function criarAgendamentoCapital() {
            if (!vm.isAgCapital && (vm.isAssociado || vm.isCorrentista)){
                $location.path('/cadastro/correntista/' + vm.cpf + '/agendamento-capital');
                vm.modal.close();
            }
        }

        function criarProdServ() {
            if (!vm.isProdServ && vm.isCorrentista){
                $location.path('/cadastro/correntista/' + vm.cpf + '/produtos-e-servicos');
                vm.modal.close();
            }
        }

        function alteracaoCadastral() {
            $location.path('/alteracao/' + vm.cpf);
            vm.modal.close();
        }

        function modalClose() {
            $interval.cancel(vm.theInterval);
            vm.open = false;
        }
    }
}());
