(function () {
    'use strict';

    listaProcuradoresController.$inject = ['$rootScope', 'dialogs', 'procuradorConvert'];

    angular
        .module('app')
        .component('listaProcuradores', {
            bindings: {
                procuradores: '=',
                readonly: '<',
                onNovo: '&',
                onEditar: '&',
                onRemover: '&',
                esconderBotaoNovo: '='
            },
            controller: listaProcuradoresController,
            templateUrl: './components/procuradores/lista-procuradores.html'
        });

    function listaProcuradoresController($rootScope, dialogs, procuradorConvert) {
        var vm = this;

        vm.$onInit = init;
        vm.editar = editar;
        vm.remover = remover;
        vm.novo = novo;

        function init() {
            vm.procuradores = vm.procuradores || [];
            if (vm.readonly) {
                vm.procuradores = procuradorConvert.convertGet(vm.procuradores);
            }
        }

        function novo() {
            vm.onNovo();
        }

        function editar(procurador) {
            vm.procuradores = vm.procuradores.map(function (p) {
                p.modoEdicao = false;
                return p;
            });

            procurador.modoEdicao = true;

            vm.onEditar({ procurador: angular.copy(procurador) });
        }

        function remover(procurador) {
            procurador.modoExclusao = true;
            vm.onRemover({ procurador: angular.copy(procurador) });
        }

    }
}());
