(function () {
    'use strict';

    sessaoProcuradorController.$inject = ['dialogs', 'focus', '$routeParams'];

    angular
        .module('app')
        .component('sessaoProcurador', {
            bindings: {
                procuradores: '=',
                readonly: '<'
            },
            controller: sessaoProcuradorController,
            templateUrl: './components/procuradores/sessao-procurador.html'
        });

    function sessaoProcuradorController(dialogs, focus, $routeParams) {
        var vm = this;

        vm.mostrarProcuradorForm = false;
        vm.salvar = salvar;
        vm.cancelar = cancelar;
        vm.novo = novo;
        vm.editar = editar;
        vm.remover = remover;

        function salvar(procurador) {
            if (validarCPF(procurador.cpf)) {
                var notificacao = dialogs.notify('Atenção', 'O CPF já foi cadastrado');
                notificacao
                    .result
                    .then(function () {
                        focus('procuradorCPF');
                    });

                return false;
            }

            if (procurador.modoEdicao)
                vm.procuradores = vm.procuradores.map(alterarProcurador(procurador));
            else {
                vm.procuradores.push(procurador);
            }
            
            vm.mostrarProcuradorForm = false;
            
            focus('novoProcurador');
            
            return true;
        }

        function validarCPF(cpf) {
            return vm.procuradores.some(function (item) {
                return !item.modoEdicao && item.cpf == cpf;
            });
        }

        function alterarProcurador(procuradorAltedado) {
            return function (procurador) {
                if (procurador.modoEdicao) {
                    procuradorAltedado.modoEdicao = false;
                    return procuradorAltedado;
                }
                return procurador;
            }
        }

        function cancelar() {
            vm.procuradores = vm.procuradores.map(function (procurador) {
                procurador.modoEdicao = false;

                return procurador;
            });

            vm.mostrarProcuradorForm = false;
            focus('novoProcurador');
        }

        function novo() {
            vm.mostrarProcuradorForm = true;
            focus('procuradorCPF');
        }

        function editar(procurador) {
            vm.mostrarProcuradorForm = true;

            vm.procurador = angular.copy(procurador);
        }

        function remover(procurador) {
            var confirmacao = dialogs.confirm('Confirmar', 'Confirma a remoção da participação societária?');
            confirmacao
                .result
                .then(function (btn) {
                    vm.procuradores = vm.procuradores.filter(function (procurador) {
                        return !procurador.modoExclusao;
                    });
                })
                .catch(function () {
                    vm.procuradores = vm.procuradores.map(function (procurador) {
                        procurador.modoExclusao = false;
                        return procurador;
                    });
                });
        }
    }
}());
