(function () {
    'use strict';

    dadosProcuradorController.$inject = ['moment', 'procuracao'];

    angular
        .module('app')
        .component('dadosProcurador', {
            bindings: {
                procurador: '<',
                onSalvar: '&',
                onCancelar: '&'
            },
            controller: dadosProcuradorController,
            templateUrl: './components/procuradores/dados-procurador.html'
        });

    function dadosProcuradorController(moment, procuracao) {
        var vm = this;

        vm.vigenciaMinima = moment();
        vm.dataProcuracaoMaxima = moment();
        vm.dataEntradaFormatada = moment().format('DD/MM/YYYY');
        vm.validarDataVigencia = validarDataVigencia;
        vm.validarDataProcuracao = validarDataProcuracao;

        vm.$onInit = onInit;
        vm.salvar = salvar;
        vm.cancelar = cancelar;

        function onInit() {
            procuracao
                .tipos()
                .then(function (res) {
                    vm.tiposProcuracao = res.data;
                });
        }

        function salvar(procurador) {
            vm.onSalvar({ procurador: angular.copy(procurador) });
        }

        function cancelar() {
            vm.onCancelar();
            vm.procurador = {};
        }

        function validarDataVigencia (data) {
            if (!validar(data)) {
                vm.procurador.procuracao.vigencia = {};
            }
        }

        function validarDataProcuracao (data) {
            if (!validar(data)) {
                vm.procurador.procuracao.dataProcuracao = {};
            }
        }

        function validar(data) {
            return moment(data, 'DD/MM/YYYY', true).isValid();
        }
    }
}());
