(function () {
    'use strict';

    angular
        .module('app')
        .service('procuradorConvert', procuradorConvert)

    function procuradorConvert(moment) {

        this.convertGet = convertGet;
        this.convertPost = convertPost;

        function convertGet(procuradores) {
            return procuradores.map(function (procurador) {
                procurador.procuracao.vigencia = moment(procurador.procuracao.vigencia);
                procurador.procuracao.dataProcuracao = moment(procurador.procuracao.dataProcuracao);
                procurador.dataNascimento = moment(procurador.dataNascimento);
                procurador.dataEmissao = moment(procurador.dataEmissao);
                return procurador;
            });
        }

        function convertPost(procuradores) {
            return procuradores.map(function (procurador) {
                delete procurador.representanteLegal;
                delete procurador.modalidadeRepresentanteLegal;
                delete procurador.nacionalidade;

                return procurador;
            })
        }
    }

}());
