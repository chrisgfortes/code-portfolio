(function () {
    'use strict';

    angular
        .module('app')
        .component('novaSessao', {
            transclude: true,
            bindings: {
                titulo: '@',
                mostraSessao: '<',
                info: '<'
            },
            controller: novaSessaoController,
            templateUrl: './components/nova-sessao/nova-sessao.html'
        });

    function novaSessaoController() {
        var vm = this;
        
        vm.mostrar = function () {
            vm.mostraSessao = !vm.mostraSessao;
        }

        vm.$onInit = function () {
            vm.titulo = '';
        }
    }
}());
