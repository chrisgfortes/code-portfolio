(function() {
    'use strict';

    angular
        .module('app')
        .factory('veiculosUpdate', veiculosUpdate)

    function veiculosUpdate($http, HOST, veiculoConvert) {
        return {
            salvar: salvar,
            buscar: buscar,
            excluir: excluir
        }

        function salvar(cpf, dados) {
            var url = urlBuild(cpf);
            var veiculos = angular.copy(dados);
            veiculos = veiculoConvert.convertPost(veiculos);
            return $http.put(url, veiculos);
        }

        function buscar(cpf) {
            var url = urlBuild(cpf);

            return $http
                .get(url)
                .then(transformaRetorno);
        }

        function excluir(cpf, id) {
            var url = urlBuild(cpf) + '/' + id;
            return $http.delete(url);
        }

        function urlBuild(cpf) {
            var url = HOST.terceiro + cpf + '/veiculos';
            return url;
        }

        function transformaRetorno(res) {
            return res.data;
        }
    }

}());
