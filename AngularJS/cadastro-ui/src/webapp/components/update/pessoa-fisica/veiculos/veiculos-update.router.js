(function (app) {
    'use strict';

    app.config(function ($routeProvider) {
        $routeProvider.when('/alteracao/pessoa-fisica/:cpf/veiculos', {
            templateUrl: 'components/update/pessoa-fisica/veiculos/veiculos-update.html',
            controller: 'veiculosUpdateController',
            controllerAs: 'ctrl',
            resolve: {
                isNotTerceiro: function ($route, rotaService) {
                    var cpf = $route.current.params.cpf;
                    return rotaService.isNotTerceiro(cpf);
                },
                section: function (secao) {
                    secao.nome = 'Alteracao de Cadastro | Veículos';
                },
                veiculosInfo: function (veiculosUpdate, $route, veiculoConvert) {
                    var cpf = $route.current.params.cpf;

                    return veiculosUpdate
                        .buscar(cpf)
                        .then(function (res) {
                            return res = veiculoConvert.convertGet(res || []);
                        })
                        .catch(function () {
                            return [];
                        })
                },
                temPermissao: function (controleDeAcesso) {
                    return controleDeAcesso.temAcesso('Cadastro Atualizar');
                }
            }
        });
    });
})(angular.module('app'));
