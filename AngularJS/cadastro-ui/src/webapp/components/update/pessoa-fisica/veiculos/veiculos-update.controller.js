(function (app) {
    'use strict';

    app.controller('veiculosUpdateController', veiculosUpdateController);

    function veiculosUpdateController($rootScope, veiculosInfo, veiculosUpdate, $routeParams, veiculoConvert, $timeout) {
        var vm = this;

        vm.veiculosObj = veiculosInfo;
        vm.update = update;
        vm.cpf = $routeParams.cpf;

        function update(veiculosObj){
        	var cpf = $routeParams.cpf;

            vm.savingProgress = true;

        	veiculosUpdate
        		.salvar(cpf, veiculosObj)
        		.then(function(res){
                    vm.erros = [];
        			vm.sucessos = [{
        				message: 'Cadastro atualizado com sucesso'
        			}];
        		})
        		.catch(function(erro){
                    vm.sucessos = [];
        			if (erro.status == 422) {
						vm.erros = erro.data.body || erro.data.content;
					}
        		})
                .finally(function() {
                    atualizarLista().then( function (res) {
                        vm.veiculosObj = res;
                        $rootScope.$broadcast('atualizar-descricao-tipo-veiculo', res);
                        vm.savingProgress = false;
                    });
                });

        }

        function atualizarLista() {
            return veiculosUpdate
                .buscar(vm.cpf)
                .then(function (res) {
                    return res = veiculoConvert.convertGet(res || []);
                })
                .catch(function () {
                    return [];
                })
        }
    }
})(angular.module('app'));
