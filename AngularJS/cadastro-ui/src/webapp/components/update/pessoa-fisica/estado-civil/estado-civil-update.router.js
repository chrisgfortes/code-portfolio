(function (app) {
    'use strict';

    app.config(function ($routeProvider) {
        $routeProvider.when('/alteracao/pessoa-fisica/:cpf/estado-civil', {
            templateUrl: 'components/update/pessoa-fisica/estado-civil/estado-civil-update.html',
            controller: 'estadoCivilUpdateController',
            controllerAs: 'ctrl',
            resolve: {
                isNotTerceiro: function ($route, rotaService) {
                    var cpf = $route.current.params.cpf;
                    return rotaService.isNotTerceiro(cpf);
                },
                section: function (secao) {
                    secao.nome = 'Alteracao de Cadastro | Estado Civil';
                },
                estadoCivilInfo: function (estadoCivilUpdate, $route) {
                    var cpf = $route.current.params.cpf;

                    return estadoCivilUpdate
                        .buscar(cpf)
                        .catch(function () {
                            return {};
                        })
                },
                temPermissao: function (controleDeAcesso) {
                    return controleDeAcesso.temAcesso('Cadastro Atualizar');
                }
            }
        });
    });
})(angular.module('app'));
