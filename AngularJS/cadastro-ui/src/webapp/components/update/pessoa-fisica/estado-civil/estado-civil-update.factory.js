(function() {
    'use strict';

    angular
        .module('app')
        .factory('estadoCivilUpdate', estadoCivilUpdate)

    function estadoCivilUpdate($http, HOST) {
        return {
            salvar: salvar,
            buscar: buscar
        }

        function salvar(cpf, dados) {
            var url = urlBuild(cpf);
            return $http.put(url, dados);
        }

        function buscar(cpf) {
            var url = urlBuild(cpf);

            return $http
                .get(url)
                .then(transformaRetorno);
        }

        function urlBuild(cpf) {
            var url = (HOST.terceiro + cpf + '/estado-civil');
            return url;
        }

        function transformaRetorno(res) {
            return res.data;
        }
    }

}());
