(function (app) {
    'use strict';

    app.controller('estadoCivilUpdateController', estadoCivilUpdateController);

    function estadoCivilUpdateController(estadoCivilInfo, estadoCivilUpdate, $routeParams) {
        var vm = this;

        vm.estadoCivilObj = estadoCivilInfo;
        vm.update = update;
        vm.cpf = $routeParams.cpf;

        function update(estadoCivilObj){
        	var cpf = $routeParams.cpf;
            vm.savingProgress = true;

        	estadoCivilUpdate
        		.salvar(cpf, estadoCivilObj)
        		.then(function(res){
                    vm.erros = [];
        			vm.sucessos = [{
        				message: 'Cadastro atualizado com sucesso'
        			}];
        		})
        		.catch(function(erro){
                    vm.sucessos = [];
        			if (erro.status == 422) {
						vm.erros = erro.data.body || erro.data.content;
					}
        		})
                .finally(function() {
                    vm.savingProgress = false;
                });

        }
    }
})(angular.module('app'));
