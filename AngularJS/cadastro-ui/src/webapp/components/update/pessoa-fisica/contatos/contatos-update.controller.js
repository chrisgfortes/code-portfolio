(function (app) {
    'use strict';

    app.controller('contatosUpdateController', contatosUpdateController);

    function contatosUpdateController(contatosInfo, contatosUpdate, $routeParams) {
        var vm = this;

        vm.contatos = contatosInfo;
        vm.update = update;
        vm.cpf = $routeParams.cpf;

        function update(contatos){
        	var cpf = $routeParams.cpf;
            vm.savingProgress = true;

        	contatosUpdate
        		.salvar(cpf, contatos)
        		.then(function(res){
                    vm.erros = [];
        			vm.sucessos = [{
        				message: 'Cadastro atualizado com sucesso'
        			}];
        		})
        		.catch(function(erro){
                    vm.sucessos = [];
        			if (erro.status == 422) {
						vm.erros = erro.data.body || erro.data.content;
					}
        		})
                .finally(function() {
                    vm.savingProgress = false;
                });

        }
    }
})(angular.module('app'));
