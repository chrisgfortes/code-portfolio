(function() {
    'use strict';

    angular
        .module('app')
        .factory('contatosUpdate', contatosUpdate)

    function contatosUpdate($http, HOST) {
        return {
            salvar: salvar,
            buscar: buscar
        }

        function salvar(cpf, dados) {
            var url = urlBuild(cpf);
            var _dados = angular.copy(dados);

            return $http.post(url, _dados);
        }

        function buscar(cpf) {
            var url = urlBuild(cpf);

            return $http
                .get(url)
                .then(transformaRetorno);
        }

        function urlBuild(cpf) {
            return HOST.terceiro + cpf + '/contatos';
        }

        function transformaRetorno(res) {
            return res.data;
        }
    }

}());
