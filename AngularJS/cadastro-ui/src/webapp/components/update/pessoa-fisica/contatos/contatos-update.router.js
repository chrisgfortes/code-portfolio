(function (app) {
    'use strict';

    app.config(function ($routeProvider) {
        $routeProvider.when('/alteracao/pessoa-fisica/:cpf/contatos', {
            templateUrl: 'components/update/pessoa-fisica/contatos/contatos-update.html',
            controller: 'contatosUpdateController',
            controllerAs: 'ctrl',
            resolve: {
                isNotTerceiro: function ($route, rotaService) {
                    var cpf = $route.current.params.cpf;
                    return rotaService.isNotTerceiro(cpf);
                },
                section: function (secao) {
                    secao.nome = 'Alteracao de Cadastro | contatos';
                },
                contatosInfo: function (contatosUpdate, $route) {
                    var cpf = $route.current.params.cpf;

                    return contatosUpdate
                        .buscar(cpf)
                        .catch(function () {
                            return [];
                        })
                },
                temPermissao: function (controleDeAcesso) {
                    return controleDeAcesso.temAcesso('Cadastro Atualizar');
                }
            }
        });
    });
})(angular.module('app'));
