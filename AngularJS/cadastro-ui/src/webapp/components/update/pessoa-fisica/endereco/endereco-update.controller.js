(function (app) {
    'use strict';

    app.controller('enderecoUpdateController', enderecoUpdateController);

    function enderecoUpdateController(enderecoInfo, enderecoUpdate, $routeParams) {
        var vm = this;

        vm.enderecos = enderecoInfo;
        vm.update = update;
        vm.cpf = $routeParams.cpf;

        function update(enderecos){
        	var cpf = $routeParams.cpf;
            vm.savingProgress = true;

        	enderecoUpdate
        		.salvar(cpf, enderecos)
        		.then(function(res){
                    vm.erros = [];
        			vm.sucessos = [{
        				message: 'Cadastro atualizado com sucesso'
        			}];
        		})
        		.catch(function(erro){
                    vm.sucessos = [];
        			if (erro.status == 422) {
						vm.erros = erro.data.body || erro.data.content;
					}
        		})
                .finally(function() {
                    vm.savingProgress = false;
                });

        }
    }
})(angular.module('app'));
