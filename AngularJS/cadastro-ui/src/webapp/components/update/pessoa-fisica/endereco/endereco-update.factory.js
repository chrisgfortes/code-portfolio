(function() {
    'use strict';

    angular
        .module('app')
        .factory('enderecoUpdate', enderecoUpdate)

    function enderecoUpdate($http, HOST) {
        return {
            salvar: salvar,
            buscar: buscar
        }

        function salvar(cpf, dados) {
            var url = urlBuild(cpf);
            return $http.put(url, dados);
        }

        function buscar(cpf) {
            var url = urlBuild(cpf);

            return $http
                .get(url)
                .then(transformaRetorno);
        }

        function urlBuild(cpf) {
            return HOST.terceiro + cpf + '/enderecos';
        }

        function transformaRetorno(res) {
            return res.data;
        }
    }

}());
