(function (app) {
    'use strict';

    app.config(function ($routeProvider) {
        $routeProvider.when('/alteracao/pessoa-fisica/:cpf/endereco', {
            templateUrl: 'components/update/pessoa-fisica/endereco/endereco-update.html',
            controller: 'enderecoUpdateController',
            controllerAs: 'ctrl',
            resolve: {
                isNotTerceiro: function ($route, rotaService) {
                    var cpf = $route.current.params.cpf;
                    return rotaService.isNotTerceiro(cpf);
                },
                section: function (secao) {
                    secao.nome = 'Alteracao de Cadastro | Endereço';
                },
                enderecoInfo: function (enderecoUpdate, $route) {
                    var cpf = $route.current.params.cpf;

                    return enderecoUpdate
                        .buscar(cpf)
                        .catch(function () {
                            return [];
                        })
                },
                temPermissao: function (controleDeAcesso) {
                    return controleDeAcesso.temAcesso('Cadastro Atualizar');
                }
            }
        });
    });
})(angular.module('app'));
