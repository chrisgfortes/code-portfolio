(function() {
    'use strict';

    angular
        .module('app')
        .factory('dependentesUpdate', dependentesUpdate)

    function dependentesUpdate($http, HOST, dependenteConvert) {
        return {
            salvar: salvar,
            buscar: buscar
        }

        function salvar(cpf, dados) {
            var url = urlBuild(cpf);
            var _dados = angular.copy(dados);

            _dados = _dados.map(dependenteConvert.post);

            return $http.put(url, _dados);
        }

        function buscar(cpf) {
            var url = urlBuild(cpf);

            return $http
                .get(url)
                .then(transformaRetorno)
                .then(function (dependentes){

                    dependentes = dependentes.map(function (dependente) {
                        dependente.dataNascimento = moment(dependente.dataNascimento);
                        return dependente;
                    });

                    return dependentes;
                });
        }

        function urlBuild(cpf) {
            return HOST.terceiro + cpf + '/dependentes';
        }

        function transformaRetorno(res) {
            return res.data;
        }
    }

}());
