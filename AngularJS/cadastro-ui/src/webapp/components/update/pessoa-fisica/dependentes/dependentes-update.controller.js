(function (app) {
    'use strict';

    app.controller('dependentesUpdateController', dependentesUpdateController);

    function dependentesUpdateController(dependentesInfo, dependentesUpdate, $routeParams) {
        var vm = this;

        vm.dependentes = dependentesInfo;
        vm.update = update;
        vm.cpf = $routeParams.cpf;

        function update(dependentes){
        	var cpf = $routeParams.cpf;
            vm.savingProgress = true;

        	dependentesUpdate
        		.salvar(cpf, dependentes)
        		.then(function(res){
                    vm.erros = [];
        			vm.sucessos = [{
        				message: 'Cadastro atualizado com sucesso'
        			}];
        		})
        		.catch(function(erro){
                    vm.sucessos = [];
        			if (erro.status == 422) {
						vm.erros = erro.data.body || erro.data.content;
					}
        		})
                .finally(function() {
                    vm.savingProgress = false;
                });

        }
    }
})(angular.module('app'));
