(function (app) {
    'use strict';

    app.config(function ($routeProvider) {
        $routeProvider.when('/alteracao/pessoa-fisica/:cpf/dependentes', {
            templateUrl: 'components/update/pessoa-fisica/dependentes/dependentes-update.html',
            controller: 'dependentesUpdateController',
            controllerAs: 'ctrl',
            resolve: {
                isNotTerceiro: function ($route, rotaService) {
                    var cpf = $route.current.params.cpf;
                    return rotaService.isNotTerceiro(cpf);
                },
                section: function (secao) {
                    secao.nome = 'Alteracao de Cadastro | Dependentes';
                },
                dependentesInfo: function (dependentesUpdate, $route) {
                    var cpf = $route.current.params.cpf;

                    return dependentesUpdate
                        .buscar(cpf)
                        .catch(function () {
                            return [];
                        })
                },
                temPermissao: function (controleDeAcesso) {
                    return controleDeAcesso.temAcesso('Cadastro Atualizar');
                }
            }
        });
    });
})(angular.module('app'));
