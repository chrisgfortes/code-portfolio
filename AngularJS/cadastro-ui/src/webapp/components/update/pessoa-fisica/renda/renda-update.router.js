(function (app) {
    'use strict';

    app.config(function ($routeProvider) {
        $routeProvider.when('/alteracao/pessoa-fisica/:cpf/renda', {
            templateUrl: 'components/update/pessoa-fisica/renda/renda-update.html',
            controller: 'rendaUpdateController',
            controllerAs: 'ctrl',
            resolve: {
                isNotTerceiro: function ($route, rotaService) {
                    var cpf = $route.current.params.cpf;
                    return rotaService.isNotTerceiro(cpf);
                },
                section: function (secao) {
                    secao.nome = 'Alteracao de Cadastro | Renda';
                },
                rendasInfo: function (rendaUpdate, $route) {
                    var cpf = $route.current.params.cpf;

                    return rendaUpdate
                        .buscar(cpf)
                        .catch(function () {
                            return { rendas:[] };
                        })
                },
                temPermissao: function (controleDeAcesso) {
                    return controleDeAcesso.temAcesso('Cadastro Atualizar');
                }
            }
        });
    });
})(angular.module('app'));
