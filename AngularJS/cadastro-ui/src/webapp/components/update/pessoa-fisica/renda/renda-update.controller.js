(function (app) {
    'use strict';

    app.controller('rendaUpdateController', rendaUpdateController);

    function rendaUpdateController(rendasInfo, rendaUpdate, $routeParams) {
        var vm = this;

        vm.rendasObj = rendasInfo;
        vm.update = update;
        vm.cpf = $routeParams.cpf;

        function update(rendasObj){
        	var cpf = $routeParams.cpf;
            vm.savingProgress = true;

        	rendaUpdate
        		.salvar(cpf, rendasObj)
        		.then(function(res){
                    vm.erros = [];
        			vm.sucessos = [{
        				message: 'Cadastro atualizado com sucesso'
        			}];
        		})
        		.catch(function(erro){
                    vm.sucessos = [];
        			if (erro.status == 422) {
						vm.erros = erro.data.body || erro.data.content;
					}
        		})
                .finally(function() {
                    vm.savingProgress = false;
                });

        }
    }
})(angular.module('app'));
