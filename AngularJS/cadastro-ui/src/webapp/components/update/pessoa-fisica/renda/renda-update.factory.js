(function() {
    'use strict';

    angular
        .module('app')
        .factory('rendaUpdate', rendaUpdate)

    function rendaUpdate($http, HOST, rendaConverter) {
        return {
            salvar: salvar,
            buscar: buscar
        }

        function salvar(cpf, dados) {
            var url = urlBuild(cpf);
            return $http.put(url, dados);
        }

        function buscar(cpf) {
            var url = urlBuild(cpf);

            return $http
                .get(url)
                .then(transformaRetorno)
                .then(function (objRetorno){
                    objRetorno.rendas = objRetorno.rendas.map(rendaConverter.get);

                    return objRetorno;
                });
        }

        function urlBuild(cpf) {
            return HOST.terceiro + cpf + '/renda';
        }

        function transformaRetorno(res) {
            return res.data;
        }
    }

}());
