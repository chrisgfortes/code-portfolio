(function (app) {
    'use strict';

    app.config(function ($routeProvider) {
        $routeProvider.when('/alteracao/pessoa-fisica/:cpf/imoveis', {
            templateUrl: 'components/update/pessoa-fisica/imoveis/imoveis-update.html',
            controller: 'imoveisUpdateController',
            controllerAs: 'ctrl',
            resolve: {
                isNotTerceiro: function ($route, rotaService) {
                    var cpf = $route.current.params.cpf;
                    return rotaService.isNotTerceiro(cpf);
                },
                section: function (secao) {
                    secao.nome = 'Alteracao de Cadastro | Imóveis';
                },
                imoveisInfo: function (imoveisUpdate, $route) {
                    var cpf = $route.current.params.cpf;

                    return imoveisUpdate
                        .buscar(cpf)
                        .catch(function () {
                            return [];
                        })
                },
                temPermissao: function (controleDeAcesso) {
                    return controleDeAcesso.temAcesso('Cadastro Atualizar');
                }
            }
        });
    });
})(angular.module('app'));
