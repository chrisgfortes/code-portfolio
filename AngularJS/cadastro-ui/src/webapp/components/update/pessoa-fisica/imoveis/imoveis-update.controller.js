(function (app) {
    'use strict';

    app.controller('imoveisUpdateController', imoveisUpdateController);

    function imoveisUpdateController($rootScope, imoveisInfo, imoveisUpdate, $routeParams) {
        var vm = this;

        vm.imoveisObj = imoveisInfo;
        vm.update = update;
        vm.cpf = $routeParams.cpf;

        function update(imoveisObj){
        	var cpf = $routeParams.cpf;

            vm.savingProgress = true;

        	imoveisUpdate
        		.salvar(cpf, imoveisObj)
        		.then(function(res){
                    vm.erros = [];
        			vm.sucessos = [{
        				message: 'Cadastro atualizado com sucesso'
        			}];
        		})
        		.catch(function(erro){
                    vm.sucessos = [];
                    if (erro.status == 422) {
						vm.erros = erro.data.body || erro.data.content;
					}
        		})
                .finally(function() {
                    atualizarLista().then( function (res) {
                        vm.imoveisObj = res;
                        $rootScope.$broadcast('atualizar-descricao-tipo-imovel', res);
                        vm.savingProgress = false;
                    });
                });

        }

        function atualizarLista() {
            return imoveisUpdate
                .buscar(vm.cpf)
                .then(function (res) {
                    return res;
                })
                .catch(function () {
                    return [];
                })
        }
    }
})(angular.module('app'));
