(function() {
    'use strict';

    angular
        .module('app')
        .factory('imoveisUpdate', imoveisUpdate)

    function imoveisUpdate($http, HOST) {
        return {
            salvar: salvar,
            buscar: buscar,
            excluir: excluir
        }

        function salvar(cpf, dados) {
            var url = urlBuild(cpf);
            return $http.put(url, dados);
        }

        function buscar(cpf) {
            var url = urlBuild(cpf);

            return $http
                .get(url)
                .then(transformaRetorno);
        }

        function excluir(cpf, id) {
            var url = urlBuild(cpf) + '/' + id;
            return $http.delete(url);
        }

        function urlBuild(cpf) {
            var url = (HOST.terceiro + cpf + '/imoveis');
            return url;
        }

        function transformaRetorno(res) {
            return res.data;
        }
    }

}());
