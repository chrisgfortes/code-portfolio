(function (app) {
    'use strict';

    app.config(function ($routeProvider) {
        $routeProvider.when('/alteracao/pessoa-fisica/:cpf/dados-profissionais', {
            templateUrl: 'components/update/pessoa-fisica/dados-profissionais/dados-profissionais-update.html',
            controller: 'dadosProfissionaisUpdateController',
            controllerAs: 'ctrl',
            resolve: {
                isNotTerceiro: function ($route, rotaService) {
                    var cpf = $route.current.params.cpf;
                    return rotaService.isNotTerceiro(cpf);
                },
                section: function (secao) {
                    secao.nome = 'Alteracao de Cadastro | Dados Profissionais';
                },
                dadosProfissionaisInfo: function (dadosProfissionaisUpdate, $route) {
                    var cpf = $route.current.params.cpf;

                    return dadosProfissionaisUpdate
                        .buscar(cpf)
                        .catch(function () {
                            return {};
                        })
                }
            }
        });
    });
})(angular.module('app'));
