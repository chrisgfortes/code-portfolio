(function() {
    'use strict';

    angular
        .module('app')
        .factory('dadosProfissionaisUpdate', dadosProfissionaisUpdate)

    function dadosProfissionaisUpdate($http, HOST, dadosProfissionaisConvert) {
        return {
            salvar: salvar,
            buscar: buscar
        }

        function salvar(cpf, dados) {
            var url = urlBuild(cpf);
            dados = dadosProfissionaisConvert.post(dados);
            return $http.put(url, dados);
        }

        function buscar(cpf) {
            var url = urlBuild(cpf);

            return $http
                .get(url)
                .then(transformaRetorno);
        }

        function urlBuild(cpf) {
            var url = (HOST.terceiro + cpf + '/dados-profissionais');
            return url;
        }

        function transformaRetorno(res) {
            return res.data;
        }
    }

}());
