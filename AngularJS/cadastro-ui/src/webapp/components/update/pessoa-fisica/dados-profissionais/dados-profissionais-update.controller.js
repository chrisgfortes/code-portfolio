(function (app) {
    'use strict';

    app.controller('dadosProfissionaisUpdateController', dadosProfissionaisUpdateController);

    function dadosProfissionaisUpdateController(dadosProfissionaisInfo, dadosProfissionaisUpdate, $routeParams) {
        var vm = this;

        vm.dadosProfissionais = dadosProfissionaisInfo;
        vm.update = update;
        vm.cpf = $routeParams.cpf;

        function update(dadosProfissionaisObj){
        	var cpf = $routeParams.cpf;
            vm.savingProgress = true;

        	dadosProfissionaisUpdate
        		.salvar(cpf, dadosProfissionaisObj)
        		.then(function(res){
                    vm.erros = [];
        			vm.sucessos = [{
        				message: 'Cadastro atualizado com sucesso'
        			}];
        		})
        		.catch(function(erro){
                    vm.sucessos = [];
        			if (erro.status == 422) {
						vm.erros = erro.data.body || erro.data.content;
					}
        		})
                .finally(function() {
                    vm.savingProgress = false;
                });

        }
    }
})(angular.module('app'));
