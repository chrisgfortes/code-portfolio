(function () {
    'use strict';

    angular
        .module('app')
        .value('updateLinks', [
            {
                active: true,
                descricao: 'Renda',
                url: function (cpf) {
                    return '#/alteracao/pessoa-fisica/' + cpf + '/renda';
                }
            },
            {
                active: true,
                descricao: 'Dependentes',
                url: function (cpf) {
                    return '#/alteracao/pessoa-fisica/' + cpf + '/dependentes'
                }
            },
            {
                active: true,
                descricao: 'Estado Civil',
                url: function (cpf) {
                    return '#/alteracao/pessoa-fisica/' + cpf + '/estado-civil'
                }
            },
            {
                active: true,
                descricao: 'Veículos',
                url: function (cpf) {
                    return '#/alteracao/pessoa-fisica/' + cpf + '/veiculos'
                }
            },
            {
                active: true,
                descricao: 'Imóveis',
                url: function (cpf) {
                    return '#/alteracao/pessoa-fisica/' + cpf + '/imoveis'
                }
            },
            {
                active: true,
                descricao: 'Dados Profissionais',
                url: function (cpf) {
                    return '#/alteracao/pessoa-fisica/' + cpf + '/dados-profissionais'
                }
            },
            {
                active: true,
                descricao: 'Contatos',
                url: function (cpf) {
                    return '#/alteracao/pessoa-fisica/' + cpf + '/contatos'
                }
            },
            {
                active: false,
                descricao: 'Endereço',
                url: function (cpf) {
                    return '#/alteracao/pessoa-fisica/' + cpf + '/endereco';
                }
            },
            {
                active: false,
                descricao: 'Cartão Autógrafo',
                url: function (cpf) {
                    return '#/alteracao/pessoa-fisica/' + cpf + '/cartao-autografo'
                }
            },
            {
                active: false,
                descricao: 'Dados Complementares',
                url: function (cpf) {
                    return '#/alteracao/pessoa-fisica/' + cpf + '/dados-complementares'
                }
            },
            {
                active: false,
                descricao: 'Produtos',
                url: function (cpf) {
                    return '#/alteracao/pessoa-fisica/' + cpf + '/produtos'
                }
            },
            {
                active: false,
                descricao: 'Conta Corrente',
                url: function (cpf) {
                    return '#/alteracao/pessoa-fisica/' + cpf + '/conta-corrente'
                }
            },
            {
                active: false,
                descricao: 'Matrícula',
                url: function (cpf) {
                    return '#/alteracao/pessoa-fisica/' + cpf + '/matricula'
                }
            },
            {
                active: false,
                descricao: 'Análise Cadastral',
                url: function (cpf) {
                    return '#/alteracao/pessoa-fisica/' + cpf + '/analise-cadastral'
                }
            }
        ]);
}());
