(function () {
    'use strict';

    pageTitleController.inject = ['$routeParams', 'AuthFactory', 'pessoa', '$location', 'contaCorrente', 'associado']

    angular
        .module('app')
        .component('pageTitle', {
            transclude: true,
            bindings: {
                titulo: '<',
                subtitulo: '<'
            },
            controller: pageTitleController,
            templateUrl: './components/page-title/page-title.html'
        });

    function pageTitleController($routeParams, AuthFactory, pessoa, $location, contaCorrente, associado) {
        var vm = this;

        vm.$onInit = onInit;
        vm.isPaginaAlteracao = isPaginaAlteracao;

        function onInit(){
            vm.isAlteracao = vm.isPaginaAlteracao();

            var cpf = $routeParams.cpf
            buscarTipos(cpf)
                .then(function() {
                    carregarDadosCadastrado(cpf)
                });
        }

        function buscarTipos(cpf) {
            return pessoa
                .buscarTipos(cpf)
                .then(function(tipos) {
                    setTipos(tipos);
                });
        }

        function carregarDadosCadastrado(cpf) {
            pessoa.status(cpf, 'terceiro')
                .then(function(resp){
                    setUserInformation(resp);
                })
                .catch(function(error){
                    setUserInformation(error);
                })

            !vm.isAssociado || buscarMatricula(cpf);
            !vm.isCorrentista || buscarConta(cpf);
        }

        function setUserInformation(resp){
            var coopAction = AuthFactory.buscarActionCooperativa() || AuthFactory.buscarCooperativa();

            vm.user = angular.extend(resp, {
                cpfCnpj: $routeParams.cpf,
                cooperativa: coopAction
            });
        }

        function setTipos(tipos) {
            vm.isAssociado = verificarTipos(tipos, 'ASSOCIADO');
            vm.isCorrentista = verificarTipos(tipos, 'CORRENTISTA');
        }

        function verificarTipos(arrayTipos, tipo) {
            return arrayTipos.some(function(item) {
                return item == tipo;
            });
        }

        function buscarMatricula(cpf) {
            associado
                .buscarMatricula(cpf)
                .then(function(response) {
                    vm.matriculaAssociado = response.matricula.matricula;
                });
        }

        function buscarConta(cpf) {
            contaCorrente
                .buscarPeloCpf(cpf)
                .then(function(contas) {
                    var conta = contas[0];
                    vm.contaCorrentista = conta.numero;
                });
        }

        function isPaginaAlteracao() {
            var pagina = $location.path().split('/')[1];
            return (pagina == 'alteracao');
        }
    }
}());
