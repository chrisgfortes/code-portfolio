(function() {
    'use strict';

    angular
        .module('app')
        .factory('updateDadoDataAnalise', updateDadoDataAnalise)

    function updateDadoDataAnalise($http, HOST) {
        return {
            salvar: salvar,
            buscar: buscar
        }

        function salvar(cpf, data) {
            var url = urlBuild(cpf);
            return $http.put(url, data);
        }

        function buscar(cpf) {
            var url = urlBuild(cpf);

            return $http
                .get(url)
                .then(transformaRetorno);
        }

        function urlBuild(cpf) {
            return HOST.terceiro + cpf + '/analise-cadastral';
        }

        function transformaRetorno(res) {
            return res.data;
        }
    }

}());
