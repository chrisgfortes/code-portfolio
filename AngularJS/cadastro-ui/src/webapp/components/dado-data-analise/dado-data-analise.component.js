(function () {
    'use strict';

    dadoDataAnaliseController.$inject = ['$routeParams', 'focus', 'updateDadoDataAnalise', 'moment', '$timeout'];

    angular
        .module('app')
        .component('dadoDataAnalise', {
            bindings: {},
            controller: dadoDataAnaliseController,
            templateUrl: './components/dado-data-analise/dado-data-analise.html'
        });

    function dadoDataAnaliseController($routeParams, focus, updateDadoDataAnalise, moment, $timeout) {
        var vm = this;

        vm.dataAnaliseAtual = null;
        vm.updateDataAnalise = updateDataAnalise;
        vm.buscarDataAnalise = buscarDataAnalise;
        vm.$onInit = init;

        function init() {
            vm.dataAtual = moment();
            vm.buscarDataAnalise().then(function (dataRetorno) {
                vm.dataAnalise = dataRetorno;
                vm.dataAnaliseAtual = angular.copy(vm.dataAnalise);
            });

        }

        vm.validarDataAnalise = function (dataAnalise) {
            if (!validar(dataAnalise)) {
                vm.dataAnalise = angular.copy(vm.dataAnaliseAtual);
                return;
            };
        };

        vm.salvarDataAnalise = function (dataAnalise) {
            if (!moment(vm.dataAnaliseAtual).isSame(dataAnalise)) {
                vm.isSaving = true;
                vm.updateDataAnalise(dataAnalise).then(function () {
                    vm.statusUpdate = true;
                    vm.statusError = false;
                    vm.dataAnaliseAtual = angular.copy(dataAnalise);
                }).catch(function () {
                    vm.statusUpdate = false;
                    vm.statusError = true;
                    vm.dataAnalise = angular.copy(vm.dataAnaliseAtual);
                }).finally(function () {
                    $timeout(function() { vm.isSaving = false;}, 600);
                });
            }

        };

        function buscarDataAnalise() {
            var cpf = $routeParams.cpf;
            return updateDadoDataAnalise
                .buscar(cpf)
                .then(function (retornoDataAnalise) {
                    var dataRetorno = moment(retornoDataAnalise.informacoesComplementares.dataAnalise);
                    return dataRetorno;
                })
                .catch(function () {
                    return moment();
                });

        }

        function updateDataAnalise(dataAnalise) {
            var cpf = $routeParams.cpf;

            var objUpdate = {
                informacoesComplementares: {
                    dataAnalise: dataAnalise
                }
            }

            return updateDadoDataAnalise
                .salvar(cpf, objUpdate);
        }

        function validar(data) {
            return moment(data, 'DD/MM/YYYY', true).isValid();
        }
    }

}());
