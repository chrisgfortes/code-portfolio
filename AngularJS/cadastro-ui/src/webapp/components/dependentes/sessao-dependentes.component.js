(function () {
    'use strict';

    sessaoDependentesController.$inject = ['dialogs', 'focus', '$routeParams'];

    angular
        .module('app')
        .component('sessaoDependentes', {
            transclude: true,
            bindings: {
                dependentes: '=',
                readonly: '<',
                mostraSessao: '<'
            },
            controller: sessaoDependentesController,
            templateUrl: './components/dependentes/sessao-dependentes.html'
        });

    function sessaoDependentesController(dialogs, focus, $routeParams) {
        var vm = this;

        vm.$onInit = onInit;
        vm.salvar = salvar;
        vm.cancelar = cancelar;
        vm.novo = novo;
        vm.remover = remover;
        vm.editar = editar;

        function onInit() {
            vm.mostrarDependenteForm = false;
            vm.dependentes = vm.dependentes || [];

            vm.dependentes.map(function (dependente) {
                dependente.dataNascimento = moment(dependente.dataNascimento);
                return dependente;
            });
        }

        function salvar(dependente) {
            var _dependente = angular.copy(dependente);

            if (validarCPF(_dependente.cpf)) {
                var notificacao = dialogs.notify('Atenção', 'O CPF já foi cadastrado');
                notificacao
                    .result
                    .then(function () {
                        focus('dependenteCPF');
                    });

                return false;
            }

            if (_dependente.modoEdicao) {
                atualizarListaDependente(_dependente)
            } else {
                vm.dependentes.push(_dependente);
            }

            vm.dependente = {};
            vm.mostrarDependenteForm = false;

            focus('cpfDependente');
        }

        function atualizarListaDependente(dependente) {
            vm.dependentes = vm.dependentes.map(function (item) {
                if (item.modoEdicao) {
                    dependente.modoEdicao = false;
                    return dependente;
                }
                return item;
            });
        }

        function cancelar() {
            vm.mostrarDependenteForm = false;

            vm.dependentes = vm.dependentes.map(function (dependente) {
                dependente.modoEdicao = false;

                return dependente;
            })

            vm.dependente = {};

            focus('novoDependente');
        }

        function novo() {
            vm.mostrarDependenteForm = true;

            focus('cpfDependente');
        }

        function remover(dependente) {
            var confirmacao = dialogs.confirm('Confirmar', "Confirma a remoção do dependente?");

            confirmacao
                .result
                .then(removerDaLista(dependente))
                .catch(desfazerModoExclusao)
                .finally(function () {
                    focus('novoDependente');
                });
        }

        function removerDaLista(contato) {
            return function (btn) {
                vm.dependentes = vm.dependentes.filter(modoEclusao);
            }
        }

        function modoEclusao(dependente) {
            return !dependente.modoExclusao;
        }

        function desfazerModoExclusao() {
            vm.dependentes = vm.dependentes.filter(desabilitarModoExlucao);
        }

        function desabilitarModoExlucao(dependente) {
            dependente.modoExclusao = false

            return dependente;
        }

        function editar(dependente) {
            vm.mostrarDependenteForm = true;

            vm.dependente = angular.copy(dependente);
        }

        function validarCPF(cpf) {
            return vm.dependentes.some(function (item) {
                return !item.modoEdicao && item.cpf == cpf;
            });
        }
    }
}());
