(function () {
    'use strict';

    dependenteController.$inject = ['dependente', 'moment'];

    angular
        .module('app')
        .component('dadosDependente', {
            bindings: {
                dependente: '<',
                readonly: '<',
                onSalvar: '&',
                onCancelar: '&'
            },
            controller: dependenteController,
            templateUrl: './components/dependentes/dados-dependentes.template.html'
        });

    function dependenteController(dependente, moment) {
        var vm = this;

        vm.$onInit = init;
        vm.salvar = salvar;
        vm.cancelar = cancelar;
        vm.validarData = validarData;

        function init() {
            vm.dependente = vm.dependente || {};
            vm.maxDataNascimento = moment();

            dependente
                .tipos()
                .then(function (res) {
                    vm.tiposDependencia = res.data;
                });
        }

        function salvar(dependente) {
            vm.onSalvar({ dependente: angular.copy(dependente) });

            vm.dependente = {};
        }

        function validarData (data) {
            if (!validar(data)) {
                vm.dependente.dataNascimento = {};
            }
        }

        function validar(data) {
            return moment(data, 'DD/MM/YYYY', true).isValid();
        }

        function cancelar() {
            vm.onCancelar();
        }
    };
}());
