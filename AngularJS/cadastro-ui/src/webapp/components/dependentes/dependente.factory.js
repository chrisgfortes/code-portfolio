(function(){
    'use strict';

    angular
        .module('app')
        .factory('dependente', dependente)

    /** @ngInject */
    function dependente($http, HOST){

        return {
            tipos: tipos
        }

        function tipos(){
            return $http.get(urlBase() + '/dependente/tipos', { cache: true });
        }

        function urlBase() {
            return HOST.pessoa + '/fisica';
        }
    }
}());
