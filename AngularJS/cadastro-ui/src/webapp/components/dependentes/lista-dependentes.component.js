(function () {
    'use strict';

    dependentesController.$inject = [];

    angular
        .module('app')
        .component('listaDependentes', {
            bindings: {
                dependentes: '<',
                readonly: '<',
                onEditar: '&',
                onNovo: '&',
                onRemover: '&',
                esconderBotaoNovo: '<'
            },
            controller: dependentesController,
            templateUrl: './components/dependentes/lista-dependentes.html'
        });

    function dependentesController() {
        var vm = this;

        vm.$onInit = init;
        vm.remover = remover;
        vm.novo = novo;
        vm.editar = editar;

        //////////////////////////////////////////////////

        function init() {
        }

        function remover(dependente) {
            dependente.modoExclusao = true;

            vm.onRemover({ dependente: angular.copy(dependente) });
        }

        function novo() {
            vm.onNovo();
        }

        function editar(dependente) {
            vm.dependentes = vm.dependentes.map(function (item) {
                item.modoEdicao = false;

                return item;
            });

            dependente.modoEdicao = true;

            vm.onEditar({ dependente: angular.copy(dependente) });
        }
    }
}());
