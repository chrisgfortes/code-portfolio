(function () {
    'use strict';

    angular
        .module('app')
        .service('dependenteConvert', dependenteConvert)

    /** @ngInject */
    function dependenteConvert() {

        this.post = post;

        function post(dependente) {
            var _dependente = angular.copy(dependente);

            _dependente.dataNascimento = _dependente.dataNascimento.toISOString();

            return _dependente;
        }
    }
}());
