(function () {
    'use strict';

    headerController.inject = ['AuthFactory', '$routeParams', '$rootScope', 'secao']

    angular
        .module('app')
        .component('uiHeader', {
            bindings: {
                displayToast: '<'
            },
            controller: headerController,
            templateUrl: './components/header/header.html'
        })
        .value('secao', {
            nome: ''
        });

    function headerController(AuthFactory, $routeParams, $rootScope, secao) {
        var vm = this;

        vm.$onInit = onInit;
        vm.logout = logout;
        vm.dismissToastError = dismissToastError;
        vm.dismissToastWarning = dismissToastWarning;
        vm.sidebar = sidebar;
        vm.fecharMensagem = fecharMensagem;
        vm.mensagem = {}

        $rootScope.$on('cpf-alterado', function (event, cpf) {
            vm.cpf = cpf;
        });

        $rootScope.$on('section-name', function (event, obj) {
            vm.section = obj.nome;
        });

        $rootScope.$on('display-toast-warning', function (event, mostrarToast) {
            vm.displayToastWarning = mostrarToast;
        });

        $rootScope.$on('mostrar-mensagem', function (event, data) {
            vm.mensagem = {
                titulo: data.titulo,
                descricao: data.descricao
            }
            vm.mostrarMensagem = true;
        });

        function fecharMensagem() {
            vm.mostrarMensagem = false;
        }

        function onInit() {
            vm.section = secao.nome;
        }

        function logout() {
            AuthFactory.logout();
        }

        function dismissToastError() {
            vm.displayToast = false;
            vm.dismissToastWarning();
            $rootScope.displayToast = false;
        }
        function dismissToastWarning() {
            vm.displayToastWarning = false;
        }

        function sidebar() {
            vm.isActive = !vm.isActive;
            $rootScope.$emit('sidebar-active', vm.isActive);
        }
    }
}());
