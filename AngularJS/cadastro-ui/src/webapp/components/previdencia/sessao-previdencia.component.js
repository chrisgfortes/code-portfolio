(function () {
    'use strict';

    sessaoPrevidenciaController.$inject = ['dialogs', 'moment', 'previdencia', 'focus'];


    angular
        .module('app')
        .component('sessaoPrevidencia', {
            transclude: true,
            bindings: {
                previdencias: '=',
                readonly: '<'
            },
            controller: sessaoPrevidenciaController,
            templateUrl: './components/previdencia/sessao-previdencia.html'
        });

    function sessaoPrevidenciaController(dialogs, moment, previdencia, focus) {
        var vm = this;

        vm.salvarPrevidencia = salvarPrevidencia;
        vm.cancelarPrevidencia = cancelarPrevidencia;
        vm.editarPrevidencia = editarPrevidencia;
        vm.novaPrevidencia = novaPrevidencia;
        vm.removerPrevidencia = removerPrevidencia;
        vm.previdencia = {};
        vm.previdenciaEdicao = {};
        vm.mostrarPrevidenciaForm = false;
        vm.$onInit = onInit;

        function onInit() {
            vm.previdencias = vm.previdencias || [];
            previdencia
                .tipos()
                .then(function (res) {
                    vm.tiposPrevidencia = res.data;
                    vm.previdencias = vm.previdencias.map(function (previdencia) {
                        previdencia.descricaoTipoDePrevidencia = vm.tiposPrevidencia.filter(function (tipoPrevidencia) {
                            return tipoPrevidencia.valor == previdencia.tipoPlano;
                        })[0];
                        previdencia.descricaoTipoDePrevidencia = previdencia.descricaoTipoDePrevidencia.descricao;
                        return previdencia;
                    });
                });
            previdencia
                .instituicoes()
                .then(function (res) {
                    vm.instituicoesPrevidencia = res.data;
                    vm.previdencias = vm.previdencias.map(function (previdencia) {
                        previdencia.descricaoInstituicao = vm.instituicoesPrevidencia.filter(function (instituicao) {
                            return instituicao.valor == previdencia.instituicao;
                        })[0];
                        previdencia.descricaoInstituicao = previdencia.descricaoInstituicao.descricao;
                        return previdencia;
                    });
                });
        }

        function salvarPrevidencia(previdencia) {
            var previdencia = angular.copy(previdencia);

            if (previdencia.modoEdicao) {
                atualizarListaPrevidencias(previdencia)
            } else {
                vm.previdencias.push(previdencia);
            }

            vm.previdencia = {};
            vm.mostrarPrevidenciaForm = false;
            focus('novaPrevidencia');
        }

        function cancelarPrevidencia() {
            vm.previdencia = {};

            vm.previdencias = vm.previdencias.map(function (p) {
                p.modoEdicao = false;
                return p;
            });

            vm.mostrarPrevidenciaForm = false;
            focus('novaPrevidencia');
        }

        function editarPrevidencia(previdencia) {
            vm.mostrarPrevidenciaForm = true;
            vm.previdencia = angular.copy(previdencia);
            focus('tipoPrevidencia');
        }

        function novaPrevidencia() {
            vm.mostrarPrevidenciaForm = true;
            focus('tipoPrevidencia');
        }

        function atualizarListaPrevidencias(previdencia) {
            vm.previdencias = vm.previdencias.map(function (item) {
                if (item.modoEdicao) {
                    previdencia.modoEdicao = false;
                    return previdencia;
                }
                return item;
            });
        }

        function removerPrevidencia() {
            var confirmacao = dialogs.confirm('Confirmar', 'Confirma a remoção do plano de saúde?');
            confirmacao
                .result
                .then(function (btn) {
                    vm.previdencias = vm.previdencias.filter(function (previdencia) {
                        return !previdencia.modoExclusao;
                    });
                })
                .catch(function () {
                    vm.previdencias = vm.previdencias.map(function (previdencia) {
                        previdencia.modoExclusao = false;
                        return previdencia;
                    });
                });
            focus('novaPrevidencia');
        }
    }
}());
