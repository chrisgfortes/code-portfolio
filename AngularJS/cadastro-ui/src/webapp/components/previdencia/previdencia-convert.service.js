(function () {
    'use strict';

    angular
        .module('app')
        .service('previdenciaConvert', previdenciaConvert)

    function previdenciaConvert(moment) {

        this.convertGet = convertGet;
        this.convertPost = convertPost;

        function convertGet(previdencias) {
            return previdencias.map(function (previdencia) {
                if (!!previdencia.dataInicioContribuicao) {
                    previdencia.dataInicioContribuicao = moment(previdencia.dataInicioContribuicao);
                }
                return previdencia;
            })
        }

        function convertPost(previdencias) {
            return previdencias.map(function (previdencia) {
                delete previdencia.descricaoTipoDePrevidencia;
                delete previdencia.descricaoInstituicao;
                if (!!previdencia.mesesContribuicao) {
                    delete previdencia.mesesContribuicao;
                }
                return previdencia;
            })
        }
    }

}());
