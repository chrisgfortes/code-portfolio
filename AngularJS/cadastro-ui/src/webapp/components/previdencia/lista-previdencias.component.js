(function () {
    'use strict';

    listaPrevidenciasController.$inject = ['$rootScope', 'dialogs'];

    angular
        .module('app')
        .component('listaPrevidencias', {
            bindings: {
                previdencias: '<',
                readonly: '<',
                onEditar: '&',
                onNovo: '&',
                onRemover: '&',
                escondeNovo: '<'
            },
            controller: listaPrevidenciasController,
            templateUrl: './components/previdencia/lista-previdencias.html'
        });

    function listaPrevidenciasController($rootScope, dialogs) {
        var vm = this;

        vm.remover = remover;
        vm.editar = editar;
        vm.novo = novo;
        vm.tiposPlanoSaude = [];
        vm.instituicoesPrevidencia = [];
        vm.$onInit = onInit;

        function onInit() {
            vm.previdencias = vm.previdencias || [];
        }

        function novo () {
            vm.onNovo();
        }

        function remover(previdencia) {
            previdencia.modoExclusao = true;
            vm.onRemover({ previdencia: angular.copy(previdencia) });
        }

        function editar(index) {
            vm.previdencias = vm.previdencias.map(function (p) {
                p.modoEdicao = false;
                return p;
            })

            var previdencia = vm.previdencias[index];
            previdencia.modoEdicao = true;

            vm.onEditar({ previdencia: previdencia });
        }
    }
}());
