(function(){
    'use strict';

    angular
        .module('app')
        .factory('previdencia', previdencia)

    /** @ngInject */
    function previdencia($http, HOST){

        return {
            tipos: tipos,
            instituicoes: instituicoes,
        }

        function tipos(){
            return $http.get(urlBase() + '/tipos');
        }

        function instituicoes () {
            return $http.get(urlBase() + '/instituicoes');
        }

        function urlBase () {
            return HOST.pessoa + '/fisica/previdencia';
        }
    }

}());