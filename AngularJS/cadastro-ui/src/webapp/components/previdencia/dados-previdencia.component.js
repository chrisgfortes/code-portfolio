(function () {
    'use strict';

    previdenciaController.$inject = ['moment', '$rootScope', 'previdencia'];

    angular
        .module('app')
        .component('dadosPrevidencia', {
            bindings: {
                previdencia: '<',
                onSalvar: '&',
                onCancelar: '&'
            },
            controller: previdenciaController,
            templateUrl: './components/previdencia/dados-previdencia.html'
        });

    function previdenciaController(moment, $rootScope, previdencia) {
        var vm = this;
        vm.dataMaximaInicioContribuicao = moment();
        vm.$onInit = onInit;
        vm.salvar = salvar;
        vm.cancelar = cancelar;
        vm.validarDataECalcularMeses = validarDataECalcularMeses;
        vm.tiposPrevidencia = [];
        vm.instituicoesPrevidencia = [];

        function onInit () {

            previdencia
                .tipos()
                .then(function (res) {
                    vm.tiposPrevidencia = res.data;
                });

            previdencia
                .instituicoes()
                .then(function (res) {
                    vm.instituicoesPrevidencia = res.data;
                });

        }

        function calcularTempoDeContribuicao(data) {
            vm.previdencia.mesesContribuicao = moment().diff(data, 'months');
        }

        function validarDataECalcularMeses (data) {
            if (validar(data)) {
                calcularTempoDeContribuicao(data);
            } else {
                vm.previdencia.dataInicioContribuicao = undefined;
                vm.previdencia.mesesContribuicao = undefined;
            }
        }

        function validar(data) {
            return moment(data, 'DD/MM/YYYY', true).isValid();
        }

        function salvar () {
            vm.previdencia.descricaoTipoDePrevidencia = vm.tiposPrevidencia.filter(function (tipoPrevidencia) {
                return tipoPrevidencia.valor == vm.previdencia.tipoPlano;
            })[0];
            vm.previdencia.descricaoTipoDePrevidencia = vm.previdencia.descricaoTipoDePrevidencia.descricao;

            vm.previdencia.descricaoInstituicao = vm.instituicoesPrevidencia.filter(function (instituicao) {
                return instituicao.valor == vm.previdencia.instituicao;
            })[0];
            vm.previdencia.descricaoInstituicao = vm.previdencia.descricaoInstituicao.descricao;

            vm.onSalvar({previdencia: angular.copy(vm.previdencia)});
        }

        function cancelar () {
            vm.onCancelar();
        }
    }
}());
