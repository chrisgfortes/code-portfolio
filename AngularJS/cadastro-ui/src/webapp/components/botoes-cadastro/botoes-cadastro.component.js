(function () {
    'use strict';

    botoesCadastroController.$inject = ['$location', '$routeParams'];

    angular
        .module('app')
        .component('botoesCadastro', {
            bindings: {
                model: "<",
                onSalvarRascunho: '&',
                links: '<'
            },
            controller: botoesCadastroController,
            templateUrl: './components/botoes-cadastro/botoes-cadastro.html'
        });

    function botoesCadastroController($location, $routeParams) {

        var vm = this;

        vm.$onInit = init;
        vm.salvarRascunho = salvarRascunho;
        vm.proximo = proximo;

        function init() {
            vm.linksFiltrado = vm.links.filter(ativo);
        }

        function proximo(model) {
            if (model != null) {
                vm.salvarRascunho(model).then(function() {
                    proximoPagina();
                });
            } else{
                proximoPagina();
            }
        }

        function salvarRascunho(model) {
            return vm.onSalvarRascunho({ model: angular.copy(model) });
        }

        function proximoPagina() {
            var cpf = $routeParams.cpf;
            var indexAtual = 0;
            var path = '#' + $location.path();

            vm.linksFiltrado
                .some(function (link, index) {
                    indexAtual = index;

                    return link.url(cpf).indexOf(path) != -1;
                });

            var proximoLink = vm.linksFiltrado[indexAtual + 1];

            $location.path(proximoLink.url(cpf).replace('#', ''));
        }

        function ativo(link) {
            return link.ativo;
        }
    };
}());
