(function() {
    'use strict';

    pessoaResponsavelController.$inject = ['pessoaResponsavelConvert'];

    angular
        .module('app')
        .component('pessoaResponsavel', {
            bindings: {
                pessoaResponsavel: '=',
                readonly: '<'
            },
            controller: pessoaResponsavelController,
            templateUrl: './components/pessoa-responsavel/pessoa-responsavel.html'
        });

    function pessoaResponsavelController(pessoaResponsavelConvert) {
        var vm = this;

        vm.$onInit = init;

        function init() {
            var pesRespPadrao = {
                possuiAutorizacaoResidenciaValidaExterior: false,
                possuiDomicilioFiscalAlemDeclarado: false,
                possuiGreenCardEParticipaEmpresaCapitalSuperior10PorCento: false,
                usPerson: false
            };
            vm.pessoaResponsavel = pessoaResponsavelConvert.convertGet(vm.pessoaResponsavel || pesRespPadrao);

        }
    }
}());
