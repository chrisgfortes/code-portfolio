(function () {
    'use strict';

    angular
        .module('app')
        .service('pessoaResponsavelConvert', pessoaResponsavelConvert)

    function pessoaResponsavelConvert() {

        this.convertGet = convertGet;
        this.convertPost = convertPost;

        function convertGet(pessoaResponsavel) {
            var residenciaExt = pessoaResponsavel.possuiAutorizacaoResidenciaValidaExterior;
            pessoaResponsavel.possuiAutorizacaoResidenciaValidaExterior = (residenciaExt) ? "SIM" : "NAO";
            var domFiscal = pessoaResponsavel.possuiDomicilioFiscalAlemDeclarado;
            pessoaResponsavel.possuiDomicilioFiscalAlemDeclarado = (domFiscal) ? "SIM" : "NAO";
            var greencard = pessoaResponsavel.possuiGreenCardEParticipaEmpresaCapitalSuperior10PorCento;
            pessoaResponsavel.possuiGreenCardEParticipaEmpresaCapitalSuperior10PorCento = (greencard) ? "SIM" : "NAO";
            var usPerson = pessoaResponsavel.usPerson;
            pessoaResponsavel.usPerson = (usPerson) ? "SIM" : "NAO";

            return pessoaResponsavel;
        }

        function convertPost(pessoaResponsavel) {
            var residenciaExt = pessoaResponsavel.possuiAutorizacaoResidenciaValidaExterior;
            pessoaResponsavel.possuiAutorizacaoResidenciaValidaExterior = (residenciaExt == "SIM") ? true : false;
            var domFiscal = pessoaResponsavel.possuiDomicilioFiscalAlemDeclarado;
            pessoaResponsavel.possuiDomicilioFiscalAlemDeclarado = (domFiscal == "SIM") ? true : false;
            var greencard = pessoaResponsavel.possuiGreenCardEParticipaEmpresaCapitalSuperior10PorCento;
            pessoaResponsavel.possuiGreenCardEParticipaEmpresaCapitalSuperior10PorCento = (greencard == "SIM") ? true : false;
            var usPerson = pessoaResponsavel.usPerson;
            pessoaResponsavel.usPerson = (usPerson == "SIM") ? true : false;

            return pessoaResponsavel;
        }
    }

}());
