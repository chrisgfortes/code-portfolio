(function () {
    'use strict';

    angular
        .module('app')
        .component('sessaoResumo', {
            transclude: true,
            bindings: {
                titulo: '@',
                isLast: '<'
            },
            controller: sessaoResumoController,
            templateUrl: './components/sessao-resumo/sessao-resumo.html'
        });

    function sessaoResumoController() {
        var vm = this;

        vm.mostrar = function () {
            vm.mostraSessao = !vm.mostraSessao;
        }

        vm.$onInit = function () {
            vm.titulo = '';
            vm.mostraSessao = false;
        }
    }
}());
