(function () {
    'use strict';

    angular
        .module('app')
        .service('referenciaConvert', referenciaConvert)

    function referenciaConvert() {

        this.convertGet = convertGet;
        this.convertPost = convertPost;

        function convertGet(referencias) {
            return referencias.map(function (ref) {
                if(ref.telefone.ddd && ref.telefone.numero) {
                    ref.telefone.numero = ref.telefone.ddd + ref.telefone.numero;
                }
                return ref;
            });
        }

        function convertPost(referencias) {
            return referencias.map(function (ref) {
                if(ref.telefone.numero) {
                    ref.telefone.ddd = ref.telefone.numero.substr(0, 2);
                    ref.telefone.numero = ref.telefone.numero.split(ref.telefone.ddd).pop();
                }

                return ref;
            });
        }
    }

}());