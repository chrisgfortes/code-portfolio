(function () {
    'use strict';

    listaReferenciaController.$inject = [];

    angular
        .module('app')
        .component('listaReferencia', {
            bindings: {
                referencias: '<',
                onEditar: '&',
                onNovo: '&',
                onRemover: '&',
                esconderBotaoNovo: '=',
                readonly: '<'
            },
            controller: listaReferenciaController,
            templateUrl: './components/referencia/lista-referencia.html'
        });

    function listaReferenciaController() {
        var vm = this;

        vm.$onInit = onInit;
        vm.remover = remover;
        vm.editar = editar;
        vm.novo = novo;

        function onInit() {
            vm.referencias = vm.referencias || [];
        }

        function novo () {
            vm.onNovo();
        }

        function remover(referencia) {
            referencia.modoExclusao = true;
            vm.onRemover({ referencia: angular.copy(referencia) });
        }

        function editar(referencia) {
            vm.referencias = vm.referencias.map(function (p) {
                p.modoEdicao = false;
                return p;
            });

            referencia.modoEdicao = true;

            vm.onEditar({ referencia: angular.copy(referencia) });
        }
    }
}());
