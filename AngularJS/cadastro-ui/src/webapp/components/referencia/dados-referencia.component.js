(function () {
    'use strict';

    participacaoSocietariaController.$inject = [];

    angular
        .module('app')
        .component('dadosReferencia', {
            bindings: {
                referencia: '<',
                onSalvar: '&',
                onCancelar: '&'
            },
            controller: participacaoSocietariaController,
            templateUrl: './components/referencia/dados-referencia.html'
        });

    function participacaoSocietariaController() {
        var vm = this;

        vm.$onInit = onInit;
        vm.salvar = salvar;
        vm.cancelar = cancelar;

        function onInit() {
        }

        function salvar() {
            var referencia = angular.copy(vm.referencia);

            vm.onSalvar({ referencia: referencia });

            vm.referencia = {};
        }

        function cancelar() {
            vm.onCancelar();

            vm.referencia = {};
        }
    }
}());