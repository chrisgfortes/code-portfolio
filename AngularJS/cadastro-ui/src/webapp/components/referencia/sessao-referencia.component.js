(function () {
    'use strict';

    referenciaController.$inject = ['focus', 'dialogs'];

    angular
        .module('app')
        .component('sessaoReferencia', {
            bindings: {
                referencias: '=',
                readonly: '<'
            },
            controller: referenciaController,
            templateUrl: './components/referencia/sessao-referencia.html'
        });

    function referenciaController(focus, dialogs) {
        var vm = this;

        vm.mostrarReferenciaForm = false;
        vm.$onInit = onInit;
        vm.salvar = salvar;
        vm.cancelar = cancelar;
        vm.editar = editar;
        vm.remover = remover;
        vm.novo = novo;

        function onInit() {
            vm.referencias = vm.referencias || [];
        }

        function salvar(referencia) {
            if (referencia.modoEdicao) {
                vm.referencias = vm.referencias.map(function (ref) {
                    if(ref.modoEdicao){
                        referencia.modoEdicao = false;
                        return referencia;
                    }

                    return ref;
                });

            } else {
                vm.referencias.push(referencia);
            }

            vm.mostrarReferenciaForm = false;
        }

        function cancelar() {
            vm.mostrarReferenciaForm = false;

            vm.referencias = vm.referencias.map(function (ref) {
                ref.modoEdicao = false;

                return ref;
            });
        }

        function editar(referencia) {
            vm.mostrarReferenciaForm = true;
            vm.referencia = angular.copy(referencia);
        }

        function remover(referencia) {
            var confirmacao = dialogs.confirm('Confirmar', 'Confirma a remoção da referência?');

            confirmacao
                .result
                .then(function (btn) {
                    vm.referencias = vm.referencias.filter(function (ref) {
                        return !ref.modoExclusao;
                    });
                })
                .catch(function () {
                    vm.referencias = vm.referencias.map(function (referencia) {
                        referencia.modoExclusao = false;
                        return referencia;
                    });
                });
        }

        function novo() {
            vm.mostrarReferenciaForm = true;

            focus('referenciaBancoEmpresa');
        }
    }
}());
