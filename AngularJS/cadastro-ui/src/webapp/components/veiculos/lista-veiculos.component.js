(function() {
    'use strict';

    listaVeiculoController.$inject = ['veiculoConvert'];

    angular
        .module('app')
        .component('listaVeiculos', {
            bindings: {
                veiculos: '=',
                readonly: '<',
                onEditar: '&',
                onVisualizar: '&',
                onNovo: '&',
                onRemover: '&',
                esconderBotaoNovo: '<'
            },
            controller: listaVeiculoController,
            templateUrl: './components/veiculos/lista-veiculos.template.html'
        });

    function listaVeiculoController(veiculoConvert) {
        var vm = this;

        vm.$onInit = init;
        vm.novo = novo;
        vm.editar = editar;
        vm.visualizar = visualizar;
        vm.remover = remover;

        function init() {
            vm.veiculos = vm.veiculos || [];
        }

        function novo() {
            vm.onNovo();
        }

        function editar(veiculo) {
            vm.veiculos = vm.veiculos.map(limpaModoEdicaoVisualizacao);

            veiculo.modoEdicao = true;

            vm.onEditar({
                veiculo: angular.copy(veiculo)
            });
        }

        function visualizar(veiculo) {
            vm.veiculos = vm.veiculos.map(limpaModoEdicaoVisualizacao);

            veiculo.modoVisualizacao = true;

            vm.onVisualizar({
                veiculo: angular.copy(veiculo)
            });
        }

        function limpaModoEdicaoVisualizacao(_veiculo) {
            _veiculo.modoEdicao = false;
            _veiculo.modoVisualizacao = false;
            return _veiculo;
        }

        function remover(veiculo) {
            veiculo.modoExclusao = true;

            vm.onRemover({
                veiculo: angular.copy(veiculo)
            });
        }

    }
}());
