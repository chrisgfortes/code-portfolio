(function() {
    'use strict';

    dadosVeiculoController.$inject = ['moment', 'veiculo'];

    angular
        .module('app')
        .component('dadosVeiculo', {
            bindings: {
                veiculo: '<',
                apenasLeitura: '<',
                onSalvar: '&',
                onCancelar: '&'
            },
            controller: dadosVeiculoController,
            templateUrl: './components/veiculos/dados-veiculo.template.html'
        });

    function dadosVeiculoController(moment, veiculo) {
        var vm = this;

        vm.$onInit = init;
        vm.salvar = salvar;
        vm.cancelar = cancelar;
        vm.maxDataFabricacao = moment().year();

        function init() {
            veiculo
                .tipos()
                .then(function(res) {
                    vm.tipos = res.data;
                });

            veiculo
                .situacoes()
                .then(function(res) {
                    vm.situacoes = res.data;
                });
        }

        function salvar() {
            var descricaoTipoVeiculo = vm.tipos.filter(function (tipoVeiculo) {
                return tipoVeiculo.valor == vm.veiculo.tipoVeiculo;
            })[0];
            vm.veiculo.descricaoTipoDeVeiculo = descricaoTipoVeiculo.descricao;

            vm.onSalvar({
                veiculo: angular.copy(vm.veiculo)
            });
        }

        function cancelar() {
            vm.onCancelar();
        }
    }
}());
