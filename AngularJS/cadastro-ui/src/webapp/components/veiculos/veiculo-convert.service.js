(function() {
    'use strict';

    angular
        .module('app')
        .service('veiculoConvert', veiculoConvert)

    function veiculoConvert() {

        this.convertPost = convertPost;
        this.convertGet = convertGet;
        var vm = this;

        function convertPost(veiculos) {
            return veiculos.map(function(veiculo) {
                if (!!veiculo.anoFabricacao)
                    veiculo.anoFabricacao = veiculo.anoFabricacao.year();

                if (!!veiculo.anoModelo)
                    veiculo.anoModelo = veiculo.anoModelo.year();

                return veiculo;
            })
        }

        function convertGet(veiculos) {
            return veiculos.map(function(veiculo) {
                veiculo.anoFabricacao = moment(veiculo.anoFabricacao, 'YYYY-MM-DD');

                if (!!veiculo.anoModelo)
                    veiculo.anoModelo = moment(veiculo.anoModelo, 'YYYY-MM-DD');

                return veiculo;
            })
        }

    }

}());
