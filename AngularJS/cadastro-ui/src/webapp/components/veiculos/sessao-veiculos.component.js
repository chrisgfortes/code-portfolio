(function() {
    'use strict';

    sessaoVeiculosController.$inject = ['$rootScope', '$routeParams', 'dialogs', 'veiculo', 'veiculoConvert', 'veiculosUpdate'];

    angular
        .module('app')
        .component('sessaoVeiculos', {
            transclude: true,
            bindings: {
                veiculos: '=',
                isUpdate: '<',
                readonly: '<',
                mostraSessao: '<'
            },
            controller: sessaoVeiculosController,
            templateUrl: './components/veiculos/sessao-veiculos.html'
        });

    function sessaoVeiculosController($rootScope, $routeParams, dialogs, veiculo, veiculoConvert, veiculosUpdate) {
        var vm = this;

        vm.$onInit = onInit;
        vm.novo = novo;
        vm.cancelar = cancelar;
        vm.salvar = salvar;
        vm.editar = editar;
        vm.visualizar = visualizar;
        vm.remover = remover;
        vm.mostrarVeiculoForm = false;

        function onInit() {
            vm.veiculos = veiculoConvert.convertGet(vm.veiculos || []);

            veiculo
                .tipos()
                .then(function(res) {
                    vm.tipos = res.data;
                    vm.veiculos = vm.veiculos.map(adicionaDescricaoTipoDeVeiculo)
                });
        }

        function adicionaDescricaoTipoDeVeiculo (veiculo) {
            var descricaoTipoDeVeiculo = vm.tipos.filter(filtrarVeiculoSelecionado(veiculo))[0] || vm.tipos[0];
            veiculo.descricaoTipoDeVeiculo = descricaoTipoDeVeiculo.descricao;
            veiculo.tipoVeiculo = veiculo.tipoVeiculo ||  descricaoTipoDeVeiculo.valor;
            return veiculo;
        }

        function filtrarVeiculoSelecionado(veiculo) {
            return function (tipoVeiculo) {
                return tipoVeiculo.valor == veiculo.tipoVeiculo;
            }
        }

        function novo() {
            vm.mostrarVeiculoForm = true;
        }

        function salvar(veiculo) {
            var _veiculo = angular.copy(veiculo);

            if (_veiculo.modoEdicao) {
                atualizarLista(_veiculo);
            } else {
                vm.veiculos.push(_veiculo);
            }

            vm.veiculo = {};
            vm.mostrarVeiculoForm = false;
            vm.apenasLeitura = false;
        }

        function cancelar() {
            vm.mostrarVeiculoForm = false;

            vm.veiculo = vm.veiculos.map(function(_veiculo) {
                _veiculo.modoEdicao = false;
                _veiculo.modoVisualizacao = false;

                return _veiculo;
            });

            vm.veiculo = {};
            vm.apenasLeitura = false;

            focus('novoVeiculo');
        }

        function editar(veiculo) {
            vm.mostrarVeiculoForm = true;
            vm.apenasLeitura = false;
            vm.veiculo = angular.copy(veiculo);
        }

        function visualizar(veiculo) {
            vm.mostrarVeiculoForm = true;
            vm.apenasLeitura = true;
            vm.veiculo = angular.copy(veiculo);
        }

        function remover(veiculo) {
            var confirmacao = dialogs.confirm('Confirmar', "Confirma a remoção do veículo?");

            confirmacao
                .result
                .then(removerDaLista(veiculo))
                .catch(desfazerModoExclusao)
                .finally(function () {
                    focus('novoVeiculo');
                });
        }

        function removerDaLista(veiculo) {
            if (vm.isUpdate && !!veiculo.id) {
                return function () {
                    veiculosUpdate
                        .excluir($routeParams.cpf, veiculo.id)
                        .then(function () {
                            vm.veiculos = vm.veiculos.filter(modoExclusao);
                        });
                }
            } else {
                return function () {
                    vm.veiculos = vm.veiculos.filter(modoExclusao);
                }
            }
        }

        function modoExclusao(item) {
            return !item.modoExclusao;
        }

        function desfazerModoExclusao() {
            vm.veiculos = vm.veiculos.filter(desabilitarModoExlucao);
        }

        function desabilitarModoExlucao(item) {
            item.modoExclusao = false

            return item;
        }

        function atualizarLista(veiculo) {
            vm.veiculos = vm.veiculos.map(function(item) {
                if (item.modoEdicao) {
                    veiculo.modoEdicao = false;
                    return veiculo;
                }
                return item;
            });
        }

        $rootScope.$on('atualizar-descricao-tipo-veiculo', function (e, veiculos) {
            vm.veiculos = veiculos.map(adicionaDescricaoTipoDeVeiculo);
        });

    }
}());
