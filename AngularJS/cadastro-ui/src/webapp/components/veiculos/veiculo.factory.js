(function () {
    'use strict';

    angular
        .module('app')
        .factory('veiculo', veiculo)

    /** @ngInject */
    function veiculo($http, HOST) {

        return {
            tipos: tipos,
            situacoes: situacoes
        }

        function tipos() {
            return $http.get(urlBase() + '/veiculo/tipos');
        }

        function situacoes() {
            return $http.get(urlBase() + '/veiculo/situacoes');
        }

        function urlBase() {
            return HOST.pessoa + '/fisica';
        }
    }
}());