(function () {
    'use strict';

    angular
        .module('app')
        .factory('restricaoAcatada', restricaoAcatada)

    /** @ngInject */
    function restricaoAcatada($http, HOST) {

        return {
            buscar: buscar
        }

        function buscar() {
            return $http.get(HOST.pessoa + '/analise-cadastral/restricoes-acatadas', { cache: true });
        }
    }

}());