(function () {
    'use strict';

    restricaoAcatadaController.$inject = ['restricaoAcatada', '$rootScope', 'dialogs', 'AuthFactory', 'moment'];

    angular
        .module('app')
        .component('dadosRestricaoAcatada', {
            bindings: {
                restricaoAcatada: '<',
                nomeComponente: '@',
                onSalvar: '&',
                onCancelar: '&'
            },
            controller: restricaoAcatadaController,
            templateUrl: './components/restricao-acatada/dados-restricao-acatada.html'
        });

    function restricaoAcatadaController(restricaoAcatada, $rootScope, dialogs, AuthFactory, moment) {
        var vm = this;

        vm.$onInit = onInit;
        vm.salvar = salvar;
        vm.cancelar = cancelar;

        function onInit() {
            vm.isRequired = false;
            vm.restricao = {
                usuario: AuthFactory.getUsuario().nome
            };

            restricaoAcatada
                .buscar()
                .then(function (res) {
                    vm.tipoRestricao = res.data;
                });
        }

        function salvar(restricaoAcatada) {
            var restricaoAcatada = angular.copy(vm.restricaoAcatada);

            vm.onSalvar({ restricaoAcatada: restricaoAcatada });

            vm.restricaoAcatada = {};
        }

        function cancelar() {
            vm.onCancelar();

            vm.restricaoAcatada = {};
        }
    }
}());
