(function () {
    'use strict';

    angular
        .module('app')
        .service('restricoesAcatadasConvert', restricoesAcatadasConvert)

    function restricoesAcatadasConvert(moment) {

        this.convertGet = convertGet;

        function convertGet(restricoesAcatadas) {
            angular.forEach(restricoesAcatadas, function (restricao, index) {
                restricoesAcatadas[index].data = moment(restricao.data);
            });
            return restricoesAcatadas;
        }
    }

}());
