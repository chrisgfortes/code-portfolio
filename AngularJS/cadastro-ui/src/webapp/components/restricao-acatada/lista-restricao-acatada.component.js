(function () {
    'use strict';

    restricoesListaController.$inject = ['$rootScope', 'dialogs', 'moment'];

    angular
        .module('app')
        .component('listaRestricaoAcatada', {
            bindings: {
                restricoesAcatadas: '=',
                readonly: '<',
                onEditar: '&',
                onNovo: '&',
                onRemover: '&',
                esconderBotaoNovo: '='
            },
            controller: restricoesListaController,
            templateUrl: './components/restricao-acatada/lista-restricao-acatada.html'
        });

    function restricoesListaController($rootScope, dialogs, moment) {
        var vm = this;

        vm.$onInit = onInit;
        vm.remover = remover;
        vm.editar = editar;
        vm.novo = novo;

        function onInit() {
            vm.restricoesAcatadas = restricoesAcatadasConverter(vm.restricoesAcatadas || []);
        }

        function novo() {
            vm.onNovo();
        }

        function remover(restricaoAcatada) {
            restricaoAcatada.modoExclusao = true;
            vm.onRemover({ restricaoAcatada: angular.copy(restricaoAcatada) });
        }

        function editar(restricaoAcatada) {
            vm.restricoesAcatadas = vm.restricoesAcatadas.map(function (p) {
                p.modoEdicao = false;
                return p;
            });

            restricaoAcatada.modoEdicao = true;

            vm.onEditar({ restricaoAcatada: angular.copy(restricaoAcatada) });
        }

        function restricoesAcatadasConverter(ra) {
            if (!!ra) {
                angular.forEach(ra, function(restricao, index) {
                    ra[index].data = moment(restricao.data);
                });
            }

            return ra;
        }
    }
}());
