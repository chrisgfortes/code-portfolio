(function () {
    'use strict';

    sessaoRestricaoAcatadaController.$inject = ['focus', 'dialogs', 'restricoesAcatadasConvert'];

    angular
        .module('app')
        .component('sessaoRestricaoAcatada', {
            bindings: {
                restricoesAcatadas: '=',
                readonly: '<'
            },
            controller: sessaoRestricaoAcatadaController,
            templateUrl: './components/restricao-acatada/sessao-restricao-acatada.html'
        });

    function sessaoRestricaoAcatadaController(focus, dialogs, restricoesAcatadasConvert) {
        var vm = this;

        vm.mostrarRestricaoAcatadaForm = false;
        vm.$onInit = onInit;
        vm.salvar = salvar;
        vm.cancelar = cancelar;
        vm.editar = editar;
        vm.remover = remover;
        vm.novo = novo;

        function onInit() {
            vm.restricoesAcatadas = restricoesAcatadasConvert.convertGet(vm.restricoesAcatadas) || [];
        }

        function salvar(restricaoAcatada) {
            if (restricaoAcatada.modoEdicao) {
                vm.restricoesAcatadas = vm.restricoesAcatadas.map(function (ref) {
                    if(ref.modoEdicao){
                        restricaoAcatada.modoEdicao = false;
                        return restricaoAcatada;
                    }

                    return ref;
                });

            } else {
                vm.restricoesAcatadas.push(restricaoAcatada);
            }

            vm.mostrarRestricaoAcatadaForm = false;

            focus('novaRestricoesAcatadas');
        }

        function cancelar() {
            vm.restricaoAcatada = {};
            vm.restricoesAcatadas = vm.restricoesAcatadas.map(function (ref) {
                ref.modoEdicao = false;

                return ref;
            });

            vm.mostrarRestricaoAcatadaForm = false;
            focus('novaRestricoesAcatadas');
        }

        function editar(restricaoAcatada) {
            vm.mostrarRestricaoAcatadaForm = true;
            vm.restricaoAcatada = angular.copy(restricaoAcatada);
        }

        function remover(restricaoAcatada) {
            var confirmacao = dialogs.confirm('Confirmar', 'Confirma a remoção da Restrição Acatada?');

            confirmacao
                .result
                .then(function (btn) {
                    vm.restricoesAcatadas = vm.restricoesAcatadas.filter(function (ref) {
                        return !ref.modoExclusao;
                    });
                }).catch(function () {
                    vm.restricoesAcatadas = vm.restricoesAcatadas.map(function (restricao) {
                        restricao.modoExclusao = false;
                        return restricao;
                    });
                });
        }

        function novo() {
            vm.mostrarRestricaoAcatadaForm = true;

            focus('restricaoAcatadaData');
        }
    }
}());
