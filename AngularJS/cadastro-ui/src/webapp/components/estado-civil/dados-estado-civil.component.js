(function () {
    'use strict';

    estadoCivilController.$inject = [
        'cepFactory',
        'dialogs',
        'focus',
        'dadosProfissional',
        'documento',
        'pessoa',
        'conjugeConvert'
    ];

    angular
        .module('app')
        .component('dadosEstadoCivil', {
            bindings: {
                estadoCivil: '=',
                conjuge: '=',
                regimeCasamento: '=',
                readonly: '<'
            },
            controller: estadoCivilController,
            templateUrl: './components/estado-civil/dados-estado-civil.html'
        });


    function estadoCivilController(cepFactory, dialogs, focus, dadosProfissional, documento, pessoa, conjugeConvert) {
        var vm = this;
        vm.orgaosExpeditores = [];

        vm.$onInit = init;
        vm.buscarOrgaosExpeditores = buscarOrgaosExpeditores;
        vm.alterarEstadoCivil = alterarEstadoCivil;
        vm.maxDataNascimento = moment();
        vm.maxDataEmissao = moment();
        vm.atribuirResultado = atribuirResultado;

        function init() {
            carregarListas();
        }

        function atribuirResultado(model) {
            vm.conjuge = conjugeConvert.get(model);

            !vm.conjuge.tipoIdentificacao || vm.buscarOrgaosExpeditores(vm.conjuge.tipoIdentificacao);

            if (model.notFound){
                delete vm.conjuge.notFound;

                var objContatosPadrao = {
                    telefones: [],
                    emails: []
                }
                vm.conjuge.contatos = objContatosPadrao;
            }
        }

        function buscarOrgaosExpeditores(id) {
            documento
                .orgaoemissor(id)
                .then(function (res) {
                    vm.orgaosExpeditores = res.data;
                });
        }

        function validar(data) {
            return moment(data, 'DD/MM/YYYY', true).isValid();
        }

        vm.validarDataNascimento = function (dataNascimento, dataEmissao) {
            if (!validar(dataNascimento)) {
                vm.conjuge.dataNascimento = null;
                return;
            }

            if (dataNascimento.isAfter(moment().subtract(16, 'years'))) {

                var notificacao = dialogs.notify('Atenção', 'O conjuge tem que ser maior de 16 anos.');
                notificacao
                    .result
                    .then(function () {
                        vm.conjuge.dataNascimento = null;

                        focus('DataNascimentoEC');
                    });

                return;
            }

            if (dataNascimento.isAfter(dataEmissao)) {
                vm.conjuge.dataEmissao = null;
            }
        };

        vm.validarDataEmissao = function (dataEmissao) {
            if (!validar(dataEmissao)) {
                vm.conjuge.dataEmissao = null
            }
        }

        function alterarEstadoCivil(estadoCivil) {
            var temConjuge = ['CASADO', 'UNIAO_ESTAVEL'];

            if (temConjuge.indexOf(estadoCivil) != -1) return;

            delete vm.conjuge;
            delete vm.regimeCasamento;
        }

        function carregarListas() {
            documento
                .identificacoes()
                .then(function (res) {
                    vm.identificacoes = res.data;
                });

            pessoa
                .estadoCivil()
                .then(function (res) {
                    vm.estadosCivis = res.data;
                });

            pessoa
                .regimeCasamento()
                .then(function (res) {
                    vm.regimes = res.data;
                });

            cepFactory
                .estados()
                .then(function (res) {
                    vm.estados = res.data;
                });

            dadosProfissional
                .profissoes()
                .then(function (res) {
                    vm.profissoes = res.data;
                });

            if (!!vm.conjuge) {
                if (!!vm.conjuge.tipoIdentificacao) {
                    vm.buscarOrgaosExpeditores(vm.conjuge.tipoIdentificacao);
                }
                if (!!vm.conjuge.dataEmissao) {
                    vm.conjuge.dataEmissao = moment(vm.conjuge.dataEmissao);
                }
                if (!!vm.conjuge.dataNascimento) {
                    vm.conjuge.dataNascimento = moment(vm.conjuge.dataNascimento);
                }
                if (!!vm.estadoCivil) {
                    alterarEstadoCivil(vm.estadoCivil);
                }
            }
        }
    }
}());
