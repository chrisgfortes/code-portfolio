(function () {
    'use strict';

    angular
        .module('app')
        .service('conjugeConvert', conjugeConvert)

    /** @ngInject */
    function conjugeConvert(moment) {

        this.post = post;
        this.get = get;

        function post(conjuge) {
            var _conjuge = angular.copy(conjuge);

            if (!!_conjuge && !_conjuge.profissao) _conjuge.profissao = null;

            return _conjuge;
        }

        function get(conjuge) {
            var _conjuge = angular.copy(conjuge);

            if (!!_conjuge.dataEmissao) _conjuge.dataEmissao = moment(_conjuge.dataEmissao);
            if (!!_conjuge.dataNascimento) _conjuge.dataNascimento = moment(_conjuge.dataNascimento);

            return _conjuge;
        }
    }
}());
