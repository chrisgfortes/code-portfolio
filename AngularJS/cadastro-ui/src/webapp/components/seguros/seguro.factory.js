(function () {
    'use strict';

    angular
        .module('app')
        .factory('seguro', seguro)

    /** @ngInject */
    function seguro($http, HOST) {

        return {
            tipos: tipos,
            seguradoras: seguradoras
        }

        function tipos() {
            return $http.get(urlBase() + '/tipos');
        }

        function seguradoras(tipo) {
            return $http.get(urlBase() + '/seguradoras?tipoSeguro=' + tipo);
        }

        function urlBase() {
            return HOST.pessoa + '/seguro';
        }
    }
}());