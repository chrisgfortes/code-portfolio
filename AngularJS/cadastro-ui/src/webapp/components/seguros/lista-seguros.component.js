(function () {
    'use strict';

    listaSegurosController.$inject = ['seguro', 'seguroConvert'];

    angular
        .module('app')
        .component('listaSeguros', {
            bindings: {
                seguros: '<',
                readonly: '<',
                onEditar: '&',
                onNovo: '&',
                onRemover: '&',
                esconderBotaoNovo: '<'
            },
            controller: listaSegurosController,
            templateUrl: './components/seguros/lista-seguros.template.html'
        });

    function listaSegurosController(seguro, seguroConvert) {
        var vm = this;

        vm.$onInit = init;
        vm.remover = remover;
        vm.editar = editar;
        vm.novo = novo;

        function init() {
            vm.seguros = vm.seguros || [];

            seguro
                .tipos()
                .then(function (res) {
                    vm.tipoSeguros = res.data;

                    vm.seguros.map(function (_seguro) {
                        vm.tipoSeguros
                            .forEach(tipoSeguroBuild(_seguro));

                        seguro
                            .seguradoras(_seguro.tipoSeguro)
                            .then(function (seguradoras) {
                                seguradoras
                                    .data
                                    .forEach(seguradoraBuild(_seguro));
                            });

                        return _seguro;
                    });
                });

            if (vm.readonly) {
                vm.seguros = seguroConvert.convertGet(vm.seguros);
            }
        }

        function tipoSeguroBuild(seguro) {
            return function (tipo) {
                if (seguro.tipoSeguro == tipo.valor) {
                    seguro.tipoDescricao = tipo.descricao;
                }
            }
        }

        function seguradoraBuild(seguro) {
            return function (seguradora) {
                if (seguro.seguradora == seguradora.valor) {
                    seguro.seguradoraDescricao = seguradora.descricao;
                }
            }
        }

        function novo() {
            vm.onNovo();
        }

        function remover(seguro) {
            seguro.modoExclusao = true;
            vm.onRemover({ seguro: angular.copy(seguro) });
        }

        function editar(seguro) {
            vm.seguros = vm.seguros.map(function (_seguro) {
                _seguro.modoEdicao = false;
                return _seguro;
            });

            seguro.modoEdicao = true;

            vm.onEditar({ seguro: angular.copy(seguro) });
        }
    }
}());
