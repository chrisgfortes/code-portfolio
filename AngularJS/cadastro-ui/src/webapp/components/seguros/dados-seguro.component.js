(function() {
    'use strict';

    dadosSeguroController.$inject = ['moment', 'seguro'];

    angular
        .module('app')
        .component('dadosSeguro', {
            bindings: {
                seguro: '<',
                onSalvar: '&',
                onCancelar: '&'
            },
            controller: dadosSeguroController,
            templateUrl: './components/seguros/dados-seguro.template.html'
        });

    function dadosSeguroController(moment, seguro) {
        var vm = this;

        vm.$onInit = init;
        vm.salvar = salvar;
        vm.buscarSeguradoras = buscarSeguradoras;
        vm.cancelar = cancelar;
        vm.minDataVencimento = moment();

        function init() {
            seguro
                .tipos()
                .then(function(res) {
                    vm.tipoSeguros = res.data;

                    buscarSeguradoras(vm.seguro.tipoSeguro);
                });
        }

        function buscarSeguradoras(tipo) {
            if (!tipo) {
                return;
            }

            seguro
                .seguradoras(tipo)
                .then(function(res) {
                    vm.seguradoras = res.data;
                });
        }

        function salvar(seguro) {
            vm.tipoSeguros.filter(function(t) {
                if (seguro.tipoSeguro == t.valor) {
                    seguro.tipoDescricao = t.descricao;
                }
            });

            vm.seguradoras.filter(function(s) {
                if (seguro.seguradora == s.valor) {
                    seguro.seguradoraDescricao = s.descricao;
                }
            });

            vm.onSalvar({ seguro: seguro });
        }

        function cancelar() {
            vm.onCancelar();
        }

    }
}());
