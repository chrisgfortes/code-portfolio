(function() {
    'use strict';

    angular
        .module('app')
        .service('seguroConvert', seguroConvert)

    function seguroConvert(moment) {

        this.convertPost = convertPost;
        this.convertGet = convertGet;
        var vm = this;

        function convertPost(seguros) {
            return seguros.map(function(seguro) {
                delete seguro.tipoDescricao;
                delete seguro.seguradoraDescricao;
                return seguro;
            })
        }

        function convertGet(seguros) {
            return seguros.map(function(seguro) {
                seguro.dataVencimento = moment(seguro.dataVencimento)
                return seguro;
            })
        }

    }

}());
