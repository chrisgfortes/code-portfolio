(function() {
    'use strict';

    sessaoSeguroController.$inject = ['dialogs', 'focus'];

    angular
        .module('app')
        .component('sessaoSeguro', {
            transclude: true,
            bindings: {
                seguros: '=',
                readonly: '<'
            },
            controller: sessaoSeguroController,
            templateUrl: './components/seguros/sessao-seguro.html'
        });

    function sessaoSeguroController(dialogs, focus) {
        var vm = this;

        vm.salvar = salvar;
        vm.cancelar = cancelar;
        vm.editar = editar;
        vm.novo = novo;
        vm.remover = remover;
        vm.$onInit = onInit;

        function onInit () {
            vm.seguros = vm.seguros || [];
        }

        vm.mostrarSeguroForm = false;
        vm.seguro = {};

        function salvar(seguro) {
            var s = angular.copy(seguro);

            if (s.modoEdicao) {
                atualizarListaSeguros(s);
            } else {
                vm.seguros.push(s);
            }

            vm.seguro = {};
            vm.mostrarSeguroForm = false;
        }

        function cancelar() {
            vm.seguros.forEach(function(seguro) {
                seguro.modoEdicao = false;
            });

            vm.mostrarSeguroForm = false;

            vm.seguros = vm.seguros.map(function(_seguro) {
                _seguro.modoEdicao = false;

                return _seguro;
            });

            vm.seguro = {};

            focus('novoSeguro');
        }

        function editar(seguro) {
            vm.mostrarSeguroForm = true;

            vm.seguro = angular.copy(seguro);
        }

        function novo() {
            vm.mostrarSeguroForm = true;

            focus('tipoSeguro');
        }

        function atualizarListaSeguros(seguro) {
            vm.seguros = vm.seguros.map(function(item) {
                if (item.modoEdicao) {
                    seguro.modoEdicao = false;
                    return seguro;
                }
                return item;
            });
        }

        function remover(seguro) {
            var confirmacao = dialogs.confirm('Confirmar', 'Confirma a remoção do seguro?');
            confirmacao
                .result
                .then(function (btn) {
                    vm.seguros = vm.seguros.filter(function (_seguro) {
                        return !_seguro.modoExclusao;
                    });
                })
                .catch(function () {
                    vm.seguros = vm.seguros.map(function (seguro) {
                        seguro.modoExclusao = false;
                        return seguro;
                    });
                });
        }
    }
}());
