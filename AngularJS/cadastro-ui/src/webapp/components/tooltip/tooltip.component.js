(function () {
    'use strict';

    tooltipController.$inject = [];

    angular
        .module('app')
        .component('tooltip', {
            bindings: {
                texto: '<',
            },
            controller: tooltipController,
            templateUrl: './components/tooltip/tooltip.html'
        });

    function tooltipController(contato) {
        var vm = this;

        vm.$onInit = onInit;

        function onInit() {

        }
    }
}());
