(function() {
    'use strict';

    matriculaVinculadaController.$inject = ['pessoa', 'dialogs', 'focus', 'dadosProfissional'];

    angular
        .module('app')
        .component('matriculaVinculada', {
            bindings: {
                vinculo: '=',
                readonly: '<'
            },
            controller: matriculaVinculadaController,
            templateUrl: './components/matricula-vinculada/dados-matricula-vinculada.html'
        });

    function matriculaVinculadaController(pessoa, dialogs, focus, dadosProfissional) {
        var vm = this;

        vm.buscaPorMatricula = buscaPorMatricula;
        vm.isVinculoValido = isVinculoValido;

        vm.$onInit = init;

        function init() {
            dadosProfissional
                .tipoVinculo()
                .then(function(res) {
                    vm.tiposVinculos = res.data;
                });
        }

        function isVinculoValido() {
            return !!vm.vinculo && !!vm.vinculo.nomeAssociado;
        }

        function buscaPorMatricula(matricula) {
            if(!matricula) {
                vm.vinculo = {};
                return;
            };

            pessoa
                .buscar(matricula)
                .then(function(res) {
                    var associado = res.data[0];
                    vm.vinculo.nomeAssociado = associado.nome;
                })
                .catch(function (err) {
                    dialogs.notify('Atenção', "O número de matrícula informado não foi encontrado.");
                    vm.vinculo = {};

                    focus('numeroMatricula');
                });
        }
    }
}());
