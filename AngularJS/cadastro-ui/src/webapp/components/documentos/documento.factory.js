(function () {
    'use strict';

    angular
        .module('app')
        .factory('documento', documento)

    /** @ngInject */
    function documento($http, HOST) {

        return {
            identificacoes: identificacoes,
            orgaoemissor: orgaoemissor
        }

        function identificacoes() {
            return $http.get(urlBase() + '/identificacoes', { cache: true });
        }

        function orgaoemissor(id) {
            return $http.get(urlBase() + '/orgaos-emissores?documento=' + id, { cache: true });
        }

        function urlBase() {
            return HOST.pessoa + '/documento';
        }
    }

}());