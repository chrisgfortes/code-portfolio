(function () {
    'use strict';

    documentoController.$inject = ['representanteLegal', 'cepFactory', 'moment', '$scope', 'dialogs', 'focus', 'documento'];

    angular
        .module('app')
        .component('dadosDocumento', {
            bindings: {
                documento: '=',
                validadeNaturalidade: '=',
                nomeComponente: '@',
                ocultarCampos: '<',
                readonly: '<',
                onRepresentanteSelecionado: '&'
            },
            controller: documentoController,
            templateUrl: './components/documentos/dados-documentos.html'
        });

    function documentoController(representanteLegal, cepFactory, moment, $scope, dialogs, focus, documento) {
        var vm = this;

        vm.representantesLegal = [];
        vm.confirmaMenorDeIdade = confirmaMenorDeIdade;
        vm.carregarCidades = carregarCidades;
        vm.defineNacionalidade = defineNacionalidade;
        vm.representanteLegalSelecionado = representanteLegalSelecionado;
        vm.buscarOrgaosExpeditores = buscarOrgaosExpeditores;
        vm.dataNascimentoInvalida = false;
        vm.orgaosExpeditores = [];
        vm.buscaNacionalidades = buscaNacionalidades;
        vm.buscaNaturalidade = buscaNaturalidade;
        vm.dataAtual = moment();

        vm.$onInit = init;

        function buscarOrgaosExpeditores(id) {
            if (!id) return;

            documento
                .orgaoemissor(id)
                .then(function (res) {
                    vm.orgaosExpeditores = res.data;
                });

            vm.maxDataNascimento = (id == "CARTEIRA_MOTORISTA") ? moment().subtract(18, 'years') : moment();

            if (!!vm.documento.dataNascimento && vm.maxDataNascimento.isBefore(vm.documento.dataNascimento)) {
                var notificar = dialogs.notify('Atenção', 'Para possuir carteira de motorista o cadastrado deve ser maior de 18 anos.');

                vm.documento.dataNascimento = undefined;
                notificar
                    .result
                    .then(function () {
                        focus('dadosPessoaisDataNascimento');
                    });
            }
        }

        function setModalidadeRepresentanteLegal(modalidade) {
            vm.documento.modalidadeRepresentanteLegal = {
                selected: vm.representantesLegal.filter(function (representante) {
                    return representante.valor == modalidade;
                })[0]
            };

            representanteLegalSelecionado(vm.documento.modalidadeRepresentanteLegal.selected);
        }

        function init() {
            vm.ocultar = {};
            vm.nomeComponente = '';
            var novoDocumento = {
                modalidadeRepresentanteLegal: "NENHUM",
                nacionalidade: "Brasileiro(a)"
            };

            vm.documento = vm.documento || novoDocumento;

            ocultarCamposDocumentos();

            if (!vm.ocultar.tipoIdentificacao) {
                documento
                    .identificacoes()
                    .then(function (res) {
                        vm.identificacoes = res.data;
                    });
            }

            if (!vm.ocultar.representanteLegal) {
                representanteLegal
                    .modalidades()
                    .then(function (res) {
                        vm.representantesLegal = res.data;
                        if (!!vm.documento.modalidadeRepresentanteLegal) {
                            setModalidadeRepresentanteLegal(vm.documento.modalidadeRepresentanteLegal);
                        }
                    });
            }

            if (!vm.ocultar.ufExpeditor) {
                cepFactory
                    .estados()
                    .then(function (res) {
                        vm.estados = res.data;
                    });
            }

            if (!vm.ocultar.nacionalidade) {
                cepFactory
                    .paises()
                    .then(function (res) {
                        vm.paises = res.data;
                        if (!!vm.documento.nacionalidade) {
                            vm.documento.nacionalidade =
                                vm.paises.filter(function (pais) {
                                    return pais.descricaoNacionalidade == vm.documento.nacionalidade;
                                })[0];
                        }
                    });
            }

            defineNacionalidade(vm.documento.nacionalidade);

            if (!!vm.documento.dataEmissao) {
                vm.documento.dataEmissao = moment(vm.documento.dataEmissao);
            }
            if (!!vm.documento.dataNascimento) {
                vm.documento.dataNascimento = moment(vm.documento.dataNascimento);
                vm.minDataEmissao = angular.copy(vm.documento.dataNascimento);
            }
            if (!!vm.documento.tipoIdentificacao) {
                vm.buscarOrgaosExpeditores(vm.documento.tipoIdentificacao);
            }
            if (!!vm.documento.uf) {
                vm.carregarCidades(vm.documento.uf);
            }
        }

        vm.onChange = function (dataNascimento, dataEmissao) {
            if (validar(dataNascimento)) {
                confirmaMenorDeIdade(dataNascimento);
            } else {
                vm.documento.dataNascimento = undefined;
            }

            if (!!dataNascimento && !!dataEmissao) {
                if (dataNascimento.isAfter(dataEmissao)) {
                    vm.documento.dataEmissao = undefined;
                }
            }
            vm.minDataEmissao = angular.copy(dataNascimento);
        };

        vm.validarDataEmissao = function (dataEmissao) {
            if (!validar(dataEmissao)) {
                vm.documento.dataEmissao = undefined
            }
        }

        vm.brasileiro = true;

        vm.maxDataNascimento = moment();

        function confirmaMenorDeIdade(data) {
            if (!validar(data)) {
                vm.dataInvalida = true;
                return;
            }

            vm.dataInvalida = false;

            var dataConvertida = moment(data, 'DD/MM/YYYY').toDate();

            var idade = moment().diff(dataConvertida, 'years');

            var maiorDeIdade = idade > 17;

            if (!maiorDeIdade) {
                setModalidadeRepresentanteLegal('PF_MENOR_IDADE');
            }

            return maiorDeIdade;
        };

        function validar(data) {
            return moment(data, 'DD/MM/YYYY', true).isValid();
        }

        function carregarCidades(uf) {
            if (!uf) {
                vm.documento.naturalidade = {};
                return;
            }

            vm.documento.naturalidade = vm.documento.naturalidade || {};
            cepFactory
                .cidades(uf)
                .then(function (res) {
                    vm.cidades = res.data;
                    if (!!vm.documento.naturalidade) {
                        vm.documento.naturalidade = {
                            selected: vm.cidades.filter(function (cidade) {
                                return cidade.nomeCidade == vm.documento.naturalidade;
                            })[0]
                        }
                    }
                });
        }

        function defineNacionalidade(nacionalidade) {
            if (!nacionalidade) return;

            vm.brasileiro = nacionalidade == 'Brasileiro(a)' || nacionalidade.siglaPais == 'BRA';
        }

        function representanteLegalSelecionado(representante) {
            if (!representante) {
                return;
            }

            var representantesPodemVer = ['PF_INCAPAZ', 'PF_MENOR_IDADE'];

            var mostrarRepresentante = representantesPodemVer.some(function (_representante) {
                return representante.valor == _representante;
            });

            vm.onRepresentanteSelecionado({ representante: representante, mostrarRepresentante: mostrarRepresentante });
        }

        function ocultarCamposDocumentos() {
            var campos = vm.ocultarCampos;

            if (!campos) return;

            campos.forEach(function (campo) {
                vm.ocultar[campo] = true;
            });
        }

        function buscaNacionalidades($select) {
            if (!$select.search) {
                vm.nacionalidades = [];
                return;
            }

            vm.nacionalidades = vm.paises.filter(function (pais) {
                return pais.descricaoNacionalidade.search($select.search);
            });
        }

        function buscaNaturalidade($select) {
            if (!$select.search) {
                vm.naturalidades = [];
                return;
            }

            vm.naturalidades = vm.cidades.filter(function (cidade) {
                return cidade.nomeCidade.search($select.search);
            });
        }
    }
}());
