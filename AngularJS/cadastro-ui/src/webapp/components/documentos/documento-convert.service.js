(function () {
    'use strict';

    angular
        .module('app')
        .service('documentoConvert', documentoConvert)

    /** @ngInject */
    function documentoConvert() {

        this.post = post;

        function post(documento) {
            var _documento = angular.copy(documento);

            _documento.modalidadeRepresentanteLegal = documento.modalidadeRepresentanteLegal.selected.valor;

            var nacionalidade = documento.nacionalidade;

            _documento.nacionalidade = nacionalidade.descricaoNacionalidade;

            if (nacionalidade.id != 7) {
                delete _documento.uf;
                delete _documento.naturalidade;
            } else {
                _documento.naturalidade = documento.naturalidade ? documento.naturalidade.selected.nomeCidade: undefined;
            }

            return _documento;
        }
    }
}());