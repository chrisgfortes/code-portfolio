(function () {
    'use strict';

    dadosMatriculaController.$inject = [
        '$routeParams',
        'enderecoUpdate'
    ];


    angular
        .module('app')
        .component('dadosMatricula', {
            bindings: {
                matricula: '<',
                readonly: '<'
            },
            controller: dadosMatriculaController,
            templateUrl: './components/dados-matricula/dados-matricula.html'
        });

    function dadosMatriculaController($routeParams, enderecoUpdate) {
        var vm = this;

        vm.$onInit = init;

        function init() {
            vm.matricula = vm.matricula || {};
            carregarDados();
        }

        function carregarDados() {
            enderecoUpdate
                .buscar($routeParams.cpf)
                .then(function (res) {
                    vm.enderecos = res;
                    montarEndereco();
                });

        }

        function montarEndereco() {
            vm.matricula.enderecoPrincipal = stringEndereco(vm.enderecos.filter(filtrarEnderecoPrincipal)[0]);
            vm.matricula.enderecoEmpostamento = stringEndereco(vm.enderecos.filter(filtrarEnderecoEmpostamento)[0]);
        }

        function filtrarEnderecoPrincipal(endereco) {
            return endereco.principal;
        }

        function filtrarEnderecoEmpostamento(endereco) {
            return endereco.empostamento;
        }

        function stringEndereco(endereco) {
            if (!!endereco) {
                return endereco.logradouro + ', ' + endereco.numero + ' - CEP: ' + endereco.cep;
            } else{
                return "Não informado.";
            }
        }
    }
}());
