(function () {
    'use strict';

    fontePagadoraController.inject = ['renda','$rootScope']

    angular
        .module('app')
        .component('dadosFontePagadora', {
            bindings: {
                fontePagadora: '=',
                isNova: '<'
            },
            controller: fontePagadoraController,
            templateUrl: './components/fonte-pagadora/dados-fonte-pagadora.html'
        });

    function fontePagadoraController(renda, $rootScope) {
        var vm = this;

        vm.$onInit = onInit;
        vm.$postLink = postLink;
        vm.alterarFolha = alterarFolha;
        vm.alterarTipo = alterarTipo;
        vm.configClassNomeEmpresa = configClassNomeEmpresa;
        vm.isOutros = false;
        vm.requeridoCPF = false;
        vm.requeridoCNPJ = false;

        function onInit() {
            renda
                .folhas()
                .then(function (folhas) {
                    vm.folhas = folhas;
                });

            renda
                .ramos()
                .then(function (ramos) {
                    vm.ramos = ramos;
                });

            renda
                .tiposControle()
                .then(function (tiposControle) {
                    vm.tiposControle = tiposControle;
                });

        }

        function postLink() {
            if (angular.equals(vm.fontePagadora, {})) {
                vm.fontePagadora.tipo = 'cnpj';
            }

            alterarFolha(vm.fontePagadora.folhaLayout);
            alterarTipo(vm.fontePagadora.tipo);
        }

        function alterarFolha(folha) {
            if (!folha) {
                vm.mostrarEmpresaETipoControle = false;
                vm.fontePagadora.creditoConsignado = false;
                vm.fontePagadora.tipoControle = "PRIVADO";
                vm.fontePagadora.folhaLayout = null;

                return;
            }

            var itens = ['OUTROS_NENHUM', 'UNIMED', 'USIMED'];

            vm.mostrarEmpresaETipoControle = itens.some(function (item) {
                return item == folha;
            });
        }

        function alterarTipo(tipo) {
            vm.isOutros = tipo == 'semCnpj';
            vm.requeridoCPF = tipo == 'cpf';
            vm.requeridoCNPJ = tipo == 'cnpj';
        }

        function configClassNomeEmpresa(isOutros) {
            return {
                "col-md-12": isOutros,
                "col-md-6": !isOutros,
                "col-sm-12": isOutros,
                "col-sm-6": !isOutros,
                "col-xs-12": true,
            }
        }

        $rootScope.$on('nova-fonte-pagadora', function (e) {
            vm.isNova = true;
            vm.fontePagadora = { tipo: 'cnpj'};
        })
    }
}());
