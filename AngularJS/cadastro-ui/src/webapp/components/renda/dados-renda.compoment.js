(function () {
    'use strict';

    rendaController.inject = ['renda', 'focus', 'moment', '$rootScope']

    angular
        .module('app')
        .component('dadosRenda', {
            bindings: {
                renda: '<',
                onCancelar: '&',
                onSalvar: '&'
            },
            controller: rendaController,
            templateUrl: './components/renda/dados-renda.html'
        });

    function rendaController(renda, focus, moment, $rootScope) {
        var vm = this;

        vm.$onInit = onInit;
        vm.$postLink = postLink;
        vm.cancelar = cancelar;
        vm.salvar = salvar;
        vm.buscarFontePagadora = buscarFontePagadora;
        vm.fontePagadoraSelecionada = fontePagadoraSelecionada;
        vm.validarData = validarData;
        vm.maxData = moment();
        vm.tiposFontePagadora = {
            cpf: 1,
            cnpj: 2,
            semCnpj: 3
        }

        function onInit() {
            vm.isNova = true;

            vm.novaFontePagadora = !renda.fontePagadora;

            renda
                .tipos()
                .then(function (tipos) {
                    vm.tipos = tipos;
                });

            renda
                .comprovacoes()
                .then(function (comprovacoes) {
                    vm.comprovacoes = comprovacoes;
                });
        }

        function postLink() {
            if (!vm.renda.modoEdicao) {
                vm.isNova = true;
                return;
            }


            segregarCpfCnpj(vm.renda.fontePagadora);
        }

        function cancelar() {
            vm.onCancelar()
        }

        function salvar(renda) {
            var descricaoTipoDeRenda = vm.tipos.filter(function (tipoRenda) {
                return tipoRenda.valor == renda.tipo;
            })[0];
            renda.descricaoTipoDeRenda = descricaoTipoDeRenda.descricao;
            renda.fontePagadora.cpfCnpj = converterCpjCnpj(renda.fontePagadora);

            vm.onSalvar({ renda: angular.copy(renda) })
        }

        function buscarFontePagadora(query) {
            if (!query) return;
            var tipoFontePagadora = vm.tiposFontePagadora[vm.renda.fontePagadora.tipo];
            renda
                .buscarFontePagadora(query, tipoFontePagadora)
                .then(function (fontes) {
                    vm.fontesPagadoras = fontes;

                    return fontes;
                });
        }

        function fontePagadoraSelecionada(item) {
            if (!item) {
                $rootScope.$broadcast('nova-fonte-pagadora');

                return;
            }

            vm.isNova = !item.id;
            vm.renda.fontePagadora = item;

            segregarCpfCnpj(item);
        }

        function segregarCpfCnpj(item) {
            if (item.cpfCnpj && item.cpfCnpj.length == 11) {
                vm.renda.fontePagadora.tipo = 'cpf';
                vm.renda.fontePagadora.cpf = item.cpfCnpj;

                return;
            }

            if (item.cpfCnpj && item.cpfCnpj.length == 14) {
                vm.renda.fontePagadora.tipo = 'cnpj';
                vm.renda.fontePagadora.cnpj = item.cpfCnpj;

                return;
            }

            vm.renda.fontePagadora.tipo = 'semCnpj';
        }

        function converterCpjCnpj(fontePagadora) {
            if (fontePagadora.tipo == 'cpf')
                return fontePagadora.cpf;

            if (fontePagadora.tipo == 'cnpj')
                return fontePagadora.cnpj;

            return '';
        }

        function validarData(prop, data) {
            if (!validar(data)) {
                vm.renda[prop] = undefined;
            }
        }

        function validar(data) {
            return moment(data, 'DD/MM/YYYY', true).isValid();
        }
    }
}());
