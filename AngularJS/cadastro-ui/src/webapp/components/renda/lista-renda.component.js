(function () {
    'use strict';

    rendaController.inject = ['renda']

    angular
        .module('app')
        .component('listaRenda', {
            bindings: {
                rendas: '<',
                totalRendas: '<',
                onNovo: '&',
                onEditar: '&',
                onRemover: '&',
                esconderBotaoNovo: '<',
                readonly: '<'
            },
            controller: rendaController,
            templateUrl: './components/renda/lista-renda.html'
        });

    function rendaController(renda) {
        var vm = this;

        vm.$onInit = onInit;
        vm.novo = novo;
        vm.editar = editar;
        vm.remover = remover;

        function onInit() {
            vm.rendas = vm.rendas || [];
        }

        function novo() {
            vm.onNovo()
        }

        function editar(renda) {
            vm.rendas.map(function (item) {
                item.modoEdicao = false;

                return item;
            });

            renda.modoEdicao = true;

            vm.onEditar({ renda: angular.copy(renda) })
        }

        function remover(renda) {
            renda.modoExclusao = true;

            vm.onRemover({ renda: angular.copy(renda) })
        }
    }
}());
