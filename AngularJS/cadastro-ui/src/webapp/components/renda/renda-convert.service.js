(function(){
    'use strict';

    angular
        .module('app')
        .service('rendaConverter', rendaConverter)

    function rendaConverter(moment){

        this.get = get;

        function get(renda){
            if(renda.data)
                renda.data = moment(renda.data);

            if(renda.dataAdmissao)
                renda.dataAdmissao = moment(renda.dataAdmissao); 

            return renda;
        }
    }

}());