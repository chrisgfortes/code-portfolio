(function () {
    'use strict';

    angular
        .module('app')
        .factory('renda', renda)

    function renda($http, HOST, rendaConverter) {

        return {
            folhas: folhas,
            ramos: ramos,
            tipos: tipos,
            comprovacoes: comprovacoes,
            tiposControle: tiposControle,
            buscarFontePagadora: buscarFontePagadora
        }

        function folhas() {
            var url = urlRendaBase('folhas-layouts');

            return $http
                .get(url, { cache: true })
                .then(transformaRetorno);
        }

        function ramos() {
            var url = urlRendaBase('ramos');

            return $http
                .get(url, { cache: true })
                .then(transformaRetorno);
        }

        function tipos() {
            var url = urlRendaBase('tipos');

            return $http
                .get(url, { cache: true })
                .then(transformaRetorno);
        }

        function comprovacoes() {
            var url = urlRendaBase('comprovacoes');

            return $http
                .get(url, { cache: true })
                .then(transformaRetorno);
        }

        function tiposControle() {
            var url = urlRendaBase('tipos-controle');

            return $http
                .get(url, { cache: true })
                .then(transformaRetorno);
        }

        function buscarFontePagadora(query, tipoFontePagadora) {
            var url = urlRendaBase('fonte-pagadora');
            return $http
                .get(url + '?query=' + query + '&tipoFontePagadora=' + tipoFontePagadora)
                .then(transformaRetorno);
        }

        function urlRendaBase(path) {
            return HOST.pessoa + '/renda/' + path;
        }

        function transformaRetorno(res) {
            return res.data;
        }
    }
}());
