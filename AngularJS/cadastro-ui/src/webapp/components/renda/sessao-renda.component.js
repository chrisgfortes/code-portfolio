(function () {
    'use strict';

    sessaoRendaController.$inject = ['dialogs', 'focus', 'renda', 'moment'];

    angular
        .module('app')
        .component('sessaoRenda', {
            bindings: {
                rendasObj: '=',
                readonly: '<',
                mostraSessao: '<'
            },
            controller: sessaoRendaController,
            templateUrl: './components/renda/sessao-renda.html'
        });

    function sessaoRendaController(dialogs, focus, renda, moment) {
        var vm = this;

        vm.salvar = salvar;
        vm.cancelar = cancelar;
        vm.editar = editar;
        vm.validarData = validarData;
        vm.novo = novo;
        vm.remover = remover;
        vm.mostrarRendaForm = false;
        vm.maxDate = moment();
        vm.$onInit = onInit;

        function onInit() {
            vm.rendasObj = vm.rendasObj || { rendas:[] };
            vm.rendasObj.dataAtualizacaoRenda = vm.rendasObj.dataAtualizacaoRenda
                                                ? moment(vm.rendasObj.dataAtualizacaoRenda)
                                                : moment();
            vm.renda = rendaFactory();
            vm.totalRendas = calcularTotalRenda();
            renda
                .tipos()
                .then(function (tipos) {
                    vm.tipos = tipos;
                    vm.rendasObj.rendas = vm.rendasObj.rendas.map(adicionaDescricaoTipoDeRenda);
                });
        }

        function adicionaDescricaoTipoDeRenda (renda) {
            var descricaoTipoDeRenda = vm.tipos.filter(filtrarRendaSelecionado(renda))[0];
            renda.descricaoTipoDeRenda = descricaoTipoDeRenda.descricao;
            return renda;
        }

        function filtrarRendaSelecionado(renda) {
            return function (tipoRenda) {
                return tipoRenda.valor == renda.tipo;
            }
        }

        function salvar(renda) {
            var _renda = angular.copy(renda);

            if (_renda.modoEdicao) {
                atualizarLista(_renda)
            } else {
                vm.rendasObj.rendas.push(_renda);
            }

            vm.renda = rendaFactory();
            vm.mostrarRendaForm = false;
            vm.totalRendas = calcularTotalRenda();
        }

        function atualizarLista(renda) {
            vm.rendasObj.rendas = vm.rendasObj.rendas.map(function (item) {
                if (item.modoEdicao) {
                    renda.modoEdicao = false;
                    return renda;
                }
                return item;
            });
        }

        function cancelar() {
            vm.renda = rendaFactory();

            vm.rendasObj.rendas = vm.rendasObj.rendas.map(function (item) {
                item.modoEdicao = false;

                return item;
            });

            vm.mostrarRendaForm = false;
            vm.totalRendas = calcularTotalRenda();
        }

        function editar(renda) {
            vm.mostrarRendaForm = true;
            vm.renda = angular.copy(renda);
        }

        function novo() {
            vm.mostrarRendaForm = true;

            focus('cnpjNovo');
        }

        function remover(renda) {
            var confirmacao = dialogs.confirm('Confirmar', 'Confirma a remoção do renda?');
            confirmacao
                .result
                .then(function (btn) {
                    vm.rendasObj.rendas = vm.rendasObj.rendas.filter(function (item) {
                        return !item.modoExclusao;
                    });

                    vm.totalRendas = calcularTotalRenda();
                })
                .catch(function () {
                    vm.rendasObj.rendas = vm.rendasObj.rendas.map(function (item) {
                        item.modoExclusao = false;
                        return item;
                    });
                });
        }

        function rendaFactory() {
            return { fontePagadora: {} };
        }

        function calcularTotalRenda() {
            return vm.rendasObj.rendas
                        .map(extrairRemuneracao)
                        .reduce(somar, 0);
        }

        function extrairRemuneracao(renda) {
            return renda.remuneracao;
        }

        function somar(valor, prox) {
            return valor + prox;
        }

        function validarData () {
            if (!validar(vm.rendasObj.dataAtualizacaoRenda)) {
                vm.rendasObj.dataAtualizacaoRenda = null;
            }
        }

        function validar(data) {
            return moment(data, 'DD/MM/YYYY', true).isValid();
        }
    }
}());
