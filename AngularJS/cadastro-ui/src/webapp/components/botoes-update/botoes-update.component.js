(function () {
    'use strict';

    botoesUpdateController.$inject = [];

    angular
        .module('app')
        .component('botoesUpdate', {
            bindings: {
                model: "<",
                onUpdate: '&',
                cpf: '<',
                progress: '<',
                isSubmit: '<'
            },
            controller: botoesUpdateController,
            templateUrl: './components/botoes-update/botoes-update.html'
        });

    function botoesUpdateController() {

        var vm = this;
        vm.update = update;

        function update(model) {
            return vm.onUpdate({ model: angular.copy(model) });
        }

    };
}());
