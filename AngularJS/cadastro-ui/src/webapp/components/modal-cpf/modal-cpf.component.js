(function() {
    'use strict';

    modalCpfController.$inject = ['$location', '$rootScope', 'urlService', 'focus', '$timeout', 'modalCPFFactory', 'AuthFactory', 'cpfService', 'pessoa'];

    angular
        .module('app')
        .component('modalCpf', {
            bindings: {
                open: '<',
                url: '<',
                titulo: '<'
            },
            controller: modalCpfController,
            controllerAs: '$ctrl',
            templateUrl: './components/modal-cpf/modal-cpf.template.html'
        });

    function modalCpfController($location, $rootScope, urlService, focus, $timeout, modalCPFFactory, AuthFactory, cpfService, pessoa) {
        var vm = this;

        vm.$onInit = init;
        vm.buscar = _buscar;
        vm.changeFilter = changeFilter;
        vm.resetarModal = resetarModal;
        vm.modal = {
            close: _modalClose
        }

        vm.cooperativa = {
            selected: {}
        };

        function init() {
            vm.tipo = undefined;
            vm.filter = 'CPF';
            var user = AuthFactory.getUsuario();

            modalCPFFactory
                .cooperativasModal()
                .then(function(res) {
                    vm.cooperativa.selected = angular.copy(res);
                    delete vm.cooperativa.selected.inferior;

                    vm.cooperativas = res.inferior || [];
                    vm.cooperativas = [vm.cooperativa.selected].concat(vm.cooperativas);
                })
                .catch(function(err) {
                    vm.cooperativas = err;
                });
        }

        $rootScope.$on('open-modal-cpf', function(event, data) {
            vm.open = data.enable;
            vm.titulo = data.titulo;
            vm.url = data.url;
            vm.isAlteracao = data.isAlteracao || false;
            $timeout(function() {
                focus('cpfField');
            }, 300);
        });

        function _buscar() {
            vm.filter === 'CPF' ?
                redirecionar(vm.dadoCpf) :
                buscarCpfERedirecionar(vm.dadoMatriculaConta);
        }

        function buscarCpfERedirecionar(dadoMatriculaConta) {
            vm.erroBuscaCpf = false;
            var tipo = vm.filter;

            buscarCpf(tipo, dadoMatriculaConta)
                .then(function(cpf) {
                    !cpf || redirecionar(cpf);
                })

        }

        function buscarCpf(tipo, valor) {
            var matriculaConta = valor;
            return pessoa
                .dadosCadastrado(tipo, valor)
                .then(function(retorno) {
                    return retorno.data.cpfCnpj;
                })
                .catch(function() {
                    vm.erroBuscaCpf = true;
                    vm.dadoMatriculaConta = matriculaConta;
                });
        }

        function redirecionar(cpf) {
            if (cpfService.validarCPF(cpf)) {

                buscarTipos(cpf)
                    .then(function() {
                        salvarCooperativa();
                        if (vm.isAlteracao && vm.tipos.length > 0) {
                            $location.path('alteracao/' + cpf);
                        } else {
                            if (!vm.tipo) {
                                novoCadastro(cpf)
                            } else if (vm.tipo == "produtos-e-servicos") {
                                $location.path('alteracao/' + cpf);
                            } else {
                                showModalSteps(cpf);
                            }
                        }
                        vm.resetarModal();
                        vm.modal.close();
                    });
            }
        }

        function salvarCooperativa() {
            var coopSelecionada = vm.cooperativa.selected.codigo;
            AuthFactory.salvarActionCooperativa(coopSelecionada);
        }

        function novoCadastro(cpf) {
            $location.path('cadastro/terceiro/' + cpf + '/dados-pessoais');

            if (vm.isAlteracao) {
                $rootScope.$emit('mostrar-mensagem', {
                    titulo: 'CPF não cadastrado',
                    descricao: 'O cadastro do CPF ' + cpf + ' ainda não foi confirmado.'
                });
            }
        }

        function showModalSteps(cpf) {
            var modal = {
                enable: true,
                cpf: cpf
            };

            $rootScope.$emit('show-modal-steps', modal);
        }

        function buscarTipos(cpf) {
            return pessoa
                .buscarTipos(cpf)
                .then(function(response) {
                    vm.tipos = response;
                    !verificarTipos(response, 'TERCEIRO') || (vm.tipo = 'terceiro');
                    !verificarTipos(response, 'ASSOCIADO') || (vm.tipo = 'associado');
                    !verificarTipos(response, 'CORRENTISTA') || (vm.tipo = 'correntista');
                    !verificarTipos(response, 'AGENDAMENTO_CAPITAL') || (vm.tipo = 'agendamento-capital');
                    !verificarTipos(response, 'PROD_SERV') || (vm.tipo = 'produtos-e-servicos');
                });
        }

        function verificarTipos(arrayTipos, tipo) {
            return arrayTipos.some(function(item) {
                return item == tipo;
            });
        }

        function _modalClose() {
            vm.open = false;
        }

        function changeFilter(filtro) {
            vm.dadoCpf = vm.dadoMatriculaConta = null;
            vm.erroBuscaCpf = false;

            filtro === 'CPF' ?
                focus('cpfField') : focus('matriculaContaField');
        }

        function resetarModal() {
            vm.tipos = [];
            vm.tipo = undefined;
            vm.filter = "CPF";
            vm.erroBuscaCpf = false;
        }
    }
}());
