(function() {
    'use strict';

    angular
        .module('app')
        .factory('modalCPFFactory', modalCPFFactory)

    function modalCPFFactory($http, HOST, AuthFactory, $httpParamSerializer) {
        return {
            cooperativasModal: _cooperativasModal
        }

        function _cooperativasModal() {
            var cooperativa = AuthFactory.buscarCooperativa();
            var url = HOST.cadastro + '/cooperativas/' + cooperativa + '/hierarquias';

            return $http.get(url)
                  .then(function (res) {
                      return res.data;
                  });
        }
    }

}());
