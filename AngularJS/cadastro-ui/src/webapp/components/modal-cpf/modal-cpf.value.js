(function () {
    'use strict';

    angular
        .module('app')
        .value('redirectUrls', [
            {
                type: 'alteracoes',
                url: 'alteracao/:cpf/'
            },
            {
                type: 'terceiro',
                url: 'cadastro/terceiro/:cpf/dados-pessoais/'
            },
            {
                type: 'matricula',
                url: 'cadastro/associado/:cpf/dados-pessoais/'
            },
            {
                type: 'conta-corrente',
                url: 'cadastro/correntista/:cpf/conta-corrente/'
            }
        ]);
}());