(function () {
    'use strict';

    representanteLegalController.$inject = ['cepFactory', 'dialogs', 'focus', '$routeParams', 'documento'];

    angular
        .module('app')
        .component('dadosRepresentanteLegal', {
            bindings: {
                representante: '<',
                readonly: '<'
            },
            controller: representanteLegalController,
            templateUrl: './components/representante-legal/dados-representante-legal.html'
        });

    function representanteLegalController(cepFactory, dialogs, focus, $routeParams, documento) {

        var vm = this;

        vm.$onInit = init;
        vm.$onDestroy = onDestroy;
        vm.confirmaMenorDeIdade = confirmaMenorDeIdade;
        vm.buscarOrgaosExpeditores = buscarOrgaosExpeditores;
        vm.dataValida = false;
        vm.orgaosExpeditores = [];

        function init() {
            vm.representante = vm.representante || {};

            documento
                .identificacoes()
                .then(function (res) {
                    vm.identificacoes = res.data;
                });

            cepFactory
                .estados()
                .then(function (res) {
                    vm.estados = res.data;
                });

            if (!!vm.representante.tipoIdentificacao) {
                vm.buscarOrgaosExpeditores(vm.representante.tipoIdentificacao);
            }
            if (!!vm.representante.dataEmissao) {
                vm.representante.dataEmissao = moment(vm.representante.dataEmissao);
            }
            if (!!vm.representante.dataNascimento) {
                vm.representante.dataNascimento = moment(vm.representante.dataNascimento);
            }
        }

        function onDestroy() {
            vm.representante = undefined;
        }

        vm.onChange = function (dataNascimento, dataEmissao) {
            if (validar(dataNascimento)) {
                confirmaMenorDeIdade(dataNascimento);
            } else {
                vm.representante.dataNascimento = {};

                return;
            }

            if (dataNascimento.isAfter(dataEmissao)) {
                vm.representante.dataEmissao = {};
            }

            vm.minDataEmissao = angular.copy(dataNascimento);
        };

        vm.validarDataEmissao = function (dataEmissao) {
            if (!validar(dataEmissao)) {
                vm.representante.dataEmissao = {}
            }
        }

        vm.maxDataNascimento = moment();

        function confirmaMenorDeIdade(dataNascimento) {
            var dataConvertida = moment(dataNascimento, 'DD/MM/YYYY').toDate();
            var idade = moment().diff(dataConvertida, 'years');

            if (idade < 18) {
                var confirmacao = dialogs.confirm('Confirmar', 'Confirma a menor idade do representante legal?');
                confirmacao
                    .result
                    .catch(function () {
                        vm.representante.dataNascimento = {};
                        focus('dataNascimentoRepresentanteLegal');
                    });
            }
        }

        function validar(data) {
            return moment(data, 'DD/MM/YYYY', true).isValid();
        }

        function buscarOrgaosExpeditores(id) {
            documento
                .orgaoemissor(id)
                .then(function (res) {
                    vm.orgaosExpeditores = res.data;
                });
        }
    }
}());
