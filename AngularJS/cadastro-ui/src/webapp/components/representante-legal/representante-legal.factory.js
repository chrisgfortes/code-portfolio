(function () {
    'use strict';

    angular
        .module('app')
        .factory('representanteLegal', representanteLegal)

    /** @ngInject */
    function representanteLegal($http, HOST) {

        return {
            modalidades: modalidades
        }

        function modalidades() {
            return $http.get(urlBase() + '/modalidades', { cache: true });
        }

        function urlBase() {
            return HOST.pessoa + '/fisica/representante-legal';
        }
    }
}());