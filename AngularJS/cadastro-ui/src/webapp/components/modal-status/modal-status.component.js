(function () {
    'use strict';

    modalStatusController.$inject = ['$location', '$rootScope', 'pessoa', '$interval', 'controleDeAcesso', '$timeout'];

    angular
        .module('app')
        .component('modalStatus', {
            bindings: {
                open: '<',
                titulo: '<',
                cpf: '<'
            },
            controller: modalStatusController,
            controllerAs: '$ctrl',
            templateUrl: './components/modal-status/modal-status.template.html'
        });

    function modalStatusController($location, $rootScope, pessoa, $interval, controleDeAcesso, $timeout) {
        var vm = this;
        vm.$onInit = init;
        vm.startPulling = startPulling;

        function init() {
            vm.titulo = "Confirmando Cadastro de " + vm.titulo;
        }

        vm.modal = {
            close: _modalClose
        }

        $rootScope.$on('usuario-pendente', function (event, data) {
            vm.titulo = data.titulo ? ("Confirmando Cadastro de " + data.titulo) : "Confirmando Cadastro";
            vm.pendente = true;
            vm.sucesso = false;
            vm.erro = false;
            vm.nome = data.nome;
            vm.cpfCnpj = data.cpf;
            vm.isAssociado = (data.tipo === 'associado');
            vm.matriculaAssociado = undefined;
            vm.tipo = data.tipo;
            vm.open = data.enable;
            vm.startPulling(data.cpf, data.tipo);
        });

        function startPulling(cpf, tipo) {
            if (tipo == 'correntista') {
                var response = {
                    status: "OK"
                }
                mostrarSucessoOuErro(cpf, response);
            } else{
                vm.theInterval = $interval(function () {
                    pessoa
                    .status(cpf, tipo)
                    .then(function (response) {
                        !(response.status === "OK" || response.status === "ERRO") || mostrarSucessoOuErro(cpf, response);
                    });
                }.bind(this), 2000);
            }
        }

        function mostrarSucessoOuErro(cpf, response) {
            var sucesso = response.status === 'OK'
            vm.sucesso = sucesso;
            vm.erro = !sucesso;
            vm.pendente = false;
            !vm.sucesso || showModalSteps(cpf);
        }

        function showModalSteps(cpf) {
            var modal = {
                enable: true,
                cpf: cpf
            };

            $timeout(function() {
                vm.modal.close();
                $rootScope.$emit('show-modal-steps', modal);
            }, 1000);
        }

        function _modalClose() {
            $interval.cancel(vm.theInterval);
            vm.open = false;
        }
    }
}());
