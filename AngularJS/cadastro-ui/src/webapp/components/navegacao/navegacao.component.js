(function () {
    'use strict';

    navegacaoController.inject = ['$routeParams', '$location']

    angular
        .module('app')
        .component('navegacao', {
            bindings: {
                links: '<'
            },
            controller: navegacaoController,
            templateUrl: './components/navegacao/navegacao.html'
        });

    function navegacaoController($routeParams, $location) {
        var vm = this;

        vm.cpf = $routeParams.cpf;
        vm.$onInit = onInit;

        function onInit() {
            vm.links = vm.links.filter(ativo).map(linkAtual(vm.cpf));
        }

        function ativo(link) {
            return link.ativo;
        }

        function linkAtual(cpf) {
            var isCorrent = false;
            var path = '#' + $location.path();

            return function (link) {
                link.isCorrent = link.url(cpf).indexOf(path) != -1;
                link.isCompleted = false;

                if (!isCorrent) {
                    link.isCompleted = true;
                    isCorrent = link.isCorrent;
                }

                return link;
            }
        }
    }
}());