(function() {
    'use strict';

    angular
        .module('app')
        .factory('linksFactory', linksFactory)

    /** @ngInject */
    function linksFactory() {

        return function (urlBase) {
            return [
                {
                    ativo: true,
                    descricao: 'Dados<br>Pessoais',
                    url: function (cpf) {
                        return '#/cadastro/' + urlBase + '/' + cpf + '/dados-pessoais/'
                    }
                },
                {
                    ativo: true,
                    descricao: 'Dados<br>Complementares',
                    url: function (cpf) {
                        return '#/cadastro/' + urlBase + '/' + cpf + '/dados-complementares'
                    }
                },
                {
                    ativo: true,
                    descricao: 'Bens',
                    url: function (cpf) {
                        return '#/cadastro/' + urlBase + '/' + cpf + '/bens'
                    }
                },
                {
                    ativo: true,
                    descricao: 'Cartão<br>Autógrafo',
                    url: function (cpf) {
                        return '#/cadastro/' + urlBase + '/' + cpf + '/cartao-autografo';
                    }
                },
                {
                    ativo: true,
                    descricao: 'Renda',
                    url: function (cpf) {
                        return '#/cadastro/' + urlBase + '/' + cpf + '/renda';
                    }
                },
                {
                    ativo: true,
                    descricao: 'Análise<br>Cadastral',
                    url: function (cpf) {
                        return '#/cadastro/' + urlBase + '/' + cpf + '/analise-cadastral';
                    }
                },
                {
                    ativo: true,
                    descricao: 'Resumo',
                    url: function (cpf) {
                        return '#/cadastro/' + urlBase + '/' + cpf + '/resumo';
                    }
                }
            ]
        }
    }

}());
