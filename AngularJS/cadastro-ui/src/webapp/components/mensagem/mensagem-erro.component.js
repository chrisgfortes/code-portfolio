(function () {
    'use strict';
    
    mensagemErroController.inject = []
    
    angular
        .module ('app')
        .component ('mensagemErro', {
            bindings: {
                mensagens: '<'
            },
            controller: mensagemErroController,
            templateUrl: './components/mensagem/mensagem-erro.html'
        });
    
        function mensagemErroController(){
            var vm = this;
    
            vm.$onInit = onInit;
    
            function onInit() {}
        }
} ());