(function () {
    'use strict';
    
    mensagemSucessoController.inject = []
    
    angular
        .module ('app')
        .component ('mensagemSucesso', {
            bindings: {
                mensagens: '<'
            },
            controller: mensagemSucessoController,
            templateUrl: './components/mensagem/mensagem-sucesso.html'
        });
    
        function mensagemSucessoController(){
            var vm = this;
    
            vm.$onInit = onInit;
    
            function onInit() {}
        }
} ());