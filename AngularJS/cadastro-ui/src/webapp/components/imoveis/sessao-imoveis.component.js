(function () {
    'use strict';

    sessaoImoveisController.$inject = ['$rootScope', '$routeParams', 'dialogs', 'moment', 'focus', 'imovel', 'imoveisUpdate'];


    angular
        .module('app')
        .component('sessaoImoveis', {
            transclude: true,
            bindings: {
                imoveis: '=',
                isUpdate: '<',
                readonly: '<',
                mostraSessao: '<'
            },
            controller: sessaoImoveisController,
            templateUrl: './components/imoveis/sessao-imoveis.html'
        });

    function sessaoImoveisController($rootScope, $routeParams, dialogs, moment, focus, imovel, imoveisUpdate) {
        var vm = this;

        vm.salvar = salvar;
        vm.cancelar = cancelar;
        vm.editar = editar;
        vm.visualizar = visualizar;
        vm.novo = novo;
        vm.remover = remover;
        vm.imovel = {};
        vm.imovelEdicao = {};
        vm.mostrarImovelForm = false;
        vm.$onInit = onInit;

        function onInit() {
            vm.imoveis = vm.imoveis || [];

            imovel
                .tipos()
                .then(function (res) {
                    vm.tiposImovel = res.data;
                    vm.imoveis = vm.imoveis.map(adicionaDescricaoTipoDeImovel);
                });
        }

        function adicionaDescricaoTipoDeImovel (imovel) {
            var descricaoTipoDeImovel = vm.tiposImovel.filter(filtrarImovelSelecionado(imovel))[0];
            imovel.descricaoTipoDeImovel = descricaoTipoDeImovel.descricao;
            return imovel;
        }

        function filtrarImovelSelecionado(imovel) {
            return function (tipoImovel) {
                return tipoImovel.valor == imovel.tipo;
            }
        }

        function salvar(imovel) {
            var _imovel = angular.copy(imovel);

            if (_imovel.modoEdicao) {
                atualizarListaImoveis(_imovel)
            } else {
                vm.imoveis.push(_imovel);
            }

            vm.imovel = {};
            vm.mostrarImovelForm = false;
            focus('novoImovel');
        }

        function cancelar() {
            vm.imovel = {};

            vm.imoveis = vm.imoveis.map(mudarModoEdicaoParaFalse);

            vm.mostrarImovelForm = false;
            focus('novoImovel');
        }

        function visualizar(imovel) {
            vm.mostrarImovelForm = true;
            vm.apenasLeitura = true;
            vm.imovel = angular.copy(imovel);
        }

        function mudarModoEdicaoParaFalse(i) {
            i.modoEdicao = false;
            return i;
        }

        function editar(imovel) {
            vm.mostrarImovelForm = true;
            vm.imovel = angular.copy(imovel);
            focus('tipoImovel');
        }

        function novo() {
            vm.mostrarImovelForm = true;
            focus('tipoImovel');
        }

        function atualizarListaImoveis(imovel) {
            vm.imoveis = vm.imoveis.map(atualizaModoEdicaoImovel(imovel));
        }

        function atualizaModoEdicaoImovel(imovel) {
            return function (item) {
                if (item.modoEdicao) {
                    imovel.modoEdicao = false;
                    return imovel;
                }
                return item;
            }
        }

        function remover(imovel) {
            var confirmacao = dialogs.confirm('Confirmar', 'Confirma a remoção do imóvel?');

            confirmacao
                .result
                .then(removerDaLista(imovel))
                .catch(desfazerModoExclusao)
                .finally(function () {
                    focus('novoImovel');
                });

        }

        function modoExclusao(item) {
            return !item.modoExclusao;
        }

        function removerDaLista(imovel) {
            if (vm.isUpdate && !!imovel.id) {
                return excluirImovelDoSau(imovel.id);
            } else {
                return function () {
                    vm.imoveis = vm.imoveis.filter(modoExclusao);
                }
            }
        }

        function excluirImovelDoSau(id) {
            return function (){
                imoveisUpdate
                .excluir($routeParams.cpf, id)
                .then(function () {
                    vm.imoveis = vm.imoveis.filter(modoExclusao);
                });
            }
        }

        function desfazerModoExclusao() {
            vm.imoveis = vm.imoveis.filter(desabilitarModoExlucao);
        }

        function desabilitarModoExlucao(item) {
            item.modoExclusao = false

            return item;
        }

        $rootScope.$on('atualizar-descricao-tipo-imovel', function (e, imoveis) {
            vm.imoveis = imoveis.map(adicionaDescricaoTipoDeImovel);
        });
    }
}());
