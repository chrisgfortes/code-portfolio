(function () {
    'use strict';

    angular
        .module('app')
        .service('imovelConvert', imovelConvert)

    function imovelConvert() {

        this.convertGet = convertGet;
        this.convertPost = convertPost;

        function convertGet(imoveis) {
            return imoveis;
        }

        function convertPost(imoveis) {
            imoveis = imoveis.map(function(imovel) {
                if(!!imovel.vendedor && !validarVendedor(imovel.vendedor)){
                    delete imovel.vendedor;
                }
                return imovel;
            });
            return imoveis;
        }

        function validarVendedor(vendedor) {
            if (!vendedor.nome && !vendedor.cpfCnpj) {
                return false;
            }
            
            return true;
        }
    }

}());
