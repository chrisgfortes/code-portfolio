(function () {
    'use strict';

    imovelController.$inject = ['moment', '$rootScope', 'imovel', '$routeParams', 'dialogs', 'focus'];

    angular
        .module('app')
        .component('dadosImoveis', {
            bindings: {
                imovel: '<',
                onSalvar: '&',
                onCancelar: '&'
            },
            controller: imovelController,
            templateUrl: './components/imoveis/dados-imoveis.html'
        });

    function imovelController(moment, $rootScope, imovel, $routeParams, dialogs, focus) {
        var vm = this;
        vm.$onInit = onInit;
        vm.salvar = salvar;
        vm.cancelar = cancelar;
        vm.validarCpf = validarCpf;
        vm.tiposImovel = [];

        function onInit () {
            imovel
                .tipos()
                .then(function (res) {
                    vm.tiposImovel = res.data;
                });
            imovel
                .tiposDestinacoes()
                .then(function (res) {
                    vm.tiposDestImovel = res.data;
                });
            imovel
                .tiposSituacao()
                .then(function (res) {
                    vm.tiposSituacaoImovel = res.data;
                });
                
            vm.validarObrigatoriedade();
        }

        function salvar () {
            var descricaoTipoImovel = vm.tiposImovel.filter(function (tipoImovel) {
                return tipoImovel.valor == vm.imovel.tipo;
            })[0];
            vm.imovel.descricaoTipoDeImovel = descricaoTipoImovel.descricao;

            vm.onSalvar({imovel: angular.copy(vm.imovel)});
        }

        function cancelar () {
            vm.onCancelar();
        }

        function validarCpf(cpf) {
            if (cpf == $routeParams.cpf) {
                var notificacao = dialogs.notify('Atenção', 'O CPF não pode ser igual o do cadastrado');
                notificacao
                .result
                .then(function () {
                    vm.imovel.vendedor.cpfCnpj = "";
                    focus('cpfCnpjVendedorImovel');
                });

                return false;
            }
        }

        vm.validarObrigatoriedade = function() {
            if (!!vm.imovel.vendedor) {
                if(!!vm.imovel.vendedor.nome && vm.imovel.vendedor.nome.length > 0){
                    vm.isRequired = true;
                    return;
                }
                if(!!vm.imovel.vendedor.cpfCnpj && vm.imovel.vendedor.cpfCnpj.length > 0){
                    vm.isRequired = true;
                    return;
                }
                vm.isRequired = false;
                delete vm.imovel.vendedor;
            }
        }
    }
}());
