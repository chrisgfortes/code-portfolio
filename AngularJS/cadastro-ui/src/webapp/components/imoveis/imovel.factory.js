(function(){
    'use strict';

    angular
        .module('app')
        .factory('imovel', imovel)

    /** @ngInject */
    function imovel($http, HOST){

        return {
            tipos: tipos,
            tiposDestinacoes: tiposDestinacoes,
            tiposSituacao: tiposSituacao
        }

        function tipos(){
            return $http.get(urlBase() + '/imovel/tipos');
        }

        function tiposDestinacoes () {
            return $http.get(urlBase() + '/imovel/destinacoes');
        }

        function tiposSituacao () {
            return $http.get(urlBase() + '/imovel/situacoes');
        }

        function urlBase() {
            return HOST.pessoa + '/fisica';
        }
    }

}());