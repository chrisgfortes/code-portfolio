(function () {
    'use strict';

    listaImoveisController.$inject = ['$rootScope', 'dialogs'];

    angular
        .module('app')
        .component('listaImoveis', {
            bindings: {
                imoveis: '=',
                readonly: '<',
                onEditar: '&',
                onVisualizar: '&',
                onNovo: '&',
                onRemover: '&',
                escondeNovo: '<'
            },
            controller: listaImoveisController,
            templateUrl: './components/imoveis/lista-imoveis.html'
        });

    function listaImoveisController($rootScope, dialogs) {
        var vm = this;

        vm.remover = remover;
        vm.editar = editar;
        vm.visualizar = visualizar;
        vm.novo = novo;
        vm.$onInit = onInit;

        function onInit() {
            vm.imoveis = vm.imoveis || [];
        }

        function novo () {
            vm.onNovo();
        }

        function remover(imovel) {
            imovel.modoExclusao = true;
            vm.onRemover({ imovel: angular.copy(imovel) });
        }

        function editar(imovel) {
            vm.imoveis = vm.imoveis.map(limpaModoEdicaoVisualizacao);
            imovel.modoEdicao = true;
            vm.onEditar({ imovel: angular.copy(imovel) });
        }

        function visualizar(imovel) {
            vm.imoveis = vm.imoveis.map(limpaModoEdicaoVisualizacao);

            imovel.modoVisualizacao = true;

            vm.onVisualizar({
                imovel: angular.copy(imovel)
            });
        }

        function limpaModoEdicaoVisualizacao(_imovel) {
            _imovel.modoEdicao = false;
            _imovel.modoVisualizacao = false;
            return _imovel;
        }
    }
}());
