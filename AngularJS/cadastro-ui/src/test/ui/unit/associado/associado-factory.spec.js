describe('Factory: associado', function () {

    var associado;
    var httpBackend;
    var HOST;

    beforeEach(module('app'));

    beforeEach(inject(function (_associado_, _$httpBackend_, _HOST_) {
        associado = _associado_;
        httpBackend = _$httpBackend_;
        HOST = _HOST_;

        httpBackend.whenGET(/\.html$/).respond('');
    }));

    afterEach(function () {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('deve está definido', function () {
        expect(associado).toBeDefined();
    });

    it('.buscar()', function() {
        var modelsResponse = [{}];
        var matricula = '123456798'

        httpBackend
            .expectGET(HOST.terceiro + 'terceiro?tipo=MATRICULA&valor=' + matricula)
            .respond(200, modelsResponse);

        associado
            .buscar(matricula)
            .then(function(associado) {
                expect(associado).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.situacoes()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.associado +'situacoes-associacao')
            .respond(200, modelsResponse);

        associado
            .situacoes()
            .then(function(situacoes) {
                expect(situacoes).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.buscarMatricula()', function() {
        var modelsResponse = matriculaFake();
        var cpf = '123456798'

        httpBackend
            .expectGET(HOST.associado + '?cpfCnpj=' + cpf)
            .respond(200, modelsResponse);

        associado
            .buscarMatricula(cpf)
            .then(function(associado) {
                expect(associado).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    function matriculaFake() {
        return {
            "matricula": {
                "cpfCnpj": "88869538796",
                "cooperativa": 566,
                "matricula": 145645,
                "posto": 2
            }
        }
    }
});
