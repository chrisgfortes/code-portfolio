describe('Component: Dados Matrícula', function () {

    var $componentController,
        AuthFactory,
        cooperativasFactory,
        ctrl,
        matriculaFactory,
        $q;

    var enderecos = getEnderecos();

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _AuthFactory_, _cooperativasFactory_, _matricula_, _$q_) {
        $componentController = _$componentController_;
        AuthFactory = _AuthFactory_;
        cooperativasFactory = _cooperativasFactory_;
        matriculaFactory = _matricula_('associado');
        $q = _$q_;

        var bindings = {
            matricula: {},
            readonly: false,
            factory: matriculaFactory
        };

        ctrl = $componentController('dadosMatricula', {
            AuthFactory: _AuthFactory_,
            cooperativasFactory: _cooperativasFactory_,
            $routeParams: {cpf: '12345678912'},
        }, bindings);

        ctrl.matricula = {
            numero: 135321,
            posto: 2,
            enderecoPrincipal: "",
            enderecoEmpostamento: ""
        };

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('deve definir os bindings', function () {
        expect(ctrl.matricula).toBeDefined();
        expect(ctrl.readonly).toBeDefined();
    });

    it('deve buscar endereços', inject(function($httpBackend, HOST) {
        $httpBackend.whenGET(/\.html$/).respond('');
        var cpf = '12345678912';

        $httpBackend
          .expectGET(HOST.terceiro + cpf + '/enderecos')
          .respond(200, getEnderecos());

        $httpBackend.flush();

        expect(ctrl.enderecos).toEqual(getEnderecos());
        expect(ctrl.matricula.enderecoEmpostamento).toEqual("Não informado.");
        expect(ctrl.matricula.enderecoPrincipal).toEqual("Rua dos Abacates, 123 - CEP: 96745000");

    }));

    it('deve setar matricula como vazia caso não seja informada', function () {
        ctrl.matricula = undefined;

        ctrl.$onInit();

        expect(ctrl.matricula).toEqual({});
    });
});

function getEnderecos() {
    return [
        {
            bairro: "Bom Jesus",
            caixaPostal: 0,
            cep: 96745000,
            codigoCidade: 0,
            complemento: "string",
            empostamento: false,
            enderecoSemNumero: false,
            estado: "RS",
            idEmpostamento: true,
            logradouro: "Rua dos Abacates",
            numero: "123",
            principal: true,
            resideDesde: "2017-06-12",
            situacaoEndereco: "PROPRIA",
            tipoEndereco: "RESIDENCIAL",
            valorAluguel: 0
        }
    ]
}
