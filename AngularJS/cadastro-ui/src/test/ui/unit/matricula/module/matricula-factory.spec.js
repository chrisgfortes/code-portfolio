describe('factory: matricula', function () {

    var matricula,
        moment,
        httpBackend,
        HOST;

    beforeEach(module('app'));

    beforeEach(inject(function ($httpBackend, _matricula_, _HOST_, _moment_) {
        matricula = _matricula_('associado');
        HOST = _HOST_;
        moment = _moment_;
    }));

    it('.salvar(cpf, matricula)', inject(function ($httpBackend) {
        $httpBackend.whenGET(/\.html$/).respond('');
        var cpf = '83084412545';
        var matriculaResponse = matriculaPost();
        var dadosFake = matriculaFake();

        $httpBackend
            .expectPOST(HOST.associado + cpf)
            .respond(201, dadosFake);

        matricula
            .salvar(cpf, matriculaResponse)
            .then(function (response) {
                var dados = response.data;

                expect(dados).toEqual(dadosFake);
            });

        $httpBackend.flush();
    }));

    function matriculaFake() {
        return {
            numero: 153135,
            dadosCadastrais: {
                dataAssociacao: moment('2017-08-20')
            }
        }
    }

    function matriculaPost() {
        return {
            numero: 153135,
            dadosCadastrais: {
                dataAssociacao: '2017-08-20'
            }
        }
    }

});
