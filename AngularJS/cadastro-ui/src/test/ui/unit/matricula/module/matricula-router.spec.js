describe('Router: matricula-router', function() {
    var matricula,
        $route,
        $injector,
        HOST,
        linksFactory,
        rotaService,
        $q;

    beforeEach(module('app'));

    beforeEach(inject(function(_$route_, _matricula_, _$injector_, $httpBackend, _HOST_, _linksFactory_, _rotaService_, _$q_) {
        $route = _$route_;
        matricula = _matricula_;
        $injector = _$injector_;
        HOST = _HOST_;
        linksFactory = _linksFactory_;
        rotaService = _rotaService_;
        $q = _$q_;
    }));

    describe('Rota de Associado', function() {
        testarRota('associado');
    });

    function testarRota(nomeDaRota) {
        it('Deve testar matricula-router', function() {
            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/matricula'];

            expect(rota.controller).toBe('matriculaController');
            expect(rota.templateUrl).toBe('./modules/matricula/matricula.html');
        });

        it('deve verificar se é terceiro', function() {
            var cpf = '93994468248';

            var deferred = $q.defer();
            deferred.resolve({});
            spyOn(rotaService, 'isNotTerceiro').and.returnValue(deferred.promise);

            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/matricula'];
            $route = {
                current: {
                    params: {
                        cpf: cpf
                    }
                }
            }

            rota.resolve.isNotTerceiro($route, rotaService);
            expect(rotaService.isNotTerceiro).toHaveBeenCalledWith(cpf);
        });

        it('deve verificar se já é associado', function() {
            var cpf = '93994468248';

            var deferred = $q.defer();
            deferred.resolve({});
            spyOn(rotaService, 'isAssociado').and.returnValue(deferred.promise);

            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/matricula'];
            $route = {
                current: {
                    params: {
                        cpf: cpf
                    }
                }
            }

            rota.resolve.isAssociado($route, rotaService);
            expect(rotaService.isAssociado).toHaveBeenCalledWith(cpf);
        });

        it('Deve definir a factory', function() {
            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/matricula'];

            var factory = matricula(nomeDaRota);
            var retorno = rota.resolve.matriculaFactory(matricula);

            expect(Object.getOwnPropertyNames(retorno)).toEqual(Object.getOwnPropertyNames(factory));
        });
    }

    function mapearRetorno(urls, cpf) {
        urls = urls.map(function(item) {
            item.url = item.url(cpf);
            return item
        })
        return urls;
    }
});
