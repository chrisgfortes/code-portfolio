describe('controller: matriculaController', function () {

    var ctrl, $rootScope, matriculaFactory, $q, $scope, AuthFactory, linksFactory, urls, pessoa;

    var cpf = '12345678912';
    var erros = [{}];
    var matricula = {}

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_$controller_, _$rootScope_, _matricula_, _$q_, _AuthFactory_, _linksFactory_, _pessoa_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        matriculaFactory = _matricula_('associado');
        urls = _linksFactory_('associado');
        $q = _$q_;
        AuthFactory = _AuthFactory_;
        pessoa = _pessoa_;

        var deferred = $q.defer();
        deferred.resolve({
            data: matricula
        });

        spyOn(AuthFactory, 'getUsuario').and.returnValue({ nome: 'camila', cooperativa: 566 });


        ctrl = $controller('matriculaController', {
            $scope: $scope,
            matriculaFactory: matriculaFactory,
            urls: urls,
            $routeParams: { cpf: '12345678912' },
            matriculaInfo: {}
        });

    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
        expect(ctrl.associado.matricula.cooperativa).toBeDefined();
    });

    it('deve chamar .salvar(matricula)', function () {
        var deferred = $q.defer();
        deferred.resolve({});
        spyOn(matriculaFactory, 'salvar').and.returnValue(deferred.promise);

        var statusPessoa = pessoaStatusFake();
        var deferredStatus = $q.defer();
        deferredStatus.resolve(statusPessoa);
        spyOn(pessoa, 'status').and.returnValue(deferredStatus.promise);

        spyOn($rootScope, '$emit').and.callThrough();

        var modal = {
            enable: true,
            cpf: statusPessoa.cpfCnpj,
            nome: statusPessoa.nome,
            titulo: 'Associado',
            tipo: 'associado'
        };

        var matricula = {
            cpf: cpf
        };

        ctrl.salvar(matricula);

        $rootScope.$apply();

        expect(matriculaFactory.salvar).toHaveBeenCalledWith(ctrl.cpf, matricula);
        expect(pessoa.status).toHaveBeenCalledWith(ctrl.cpf, 'terceiro');
        expect($rootScope.$emit).toHaveBeenCalledWith('usuario-pendente', modal);
    });

    it('deve mostrar messagem de validação, envida pelo body', function () {
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                body: erros
            }
        });

        spyOn(matriculaFactory, 'salvar')
            .and
            .returnValue(deferred.promise);

        var matricula = {
            numero: 123553
        };

        ctrl.salvar(matricula);

        $scope.$apply();

        expect(matriculaFactory.salvar).toHaveBeenCalledWith(ctrl.cpf, matricula);
        expect(ctrl.erros).toEqual(erros);
    });

    it('deve mostrar messagem de validação, envida pelo content', function () {
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                content: erros
            }
        });

        spyOn(matriculaFactory, 'salvar')
            .and
            .returnValue(deferred.promise);

        var matricula = {
            numero: 53153
        };

        ctrl.salvar(matricula);

        $scope.$apply();

        expect(matriculaFactory.salvar).toHaveBeenCalledWith(ctrl.cpf, matricula);
        expect(ctrl.erros).toEqual(erros);
    });

    it('deve mostrar messagem de validação, envida pelo message', function () {
        var mensagemDeErro = 'Erro de validação';
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                message: mensagemDeErro
            }
        });

        spyOn(matriculaFactory, 'salvar')
            .and
            .returnValue(deferred.promise);

        var matricula = {
            numero: 53153
        };

        ctrl.salvar(matricula);

        $scope.$apply();

        expect(matriculaFactory.salvar).toHaveBeenCalledWith(ctrl.cpf, matricula);
        expect(ctrl.erros[0].message).toEqual(mensagemDeErro);
    });

    it('não deve mostrar messagem de validação', function () {
        var deferred = $q.defer();
        deferred.reject({
            status: 400,
            data: {
                content: erros
            }
        });

        spyOn(matriculaFactory, 'salvar')
            .and
            .returnValue(deferred.promise);

        var matricula = {
            numero: 84652
        };

        ctrl.salvar(matricula);

        $scope.$apply();

        expect(matriculaFactory.salvar).toHaveBeenCalledWith(ctrl.cpf, matricula);
        expect(ctrl.erros).not.toBeDefined();
    });


    function pessoaStatusFake() {
        return {
            cpfCnpj: "93994468248",
            nome: "Lancelot de Jesus",
            status: "OK"
        }
    }

});
