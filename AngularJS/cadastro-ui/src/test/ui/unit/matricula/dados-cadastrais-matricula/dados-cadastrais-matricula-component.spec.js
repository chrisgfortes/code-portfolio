describe('Component: dadosCadastraisMatricula', function () {

    var $rootScope,
        associado,
        pessoa,
        $q;

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_$componentController_, _$rootScope_, _$q_, _associado_, _moment_, _pessoa_) {
        $componentController = _$componentController_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        associado = _associado_;
        pessoa = _pessoa_;

        ctrl = $componentController('dadosCadastraisMatricula', {
            associado: _associado_,
            moment: _moment_,
            pessoa: _pessoa_
        }, {
                dadosCadastrais: {}
            });
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('deve definir talao', function () {
        ctrl.$onInit();

        expect(ctrl.dadosCadastrais).toBeDefined();
        expect(ctrl.dadosCadastrais.situacaoAssociacao).toBe('ATIVO');
    });

    it('deve serem chamados ao inicializar o componente', function () {
        var dados = [{}];

        spyOn(associado, 'situacoes').and.callFake(respostaFake(dados));

        ctrl.$onInit();

        $rootScope.$apply();

        expect(associado.situacoes).toHaveBeenCalled();
        expect(ctrl.situacoes.length).toBe(1);
    });

    it('deve aceitar data valida em validaData()', function () {
        var data = moment('2017-06-01');
        ctrl.dadosCadastrais = { dataAssociacao: moment() };

        ctrl.validaData(data);
        expect(ctrl.dadosCadastrais.dataAssociacao).not.toBeUndefined();
    });

    it('não deve aceitar data inválida em validaData()', function () {
        var data = moment("7898-45-45");
        ctrl.dadosCadastrais = { dataAssociacao: moment() };

        ctrl.validaData(data);
        expect(ctrl.dadosCadastrais.dataAssociacao).toBeUndefined();
    });

    it('não deve consultar o associado sem matricula ', function () {
        var dados = {};
        var matricula = undefined
        ctrl.associados = undefined

        spyOn(associado, 'buscar').and.callThrough();

        ctrl.buscarMatricula(matricula);

        $rootScope.$apply();

        expect(associado.buscar).not.toHaveBeenCalled();
    });

    it('deve consultar o associado pela matricula ', function () {
        var dados = { nome: 'Fulano'};
        var matricula = '123456'
        ctrl.associados = undefined

        var deferred = $q.defer()
        deferred.resolve(dados)

        spyOn(associado, 'buscar').and.returnValue(deferred.promise)

        ctrl.buscarMatricula(matricula);

        $rootScope.$apply();

        expect(associado.buscar).toHaveBeenCalledWith(matricula);
        expect(ctrl.dadosCadastrais.nomeProponente).toBe(dados.nome);
    });

    it('deve falhar ao tentar consultar o associado pela matricula ', function () {
        var dados = {};
        var matricula = '123456'
        ctrl.associados = undefined
        ctrl.dadosCadastrais.nomeProponente = undefined

        var deferred = $q.defer()
        deferred.reject({})

        spyOn(associado, 'buscar').and.returnValue(deferred.promise)

        ctrl.buscarMatricula(matricula);

        $rootScope.$apply();

        expect(associado.buscar).toHaveBeenCalledWith(matricula);
        expect(ctrl.dadosCadastrais.nomeProponente).toBe('');
        
    });
});