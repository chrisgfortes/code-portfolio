describe('Component: Navegacao', function () {

    var $componentController,
        links,
        $location,
        cpf = '53576565418',
        ctrl;

    beforeEach(module('app'));
    beforeEach(inject(function (_$componentController_, linksFactory, _$location_) {
        $componentController = _$componentController_;
        links = linksFactory('terceiro');
        $location = _$location_;

        spyOn($location, 'path').and.returnValue('/cadastro/terceiro/' + cpf + '/bens')

        var bindings = {
            links: links
        }

        ctrl = $componentController('navegacao', {
            $location: _$location_,
            $routeParams: {
                cpf: cpf
            }
        }, bindings);

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o caminho atual', function () {

        expect(ctrl.links[0].isCorrent).toBe(false);
        expect(ctrl.links[0].isCompleted).toBe(true);

        expect(ctrl.links[1].isCorrent).toBe(false);
        expect(ctrl.links[1].isCompleted).toBe(true);

        expect(ctrl.links[2].isCorrent).toBe(true);
        expect(ctrl.links[2].isCompleted).toBe(true);

        expect(ctrl.links[3].isCorrent).toBe(false);
        expect(ctrl.links[3].isCompleted).toBe(false);
    });


    it('deve mostrar somente os links ativos', function () {
        var quantidadeAnterior = ctrl.links.length;

        ctrl.links = ctrl.links.map(function (link) {
            if (link.descricao == 'Bens')
                link.ativo = false;

            return link;
        });

        ctrl.$onInit();

        var quantidadeEsperado = quantidadeAnterior - 1;

        expect(ctrl.links.length).toBe(quantidadeEsperado);
    });
});
