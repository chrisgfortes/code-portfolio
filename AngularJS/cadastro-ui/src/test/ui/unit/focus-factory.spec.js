describe('Service: focus', function () {

    var focus;
    var $timeout;
    var $window;

    beforeEach(module('app'));

    beforeEach(inject(function (_focus_, _$timeout_, _$window_) {
        focus = _focus_;
        $timeout = _$timeout_;
        $window = _$window_;
    }));

    it('deve está definido', function () {
        expect(focus).toBeDefined();
    });

    it('deve chamar getElementById', function () {
        var document = $window.document;
        var id = 'ID';

        spyOn(document, 'getElementById').and.returnValue({ focus: function () { } });

        focus(id);

        $timeout.flush();

        expect(document.getElementById).toHaveBeenCalledWith(id);
    });

    it('deve chamar getElementsByName', function () {
        var document = $window.document;
        var id = 'ID';

        spyOn(document, 'getElementsByName').and.returnValue({ focus: function () { } });

        focus(id);

        $timeout.flush();

        expect(document.getElementsByName).toHaveBeenCalledWith(id);
    });
});