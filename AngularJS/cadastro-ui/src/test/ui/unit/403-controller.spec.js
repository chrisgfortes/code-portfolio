describe('controller: 403Controller', function () {

    var ctrl, $rootScope;


    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_$controller_, _$rootScope_) {
        $rootScope = _$rootScope_;
        $controller = _$controller_;

        ctrl = $controller('403Controller', null);
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
        expect($rootScope.isLogged).toBe(false);
    });

});
