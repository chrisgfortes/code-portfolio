
describe('Component: tooltip', function () {

    var $componentController,
        contato,
        ctrl;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_) {
        var bindings = {
            texto: {}
        };

        $componentController = _$componentController_;
        ctrl = $componentController('tooltip', null, bindings);
        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });
});
