describe('Component: dadosPoderes', function () {

    var $componentController,
        ctrl,
        HOST;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _HOST_) {
        $componentController = _$componentController_;
        HOST = _HOST_;

        var bindings = {
            poderes: {},
            host: HOST.terceiro
        };

        ctrl = $componentController('dadosPoderes', null, bindings);

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o poderes no bindings', function () {
        expect(ctrl.poderes).toBeDefined();
    });

    it('deve carregar tipo poder', inject(function (HOST, $httpBackend) {
        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;

        var tipos = [{
                valor: "CONJUNTA",
                descricao: "Conjunta"
              },
              {
                valor: "INDIVIDUAL",
                descricao: "Individual"
              },
              {
                valor: "PREJUDICADO",
                descricao: "Prejudicado"
              }];

        httpBackend
            .expectGET(HOST.pessoa + '/fisica/cartao-autografo/permissao-poder')
            .respond(200, tipos);

        httpBackend.flush();

        expect(ctrl.factory).toBeDefined();
        expect(ctrl.opcoes).toEqual(tipos);

        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    }));

    it('não deve ter campos required', function () {
        ctrl.poderes.cheques = {};

        ctrl.validar();

        expect(ctrl.isRequired).toBe(false);
    });

    it('não deve ter campos required limpando atributos vazios', function () {
        ctrl.poderes.cheques = {
            "emitir": ""
        };

        ctrl.validar();

        expect(ctrl.isRequired).toBe(false);
    });

    it('deve ter campos required', function () {
        ctrl.poderes.cheques = {
            "emitir": "INDIVIDUAL"
        };

        ctrl.validar();

        expect(ctrl.isRequired).toBe(true);
    })

    it('deve selecionar todos', function () {
        var poderesAlterado = getPoderes('INDIVIDUAL');
        ctrl.poderes = getPoderes('CONJUNTA');

        ctrl.selectAll('INDIVIDUAL');

        expect(ctrl.poderes).toEqual(poderesAlterado);
    })

    it('deve desmarcar todos', function () {
        var descricao = "INDIVIDUAL";
        ctrl.listaPoderes = getListaPoderes(descricao);
        ctrl.poderes = getPoderes(descricao);
        ctrl.allSelected = descricao;

        ctrl.selectAll(descricao);

        expect(ctrl.poderes).toEqual({});
    })

    it('deve desmarcar selecionado', function () {
        spyOn(ctrl, 'validar').and.callThrough();

        var descricaoPoder = "CONJUNTA";
        var secao = "cheques";
        var poder = "emitir";

        var poderesOriginal = getPoderes(descricaoPoder);
        var listaPoderesOriginal = getListaPoderes(descricaoPoder);
        ctrl.poderes = getPoderes(descricaoPoder);
        ctrl.listaPoderes = getListaPoderes(descricaoPoder);

        var $event= {
            target:{ value: descricaoPoder }
        }

        ctrl.deselect($event, secao, poder, 0);

        expect(ctrl.poderes).not.toEqual(poderesOriginal);
        expect(ctrl.listaPoderes[secao][0]['selected']).not.toEqual(listaPoderesOriginal[secao][0]['selected']);
        expect(ctrl.listaPoderes[secao][0]['selected']).toEqual('');
        expect(ctrl.poderes[secao][poder]).not.toBeDefined();
        expect(ctrl.validar).toHaveBeenCalled();
    })

    it('deve trocar selecao de poder', function () {
        var descricaoPoder = "CONJUNTA";
        var secao = "cheques";
        var poder = "emitir";

        var poderesOriginal = getPoderes(descricaoPoder);
        var listaPoderesOriginal = getListaPoderes(descricaoPoder);
        ctrl.poderes = getPoderes(descricaoPoder);
        ctrl.listaPoderes = getListaPoderes(descricaoPoder);

        var $event= {
            target:{ value: "INDIVIDUAL" }
        }

        ctrl.deselect($event, secao, poder, 0);

        expect(ctrl.poderes).not.toEqual(poderesOriginal);
        expect(ctrl.listaPoderes[secao][0]['selected']).not.toEqual(listaPoderesOriginal[secao][0]['selected']);
        expect(ctrl.listaPoderes[secao][0]['selected']).toEqual('INDIVIDUAL');
        expect(ctrl.poderes[secao][poder]).toBeDefined();
        expect(ctrl.poderes[secao][poder]).toEqual('INDIVIDUAL');
    })

    it('deve atualizar listaPoderes se evento for disparado', inject(function ($rootScope) {
        var poderes = getPoderes('INDIVIDUAL');
        ctrl.listaPoderes = getListaPoderes('CONJUNTA');

        $rootScope.$broadcast('atualizar-lista-poderes', poderes);

        expect(ctrl.listaPoderes).toEqual(getListaPoderes('INDIVIDUAL'));
        expect(ctrl.allSelected).toEqual('');
        expect(ctrl.marcarTodos).toEqual('');
    }));

});

function getPoderes(valor) {
    return {
       cheques: {
           emitir: valor,
           endossar: valor,
           caucionar: valor,
           avalizar: valor
       },
       autorizacoes: {
           contratoEmprestimo: valor,
           notaPromissoria: valor,
           debitos: valor
       },
       outrasAutorizacoes: {
           assinarContratos: valor,
           saque: valor,
           negociarContratos: valor,
           transferencia: valor,
           abrirEncerrarContas: valor,
           pagamentos: valor,
           solicitarExtrato: valor,
           entregarDocumentoPagamento: valor,
           retirarChequeDevolvido: valor
       }
   };
}

function getListaPoderes(valor) {
    return {
        cheques:[
            {
                descricao: "Emitir",
                contrato: "emitir",
                selected: valor
            },
            {
                descricao: "Endossar",
                contrato: "endossar",
                selected: valor
            },
            {
                descricao: "Caucionar",
                contrato: "caucionar",
                selected: valor
            },
            {
                descricao: "Avalizar",
                contrato: "avalizar",
                selected: valor
            }
        ],
        autorizacoes:[
            {
                descricao: "Contrato de Empréstimo",
                contrato: "contratoEmprestimo",
                selected: valor
            },
            {
                descricao: "Nota Promissória",
                contrato: "notaPromissoria",
                selected: valor
            },
            {
                descricao: "Débitos",
                contrato: "debitos",
                selected: valor
            }
        ],
        outrasAutorizacoes:[
            {
                descricao: "Assinar Contratos",
                contrato: "assinarContratos",
                selected: valor
            },
            {
                descricao: "Saque",
                contrato: "saque",
                selected: valor
            },
            {
                descricao: "Negociar Contratos",
                contrato: "negociarContratos",
                selected: valor
            },
            {
                descricao: "Transferência",
                contrato: "transferencia",
                selected: valor
            },
            {
                descricao: "Abrir Ou Encerrar Contas",
                contrato: "abrirEncerrarContas",
                selected: valor
            },
            {
                descricao: "Pagamentos",
                contrato: "pagamentos",
                selected: valor
            },
            {
                descricao: "Solicitar Extrato",
                contrato: "solicitarExtrato",
                selected: valor
            },
            {
                descricao: "Entregar Doc. Para Pgtos",
                contrato: "entregarDocumentoPagamento",
                selected: valor
            },
            {
                descricao: "Retirar Cheque Devolvido",
                contrato: "retirarChequeDevolvido",
                selected: valor
            }
        ]
    }
}
