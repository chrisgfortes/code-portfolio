describe('Component: sessaoPoderes', function() {

    var $componentController,
        dialogs,
        $q,
        $rootScope,
        ctrl;

    beforeEach(module('app'));

    beforeEach(inject(function(_$componentController_, _dialogs_, _$q_, _$rootScope_) {
        $componentController = _$componentController_;
        dialogs = _dialogs_;
        $q = _$q_;
        $rootScope = _$rootScope_;

        var bindings = {
            poderes: {}
        };

        ctrl = $componentController('sessaoPoderes', {
            dialogs: _dialogs_
        }, bindings);

        ctrl.$onInit();
    }));

    it('deve estar definido', function() {
        expect($componentController).toBeDefined();
    });

    it('deve definir o poderes no bindings', function() {
        expect(ctrl.poderes).toBeDefined();
    });
});
