describe('Component: dadosTitular', function () {

    var $componentController,
        ctrl, $q,
        titularidades,
        message,
        dialogs,
        $rootScope,
        $routeParams,
        cpf;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _titularidades_, _message_, _dialogs_, _$rootScope_, _$routeParams_, _$q_) {
        $componentController = _$componentController_;

        $q = _$q_;
        titularidades = _titularidades_;
        message = _message_;
        dialogs = _dialogs_;
        $rootScope = _$rootScope_;
        $routeParams = _$routeParams_;
        cpf = '34127844949';

        var bindings = {
            titular: {}
        };

        ctrl = $componentController('dadosTitular', null, bindings);

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve chamar o evento onSalvar', function () {
        var onSalvarSpy = jasmine.createSpy('onSalvar');

        var bindings = {
            onSalvar: onSalvarSpy
        };

        ctrl = $componentController('dadosTitular', null, bindings);

        ctrl.salvar();

        expect(onSalvarSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onCancelar', function () {
        var onCancelarSpy = jasmine.createSpy('onCancelar');

        var bindings = {
            onCancelar: onCancelarSpy
        };

        var ctrl = $componentController('dadosTitular', null, bindings);

        ctrl.cancelar();

        expect(onCancelarSpy).toHaveBeenCalled();
    });

    it('deve buscar matricula e existir', function () {
        var dados = {
            "cpfCnpj": "34127844949",
            "nome": "Cooperado Jose 1",
            "dataNascimento": "1958-03-15"
        };

        var getMatriculaPromise = $q.defer();
        getMatriculaPromise.resolve({
            data: dados
        });

        spyOn(titularidades, 'get').and.returnValue(getMatriculaPromise.promise);
        
        ctrl.getMatricula(19);

        $rootScope.$apply();

        expect(ctrl.titular.cpfCnpj).toBe(cpf);
        expect(ctrl.titular.nome).toBe('Cooperado Jose 1');
    });

    it('deve buscar matricula e não existir', function () {
        var dados = respostaFakeMatricula();

        var getMatriculaPromise = $q.defer();
        getMatriculaPromise.reject({
            status: 422
        });

        spyOn(titularidades, 'get').and.returnValue(getMatriculaPromise.promise);

        ctrl.getMatricula(26);

        $rootScope.$apply();

        expect(ctrl.titular.matricula).toBe(26);
    });

    it('deve ser undefined matricula', function () {
        ctrl.getMatricula();
        expect(ctrl.titular.cpfCnpj).toBeUndefined();
        expect(ctrl.titular.nome).toBeUndefined();
    });
});

function respostaFakeMatricula() {
    return {
        "cpfCnpj": "34127844949",
        "nome": "Cooperado Jose 1",
        "dataNascimento": "1958-03-15"
    }
}