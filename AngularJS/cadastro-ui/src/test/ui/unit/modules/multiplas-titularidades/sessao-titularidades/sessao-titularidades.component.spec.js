describe('Component: sessaoTitularidades', function () {

    var $componentController,
        ctrl, 
        $q,
        cpf = '34127844949',
        matricula = 19,
        $rootScope,
        associado,
        dialogs;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _$q_, _associado_, _$routeParams_, _$rootScope_, _dialogs_) {
        $componentController = _$componentController_;
        $q = _$q_;
        associado = _associado_;
        $routeParams = _$routeParams_;
        $rootScope = _$rootScope_;
        dialogs = _dialogs_;

        var bindings = {
            titularidades: []
        };

        ctrl = $componentController('sessaoTitularidades', {
            $routeParams: {
                cpf: cpf
            }
        }, bindings);
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve buscar Matricula', function () {
        var modelsResponse = respostaFakeMatricula();
        
        var deferred = $q.defer();
        deferred.resolve(modelsResponse);
        
        $routeParams = {
            cpf: cpf
        };

        spyOn(associado, 'buscarMatricula').and.returnValue(deferred.promise);

        associado.buscarMatricula($routeParams.cpf);

        ctrl.$onInit();
        $rootScope.$apply();
    });

    it('.salvar() deve salvar titular', function () {
        var titular = {
            "matricula": 35,
            "cpfCnpj": "37840916953",
            "nome": "Cooperado Jose4",
            "modoEdicao": false
        }

        ctrl.salvar(titular);

        expect(ctrl.titularidades.length).toBe(1);
    });

    it('.salvar() deve validar se o titular é o mesmo que cadastrou', function () {
        var titular = {
            "matricula": 35
        }

        ctrl.matriculaUser = 35;

        ctrl.salvar(titular);

        expect(ctrl.titular).toBeUndefined();
        expect(ctrl.mostrarTitularForm).toBeUndefined();
    });

    it('.salvar() modo de edicao false', function () {
        var titular = {
            "matricula": 35,
            "modoEdicao": false
        }

        ctrl.salvar(titular);

        expect(ctrl.titular).toBeUndefined();
        expect(ctrl.mostrarTitularForm).toBeUndefined();
    });

    it('.salvar() modo de edicao false, matricula correta', function () {
        var titular = {
            "matricula": 3,
            "modoEdicao": false,
            "nome": "Teste",
            "cpfCnpj": cpf
        }

        ctrl.salvar(titular);

        expect(ctrl.titular).toBeUndefined();
        expect(ctrl.mostrarTitularForm).toBeUndefined();
    });

    it('.salvar() modo de edicao true e matricula existe na lista', function () {
        var titular = {
            "matricula": 35,
            "modoEdicao": false,
            "nome": "Teste",
            "cpfCnpj": cpf
        }

        ctrl.titularidades = [titular];

        ctrl.salvar(titular);

        expect(ctrl.titular).toBeUndefined();
        expect(ctrl.mostrarTitularForm).toBeUndefined();
    });

    it('.salvar() modo de edicao true, e update de dados', function () {
        var titular = {
            "matricula": 35,
            "modoEdicao": true,
            "nome": "Teste",
            "cpfCnpj": cpf
        }

        ctrl.titularidades = [titular];

        ctrl.salvar(titular);
        
        expect(ctrl.titularidades[0].modoEdicao).toBe(false);
    });

    it('.salvar() modo de edicao true, e update de dados', function () {
        var titularFirst = {
            "matricula": 35,
            "modoEdicao": true,
            "nome": "Teste",
            "cpfCnpj": cpf
        }

        var titularSecond = {
            "matricula": 35,
            "modoEdicao": false,
            "nome": "Teste",
            "cpfCnpj": cpf
        }

        ctrl.titularidades = [
            titularFirst,
            titularSecond
        ];

        ctrl.salvar(titularFirst);

        expect(ctrl.titularidades[1]).toEqual(titularSecond);
    });

    it('deve CANCELAR', function () {
        var titular = {
            "matricula": 35,
            "modoEdicao": true,
            "nome": "Teste",
            "cpfCnpj": cpf
        };

        ctrl.mostrarTitularForm = true;

        ctrl.titularidades.push(titular);

        ctrl.cancelar();

        expect(ctrl.titularidades.length).toBe(1);
        expect(ctrl.titularidades[0].modoEdicao).toBe(false);
        expect(ctrl.mostrarTitularForm).toBe(false);
    });

    it('deve NOVO formulário', function () {
        ctrl.mostrarRestricaoAcatadaForm = false;

        ctrl.novo();

        expect(ctrl.mostrarTitularForm).toBe(true);
    });

    it('deve REMOVER', function () {
        var deferred = $q.defer();
        deferred.resolve('yes');
        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var titularFirst = {
            "matricula": 35,
            "modoEdicao": false,
            "nome": "Teste",
            "cpfCnpj": cpf,
            "modoExclusao": false
        }

        var titularSecond = {
            "matricula": 35,
            "modoEdicao": false,
            "nome": "Teste",
            "cpfCnpj": cpf,
            "modoExclusao": false
        }

        ctrl.titularidades.push(titularFirst);
        ctrl.titularidades.push(titularSecond);

        ctrl.remover(titularFirst);

        $rootScope.$apply();

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.titularidades.length).toBe(2);
        expect(ctrl.titularidades[0]).toEqual(titularFirst);
    });

    it('deve REMOVER', function () {
        var deferred = $q.defer();
        deferred.reject();
        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var titularFirst = {
            "matricula": 35,
            "modoEdicao": false,
            "nome": "Teste",
            "cpfCnpj": cpf,
            "modoExclusao": false
        }

        var titularSecond = {
            "matricula": 35,
            "modoEdicao": false,
            "nome": "Teste",
            "cpfCnpj": cpf,
            "modoExclusao": false
        }

        ctrl.titularidades.push(titularFirst);
        ctrl.titularidades.push(titularSecond);

        ctrl.remover(titularFirst);

        $rootScope.$apply();

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.titularidades.length).toBe(2);
        expect(ctrl.titularidades[0]).toEqual(titularFirst);
    });

    it('deve ficar em MODO EDIÇÃO', function () {
        ctrl.mostrarRestricaoAcatadaForm = false;
        ctrl.referencia = null;

        var titularFirst = {
            "matricula": 35,
            "modoEdicao": false,
            "nome": "Teste",
            "cpfCnpj": cpf
        }

        ctrl.editar(titularFirst);

        expect(ctrl.mostrarTitularForm).toBe(true);
        expect(ctrl.titular).toEqual(titularFirst);
    });
});

function respostaFakeMatricula() {
    return {
        matricula: {
            matricula: 19
        }
    }
}