describe('Component: listaTitularidades', function () {

    var $componentController,
        ctrl, $q,
        titularidades,
        message,
        dialogs,
        $rootScope,
        $routeParams,
        cpf;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_) {
        $componentController = _$componentController_;

        cpf = '34127844949';

        var bindings = {
            titularidades: []
        };

        ctrl = $componentController('listaTitularidades', null, bindings);

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve chamar o evento onRemover', function () {
        var onRemoverSpy = jasmine.createSpy('onRemover');

        var bindings = {
            onRemover: onRemoverSpy
        };

        var ctrl = $componentController('listaTitularidades', null, bindings);

        var titular = {
            modoExclusao: false
        };

        var esperado = {
            titular: {
                modoExclusao: true
            }
        };

        ctrl.remover(titular);

        expect(onRemoverSpy).toHaveBeenCalledWith(esperado);
        expect(titular.modoExclusao).toBe(true);
    });

    it('deve chamar o evento onNovo', function () {
        var onNovoSpy = jasmine.createSpy('onNovo');

        var bindings = {
            onNovo: onNovoSpy
        };

        var ctrl = $componentController('listaTitularidades', null, bindings);

        ctrl.novo();

        expect(onNovoSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onEditar', function () {
        var onEditarSpy = jasmine.createSpy('onEditar');

        var bindings = {
            onEditar: onEditarSpy
        };

        var ctrl = $componentController('listaTitularidades', null, bindings);

        ctrl.titularidades = [{ modoEdicao: false }];

        var titular = {
            modoEdicao: false
        };

        var esperado = {
            titular: {
                modoEdicao: true
            }
        };

        ctrl.editar(titular);

        expect(onEditarSpy).toHaveBeenCalledWith(esperado);
        expect(titular.modoEdicao).toBe(true);
    });
});

function respostaFakeMatricula() {
    return {
        "cpfCnpj": "34127844949",
        "nome": "Cooperado Jose 1",
        "dataNascimento": "1958-03-15"
    }
}