describe('factory: Titularidades', function () {

    var HOST,
        titularidades,
        httpBackend;

    beforeEach(module('app'));

    beforeEach(inject(function ($httpBackend, _HOST_, _titularidades_) {
        httpBackend = $httpBackend;
        HOST = _HOST_;
        titularidades = _titularidades_;

        $httpBackend.whenGET(/\.html$/).respond('');
    }));

    afterEach(function () {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.get()', function () {
        var modelsResponse = respostaFakeMatricula();
        var matricula = 19;

        httpBackend
            .expectGET(HOST.cadastro + '/terceiros/terceiro?tipo=MATRICULA&valor=' + matricula)
            .respond(200, modelsResponse);

        titularidades
            .get(matricula)
            .then(function (correntista) {
                expect(correntista.data).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });
});

function respostaFakeMatricula() {
    return {
        "cpfCnpj": "34127844949",
        "nome": "Cooperado Jose 1",
        "dataNascimento": "1958-03-15"
    }
}