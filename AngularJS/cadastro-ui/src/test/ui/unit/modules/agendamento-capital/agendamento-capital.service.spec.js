describe('Service: AgendamentoCapitalConvert', function () {

    var agendamentoCapitalConverter;

    beforeEach(module('app'));

    beforeEach(inject(function (_agendamentoCapitalConverter_) {
        agendamentoCapitalConverter = _agendamentoCapitalConverter_;
    }));

    it('deve estar definido', function () {
        expect(agendamentoCapitalConverter).toBeDefined();
    });

    it('deve converter datas para outro formato', function () {
        var data = _agendamentoCapitalDataSave();
        var dataConverted = agendamentoCapitalConverter.get(data.toConvert);
        expect(dataConverted).toEqual(data.converted);
    });

    it('deve retornar mesmo objeto sem conversao', function () {
        var data = _agendamentoCapitalDataSave();
        var dataConverted = agendamentoCapitalConverter.get(data.empty);
        expect(dataConverted).toEqual(data.empty);
    });

    it('deve converter - post', function () {
        var data = _agendamentoCapitalDataSave();
        var dataConverted = agendamentoCapitalConverter.post(data.toConvert);
        expect(dataConverted).toEqual(data.convertPost);
    });
});

function _agendamentoCapitalDataSave() {
    var converted = {
        "renovacaoAutomatica": false,
        "contaDebito": '70165',
        "matricula": 115975,
        "tipoLancamentoCapital": '503',
        "capitalAIntegralizar": 300,
        "valorEntrada": 100,
        "quantidadeParcelas": 2,
        "valorParcela": 100,
        "dataEntrada": '2017-06-01',
        "tipoAgendamento": 'U',
        "anoMesPrimeiroVencimento": '2017-06',
        "diaPrimeiroVencimento": '01',
        "formaRecebimento": 'N',
        "verificaSaldo": false
    }

    var convert = {
        "renovacaoAutomatica": false,
        "contaDebito": "70165",
        "matricula": 115975,
        "tipoLancamentoCapital": "503",
        "capitalAIntegralizar": 300,
        "valorEntrada": 100,
        "quantidadeParcelas": 2,
        "valorParcela": 100,
        "dataEntrada": "2017-08-16T03:00:00.000Z",
        "tipoAgendamento": "U",
        "anoMesPrimeiroVencimento": "2017-06-01T03:00:00.000Z",
        "diaPrimeiroVencimento": "2017-06-01T03:00:00.000Z",
        "formaRecebimento": "N",
        "verificaSaldo": false
    }

    var convertPost = {
        "renovacaoAutomatica": false,
        "contaDebito": "70165",
        "matricula": 115975,
        "tipoLancamentoCapital": "503",
        "capitalAIntegralizar": 300,
        "valorEntrada": 100,
        "quantidadeParcelas": 2,
        "valorParcela": 100,
        "dataEntrada": "2017-08-16T03:00:00.000Z",
        "tipoAgendamento": "U",
        "anoMesPrimeiroVencimento": "2017-06",
        "diaPrimeiroVencimento": "01",
        "formaRecebimento": "N",
        "verificaSaldo": false
    }

    var empty = {}

    return {
        converted: converted,
        toConvert: convert,
        convertPost: convertPost,
        empty: empty
    }
}
