describe('Router: Agendamento-capital', function () {
    var $route,
        rota,
        $rootScope;

    beforeEach(module('app'));

    beforeEach(inject(function (_$route_, _$rootScope_) {
        $route = _$route_;
        $rootScope = _$rootScope_;

        rota = $route.routes['/cadastro/correntista/:cpf/agendamento-capital'];
    }));

    it('deve validar as configurações', function () {
        expect(rota.templateUrl).toBe('./modules/agendamento-capital/agendamento-capital.html');
        expect(rota.controller).toBe('agendamentoCapitalController');
        expect(rota.controllerAs).toBe('ctrl');
    });

    it('deve validar se o cpf acessado é associado',
        inject(function (rotaService, $rootScope, pessoa, $q) {
            var deferred = $q.defer();
            deferred.resolve({});

            var cpf = '37840916953';

            $route = {
                current: {
                    params: {
                        cpf: cpf
                    }
                }
            }

            spyOn(rotaService, 'isNotAssociado').and.returnValue(deferred.promise);

            rota.resolve.isNotAssociado($route, rotaService);
            $rootScope.$apply();

            expect(rotaService.isNotAssociado).toHaveBeenCalledWith(cpf);
        }));

});
