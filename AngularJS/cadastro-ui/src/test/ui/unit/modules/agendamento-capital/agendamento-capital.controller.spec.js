describe('Controller: agendamentoCapitalController', function() {

    var $controller,
        $rootScope,
        agendamentoCapital,
        $timeout,
        $q,
        cpf = '12345678912',
        erros = [{}];

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function(_$controller_, _$rootScope_, _$q_, _agendamentoCapital_, _$timeout_, _pessoa_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        agendamentoCapital = _agendamentoCapital_;
        $timeout = _$timeout_;
        $q = _$q_;
        pessoa = _pessoa_;

        ctrl = $controller('agendamentoCapitalController', {
            $routeParams: {
                cpf: cpf
            }
        });
    }));

    it('deve estar definido', function() {
        expect(ctrl).toBeDefined();
    });

    it('deve salvar agendamento de capital', function() {
        var data = _agendamentoCapitalDataSave();

        var deferred = $q.defer();
        deferred.resolve({});

        spyOn(agendamentoCapital, 'salvar')
            .and
            .returnValue(deferred.promise);

        spyOn(pessoa, 'status')
            .and
            .returnValue(deferred.promise);

        spyOn($rootScope, '$emit')
            .and
            .callThrough();

        ctrl.salvar(data);

        $rootScope.$apply();

        expect(agendamentoCapital.salvar).toHaveBeenCalledWith(data.agendamentoIntegralizacoes);
        expect(ctrl.sucessos).toBeDefined();
        expect(pessoa.status).toHaveBeenCalledWith(cpf, 'terceiro');
        expect($rootScope.$emit).toHaveBeenCalled();
    });

    it('deve salvar agendamento de capital e dar erro 422', function() {
        var data = _agendamentoCapitalDataSave();

        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                content: erros
            }
        });

        spyOn(agendamentoCapital, 'salvar')
            .and
            .returnValue(deferred.promise);

        ctrl.salvar(data);

        $rootScope.$apply();

        expect(agendamentoCapital.salvar).toHaveBeenCalledWith(data.agendamentoIntegralizacoes);
        expect(ctrl.erros).toBeDefined();
    });

    it('deve salvar agendamento de capital e dar erro 500', function() {
        var data = _agendamentoCapitalDataSave();

        var deferred = $q.defer();
        deferred.reject({
            status: 500,
            data: {
                content: erros
            }
        });

        spyOn(agendamentoCapital, 'salvar')
            .and
            .returnValue(deferred.promise);

        ctrl.salvar(data);

        $rootScope.$apply();

        expect(agendamentoCapital.salvar).toHaveBeenCalledWith(data.agendamentoIntegralizacoes);
        expect(ctrl.erros[0].message).toEqual('Ocorreu um erro no servidor, tente novamente.');
    });
});

function _agendamentoCapitalDataSave() {
    return {
        "agendamentoIntegralizacoes": {
            "renovacaoAutomatica": false,
            "contaDebito": 70165,
            "matricula": 115975,
            "tipoLancamentoCapital": {
                "codigo": 503,
                "descricaoSucinta": "RETORNO CAPITAL"
            },
            "capitalAIntegralizar": 300,
            "valorEntrada": 100,
            "quantidadeParcelas": 2,
            "valorParcela": 100,
            "dataEntrada": "2017-06-01",
            "tipoAgendamento": {
                "valor": "U",
                "descricao": "Último dia útil"
            },
            "anoMesPrimeiroVencimento": "2017-06",
            "formaRecebimento": {
                "valor": "N",
                "descricao": "Cobrança"
            },
            "verificaSaldo": false
        }
    }
}
