describe('Factory: Agendamento Capital', function () {

    var HOST,
        $q,
        cpf = '93994468248',
        agendamentoCapital,
        agendamentoCapitalConverter;

    beforeEach(module('app'));

    beforeEach(inject(function (_HOST_, _$q_, _agendamentoCapital_, _agendamentoCapitalConverter_) {
        HOST = _HOST_;
        $q = _$q_; 
        agendamentoCapital = _agendamentoCapital_;
        agendamentoCapitalConverter = _agendamentoCapitalConverter_;
    }));

    it('.salvar(agendamentoCapital)', inject(function ($httpBackend) {
        $httpBackend.whenGET(/\.html$/).respond('');

        var cpf = '93994468248';
        var dadosFake = _agendamentoCapitalDataSave();

        $httpBackend
            .expectPOST(HOST.agendamentoCapital + "/")
            .respond(201);

        agendamentoCapital
            .salvar(dadosFake)
            .then(function (response) {
                expect(response.status).toBe(201);
            });

        $httpBackend.flush();
    }));
});

function _agendamentoCapitalDataSave() {
    return {
        "renovacaoAutomatica": false,
        "contaDebito": "70165",
        "matricula": 115975,
        "tipoLancamentoCapital": "503",
        "capitalAIntegralizar": 300,
        "valorEntrada": 100,
        "quantidadeParcelas": 2,
        "valorParcela": 100,
        "dataEntrada": "2017-08-16T03:00:00.000Z",
        "tipoAgendamento": "U",
        "anoMesPrimeiroVencimento": "2017-06-01T03:00:00.000Z",
        "formaRecebimento": "N",
        "verificaSaldo": false
    }
}