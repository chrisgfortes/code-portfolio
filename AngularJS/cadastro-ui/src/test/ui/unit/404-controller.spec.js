describe('controller: notFoundController - Erro 404', function () {

    var ctrl, $rootScope;


    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_$controller_, _$rootScope_) {
        $rootScope = _$rootScope_;
        $controller = _$controller_;

        ctrl = $controller('notFoundController', null);
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

});
