describe('Component: uiHeader', function() {

    var $componentController,
        AuthFactory,
        $rootScope,
        cpf = '12345678912',
        ctrl;

    beforeEach(module('app'));

    beforeEach(inject(function(_$componentController_, _AuthFactory_, _$rootScope_) {
        $componentController = _$componentController_;
        AuthFactory = _AuthFactory_;
        $rootScope = _$rootScope_;

        spyOn(AuthFactory, 'getUsuario').and.returnValue({ cooperativa: 566 })

        var bindings = {
            sectionName: 'Cadastro'
        };

        ctrl = $componentController('uiHeader', {
            AuthFactory: _AuthFactory_,
            $routeParams: { cpf: cpf },
        }, bindings);

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o sectionName no bindings', function() {
        expect(ctrl.sectionName).toBeDefined();
    });

    it('deve chamar .logout()', inject(function ($location) {
        spyOn(AuthFactory, 'logout').and.callThrough();
        spyOn($location, 'path').and.callThrough();

        ctrl.logout();

        $rootScope.$apply();

        expect(AuthFactory.logout).toHaveBeenCalled();
    }));

    it('deve chamar .dismissToastError()', function () {
        ctrl.displayToast = true;
        $rootScope.displayToast = true;

        ctrl.dismissToastError();

        expect(ctrl.displayToast).toBe(false);
        expect($rootScope.displayToast).toBe(false);
    });

    it('deve chamar .dismissToastWarning()', function () {
        ctrl.displayToastWarning = true;

        ctrl.dismissToastWarning();

        expect(ctrl.displayToastWarning).toBe(false);
    });

    it('deve chamar $rootScope.$emit()', function () {
        var cpf = '75383171800';
        var nome = 'Nome da seção';
        var data = {titulo:'Oops', descricao:'Descrição do problema'};

        ctrl.cpf = undefined;
        ctrl.section = undefined;
        ctrl.displayToastWarning = false;
        ctrl.mostrarMensagem = false;

        spyOn($rootScope, '$emit').and.callThrough();
        spyOn($rootScope, '$on').and.callThrough();

        $rootScope.$emit('cpf-alterado', cpf);
        $rootScope.$emit('section-name', {nome:nome});
        $rootScope.$emit('display-toast-warning', true);
        $rootScope.$emit('mostrar-mensagem', data);

        expect($rootScope.$emit).toHaveBeenCalledWith('cpf-alterado', cpf);
        expect($rootScope.$emit).toHaveBeenCalledWith('section-name', {nome:nome});
        expect($rootScope.$emit).toHaveBeenCalledWith('display-toast-warning', true);
        expect($rootScope.$emit).toHaveBeenCalledWith('mostrar-mensagem', data);
        expect(ctrl.cpf).toEqual(cpf);
        expect(ctrl.section).toEqual(nome);
        expect(ctrl.displayToastWarning).toBe(true);
        expect(ctrl.mostrarMensagem).toBe(true);
    });

    it('deve habilitar o sidebar', function () {
        spyOn($rootScope, '$emit').and.callThrough();
        ctrl.isActive = false;

        ctrl.sidebar();

        expect($rootScope.$emit).toHaveBeenCalledWith('sidebar-active', true);
    });

    it('deve desabilitar o sidebar', function () {
        spyOn($rootScope, '$emit').and.callThrough();
        ctrl.isActive = true;

        ctrl.sidebar();

        expect($rootScope.$emit).toHaveBeenCalledWith('sidebar-active', false);
    });

    it('deve fechar mensagem', function () {
        ctrl.mostrarMensagem = true;

        ctrl.fecharMensagem();

        expect(ctrl.mostrarMensagem).toBe(false);
    });
});
