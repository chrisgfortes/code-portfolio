describe('factory: avaliacaoGeral', function() {

    var avaliacaoGeral,
        HOST,
        httpBackend;

    beforeEach(module('app'));

    beforeEach(inject(function($httpBackend, _avaliacaoGeral_, _HOST_) {
        avaliacaoGeral = _avaliacaoGeral_;
        HOST = _HOST_;

        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));

    afterEach(function() {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.buscar()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.pessoa + '/analise-cadastral/avaliacoes-gerenciais')
            .respond(200, modelsResponse);

        avaliacaoGeral
            .buscar()
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });
});