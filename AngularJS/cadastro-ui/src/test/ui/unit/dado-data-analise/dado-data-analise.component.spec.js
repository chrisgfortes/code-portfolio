describe('Component: dado-data-analise', function () {

    var $componentController,
        cpf = '53576565418',
        $routeParams,
        $rootScope,
        updateDadoDataAnalise,
        $q,
        ctrl,
        dialogs,
        moment,
        $timeout;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _$routeParams_, _$rootScope_, _updateDadoDataAnalise_, _$q_, _dialogs_, _moment_, _$timeout_) {
        $componentController = _$componentController_;
        $routeParams = _$routeParams_;
        $rootScope = _$rootScope_;
        updateDadoDataAnalise = _updateDadoDataAnalise_;
        $q = _$q_;
        dialogs = _dialogs_;
        moment = _moment_;
        $timeout = _$timeout_;

        ctrl = $componentController('dadoDataAnalise', {
            $routeParams: {
                cpf: cpf
            },
            dialogs: _dialogs_
        }, null);

    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('deve validar data e setar padrão se for inválida', function () {
        ctrl.dataAnaliseAtual = moment();
        ctrl.dataAnalise = undefined;
        var dataAnalise = undefined;

        ctrl.validarDataAnalise(dataAnalise);

        expect(ctrl.dataAnalise).toBeDefined();
        expect(ctrl.dataAnalise).toEqual(ctrl.dataAnaliseAtual);
    });

    it('deve voltar a data anterior se data for inválida', function () {
        ctrl.dataAnaliseAtual = moment().subtract(1, 'day');
        ctrl.dataAnalise = '9999-99-99';

        ctrl.validarDataAnalise(ctrl.dataAnalise);

        $rootScope.$apply();

        expect(ctrl.dataAnalise).toEqual(ctrl.dataAnaliseAtual);
    });

    it('deve salvar data análise se for diferente de data atualmente setada', function () {
        var deferredAlterar = $q.defer();
        deferredAlterar.resolve();
        spyOn(updateDadoDataAnalise, 'salvar').and.returnValue(deferredAlterar.promise);

        ctrl.dataAnaliseAtual = moment().subtract(1, 'day');
        ctrl.dataAnalise = moment();
        ctrl.isSaving = true;

        ctrl.salvarDataAnalise(ctrl.dataAnalise);

        $rootScope.$apply();
        $timeout.flush();

        expect(ctrl.isSaving).toBe(false);
        expect(ctrl.dataAnaliseAtual).toEqual(ctrl.dataAnalise);
        expect(updateDadoDataAnalise.salvar).toHaveBeenCalled()
    });

    it('deve setar como data anterior se não conseguir salvar', function () {
        var deferredAlterar = $q.defer();
        deferredAlterar.reject();
        spyOn(updateDadoDataAnalise, 'salvar').and.returnValue(deferredAlterar.promise);

        ctrl.dataAnaliseAtual = moment().subtract(1, 'day');
        ctrl.dataAnalise = moment();
        ctrl.isSaving = true;

        ctrl.salvarDataAnalise(ctrl.dataAnalise);

        $rootScope.$apply();
        $timeout.flush();

        expect(ctrl.isSaving).toBe(false);
        expect(ctrl.dataAnalise).toEqual(ctrl.dataAnaliseAtual);
        expect(updateDadoDataAnalise.salvar).toHaveBeenCalled()
    });

    it('não deve salvar se data selecionada for a mesma da atual', function () {
        spyOn(updateDadoDataAnalise, 'salvar').and.callThrough();

        var data = moment();
        ctrl.dataAnaliseAtual = data;
        ctrl.dataAnalise = data;

        ctrl.salvarDataAnalise(ctrl.dataAnalise);

        $rootScope.$apply();

        expect(ctrl.dataAnalise).toEqual(ctrl.dataAnaliseAtual);
        expect(updateDadoDataAnalise.salvar).not.toHaveBeenCalled();
    });

    it('deve setar valores no init', function () {
        var retornoFake = {
                informacoesComplementares: {
                    dataAnalise:'2010-05-05'
                }};

        var deferredAlterar = $q.defer();
        deferredAlterar.resolve(retornoFake);

        spyOn(updateDadoDataAnalise, 'buscar').and.returnValue(deferredAlterar.promise);

        ctrl.dataAnaliseAtual = undefined;
        ctrl.dataAnalise = undefined;
        ctrl.dataAtual = undefined;

        ctrl.$onInit();

        $rootScope.$apply();

        expect(ctrl.dataAtual).toBeDefined();
        expect(ctrl.dataAnaliseAtual).toEqual(ctrl.dataAnalise);
    });

    it('deve setar data de análise como atual caso não a encontre', function () {
        var deferredAlterar = $q.defer();
        deferredAlterar.reject();

        spyOn(updateDadoDataAnalise, 'buscar').and.returnValue(deferredAlterar.promise);

        ctrl.dataAnaliseAtual = undefined;
        ctrl.dataAnalise = undefined;
        ctrl.dataAtual = undefined;

        ctrl.$onInit();

        $rootScope.$apply();

        expect(ctrl.dataAtual).toBeDefined();
        expect(moment.isMoment(ctrl.dataAtual)).toBe(true);
        expect(ctrl.dataAnaliseAtual).toEqual(ctrl.dataAnalise);
    });

});
