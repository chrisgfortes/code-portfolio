describe('factory: updateDadoDataAnalise', function () {

    var updateDadoDataAnalise,
        moment,
        HOST;

    beforeEach(module('app'));

    beforeEach(inject(function (_updateDadoDataAnalise_, _moment_, _HOST_) {
        updateDadoDataAnalise = _updateDadoDataAnalise_;
        moment = _moment_;
        HOST = _HOST_;
    }));

    it('.buscar(cpf)', inject(function($httpBackend) {
        $httpBackend.whenGET(/\.html$/).respond('');
        var cpf = '83084412545';
        var dadosFake = analiseCadastralFake();

        $httpBackend
            .expectGET(HOST.terceiro + cpf + '/analise-cadastral')
            .respond(200, dadosFake);

        updateDadoDataAnalise
            .buscar(cpf)
            .then(function (response) {
                var dados = response;
                expect(dados).toEqual(dadosFake);
            });

        $httpBackend.flush();
    }));

    it('.salvar(cpf, obj)', inject(function($httpBackend) {
        var cpf = '83084412545';
        var dadosFake = analiseCadastralFake();
        var analiseCadastralResponse = analiseCadastralPost();

        $httpBackend
            .expectPUT(HOST.terceiro + cpf + '/analise-cadastral')
            .respond(201, dadosFake);

        updateDadoDataAnalise
            .salvar(cpf, analiseCadastralResponse)
            .then(function (response) {
                var dados = response.data;

                expect(dados).toEqual(dadosFake);
            });

        $httpBackend.flush();
    }));

    function analiseCadastralFake() {
        return {
            "cooperativa": null,
            "cpf": null,
            "informacoesComplementares": {
                "codigoPessoa": null,
                "dataAnalise": "2017-05-29",
                "observacaoContrato": null,
                "observacao": null,
                "codigoPosto": null,
                "codigoTipoPessoa": null,
                "avaliacaoGerente": null
            },
            "restricoesAcatadas": null,
            "matriculaVinculada": null,
            "pessoaResponsavelFATCA": null
        }
    }

    function analiseCadastralPost() {
        return {
            "cooperativa": null,
            "cpf": null,
            "informacoesComplementares": {
                "codigoPessoa": null,
                "dataAnalise": moment("2017-05-29"),
                "observacaoContrato": null,
                "observacao": null,
                "codigoPosto": null,
                "codigoTipoPessoa": null,
                "avaliacaoGerente": null
            },
            "restricoesAcatadas": null,
            "matriculaVinculada": null,
            "pessoaResponsavelFATCA": null
        }
    }

});
