describe('factory: AuthFactory', function () {

    var AuthFactory,
        localStorage,
        $window;

    var tokenValid = 'eyJhbGciOiJIUzI1NiJ9.eyJjZFVzdWFyaW8iOiJjYW1pbGEuMDU2NiIsImNkVXN1YXJpb1Npc3RlbWEiOiIiLCJjZENvb3BlcmF0aXZhIjoiMDU2NiIsImNkQ29vcGVyYXRpdmFDZW50cmFsIjo1MDcsInN1YiI6IlByZS1hcHJvdmFkbyJ9.lSWPGnj0Oug2jMlpWNkm0glf3_5RSTh60sQjy7KLnCc';

    beforeEach(module('app'));

    beforeEach(inject(function (_AuthFactory_, _$window_) {
        AuthFactory = _AuthFactory_;
        $window = _$window_;

        localStorage = _$window_.localStorage;
    }));

    it('deve estar definido', function () {
        expect(AuthFactory).toBeDefined();
    });

    it('.isAuthenticated()', function () {
        spyOn(localStorage, 'getItem').and.callFake(function (key) {
            return tokenValid;
        });
        var isAuth = AuthFactory.isAuthenticated();

        expect(isAuth).toBe(true);
    });

    it('.logout()', inject(function ($location) {
        spyOn(localStorage, 'clear');
        spyOn($location, 'path');

        var isAuth = AuthFactory.logout();

        expect(localStorage.clear).toHaveBeenCalled();
    }));

    it('.getUsuario()', inject(function ($location) {
        spyOn(localStorage, 'getItem').and.callFake(function (key) {
            return tokenValid;
        });

        var esperado = {
            nome: 'camila',
            cooperativa: 566
        };

        var usuario = AuthFactory.getUsuario();

        expect(usuario).toEqual(esperado);
    }));

    it('não tem usuario', inject(function ($location) {
        spyOn(localStorage, 'getItem').and.returnValue(undefined);

        var usuario = AuthFactory.getUsuario();

        expect(usuario).toBeUndefined();
    }));

    it('.salvarToken()', function () {
        spyOn(localStorage, 'setItem').and.callFake(function (key) {
            return userToken();
        });

        AuthFactory.salvarToken(userToken());

        expect(localStorage.setItem).toHaveBeenCalledWith('Authorization', userToken());
    });

    it('.salvarTokenLegacy()', function () {
       spyOn(localStorage, 'setItem').and.callFake(function (key) {
           return userTokenLegacy();
       });

       AuthFactory.salvarTokenLegacy(userTokenLegacy());

       expect(localStorage.setItem).toHaveBeenCalledWith('session', userTokenLegacy());
    });

    it('.salvarCooperativa()', function () {
        var cooperativa = 507;

        spyOn(localStorage, 'setItem').and.callFake(function (key) {
            return cooperativa;
        });

        AuthFactory.salvarCooperativa(cooperativa);

        expect(localStorage.setItem).toHaveBeenCalledWith('user', cooperativa);
    });

    it('.salvarActionCooperativa()', function () {
      var cooperativaAction = 566;

      spyOn(localStorage, 'setItem').and.callFake(function (key) {
          return cooperativaAction;
      });

      AuthFactory.salvarActionCooperativa(cooperativaAction);

      expect(localStorage.setItem).toHaveBeenCalledWith('action', cooperativaAction);
    });

    it('.salvarPermissoes()', function () {
      var permissoesJson = permissoes();

      spyOn(localStorage, 'setItem').and.callFake(function (key) {
          return permissoesJson;
      });

      AuthFactory.salvarPermissoes(permissoesJson);

      expect(localStorage.setItem).toHaveBeenCalledWith('permissoes', JSON.stringify(permissoesJson));
    });

    it('.buscarPermissoes()', function () {
      var permissoesJson = permissoes();

      spyOn($window.localStorage, 'getItem').and.callFake(function (key) {
          return JSON.stringify(permissoesJson);
      });

      var retornoPermissoes = AuthFactory.buscarPermissoes();

      expect(retornoPermissoes).toEqual(permissoesJson);
    });

    it('.buscarCooperativa()', function () {
      var coop = 507;

      spyOn($window.localStorage, 'getItem').and.callFake(function (key) {
          return coop;
      });

      var retornoCooperativa = AuthFactory.buscarCooperativa();

      expect(retornoCooperativa).toEqual(coop);
    });

    it('.buscarActionCooperativa()', function () {
      var coop = 566;

      spyOn($window.localStorage, 'getItem').and.callFake(function (key) {
          return coop;
      });

      var retornoCooperativa = AuthFactory.buscarActionCooperativa();

      expect(retornoCooperativa).toEqual(coop);
    });

    it('.hasSession() - true se todos os itens existirem no storage', function () {
      var coop = 566;
      var tokenLegacy = userTokenLegacy();
      var token = userToken();
      var permissoesJson = permissoes();

      spyOn($window.localStorage, 'getItem').and.callFake(function (key) {
          if (key === 'user') {
              return coop;
          }
          else if (key === 'Authorization') {
              return token;
          }
          else if (key === 'session') {
              return tokenLegacy;
          }
          else if (key === 'permissoes') {
              return JSON.stringify(permissoesJson);
          }
      });

      var retorno = AuthFactory.hasSession();
      expect(retorno).toBe(true);
    });

    it('.hasSession() - false se algum dos itens não existir no storage', function () {
      var coop = 566;
      var tokenLegacy = userTokenLegacy();
      var token = userToken();
      var permissoesJson = permissoes();

      spyOn($window.localStorage, 'getItem').and.callFake(function (key) {
          if (key === 'chaveErrada') {
              return coop;
          }
          else if (key === 'Authorization') {
              return token;
          }
          else if (key === 'session') {
              return tokenLegacy;
          }
          else if (key === 'permissoes') {
              return JSON.stringify(permissoesJson);
          }
      });

      var retorno = AuthFactory.hasSession();
      expect(retorno).toBe(false);
    });

    function permissoes(){
        return [{
            "chave": "Cadastro Inserir",
            "telas": [
              "/cadastro",
              "/cadastro/terceiro/:cpf/dados-pessoais/",
              "/cadastro/terceiro/:cpf/dados-complementares",
              "/cadastro/terceiro/:cpf/bens",
              "/cadastro/terceiro/:cpf/cartao-autografo",
              "/cadastro/terceiro/:cpf/renda",
              "/cadastro/terceiro/:cpf/analise-cadastral",
              "/cadastro/terceiro/:cpf/resumo",
              "/cadastro/associado/:cpf/dados-pessoais/",
              "/cadastro/associado/:cpf/dados-complementares",
              "/cadastro/associado/:cpf/bens",
              "/cadastro/associado/:cpf/cartao-autografo",
              "/cadastro/associado/:cpf/renda",
              "/cadastro/associado/:cpf/matricula",
              "/cadastro/associado/:cpf/analise-cadastral",
              "/cadastro/associado/:cpf/resumo"
            ]
          }]
    }

    function userToken() {
        return {"value":"eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICIyTWhzS0xHS2ZRbHJxNlkyc3pFcHVtVm1rR2Y4MmZXTVN6NlJLcnhac3BBIn0."
        + "eyJqdGkiOiIyOTc5YmU1Mi03NTc3LTQzMmEtYmIyYi00Yzg5Y2RkZGRjMzMiLCJleHAiOjE1MDA5ODY4OTIsIm5iZiI6MCwiaWF0IjoxNTAwOTg2MjkyLCJpc3MiOiJodHRwOi8vb2F1"
        + "dGgtdHN0LmUtdW5pY3JlZC5jb20uYnIvYXV0aC9yZWFsbXMvVW5pY3JlZFJlYWxtIiwiYXVkIjoidHN0LWNsaWVudCIsInN1YiI6ImRjNGM5ZGNhLWM3ODEtNGI4OC1hZmFhLTZjNGZjYmEwN"
        + "zkyZCIsInR5cCI6IkJlYXJlciIsImF6cCI6InRzdC1jbGllbnQiLCJhdXRoX3RpbWUiOjE1MDA5ODYyOTIsInNlc3Npb25fc3RhdGUiOiI1MzAzMDc5YS0yMWRkLTRiMDMtOTcwMS1lY2IzYW"
        + "M5YmZkMDkiLCJhY3IiOiIxIiwiY2xpZW50X3Nlc3Npb24iOiIxMmMyM2MwMC03ZDRkLTQzOWItODhkNS05OWU4Y2E0Y2I2YzIiLCJhbGxvd2VkLW9yaWdpbnMiOltdLCJyZWFsbV9hY2Nlc3MiOnsi"
        + "cm9sZXMiOlsiUk9MRV9BRE1JTiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7fSwibmFtZSI6IiIsImRpc3Rpbmd1aXNoZWROYW1lIjoiQ049Y2FtaWxhLE9VPTA1NjYsT1U9U2l0ZTEsT1U9UHJvZHVjYW"
        + "8sT1U9VXN1YXJpb3MsT1U9VFMsREM9ZS11bmljcmVkaG9tb2xvZyxEQz1jb20sREM9YnIiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJjYW1pbGEuMDU2NiJ9.YtpJgpZTD4QJEUW12WJ2qFeDLvTJV9"
        + "kmt44MzYej8MlTvIaxbwUJoMLxaoAITnUOV94C7Cl5DI1NMeeT8dAYtMPYvqj5yztfYl1NpjlKX6VF-zN66domw4FIJ5xvOBFcOVk8MgNu0RmyS1hE_Rm1oEKQs3xeyP4Q-_K6eSiuFtfHErORhbHb"
        + "l2YjlpL8Dg4uldxNTEXZJdRy0qokZ9eruEFEU8BJlBboMDmtb-Sykmlb78duya2f2k310jO3UEX_MwyDJE2n5Bs4S4wfQfe4nRn2B3VUVtZGVJh5xZpSF2sXMfUZZ52lC8"
        + "5YS_BD4x7ckQyPQB3QuRYWGOq5Kpk0jA"}
    }

    function userTokenLegacy() {
        return {"value":"eyJhbGciOiJIUzI1NiJ9.eyJjZFVzdWFyaW8iOiJjYW1pbGEuMDU2NiIsImNkQ29vcGVyYXRpdmEiOjU2NiwiY2RDb29wZXJhdGl2Y"
                + "UNlbnRyYWwiOjU2Niwic3ViIjoiUHJlLWFwcm92YWRvIn0.ybqQt9_0fBqnNIQG3tgEByV64ieDnDXkpNWAAgWziwQ"}
    }
});
