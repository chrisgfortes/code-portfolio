describe('Router: dependentesUpdate-router', function () {

    var dependentesUpdate,
        $route,
        rota,
        secao,
        $rootScope,
        $q,
        rotaService,
        cpf = '123456789123';

    beforeEach(module('app'));

    beforeEach(inject(function (_dependentesUpdate_, _$rootScope_, _secao_, _$route_, _$q_, _rotaService_) {
        $route = _$route_;
        $rootScope = _$rootScope_;
        $route = _$route_;
        dependentesUpdate = _dependentesUpdate_;
        secao = _secao_;
        $q = _$q_;
        rotaService = _rotaService_;

        rota = $route.routes['/alteracao/pessoa-fisica/:cpf/dependentes'];
    }));

    it('Deve testar dependentesUpdate-router', function() {
        expect(rota.controller).toBe('dependentesUpdateController');
        expect(rota.templateUrl).toBe('components/update/pessoa-fisica/dependentes/dependentes-update.html');
    });

    it('Deve chamar dependentesUpdate.buscar()', function () {
        var deferred = $q.defer();
        deferred.resolve([{}]);

        spyOn(dependentesUpdate, 'buscar').and.returnValue(deferred.promise);

        $route = {
            current: {
                params: {
                    cpf: cpf
                }
            }
        }

        rota.resolve.dependentesInfo(dependentesUpdate, $route);
        $rootScope.$apply();

        expect(dependentesUpdate.buscar).toHaveBeenCalledWith(cpf);
    });

    it('Deve chamar dependentesUpdate Reject', function () {
        var deferred = $q.defer();
        deferred.reject([]);

        spyOn(dependentesUpdate, 'buscar').and.returnValue(deferred.promise);

        $route = {
            current: {
                params: {
                    cpf: cpf
                }
            }
        }

        rota.resolve.dependentesInfo(dependentesUpdate, $route);
        $rootScope.$apply();

        expect(dependentesUpdate.buscar).toHaveBeenCalledWith(cpf);
    });

    it('Deve chamar dependentesUpdate.buscar()', function () {
        rota.resolve.section(secao);
        $rootScope.$apply();

        expect(secao.nome).toBeDefined();
    });

    it('deve verificar se é terceiro', function() {
        var cpf = '93994468248';

        var deferred = $q.defer();
        deferred.resolve({});
        spyOn(rotaService, 'isNotTerceiro').and.returnValue(deferred.promise);

        $route = {
            current: {
                params: {
                    cpf: cpf
                }
            }
        }

        rota.resolve.isNotTerceiro($route, rotaService);
        expect(rotaService.isNotTerceiro).toHaveBeenCalledWith(cpf);
    });
});
