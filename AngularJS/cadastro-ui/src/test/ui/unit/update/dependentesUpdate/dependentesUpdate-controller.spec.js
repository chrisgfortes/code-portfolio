describe('controller: dependentesUpdateController', function () {

    var ctrl, cpf = '08094498625';
    var dependentesInfo, dependentesUpdate, $rootScope;
    var erros = [{}];

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_dependentesUpdate_, _$controller_, _$rootScope_, _$q_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        dependentesUpdate = _dependentesUpdate_;

        ctrl = $controller('dependentesUpdateController', {
            dependentesInfo: [],
            dependentesUpdate: _dependentesUpdate_,
            $routeParams: {
                cpf: cpf
            }
        });
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('deve chamar update', function () {
        var dependentes = [];
        var deferred = $q.defer();
        deferred.resolve({});
        ctrl.savingProgress = true;

        spyOn(dependentesUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(dependentes);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(dependentesUpdate.salvar).toHaveBeenCalledWith(cpf, dependentes);
    });

    it('deve mostrar messagem de validação, envida pelo body', function () {
        var dependentes = [];
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                body: erros
            }
        });
        ctrl.savingProgress = true;

        spyOn(dependentesUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(dependentes);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(dependentesUpdate.salvar).toHaveBeenCalledWith(cpf, dependentes);
        expect(ctrl.erros).toEqual(erros);
        expect(ctrl.sucessos).toEqual([]);
    });

    it('deve mostrar messagem de validação, envida pelo content', function () {
        var dependentes = [];
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                content: erros
            }
        });
        ctrl.savingProgress = true;

        spyOn(dependentesUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(dependentes);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(dependentesUpdate.salvar).toHaveBeenCalledWith(cpf, dependentes);
        expect(ctrl.erros).toEqual(erros);
        expect(ctrl.sucessos).toEqual([]);
    });

    it('não deve mostrar messagem de validação', function () {
        var dependentes = [];
        var deferred = $q.defer();
        deferred.reject({
            status: 400,
            data: {
                content: erros
            }
        });
        ctrl.savingProgress = true;

        spyOn(dependentesUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(dependentes);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(dependentesUpdate.salvar).toHaveBeenCalledWith(cpf, dependentes);
        expect(ctrl.erros).not.toBeDefined();
        expect(ctrl.sucessos).toEqual([]);
    });
});
