describe('controller: rendaUpdateController', function () {

    var ctrl, cpf = '08094498625';
    var rendasInfo, rendaUpdate, $rootScope, moment;
    var erros = [{}];

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_rendaUpdate_, _$controller_, _$rootScope_, _$q_, _moment_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        rendaUpdate = _rendaUpdate_;
        moment = _moment_;

        ctrl = $controller('rendaUpdateController', {
            rendasInfo: [],
            rendaUpdate: _rendaUpdate_,
            $routeParams: {
                cpf: cpf
            }
        });
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('deve chamar update', function () {
        var obj= {
            rendas: [],
            dataAtualizacaoRenda: moment()
        }
        var deferred = $q.defer();
        deferred.resolve({});
        ctrl.savingProgress = true;

        spyOn(rendaUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(obj);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(rendaUpdate.salvar).toHaveBeenCalledWith(cpf, obj);
    });

    it('deve mostrar messagem de validação, envida pelo body', function () {
        var obj= {
            rendas: [],
            dataAtualizacaoRenda: moment()
        }
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                body: erros
            }
        });
        ctrl.savingProgress = true;

        spyOn(rendaUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(obj);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(rendaUpdate.salvar).toHaveBeenCalledWith(cpf, obj);
        expect(ctrl.erros).toEqual(erros);
        expect(ctrl.sucessos).toEqual([]);
    });

    it('deve mostrar messagem de validação, envida pelo content', function () {
        var obj= {
            rendas: [],
            dataAtualizacaoRenda: moment()
        }
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                content: erros
            }
        });
        ctrl.savingProgress = true;

        spyOn(rendaUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(obj);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(rendaUpdate.salvar).toHaveBeenCalledWith(cpf, obj);
        expect(ctrl.erros).toEqual(erros);
        expect(ctrl.sucessos).toEqual([]);
    });

    it('não deve mostrar messagem de validação', function () {
        var obj= {
            rendas: [],
            dataAtualizacaoRenda: moment()
        }
        var deferred = $q.defer();
        deferred.reject({
            status: 400,
            data: {
                content: erros
            }
        });
        ctrl.savingProgress = true;

        spyOn(rendaUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(obj);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(rendaUpdate.salvar).toHaveBeenCalledWith(cpf, obj);
        expect(ctrl.erros).not.toBeDefined();
        expect(ctrl.sucessos).toEqual([]);
    });
});
