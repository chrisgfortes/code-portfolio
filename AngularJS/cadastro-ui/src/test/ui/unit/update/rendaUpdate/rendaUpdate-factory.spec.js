describe('factory: rendaUpdate', function () {

    var rendaUpdate,
        httpBackend,
        moment,
        HOST;

    beforeEach(module('app'));

    beforeEach(inject(function ($httpBackend, _rendaUpdate_, _moment_, _HOST_) {
        rendaUpdate = _rendaUpdate_;
        moment = _moment_;
        HOST = _HOST_;

        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));

    afterEach(function () {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.buscar(cpf)', function () {
        var cpf = '83084412545';
        var dadosFake = mock(moment);

        httpBackend
            .expectGET(HOST.terceiro + cpf + '/renda')
            .respond(200, dadosFake);

        rendaUpdate
            .buscar(cpf)
            .then(function (response) {
                var dados = response;
                expect(dados).toEqual(dadosFake);
            });

        httpBackend.flush();
    });

    it('.salvar(cpf, dados)', function () {
        var cpf = '83084412545';
        var dadosFake = mock(moment);

        httpBackend
            .expectPUT(HOST.terceiro + cpf + '/renda')
            .respond(201, dadosFake);

        rendaUpdate
            .salvar(cpf, dadosFake)
            .then(function (response) {
                var dados = response.data;
                expect(dados).toEqual(dadosFake);
            });

        httpBackend.flush();
    });
});

function mock(moment){
 return {
     dataAtualizacaoRenda:moment(),
     rendas: [
      {
        "comprovacao": "IMPOSTO_RENDA",
        "data": moment("2017-06-05"),
        "dataAdmissao": moment("2017-06-05"),
        "fontePagadora": {
          "cpfCnpj": "string",
          "creditoConsignado": true,
          "entidadeCooperativa": true,
          "folhaLayout": "OUTROS_NENHUM",
          "id": 0,
          "nome": "string",
          "ramo": "COMERCIO",
          "tipoControle": "PUBLICO"
        },
        "id": 0,
        "remuneracao": 0,
        "tipo": "COMPROVADA"
      }
  ]}
}
