describe('controller: enderecoUpdateController', function () {

    var $controller,
    	ctrl,
    	enderecoInfo,
    	$routeParams,
    	enderecoUpdate,
    	cpf = '42045016803',
    	erros = [{}],
    	$rootScope;

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_$controller_, _enderecoUpdate_, _$routeParams_, _$q_, _$rootScope_) {
        $controller = _$controller_;
        enderecoUpdate = _enderecoUpdate_;
        $routeParams = _$routeParams_;
        $q = _$q_;
        $rootScope = _$rootScope_;

        ctrl = $controller('enderecoUpdateController', {
        	enderecoInfo: [],
        	enderecoUpdate: _enderecoUpdate_,
            $routeParams: {
                cpf: cpf
            }
        });
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('deve chamar update', function () {
        var enderecos = [];
        var deferred = $q.defer();
        deferred.resolve({});
        ctrl.savingProgress = true;

        spyOn(enderecoUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(enderecos);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(enderecoUpdate.salvar).toHaveBeenCalledWith(cpf, enderecos);
    });

    it('deve mostrar messagem de validação, envida pelo body', function () {
        var enderecos = [{}];
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                body: erros
            }
        });
        ctrl.savingProgress = true;

        spyOn(enderecoUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(enderecos);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(enderecoUpdate.salvar).toHaveBeenCalledWith(cpf, enderecos);
        expect(ctrl.erros).toEqual(erros);
        expect(ctrl.sucessos).toEqual([]);
    });

    it('deve mostrar messagem de validação, envida pelo content', function () {
        var enderecos = [{}];
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                content: erros
            }
        });
        ctrl.savingProgress = true;

        spyOn(enderecoUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(enderecos);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(enderecoUpdate.salvar).toHaveBeenCalledWith(cpf, enderecos);
        expect(ctrl.erros).toEqual(erros);
        expect(ctrl.sucessos).toEqual([]);
    });

    it('não deve mostrar messagem de validação', function () {
        var enderecos = [{}];
        var deferred = $q.defer();
        deferred.reject({
            status: 400,
            data: {
                content: erros
            }
        });
        ctrl.savingProgress = true;

        spyOn(enderecoUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(enderecos);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(enderecoUpdate.salvar).toHaveBeenCalledWith(cpf, enderecos);
        expect(ctrl.erros).not.toBeDefined();
        expect(ctrl.sucessos).toEqual([]);
    });
});
