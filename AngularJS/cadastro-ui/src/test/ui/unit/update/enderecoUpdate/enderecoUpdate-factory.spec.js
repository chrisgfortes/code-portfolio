describe('factory: enderecoUpdate', function () {

    var enderecoUpdate,
        httpBackend,
        moment,
        HOST;

    beforeEach(module('app'));

    beforeEach(inject(function ($httpBackend, _enderecoUpdate_, _moment_, _HOST_) {
        enderecoUpdate = _enderecoUpdate_;
        moment = _moment_;
        HOST = _HOST_;

        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));

    afterEach(function () {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.buscar(cpf)', function () {
        var cpf = '83084412545';
        var dadosFake = enderecoFake(moment);

        httpBackend
            .expectGET(HOST.terceiro + cpf + '/enderecos')
            .respond(200, dadosFake);

        enderecoUpdate
            .buscar(cpf)
            .then(function (response) {
                var dados = response;
                expect(dados).toEqual(dadosFake);
            });

        httpBackend.flush();
    });

    it('.salvar(cpf, dados)', function () {
        var cpf = '83084412545';
        var dadosFake = enderecoFake(moment);

        httpBackend
            .expectPUT(HOST.terceiro + cpf + '/enderecos')
            .respond(201, dadosFake);

        enderecoUpdate
            .salvar(cpf, dadosFake)
            .then(function (response) {
                var dados = response.data;
                expect(dados).toEqual(dadosFake);
            });

        httpBackend.flush();
    });
});

function enderecoFake(moment){
    return [{
        "empostamento": true,
        "principal": true,
        "tipoEndereco": "RESIDENCIAL",
        "cep": 92990000,
        "logradouro": "Rua Carlos Gomes",
        "numero": "300",
        "enderecoSemNumero": false,
        "bairro": "centro",
        "estado": "RS",
        "codigoCidade": 7665,
        "situacaoEndereco": "PROPRIA",
        "resideDesde": moment("2017-01-01")
    }]
}
