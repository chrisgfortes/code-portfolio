describe('Router: enderecoUpdate-router', function () {

    var enderecoUpdate,
        $route,
        rota,
        secao,
        $rootScope,
        $q,
        rotaService,
        cpf = '123456789123';

    beforeEach(module('app'));

    beforeEach(inject(function (_enderecoUpdate_, _$rootScope_, _secao_, _$route_, _$q_, _rotaService_) {
        $route = _$route_;
        $rootScope = _$rootScope_;
        $route = _$route_;
        enderecoUpdate = _enderecoUpdate_;
        secao = _secao_;
        $q = _$q_;
        rotaService = _rotaService_;

        rota = $route.routes['/alteracao/pessoa-fisica/:cpf/endereco'];
    }));

    it('Deve testar enderecoUpdate-router', function() {
        expect(rota.controller).toBe('enderecoUpdateController');
        expect(rota.templateUrl).toBe('components/update/pessoa-fisica/endereco/endereco-update.html');
    });

    it('Deve chamar enderecoUpdate.buscar()', function () {
        var deferred = $q.defer();
        deferred.resolve([{}]);

        spyOn(enderecoUpdate, 'buscar').and.returnValue(deferred.promise);

        $route = {
            current: {
                params: {
                    cpf: cpf
                }
            }
        }

        rota.resolve.enderecoInfo(enderecoUpdate, $route);
        $rootScope.$apply();

        expect(enderecoUpdate.buscar).toHaveBeenCalledWith(cpf);
    });

    it('Deve chamar enderecoUpdate Reject', function () {
        var deferred = $q.defer();
        deferred.reject([]);

        spyOn(enderecoUpdate, 'buscar').and.returnValue(deferred.promise);

        $route = {
            current: {
                params: {
                    cpf: cpf
                }
            }
        }

        rota.resolve.enderecoInfo(enderecoUpdate, $route);
        $rootScope.$apply();

        expect(enderecoUpdate.buscar).toHaveBeenCalledWith(cpf);
    });

    it('Deve chamar enderecoUpdate.buscar()', function () {
        rota.resolve.section(secao);
        $rootScope.$apply();

        expect(secao.nome).toBeDefined();
    });

    it('deve verificar se é terceiro', function() {
        var cpf = '93994468248';

        var deferred = $q.defer();
        deferred.resolve({});
        spyOn(rotaService, 'isNotTerceiro').and.returnValue(deferred.promise);

        $route = {
            current: {
                params: {
                    cpf: cpf
                }
            }
        }

        rota.resolve.isNotTerceiro($route, rotaService);
        expect(rotaService.isNotTerceiro).toHaveBeenCalledWith(cpf);
    });
});
