describe('factory: estadoCivilUpdate', function () {

    var estadoCivilUpdate,
        httpBackend,
        moment,
        HOST;

    beforeEach(module('app'));

    beforeEach(inject(function ($httpBackend, _estadoCivilUpdate_, _moment_, _HOST_) {
        estadoCivilUpdate = _estadoCivilUpdate_;
        moment = _moment_;
        HOST = _HOST_;

        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));

    afterEach(function () {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.buscar(cpf)', function () {
        var cpf = '83084412545';
        var dadosFake = estadoCivilFake(moment);

        httpBackend
            .expectGET(HOST.terceiro + cpf + '/estado-civil')
            .respond(200, dadosFake);

        estadoCivilUpdate
            .buscar(cpf)
            .then(function (response) {
                var dados = response;
                expect(dados).toEqual(dadosFake);
            });

        httpBackend.flush();
    });

    it('.salvar(cpf, dados)', function () {
        var cpf = '83084412545';
        var dadosFake = estadoCivilFake(moment);

        httpBackend
            .expectPUT(HOST.terceiro + cpf + '/estado-civil')
            .respond(201, dadosFake);

        estadoCivilUpdate
            .salvar(cpf, dadosFake)
            .then(function (response) {
                var dados = response.data;
                expect(dados).toEqual(dadosFake);
            });

        httpBackend.flush();
    });
});

function estadoCivilFake(moment){
 return {
     conjuge: {
        contatos: {
          emails: [
            {
              endereco: "claraRub@gmail.com",
              observacao: "Casa",
              tipoEmail: "PESSOAL"
            }
          ],
          telefones: [
            {
              ddd: "51",
              numero: "845687788",
              observacao: "string",
              tipoTelefone: "RESIDENCIAL"
            }
          ]
        },
        cpf: "51108522866",
        dataEmissao: moment("2017-06-14"),
        dataNascimento: moment("2017-06-14"),
        empresa: "Google",
        filiacao: {
          nomeMae: "Maria Julieta",
          nomeMaeNaoDeclarado: true,
          nomePai: "João Carlos",
          nomePaiNaoDeclarado: true
        },
        nomeCompleto: "Clara Rubik Schortszerbitsk",
        numeroIdentificacao: "15353112",
        orgaoExpedidor: "SSP",
        profissao: "OTORRINOLARINGOLOGISTA",
        tipoIdentificacao: "CARTEIRA_IDENTIDADE",
        ufExpedidor: "RS",
        valorRenda: 351556.89
    },
    estadoCivil: "CASADO",
    regimeCasamento: "COMUNHAO_BENS"
 }
}
