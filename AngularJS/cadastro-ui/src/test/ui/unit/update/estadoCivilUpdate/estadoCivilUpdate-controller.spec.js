describe('controller: estadoCivilUpdateController', function () {

    var ctrl, cpf = '08094498625';
    var estadoCivilInfo, estadoCivilUpdate, $rootScope;
    var erros = [{}];

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_estadoCivilUpdate_, _$controller_, _$rootScope_, _$q_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        estadoCivilUpdate = _estadoCivilUpdate_;

        ctrl = $controller('estadoCivilUpdateController', {
            estadoCivilInfo: {},
            estadoCivilUpdate: _estadoCivilUpdate_,
            $routeParams: {
                cpf: cpf
            }
        });
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('deve chamar update', function () {
        var estadoCivilObj = {};
        var deferred = $q.defer();
        deferred.resolve({});
        ctrl.savingProgress = true;

        spyOn(estadoCivilUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(estadoCivilObj);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(estadoCivilUpdate.salvar).toHaveBeenCalledWith(cpf, estadoCivilObj);
    });

    it('deve mostrar messagem de validação, envida pelo body', function () {
        var estadoCivilObj = [];
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                body: erros
            }
        });
        ctrl.savingProgress = true;

        spyOn(estadoCivilUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(estadoCivilObj);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(estadoCivilUpdate.salvar).toHaveBeenCalledWith(cpf, estadoCivilObj);
        expect(ctrl.erros).toEqual(erros);
        expect(ctrl.sucessos).toEqual([]);
    });

    it('deve mostrar messagem de validação, envida pelo content', function () {
        var estadoCivilObj = {};
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                content: erros
            }
        });
        ctrl.savingProgress = true;

        spyOn(estadoCivilUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(estadoCivilObj);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(estadoCivilUpdate.salvar).toHaveBeenCalledWith(cpf, estadoCivilObj);
        expect(ctrl.erros).toEqual(erros);
        expect(ctrl.sucessos).toEqual([]);
    });

    it('não deve mostrar messagem de validação', function () {
        var estadoCivilObj = [];
        var deferred = $q.defer();
        deferred.reject({
            status: 400,
            data: {
                content: erros
            }
        });
        ctrl.savingProgress = true;

        spyOn(estadoCivilUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(estadoCivilObj);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(estadoCivilUpdate.salvar).toHaveBeenCalledWith(cpf, estadoCivilObj);
        expect(ctrl.erros).not.toBeDefined();
        expect(ctrl.sucessos).toEqual([]);
    });
});
