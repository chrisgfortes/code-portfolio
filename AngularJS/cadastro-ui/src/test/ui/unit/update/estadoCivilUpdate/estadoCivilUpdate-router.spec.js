describe('Router: estadoCivilUpdate-router', function () {

    var estadoCivilUpdate,
        $route,
        rota,
        secao,
        $rootScope,
        $q,
        rotaService,
        cpf = '123456789123';

    beforeEach(module('app'));

    beforeEach(inject(function (_estadoCivilUpdate_, _$rootScope_, _secao_, _$route_, _$q_, _rotaService_) {
        $route = _$route_;
        $rootScope = _$rootScope_;
        $route = _$route_;
        estadoCivilUpdate = _estadoCivilUpdate_;
        secao = _secao_;
        $q = _$q_;
        rotaService = _rotaService_;

        rota = $route.routes['/alteracao/pessoa-fisica/:cpf/estado-civil'];
    }));

    it('Deve testar estadoCivilUpdate-router', function() {
        expect(rota.controller).toBe('estadoCivilUpdateController');
        expect(rota.templateUrl).toBe('components/update/pessoa-fisica/estado-civil/estado-civil-update.html');
    });

    it('Deve chamar estadoCivilUpdate.buscar()', function () {
        var deferred = $q.defer();
        deferred.resolve({});

        spyOn(estadoCivilUpdate, 'buscar').and.returnValue(deferred.promise);

        $route = {
            current: {
                params: {
                    cpf: cpf
                }
            }
        }

        rota.resolve.estadoCivilInfo(estadoCivilUpdate, $route);
        $rootScope.$apply();

        expect(estadoCivilUpdate.buscar).toHaveBeenCalledWith(cpf);
    });

    it('Deve chamar estadoCivilUpdate Reject', function () {
        var deferred = $q.defer();
        deferred.reject({});

        spyOn(estadoCivilUpdate, 'buscar').and.returnValue(deferred.promise);

        $route = {
            current: {
                params: {
                    cpf: cpf
                }
            }
        }

        rota.resolve.estadoCivilInfo(estadoCivilUpdate, $route);
        $rootScope.$apply();

        expect(estadoCivilUpdate.buscar).toHaveBeenCalledWith(cpf);
    });

    it('Deve chamar estadoCivilUpdate.buscar()', function () {
        rota.resolve.section(secao);
        $rootScope.$apply();

        expect(secao.nome).toBeDefined();
    });

    it('deve verificar se é terceiro', function() {
        var cpf = '93994468248';

        var deferred = $q.defer();
        deferred.resolve({});
        spyOn(rotaService, 'isNotTerceiro').and.returnValue(deferred.promise);

        $route = {
            current: {
                params: {
                    cpf: cpf
                }
            }
        }

        rota.resolve.isNotTerceiro($route, rotaService);
        expect(rotaService.isNotTerceiro).toHaveBeenCalledWith(cpf);
    });
});
