describe('controller: imoveisController', function () {

    var ctrl, cpf = '08094498625';
    var imoveisInfo, imoveisUpdate, $rootScope;
    var erros = [{}];

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_imoveisUpdate_, _$controller_, _$rootScope_, _$q_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        imoveisUpdate = _imoveisUpdate_;

        ctrl = $controller('imoveisUpdateController', {
            imoveisInfo: {},
            imoveisUpdate: _imoveisUpdate_,
            $routeParams: {
                cpf: cpf
            }
        });
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('deve chamar update', function () {
        var imoveisObj = [];
        var deferred = $q.defer();
        deferred.resolve([]);
        ctrl.savingProgress = true;

        spyOn(imoveisUpdate, 'salvar').and.returnValue(deferred.promise);
        spyOn(imoveisUpdate, 'buscar').and.returnValue(deferred.promise);
        spyOn($rootScope, '$broadcast').and.callThrough();

        ctrl.update(imoveisObj);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(imoveisUpdate.salvar).toHaveBeenCalledWith(cpf, imoveisObj);
        expect($rootScope.$broadcast).toHaveBeenCalledWith('atualizar-descricao-tipo-imovel', []);
    });

    it('deve mostrar messagem de validação, envida pelo body', function () {
        var imoveisObj = [];
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                body: erros
            }
        });
        ctrl.savingProgress = true;

        spyOn(imoveisUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(imoveisObj);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(imoveisUpdate.salvar).toHaveBeenCalledWith(cpf, imoveisObj);
        expect(ctrl.erros).toEqual(erros);
        expect(ctrl.sucessos).toEqual([]);
    });

    it('deve mostrar messagem de validação, envida pelo content', function () {
        var imoveisObj = {};
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                content: erros
            }
        });
        ctrl.savingProgress = true;

        spyOn(imoveisUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(imoveisObj);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(imoveisUpdate.salvar).toHaveBeenCalledWith(cpf, imoveisObj);
        expect(ctrl.erros).toEqual(erros);
        expect(ctrl.sucessos).toEqual([]);
    });

    it('não deve mostrar messagem de validação', function () {
        var imoveisObj = [];
        var deferred = $q.defer();
        deferred.reject({
            status: 400,
            data: {
                content: erros
            }
        });
        ctrl.savingProgress = true;

        spyOn(imoveisUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(imoveisObj);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(imoveisUpdate.salvar).toHaveBeenCalledWith(cpf, imoveisObj);
        expect(ctrl.erros).not.toBeDefined();
        expect(ctrl.sucessos).toEqual([]);
    });
});
