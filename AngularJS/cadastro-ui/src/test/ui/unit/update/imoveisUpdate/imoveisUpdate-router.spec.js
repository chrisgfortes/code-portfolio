describe('Router: imoveisUpdate-router', function () {

    var imoveisUpdate,
        $route,
        rota,
        secao,
        $rootScope,
        $q,
        rotaService,
        cpf = '123456789123';

    beforeEach(module('app'));

    beforeEach(inject(function (_imoveisUpdate_, _$rootScope_, _secao_, _$route_, _$q_, _rotaService_) {
        $route = _$route_;
        $rootScope = _$rootScope_;
        $route = _$route_;
        imoveisUpdate = _imoveisUpdate_;
        secao = _secao_;
        $q = _$q_;
        rotaService = _rotaService_;

        rota = $route.routes['/alteracao/pessoa-fisica/:cpf/imoveis'];
    }));

    it('Deve testar imoveisUpdate-router', function() {
        expect(rota.controller).toBe('imoveisUpdateController');
        expect(rota.templateUrl).toBe('components/update/pessoa-fisica/imoveis/imoveis-update.html');
    });

    it('Deve chamar imoveisUpdate.buscar()', function () {
        var deferred = $q.defer();
        deferred.resolve({});

        spyOn(imoveisUpdate, 'buscar').and.returnValue(deferred.promise);

        $route = {
            current: {
                params: {
                    cpf: cpf
                }
            }
        }

        rota.resolve.imoveisInfo(imoveisUpdate, $route);
        $rootScope.$apply();

        expect(imoveisUpdate.buscar).toHaveBeenCalledWith(cpf);
    });

    it('Deve chamar imoveisUpdate Reject', function () {
        var deferred = $q.defer();
        deferred.reject({});

        spyOn(imoveisUpdate, 'buscar').and.returnValue(deferred.promise);

        $route = {
            current: {
                params: {
                    cpf: cpf
                }
            }
        }

        rota.resolve.imoveisInfo(imoveisUpdate, $route);
        $rootScope.$apply();

        expect(imoveisUpdate.buscar).toHaveBeenCalledWith(cpf);
    });

    it('Deve chamar imoveisUpdate.buscar()', function () {
        rota.resolve.section(secao);
        $rootScope.$apply();

        expect(secao.nome).toBeDefined();
    });

    it('deve verificar se é terceiro', function() {
        var cpf = '93994468248';

        var deferred = $q.defer();
        deferred.resolve({});
        spyOn(rotaService, 'isNotTerceiro').and.returnValue(deferred.promise);

        $route = {
            current: {
                params: {
                    cpf: cpf
                }
            }
        }

        rota.resolve.isNotTerceiro($route, rotaService);
        expect(rotaService.isNotTerceiro).toHaveBeenCalledWith(cpf);
    });
});
