describe('factory: imoveisUpdate', function () {

    var imoveisUpdate,
        httpBackend,
        moment,
        HOST;

    beforeEach(module('app'));

    beforeEach(inject(function ($httpBackend, _imoveisUpdate_, _moment_, _HOST_) {
        imoveisUpdate = _imoveisUpdate_;
        moment = _moment_;
        HOST = _HOST_;

        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));

    afterEach(function () {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.buscar(cpf)', function () {
        var cpf = '83084412545';
        var dadosFake = imoveisFake(moment);

        httpBackend
            .expectGET(HOST.terceiro + cpf + '/imoveis')
            .respond(200, dadosFake);

        imoveisUpdate
            .buscar(cpf)
            .then(function (response) {
                var dados = response;
                expect(dados).toEqual(dadosFake);
            });

        httpBackend.flush();
    });

    it('.salvar(cpf, dados)', function () {
        var cpf = '83084412545';
        var dadosFake = imoveisFake(moment);

        httpBackend
            .expectPUT(HOST.terceiro + cpf + '/imoveis')
            .respond(201, dadosFake);

        imoveisUpdate
            .salvar(cpf, dadosFake)
            .then(function (response) {
                var dados = response.data;
                expect(dados).toEqual(dadosFake);
            });

        httpBackend.flush();
    });

    it('.excluir(cpf, id)', function () {
        var modelResponse = {};

        var cpf = '83084412545';
        var id = 1;

        httpBackend
            .expectDELETE(HOST.terceiro + cpf + '/imoveis/' + id)
            .respond(204, modelResponse);

        imoveisUpdate
            .excluir(cpf, id)
            .then(function (response) {
                var dados = response.data;
                expect(dados).toEqual(modelResponse);
            });

        httpBackend.flush();
    });
});

function imoveisFake(moment){
 return {
     conjuge: {
        contatos: {
          emails: [
            {
              endereco: "claraRub@gmail.com",
              observacao: "Casa",
              tipoEmail: "PESSOAL"
            }
          ],
          telefones: [
            {
              ddd: "51",
              numero: "845687788",
              observacao: "string",
              tipoTelefone: "RESIDENCIAL"
            }
          ]
        },
        cpf: "51108522866",
        dataEmissao: moment("2017-06-14"),
        dataNascimento: moment("2017-06-14"),
        empresa: "Google",
        filiacao: {
          nomeMae: "Maria Julieta",
          nomeMaeNaoDeclarado: true,
          nomePai: "João Carlos",
          nomePaiNaoDeclarado: true
        },
        nomeCompleto: "Clara Rubik Schortszerbitsk",
        numeroIdentificacao: "15353112",
        orgaoExpedidor: "SSP",
        profissao: "OTORRINOLARINGOLOGISTA",
        tipoIdentificacao: "CARTEIRA_IDENTIDADE",
        ufExpedidor: "RS",
        valorRenda: 351556.89
    },
    imoveis: "CASADO",
    regimeCasamento: "COMUNHAO_BENS"
 }
}
