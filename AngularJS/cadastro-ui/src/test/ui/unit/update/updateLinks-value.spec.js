describe('Value: UpdateLinks', function () {

    var updateLinks,
        cpf = '123';

    beforeEach(module('app'));

    beforeEach(inject(function (_updateLinks_) {
        updateLinks = _updateLinks_;
    }));

    it('deve está definido', function () {
        expect(updateLinks).toBeDefined();
    });

    it('deve retornar uma string de url', function () {
        var urls = mockUrls();

        updateLinks.forEach(function(item, index){
            var url = item.url(cpf);

            var objUrl = urls.filter(function(mock, index){
                var pattern = new RegExp(mock, 'g');
                var res = pattern.test(url);

                if (res) {
                    return mock;
                }
            });

            if(objUrl.length > 0){
                var hash = '#/alteracao/pessoa-fisica/'.concat(cpf, '/', objUrl);
                expect(url).toBe(hash);
            }
        });
    });
});

function mockUrls(){
    return [
        'renda',
        'endereço',
        'dependentes',
        'estado-civil',
        'dados-profissionais',
        'imoveis',
        'veiculos',
        'cartao-autografo',
        'dados-pessoais',
        'dados-complementares',
        'produtos',
        'conta-corrente',
        'matricula',
        'analise-cadastral'
    ]
}
