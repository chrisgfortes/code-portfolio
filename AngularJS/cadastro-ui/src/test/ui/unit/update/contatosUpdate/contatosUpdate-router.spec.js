describe('Router: contatosUpdate-router', function () {

    var contatosUpdate,
        $route,
        rota,
        secao,
        $rootScope,
        $q,
        rotaService,
        cpf = '123456789123';

    beforeEach(module('app'));

    beforeEach(inject(function (_contatosUpdate_, _$rootScope_, _secao_, _$route_, _$q_, _rotaService_) {
        $route = _$route_;
        $rootScope = _$rootScope_;
        $route = _$route_;
        contatosUpdate = _contatosUpdate_;
        secao = _secao_;
        $q = _$q_;
        rotaService = _rotaService_;

        rota = $route.routes['/alteracao/pessoa-fisica/:cpf/contatos'];
    }));

    it('Deve testar contatosUpdate-router', function() {
        expect(rota.controller).toBe('contatosUpdateController');
        expect(rota.templateUrl).toBe('components/update/pessoa-fisica/contatos/contatos-update.html');
    });

    it('Deve chamar contatosUpdate.buscar()', function () {
        var deferred = $q.defer();
        deferred.resolve([{}]);

        spyOn(contatosUpdate, 'buscar').and.returnValue(deferred.promise);

        $route = {
            current: {
                params: {
                    cpf: cpf
                }
            }
        }

        rota.resolve.contatosInfo(contatosUpdate, $route);
        $rootScope.$apply();

        expect(contatosUpdate.buscar).toHaveBeenCalledWith(cpf);
    });

    it('Deve chamar contatosUpdate Reject', function () {
        var deferred = $q.defer();
        deferred.reject([]);

        spyOn(contatosUpdate, 'buscar').and.returnValue(deferred.promise);

        $route = {
            current: {
                params: {
                    cpf: cpf
                }
            }
        }

        rota.resolve.contatosInfo(contatosUpdate, $route);
        $rootScope.$apply();

        expect(contatosUpdate.buscar).toHaveBeenCalledWith(cpf);
    });

    it('Deve chamar contatosUpdate.buscar()', function () {
        rota.resolve.section(secao);
        $rootScope.$apply();

        expect(secao.nome).toBeDefined();
    });

    it('deve verificar se é terceiro', function() {
        var cpf = '93994468248';

        var deferred = $q.defer();
        deferred.resolve({});
        spyOn(rotaService, 'isNotTerceiro').and.returnValue(deferred.promise);

        $route = {
            current: {
                params: {
                    cpf: cpf
                }
            }
        }

        rota.resolve.isNotTerceiro($route, rotaService);
        expect(rotaService.isNotTerceiro).toHaveBeenCalledWith(cpf);
    });
});
