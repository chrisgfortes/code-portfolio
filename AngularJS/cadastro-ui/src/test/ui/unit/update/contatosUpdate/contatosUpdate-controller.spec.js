describe('controller: contatosUpdateController', function () {

    var ctrl, cpf = '08094498625';
    var contatosInfo, contatosUpdate, $rootScope;
    var erros = [{}];

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_contatosUpdate_, _$controller_, _$rootScope_, _$q_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        contatosUpdate = _contatosUpdate_;

        ctrl = $controller('contatosUpdateController', {
            contatosInfo: [],
            contatosUpdate: _contatosUpdate_,
            $routeParams: {
                cpf: cpf
            }
        });
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('deve chamar update', function () {
        var contatos = [];
        var deferred = $q.defer();
        deferred.resolve({});
        ctrl.savingProgress = true;

        spyOn(contatosUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(contatos);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(contatosUpdate.salvar).toHaveBeenCalledWith(cpf, contatos);
    });

    it('deve mostrar messagem de validação, envida pelo body', function () {
        var contatos = [];
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                body: erros
            }
        });
        ctrl.savingProgress = true;

        spyOn(contatosUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(contatos);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(contatosUpdate.salvar).toHaveBeenCalledWith(cpf, contatos);
        expect(ctrl.erros).toEqual(erros);
        expect(ctrl.sucessos).toEqual([]);
    });

    it('deve mostrar messagem de validação, envida pelo content', function () {
        var contatos = [];
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                content: erros
            }
        });
        ctrl.savingProgress = true;

        spyOn(contatosUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(contatos);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(contatosUpdate.salvar).toHaveBeenCalledWith(cpf, contatos);
        expect(ctrl.erros).toEqual(erros);
        expect(ctrl.sucessos).toEqual([]);
    });

    it('não deve mostrar messagem de validação', function () {
        var contatos = [];
        var deferred = $q.defer();
        deferred.reject({
            status: 400,
            data: {
                content: erros
            }
        });
        ctrl.savingProgress = true;

        spyOn(contatosUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(contatos);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(contatosUpdate.salvar).toHaveBeenCalledWith(cpf, contatos);
        expect(ctrl.erros).not.toBeDefined();
        expect(ctrl.sucessos).toEqual([]);
    });
});
