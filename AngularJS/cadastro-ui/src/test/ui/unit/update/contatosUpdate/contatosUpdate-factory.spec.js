describe('factory: contatosUpdate', function () {

    var contatosUpdate,
        httpBackend,
        moment,
        HOST;

    beforeEach(module('app'));

    beforeEach(inject(function ($httpBackend, _contatosUpdate_, _moment_, _HOST_) {
        contatosUpdate = _contatosUpdate_;
        moment = _moment_;
        HOST = _HOST_;

        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));

    afterEach(function () {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.buscar(cpf)', function () {
        var cpf = '83084412545';
        var dadosFake = contatosFake(moment);

        httpBackend
            .expectGET(HOST.terceiro + cpf + '/contatos')
            .respond(200, dadosFake);

        contatosUpdate
            .buscar(cpf)
            .then(function (response) {
                var dados = response;
                expect(dados).toEqual(dadosFake);
            });

        httpBackend.flush();
    });

    it('.salvar(cpf, dados)', function () {
        var cpf = '83084412545';
        var dadosFake = contatosFake(moment);

        httpBackend
            .expectPOST(HOST.terceiro + cpf + '/contatos')
            .respond(201, dadosFake);

        contatosUpdate
            .salvar(cpf, dadosFake)
            .then(function (response) {
                var dados = response.data;
                expect(dados).toEqual(dadosFake);
            });

        httpBackend.flush();
    });
});

function contatosFake(moment){
 return [
    {
      "cpf": "83084412545",
      "dataNascimento": moment("2017-06-14"),
      "nomeCompleto": "Gabriela Maria Rubik",
      "tipoDependencia": "CONJUGE",
      "valorPensao": 1315.2,
      "valorRenda": 2508.57
    }
  ]
}
