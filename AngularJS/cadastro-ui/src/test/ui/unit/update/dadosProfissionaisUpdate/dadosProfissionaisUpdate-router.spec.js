describe('Router: dadosProfissionaisUpdate-router', function () {

    var dadosProfissionaisUpdate,
        $route,
        rota,
        secao,
        $rootScope,
        $q,
        rotaService,
        cpf = '123456789123';

    beforeEach(module('app'));

    beforeEach(inject(function (_dadosProfissionaisUpdate_, _$rootScope_, _secao_, _$route_, _$q_, _rotaService_) {
        $route = _$route_;
        $rootScope = _$rootScope_;
        $route = _$route_;
        dadosProfissionaisUpdate = _dadosProfissionaisUpdate_;
        secao = _secao_;
        $q = _$q_;
        rotaService = _rotaService_;

        rota = $route.routes['/alteracao/pessoa-fisica/:cpf/dados-profissionais'];
    }));

    it('Deve testar dadosProfissionaisUpdate-router', function() {
        expect(rota.controller).toBe('dadosProfissionaisUpdateController');
        expect(rota.templateUrl).toBe('components/update/pessoa-fisica/dados-profissionais/dados-profissionais-update.html');
    });

    it('Deve chamar dadosProfissionaisUpdate.buscar()', function () {
        var deferred = $q.defer();
        deferred.resolve({});

        spyOn(dadosProfissionaisUpdate, 'buscar').and.returnValue(deferred.promise);

        $route = {
            current: {
                params: {
                    cpf: cpf
                }
            }
        }

        rota.resolve.dadosProfissionaisInfo(dadosProfissionaisUpdate, $route);
        $rootScope.$apply();

        expect(dadosProfissionaisUpdate.buscar).toHaveBeenCalledWith(cpf);
    });

    it('Deve chamar dadosProfissionaisUpdate Reject', function () {
        var deferred = $q.defer();
        deferred.reject({});

        spyOn(dadosProfissionaisUpdate, 'buscar').and.returnValue(deferred.promise);

        $route = {
            current: {
                params: {
                    cpf: cpf
                }
            }
        }

        rota.resolve.dadosProfissionaisInfo(dadosProfissionaisUpdate, $route);
        $rootScope.$apply();

        expect(dadosProfissionaisUpdate.buscar).toHaveBeenCalledWith(cpf);
    });

    it('Deve chamar dadosProfissionaisUpdate.buscar()', function () {
        rota.resolve.section(secao);
        $rootScope.$apply();

        expect(secao.nome).toBeDefined();
    });

    it('deve verificar se é terceiro', function() {
        var cpf = '93994468248';

        var deferred = $q.defer();
        deferred.resolve({});
        spyOn(rotaService, 'isNotTerceiro').and.returnValue(deferred.promise);

        $route = {
            current: {
                params: {
                    cpf: cpf
                }
            }
        }

        rota.resolve.isNotTerceiro($route, rotaService);
        expect(rotaService.isNotTerceiro).toHaveBeenCalledWith(cpf);
    });
});
