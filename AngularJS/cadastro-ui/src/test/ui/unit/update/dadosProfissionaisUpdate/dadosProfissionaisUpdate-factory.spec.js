describe('factory: dadosProfissionaisUpdate', function () {

    var dadosProfissionaisUpdate,
        httpBackend,
        moment,
        HOST;

    beforeEach(module('app'));

    beforeEach(inject(function ($httpBackend, _dadosProfissionaisUpdate_, _moment_, _HOST_) {
        dadosProfissionaisUpdate = _dadosProfissionaisUpdate_;
        moment = _moment_;
        HOST = _HOST_;

        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));

    afterEach(function () {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.buscar(cpf)', function () {
        var cpf = '83084412545';
        var dadosFake = dadosProfissionaisFake(moment);

        httpBackend
            .expectGET(HOST.terceiro + cpf + '/dados-profissionais')
            .respond(200, dadosFake);

        dadosProfissionaisUpdate
            .buscar(cpf)
            .then(function (response) {
                var dados = response;
                expect(dados).toEqual(dadosFake);
            });

        httpBackend.flush();
    });

    it('.salvar(cpf, dados)', function () {
        var cpf = '83084412545';
        var dadosFake = dadosProfissionaisFake(moment);

        httpBackend
            .expectPUT(HOST.terceiro + cpf + '/dados-profissionais')
            .respond(201, dadosFake);

        dadosProfissionaisUpdate
            .salvar(cpf, dadosFake)
            .then(function (response) {
                var dados = response.data;
                expect(dados).toEqual(dadosFake);
            });

        httpBackend.flush();
    });
});

function dadosProfissionaisFake(moment){
 return {
         "id":null,
         "idPessoaFisica":null,
         "grauInstrucao":"ENSINO_FUNDAMENTAL_INCOMPLETO",
         "cdProfissao":433,
         "cdEspecialidade":204,
         "nrRegistroProfissional":"134131435",
         "cdOrgaoResponsavel":43,
         "ufRegistro":"PB",
         "inicioProfissional":"2017-06-07"
      }
}
