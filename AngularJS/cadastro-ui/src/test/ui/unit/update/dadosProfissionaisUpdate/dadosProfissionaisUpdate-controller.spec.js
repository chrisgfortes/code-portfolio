describe('controller: dadosProfissionaisUpdateController', function () {

    var ctrl, cpf = '08094498625';
    var dadosProfissionaisInfo, dadosProfissionaisUpdate, $rootScope;
    var erros = [{}];

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_dadosProfissionaisUpdate_, _$controller_, _$rootScope_, _$q_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        dadosProfissionaisUpdate = _dadosProfissionaisUpdate_;

        ctrl = $controller('dadosProfissionaisUpdateController', {
            dadosProfissionaisInfo: {},
            dadosProfissionaisUpdate: _dadosProfissionaisUpdate_,
            $routeParams: {
                cpf: cpf
            }
        });
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('deve chamar update', function () {
        var dadosProfissionaisObj = {};
        var deferred = $q.defer();
        deferred.resolve({});
        ctrl.savingProgress = true;

        spyOn(dadosProfissionaisUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(dadosProfissionaisObj);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(dadosProfissionaisUpdate.salvar).toHaveBeenCalledWith(cpf, dadosProfissionaisObj);
    });

    it('deve mostrar messagem de validação, envida pelo body', function () {
        var dadosProfissionaisObj = [];
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                body: erros
            }
        });
        ctrl.savingProgress = true;

        spyOn(dadosProfissionaisUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(dadosProfissionaisObj);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(dadosProfissionaisUpdate.salvar).toHaveBeenCalledWith(cpf, dadosProfissionaisObj);
        expect(ctrl.erros).toEqual(erros);
        expect(ctrl.sucessos).toEqual([]);
    });

    it('deve mostrar messagem de validação, envida pelo content', function () {
        var dadosProfissionaisObj = {};
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                content: erros
            }
        });
        ctrl.savingProgress = true;

        spyOn(dadosProfissionaisUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(dadosProfissionaisObj);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(dadosProfissionaisUpdate.salvar).toHaveBeenCalledWith(cpf, dadosProfissionaisObj);
        expect(ctrl.erros).toEqual(erros);
        expect(ctrl.sucessos).toEqual([]);
    });

    it('não deve mostrar messagem de validação', function () {
        var dadosProfissionaisObj = [];
        var deferred = $q.defer();
        deferred.reject({
            status: 400,
            data: {
                content: erros
            }
        });
        ctrl.savingProgress = true;

        spyOn(dadosProfissionaisUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(dadosProfissionaisObj);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(dadosProfissionaisUpdate.salvar).toHaveBeenCalledWith(cpf, dadosProfissionaisObj);
        expect(ctrl.erros).not.toBeDefined();
        expect(ctrl.sucessos).toEqual([]);
    });
});
