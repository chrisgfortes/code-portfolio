describe('controller: veiculosController', function () {

    var ctrl, cpf = '08094498625';
    var veiculosInfo, veiculosUpdate, $rootScope;
    var erros = [{}];

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_veiculosUpdate_, _$controller_, _$rootScope_, _$q_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        veiculosUpdate = _veiculosUpdate_;

        ctrl = $controller('veiculosUpdateController', {
            veiculosInfo: {},
            veiculosUpdate: _veiculosUpdate_,
            $routeParams: {
                cpf: cpf
            }
        });
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('deve chamar update', function () {
        var veiculosObj = {};
        var deferred = $q.defer();
        deferred.resolve({});
        ctrl.savingProgress = true;

        spyOn(veiculosUpdate, 'salvar').and.returnValue(deferred.promise);
        spyOn(veiculosUpdate, 'buscar').and.returnValue(deferred.promise);
        spyOn($rootScope, '$broadcast').and.callThrough();

        ctrl.update(veiculosObj);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(veiculosUpdate.salvar).toHaveBeenCalledWith(cpf, veiculosObj);
        expect($rootScope.$broadcast).toHaveBeenCalledWith('atualizar-descricao-tipo-veiculo', []);
    });

    it('deve mostrar messagem de validação, envida pelo body', function () {
        var veiculosObj = [];
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                body: erros
            }
        });
        ctrl.savingProgress = true;

        spyOn(veiculosUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(veiculosObj);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(veiculosUpdate.salvar).toHaveBeenCalledWith(cpf, veiculosObj);
        expect(ctrl.erros).toEqual(erros);
        expect(ctrl.sucessos).toEqual([]);
    });

    it('deve mostrar messagem de validação, envida pelo content', function () {
        var veiculosObj = {};
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                content: erros
            }
        });
        ctrl.savingProgress = true;

        spyOn(veiculosUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(veiculosObj);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(veiculosUpdate.salvar).toHaveBeenCalledWith(cpf, veiculosObj);
        expect(ctrl.erros).toEqual(erros);
        expect(ctrl.sucessos).toEqual([]);
    });

    it('não deve mostrar messagem de validação', function () {
        var veiculosObj = [];
        var deferred = $q.defer();
        deferred.reject({
            status: 400,
            data: {
                content: erros
            }
        });
        ctrl.savingProgress = true;

        spyOn(veiculosUpdate, 'salvar').and.returnValue(deferred.promise);

        ctrl.update(veiculosObj);

        $rootScope.$apply();

        expect(ctrl.savingProgress).toBe(false);
        expect(veiculosUpdate.salvar).toHaveBeenCalledWith(cpf, veiculosObj);
        expect(ctrl.erros).not.toBeDefined();
        expect(ctrl.sucessos).toEqual([]);
    });
});
