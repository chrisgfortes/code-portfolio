describe('factory: veiculosUpdate', function () {

    var veiculosUpdate,
        httpBackend,
        moment,
        HOST;

    beforeEach(module('app'));

    beforeEach(inject(function ($httpBackend, _veiculosUpdate_, _moment_, _HOST_) {
        veiculosUpdate = _veiculosUpdate_;
        moment = _moment_;
        HOST = _HOST_;

        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));

    afterEach(function () {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.buscar(cpf)', function () {
        var cpf = '83084412545';
        var dadosFake = veiculosFake(moment);

        httpBackend
            .expectGET(HOST.terceiro + cpf + '/veiculos')
            .respond(200, dadosFake);

        veiculosUpdate
            .buscar(cpf)
            .then(function (response) {
                var dados = response;
                expect(dados).toEqual(dadosFake);
            });

        httpBackend.flush();
    });

    it('.salvar(cpf, dados)', function () {
        var cpf = '83084412545';
        var dadosFake = veiculosFake(moment);

        httpBackend
            .expectPUT(HOST.terceiro + cpf + '/veiculos')
            .respond(201, dadosFake);

        veiculosUpdate
            .salvar(cpf, dadosFake)
            .then(function (response) {
                var dados = response.data;
                expect(dados).toEqual(dadosFake);
            });

        httpBackend.flush();
    });

    it('.excluir(cpf, id)', function () {
        var modelResponse = {};

        var cpf = '83084412545';
        var id = 1;

        httpBackend
            .expectDELETE(HOST.terceiro + cpf + '/veiculos/' + id)
            .respond(204, modelResponse);

        veiculosUpdate
            .excluir(cpf, id)
            .then(function (response) {
                var dados = response.data;
                expect(dados).toEqual(modelResponse);
            });

        httpBackend.flush();
    });
});

function veiculosFake(moment){
 return [{
          "tipoVeiculo":"AUTOMOTIVO",
          "situacaoVeiculo":"LIVRE",
          "modelo":"modelo 1",
          "marca":"xuxa",
          "anoFabricacao":moment(1990),
          "valorVeiculo":234.56,
          "valorAlienado":null,
          "chassis":null,
          "renavam":null,
          "anoModelo":moment(1992),
          "numeroNotaFiscal":null,
          "nomeFornecedor":null,
          "placa":null,
          "numeroCertificado":null
       },
       {
          "tipoVeiculo":"CARGA",
          "situacaoVeiculo":"ALIENADO",
          "modelo":"Fusca",
          "marca":"VW",
          "anoFabricacao":moment(2010),
          "valorVeiculo":3151.31,
          "valorAlienado":32.12,
          "chassis":"asd531453",
          "renavam":"35132135",
          "anoModelo":moment(2010),
          "numeroNotaFiscal":51352153,
          "nomeFornecedor":"Vasop",
          "placa":"jlk1234",
          "numeroCertificado":"5315135"
       }
    ]
}
