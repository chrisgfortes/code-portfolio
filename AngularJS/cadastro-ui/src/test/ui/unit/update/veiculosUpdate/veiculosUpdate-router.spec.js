describe('Router: veiculosUpdate-router', function () {

    var veiculosUpdate,
        $route,
        rota,
        secao,
        $rootScope,
        $q,
        rotaService,
        cpf = '123456789123';

    beforeEach(module('app'));

    beforeEach(inject(function (_veiculosUpdate_, _$rootScope_, _secao_, _$route_, _$q_, _rotaService_) {
        $route = _$route_;
        $rootScope = _$rootScope_;
        $route = _$route_;
        veiculosUpdate = _veiculosUpdate_;
        secao = _secao_;
        $q = _$q_;
        rotaService = _rotaService_;

        rota = $route.routes['/alteracao/pessoa-fisica/:cpf/veiculos'];
    }));

    it('Deve testar veiculosUpdate-router', function() {
        expect(rota.controller).toBe('veiculosUpdateController');
        expect(rota.templateUrl).toBe('components/update/pessoa-fisica/veiculos/veiculos-update.html');
    });

    it('Deve chamar veiculosUpdate.buscar()', function () {
        var deferred = $q.defer();
        deferred.resolve({});

        spyOn(veiculosUpdate, 'buscar').and.returnValue(deferred.promise);

        $route = {
            current: {
                params: {
                    cpf: cpf
                }
            }
        }

        rota.resolve.veiculosInfo(veiculosUpdate, $route);
        $rootScope.$apply();

        expect(veiculosUpdate.buscar).toHaveBeenCalledWith(cpf);
    });

    it('Deve chamar veiculosUpdate Reject', function () {
        var deferred = $q.defer();
        deferred.reject({});

        spyOn(veiculosUpdate, 'buscar').and.returnValue(deferred.promise);

        $route = {
            current: {
                params: {
                    cpf: cpf
                }
            }
        }

        rota.resolve.veiculosInfo(veiculosUpdate, $route);
        $rootScope.$apply();

        expect(veiculosUpdate.buscar).toHaveBeenCalledWith(cpf);
    });

    it('Deve chamar veiculosUpdate.buscar()', function () {
        rota.resolve.section(secao);
        $rootScope.$apply();

        expect(secao.nome).toBeDefined();
    });

    it('deve verificar se é terceiro', function() {
        var cpf = '93994468248';

        var deferred = $q.defer();
        deferred.resolve({});
        spyOn(rotaService, 'isNotTerceiro').and.returnValue(deferred.promise);

        $route = {
            current: {
                params: {
                    cpf: cpf
                }
            }
        }

        rota.resolve.isNotTerceiro($route, rotaService);
        expect(rotaService.isNotTerceiro).toHaveBeenCalledWith(cpf);
    });
});
