describe('Component: dadosCpfNome', function () {

    var $componentController,
        ctrl;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_) {
        $componentController = _$componentController_;

        ctrl = $componentController('dadosCpfNome');

        ctrl.$onInit();
    }));

    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o cpfNome no bindings', function () {
        expect(ctrl.cpfNome).toBeDefined();
    });

    it('deve mostrar NOME e SOBRENOME', function () {
        var nomeCompletro = 'João Pereira da Silva';
        ctrl.abreviarNome(nomeCompletro);
        expect(ctrl.cpfNome.nomeSucinto).toBe('João Silva');
    });

    it('não deve mostrar NOME duplicado caso seja apenas 1', function () {
        var nomeCompletro = 'João';
        ctrl.abreviarNome(nomeCompletro);
        expect(ctrl.cpfNome.nomeSucinto).toBe('João');
    });

    it('não deve mostrar NOME duplicado caso seja apenas 1', function () {
        var nomeCompletro = 'Joãoasddsaqwereqasdtreasder';
        ctrl.abreviarNome(nomeCompletro);
        expect(ctrl.cpfNome.nomeSucinto).toBe('Joãoasddsaqwereqasdtreasde');
        expect(ctrl.cpfNome.nomeSucinto.length).toBe(26);
    });
});
