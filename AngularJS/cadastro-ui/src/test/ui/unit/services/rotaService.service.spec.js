describe('rotaService.service.js', function() {

    var rotaService, cpf = '93994468248';

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function(_rotaService_) {
        rotaService = _rotaService_;

    }));

    it('isTerceiro(cpf).then()',
        inject(function($rootScope, pessoa, $location, $q) {
            var status = pessoaStatusFake();

            var deferred = $q.defer();
            deferred.resolve(status);

            spyOn(pessoa, 'status').and.returnValue(deferred.promise);
            spyOn($rootScope, '$emit').and.callThrough();
            spyOn($location, 'path').and.callThrough();

            rotaService.isTerceiro(cpf);

            $rootScope.$apply();

            var mensagem = {
                titulo: 'Terceiro já cadastrado',
                descricao: 'O cadastro do CPF ' + status.cpfCnpj + ' já foi inserido anteriormente.'
            }

            expect(pessoa.status).toHaveBeenCalledWith(cpf, 'terceiro');
            expect($rootScope.$emit).toHaveBeenCalledWith('mostrar-mensagem', mensagem);
            expect($location.path).toHaveBeenCalledWith('cadastro');
        })
    );

    it('isTerceiro(cpf).catch()',
        inject(function($rootScope, pessoa, $location, $q) {
            var status = pessoaStatusFake();

            var deferred = $q.defer();
            deferred.reject({
                status: 404
            });

            spyOn(pessoa, 'status').and.returnValue(deferred.promise);
            spyOn($rootScope, '$emit').and.callThrough();
            spyOn($location, 'path').and.callThrough();

            rotaService.isTerceiro(cpf);

            $rootScope.$apply();

            var mensagem = {
                titulo: 'Terceiro já cadastrado',
                descricao: 'O cadastro do CPF ' + status.cpfCnpj + ' já foi inserido anteriormente.'
            }

            expect(pessoa.status).toHaveBeenCalledWith(cpf, 'terceiro');
            expect($rootScope.$emit).not.toHaveBeenCalledWith('mostrar-mensagem', mensagem);
            expect($location.path).not.toHaveBeenCalledWith('cadastro');
        })
    );

    it('isNotTerceiro(cpf).catch',
        inject(function($rootScope, pessoa, $location, $q) {
            var status = pessoaStatusFake();

            var deferred = $q.defer();
            deferred.reject({
                status: 404
            });

            spyOn(pessoa, 'status').and.returnValue(deferred.promise);
            spyOn($rootScope, '$emit').and.callThrough();
            spyOn($location, 'path').and.callFake(respostaFake({
                status: 200
            }));

            rotaService.isNotTerceiro(cpf);

            $rootScope.$apply();

            var mensagem = {
                titulo: 'Terceiro não cadastrado',
                descricao: 'O CPF pesquisado ainda não é terceiro.'
            }

            expect(pessoa.status).toHaveBeenCalledWith(cpf, 'terceiro');
            expect($rootScope.$emit).toHaveBeenCalledWith('mostrar-mensagem', mensagem);
            expect($location.path).toHaveBeenCalledWith('cadastro');
        })
    );

    it('isAssociado(cpf).then()',
        inject(function($rootScope, pessoa, $location, $q) {
            var status = pessoaStatusFake();

            var deferred = $q.defer();
            deferred.resolve(status);

            spyOn(pessoa, 'status').and.returnValue(deferred.promise);
            spyOn($rootScope, '$emit').and.callThrough();
            spyOn($location, 'path').and.callThrough();

            rotaService.isAssociado(cpf);

            $rootScope.$apply();

            var mensagem = {
                titulo: 'Associado já cadastrado',
                descricao: 'O cadastro do CPF ' + status.cpfCnpj + ' já foi confirmado como associado anteriormente.'
            }

            expect(pessoa.status).toHaveBeenCalledWith(cpf, 'associado');
            expect($rootScope.$emit).toHaveBeenCalledWith('mostrar-mensagem', mensagem);
            expect($location.path).toHaveBeenCalledWith('cadastro');
        })
    );

    it('isTerceiro(cpf).catch()',
        inject(function($rootScope, pessoa, $location, $q) {
            var status = pessoaStatusFake();

            var deferred = $q.defer();
            deferred.reject({
                status: 404
            });

            spyOn(pessoa, 'status').and.returnValue(deferred.promise);
            spyOn($rootScope, '$emit').and.callThrough();
            spyOn($location, 'path').and.callThrough();

            rotaService.isAssociado(cpf);

            $rootScope.$apply();

            var mensagem = {
                titulo: 'Associado já cadastrado',
                descricao: 'O cadastro do CPF ' + status.cpfCnpj + ' já foi confirmado como associado anteriormente.'
            }

            expect(pessoa.status).toHaveBeenCalledWith(cpf, 'associado');
            expect($rootScope.$emit).not.toHaveBeenCalledWith('mostrar-mensagem', mensagem);
            expect($location.path).not.toHaveBeenCalledWith('cadastro');
        })
    );

    it('isNotAssociado(cpf).catch',
        inject(function($rootScope, pessoa, $location, $q) {
            var status = pessoaStatusFake();

            var deferred = $q.defer();
            deferred.reject({
                status: 404
            });

            spyOn(pessoa, 'status').and.returnValue(deferred.promise);
            spyOn($rootScope, '$emit').and.callThrough();
            spyOn($location, 'path').and.callFake(respostaFake({
                status: 200
            }));

            rotaService.isNotAssociado(cpf);

            $rootScope.$apply();

            var mensagem = {
                titulo: 'Associado não cadastrado',
                descricao: 'O CPF pesquisado ainda não é associado.'
            }

            expect(pessoa.status).toHaveBeenCalledWith(cpf, 'associado');
            expect($rootScope.$emit).toHaveBeenCalledWith('mostrar-mensagem', mensagem);
            expect($location.path).toHaveBeenCalledWith('cadastro');
        })
    );

    function pessoaStatusFake() {
        return {
            cpfCnpj: cpf,
            nome: "Lancelot de Jesus",
            status: "OK"
        }
    }

});
