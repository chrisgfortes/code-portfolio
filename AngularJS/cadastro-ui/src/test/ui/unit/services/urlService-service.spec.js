describe('Service: urlService', function () {

    var urlService,
        patternDelimiter = /^\:/g,
        cpf = '93994468248';

    beforeEach(module('app'));

    beforeEach(inject(function (_urlService_) {
        urlService = _urlService_;
    }));

    it('deve estar definido', function () {
        expect(urlService).toBeDefined();
    });

    it('Mapea os parametros da string', function () {
        var url = 'cadastro/terceiro/:cpf/dados-pessoais';
        var mapper = {
            cpf: cpf
        }

        var urlDirect = urlService.parse(url, mapper);

        expect(urlDirect).toEqual('cadastro/terceiro/' + cpf + '/dados-pessoais');
    });

    it('Mapea os parametros da string mas não acha mapper', function () {
        var url = 'cadastro/terceiro/:id/dados-pessoais';
        var mapper = {
            cpf: cpf
        }

        var urlDirect = urlService.parse(url, mapper);
        expect(urlDirect).toEqual('cadastro/terceiro/:id/dados-pessoais');
    });

    it('Mapea os parametros da string com mais de um paramentro', function () {
        var url = 'cadastro/terceiro/:cpf/dados-pessoais/:id';
        var mapper = {
            cpf: cpf,
            id: 123
        }

        var urlDirect = urlService.parse(url, mapper);
        expect(urlDirect).toEqual('cadastro/terceiro/93994468248/dados-pessoais/123');
    });

    it('Get url selecionada', function () {
        var urlObj = urlService.getUrl('terceiro');
        expect(urlObj).toEqual(_mockUrl());
    });
});

function _mockUrl(){
    return {
        type: 'terceiro',
        url: 'cadastro/terceiro/:cpf/dados-pessoais/'
    }
}