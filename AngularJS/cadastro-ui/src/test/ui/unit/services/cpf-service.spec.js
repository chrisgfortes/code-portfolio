describe('cpf-service.js', function () {

    var cpfService;

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_cpfService_) {
        cpfService = _cpfService_;

    }));

    it('Deve validar se cpf é cheio de números iguais', function () {
        var cpfs = ['1', '00000000000', '11111111111', '22222222222', '33333333333',
            '44444444444', '55555555555', '66666666666', '77777777777', '88888888888', '99999999999']

        cpfs.forEach(function (cpf) {
            expect(cpfService.validarCPF(cpf)).toBeFalsy();
        })
    });

    it('Não deve aceitar CPF inválido', function () {
        var cpfs = ['02345678909', '12345678908', '12365498712']

        cpfs.forEach(function (cpf) {
            expect(cpfService.validarCPF(cpf)).toBeFalsy();
        })
    });

    it('Deve aceitar CPF válido', function () {
        expect(cpfService.validarCPF('03057115209')).toBe(true);
    });

});
