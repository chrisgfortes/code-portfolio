describe('Component: toast', function () {

    var $componentController,
        ctrl,
        $route,
        $rootScope;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _$route_, _$rootScope_) {
        $componentController = _$componentController_;
        $route = _$route_;
        $rootScope = _$rootScope_;

        var bindings = {
            showToast: false,
            title: '',
            message: '',
            error: true
        }

        ctrl = $componentController('toastMessage', null, bindings);

    }));

    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o bindings', function () {
        expect(ctrl.showToast).toBeDefined();
        expect(ctrl.title).toBeDefined();
        expect(ctrl.message).toBeDefined();
        expect(ctrl.error).toBeDefined();
    });

    it('deve fechar o toast', function () {
        var dismissSpy = jasmine.createSpy('dismiss');
        var bindings = {
            dismiss: dismissSpy
        };

        var ctrl = $componentController('toastMessage', null, bindings);

        ctrl.dismissToast();

        expect(dismissSpy).toHaveBeenCalled();
        expect($rootScope.displayToast).toBe(false);
    });

});
