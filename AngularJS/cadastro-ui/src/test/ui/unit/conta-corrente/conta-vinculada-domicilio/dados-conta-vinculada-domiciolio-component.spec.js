describe('Component: dadosContaVinculadaDomicilio', function () {

    var $rootScope,
        contaCorrente,
        $q;

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_$componentController_, _$rootScope_, _$q_, _contaCorrente_, _moment_) {
        $componentController = _$componentController_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        contaCorrente = _contaCorrente_;

        ctrl = $componentController('dadosContaVinculadaDomicilio', {
            contaCorrente: _contaCorrente_,
            moment: _moment_
        }, {
            contaVinculadaDomicilio: {}
        });
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('deve instanciar', function () {
        ctrl.contaVinculadaDomicilio = undefined;
        ctrl.$onInit()

        expect(ctrl.contaVinculadaDomicilio).toBeDefined();
    });

    it('deve serem chamados ao inicializar o componente', function () {
         ctrl.contaVinculadaDomicilio = { tipoTravaDomicilioBancario: undefined }
        var dados = [{}];

        spyOn(contaCorrente, 'domiciliosBancarios').and.callFake(respostaFake(dados));

        ctrl.$onInit();

        expect(contaCorrente.domiciliosBancarios).toHaveBeenCalled();
        expect(ctrl.contaVinculadaDomicilio.tipoTravaDomicilioBancario).toBe('NAO');
        expect(ctrl.domiciliosBancarios.length).toBe(1);
    });

    it('deve aceitar data valida em validaData()', function () {
        var data = '2017-06-01';
        ctrl.contaVinculadaDomicilio = { dataVigenciaTravaVisa: moment() };
        var campo = 'dataVigenciaTravaVisa'

        ctrl.validaData(data, campo);
        expect(ctrl.contaVinculadaDomicilio.dataVigenciaTravaVisa).not.toBeUndefined();
    });

    it('não deve aceitar data inválida em validaData()', function () {
        var data = "7898-45-45";
        ctrl.contaVinculadaDomicilio = { dataVigenciaTravaVisa: moment() };
        var campo = 'dataVigenciaTravaVisa'

        ctrl.validaData(data, campo);
        expect(ctrl.contaVinculadaDomicilio.dataVigenciaTravaVisa).toBeUndefined();
    });

    it('deve mostrar campo data visa', inject(function($rootScope) {
        var mostrar = ['AMBAS', 'VISA'];

        mostrar.forEach(function(domicilioBancario) {
            var resposta = ctrl.mostarVisa(domicilioBancario);

            expect(resposta).toBe(true);            
        });
    }));

     it('deve mostrar campo data master', inject(function($rootScope) {
        var mostrar = ['AMBAS', 'MASTER'];

        mostrar.forEach(function(domicilioBancario) {
            var resposta = ctrl.mostarMaster(domicilioBancario);

            expect(resposta).toBe(true);            
        });
    }));
});