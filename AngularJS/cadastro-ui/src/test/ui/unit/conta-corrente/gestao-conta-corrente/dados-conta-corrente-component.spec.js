describe('Component: dadosContaCorrente', function () {

    var $rootScope,
        contaCorrente,
        $q;

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_$componentController_, _$rootScope_, _$q_, _contaCorrente_, _moment_) {
        $componentController = _$componentController_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        contaCorrente = _contaCorrente_;

        ctrl = $componentController('dadosContaCorrente', {
            contaCorrente: _contaCorrente_,
            moment: _moment_
        });
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('deve serem chamados ao inicializar o componente', inject(function (enderecoUpdate) {
        var dados = [{}];

        spyOn(contaCorrente, 'tiposConta').and.callFake(respostaFake(dados));
        spyOn(contaCorrente, 'modalidades').and.callFake(respostaFake(dados));
        spyOn(contaCorrente, 'contatosUnicred').and.callFake(respostaFake(dados));
        spyOn(contaCorrente, 'assinaturas').and.callFake(respostaFake(dados));
        spyOn(contaCorrente, 'cpmf').and.callFake(respostaFake(dados));
        spyOn(contaCorrente, 'situacoes').and.callFake(respostaFake(dados));
        spyOn(enderecoUpdate, 'buscar').and.callFake(respostaFake([{
            id: 123,
            tipoEndereco: 'RESIDENCIAL',
            enderecoSemNumero: true,
            numero: 2,
            cep: '94090330',
            logradouro: 'Rua x'
        }]));

        ctrl.$onInit();

        expect(contaCorrente.tiposConta).toHaveBeenCalled();
        expect(ctrl.tiposConta.length).toBe(1);

        expect(contaCorrente.modalidades).toHaveBeenCalled();
        expect(ctrl.modalidades.length).toBe(1);

        expect(contaCorrente.contatosUnicred).toHaveBeenCalled();
        expect(ctrl.contatosUnicred.length).toBe(1);

        expect(contaCorrente.assinaturas).toHaveBeenCalled();
        expect(ctrl.assinaturas.length).toBe(1);

        expect(contaCorrente.cpmf).toHaveBeenCalled();
        expect(ctrl.cpmfLista.length).toBe(1);

        expect(contaCorrente.situacoes).toHaveBeenCalled();
        expect(ctrl.situacoes.length).toBe(1);

        expect(enderecoUpdate.buscar).toHaveBeenCalled();
        expect(ctrl.enderecos.length).toBe(1);

        expect(ctrl.contaCorrente.centralizaSaldo).toBe(true);
        expect(ctrl.contaCorrente.compePropria).toBe(true);
    }));

    it('deve disparar evento \'alterado-compe-propria\'', inject(function($rootScope) {
        spyOn($rootScope, '$broadcast').and.callThrough();

        ctrl.$onInit();

        ctrl.contaCorrente.compePropria = true

        ctrl.alteradoCompePropria()

        expect($rootScope.$broadcast).toHaveBeenCalledWith('alterado-compe-propria', ctrl.contaCorrente.compePropria);
    }));

    it('deve notificar alteração de endereço', function() {
        spyOn($rootScope, '$broadcast').and.callThrough();

        var endereco = { descricao: 'Rua x, 2, CEP - 12456789'};

        ctrl.alteradoEndereco(JSON.stringify(endereco))

        expect($rootScope.$broadcast).toHaveBeenCalledWith('conta-corrente-endereco', endereco);
    });
});