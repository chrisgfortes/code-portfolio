describe('Component: dadosEmprestimo', function () {

    var $rootScope,
        contaCorrente,
        $q;

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_$componentController_, _$rootScope_, _$q_, _contaCorrente_, _moment_) {
        $componentController = _$componentController_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        contaCorrente = _contaCorrente_;

        ctrl = $componentController('dadosEmprestimo', {
            contaCorrente: _contaCorrente_,
            moment: _moment_
        }, {
            indicadoresEmprestimo: {}
        });
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('deve definir indicadoresEmprestimo', function () {
        ctrl.$onInit();

        expect(ctrl.indicadoresEmprestimo).toBeDefined();
    });
});