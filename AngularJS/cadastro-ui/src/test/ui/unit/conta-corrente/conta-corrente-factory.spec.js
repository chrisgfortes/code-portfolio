describe('factory: contraCorrente', function () {

    var HOST,
        contaCorrente,
        httpBackend;

    beforeEach(module('app'));

    beforeEach(inject(function ($httpBackend, _HOST_, _contaCorrente_) {
        httpBackend = $httpBackend;
        HOST = _HOST_;
        contaCorrente = _contaCorrente_;

        $httpBackend.whenGET(/\.html$/).respond('');
    }));

    afterEach(function () {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.contatosUnicred()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.contaCorrente + 'gerentes')
            .respond(200, modelsResponse);

        contaCorrente
            .contatosUnicred()
            .then(function(gerentes) {
                expect(gerentes).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.modalidades()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.contaCorrente + 'modalidades')
            .respond(200, modelsResponse);

        contaCorrente
            .modalidades()
            .then(function(modalidades) {
                expect(modalidades).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.cpmf()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.contaCorrente + 'tipos-cpmf')
            .respond(200, modelsResponse);

        contaCorrente
            .cpmf()
            .then(function(modalidades) {
                expect(modalidades).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.assinaturas()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.contaCorrente + 'assinaturas')
            .respond(200, modelsResponse);

        contaCorrente
            .assinaturas()
            .then(function(assinaturas) {
                expect(assinaturas).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.situacoes()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.contaCorrente + 'situacoes')
            .respond(200, modelsResponse);

        contaCorrente
            .situacoes()
            .then(function(situacoes) {
                expect(situacoes).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

     it('.tiposConta()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.contaCorrente + 'tipos-conta')
            .respond(200, modelsResponse);

        contaCorrente
            .tiposConta()
            .then(function(tiposConta) {
                expect(tiposConta).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.situacoesContaEspelho()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.contaCorrente + 'situacoes-conta-espelho')
            .respond(200, modelsResponse);

        contaCorrente
            .situacoesContaEspelho()
            .then(function(situacoesContaEspelho) {
                expect(situacoesContaEspelho).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.pacotesTarifaBB()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.contaCorrente + 'pacotes-tarifas-conta-espelho')
            .respond(200, modelsResponse);

        contaCorrente
            .pacotesTarifaBB()
            .then(function(pacotesTarifaBB) {
                expect(pacotesTarifaBB).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.tiposCalculoLimiteContaEspelho()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.contaCorrente + 'tipos-calculo-limite-conta-espelho')
            .respond(200, modelsResponse);

        contaCorrente
            .tiposCalculoLimiteContaEspelho()
            .then(function(tiposCalculoLimiteContaEspelho) {
                expect(tiposCalculoLimiteContaEspelho).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.domiciliosBancarios()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.contaCorrente + 'tipos-travas-domicilio-bancario')
            .respond(200, modelsResponse);

        contaCorrente
            .domiciliosBancarios()
            .then(function(domiciliosBancarios) {
                expect(domiciliosBancarios).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.numerosFolhasSolicitadas()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.contaCorrente + 'numero-folhas-talao')
            .respond(200, modelsResponse);

        contaCorrente
            .numerosFolhasSolicitadas()
            .then(function(numerosFolhasSolicitadas) {
                expect(numerosFolhasSolicitadas).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.pacoteServicos()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.contaCorrente + 'pacote-servicos')
            .respond(200, modelsResponse);

        contaCorrente
            .pacoteServicos()
            .then(function(pacoteServicos) {
                expect(pacoteServicos).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.salvar()', function() {
        var modelsResponse = [{}];
        var _contaCorrente = {};

        httpBackend
            .expectPOST(HOST.contaCorrente, _contaCorrente)
            .respond(200, modelsResponse);


        contaCorrente
            .salvar(_contaCorrente)
            .then(function(res) {
                expect(res.data).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.buscarPeloCpf(cpf)', function() {
        var cpf = '93994468248';
        var modelsResponse = [{}];
        httpBackend
            .expectGET(HOST.contaCorrente + '?cpfCnpj=' + cpf)
            .respond(200, modelsResponse);

        contaCorrente
            .buscarPeloCpf(cpf)
            .then(function(contas) {
                expect(contas).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });
});
