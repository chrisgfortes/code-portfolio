describe('Controller: contaCorrenteController', function () {

    var $rootScope,
        contaCorrente,
        cpf = '12345678912',
        $q,
        pessoa;

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_$controller_, _$rootScope_, _$q_, _contaCorrente_, _pessoa_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        contaCorrente = _contaCorrente_;
        pessoa = _pessoa_;

        ctrl = $controller('contaCorrenteController', {
            $routeParams: { cpf: cpf },
            posto: 0
        });
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('deve escutar o evento \'alterado-compe-propria\'', function () {
        ctrl.mostrarContaEspelho = false;

        $rootScope.$broadcast('alterado-compe-propria', true);

        expect(ctrl.mostrarContaEspelho).toBe(false);
    });

    it('deve chamar .salvar(contaCorrente)', function () {
        var deferred = $q.defer();
        deferred.resolve({});
        spyOn(contaCorrente, 'salvar').and.returnValue(deferred.promise);

        var statusPessoa = pessoaStatusFake();
        var deferredStatus = $q.defer();
        deferredStatus.resolve(statusPessoa);
        spyOn(pessoa, 'status').and.returnValue(deferredStatus.promise);

        spyOn($rootScope, '$emit').and.callThrough();
        var modal = {
            enable: true,
            cpf: statusPessoa.cpfCnpj,
            nome: statusPessoa.nome,
            titulo: 'Correntista',
            tipo: 'correntista'
        };

        var _contaCorrente = {
            tarifas: {
                naoUtilizaPacoteTarifas: false
            },
            endereco: JSON.stringify({ id: 1, descricao: 'teste', tipoEndereco: 'RESIDENCIAL' })
        };

        ctrl.salvar(_contaCorrente);

        $rootScope.$apply();

        var endereco = JSON.parse(_contaCorrente.endereco)

        _contaCorrente.cpfCnpjPrimeiroTitular = cpf;
        _contaCorrente.empostamento = endereco.id;
        _contaCorrente.tipoEnderecoEmpostamento = endereco.tipoEndereco;

        expect(contaCorrente.salvar).toHaveBeenCalledWith(_contaCorrente);
        expect(ctrl.sucessos).toBeDefined();
        expect($rootScope.$emit).toHaveBeenCalledWith('usuario-pendente', modal);
    });

    it('deve validar antes de salvar', function () {
        var deferred = $q.defer();
        deferred.resolve({});
        spyOn(contaCorrente, 'salvar').and.returnValue(deferred.promise);

        var statusPessoa = pessoaStatusFake();
        var deferredStatus = $q.defer();
        deferredStatus.resolve(statusPessoa);
        spyOn(pessoa, 'status').and.returnValue(deferredStatus.promise);

        spyOn($rootScope, '$emit').and.callThrough();
        var modal = {
            enable: true,
            cpf: statusPessoa.cpfCnpj,
            nome: statusPessoa.nome,
            titulo: 'Correntista',
            tipo: 'correntista'
        };

        var _contaCorrente = {
            compePropria: true,
            contaEspelho: {},
            endereco: JSON.stringify({ id: 1, descricao: 'teste', tipoEndereco: 'RESIDENCIAL' }),
            tarifas: {
                naoUtilizaPacoteTarifas: true
            }
        };

        ctrl.salvar(_contaCorrente);


        $rootScope.$apply();

        delete _contaCorrente.contaEspelho
        delete _contaCorrente.tarifas
        _contaCorrente.cpfCnpjPrimeiroTitular = cpf;
        _contaCorrente.empostamento = 0;

        var endereco = JSON.parse(_contaCorrente.endereco);
        _contaCorrente.empostamento = endereco.id;
        _contaCorrente.tipoEnderecoEmpostamento = endereco.tipoEndereco;

        expect(contaCorrente.salvar).toHaveBeenCalledWith(_contaCorrente);
        expect(ctrl.sucessos).toBeDefined();
        expect($rootScope.$emit).toHaveBeenCalledWith('usuario-pendente', modal);
    });

    it('deve notificar erro ao tentar salvar(contaCorrente), status 422', function () {
        var mensagensErros = [];

        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                details: mensagensErros
            }
        });

        spyOn(contaCorrente, 'salvar')
            .and
            .returnValue(deferred.promise);

        var _contaCorrente = {
            endereco: JSON.stringify({ id: 1, descricao: 'teste', tipoEndereco: 'RESIDENCIAL' }),
            tarifas: {
                naoUtilizaPacoteTarifas: false
            }
        };

        ctrl.salvar(_contaCorrente);

        $rootScope.$apply();

        var endereco = JSON.parse(_contaCorrente.endereco);
        _contaCorrente.empostamento = endereco.id;
        _contaCorrente.tipoEnderecoEmpostamento = endereco.tipoEndereco;

        _contaCorrente.cpfCnpjPrimeiroTitular = cpf;

        expect(contaCorrente.salvar).toHaveBeenCalledWith(_contaCorrente);
        expect(ctrl.erros).toEqual(mensagensErros);
    });


    it('deve notificar erro ao tentar salvar(contaCorrente), status 500', function () {
        var mensagensErros = [{ message: 'Ocorreu um erro no servidor, tente novamente.' }];

        var deferred = $q.defer();
        deferred.reject({
            status: 500,
            data: mensagensErros
        });
        spyOn(contaCorrente, 'salvar').and.returnValue(deferred.promise);

        var _contaCorrente = {
            endereco: JSON.stringify({ id: 1, descricao: 'teste', tipoEndereco: 'RESIDENCIAL' }),
            tarifas: {
                naoUtilizaPacoteTarifas: false
            }
        };

        ctrl.salvar(_contaCorrente);

        $rootScope.$apply();

        _contaCorrente.cpfCnpjPrimeiroTitular = cpf;

        var endereco = JSON.parse(_contaCorrente.endereco);
        _contaCorrente.empostamento = endereco.id;
        _contaCorrente.tipoEnderecoEmpostamento = endereco.tipoEndereco;

        expect(contaCorrente.salvar).toHaveBeenCalledWith(_contaCorrente);
        expect(ctrl.erros).toEqual(mensagensErros);
    });

    function pessoaStatusFake() {
        return {
            cpfCnpj: "93994468248",
            nome: "Lancelot de Jesus",
            status: "OK"
        }
    }

});
