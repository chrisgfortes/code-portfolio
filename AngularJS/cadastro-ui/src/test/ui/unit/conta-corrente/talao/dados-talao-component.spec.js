describe('Component: dadosTalao', function () {

    var $rootScope,
        contaCorrente,
        $q;

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_$componentController_, _$rootScope_, _$q_, _contaCorrente_, _moment_) {
        $componentController = _$componentController_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        contaCorrente = _contaCorrente_;

        ctrl = $componentController('dadosTalao', {
            contaCorrente: _contaCorrente_,
            moment: _moment_
        }, {
            talao: {}
        });
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('deve definir talao', function () {
        ctrl.$onInit();

        expect(ctrl.talao).toBeDefined();
    });

    it('deve serem chamados ao inicializar o componente', function () {
        var dados = [{}];

        spyOn(contaCorrente, 'numerosFolhasSolicitadas').and.callFake(respostaFake(dados));

        ctrl.$onInit();

        $rootScope.$apply();

        expect(contaCorrente.numerosFolhasSolicitadas).toHaveBeenCalled();
         expect(ctrl.numerosFolhasSolicitadas.length).toBe(1);
    });
});