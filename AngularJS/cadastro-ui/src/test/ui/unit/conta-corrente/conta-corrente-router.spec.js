describe('Router: conta-corrente', function() {

    var $route,
        rota,
        $rootScope;

    beforeEach(module('app'));

    beforeEach(inject(function(_$route_, _$rootScope_) {
        $route = _$route_;
        $rootScope = _$rootScope_;

        rota = $route.routes['/cadastro/correntista/:cpf/conta-corrente'];
    }));

    it('deve validar as configurações', function() {
        expect(rota.templateUrl).toBe('./modules/conta-corrente/conta-corrente.html');
        expect(rota.controller).toBe('contaCorrenteController');
        expect(rota.controllerAs).toBe('ctrl');
    });

    it('deve validar se o cpf acessado é associado',
        inject(function (rotaService, $rootScope, pessoa, $q) {
            var deferred = $q.defer();
            deferred.resolve({});

            var cpf = '37840916953';

            $route = {
                current: {
                    params: {
                        cpf: cpf
                    }
                }
            }

            spyOn(rotaService, 'isNotAssociado').and.returnValue(deferred.promise);

            rota.resolve.isNotAssociado($route, rotaService);
            $rootScope.$apply();

            expect(rotaService.isNotAssociado).toHaveBeenCalledWith(cpf);
        }));

    it('deve retornar o posto do associado',
        inject(function(associado, $location, $rootScope, $q) {
            var cpf = '1234578912'

            var modelsResponse = {
                matricula: {
                    posto: 123
                }
            }

            var deferred = $q.defer();
            deferred.resolve(modelsResponse);

            spyOn(associado, 'buscarMatricula').and.returnValue(deferred.promise);

            $route = {
                current: {
                    params: {
                        cpf: cpf
                    }
                }
            }

            var resposta = rota.resolve.posto(associado, $route);

            $rootScope.$apply();

            expect(associado.buscarMatricula).toHaveBeenCalledWith(cpf);
            expect(resposta.$$state.value).toBe(modelsResponse.matricula.posto);
        }));

    it('deve tratar o erro na chamada do analise cadastral',
        inject(function(associado, $location, $rootScope, $q) {
            var cpf = '1234578912'

            var modelsResponse = {}

            var deferred = $q.defer();
            deferred.reject(modelsResponse);

            spyOn(associado, 'buscarMatricula').and.returnValue(deferred.promise);


            $route = {
                current: {
                    params: {
                        cpf: cpf
                    }
                }
            }

            var resposta = rota.resolve.posto(associado, $route);

            $rootScope.$apply();

            expect(resposta.$$state.value).toBe(0);
        }));
});
