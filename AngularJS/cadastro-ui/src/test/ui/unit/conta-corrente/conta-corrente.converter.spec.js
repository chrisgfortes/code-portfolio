describe('factory: contraCorrenteConverter', function () {

    var contaCorrenteConverter;

    beforeEach(module('app'));

    beforeEach(inject(function (_contaCorrenteConverter_) {
        contaCorrenteConverter = _contaCorrenteConverter_;
    }));

    it('.salvar() deve ser undefined', function () {
        var contaCorrente = {
            clienteDesde: true
        };

        var ctrl = contaCorrenteConverter
            .salvar(contaCorrente);

        expect(ctrl.clienteDesde).toBe(undefined);
    });

    it('.salvar() não deve converter nada', function () {
        var contaCorrente = {};

        var ctrl = contaCorrenteConverter
            .salvar(contaCorrente);

        expect(ctrl).toEqual(contaCorrente);
    });
});
