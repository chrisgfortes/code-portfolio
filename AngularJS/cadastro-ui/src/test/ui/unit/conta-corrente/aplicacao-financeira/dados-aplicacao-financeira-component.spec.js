describe('Component: dadosAplicacaoFinanceira', function () {

    var $rootScope,
        contaCorrente,
        $q;

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_$componentController_, _$rootScope_, _$q_, _contaCorrente_, _moment_) {
        $componentController = _$componentController_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        contaCorrente = _contaCorrente_;

        ctrl = $componentController('dadosAplicacaoFinanceira',  null, {
            aplicacaoFinanceira: {}
        });
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('deve definir aplicacaoFinanceira', function () {
        ctrl.$onInit();

        expect(ctrl.aplicacaoFinanceira).toBeDefined();
    });
});