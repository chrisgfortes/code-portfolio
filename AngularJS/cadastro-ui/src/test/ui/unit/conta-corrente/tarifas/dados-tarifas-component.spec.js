describe('Component: dadosTarifas', function () {

    var $rootScope,
        contaCorrente,
        $q;

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_$componentController_, _$rootScope_, _$q_, _contaCorrente_, _moment_) {
        $componentController = _$componentController_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        contaCorrente = _contaCorrente_;

        ctrl = $componentController('dadosTarifas', {
            contaCorrente: _contaCorrente_,
            moment: _moment_
        }, {
                tarifas: {}
            });
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('deve definir tarifas', function () {
        ctrl.$onInit();

        expect(ctrl.tarifas).toBeDefined();
    });

    it('deve serem chamados ao inicializar o componente', function () {
        var dados = [{}];

        spyOn(contaCorrente, 'pacoteServicos').and.callFake(respostaFake(dados));

        ctrl.$onInit();

        $rootScope.$apply();

        expect(contaCorrente.pacoteServicos).toHaveBeenCalled();
        expect(ctrl.pacotesServicos.length).toBe(1);
    });

    it('deve limpar tarifa', function () {
        ctrl.utilizaPacoteTarifa(false)

        var esperado = { utilizaPacoteTarifas: false, codigoPacote: undefined, diaCobranca: undefined }

        expect(ctrl.tarifas).toEqual(esperado);
    });

    it('não deve limpar tarifa', function () {
        ctrl.tarifas = {
            codigoPacote: 1,
            isento: true,
            diaCobranca: 10
        }

        ctrl.utilizaPacoteTarifa(true)

        expect(ctrl.tarifas).toEqual(ctrl.tarifas);
    });
});