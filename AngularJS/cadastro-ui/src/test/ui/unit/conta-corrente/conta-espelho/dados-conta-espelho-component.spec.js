describe('Component: dadosContaEspelho', function () {

    var $rootScope,
        contaCorrente,
        $q;

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_$componentController_, _$rootScope_, _$q_, _contaCorrente_, _moment_) {
        $componentController = _$componentController_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        contaCorrente = _contaCorrente_;

        ctrl = $componentController('dadosContaEspelho', {
            contaCorrente: _contaCorrente_,
            moment: _moment_
        }, {
                contaEspelho: { enderecoCorrespondencia: '' }
            });
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('deve estar definido', function () {
        ctrl.contaEspelho = undefined;

        ctrl.$onInit();

        expect(ctrl.contaEspelho).toBeDefined();
    });

    it('deve serem chamados ao inicializar o componente', function () {
        var dados = [{}];
        var endereco = { logradouro: 'Rua X', numero: '1', cep: '9875645361', enderecoSemNumero: true };

        spyOn(contaCorrente, 'situacoesContaEspelho').and.callFake(respostaFake(dados));
        spyOn(contaCorrente, 'pacotesTarifaBB').and.callFake(respostaFake(dados));
        spyOn(contaCorrente, 'tiposCalculoLimiteContaEspelho').and.callFake(respostaFake(dados));

        ctrl.$onInit();

        $rootScope.$apply();

        expect(contaCorrente.situacoesContaEspelho).toHaveBeenCalled();
        expect(ctrl.situacoesContaEspelho.length).toBe(1);

        expect(contaCorrente.pacotesTarifaBB).toHaveBeenCalled();
        expect(ctrl.pacotesTarifas.length).toBe(1);

        expect(contaCorrente.tiposCalculoLimiteContaEspelho).toHaveBeenCalled();
        expect(ctrl.tiposCalculoLimiteContaEspelho.length).toBe(1);
    });

    it('deve atualizar endereco correspondência', inject(function ($rootScope) {
        $rootScope.$emit('conta-corrente-endereco', { descricao: 'Rua x, 123, CEP - 123456789' });

        expect(ctrl.contaEspelho.enderecoCorrespondencia).toEqual('Rua x, 123, CEP - 123456789');
    }));
});