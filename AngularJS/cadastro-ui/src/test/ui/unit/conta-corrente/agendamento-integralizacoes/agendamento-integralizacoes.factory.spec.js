describe('factory: agendamentoIntegralizacoes', function () {

    var HOST,
        agendamento,
        httpBackend;

    beforeEach(module('app'));

    beforeEach(inject(function ($httpBackend, _HOST_, _agendamento_) {
        httpBackend = $httpBackend;
        HOST = _HOST_;
        agendamento = _agendamento_;

        $httpBackend.whenGET(/\.html$/).respond('');
    }));

    afterEach(function () {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.formasRecebimento()', function() {
        var modelsResponse = {
            data: [{}]
        }

        httpBackend
            .expectGET(HOST.agendamentoCapital + '/formas-recebimento')
            .respond(200, modelsResponse);

        agendamento
            .formasRecebimento()
            .then(function(recebimentos) {
                expect(recebimentos.data).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.indicadores()', function() {
        var modelsResponse = {
            data: [{}]
        }

        httpBackend
            .expectGET(HOST.agendamentoCapital + '/indicadores')
            .respond(200, modelsResponse);

        agendamento
            .indicadores()
            .then(function(indicadores) {
                expect(indicadores.data).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.tiposAgendamento()', function() {
        var modelsResponse = {
            data: [{}]
        }

        httpBackend
            .expectGET(HOST.agendamentoCapital + '/tipos-agendamento')
            .respond(200, modelsResponse);

        agendamento
            .tiposAgendamento()
            .then(function(tipos) {
                expect(tipos.data).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.tiposLancamento()', function() {
        var modelsResponse = {
            data: [{}]
        }

        httpBackend
            .expectGET(HOST.agendamentoCapital + '/tipos-lancamento')
            .respond(200, modelsResponse);

        agendamento
            .tiposLancamento()
            .then(function(tipos) {
                expect(tipos.data).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.tiposLancamento()', function() {
        var modelsResponse = {
            data: [{}]
        }

        httpBackend
            .expectGET(HOST.agendamentoCapital + '/valor-indicador')
            .respond(200, modelsResponse);

        agendamento
            .valorIndicador()
            .then(function(valores) {
                expect(valores.data).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.getDadosConta()', function () {
        var modelsResponse = {
            data: [{}]
        }

        httpBackend
            .expectGET(HOST.cadastro + '/contas-correntes/37840916953')
            .respond(200, modelsResponse);

        agendamento
            .getDadosConta('37840916953')
            .then(function (conta) {
                expect(conta.data).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });
});