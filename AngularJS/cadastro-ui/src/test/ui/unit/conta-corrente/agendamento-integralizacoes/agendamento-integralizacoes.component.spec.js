describe('Component: DadosAgendamentoIntegralizacoes', function() {

    var $rootScope,
        agendamento,
        moment,
        dialogs,
        message,
        $q,
        associado,
        ctrl;

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function(_$componentController_, _$rootScope_, _$q_, _agendamento_, _moment_, _dialogs_, _message_, _associado_) {
        $componentController = _$componentController_;
        $rootScope = _$rootScope_;
        agendamento = _agendamento_;
        moment = _moment_;
        dialogs = _dialogs_;
        message = _message_;
        $q = _$q_;
        associado = _associado_;

        ctrl = $componentController('dadosAgendamentoIntegralizacoes', {
            agendamento: agendamento,
            moment: moment,
            dialogs: dialogs,
            message: message
        }, {
            agendamentoIntegralizacoes: {}
        });
    }));

    it('deve estar definido', function() {
        expect(ctrl).toBeDefined();
    });

    it('deve serem chamados ao inicializar o componente', function() {
        var dados = {
            data: [{}]
        }

        spyOn(agendamento, 'formasRecebimento').and.callFake(respostaFake(dados));
        spyOn(agendamento, 'indicadores').and.callFake(respostaFake(dados));
        spyOn(agendamento, 'tiposAgendamento').and.callFake(respostaFake(dados));
        spyOn(agendamento, 'tiposLancamento').and.callFake(respostaFake(dados));
        spyOn(agendamento, 'valorIndicador').and.callFake(respostaFake(dados));
        spyOn(associado, 'buscarMatricula').and.callFake(respostaFake({
            matricula: {
                matricula: 107155
            }
        }));

        ctrl.$onInit();

        expect(ctrl.formasRecebimento.length).toBe(1);
        expect(ctrl.formasRecebimento).toEqual(dados.data);

        expect(ctrl.indicadores.length).toBe(1);
        expect(ctrl.indicadores).toEqual(dados.data);

        expect(ctrl.tiposAgendamento.length).toBe(1);
        expect(ctrl.tiposAgendamento).toEqual(dados.data);

        expect(ctrl.tiposLancamento.length).toBe(1);
        expect(ctrl.tiposLancamento).toEqual(dados.data);

        expect(ctrl.valorIndicador.length).toBe(1);
        expect(ctrl.valorIndicador).toEqual(dados.data);
    });

    it('deve serem mostrado campo dos indicadores V', function() {
        var data = JSON.stringify({
            "descricao": "Indicador",
            "valor": "V"
        });

        ctrl.agendamentoIntegralizacoes = {
            valorIndicador: data
        };

        ctrl.setValorIndicador();

        expect()
    });

    it('deve serem mostrado campo dos indicadores I', function() {
        var data = JSON.stringify({
            "descricao": "Indicador",
            "valor": "I"
        });

        ctrl.agendamentoIntegralizacoes = {
            valorIndicador: data
        };

        ctrl.setValorIndicador();

        expect()
    });

    it('não deve mostrar nenhum indicador', function() {
        var data = JSON.stringify({
            "descricao": "Indicador",
            "valor": "O"
        });

        ctrl.agendamentoIntegralizacoes = {
            valorIndicador: data
        };

        ctrl.setValorIndicador();

        expect()
    });

    it('não deve serem mostrado campo dos indicadores', function() {
        ctrl.agendamentoIntegralizacoes = {
            valorIndicador: undefined
        };

        ctrl.setValorIndicador();

        expect(ctrl.agendamentoIntegralizacoes.valorIndicador).toBeUndefined();
    });

    it('deve buscar nome do correntista', function() {
        var conta = '37840916953';
        var dados = _dadosConta();

        var getContaPromise = $q.defer();
        getContaPromise.resolve(dados);

        spyOn(agendamento, 'getDadosConta').and.returnValue(getContaPromise.promise);

        ctrl.agendamentoIntegralizacoes = {};
        ctrl.buscarConta(conta);

        $rootScope.$apply();

        expect(ctrl.nomeCorrentista).toEqual("Clinica39740");
    });

    it('deve buscar correntista sem nome', function() {
        var conta = '37840916953';
        var dados = _dadosConta(true);

        var getContaPromise = $q.defer();
        getContaPromise.resolve(dados);

        spyOn(agendamento, 'getDadosConta').and.returnValue(getContaPromise.promise);

        ctrl.agendamentoIntegralizacoes = {};
        ctrl.buscarConta(conta);

        $rootScope.$apply();

        expect(ctrl.nomeCorrentista).toEqual("Correntista sem nome cadastrado.");
    });

    it('não deve buscar conta', function() {
        var conta = '37840916953';

        var getContaPromise = $q.defer();
        getContaPromise.reject({
            status: 404
        });

        spyOn(agendamento, 'getDadosConta').and.returnValue(getContaPromise.promise);

        ctrl.buscarConta(conta);
        $rootScope.$apply();

        expect(agendamento.getDadosConta).toHaveBeenCalled();
        expect(ctrl.nomeCorrentista).toEqual("Não encontrado.");
        expect(ctrl.agendamentoIntegralizacoes.contaDebito).toBeNull();
    });

    it('não deve buscar conta', function() {
        var conta = '';
        var dados = {
            data: {}
        };

        var getContaPromise = $q.defer();
        getContaPromise.resolve(dados);

        spyOn(agendamento, 'getDadosConta').and.returnValue(getContaPromise.promise);

        ctrl.buscarConta(conta);

        $rootScope.$apply();

        expect(ctrl.agendamentoIntegralizacoes.matricula).toBeUndefined();
    });

    it('deve mostrar Dia fixo nos campos', function() {
        var dataTipoAgendamento = "F";

        ctrl.agendamentoIntegralizacoes = {
            tipoAgendamento: dataTipoAgendamento,
            anoMesPrimeiroVencimento: '08-2017'
        }

        ctrl.setTypeVenc();

        expect(ctrl.diaPrimeiroVenc).toBe(true);
        expect(ctrl.agendamentoIntegralizacoes.anoMesPrimeiroVencimento).toBeUndefined();
    });

    it('deve mostrar Dia fixo nos campos', function() {
        var dataTipoAgendamento = "F";

        ctrl.agendamentoIntegralizacoes = {
            tipoAgendamento: dataTipoAgendamento
        }

        ctrl.setTypeVenc();

        expect(ctrl.diaPrimeiroVenc).toBe(true);
        expect(ctrl.agendamentoIntegralizacoes.anoMesPrimeiroVencimento).toBeUndefined();
    });

    it('deve mostrar Mes ano nos campos', function() {
        var dataTipoAgendamento = "U";

        ctrl.agendamentoIntegralizacoes = {
            tipoAgendamento: dataTipoAgendamento,
            diaPrimeiroVencimento: '08'
        }

        ctrl.setTypeVenc();

        expect(ctrl.mesAnoVenc).toBe(true);
        expect(ctrl.agendamentoIntegralizacoes.anoMesPrimeiroVencimento).toBeUndefined();
    });

    it('deve mostrar Dia fixo nos campos', function() {
        var dataTipoAgendamento = "U";

        ctrl.agendamentoIntegralizacoes = {
            tipoAgendamento: dataTipoAgendamento
        }

        ctrl.setTypeVenc();

        expect(ctrl.mesAnoVenc).toBe(true);
        expect(ctrl.agendamentoIntegralizacoes.diaPrimeiroVencimento).toBeUndefined();
    });

    it('não deve conter tipoAgendamento', function() {
        ctrl.agendamentoIntegralizacoes = {}

        ctrl.setTypeVenc();

        expect(ctrl.diaPrimeiroVenc).toBe(true);
        expect(ctrl.mesAnoVenc).toBe(false);
        expect(ctrl.agendamentoIntegralizacoes.diaPrimeiroVencimento).toEqual('');
    });

    it('não deve conter tipoAgendamento', function() {
        ctrl.agendamentoIntegralizacoes = {}

        ctrl.setTypeVenc();

        expect(ctrl.diaPrimeiroVenc).toBe(true);
        expect(ctrl.mesAnoVenc).toBe(false);
        expect(ctrl.agendamentoIntegralizacoes.diaPrimeiroVencimento).toEqual('');
    });

    it('deve ser vazio tipoAgendamento', function() {
        ctrl.agendamentoIntegralizacoes = {
            tipoAgendamento: ''
        }

        ctrl.setTypeVenc();

        expect(ctrl.diaPrimeiroVenc).toBe(false);
        expect(ctrl.mesAnoVenc).toBe(false);
    });

    it('deve mostrar Dia fixo nos campos', function() {
        var dataTipoAgendamento = JSON.stringify({
            "valor": "F",
            "descricao": "Dia fixo"
        });

        ctrl.agendamentoIntegralizacoes = {
            tipoAgendamento: dataTipoAgendamento,
            anoMesPrimeiroVencimento: ''
        }

        ctrl.setTypeVenc();

        expect(ctrl.diaPrimeiroVenc).toBe(false);
        expect(ctrl.agendamentoIntegralizacoes.anoMesPrimeiroVencimento).toEqual('');
    });

    it('deve mostrar Último dia útil nos campos', function() {
        var dataTipoAgendamento = JSON.stringify({
            "valor": "U",
            "descricao": "Último dia útil"
        });

        ctrl.agendamentoIntegralizacoes = {
            tipoAgendamento: dataTipoAgendamento,
            diaPrimeiroVencimento: ''
        }

        ctrl.setTypeVenc();

        expect(ctrl.mesAnoVenc).toBe(false);
        expect(ctrl.agendamentoIntegralizacoes.diaPrimeiroVencimento).toEqual('');
    });

    it('não deve conter diaPrimeiroVencimento', function() {
        var dataTipoAgendamento = JSON.stringify({
            "valor": "U",
            "descricao": "Último dia útil"
        });

        ctrl.agendamentoIntegralizacoes = {
            tipoAgendamento: dataTipoAgendamento
        }

        ctrl.setTypeVenc();

        expect(ctrl.agendamentoIntegralizacoes.diaPrimeiroVencimento).toBeUndefined();
    });

    it('deve conter formaRecebimento', function() {
        var agendamento = JSON.stringify({
            "valor": "S",
            "descricao": "Débito em Conta"
        });

        ctrl.agendamentoIntegralizacoes = {
            formaRecebimento: agendamento
        };

        ctrl.setVerificaSaldo();

        var validate = false;

        expect(ctrl.showVerificaSaldo).toBe(validate);
        expect(ctrl.agendamentoIntegralizacoes.verificaSaldo).toBe(validate);
    });

    it('não deve conter formaRecebimento', function() {
        var agendamento = JSON.stringify({
            "valor": "N",
            "descricao": "Débito em Conta"
        });

        ctrl.agendamentoIntegralizacoes = {};

        ctrl.setVerificaSaldo();

        var validate = false;

        expect(ctrl.showVerificaSaldo).toBe(validate);
        expect(ctrl.agendamentoIntegralizacoes.verificaSaldo).toBe(validate);
    });

    it('deve calcular valor', function() {
        ctrl.agendamentoIntegralizacoes = {
            capitalAIntegralizar: 300,
            valorEntrada: 100,
            quantidadeParcelas: 2
        }

        ctrl.calcularValores();

        expect(ctrl.agendamentoIntegralizacoes.valorParcela).toEqual(100);
    });

    it('deve calcular quantidade de parcelas', function() {
        ctrl.agendamentoIntegralizacoes = {
            capitalAIntegralizar: 10100,
            valorEntrada: 100,
            quantidadeParcelas: 2,
            valorParcela: 500
        }

        ctrl.calcularQtdParcelas();

        expect(ctrl.agendamentoIntegralizacoes.quantidadeParcelas).toEqual(20);
    });

    it('deve verificar se valor da parcela não é maior do que o total', function() {
        ctrl.agendamentoIntegralizacoes = {
            capitalAIntegralizar: 100,
            valorEntrada: 50,
            quantidadeParcelas: 2,
            valorParcela: 5000
        }

        ctrl.calcularQtdParcelas();

        expect(ctrl.agendamentoIntegralizacoes.quantidadeParcelas).toEqual(2);
    });

    it('deve checar o valor de entrada e deve ser maior que capital', function() {
        ctrl.agendamentoIntegralizacoes = {
            valorEntrada: 200,
            capitalAIntegralizar: 100
        }

        spyOn($rootScope, '$emit').and.callThrough();

        ctrl.checkValorEntrada();

        $rootScope.$apply();

        expect($rootScope.$emit).toHaveBeenCalledWith('mostrar-mensagem', {
            titulo: 'Agendamento Integralizações',
            descricao: 'O valor de entrada deve ser menor que o de capitalização'
        });

        expect(ctrl.agendamentoIntegralizacoes.valorEntrada).toEqual('');
        expect(ctrl.agendamentoIntegralizacoes.quantidadeParcelas).toEqual('');
        expect(ctrl.agendamentoIntegralizacoes.valorParcela).toEqual('');
    });

    it('deve checar o valor de entrada e deve ser menor', function() {
        ctrl.agendamentoIntegralizacoes = {
            valorEntrada: 100,
            capitalAIntegralizar: 200
        }

        ctrl.checkValorEntrada();

        expect(ctrl.agendamentoIntegralizacoes.valorEntrada).toEqual(100);
        expect(ctrl.agendamentoIntegralizacoes.capitalAIntegralizar).toEqual(200);
    });

    it('deve aceitar data de entrada válida', function() {
        var data = moment();
        ctrl.agendamentoIntegralizacoes = {
            dataEntrada: data
        }

        ctrl.validarDataEntrada(ctrl.agendamentoIntegralizacoes.dataEntrada);

        expect(ctrl.agendamentoIntegralizacoes.dataEntrada).not.toBeNull();
        expect(ctrl.agendamentoIntegralizacoes.dataEntrada).toEqual(data);
    });

    it('não deve aceitar data de entrada inválida', function() {
        var data = '99/99/9999';
        ctrl.agendamentoIntegralizacoes = {
            dataEntrada: data
        }

        ctrl.validarDataEntrada(ctrl.agendamentoIntegralizacoes.dataEntrada);

        expect(ctrl.agendamentoIntegralizacoes.dataEntrada).toBeNull();
        expect(ctrl.agendamentoIntegralizacoes.dataEntrada).not.toEqual(data);
    });
});

function _tipoAgendamento() {
    var data = [{
        "valor": "F",
        "descricao": "Dia fixo"
    }, {
        "valor": "U",
        "descricao": "Último dia útil"
    }];

    return JSON.stringify(data);
}

function _formasRecebimento() {
    var data = [{
        "valor": "S",
        "descricao": "Débito em Conta"
    }, {
        "valor": "N",
        "descricao": "Cobrança"
    }, {
        "valor": "M",
        "descricao": "Manual"
    }, {
        "valor": "B",
        "descricao": "Banco (Déb.Autom.)"
    }];

    return JSON.stringify(data);
}

function _dadosConta(nameless) {
    return {
        data: {
            "cooperadoPessoa": {
                "cooperativa": 566,
                "nome": (nameless ? "" : "Clinica39740"),
                "identificador": "02475696000138",
                "tipoPessoa": "PJ",
                "cpfCnpj": "02475696000138",
                "telefone": "5133438000",
                "nrMatricula": 115975,
                "nomeConjuge": null,
                "cpfConjuge": null,
                "situacao": "ATIVO"
            },
            "contaCorrente": {
                "compePropria": true,
                "numero": 70165,
                "tipo": 1,
                "modalidade": "INDIVIDUAL",
                "centralizaSaldo": false,
                "gerenteConta": "GERENTE",
                "clienteDesde": null,
                "assinatura": null,
                "cpmf": null,
                "nomeIdentificador": null,
                "situacao": "LIVRE",
                "posto": 5,
                "empostamento": 1,
                "tipoEnderecoEmpostamento": "COMERCIAL",
                "cpfCnpjPrimeiroTitular": "02475696000138",
                "valorLimiteCredito": 10000,
                "numeroContaOriginal": 70165,
                "contaEspelho": null,
                "domicilioBancario": null,
                "talao": null,
                "indicadoresEmprestimo": null,
                "aplicacaoFinanceira": null,
                "tarifas": {
                    "isento": false,
                    "diaCobranca": null,
                    "codigoPacote": null
                }
            }
        }
    }
}
