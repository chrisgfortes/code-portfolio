describe('Service: formatadorEndereco', function () {

    var formatadorEndereco;

    beforeEach(module('app'));

    beforeEach(inject(function (_formatadorEndereco_) {
        formatadorEndereco = _formatadorEndereco_;
    }));

    it('deve notificar alteração de endereço', function () {

        var endereco = { logradouro: 'Rua X', numero: '1', cep: '9875645361', enderecoSemNumero: true };

        var enderecoFormatado = formatadorEndereco.formatar(endereco);

        var enderecoEsperado = 'Rua X, 1, CEP - 9875645361';

        expect(enderecoFormatado).toBe(enderecoEsperado)
    });

    it('deve notificar alteração de endereço, sem numero', function () {

        var endereco = { logradouro: 'Rua X', numero: '1', cep: '9875645361', enderecoSemNumero: false };

        var enderecoFormatado = formatadorEndereco.formatar(endereco);

        var enderecoEsperado = 'Rua X, CEP - 9875645361';

        expect(enderecoFormatado).toBe(enderecoEsperado)
    });

    it('deve notificar erro ao passar endereço indefinido', function () {
        expect(function () {
           formatadorEndereco.formatar()
        }).toThrow('Endereço não informado.')
    });
});