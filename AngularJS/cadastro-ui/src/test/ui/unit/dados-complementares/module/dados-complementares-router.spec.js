describe('Router: dadosComplementares-router', function () {

    var dadosComplementares,
        $route,
        $injector,
        HOST,
        $q,
        rotaService,
        linksFactory;

    beforeEach(module('app'));

    beforeEach(inject(function (_$route_, _dadosComplementares_, _$injector_, $httpBackend, _HOST_, _linksFactory_, _$q_, _rotaService_) {
        $route = _$route_;
        dadosComplementares = _dadosComplementares_;
        $injector = _$injector_;
        HOST = _HOST_;
        linksFactory = _linksFactory_;
        $q = _$q_;
        rotaService = _rotaService_;
    }));

    describe('Rota de Terceiro', function () {
        testarRota('terceiro');
    });

    function testarRota(nomeDaRota) {
        it('Deve testar dadosComplementares-router', function() {
            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/dados-complementares'];

            expect(rota.controller).toBe('dadosComplementaresController');
            expect(rota.templateUrl).toBe('./modules/dados-complementares/dados-complementares.html');
        });

        it('Deve chamar dadosComplementares.buscar()', inject(function($httpBackend) {
            $httpBackend.whenGET(/\.html$/).respond('');

            var cpf = '123456789123';
            var retornoEsperado = { procuradores: [],
                                    referencias: [],
                                    planosSaude: [],
                                    seguros: [],
                                    previdencias: [],
                                    participacoesSocietarias: [] };

            $httpBackend
                .expectGET(HOST[nomeDaRota] + cpf + '/rascunho/dadoscomplementares')
                .respond(200, {});

            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/dados-complementares'];

            $route = {current: { params: { cpf: cpf } } };

            var retorno = rota.resolve.dadosComplementaresInfo(dadosComplementares, $route);
            $httpBackend.flush();
            expect(retorno.$$state.value).toEqual(retornoEsperado);
        }));

        it('não encontra dados complementares', inject(function($httpBackend) {
            $httpBackend.whenGET(/\.html$/).respond('');

            var cpf = '123456789123';

            $httpBackend
                .expectGET(HOST[nomeDaRota] + cpf + '/rascunho/dadoscomplementares')
                .respond(404, {});

            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/dados-complementares'];

            $route = {current: { params: { cpf: cpf } } };

            var retorno = rota.resolve.dadosComplementaresInfo(dadosComplementares, $route);
            $httpBackend.flush();
            expect(retorno.$$state.value).toEqual({});
        }));

        it('Deve carregar urls', function () {
            var cpf = '123456789123';
            var compare = mapearRetorno(linksFactory(nomeDaRota), cpf);
            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/dados-complementares'];

            var retorno = rota.resolve.urls(linksFactory);
            retorno = mapearRetorno(retorno, cpf);

            expect(retorno).toEqual(compare);
        });

        it('Deve definir a factory', function () {
            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/dados-complementares'];

            var factory = dadosComplementares(nomeDaRota);
            var retorno = rota.resolve.dadosComplementaresFactory(dadosComplementares);

            expect(Object.getOwnPropertyNames(retorno)).toEqual(Object.getOwnPropertyNames(factory));
        });

        it('deve verificar se é terceiro', function() {
            var cpf = '93994468248';

            var deferred = $q.defer();
            deferred.resolve({});
            spyOn(rotaService, 'isTerceiro').and.returnValue(deferred.promise);

            var rota = $route.routes['/cadastro/'+ nomeDaRota +'/:cpf/dados-complementares'];
            $route = {
                current: {
                    params: {
                        cpf: cpf
                    }
                }
            }

            rota.resolve.isTerceiro($route, rotaService);
            expect(rotaService.isTerceiro).toHaveBeenCalledWith(cpf);
        });
    }

    function mapearRetorno(urls, cpf) {
        urls = urls.map(function (item) {
            item.url = item.url(cpf);
            return item
        })
        return urls;
    }
});
