describe('factory: dadosComplementares', function () {

    var dadosComplementares,
        moment,
        HOST;

    beforeEach(module('app'));

    beforeEach(inject(function (_dadosComplementares_, _moment_, _HOST_) {
        dadosComplementares = _dadosComplementares_('terceiro');
        moment = _moment_;
        HOST = _HOST_;
    }));

    it('.buscar(cpf)', inject(function($httpBackend) {
        $httpBackend.whenGET(/\.html$/).respond('');

        var cpf = '83084412545';
        var dadosComplementaresResponse = dadoscomplementaresPost(moment);

        $httpBackend
            .expectGET(HOST.terceiro + cpf + '/rascunho/dadoscomplementares')
            .respond(200, dadosComplementaresResponse);

        dadosComplementares
            .buscar(cpf)
            .then(function (response) {
                var dados = response;
                expect(dados).toEqual(dadosComplementaresResponse);
            });

        $httpBackend.flush();
    }));

    it('.salvar(cpf, dadosComplementares)', inject(function($httpBackend) {
        $httpBackend.whenGET(/\.html$/).respond('');

        var cpf = '83084412545';
        var dadosComplementaresResponse = dadoscomplementaresPost(moment);
        var dadosFake = dadoscomplementaresFake();

        $httpBackend
            .expectPUT(HOST.terceiro + cpf + '/rascunho/dadoscomplementares')
            .respond(201, dadosFake);

        dadosComplementares
            .salvar(cpf, dadosComplementaresResponse)
            .then(function (response) {
                var dados = response.data;
                expect(dados).toEqual(dadosFake);
            });

        $httpBackend.flush();
    }));


    function dadoscomplementaresFake() {
        return {
            "procuradores": [{
                "cpf": "00360741010",
                "nomeCompleto": "dariano soares",
                "numeroIdentificacao": "123",
                "orgaoExpedidor": "SSP",
                "tipoIdentificacao": "CARTEIRA_IDENTIDADE",
                "ufExpedidor": "AC",
                "dataEmissao": "2000-03-01",
                "dataNascimento": "1980-03-01",
                "cargo": "programador",
                "procuracao": {
                    "tipoProcuracao": "PARTICULAR",
                    "vigencia": "2017-03-31",
                    "dataProcuracao": "2017-04-19",
                    "livro": "livro",
                    "folha": "folha",
                    "dataEntrada": "2017-03-20",
                    "tabelionato": "tabelionato teste",
                    "prazo": "DETERMINADO",
                    "observacoes": "observacoes teste",
                    "revogado": false
                }
            }],
            "planosSaude": [{
                "tipoPlanoDeSaude": "MEDICO",
                "instituicao": "INSTITUICAO1",
                "valorPlanoDeSaude": 54.65,
                "cobertura": "COMPLETA",
                "dataVencimento": "2017-03-20",
                "modoEdicao": false
            }],
            "seguros": [{
                "tipoSeguro": "SEGURO_SAUDE",
                "seguradora": "PORTO_SEGURO",
                "dataVencimento": "2017-03-01",
                "valorSegurado": 234.23
            }],
            "referencias": [{
                "nomeAgenciaLoja": "Agencia teste",
                "nomeBancoEmpresa": "Branco teste",
                "telefone": {
                    "ddd": "51",
                    "numero": "99999999",
                    "observacao": "observacao",
                    "tipoTelefone": "RESIDENCIAL"
                }
            }],
            "previdencias": [{
                "dataInicioContribuicao": "2017-04-03",
                "instituicao": "QUANTA",
                "numeroDependentesSemPlano": 0,
                "numeroProposta": 0,
                "possuiDependente": true,
                "tipoPlano": "PGBL",
                "valorContribuicao": 432.53,
                "valorMontante": 22.43
            }],
            "participacoesSocietarias": [{}]
        }
    }

    function dadoscomplementaresPost(moment) {
        return {
            "procuradores": [{
                "cpf": "00360741010",
                "nomeCompleto": "dariano soares",
                "numeroIdentificacao": "123",
                "orgaoExpedidor": "SSP",
                "tipoIdentificacao": "CARTEIRA_IDENTIDADE",
                "ufExpedidor": "AC",
                "dataEmissao": moment("2000-03-01"),
                "dataNascimento": moment("1980-03-01"),
                "cargo": "programador",
                "procuracao": {
                    "tipoProcuracao": "PARTICULAR",
                    "vigencia": moment("2017-03-31"),
                    "dataProcuracao": moment("2017-04-19"),
                    "livro": "livro",
                    "folha": "folha",
                    "dataEntrada": moment("2017-03-20"),
                    "tabelionato": "tabelionato teste",
                    "prazo": "DETERMINADO",
                    "observacoes": "observacoes teste",
                    "revogado": false
                }
            }],
            "planosSaude": [{
                "tipoPlanoDeSaude": "MEDICO",
                "instituicao": "INSTITUICAO1",
                "valorPlanoDeSaude": 54.65,
                "cobertura": "COMPLETA",
                "dataVencimento": moment("2017-03-20"),
                "modoEdicao": false
            }],
            "seguros": [{
                "tipoSeguro": "SEGURO_SAUDE",
                "seguradora": "PORTO_SEGURO",
                "dataVencimento": moment("2017-03-01"),
                "valorSegurado": 234.23
            }],
            "referencias": [{
                "nomeAgenciaLoja": "Agencia teste",
                "nomeBancoEmpresa": "Branco teste",
                "telefone": {
                    "numero": "5199999999",
                    "observacao": "observacao",
                    "tipoTelefone": "RESIDENCIAL"
                }
            }],
            "previdencias": [{
                "dataInicioContribuicao": moment("2017-04-03"),
                "instituicao": "QUANTA",
                "numeroDependentesSemPlano": 0,
                "numeroProposta": 0,
                "possuiDependente": true,
                "tipoPlano": "PGBL",
                "valorContribuicao": 432.53,
                "valorMontante": 22.43,
                "mesesContribuicao": 2
            }],
            "participacoesSocietarias": [{}]
        }
    }
});
