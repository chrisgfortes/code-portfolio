describe('controller: dadosComplementaresController', function () {

    var ctrl,
        $rootScope,
        dadosComplementaresFactory,
        $q,
        $scope,
        urls,
        moment;

    var cpf = '12345678912';
    var erros = [{}];
    var dadosComplementares = {}

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_$controller_, _$rootScope_, _dadosComplementares_, _$q_, _linksFactory_, _moment_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        dadosComplementaresFactory = _dadosComplementares_('terceiro');
        urls = _linksFactory_('terceiro');
        $q = _$q_,
        moment= _moment_;

        ctrl = $controller('dadosComplementaresController', {
            $scope: $scope,
            dadosComplementaresFactory: dadosComplementaresFactory,
            urls: urls,
            $routeParams: { cpf: '12345678912' },
            dadosComplementaresInfo: {}
        });
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('deve chamar .salvar(cpf, dadosComplementares)', function () {
        var deferred = $q.defer();
        deferred.resolve({});

        spyOn(dadosComplementaresFactory, 'salvar')
            .and
            .returnValue(deferred.promise);

        var dadosComplementares = dadosComplementaresMock(cpf);

        ctrl.salvar(dadosComplementares);

        $rootScope.$apply();

        expect(dadosComplementaresFactory.salvar).toHaveBeenCalledWith(cpf, dadosComplementares);
    });

    it('deve mostrar messagem de validação, envida pelo body', function () {
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                body: erros
            }
        });

        spyOn(dadosComplementaresFactory, 'salvar')
            .and
            .returnValue(deferred.promise);

        var dadosComplementares = dadosComplementaresMock(cpf);

        ctrl.salvar(dadosComplementares);

        $rootScope.$apply();

        expect(dadosComplementaresFactory.salvar).toHaveBeenCalledWith(cpf, dadosComplementares);
        expect(ctrl.erros).toEqual(erros);
    });

    it('deve mostrar messagem de validação, envida pelo content', function () {
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                content: erros
            }
        });

        spyOn(dadosComplementaresFactory, 'salvar')
            .and
            .returnValue(deferred.promise);

        var dadosComplementares = dadosComplementaresMock(cpf);

        ctrl.salvar(dadosComplementares);

        $rootScope.$apply();

        expect(dadosComplementaresFactory.salvar).toHaveBeenCalledWith(cpf, dadosComplementares);
        expect(ctrl.erros).toEqual(erros);
    });

    it('não deve mostrar messagem de validação', function () {
        var deferred = $q.defer();
        deferred.reject({
            status: 400,
            data: {
                content: erros
            }
        });

        spyOn(dadosComplementaresFactory, 'salvar')
            .and
            .returnValue(deferred.promise);

        var dadosComplementares = dadosComplementaresMock(cpf);

        ctrl.salvar(dadosComplementares);

        $rootScope.$apply();

        expect(dadosComplementaresFactory.salvar).toHaveBeenCalledWith(cpf, dadosComplementares);
        expect(ctrl.erros).not.toBeDefined();
    });

    function dadosComplementaresMock(cpf) {
        return {
            "procuradores": [{
                "cpf": "00360741010",
                "nomeCompleto": "dariano soares",
                "numeroIdentificacao": "123",
                "orgaoExpedidor": "SSP",
                "tipoIdentificacao": "CARTEIRA_IDENTIDADE",
                "ufExpedidor": "AC",
                "dataEmissao": moment("2000-03-01"),
                "dataNascimento": moment("1980-03-01"),
                "cargo": "programador",
                "procuracao": {
                    "tipoProcuracao": "PARTICULAR",
                    "vigencia": moment("2017-03-31"),
                    "dataProcuracao": moment("2017-04-19"),
                    "livro": "livro",
                    "folha": "folha",
                    "dataEntrada": moment("2017-03-20"),
                    "tabelionato": "tabelionato teste",
                    "prazo": "DETERMINADO",
                    "observacoes": "observacoes teste",
                    "revogado": false
                }
            }],
            "planosSaude": [{
                "tipoPlanoDeSaude": "MEDICO",
                "instituicao": "INSTITUICAO1",
                "valorPlanoDeSaude": 54.65,
                "cobertura": "COMPLETA",
                "dataVencimento": moment("2017-03-20"),
                "modoEdicao": false
            }],
            "seguros": [{
                "tipoSeguro": "SEGURO_SAUDE",
                "seguradora": "PORTO_SEGURO",
                "dataVencimento": moment("2017-03-01"),
                "valorSegurado": 234.23
            }],
            "referencias": [{
                "nomeAgenciaLoja": "Agencia teste",
                "nomeBancoEmpresa": "Branco teste",
                "telefone": {
                    "numero": "5199999999",
                    "observacao": "observacao",
                    "tipoTelefone": "RESIDENCIAL"
                }
            }],
            "previdencias": [{
                "dataInicioContribuicao": moment("2017-04-03"),
                "instituicao": "QUANTA",
                "numeroDependentesSemPlano": 0,
                "numeroProposta": 0,
                "possuiDependente": true,
                "tipoPlano": "PGBL",
                "valorContribuicao": 432.53,
                "valorMontante": 22.43,
                "mesesContribuicao": 2
            }],
            "participacoesSocietarias": [{}]
        }
    }
});
