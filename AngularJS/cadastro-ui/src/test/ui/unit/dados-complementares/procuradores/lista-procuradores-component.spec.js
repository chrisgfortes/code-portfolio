describe('Component: listaProcuradores', function () {

    var $componentController,
        $rootScope,
        ctrl;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _$rootScope_) {
        $componentController = _$componentController_;
        $rootScope = _$rootScope_;

        var bindings = {
            procuradores: [],
            readonly: true
        };

        ctrl = $componentController('listaProcuradores', {
            $rootScope: _$rootScope_
        }, bindings);

        ctrl.$onInit();
    }));

    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir os procuradores no bindings', function () {
        expect(ctrl.procuradores).toBeDefined();
        expect(ctrl.procuradores.length).toBe(0);
    });

    it('deve definir os procuradores', function () {
        ctrl.procuradores = undefined;

        ctrl.$onInit();

        expect(ctrl.procuradores).toBeDefined();
        expect(ctrl.procuradores.length).toBe(0);
    });

    it('deve chamar o evento onNovo', function () {
        var onNovoSpy = jasmine.createSpy('onNovo');

        var bindings = {
            onNovo: onNovoSpy
        };

        var ctrl = $componentController('listaProcuradores', null, bindings);

        ctrl.novo();

        expect(onNovoSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onRemover', function () {
        var onRemoverSpy = jasmine.createSpy('onRemover');

        var bindings = {
            onRemover: onRemoverSpy
        };

        var ctrl = $componentController('listaProcuradores', null, bindings);

        var procurador = {
            modoExclusao: false
        };

        var esperado = {
            procurador: {
                modoExclusao: true
            }
        };

        ctrl.remover(procurador);

        expect(onRemoverSpy).toHaveBeenCalledWith(esperado);
        expect(procurador.modoExclusao).toBe(true);
    });

    it('deve chamar o evento onEditar', function () {
        var onEditarSpy = jasmine.createSpy('onEditar');

        var bindings = {
            onEditar: onEditarSpy
        };

        var ctrl = $componentController('listaProcuradores', null, bindings);

        ctrl.procuradores = [{ modoEdicao: false }];

        var procurador = {
            modoEdicao: false
        };

        var esperado = {
            procurador: {
                modoEdicao: true
            }
        };

        ctrl.editar(procurador);

        expect(onEditarSpy).toHaveBeenCalledWith(esperado);
        expect(procurador.modoEdicao).toBe(true);
    });
});
