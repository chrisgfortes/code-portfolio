describe('Service: procuracao.factory', function () {

    var procuracao,
        httpBackend,
        HOST;

    beforeEach(module('app'));

    beforeEach(inject(function ($httpBackend, _procuracao_, _HOST_) {
        procuracao = _procuracao_;
        HOST = _HOST_;
        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));

    afterEach(function () {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.tipos()', function () {
        var cpf = '83084412545';
        var procuracaoResponse = {
            data: [
                {
                    descricao: "Particular",
                    valor: "PARTICULAR"
                }
            ]
        };

        httpBackend
            .expectGET(HOST.pessoa + '/fisica/procurador/procuracao/tipos')
            .respond(200, procuracaoResponse);

        procuracao
            .tipos()
            .then(function (response) {
                var dados = response.data;
                expect(dados).toEqual(procuracaoResponse);
            });

        httpBackend.flush();
    });
});