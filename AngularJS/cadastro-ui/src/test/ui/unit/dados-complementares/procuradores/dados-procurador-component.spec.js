describe('Component: dadosProcurador', function () {

    var $componentController,
        moment,
        $rootScope,
        procuracao,
        ctrl;

    var tiposProcuracao = [{}];

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _moment_, _$rootScope_, _procuracao_) {
        $componentController = _$componentController_;
        moment = _moment_;
        $rootScope = _$rootScope_;
        procuracao = _procuracao_;

        var bindings = {
            procurador: {}
        };

        spyOn(procuracao, 'tipos').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: tiposProcuracao }); }
            };
        });

        ctrl = $componentController('dadosProcurador', {
            moment: _moment_,
            $rootScope: _$rootScope_,
            procuracao: procuracao
        }, bindings);

        ctrl.$onInit();
    }));

    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o procurador no bindings', function () {
        expect(ctrl.procurador).toBeDefined();
    });

    it('deve ser hoje a DATA minima da VIGENCIA', function () {
        expect(ctrl.vigenciaMinima.format('DD/MM/YYYY')).toBe(moment().format('DD/MM/YYYY'));
    });

    it('deve chamar procuracao.tipos()', function () {
        expect(procuracao.tipos).toHaveBeenCalled();
        expect(ctrl.tiposProcuracao).toEqual(tiposProcuracao);
    });

    it('VIGENCIA não deve ser menor que hoje', function () {
        var ontem = moment().add(-1, 'day');
        var isMaior = ontem.isAfter(ctrl.vigenciaMinima);

        expect(isMaior).toBe(false);
    });

    it('DATA da PROCURAÇÃO NÃO deve ser futura', function () {
        var amanha = moment().add(1, 'day');
        var naoEhFutura = ctrl.dataProcuracaoMaxima.isBefore(amanha)

        expect(naoEhFutura).toBe(true);
    });

    it('DATA da PROCURAÇÃO deve ser IGUAL ou MENOR que hoje', function () {
        var ontem = moment().add(-1, 'day');
        var hoje = moment();
        var isMaior = ctrl.dataProcuracaoMaxima.isAfter(ontem);
        var isHoje = ctrl.dataProcuracaoMaxima.isSame(hoje, 'day');

        expect(isMaior).toBe(true);
        expect(isHoje).toBe(true);
    });

    it('deve chamar o evento onSalvar', function () {
        var onSalvarSpy = jasmine.createSpy('onSalvar');

        var bindings = {
            onSalvar: onSalvarSpy
        };

        var ctrl = $componentController('dadosProcurador', null, bindings);

        var procurador = { cpf: '12123123' };
        ctrl.salvar(procurador);

        expect(onSalvarSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onCancelar', function () {
        var onCancelarSpy = jasmine.createSpy('onCancelar');

        var bindings = {
            procurador: { cpf: '12123123' },
            onCancelar: onCancelarSpy
        };

        var ctrl = $componentController('dadosProcurador', null, bindings);

        ctrl.cancelar();

        expect(onCancelarSpy).toHaveBeenCalled();
    });

    it('deve aceitar DATA válida', function () {
        ctrl.procurador = {
            procuracao: {
                vigencia: moment('2000-01-01'),
                dataProcuracao: moment('1999-01-01')
            }
        };

        ctrl.validarDataVigencia(ctrl.procurador.procuracao.vigencia);
        ctrl.validarDataProcuracao(ctrl.procurador.procuracao.dataProcuracao);

        expect(ctrl.procurador.procuracao.vigencia).not.toEqual({});
        expect(ctrl.procurador.procuracao.dataProcuracao).not.toEqual({});
    });

    it('não deve aceitar DATA inválida', function () {
        ctrl.procurador = {
            procuracao: {
                vigencia: moment('2000-23-56'),
                dataProcuracao: moment('1999-23-56')
            }
        };
        ctrl.validarDataVigencia(ctrl.procurador.procuracao.vigencia);
        ctrl.validarDataProcuracao(ctrl.procurador.procuracao.dataProcuracao);

        expect(ctrl.procurador.procuracao.vigencia).toEqual({});
        expect(ctrl.procurador.procuracao.dataProcuracao).toEqual({});
    });
});
