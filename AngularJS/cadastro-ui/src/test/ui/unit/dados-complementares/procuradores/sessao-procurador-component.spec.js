describe('Component: sessaoProcurador', function () {

    var $componentController,
        dialogs,
        $q,
        $rootScope,
        $routeParams,
        ctrl;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _dialogs_, _$q_, _$rootScope_, _$routeParams_) {
        $componentController = _$componentController_;
        dialogs = _dialogs_;
        $q = _$q_;
        $rootScope = _$rootScope_;
        $routeParams = _$routeParams_;

        var bindings = {
            procuradores: []
        };

        _$routeParams_.cpf = '24187541209';

        ctrl = $componentController('sessaoProcurador',
            {
                dialogs: _dialogs_,
                $routeParams: _$routeParams_
            }, bindings);
    }));

    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o procuradores no bindings', function () {
        expect(ctrl.procuradores).toBeDefined();
    });

    it('deve SALVAR uma PROCURADOR', function () {
        var procurador = {
            cpf: '123456789',
            nomeCompleto: 'nome completo',
            modoEdicao: false
        };
        ctrl.mostrarProcuradorForm = true;

        ctrl.salvar(procurador);

        expect(ctrl.procuradores.length).toBe(1);
        expect(ctrl.mostrarProcuradorForm).toBe(false);
    });

    it('não deve SALVAR uma PROCURADOR com o mesmo CPF de outro PROCURADOR', function () {
        var procurador = {
            cpf: '123456789',
            nomeCompleto: 'nome completo',
            modoEdicao: false
        };
        var procuradorSecundario = {
            cpf: '123456789',
            nomeCompleto: 'nome completo dois',
            modoEdicao: false
        };
        ctrl.mostrarProcuradorForm = true;
        ctrl.salvar(procurador);
        expect(ctrl.mostrarProcuradorForm).toBe(false);

        ctrl.mostrarProcuradorForm = true;
        ctrl.salvar(procuradorSecundario);
        expect(ctrl.procuradores.length).toBe(1);
        expect(ctrl.mostrarProcuradorForm).toBe(true);
    });

    it('deve EDITAR uma PROCURADOR', function () {
        var procurador = {
            cpf: '46237885552',
            nomeCompleto: 'nome completo',
            modoEdicao: true
        };

        var procuradorEditada = {
            cpf: '46237885552',
            nomeCompleto: 'nome completo editado',
            modoEdicao: true
        };

        var coadijuvante = {
            cpf: '07458207000',
            nomeCompleto: 'coadijuvante editado',
            modoEdicao: false
        };

        ctrl.procuradores.push(procurador);
        ctrl.procuradores.push(coadijuvante);

        ctrl.mostrarProcuradorForm = true;

        ctrl.salvar(procuradorEditada);

        expect(ctrl.procuradores[0].nomeCompleto).toBe('nome completo editado');
        expect(ctrl.procuradores[0].modoEdicao).toBe(false);
        expect(ctrl.procuradores[1]).toEqual(coadijuvante);
        expect(ctrl.procuradores.length).toBe(2);
        expect(ctrl.mostrarProcuradorForm).toBe(false);
    });

    it('deve CANCELAR nova/edição PROCURADOR', function () {
        var procurador = {
            cpf: '123456789',
            nomeCompleto: 'nome completo',
            modoEdicao: true
        };

        ctrl.mostrarProcuradorForm = true;

        ctrl.procuradores.push(procurador);

        ctrl.cancelar();

        expect(ctrl.procuradores.length).toBe(1);
        expect(ctrl.procuradores[0].modoEdicao).toBe(false);
        expect(ctrl.mostrarProcuradorForm).toBe(false);
    });

    it('deve NOVO formulário', function () {
        ctrl.mostrarProcuradorForm = false;

        ctrl.novo();

        expect(ctrl.mostrarProcuradorForm).toBe(true);
    });

    it('deve ficar em MODO EDIÇÃO o PROCURADOR', function () {
        ctrl.mostrarProcuradorForm = false;
        ctrl.referencia = null;

        var procurador = {
            cpf: '123456789',
            nomeCompleto: 'nome completo',
            modoEdicao: true
        };

        ctrl.editar(procurador);

        expect(ctrl.mostrarProcuradorForm).toBe(true);
        expect(ctrl.procurador).toEqual(procurador);
    });

    it('deve REMOVER a PROCURADOR', function () {
        var deferred = $q.defer();
        deferred.resolve('yes');
        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var procurador = {
            cpf: '123456789',
            nomeCompleto: 'nome completo',
            modoEdicao: false,
            modoExclusao: false
        };

        var coadijuvante = {
            cpf: '123456789',
            nomeCompleto: 'nome completo',
            modoExclusao: true
        };

        ctrl.procuradores.push(procurador);
        ctrl.procuradores.push(coadijuvante);

        ctrl.remover(coadijuvante);

        $rootScope.$apply();

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.procuradores.length).not.toBe(2);
        expect(ctrl.procuradores.length).toBe(1);
        expect(ctrl.procuradores[0]).toEqual(procurador);
    });

    it('deve cancelar a REMOVOÇÃO de um procurador', function () {
        var deferred = $q.defer();
        deferred.reject();
        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var procurador = {
            cpf: '123456789',
            nomeCompleto: 'nome completo',
            modoEdicao: false,
            modoExclusao: false
        };

        var coadijuvante = {
            cpf: '123456789',
            nomeCompleto: 'nome completo',
            modoExclusao: true
        };

        ctrl.procuradores.push(procurador);
        ctrl.procuradores.push(coadijuvante);

        ctrl.remover(coadijuvante);

        $rootScope.$apply();

        var todosDesabilitados = ctrl.procuradores
            .some(function (procurador) {
                return !procurador.modoExclusao;
            });

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.procuradores.length).toBe(2);
        expect(todosDesabilitados).toBe(true);
    });

    it('deve valdar CPF', function () {
        spyOn(dialogs, 'notify').and.callFake(function () {
            return {
                result: { then: function (callback) { return callback({  }); }}
            };
        });

        var procurador = {
            cpf: '07458207000',
            nomeCompleto: 'nome completo',
            modoEdicao: true
        };

        var procuradorEditada = {
            cpf: '46237885552',
            nomeCompleto: 'nome completo editado',
            modoEdicao: true
        };

        var coadijuvante = {
            cpf: '46237885552',
            nomeCompleto: 'coadijuvante editado',
            modoEdicao: false
        };

        ctrl.procuradores.push(procurador);
        ctrl.procuradores.push(coadijuvante);

        ctrl.salvar(procuradorEditada);

        expect(dialogs.notify).toHaveBeenCalledWith('Atenção', 'O CPF já foi cadastrado');
    });
});
