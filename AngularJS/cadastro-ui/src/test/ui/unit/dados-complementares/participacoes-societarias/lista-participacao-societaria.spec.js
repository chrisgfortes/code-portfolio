describe('Component: listaParticipacaoSocietaria', function () {

    var $componentController,
        ctrl;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_) {
        $componentController = _$componentController_;

        var bindings = {
            participacoesSocietarias: []
        };

        ctrl = $componentController('listaParticipacaoSocietaria',
            null,
            bindings
        );

        ctrl.$onInit();
    }));

    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o participacoesSocietarias no bindings', function () {
        expect(ctrl.participacoesSocietarias).toBeDefined();
    });

    it('deve chamar o evento onEditar', function () {
        var onEditarSpy = jasmine.createSpy('onEditar');

        var bindings = {
            onEditar: onEditarSpy
        };

        var index = 0;

        var ctrl = $componentController('listaParticipacaoSocietaria', null, bindings);

        ctrl.participacoesSocietarias = [{ modoEdicao : false }];

        ctrl.editar(index);

        var participacao = ctrl.participacoesSocietarias[0];

        expect(onEditarSpy).toHaveBeenCalled();
        expect(participacao.modoEdicao).toBe(true);
    });

    it('deve chamar o evento onNovo', function () {
        var onNovoSpy = jasmine.createSpy('onNovo');

        var bindings = {
            onNovo: onNovoSpy
        };

        var ctrl = $componentController('listaParticipacaoSocietaria', null, bindings);

        ctrl.novo();

        expect(onNovoSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onRemover', function () {
        var onRemoverSpy = jasmine.createSpy('onRemover');

        var bindings = {
            onRemover: onRemoverSpy
        };

        var ctrl = $componentController('listaParticipacaoSocietaria', null, bindings);

        var esperado = {
            participacaoSocietaria: {
                modoExclusao: true
            }
        };

        ctrl.participacoesSocietarias = [{ modoExclusao : false }];
        var participacao = ctrl.participacoesSocietarias[0];

        ctrl.remover(participacao);

        expect(onRemoverSpy).toHaveBeenCalledWith(esperado);
    });
});