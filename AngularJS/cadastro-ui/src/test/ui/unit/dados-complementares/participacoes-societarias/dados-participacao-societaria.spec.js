describe('Component: dadosParticipacaoSocietaria', function () {

    var $componentController,
        ctrl,
        dialogs,
        $q;

    var funcoesCargos = [{}];

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _participacaoSocietaria_, _dialogs_, _$q_) {
        $componentController = _$componentController_;
        participacaoSocietaria = _participacaoSocietaria_;
        dialogs = _dialogs_;
        $q = _$q_;

        var bindings = {
            participacaoSocietaria: {}
        };

        spyOn(participacaoSocietaria, 'funcoes').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: funcoesCargos }); }
            };
        });

        ctrl = $componentController('dadosParticipacaoSocietaria',
            {
                participacaoSocietaria: _participacaoSocietaria_,
                dialogs: _dialogs_
            },
            bindings
        );

        ctrl.$onInit();
    }));

    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o participacaoSocietaria no bindings', function () {
        expect(ctrl.participacaoSocietaria).toBeDefined();
    });

    it('deve chamar participacaoSocietaria.funcoes()', function () {
        expect(participacaoSocietaria.funcoes).toHaveBeenCalled();
        expect(ctrl.funcoesCargos).toEqual(funcoesCargos);
    });

    it('deve chamar o evento onSalvar', function () {
        var onSalvarSpy = jasmine.createSpy('onSalvar');

        var bindings = {
            participacaoSocietaria: {},
            onSalvar: onSalvarSpy
        };

        var ctrl = $componentController('dadosParticipacaoSocietaria', null, bindings);
        ctrl.funcoesCargos = [
            {
                valor: "SOCIO",
                descricao: "Sócio",
            }
        ];
        ctrl.participacaoSocietaria = {
            cnpj: "62469768000166",
            nome: "asdasd",
            percentualParticipacao: 65.45,
            funcaoCargo: "SOCIO"
        };

        ctrl.salvar();

        expect(onSalvarSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onCancelar', function () {
        var onCancelarSpy = jasmine.createSpy('onCancelar');

        var bindings = {
            participacaoSocietaria: {},
            onCancelar: onCancelarSpy
        };

        var ctrl = $componentController('dadosParticipacaoSocietaria', null, bindings);

        ctrl.cancelar();

        expect(onCancelarSpy).toHaveBeenCalled();
    });
});
