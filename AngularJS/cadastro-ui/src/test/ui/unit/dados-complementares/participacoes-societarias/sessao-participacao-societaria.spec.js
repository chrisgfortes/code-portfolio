describe('Component: sessaoParticipacaoSocietaria', function () {

    var $componentController,
        dialogs,
        $q,
        $rootScope,
        ctrl;

    var funcoesCargos = [{
        valor: 'MEDICO',
        descricao: 'Médico'
    }];

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _dialogs_, _$q_, _$rootScope_, _participacaoSocietaria_) {
        $componentController = _$componentController_;
        dialogs = _dialogs_;
        $q = _$q_;
        $rootScope = _$rootScope_;
        participacaoSocietaria = _participacaoSocietaria_;

        var bindings = {
            participacoesSocietarias: []
        };

        spyOn(participacaoSocietaria, 'funcoes').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: funcoesCargos }); }
            };
        });

        ctrl = $componentController('sessaoParticipacaoSocietaria',
            {
                dialogs: _dialogs_,
                participacaoSocietaria: _participacaoSocietaria_
            }, bindings);

        ctrl.$onInit();
    }));

    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o participacoesSocietarias no bindings', function () {
        expect(ctrl.participacoesSocietarias).toBeDefined();
    });

    it('deve definir o valor padrão para participacoesSocietarias', function () {
        ctrl.participacoesSocietarias = undefined;

        ctrl.$onInit();

        expect(ctrl.participacoesSocietarias).toBeDefined();
    });

    it('deve chamar participacaoSocietaria.funcoes()', function () {
        ctrl.participacoesSocietarias = [{ funcaoCargo: 'MEDICO' }];

        ctrl.$onInit();

        expect(participacaoSocietaria.funcoes).toHaveBeenCalled();
        expect(ctrl.funcoesCargos).toEqual(funcoesCargos);
    });

    it('deve SALVAR uma participação societaria', function () {
        var participacao = {
            nome: 'teste',
            modoEdicao: false
        };
        ctrl.mostrarParticipacaoSocietariaForm = true;

        ctrl.salvarParticipacaoSocietaria(participacao);

        expect(ctrl.participacoesSocietarias.length).toBe(1);
        expect(ctrl.mostrarParticipacaoSocietariaForm).toBe(false);
    });

    it('deve EDITAR uma participação societaria', function () {
        var participacao = {
            cnpj: '52545657000138',
            nome: 'teste',
            modoEdicao: true
        };

        var participacaoEditada = {
            cnpj: '52545657000138',
            nome: 'teste alterado',
            modoEdicao: true
        };

        var coadijuvante = {
            cnpj: '87055697000186',
            nome: 'coadijuvante',
            modoEdicao: false
        };

        ctrl.participacoesSocietarias.push(participacao);
        ctrl.participacoesSocietarias.push(coadijuvante);

        ctrl.mostrarParticipacaoSocietariaForm = true;

        ctrl.salvarParticipacaoSocietaria(participacaoEditada);

        expect(ctrl.participacoesSocietarias[0].nome).toBe('teste alterado');
        expect(ctrl.participacoesSocietarias[0].modoEdicao).toBe(false);
        expect(ctrl.participacoesSocietarias[1]).toEqual(coadijuvante);
        expect(ctrl.participacoesSocietarias.length).toBe(2);
        expect(ctrl.mostrarParticipacaoSocietariaForm).toBe(false);
    });

    it('deve CANCELAR nova/edição participação societaria', function () {
        var participacao = {
            nome: 'teste',
            modoEdicao: true
        };
        ctrl.mostrarParticipacaoSocietariaForm = true;

        ctrl.participacoesSocietarias.push(participacao);

        ctrl.cancelarParticipacaoSocietaria();

        expect(ctrl.participacoesSocietarias.length).toBe(1);
        expect(ctrl.participacoesSocietarias[0].modoEdicao).toBe(false);
        expect(ctrl.mostrarParticipacaoSocietariaForm).toBe(false);
    });

    it('deve NOVO uma participação societaria', function () {
        ctrl.mostrarParticipacaoSocietariaForm = false;

        ctrl.novoParticipacaoSocietaria();

        expect(ctrl.mostrarParticipacaoSocietariaForm).toBe(true);
    });

    it('deve ficar em MODO EDIÇÃO a participação societaria', function () {
        ctrl.mostrarParticipacaoSocietariaForm = false;
        ctrl.participacaoSocietaria = null;

        var participacao = {
            nome: 'teste',
            modoEdicao: true
        };

        ctrl.editarParticipacaoSocietaria(participacao);

        expect(ctrl.mostrarParticipacaoSocietariaForm).toBe(true);
        expect(ctrl.participacaoSocietaria).toEqual(participacao);
    });

    it('deve REMOVER uma participação societaria', function () {
        var deferred = $q.defer();
        deferred.resolve('yes');
        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var participacao = {
            nome: 'teste',
            modoExclusao: true
        };

        var coadijuvante = {
            nome: 'coadijuvante',
            modoExclusao: false
        };

        ctrl.participacoesSocietarias.push(participacao);
        ctrl.participacoesSocietarias.push(coadijuvante);
        var index = 0;

        ctrl.removerParticipacaoSocietaria(index);

        $rootScope.$apply();

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.participacoesSocietarias.length).toBe(1);
    });

    it('deve cancelar a REMOVOÇÃO da participação societaria', function () {
        var deferred = $q.defer();
        deferred.reject();
        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var participacao = {
            nome: 'teste',
            modoExclusao: true
        };

        var coadijuvante = {
            nome: 'coadijuvante',
            modoExclusao: false
        };

        ctrl.participacoesSocietarias.push(participacao);
        ctrl.participacoesSocietarias.push(coadijuvante);
        var index = 0;

        ctrl.removerParticipacaoSocietaria(index);

        $rootScope.$apply();

        var todosDesabilitados = ctrl.participacoesSocietarias
            .some(function (participacao) {
                return !participacao.modoExclusao;
            });

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.participacoesSocietarias.length).toBe(2);
        expect(todosDesabilitados).toBe(true);
    });

    it('deve VALIDAR um CNPJ', function () {
        spyOn(dialogs, 'notify').and.callFake(function () {
            return {
                result: { then: function (callback) { return callback({  }); }}
            };
        });
        
        var participacao = {
            cnpj: '52545657000138',
            nome: 'teste',
            modoEdicao: true
        };

        var participacaoEditada = {
            cnpj: '52545657000138',
            nome: 'teste alterado',
            modoEdicao: true
        };

        var coadijuvante = {
            cnpj: '52545657000138',
            nome: 'coadijuvante',
            modoEdicao: false
        };

        ctrl.participacoesSocietarias.push(participacao);
        ctrl.participacoesSocietarias.push(coadijuvante);

        ctrl.salvarParticipacaoSocietaria(participacaoEditada);

        expect(dialogs.notify).toHaveBeenCalledWith('Atenção', 'Esse CNPJ já foi inserido.');
    });
});
