describe('factory: participacaoSocietaria', function() {

    var participacaoSocietaria,
        HOST,
        httpBackend;

    beforeEach(module('app'));

    beforeEach(inject(function($httpBackend, _participacaoSocietaria_, _HOST_) {
        participacaoSocietaria = _participacaoSocietaria_;
        HOST = _HOST_;

        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));

    afterEach(function() {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.funcoes()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.pessoa + '/participacao-societaria/funcoes')
            .respond(200, modelsResponse);

        participacaoSocietaria
            .funcoes()
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });
});