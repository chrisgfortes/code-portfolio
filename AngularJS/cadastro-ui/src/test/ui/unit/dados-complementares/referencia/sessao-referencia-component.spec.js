describe('Component: sessaoReferencia', function () {

    var $componentController,
        dialogs,
        $q,
        $rootScope,
        ctrl;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _dialogs_, _$q_, _$rootScope_) {
        $componentController = _$componentController_;
        dialogs = _dialogs_;
        $q = _$q_;
        $rootScope = _$rootScope_;

        var bindings = {
            referencias: []
        };

        ctrl = $componentController('sessaoReferencia',
            {
                dialogs: _dialogs_
            }, bindings);

        ctrl.$onInit();
    }));

    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o referencias no bindings', function () {
        expect(ctrl.referencias).toBeDefined();
    });

    it('deve SALVAR uma REFERENCIA', function () {
        var referencia = {
            nomeBancoEmpresa: 'teste',
            modoEdicao: false
        };
        ctrl.mostrarReferenciaForm = true;

        ctrl.salvar(referencia);

        expect(ctrl.referencias.length).toBe(1);
        expect(ctrl.mostrarReferenciaForm).toBe(false);
    });

    it('deve EDITAR uma REFERENCIA', function () {
        var referencia = {
            nomeBancoEmpresa: 'teste',
            modoEdicao: true
        };

        var referenciaEditada = {
            nomeBancoEmpresa: 'teste alterado',
            modoEdicao: true
        };

        var coadijuvante = {
            nomeBancoEmpresa: 'coadijuvante',
            modoEdicao: false
        };

        ctrl.referencias.push(referencia);
        ctrl.referencias.push(coadijuvante);

        ctrl.mostrarReferenciaForm = true;

        ctrl.salvar(referenciaEditada);

        expect(ctrl.referencias[0].nomeBancoEmpresa).toBe('teste alterado');
        expect(ctrl.referencias[0].modoEdicao).toBe(false);
        expect(ctrl.referencias[1]).toEqual(coadijuvante);
        expect(ctrl.referencias.length).toBe(2);
        expect(ctrl.mostrarReferenciaForm).toBe(false);
    });

    it('deve CANCELAR nova/edição REFERENCIA', function () {
        var referencia = {
            nomeBancoEmpresa: 'teste',
            modoEdicao: true
        };
        ctrl.mostrarReferenciaForm = true;

        ctrl.referencias.push(referencia);

        ctrl.cancelar();

        expect(ctrl.referencias.length).toBe(1);
        expect(ctrl.referencias[0].modoEdicao).toBe(false);
        expect(ctrl.mostrarReferenciaForm).toBe(false);
    });

    it('deve NOVO formulário', function () {
        ctrl.mostrarReferenciaForm = false;

        ctrl.novo();

        expect(ctrl.mostrarReferenciaForm).toBe(true);
    });

    it('deve ficar em MODO EDIÇÃO a participação societaria', function () {
        ctrl.mostrarReferenciaForm = false;
        ctrl.referencia = null;

        var referencia = {
            nomeBancoEmpresa: 'teste',
            modoEdicao: true
        };

        ctrl.editar(referencia);

        expect(ctrl.mostrarReferenciaForm).toBe(true);
        expect(ctrl.referencia).toEqual(referencia);
    });

    it('deve REMOVER uma REFERENCIA', function () {
        var deferred = $q.defer();
        deferred.resolve('yes');
        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var referencia = {
            nomeBancoEmpresa: 'teste',
            modoEdicao: false,
            modoExclusao: false
        };

        var coadijuvante = {
            nomeBancoEmpresa: 'coadijuvante',
            modoExclusao: true
        };

        ctrl.referencias.push(referencia);
        ctrl.referencias.push(coadijuvante);

        ctrl.remover(coadijuvante);

        $rootScope.$apply();

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.referencias.length).toBe(1);
        expect(ctrl.referencias[0]).toEqual(referencia);
    });

    it('deve cancelar a REMOVOÇÃO de uma referência', function () {
        var deferred = $q.defer();
        deferred.reject();
        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var referencia = {
            nomeBancoEmpresa: 'teste',
            modoEdicao: false,
            modoExclusao: false
        };

        var coadijuvante = {
            nomeBancoEmpresa: 'coadijuvante',
            modoExclusao: true
        };

        ctrl.referencias.push(referencia);
        ctrl.referencias.push(coadijuvante);

        ctrl.remover(coadijuvante);

        $rootScope.$apply();

        var todosDesabilitados = ctrl.referencias
            .some(function (referencia) {
                return !referencia.modoExclusao;
            });

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.referencias.length).toBe(2);
        expect(todosDesabilitados).toBe(true);
    });
});
