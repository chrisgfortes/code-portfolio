
describe('Service: referenciaConvert', function () {

    var referenciaConvert;

    beforeEach(module('app'));

    beforeEach(inject(function (_referenciaConvert_) {
        referenciaConvert = _referenciaConvert_;
    }));

    it('deve estar definido', function () {
        expect(referenciaConvert).toBeDefined();
    });

    it('deve converter os NUMEROS de TELEFONES, no GET', function () {
        var referencias = [
            {
                nomeAgenciaLoja: "Agencia teste",
                nomeBancoEmpresa: "Branco teste",
                telefone: {
                    ddd: "51",
                    numero: "99999999",
                    observacao: "observacao",
                    tipoTelefone: "RESIDENCIAL"
                }
            },
            {
                nomeAgenciaLoja: "Agencia teste 2",
                nomeBancoEmpresa: "Branco teste 2",
                telefone: {
                    ddd: "51",
                    numero: "98888888",
                    observacao: "observacao 2",
                    tipoTelefone: "RESIDENCIAL 2"
                }
            }
        ];

        var convertido = referenciaConvert.convertGet(referencias);

        expect(convertido[0].telefone.numero).toEqual("5199999999");
        expect(convertido[1].telefone.numero).toEqual("5198888888");
    });

    it('não deve converter os NUMEROS de TELEFONES, no GET', function () {
        var referencias = [
            {
                nomeAgenciaLoja: "Agencia teste",
                nomeBancoEmpresa: "Branco teste",
                telefone: {
                    ddd: undefined,
                    numero: undefined,
                    observacao: "observacao",
                    tipoTelefone: "RESIDENCIAL"
                }
            },
            {
                nomeAgenciaLoja: "Agencia teste 2",
                nomeBancoEmpresa: "Branco teste 2",
                telefone: {
                    ddd: undefined,
                    numero: undefined,
                    observacao: "observacao 2",
                    tipoTelefone: "RESIDENCIAL 2"
                }
            }
        ];

        var convertido = referenciaConvert.convertGet(referencias);

        expect(convertido[0].telefone.numero).toBeUndefined();
        expect(convertido[1].telefone.numero).toBeUndefined();
    });

    it('deve converter os NUMEROS de TELEFONES, no POST', function () {
        var referencias = [
            {
                nomeAgenciaLoja: "Agencia teste",
                nomeBancoEmpresa: "Branco teste",
                telefone: {
                    numero: "5199999999",
                    observacao: "observacao",
                    tipoTelefone: "RESIDENCIAL"
                }
            },
            {
                nomeAgenciaLoja: "Agencia teste 2",
                nomeBancoEmpresa: "Branco teste 2",
                telefone: {
                    numero: "5198888888",
                    observacao: "observacao 2",
                    tipoTelefone: "RESIDENCIAL"
                }
            }
        ];

        var convertido = referenciaConvert.convertPost(referencias);

        expect(convertido[0].telefone.ddd).toEqual("51");
        expect(convertido[0].telefone.numero).toEqual("99999999");
        expect(convertido[1].telefone.ddd).toEqual("51");
        expect(convertido[1].telefone.numero).toEqual("98888888");
    });

    it('não deve converter os NUMEROS de TELEFONES, no POST', function () {
        var referencias = [
            {
                nomeAgenciaLoja: "Agencia teste",
                nomeBancoEmpresa: "Branco teste",
                telefone: {
                    numero: undefined,
                    observacao: "observacao",
                    tipoTelefone: "RESIDENCIAL"
                }
            },
            {
                nomeAgenciaLoja: "Agencia teste 2",
                nomeBancoEmpresa: "Branco teste 2",
                telefone: {
                    numero: undefined,
                    observacao: "observacao 2",
                    tipoTelefone: "RESIDENCIAL"
                }
            }
        ];

        var convertido = referenciaConvert.convertPost(referencias);

        expect(convertido[0].telefone.ddd).toBeUndefined();
        expect(convertido[0].telefone.numero).toBeUndefined();
        expect(convertido[1].telefone.numero).toBeUndefined();
        expect(convertido[1].telefone.numero).toBeUndefined();
    });
});


