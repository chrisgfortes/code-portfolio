
describe('Component: dadosReferencia', function() {

    var $componentController,
        ctrl;

    beforeEach(module('app'));

    beforeEach(inject(function(_$componentController_) {
        $componentController = _$componentController_;

        var bindings = {
            referencia: {}
        };

        ctrl = $componentController('dadosReferencia', null, bindings);

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o referencia no bindings', function() {
        expect(ctrl.referencia).toBeDefined();
    });

    it('deve chamar o evento onSalvar', function () {
        var onSalvarSpy = jasmine.createSpy('onSalvar');

        var bindings = {
            onSalvar: onSalvarSpy
        };

        var ctrl = $componentController('dadosReferencia', null, bindings);

        ctrl.salvar();

        expect(onSalvarSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onCancelar', function () {
        var onCancelarSpy = jasmine.createSpy('onCancelar');

        var bindings = {
            onCancelar: onCancelarSpy
        };

        var ctrl = $componentController('dadosReferencia', null, bindings);

        ctrl.cancelar();

        expect(onCancelarSpy).toHaveBeenCalled();
    });
});
    