
describe('Component: listaReferencia', function () {

    var $componentController,
        ctrl;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_) {
        $componentController = _$componentController_;

        var bindings = {
            referencias: []
        };

        ctrl = $componentController('listaReferencia', null, bindings);

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o referencias no bindings', function () {
        expect(ctrl.referencias).toBeDefined();
        expect(ctrl.referencias.length).toBe(0);
    });

    it('deve chamar o evento onNovo', function () {
        var onNovoSpy = jasmine.createSpy('onNovo');

        var bindings = {
            onNovo: onNovoSpy
        };

        var ctrl = $componentController('listaReferencia', null, bindings);

        ctrl.novo();

        expect(onNovoSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onRemover', function () {
        var onRemoverSpy = jasmine.createSpy('onRemover');

        var bindings = {
            onRemover: onRemoverSpy
        };

        var ctrl = $componentController('listaReferencia', null, bindings);

        var referencia = {
            modoExclusao: true
        };

        ctrl.remover(referencia);

        expect(onRemoverSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onEditar', function () {
        var onEditarSpy = jasmine.createSpy('onEditar');

        var bindings = {
            onEditar: onEditarSpy
        };

        var ctrl = $componentController('listaReferencia', null, bindings);

        ctrl.referencias = [{ modoEdicao: false }];

        var referencia = {
            modoEdicao: true
        };

        ctrl.editar(referencia);

        var referenciaEsperada = ctrl.referencias[0];

        expect(onEditarSpy).toHaveBeenCalled();
    });
});
