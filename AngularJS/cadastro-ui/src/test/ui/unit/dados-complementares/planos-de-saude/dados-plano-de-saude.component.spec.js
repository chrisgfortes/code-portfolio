describe('Component: dadosPlanoDeSaude', function () {

    var $componentController,
        moment,
        $rootScope,
        ctrl,
        planoDeSaude,
        $q;

    var tiposPlanoSaude = [{
        valor: "MEDICO",
        descricao: "Médico"
    }];

    var instituicoesPlanoSaude=[
        {
            valor: "AMIL",
            descricao: "Amil"
        }
    ];
    var coberturasPlanoSaude=[
        {
            valor: "COMPLETA",
            descricao: "Completa"
        },
        {
            valor: "PARCIAL",
            descricao: "Parcial"
        },
        {
            valor: "MINIMA",
            descricao: "Mínima"
        }
    ];

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _moment_, _planoDeSaude_, _$q_, _$rootScope_) {
        $componentController = _$componentController_;
        moment = _moment_;
        planoDeSaude = _planoDeSaude_;
        $q = _$q_;
        $rootScope = _$rootScope_;

        var bindings = {
            planoDeSaude: {}
        };

        ctrl = $componentController('dadosPlanoDeSaude',
            null,
            bindings
        );

        spyOn(planoDeSaude, 'tiposPlanoSaude').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: tiposPlanoSaude }); }
            };
        });
        spyOn(planoDeSaude, 'instituicoesPlanoSaude').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: instituicoesPlanoSaude }); }
            };
        });
        spyOn(planoDeSaude, 'coberturasPlanoSaude').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: coberturasPlanoSaude }); }
            };
        });

        ctrl.$onInit();
    }));

    it('deve chamar planoDeSaude.tiposPlanoSaude()', function () {
        expect(planoDeSaude.tiposPlanoSaude).toHaveBeenCalled();
    });

    it('deve chamar planoDeSaude.instituicoesPlanoSaude()', function () {
        expect(planoDeSaude.instituicoesPlanoSaude).toHaveBeenCalled();
    });

    it('deve chamar planoDeSaude.coberturasPlanoSaude()', function () {
        expect(planoDeSaude.coberturasPlanoSaude).toHaveBeenCalled();
    });

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o plano de saúde no bindings', function () {
        expect(ctrl.planoDeSaude).toBeDefined();
    });

    it('data de vencimento deve ser IGUAL ou MAIOR que hoje', function () {
        var ontem = moment().add(-1, 'day');
        var hoje = moment();
        var isMaior = ctrl.vencimentoMinima.isAfter(ontem);
        var isHoje = ctrl.vencimentoMinima.isSame(hoje, 'day');

        expect(isMaior).toBe(true);
        expect(isHoje).toBe(true);
    });

    it('deve chamar o evento onSalvar', function () {
        var onSalvarSpy = jasmine.createSpy('onSalvar');

        var bindings = {
            planoDeSaude: {},
            onSalvar: onSalvarSpy
        };

        var ctrl = $componentController('dadosPlanoDeSaude', null, bindings);
        ctrl.tiposPlanoSaude = tiposPlanoSaude;
        ctrl.instituicoesPlanoSaude = instituicoesPlanoSaude;
        ctrl.planoDeSaude = {
            tipoPlanoSaude: "MEDICO",
            instituicaoSaude: "AMIL",
            valorMensal: 65.65,
            tipoCobertura: "COMPLETA",
            dataVencimento: "2017-03-30"
        };

        ctrl.salvar();

        expect(onSalvarSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onCancelar', function () {
        var onCancelarSpy = jasmine.createSpy('onCancelar');

        var bindings = {
            planoDeSaude: {},
            onCancelar: onCancelarSpy
        };

        var ctrl = $componentController('dadosPlanoDeSaude', null, bindings);

        ctrl.cancelar();

        expect(onCancelarSpy).toHaveBeenCalled();
    });

    it('não deve aceitar DATA DE VENCIMENTO inválida', function () {
        var dataVencimento = moment('2000-23-56');
        ctrl.validarData(dataVencimento);
        expect(ctrl.planoDeSaude.dataVencimento).toBeUndefined();
    });

    it('deve aceitar DATA DE VENCIMENTO válida', function () {
        ctrl.planoDeSaude.dataVencimento = moment('2000-12-12');
        ctrl.validarData(ctrl.planoDeSaude.dataVencimento);
        expect(ctrl.planoDeSaude.dataVencimento).toBeDefined();
    });
});
