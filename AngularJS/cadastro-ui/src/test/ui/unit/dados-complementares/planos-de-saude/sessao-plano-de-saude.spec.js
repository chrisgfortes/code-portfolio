describe('Component: sessaoPlanoDeSaude', function () {

    var $componentController,
        dialogs,
        $q,
        $rootScope,
        planoDeSaudeFactory,
        ctrl;

    var tiposPlanoSaude = [{
        valor: "MEDICO",
        descricao: "Médico"
    }];

    var instituicoesPlanoSaude=[
        {
            valor: "AMIL",
            descricao: "Amil"
        }
    ];

    var planosDeSaude = [{
        tipoPlanoSaude: "MEDICO",
        instituicaoSaude: "AMIL",
        valorMensal: 65.65,
        tipoCobertura: "COMPLETA",
        dataVencimento: "2017-03-30"
    }];

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _dialogs_, _$q_, _$rootScope_, _planoDeSaude_) {
        $componentController = _$componentController_;
        dialogs = _dialogs_;
        $q = _$q_;
        $rootScope = _$rootScope_;
        planoDeSaudeFactory = _planoDeSaude_;

        var bindings = {
            planosSaude: planosDeSaude
        };

        ctrl = $componentController('sessaoPlanoDeSaude',
            {
                dialogs: _dialogs_
            }, bindings);

        spyOn(planoDeSaudeFactory, 'tiposPlanoSaude').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: tiposPlanoSaude }); }
            };
        });

        spyOn(planoDeSaudeFactory, 'instituicoesPlanoSaude').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: instituicoesPlanoSaude }); }
            };
        });

        ctrl.$onInit();
    }));

    it('deve chamar planoDeSaudeFactory.tiposPlanoSaude()', function () {
        expect(planoDeSaudeFactory.tiposPlanoSaude).toHaveBeenCalled();
    });

    it('deve carregar a descrição do tipo de plano de saúde', function () {
        var planoDeSaude = planosDeSaude[0];
        planoDeSaude.descricaoTipoDePlano = "Médico";
        
        expect(ctrl.planosSaude[0]).toEqual(planoDeSaude);
    });

    it('deve chamar planoDeSaudeFactory.instituicoesPlanoSaude()', function () {
        expect(planoDeSaudeFactory.instituicoesPlanoSaude).toHaveBeenCalled();
    });

    it('deve carregar a descrição da instituição do plano de saúde', function () {
        var planoDeSaude = planosDeSaude[0];
        planoDeSaude.descricaoInstituicao = "Amil";
        expect(ctrl.planosSaude[0]).toEqual(planoDeSaude);
    });

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o planosSaude no bindings', function () {
        expect(ctrl.planosSaude).toBeDefined();
    });

    it('deve SALVAR um plano de saúde', function () {
        var plano = {
            tipoPlanoDeSaude:"MEDICO",
            instituicao:"INSTITUICAO1",
            valorPlanoDeSaude:54.65,
            cobertura:"COMPLETA",
            dataVencimento:"2017-03-29T03:00:00.000Z",
            modoEdicao: false
        };

        ctrl.mostrarPlanoDeSaudeForm = true;

        ctrl.salvarPlanoDeSaude(plano);

        expect(ctrl.planosSaude.length).toBe(2);
        expect(ctrl.mostrarPlanoDeSaudeForm).toBe(false);
    });

    it('deve EDITAR um plano de saúde', function () {
        var plano = {
            tipoPlanoDeSaude:"MEDICO",
            instituicao:"INSTITUICAO1",
            valorPlanoDeSaude:54.65,
            cobertura:"COMPLETA",
            dataVencimento:"2017-03-29T03:00:00.000Z",
            modoEdicao: true
        };

        var planoEditado = {
            tipoPlanoDeSaude:"MEDICO",
            instituicao:"INSTITUICAO1",
            valorPlanoDeSaude:2547.65,
            cobertura:"COMPLETA",
            dataVencimento:"2017-03-29T03:00:00.000Z",
            modoEdicao: true
        };

        var planoSecundario = {
            tipoPlanoDeSaude:"ODONTOLOGICO",
            instituicao:"INSTITUICAO2",
            valorPlanoDeSaude:8795,
            cobertura:"COMPLETA",
            dataVencimento:"2017-01-29T03:00:00.000Z",
            modoEdicao: false
        };

        ctrl.planosSaude.push(plano);
        ctrl.planosSaude.push(planoSecundario);

        ctrl.mostrarPlanoDeSaudeForm = true;

        ctrl.salvarPlanoDeSaude(planoEditado);

        expect(ctrl.planosSaude[1].valorPlanoDeSaude).toBe(2547.65);
        expect(ctrl.planosSaude[1].modoEdicao).toBe(false);
        expect(ctrl.planosSaude[2]).toEqual(planoSecundario);
        expect(ctrl.planosSaude.length).toBe(3);
        expect(ctrl.mostrarPlanoDeSaudeForm).toBe(false);
    });

    it('deve CANCELAR nova/edição plano de saúde', function () {
        var plano = {
            tipoPlanoDeSaude:"MEDICO",
            instituicao:"INSTITUICAO1",
            valorPlanoDeSaude:54.65,
            cobertura:"COMPLETA",
            dataVencimento:"2017-03-29T03:00:00.000Z",
            modoEdicao: true
        };
        ctrl.mostrarPlanoDeSaudeForm = true;

        ctrl.planosSaude.push(plano);

        ctrl.cancelarPlanoDeSaude();

        expect(ctrl.planosSaude.length).toBe(2);
        expect(ctrl.planosSaude[1].modoEdicao).toBe(false);
        expect(ctrl.mostrarPlanoDeSaudeForm).toBe(false);
    });

    it('deve NOVO um plano de saúde', function () {
        ctrl.mostrarPlanoDeSaudeForm = false;

        ctrl.novoPlanoDeSaude();

        expect(ctrl.mostrarPlanoDeSaudeForm).toBe(true);
    });

    it('deve ficar em MODO EDIÇÃO o plano de saúde', function () {
        ctrl.mostrarPlanoDeSaudeForm = false;
        ctrl.planoDeSaude = null;

        var plano = {
            tipoPlanoDeSaude:"MEDICO",
            instituicao:"INSTITUICAO1",
            valorPlanoDeSaude:54.65,
            cobertura:"COMPLETA",
            dataVencimento:"2017-03-29T03:00:00.000Z",
            modoEdicao: true
        };

        ctrl.editarPlanoDeSaude(plano);

        expect(ctrl.mostrarPlanoDeSaudeForm).toBe(true);
        expect(ctrl.planoDeSaude).toEqual(plano);
    });

    it('deve REMOVER um plano de saúde', function () {
        var deferred = $q.defer();
        deferred.resolve('yes');
        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var plano = {
            tipoPlanoDeSaude:"MEDICO",
            instituicao:"INSTITUICAO1",
            valorPlanoDeSaude:54.65,
            cobertura:"COMPLETA",
            dataVencimento:"2017-03-29T03:00:00.000Z",
            modoExclusao: true
        };

        var planoSecundario = {
            tipoPlanoDeSaude:"ODONTOLOGICO",
            instituicao:"INSTITUICAO2",
            valorPlanoDeSaude:8795,
            cobertura:"COMPLETA",
            dataVencimento:"2017-01-29T03:00:00.000Z",
            modoExclusao: false
        };

        ctrl.planosSaude.push(plano);
        ctrl.planosSaude.push(planoSecundario);
        var index = 0;

        ctrl.removerPlanoDeSaude(index);

        $rootScope.$apply();

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.planosSaude.length).toBe(2);
    });

    it('deve cancelar a REMOVOÇÃO do plano de saúde', function () {
        var deferred = $q.defer();
        deferred.reject();
        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var plano = {
            tipoPlanoDeSaude:"MEDICO",
            instituicao:"INSTITUICAO1",
            valorPlanoDeSaude:54.65,
            cobertura:"COMPLETA",
            dataVencimento:"2017-03-29T03:00:00.000Z",
            modoExclusao: true
        };

        var planoSecundario = {
            tipoPlanoDeSaude:"ODONTOLOGICO",
            instituicao:"INSTITUICAO2",
            valorPlanoDeSaude:8795,
            cobertura:"COMPLETA",
            dataVencimento:"2017-01-29T03:00:00.000Z",
            modoExclusao: false
        };

        ctrl.planosSaude.push(plano);
        ctrl.planosSaude.push(planoSecundario);

        ctrl.removerPlanoDeSaude(planoSecundario);

        $rootScope.$apply();

        var todosDesabilitados = ctrl.planosSaude
            .some(function (plano) {
                return !plano.modoExclusao;
            });

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.planosSaude.length).toBe(3);
        expect(todosDesabilitados).toBe(true);
    });
});
