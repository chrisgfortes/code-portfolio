describe('factory: planoDeSaude', function() {

    var planoDeSaude,
        httpBackend,
        HOST;

    beforeEach(module('app'));

    beforeEach(inject(function($httpBackend, _planoDeSaude_, _HOST_) {
        planoDeSaude = _planoDeSaude_;
        HOST = _HOST_;
        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));

    afterEach(function() {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.tiposPlanoSaude()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.pessoa + '/fisica/plano-saude/tipos')
            .respond(200, modelsResponse);

        planoDeSaude
            .tiposPlanoSaude()
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.coberturasPlanoSaude()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.pessoa + '/fisica/plano-saude/coberturas')
            .respond(200, modelsResponse);

        planoDeSaude
            .coberturasPlanoSaude()
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.instituicoesPlanoSaude()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.pessoa + '/fisica/plano-saude/instituicoes')
            .respond(200, modelsResponse);

        planoDeSaude
            .instituicoesPlanoSaude()
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });
});