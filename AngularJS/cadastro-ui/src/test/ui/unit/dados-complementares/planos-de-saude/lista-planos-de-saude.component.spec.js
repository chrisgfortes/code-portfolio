describe('Componente LISTA PLANOS DE SAUDE', function () {

    var $componentController,
        $rootScope,
        ctrl,
        dialogs,
        $q;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _$rootScope_,_dialogs_, _$q_) {
        $componentController = _$componentController_;
        $rootScope = _$rootScope_;
        dialogs = _dialogs_;
        $q = _$q_;

        var bindings = {
            planosDeSaude: []
        };

        ctrl = $componentController('listaPlanosDeSaude',
            null,
            bindings
        );

        ctrl.$onInit();
    }));

    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o planosDeSaude no bindings', function () {
        expect(ctrl.planosDeSaude).toBeDefined();
    });

    it('deve chamar o evento onNovo', function () {
        var onNovoSpy = jasmine.createSpy('onNovo');

        var bindings = {
            planosDeSaude: {},
            onNovo: onNovoSpy
        };

        var ctrl = $componentController('listaPlanosDeSaude', null, bindings);

        ctrl.novo();

        expect(onNovoSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onEditar', function () {
        var onEditarSpy = jasmine.createSpy('onEditar');

        var bindings = {
            onEditar: onEditarSpy
        };

        var index = 0;

        var ctrl = $componentController('listaPlanosDeSaude', null, bindings);

        ctrl.planosSaude = [{ modoEdicao : false }];

        ctrl.editar(index);

        var plano = ctrl.planosSaude[0];

        expect(onEditarSpy).toHaveBeenCalled();
        expect(plano.modoEdicao).toBe(true);
    });

    it('deve chamar o evento onRemover', function () {
        var onRemoverSpy = jasmine.createSpy('onRemover');

        var bindings = {
            onRemover: onRemoverSpy
        };

        var ctrl = $componentController('listaPlanosDeSaude', null, bindings);

        ctrl.planosSaude = [{ modoExclusao : false }];
        var plano = ctrl.planosSaude[0];

        ctrl.remover(plano);

        expect(onRemoverSpy).toHaveBeenCalled();
    });

});
