describe('Component: sessaoPrevidencia', function () {

    var $componentController,
        dialogs,
        $q,
        $rootScope,
        ctrl;

    var tiposPrevidencia = [{
        valor: "PGBL",
        descricao: "PGBL"
      }];

    var instituicoesPrevidencia=[{
            valor: "QUANTA",
            descricao: "Quanta"
    }];

    var previdenciasTeste = [{
        dataInicioContribuicao: "2017-04-03",
        instituicao: "QUANTA",
        numeroDependentesSemPlano: 0,
        numeroProposta: 0,
        possuiDependente: true,
        tipoPlano: "PGBL",
        valorContribuicao: 0,
        valorMontante: 0
    }];

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _dialogs_, _$q_, _$rootScope_, _previdencia_) {
        $componentController = _$componentController_;
        dialogs = _dialogs_;
        $q = _$q_;
        $rootScope = _$rootScope_;
        previdencia = _previdencia_;

        var bindings = {
            previdencias: previdenciasTeste
        };

        ctrl = $componentController('sessaoPrevidencia',
            {
                dialogs: _dialogs_
            }, bindings);

        spyOn(previdencia, 'tipos').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: tiposPrevidencia }); }
            };
        });

        spyOn(previdencia, 'instituicoes').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: instituicoesPrevidencia }); }
            };
        });

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve chamar previdencia.tipos()', function () {
        expect(previdencia.tipos).toHaveBeenCalled();
    });

    it('deve carregar a descrição do tipo de previdência', function () {
        var previdencia = previdenciasTeste[0];
        previdencia.descricaoTipoDePrevidencia = "PGBL";
        expect(ctrl.previdencias[0]).toEqual(previdencia);
    });

    it('deve chamar previdencia.instituicoes()', function () {
        expect(previdencia.instituicoes).toHaveBeenCalled();
    });

    it('deve carregar a descrição da instituição da previdência', function () {
        var previdencia = previdenciasTeste[0];
        previdencia.descricaoInstituicao = "Quanta";
        expect(ctrl.previdencias[0]).toEqual(previdencia);
    });

    it('deve definir o previdencias no bindings', function () {
        expect(ctrl.previdencias).toBeDefined();
    });

    it('deve SALVAR uma previdência', function () {
        var previdencia = {
            dataInicioContribuicao: "2017-01-03",
            instituicao: "QUANTA",
            numeroDependentesSemPlano: 2,
            numeroProposta: 123,
            possuiDependente: true,
            tipoPlano: "PGBL",
            valorContribuicao: 55.44,
            valorMontante: 21.43
        };

        ctrl.mostrarPrevidenciaForm = true;

        ctrl.salvarPrevidencia(previdencia);

        expect(ctrl.previdencias.length).toBe(2);
        expect(ctrl.mostrarPrevidenciaForm).toBe(false);
    });

    it('deve EDITAR uma previdência', function () {
        var previdencia = {
            dataInicioContribuicao: "2017-01-03",
            instituicao: "QUANTA",
            numeroDependentesSemPlano: 2,
            numeroProposta: 123,
            possuiDependente: true,
            tipoPlano: "PGBL",
            valorContribuicao: 55.44,
            valorMontante: 21.43,
            modoEdicao: true
        };

        var previdenciaEditada = {
            dataInicioContribuicao: "2017-01-03",
            instituicao: "QUANTA",
            numeroDependentesSemPlano: 2,
            numeroProposta: 123,
            possuiDependente: true,
            tipoPlano: "PGBL",
            valorContribuicao: 551.44,
            valorMontante: 21.43,
            modoEdicao: true
        };

        var previdenciaSecundaria = {
            dataInicioContribuicao: "2012-03-01",
            instituicao: "QUANTA",
            numeroDependentesSemPlano: 0,
            numeroProposta: 643,
            possuiDependente: false,
            tipoPlano: "PGBL",
            valorContribuicao: 12.24,
            valorMontante: 1.43,
            modoEdicao: false
        };

        ctrl.previdencias.push(previdencia);
        ctrl.previdencias.push(previdenciaSecundaria);

        ctrl.mostrarPrevidenciaForm = true;

        ctrl.salvarPrevidencia(previdenciaEditada);

        expect(ctrl.previdencias[1].valorContribuicao).toBe(551.44);
        expect(ctrl.previdencias[1].modoEdicao).toBe(false);
        expect(ctrl.previdencias[2]).toEqual(previdenciaSecundaria);
        expect(ctrl.previdencias.length).toBe(3);
        expect(ctrl.mostrarPrevidenciaForm).toBe(false);
    });

    it('deve CANCELAR nova/edição previdência', function () {
        var previdencia = {
            dataInicioContribuicao: "2017-01-03",
            instituicao: "QUANTA",
            numeroDependentesSemPlano: 2,
            numeroProposta: 123,
            possuiDependente: true,
            tipoPlano: "PGBL",
            valorContribuicao: 55.44,
            valorMontante: 21.43
        };
        ctrl.mostrarPrevidenciaForm = true;

        ctrl.previdencias.push(previdencia);

        ctrl.cancelarPrevidencia();

        expect(ctrl.previdencias.length).toBe(2);
        expect(ctrl.previdencias[1].modoEdicao).toBe(false);
        expect(ctrl.mostrarPrevidenciaForm).toBe(false);
    });

    it('deve NOVO uma previdência', function () {
        ctrl.mostrarPrevidenciaForm = false;

        ctrl.novaPrevidencia();

        expect(ctrl.mostrarPrevidenciaForm).toBe(true);
    });

    it('deve ficar em MODO EDIÇÃO a previdência', function () {
        ctrl.mostrarPrevidenciaForm = false;
        ctrl.previdencia = null;

        var previdencia = {
            dataInicioContribuicao: "2017-01-03",
            instituicao: "QUANTA",
            numeroDependentesSemPlano: 2,
            numeroProposta: 123,
            possuiDependente: true,
            tipoPlano: "PGBL",
            valorContribuicao: 55.44,
            valorMontante: 21.43
        };

        ctrl.editarPrevidencia(previdencia);

        expect(ctrl.mostrarPrevidenciaForm).toBe(true);
        expect(ctrl.previdencia).toEqual(previdencia);
    });

    it('deve REMOVER uma previdência', function () {
        var deferred = $q.defer();
        deferred.resolve('yes');
        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var previdencia = {
            dataInicioContribuicao: "2017-01-03",
            instituicao: "QUANTA",
            numeroDependentesSemPlano: 2,
            numeroProposta: 123,
            possuiDependente: true,
            tipoPlano: "PGBL",
            valorContribuicao: 55.44,
            valorMontante: 21.43,
            modoExclusao: true
        };

        var previdenciaSecundaria = {
            dataInicioContribuicao: "2012-03-01",
            instituicao: "QUANTA",
            numeroDependentesSemPlano: 0,
            numeroProposta: 643,
            possuiDependente: false,
            tipoPlano: "PGBL",
            valorContribuicao: 12.24,
            valorMontante: 1.43,
            modoExclusao: false
        };

        ctrl.previdencias.push(previdencia);
        ctrl.previdencias.push(previdenciaSecundaria);
        var index = 0;

        ctrl.removerPrevidencia(index);

        $rootScope.$apply();

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.previdencias.length).toBe(2);
    });

    it('deve cancelar a REMOVOÇÃO da previdencia', function () {
        var deferred = $q.defer();
        deferred.reject();
        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var previdencia = {
            dataInicioContribuicao: "2017-01-03",
            instituicao: "QUANTA",
            numeroDependentesSemPlano: 2,
            numeroProposta: 123,
            possuiDependente: true,
            tipoPlano: "PGBL",
            valorContribuicao: 55.44,
            valorMontante: 21.43,
            modoExclusao: true
        };

        var previdenciaSecundaria = {
            dataInicioContribuicao: "2012-03-01",
            instituicao: "QUANTA",
            numeroDependentesSemPlano: 0,
            numeroProposta: 643,
            possuiDependente: false,
            tipoPlano: "PGBL",
            valorContribuicao: 12.24,
            valorMontante: 1.43,
            modoExclusao: false
        };

        ctrl.previdencias.push(previdencia);
        ctrl.previdencias.push(previdenciaSecundaria);

        ctrl.removerPrevidencia(previdenciaSecundaria);

        $rootScope.$apply();

        var todosDesabilitados = ctrl.previdencias
            .some(function (previdencia) {
                return !previdencia.modoExclusao;
            });

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.previdencias.length).toBe(3);
        expect(todosDesabilitados).toBe(true);
    });
});
