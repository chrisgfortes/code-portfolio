
describe('Service: previdenciaConvert', function () {

    var previdenciaConvert;
    var moment;

    beforeEach(module('app'));

    beforeEach(inject(function (_previdenciaConvert_, _moment_) {
        previdenciaConvert = _previdenciaConvert_;
        moment = _moment_;
    }));

    it('deve estar definido', function () {
        expect(previdenciaConvert).toBeDefined();
    });

    it('deve converter as datas, no GET', function () {
        var previdencias = [
            {
                dataInicioContribuicao: '2017-05-01'
            },
            {
                dataInicioContribuicao: '2016-05-01'
            }
        ];

        var convertido = previdenciaConvert.convertGet(previdencias);

        expect(moment.isMoment(convertido[0].dataInicioContribuicao)).toBe(true);
        expect(moment.isMoment(convertido[1].dataInicioContribuicao)).toEqual(true);
    });

    it('não deve converter as datas, no GET', function () {
        var previdencias = [
            {
                dataInicioContribuicao: undefined
            },
            {
                dataInicioContribuicao: undefined
            }
        ];

        var convertido = previdenciaConvert.convertGet(previdencias);

        expect(convertido[0].dataInicioContribuicao).not.toBeDefined();
        expect(convertido[1].dataInicioContribuicao).not.toBeDefined();
    });

    it('deve converter as datas, no POST', function () {
        var previdencias = [
            {
                descricaoTipoDePrevidencia: 'descricaoTipoDePrevidencia',
                descricaoInstituicao: 'descricaoInstituicao'
            }
        ];

        var convertido = previdenciaConvert.convertPost(previdencias);

        expect(convertido[0].descricaoTipoDePrevidencia).not.toBeDefined();
        expect(convertido[0].descricaoInstituicao).not.toBeDefined();
    });
});


