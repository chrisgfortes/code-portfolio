describe('Component: dadosPrevidencia', function () {

    var $componentController,
        moment,
        $rootScope,
        ctrl,
        previdencia,
        $q;

    var tiposPrevidencia = [{
        valor: "PGBL",
        descricao: "PGBL"
      }];

    var instituicoesPrevidencia=[
        {
            valor: "QUANTA",
            descricao: "Quanta"
        }
    ];

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _moment_, _previdencia_, _$q_, _$rootScope_) {
        $componentController = _$componentController_;
        moment = _moment_;
        previdencia = _previdencia_;
        $q = _$q_;
        $rootScope = _$rootScope_;

        var bindings = {
            previdencia: {}
        };

        ctrl = $componentController('dadosPrevidencia',
            null,
            bindings
        );

        spyOn(previdencia, 'tipos').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: tiposPrevidencia }); }
            };
        });
        spyOn(previdencia, 'instituicoes').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: instituicoesPrevidencia }); }
            };
        });

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve chamar previdencia.tipos()', function () {
        expect(previdencia.tipos).toHaveBeenCalled();
    });

    it('deve chamar previdencia.instituicoes()', function () {
        expect(previdencia.instituicoes).toHaveBeenCalled();
    });

    it('deve calcular o tempo de contribuição', function () {
        var data = moment().subtract(6, 'month');
        var seisMeses = moment().diff(data, 'months');

        ctrl.validarDataECalcularMeses(data);

        expect(ctrl.previdencia.mesesContribuicao).toBe(seisMeses);
    });

    it("não deve calcular tempo de contribuição", function () {
        ctrl.validarDataECalcularMeses(undefined);

        expect(ctrl.previdencia.dataInicioContribuicao).toBeUndefined();
        expect(ctrl.previdencia.mesesContribuicao).toBeUndefined();
    });

    it('deve definir a previdência no bindings', function () {
        expect(ctrl.previdencia).toBeDefined();
    });

    it('deve chamar o evento onSalvar', function () {
        var onSalvarSpy = jasmine.createSpy('onSalvar');

        var bindings = {
            previdencia: {},
            onSalvar: onSalvarSpy
        };

        var ctrl = $componentController('dadosPrevidencia', null, bindings);
        ctrl.tiposPrevidencia = tiposPrevidencia;
        ctrl.instituicoesPrevidencia = instituicoesPrevidencia;
        ctrl.previdencia = {
          dataInicioContribuicao: "2017-04-03",
          instituicao: "QUANTA",
          numeroDependentesSemPlano: 0,
          numeroProposta: 0,
          possuiDependente: true,
          tipoPlano: "PGBL",
          valorContribuicao: 0,
          valorMontante: 0
        };

        ctrl.salvar();

        expect(onSalvarSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onCancelar', function () {
        var onCancelarSpy = jasmine.createSpy('onCancelar');

        var bindings = {
            previdencia: {},
            onCancelar: onCancelarSpy
        };

        var ctrl = $componentController('dadosPrevidencia', null, bindings);

        ctrl.cancelar();

        expect(onCancelarSpy).toHaveBeenCalled();
    });
});
