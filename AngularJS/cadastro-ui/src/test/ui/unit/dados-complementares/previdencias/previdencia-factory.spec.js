describe('factory: previdencia', function() {

    var previdencia,
        httpBackend,
        HOST;

    beforeEach(module('app'));

    beforeEach(inject(function($httpBackend, _previdencia_, _HOST_) {
        previdencia = _previdencia_;
        HOST = _HOST_;
        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));

    afterEach(function() {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.tipos()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.pessoa + '/fisica/previdencia/tipos')
            .respond(200, modelsResponse);

        previdencia
            .tipos()
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

     it('.instituicoes()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.pessoa + '/fisica/previdencia/instituicoes')
            .respond(200, modelsResponse);

        previdencia
            .instituicoes()
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });
});