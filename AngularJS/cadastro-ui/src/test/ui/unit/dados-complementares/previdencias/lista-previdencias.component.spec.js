describe('Componente LISTA PREVIDENCIAS', function () {

    var $componentController,
        $rootScope,
        ctrl,
        dialogs,
        $q;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _$rootScope_,_dialogs_, _$q_) {
        $componentController = _$componentController_;
        $rootScope = _$rootScope_;
        dialogs = _dialogs_;
        $q = _$q_;

        var bindings = {
            previdencias: []
        };

        ctrl = $componentController('listaPrevidencias',
            null,
            bindings
        );

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o previdencias no bindings', function () {
        expect(ctrl.previdencias).toBeDefined();
    });

    it('deve definir valor padrão em previdencias', function () {
        ctrl.previdencias = undefined;

        ctrl.$onInit()

        expect(ctrl.previdencias).toEqual([]);
    });

    it('deve chamar o evento onNovo', function () {
        var onNovoSpy = jasmine.createSpy('onNovo');

        var bindings = {
            previdencias: {},
            onNovo: onNovoSpy
        };

        var ctrl = $componentController('listaPrevidencias', null, bindings);

        ctrl.novo();

        expect(onNovoSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onEditar', function () {
        var onEditarSpy = jasmine.createSpy('onEditar');

        var bindings = {
            onEditar: onEditarSpy
        };

        var index = 0;

        var ctrl = $componentController('listaPrevidencias', null, bindings);

        ctrl.previdencias = [{ modoEdicao : false }];

        ctrl.editar(index);

        var previdencia = ctrl.previdencias[0];

        expect(onEditarSpy).toHaveBeenCalled();
        expect(previdencia.modoEdicao).toBe(true);
    });

    it('deve chamar o evento onRemover', function () {
        var onRemoverSpy = jasmine.createSpy('onRemover');

        var bindings = {
            onRemover: onRemoverSpy
        };

        var ctrl = $componentController('listaPrevidencias', null, bindings);

        ctrl.previdencias = [{ modoExclusao : false }];
        var plano = ctrl.previdencias[0];

        ctrl.remover(plano);

        expect(onRemoverSpy).toHaveBeenCalled();
    });

});
