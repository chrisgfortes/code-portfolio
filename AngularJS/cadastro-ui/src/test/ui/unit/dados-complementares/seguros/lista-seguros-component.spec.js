describe('Component: listaSeguros', function () {

    var $componentController,
        ctrl,
        seguro;

    var tipoSeguros = [{ valor: 'SEGURO_VIDA', descricao: 'Seguro de vida' }];
    var seguradoras = [{ valor: 'ALLIANZ', descricao: 'Allianz' }];

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _seguro_) {
        $componentController = _$componentController_;
        seguro = _seguro_;

        var bindings = {
            seguros: []
        };

        spyOn(seguro, 'tipos').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: tipoSeguros }); }
            };
        });

        spyOn(seguro, 'seguradoras').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: seguradoras }); }
            };
        });

        ctrl = $componentController('listaSeguros',
            {
                seguro: _seguro_
            },
            bindings
        );

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o seguros no bindings', function () {
        expect(ctrl.seguros).toBeDefined();
        expect(ctrl.seguros.length).toBe(0);
    });

    it('deve definir o valor padrão para seguros', function () {
        ctrl.seguros = undefined;

        ctrl.$onInit();

        expect(ctrl.seguros).toBeDefined();
        expect(ctrl.seguros.length).toBe(0);
    });

    it('deve adicionar a descrição do tipo de seguro', function () {
        ctrl.seguros = [
            { tipoSeguro: 'SEGURO_VIDA', seguradora: 'ALLIANZ' }
        ];
        
        ctrl.readonly = true;
        
        ctrl.$onInit();

        expect(seguro.tipos).toHaveBeenCalled();
        expect(seguro.seguradoras).toHaveBeenCalled();
        expect(ctrl.tipoSeguros.length).toBe(1);
        expect(ctrl.seguros[0].tipoDescricao).toBe(tipoSeguros[0].descricao);
        expect(ctrl.seguros[0].seguradoraDescricao).toBe(seguradoras[0].descricao);
    });

    it('deve chamar o evento onNovo', function () {
        var onNovoSpy = jasmine.createSpy('onNovo');

        var bindings = {
            onNovo: onNovoSpy
        };

        var ctrl = $componentController('listaSeguros', null, bindings);

        ctrl.novo();

        expect(onNovoSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onEditar', function () {
        var onEditarSpy = jasmine.createSpy('onEditar');

        var bindings = {
            onEditar: onEditarSpy
        };

        var index = 0;

        var ctrl = $componentController('listaSeguros', null, bindings);

        ctrl.seguros = [{ modoEdicao: false }];

        var seguro = {
            modoEdicao: false
        }

        ctrl.editar(seguro);

        var seguroEsperado = {
            seguro: {
                modoEdicao: true
            }
        }

        expect(onEditarSpy).toHaveBeenCalledWith(seguroEsperado);
        expect(seguro.modoEdicao).toBe(true);
    });

    it('deve chamar o evento onRemover', function () {
        var onRemoverSpy = jasmine.createSpy('onRemover');

        var bindings = {
            onRemover: onRemoverSpy
        };

        var ctrl = $componentController('listaSeguros', null, bindings);

        ctrl.seguros = [{ modoExclusao: false }];

        var seguro = ctrl.seguros[0];

        ctrl.remover(seguro);

        var seguroEsperado = {
            seguro: {
                modoExclusao: true
            }
        }

        expect(onRemoverSpy).toHaveBeenCalledWith(seguroEsperado);
    });
});
