describe('factory: seguro', function() {

    var seguro,
        httpBackend,
        HOST;

    beforeEach(module('app'));

    beforeEach(inject(function($httpBackend, _seguro_, _HOST_) {
        seguro = _seguro_;
        HOST = _HOST_;
        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));

    afterEach(function() {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.tipos()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.pessoa + '/seguro/tipos')
            .respond(200, modelsResponse);

        seguro
            .tipos()
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.seguradoras()', function() {
        var modelsResponse = [{}];
        var tipo = 'TIPO';

        httpBackend
            .expectGET(HOST.pessoa + '/seguro/seguradoras?tipoSeguro=' + tipo)
            .respond(200, modelsResponse);

        seguro
            .seguradoras(tipo)
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });
});