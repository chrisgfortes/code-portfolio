describe('Component: dadosSeguro', function() {

    var $componentController,
        ctrl;

    var tipoSeguros = [{
        valor: 'TIPO_SEGURO', 
        descricao: 'Tipo descrição seguro'
    }];

    var seguradoras = [{
        valor: 'TIPO_SEGURADORA', 
        descricao: 'Tipo descrição seguradora'
    }];

    beforeEach(module('app'));

    beforeEach(inject(function(_$componentController_, _seguro_) {
        $componentController = _$componentController_;
        seguro = _seguro_;

        var bindings = {
            seguro: {}
        };

        spyOn(seguro, 'tipos').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: tipoSeguros }); }
            };
        });

        spyOn(seguro, 'seguradoras').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: seguradoras }); }
            };
        });

        ctrl = $componentController('dadosSeguro', null, bindings);

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o seguro no bindings', function() {
        expect(ctrl.seguro).toBeDefined();
    });

    it('deve chamar seguro.tipos()', function () {
        expect(seguro.tipos).toHaveBeenCalled();
        expect(ctrl.tipoSeguros).toEqual(tipoSeguros);
    });

    it('deve chamar seguro.seguradoras()', function () {
        ctrl.seguro.tipoSeguro = 'TIPO';

        ctrl.$onInit();

        expect(seguro.seguradoras).toHaveBeenCalledWith('TIPO');
        expect(ctrl.seguradoras).toEqual(seguradoras);
    });

    it('deve chamar o evento onSalvar', function () {
        var onSalvarSpy = jasmine.createSpy('onSalvar');

        var bindings = {
            onSalvar: onSalvarSpy
        };

        var ctrl = $componentController('dadosSeguro', null, bindings);

        var seguro = {
            tipoSeguro: 'TIPO_SEGURO',
            seguradora: 'TIPO_SEGURADORA'
        };
        
        ctrl.tipoSeguros = tipoSeguros;
        ctrl.seguradoras = seguradoras;

        ctrl.salvar(seguro);

        var seguroEsperado = {
            seguro: {
                tipoSeguro: 'TIPO_SEGURO',
                seguradora: 'TIPO_SEGURADORA',
                tipoDescricao: 'Tipo descrição seguro',
                seguradoraDescricao: 'Tipo descrição seguradora'
            }
        }

        expect(onSalvarSpy).toHaveBeenCalledWith(seguroEsperado);
    });

    it('deve chamar o evento else no onSalvar', function () {
        var onSalvarSpy = jasmine.createSpy('onSalvar');

        var bindings = {
            onSalvar: onSalvarSpy
        };

        var ctrl = $componentController('dadosSeguro', null, bindings);

        var seguro = {
            tipoSeguro: undefined,
            seguradora: undefined
        };

        ctrl.tipoSeguros = tipoSeguros;
        ctrl.seguradoras = seguradoras;

        ctrl.salvar(seguro);

        var seguroEsperado = {
            seguro: {
                tipoSeguro: 'TIPO_SEGURO',
                seguradora: 'TIPO_SEGURADORA',
                tipoDescricao: 'Tipo descrição seguro',
                seguradoraDescricao: 'Tipo descrição seguradora'
            }
        }

        expect(onSalvarSpy).not.toHaveBeenCalledWith(seguroEsperado);
    });

    it('deve chamar o evento onCancelar', function () {
        var onCancelarSpy = jasmine.createSpy('onCancelar');

        var bindings = {
            onCancelar: onCancelarSpy
        };

        var ctrl = $componentController('dadosSeguro', null, bindings);

        ctrl.cancelar();

        expect(onCancelarSpy).toHaveBeenCalled();
    });
});
    