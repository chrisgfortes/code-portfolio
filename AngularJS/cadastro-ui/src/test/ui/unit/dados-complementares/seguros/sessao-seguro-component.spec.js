describe('Component: sessaoSeguro', function () {

    var $componentController,
        dialogs,
        $q,
        $rootScope,
        ctrl;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _dialogs_, _$q_, _$rootScope_) {
        $componentController = _$componentController_;
        dialogs = _dialogs_;
        $q = _$q_;
        $rootScope = _$rootScope_;

        var bindings = {
            seguros: []
        };

        ctrl = $componentController('sessaoSeguro',
            {
                dialogs: _dialogs_
            }, bindings);

        ctrl.$onInit();
    }));

    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o seguros no bindings', function () {
        expect(ctrl.seguros).toBeDefined();
    });

    it('deve SALVAR', function () {
        var seguro = {
            valorSegurado: 20,
            modoEdicao: false
        };

        ctrl.mostrarSeguroForm = true;

        ctrl.salvar(seguro);

        expect(ctrl.seguros.length).toBe(1);
        expect(ctrl.mostrarSeguroForm).toBe(false);
    });

    it('deve EDITAR', function () {
        var seguro = {
            valorSegurado: 20,
            modoEdicao: true
        };

        var seguroEditado = {
            valorSegurado: 50,
            modoEdicao: true
        };

        var coadijuvante = {
            valorSegurado: 10,
            modoEdicao: false
        };

        ctrl.seguros.push(seguro);
        ctrl.seguros.push(coadijuvante);

        ctrl.mostrarSeguroForm = true;

        ctrl.salvar(seguroEditado);

        expect(ctrl.seguros[0].valorSegurado).toBe(50);
        expect(ctrl.seguros[0].modoEdicao).toBe(false);
        expect(ctrl.seguros[1]).toEqual(coadijuvante);
        expect(ctrl.seguros.length).toBe(2);
        expect(ctrl.mostrarSeguroForm).toBe(false);
    });

    it('deve CANCELAR', function () {
        var seguro = {
            tipoSeguro: 'TIPO_SEGURO',
            modoEdicao: true
        };

        ctrl.mostrarSeguroForm = true;

        ctrl.seguros.push(seguro);

        ctrl.cancelar();

        expect(ctrl.seguros.length).toBe(1);
        expect(ctrl.seguros[0].modoEdicao).toBe(false);
        expect(ctrl.mostrarSeguroForm).toBe(false);
    });

    it('deve NOVO formulário', function () {
        ctrl.mostrarSeguroForm = false;

        ctrl.novo();

        expect(ctrl.mostrarSeguroForm).toBe(true);
    });

    it('deve EDITAR', function () {
        ctrl.mostrarSeguroForm = false;
        ctrl.seguro = null;

        var seguro = {
            valorSegurado: 12,
            modoEdicao: true
        };

        ctrl.editar(seguro);

        expect(ctrl.mostrarSeguroForm).toBe(true);
        expect(ctrl.seguro).toEqual(seguro);
    });

    it('deve REMOVER', function () {
        var deferred = $q.defer();
        deferred.resolve('yes');

        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var seguro = {
            valorSegurado: 12,
            modoEdicao: false,
            modoExclusao: false
        };

        var coadijuvante = {
            valorSegurado: 15,
            modoExclusao: true
        };

        ctrl.seguros.push(seguro);
        ctrl.seguros.push(coadijuvante);

        ctrl.remover(coadijuvante);

        $rootScope.$apply();

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.seguros.length).not.toBe(2);
        expect(ctrl.seguros.length).toBe(1);
        expect(ctrl.seguros[0]).toEqual(seguro);
    });

    it('deve cancelar a REMOVOÇÃO de um seguro', function () {
        var deferred = $q.defer();
        deferred.reject();
        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var seguro = {
            valorSegurado: 12,
            modoEdicao: false,
            modoExclusao: false
        };

        var coadijuvante = {
            valorSegurado: 15,
            modoExclusao: true
        };

        ctrl.seguros.push(seguro);
        ctrl.seguros.push(coadijuvante);

        ctrl.remover(coadijuvante);

        $rootScope.$apply();

        var todosDesabilitados = ctrl.seguros
            .some(function (seguro) {
                return !seguro.modoExclusao;
            });

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.seguros.length).toBe(2);
        expect(todosDesabilitados).toBe(true);
    });
});
