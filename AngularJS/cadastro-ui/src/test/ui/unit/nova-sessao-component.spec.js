describe('Componente NOVA SESSAO', function () {

    var $componentController,
        ctrl;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_) {
        $componentController = _$componentController_;

        ctrl = $componentController('novaSessao');

        ctrl.$onInit();
    }));

    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o titulo e o mostraSessao no bindings', function () {
        expect(ctrl.titulo).toBeDefined();
        expect(ctrl.mostraSessao).toBeUndefined();
    });

    it('por padrão não deve mostrar a sessao', function () {
        expect(ctrl.mostraSessao).toBe(undefined);
    });

     it('deve mostrar a sessao', function () {
        ctrl.mostrar();

        expect(ctrl.mostraSessao).toBe(true);
    });
});
