describe('Service: rendaConverter', function () {
    
    var rendaConverter;
    var moment;
    
    beforeEach(module('app'));
    
    beforeEach(inject(function (_rendaConverter_, _moment_) {
        rendaConverter = _rendaConverter_;
        moment = _moment_;
    }));
    
    it('deve está definido', function () {
        expect(rendaConverter).toBeDefined();
    });
    
    it('deve converter as datas', function() {
        var renda = { data : '2017-05-22', dataAdmissao: '2000-01-01'};
        var rendaEsperada = { data: moment('2017-05-22'), dataAdmissao: moment('2000-01-01')};

        var rendaConvertida = rendaConverter.get(renda);

        expect(renda).toEqual(rendaEsperada);
    });


    it('deve passar as datas', function() {
        var renda = { data : '', dataAdmissao: ''};
        var rendaEsperada = { data: '', dataAdmissao: ''};

        var rendaConvertida = rendaConverter.get(renda);

        expect(renda).toEqual(rendaEsperada);
    });
    
});