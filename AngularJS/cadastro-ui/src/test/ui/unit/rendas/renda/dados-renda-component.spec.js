describe('Component: dadosRenda', function () {

    var $componentController,
        renda,
        ctrl,
        $rootScope;

    var tiposRenda = [{
          "valor":"COMPROVADA",
          "descricao":"Comprovada"
       },{
          "valor":"DECLARADA",
          "descricao":"Declarada"
       },{
          "valor":"FUNCIONARIO_PUBLICO",
          "descricao":"Funcionário Público"
       }];

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _renda_, _$rootScope_) {
        $componentController = _$componentController_;
        renda = _renda_;
        $rootScope = _$rootScope_;

        spyOn(renda, 'tipos').and.callFake(function () {
            return {
                then: function (callback) { return callback( tiposRenda ); }
            };
        });
        spyOn(renda, 'comprovacoes').and.callFake(respostaFake([{}]));
        spyOn(renda, 'buscarFontePagadora').and.callFake(respostaFake([{}]));

        var bindings = {
            renda: {}
        };

        ctrl = $componentController('dadosRenda', null, bindings);

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o renda no bindings', function () {
        expect(ctrl.renda).toBeDefined();
    });

    it('deve chamar renda.tipos', function () {
        expect(renda.tipos).toHaveBeenCalled();
        expect(ctrl.tipos.length).toBe(3);
    });

    it('deve chamar renda.comprovacoes', function () {
        expect(renda.comprovacoes).toHaveBeenCalled();
        expect(ctrl.comprovacoes.length).toBe(1);
    });

    it('deve definir como nova renda', function () {
        ctrl.isNova = false;
        ctrl.renda.modoEdicao = false;

        ctrl.$postLink();

        expect(ctrl.isNova).toBe(true);
    });

    it('deve extrair e definir o CPF pelo número', function () {
        var cpf = '37751036516';
        ctrl.renda.modoEdicao = true;
        ctrl.renda.fontePagadora = { cpfCnpj: cpf }

        ctrl.$postLink();

        expect(ctrl.renda.fontePagadora.tipo).toBe('cpf');
        expect(ctrl.renda.fontePagadora.cpf).toBe(cpf);
    });

    it('deve extrair e definir o CNPJ pelo número', function () {
        var cnpj = '20838194000106';
        ctrl.renda.modoEdicao = true;
        ctrl.renda.fontePagadora = { cpfCnpj: cnpj }

        ctrl.$postLink();

        expect(ctrl.renda.fontePagadora.tipo).toBe('cnpj');
        expect(ctrl.renda.fontePagadora.cnpj).toBe(cnpj);
    });

    it('deve extrair e definir o tipo com SEM CNPJ', function () {
        ctrl.renda.modoEdicao = true;
        ctrl.renda.fontePagadora = { cpfCnpj: '13' }

        ctrl.$postLink();

        expect(ctrl.renda.fontePagadora.tipo).toBe('semCnpj');
        expect(ctrl.renda.fontePagadora.cpf).not.toBeDefined();
        expect(ctrl.renda.fontePagadora.cnpj).not.toBeDefined();
    });

    it('deve chamar renda.buscarFontePagadora', function () {
        var query = 'test';
        ctrl.renda = {fontePagadora: {tipo : 'cnpj'}};

        ctrl.buscarFontePagadora(query);

        expect(renda.buscarFontePagadora).toHaveBeenCalledWith(query, ctrl.tiposFontePagadora['cnpj']);
        expect(ctrl.fontesPagadoras.length).toBe(1);
    });

    it('não deve chamar renda.buscarFontePagadora', function () {
        var query = undefined;

        ctrl.buscarFontePagadora(query);

        expect(renda.buscarFontePagadora).not.toHaveBeenCalled();
    });

    it('deve definir fonte pagadora como nova e setar valor padrão', function () {
        spyOn($rootScope, '$broadcast').and.callThrough();
        ctrl.isNova = false;
        var fontePagadora = undefined;

        ctrl.fontePagadoraSelecionada(fontePagadora);

        expect($rootScope.$broadcast).toHaveBeenCalledWith('nova-fonte-pagadora');
    });

    it('deve definir fonte pagadora como nova e setar valor selecionado', function () {
        ctrl.isNova = false;
        var fontePagadora = { id: 0, cpfCnpj: '08094498625' };

        ctrl.fontePagadoraSelecionada(fontePagadora);

        expect(ctrl.isNova).toBe(true);
        expect(ctrl.renda.fontePagadora.id).toBe(0);
    });


    it('não deve definir fonte pagadora como nova e setar valor selecionado', function () {
        ctrl.isNova = false;
        var fontePagadora = { id: 7, cpfCnpj: '08094498625' };

        ctrl.fontePagadoraSelecionada(fontePagadora);

        expect(ctrl.isNova).toBe(false);
        expect(ctrl.renda.fontePagadora.id).toBe(7);
    });

    it('não deve definir como nova renda', function () {
        ctrl.isNova = false;
        ctrl.renda.modoEdicao = true;
        ctrl.renda.fontePagadora = { cpfCnpj: undefined };

        ctrl.$postLink();

        expect(ctrl.isNova).toBe(false);
    });

    it('deve atribuir o CPF para cpfCnpj', function () {
        var cpf = 26139411548;
        var renda = { tipo:'COMPROVADA' ,fontePagadora: { tipo: 'cpf', cpf: cpf } };

        var onSalvarSpy = jasmine.createSpy('onSalvar');

        var bindings = {
            onSalvar: onSalvarSpy
        };

        var ctrl = $componentController('dadosRenda', null, bindings);
        ctrl.$onInit();

        ctrl.salvar(renda);

        expect(renda.fontePagadora.cpfCnpj).toBe(cpf);
    });

    it('deve atribuir o CNPJ para cpfCnpj', function () {
        var cnpj = 55754534000103;
        var renda = { tipo:'COMPROVADA' ,fontePagadora: { tipo: 'cnpj', cnpj: cnpj } };

        var onSalvarSpy = jasmine.createSpy('onSalvar');

        var bindings = {
            onSalvar: onSalvarSpy
        };

        var ctrl = $componentController('dadosRenda', null, bindings);
        ctrl.$onInit();

        ctrl.salvar(renda);

        expect(renda.fontePagadora.cpfCnpj).toBe(cnpj);
    });

    it('deve atribuir o SEM CNPJ para cpfCnpj', function () {
        var renda = { tipo:'COMPROVADA', fontePagadora: { tipo: 'semCnpj' } };

        var onSalvarSpy = jasmine.createSpy('onSalvar');

        var bindings = {
            onSalvar: onSalvarSpy
        };

        var ctrl = $componentController('dadosRenda', null, bindings);
        ctrl.$onInit();

        ctrl.salvar(renda);

        expect(renda.fontePagadora.cpfCnpj).toBe('');
    });

    it('deve chamar o evento onSalvar', function () {
        var onSalvarSpy = jasmine.createSpy('onSalvar');

        var bindings = {
            onSalvar: onSalvarSpy
        };

        var ctrl = $componentController('dadosRenda', null, bindings);

        var renda = { tipo:'COMPROVADA', fontePagadora: { tipo: '' } };
        ctrl.$onInit();

        ctrl.salvar(renda);

        expect(onSalvarSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onCancelar', function () {
        var onCancelarSpy = jasmine.createSpy('onCancelar');

        var bindings = {
            onCancelar: onCancelarSpy
        };

        var ctrl = $componentController('dadosRenda', null, bindings);

        ctrl.cancelar();

        expect(onCancelarSpy).toHaveBeenCalled();
    });

    it('deve aceitar DATA DE ADMISSÃO válida', function () {
        var dataAdmissao = '01/01/2000';
        ctrl.renda.dataAdmissao = dataAdmissao;

        ctrl.validarData('dataAdmissao', dataAdmissao);

        expect(ctrl.renda.dataAdmissao).toBe(dataAdmissao);
    });

    it('não deve aceitar DATA DE ADMISSÃO inválida', function () {
        ctrl.renda.dataAdmissao = '52/34/2000';

        ctrl.validarData('dataAdmissao', ctrl.renda.dataAdmissao);

        expect(ctrl.renda.dataAdmissao).toBeUndefined();
    });

    it('deve aceitar DATA DA RENDA válida', function () {
        var data = '01/01/2000';
        ctrl.renda.data = data;

        ctrl.validarData('data', data);

        expect(ctrl.renda.data).toBe(data);
    });

    it('não deve aceitar DATA DA RENDA inválida', function () {
        ctrl.renda.data = '52/34/2000';

        ctrl.validarData('data', ctrl.renda.data);

        expect(ctrl.renda.data).toBeUndefined();
    });
});
