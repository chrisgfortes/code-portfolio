describe('Component: listaRenda', function () {

    var $componentController,
        ctrl;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_) {
        $componentController = _$componentController_;

        var bindings = {
            rendas: []
        };

        ctrl = $componentController('listaRenda', null, bindings);

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o rendas no bindings', function () {
        expect(ctrl.rendas).toBeDefined();
        expect(ctrl.rendas.length).toBe(0);
    });

    it('deve chamar o evento onNovo', function () {
        var onNovoSpy = jasmine.createSpy('onNovo');

        var bindings = {
            onNovo: onNovoSpy
        };

        var ctrl = $componentController('listaRenda', null, bindings);

        ctrl.novo();

        expect(onNovoSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onRemover', function () {
        var onRemoverSpy = jasmine.createSpy('onRemover');

        var bindings = {
            onRemover: onRemoverSpy
        };

        var ctrl = $componentController('listaRenda', null, bindings);

        var renda = {
            modoExclusao: true
        };

        ctrl.remover(renda);

        expect(onRemoverSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onEditar', function () {
        var onEditarSpy = jasmine.createSpy('onEditar');

        var bindings = {
            onEditar: onEditarSpy
        };

        var ctrl = $componentController('listaRenda', null, bindings);

        ctrl.rendas = [{ modoEdicao: false }];

        var renda = {
            modoEdicao: true
        };

        ctrl.editar(renda);

        var rendaEsperada = ctrl.rendas[0];

        expect(onEditarSpy).toHaveBeenCalled();
    });
});
