describe('factory: renda Geral', function () {

    var renda,
        moment,
        HOST,
        httpBackend;

    beforeEach(module('app'));

    beforeEach(inject(function ($httpBackend, _renda_, _moment_, _HOST_) {
        renda = _renda_;
        moment = _moment_;
        HOST = _HOST_;

        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));

    afterEach(function () {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.folhas()', function () {
        var rendaResponse = [{}];

        httpBackend
            .expectGET(HOST.pessoa + '/renda/folhas-layouts')
            .respond(200, rendaResponse);

        renda
            .folhas()
            .then(function (response) {
                var dados = response;
                expect(dados).toEqual(rendaResponse);
            });

        httpBackend.flush();
    });

    it('.ramos()', function () {
        var rendaResponse = [{}];

        httpBackend
            .expectGET(HOST.pessoa + '/renda/ramos')
            .respond(200, rendaResponse);

        renda
            .ramos()
            .then(function (response) {
                var dados = response;
                expect(dados).toEqual(rendaResponse);
            });

        httpBackend.flush();
    });

    it('.tipos()', function () {
        var rendaResponse = [{}];

        httpBackend
            .expectGET(HOST.pessoa + '/renda/tipos')
            .respond(200, rendaResponse);

        renda
            .tipos()
            .then(function (response) {
                var dados = response;
                expect(dados).toEqual(rendaResponse);
            });

        httpBackend.flush();
    });

    it('.comprovacoes()', function () {
        var rendaResponse = [{}];

        httpBackend
            .expectGET(HOST.pessoa + '/renda/comprovacoes')
            .respond(200, rendaResponse);

        renda
            .comprovacoes()
            .then(function (response) {
                var dados = response;
                expect(dados).toEqual(rendaResponse);
            });

        httpBackend.flush();
    });

    it('.tiposControle()', function () {
        var rendaResponse = [{}];

        httpBackend
            .expectGET(HOST.pessoa + '/renda/tipos-controle')
            .respond(200, rendaResponse);

        renda
            .tiposControle()
            .then(function (response) {
                var dados = response;
                expect(dados).toEqual(rendaResponse);
            });

        httpBackend.flush();
    });

    it('.buscarFontePagadora(query,tipoFontePagadora)', function () {
        var query = 'rendimento';
        var tipoFontePagadora = 2;
        var rendaResponse = [{}];

        httpBackend
            .expectGET(HOST.pessoa + '/renda/fonte-pagadora?query=' + query + '&tipoFontePagadora=' + tipoFontePagadora)
            .respond(200, rendaResponse);

        renda
            .buscarFontePagadora(query, tipoFontePagadora)
            .then(function (response) {
                var dados = response;
                expect(dados).toEqual(rendaResponse);
            });

        httpBackend.flush();
    });
});
