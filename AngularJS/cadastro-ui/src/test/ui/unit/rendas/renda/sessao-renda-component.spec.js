describe('Component: sessaoRenda', function () {

    var $componentController,
        dialogs,
        $q,
        $rootScope,
        ctrl,
        renda,
        moment;

    var tiposRenda = [{
          "valor":"COMPROVADA",
          "descricao":"Comprovada"
       },{
          "valor":"DECLARADA",
          "descricao":"Declarada"
       },{
          "valor":"FUNCIONARIO_PUBLICO",
          "descricao":"Funcionário Público"
       }];

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _dialogs_, _$q_, _$rootScope_, _renda_, _moment_) {
        $componentController = _$componentController_;
        dialogs = _dialogs_;
        $q = _$q_;
        $rootScope = _$rootScope_;
        renda = _renda_;
        moment = _moment_;

        spyOn(renda, 'tipos').and.callFake(respostaFake(tiposRenda));

        var bindings = {
            rendas: []
        };

        ctrl = $componentController('sessaoRenda',
            {
                dialogs: _dialogs_
            }, bindings);

        ctrl.$onInit();
    }));

    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o rendas no bindings', function () {
        expect(ctrl.rendas).toBeDefined();
    });

    it('deve definir valor padrão de rendas no bindings', function () {
        ctrl.rendasObj = undefined;

        ctrl.$onInit();

        expect(ctrl.rendasObj.rendas).toBeDefined();
        expect(ctrl.rendasObj.rendas).toEqual([]);
    });

    it('deve formatar descricaoTipoDeRenda no init', function () {
        var rendasObj = {
            rendas: [{
                "id":null,
                "fontePagadora":{
                    "id":4769,
                    "cpfCnpj":"05570184000149",
                    "nome":"Empresa4769                             ",
                    "folhaLayout":null,
                    "creditoConsignado":false,
                    "tipoControle":"PRIVADO",
                    "ramo":"COMERCIO",
                    "entidadeCooperativa":false
                },
                "dataAdmissao":"2014-12-01",
                "remuneracao":2470.00,
                "data":"2017-02-28",
                "tipo":"FUNCIONARIO_PUBLICO",
                "comprovacao":null
            }],
            dataAtualizacaoRenda: moment()
        }

        var bindings = { rendasObj: rendasObj };
        ctrl = $componentController('sessaoRenda', null, bindings);
        ctrl.$onInit();

        expect(ctrl.rendasObj.rendas[0].tipo).toEqual('FUNCIONARIO_PUBLICO');
        expect(ctrl.rendasObj.rendas[0].descricaoTipoDeRenda).toEqual('Funcionário Público');
    });

    it('deve SALVAR', function () {
        var remuneracao = 10000;

        var renda = {
            cpf: 'teste',
            remuneracao: remuneracao,
            modoEdicao: false
        };
        ctrl.mostrarRendaForm = true;

        ctrl.salvar(renda);

        expect(ctrl.rendasObj.rendas.length).toBe(1);
        expect(ctrl.totalRendas).toBe(remuneracao);
        expect(ctrl.mostrarRendaForm).toBe(false);
    });

    it('deve EDITAR', function () {
        var renda = {
            cpf: 'teste',
            remuneracao: 10000,
            modoEdicao: true
        };

        var rendaEditada = {
            cpf: 'teste alterado',
            remuneracao: 5000,
            modoEdicao: true
        };

        var coadijuvante = {
            cpf: 'coadijuvante',
            remuneracao: 10000,
            modoEdicao: false
        };

        ctrl.rendasObj.rendas.push(renda);
        ctrl.rendasObj.rendas.push(coadijuvante);

        ctrl.mostrarRendaForm = true;

        ctrl.salvar(rendaEditada);

        expect(ctrl.rendasObj.rendas[0].cpf).toBe('teste alterado');
        expect(ctrl.rendasObj.rendas[0].modoEdicao).toBe(false);
        expect(ctrl.rendasObj.rendas[1]).toEqual(coadijuvante);
        expect(ctrl.rendasObj.rendas.length).toBe(2);
        expect(ctrl.totalRendas).toBe(15000);
        expect(ctrl.mostrarRendaForm).toBe(false);
    });

    it('deve CANCELAR nova/edição', function () {
        var renda = {
            cpf: 'teste',
            modoEdicao: true
        };
        ctrl.mostrarRendaForm = true;

        ctrl.rendasObj.rendas.push(renda);

        ctrl.cancelar();

        expect(ctrl.rendasObj.rendas.length).toBe(1);
        expect(ctrl.rendasObj.rendas[0].modoEdicao).toBe(false);
        expect(ctrl.mostrarRendaForm).toBe(false);
    });

    it('deve NOVO formulário', function () {
        ctrl.mostrarRendaForm = false;

        ctrl.novo();

        expect(ctrl.mostrarRendaForm).toBe(true);
    });

    it('deve ficar em MODO EDIÇÃO', function () {
        ctrl.mostrarRendaForm = false;
        ctrl.renda = null;

        var renda = {
            cpf: 'teste',
            modoEdicao: true
        };

        ctrl.editar(renda);

        expect(ctrl.mostrarRendaForm).toBe(true);
        expect(ctrl.renda).toEqual(renda);
    });

    it('deve REMOVER', function () {
        var deferred = $q.defer();
        deferred.resolve('yes');
        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var renda = {
            cpf: 'teste',
            remuneracao: 10000,
            modoEdicao: false,
            modoExclusao: false
        };

        var coadijuvante = {
            cpf: 'coadijuvante',
            remuneracao: 5000,
            modoExclusao: true
        };

        ctrl.rendasObj.rendas.push(renda);
        ctrl.rendasObj.rendas.push(coadijuvante);

        ctrl.remover(coadijuvante);

        $rootScope.$apply();

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.rendasObj.rendas.length).toBe(1);
        expect(ctrl.totalRendas).toBe(10000);
        expect(ctrl.rendasObj.rendas[0]).toEqual(renda);
    });

    it('deve CANCELAR remoção', function () {
        var deferred = $q.defer();
        deferred.reject();
        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var renda = {
            cpf: 'teste',
            modoEdicao: false,
            modoExclusao: false
        };

        var coadijuvante = {
            cpf: 'coadijuvante',
            modoExclusao: true
        };

        ctrl.rendasObj.rendas.push(renda);
        ctrl.rendasObj.rendas.push(coadijuvante);

        ctrl.remover(coadijuvante);

        $rootScope.$apply();

        var todosDesabilitados = ctrl.rendasObj.rendas
            .some(function (renda) {
                return !renda.modoExclusao;
            });

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.rendasObj.rendas.length).toBe(2);
        expect(todosDesabilitados).toBe(true);
    });

    it('deve validar e alterar data de atualização da renda', function () {
        var dataAtual = moment();
        ctrl.rendasObj = {
            dataAtualizacaoRenda: dataAtual
        }

        ctrl.validarData();

        expect(ctrl.rendasObj.dataAtualizacaoRenda).toEqual(dataAtual)
    });

    it('deve limpar data de atualização se a mesma for inválida', function () {
        ctrl.rendasObj = {
            dataAtualizacaoRenda: moment('9999-99-99')
        }

        ctrl.validarData();

        expect(ctrl.rendasObj.dataAtualizacaoRenda).toBeNull();
    });

});
