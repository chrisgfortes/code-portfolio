describe('Component: dadosFontePagadora', function () {

    var $componentController,
        renda,
        ctrl,
        $rootScope;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _renda_, _$rootScope_) {
        $componentController = _$componentController_;
        renda = _renda_;
        $rootScope = _$rootScope_;

        spyOn(renda, 'folhas').and.callFake(respostaFake([{}]));
        spyOn(renda, 'ramos').and.callFake(respostaFake([{}]));
        spyOn(renda, 'tiposControle').and.callFake(respostaFake([{}]));

        var bindings = {
            fontePagadora: {}
        };

        ctrl = $componentController('dadosFontePagadora', null, bindings);

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o fontePagadora no bindings', function () {
        expect(ctrl.fontePagadora).toBeDefined();
    });

    it('deve chamar renda.folhas', function () {
        expect(renda.folhas).toHaveBeenCalled();
        expect(ctrl.folhas.length).toBe(1);
    });

    it('deve chamar renda.ramos', function () {
        expect(renda.ramos).toHaveBeenCalled();
        expect(ctrl.ramos.length).toBe(1);
    });

    it('deve chamar renda.tiposControle', function () {
        expect(renda.tiposControle).toHaveBeenCalled();
        expect(ctrl.tiposControle.length).toBe(1);
    });

    it('deve definir por padrão o fontePagadora.tipo=cnpj ', function () {
        ctrl.fontePagadora = {};

        ctrl.$postLink();

        expect(ctrl.fontePagadora.tipo).toBe('cnpj');
    });

    it('deve definir o tipo: CNPJ, da fonte pagadora', function () {

        ctrl.alterarTipo('cnpj');

        expect(ctrl.isOutros).toBe(false);
        expect(ctrl.requeridoCPF).toBe(false);
        expect(ctrl.requeridoCNPJ).toBe(true);
    });

    it('deve definir o tipo: CPF, da fonte pagadora', function () {

        ctrl.alterarTipo('cpf');

        expect(ctrl.isOutros).toBe(false);
        expect(ctrl.requeridoCPF).toBe(true);
        expect(ctrl.requeridoCNPJ).toBe(false);
    });

    it('deve definir o tipo: SEM CNPJ, da fonte pagadora', function () {

        ctrl.alterarTipo('semCnpj');

        expect(ctrl.isOutros).toBe(true);
        expect(ctrl.requeridoCPF).toBe(false);
        expect(ctrl.requeridoCNPJ).toBe(false);
    });

    it('deve configurar o estilo para aumentar o tamanho, do NOME DA FONTE PAGADORA', function () {

        var configuracao = ctrl.configClassNomeEmpresa(false);

        expect(configuracao['col-md-12']).toBe(false);
        expect(configuracao['col-md-6']).toBe(true);
        expect(configuracao['col-sm-12']).toBe(false);
        expect(configuracao['col-sm-6']).toBe(true);
        expect(configuracao['col-xs-12']).toBe(true);
    });

    it('deve configurar o estilo padrão, do NOME DA FONTE PAGADORA', function () {

        var configuracao = ctrl.configClassNomeEmpresa(true);

        expect(configuracao['col-md-12']).toBe(true);
        expect(configuracao['col-md-6']).toBe(false);
        expect(configuracao['col-sm-12']).toBe(true);
        expect(configuracao['col-sm-6']).toBe(false);
        expect(configuracao['col-xs-12']).toBe(true);
    });

    it('deve manter o valor fontePagadora.tipo já definido ', function () {
        ctrl.fontePagadora.tipo = 'cpf';

        ctrl.$postLink();

        expect(ctrl.fontePagadora.tipo).toBe('cpf');
    });

    it('deve mostrar empresa e tipo controle', function () {
        ctrl.mostrarEmpresaETipoControle = false;
        var folha = 'OUTROS_NENHUM';

        ctrl.alterarFolha(folha);
        expect(ctrl.mostrarEmpresaETipoControle).toBe(true);

        ctrl.mostrarEmpresaETipoControle = false;
        folha = 'UNIMED';

        ctrl.alterarFolha(folha);
        expect(ctrl.mostrarEmpresaETipoControle).toBe(true);

        ctrl.mostrarEmpresaETipoControle = false;
        folha = 'USIMED';

        ctrl.alterarFolha(folha);
        expect(ctrl.mostrarEmpresaETipoControle).toBe(true);

    });

    it('não deve mostrar empresa e tipo controle', function () {
        ctrl.mostrarEmpresaETipoControle = true;
        var folha = undefined;

        ctrl.alterarFolha(folha);
        expect(ctrl.mostrarEmpresaETipoControle).toBe(false);
    });

    it('deve escutar o evento "nova-fonte-pagadora"', function () {
        spyOn($rootScope, '$broadcast').and.callThrough();
        spyOn($rootScope, '$on').and.callThrough();

        $rootScope.$broadcast('nova-fonte-pagadora');

        expect($rootScope.$broadcast).toHaveBeenCalledWith('nova-fonte-pagadora');
        expect(ctrl.isNova).toBe(true);
        expect(ctrl.fontePagadora).toEqual({ tipo: 'cnpj'});
    });

});
