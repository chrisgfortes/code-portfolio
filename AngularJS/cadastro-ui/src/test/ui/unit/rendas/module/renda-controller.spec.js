describe('controller: rendasController', function () {

    var ctrl, $rootScope, rendaFactory, $q, urls;

    var cpf = '12345678912';
    var erros = [{}];
    var renda = {}

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_$controller_, _$rootScope_, _rendas_, _$q_, _linksFactory_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        rendaFactory = _rendas_('terceiro');
        urls = _linksFactory_('terceiro');
        $q = _$q_;

        var deferred = $q.defer();
        deferred.resolve({
            data: renda
        });

        ctrl = $controller('rendasController', {
            rendaFactory: rendaFactory,
            urls: urls,
            $routeParams: { cpf: '12345678912' },
            rendasInfo: {}
        });
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('deve chamar .salvar(cpf, renda)', function () {
        var deferred = $q.defer();
        deferred.resolve({});

        spyOn(rendaFactory, 'salvar')
            .and
            .returnValue(deferred.promise);

        ctrl.cpf = cpf;

        var renda = {
            cpf: cpf
        };

        ctrl.salvar(renda);

        $rootScope.$apply();

        expect(rendaFactory.salvar).toHaveBeenCalledWith(cpf, renda);
    });

    it('deve mostrar messagem de validação, vinda pelo body', function () {
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                body: erros
            }
        });

        spyOn(rendaFactory, 'salvar')
            .and
            .returnValue(deferred.promise);

        ctrl.cpf = cpf;

        var renda = {
            cpf: cpf
        };

        ctrl.salvar(renda);

        $rootScope.$apply();

        expect(rendaFactory.salvar).toHaveBeenCalledWith(cpf, renda);
        expect(ctrl.erros).toEqual(erros);
    });

    it('deve mostrar messagem de validação, vinda pelo content', function () {
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                content: erros
            }
        });

        spyOn(rendaFactory, 'salvar')
            .and
            .returnValue(deferred.promise);

        ctrl.cpf = cpf;

        var renda = {
            cpf: cpf
        };

        ctrl.salvar(renda);

        $rootScope.$apply();

        expect(rendaFactory.salvar).toHaveBeenCalledWith(cpf, renda);
        expect(ctrl.erros).toEqual(erros);
    });

    it('deve mostrar messagem de validação', function () {
        var deferred = $q.defer();
        deferred.reject({
            status: 400,
            data: {
                content: erros
            }
        });

        spyOn(rendaFactory, 'salvar')
            .and
            .returnValue(deferred.promise);

        ctrl.cpf = cpf;

        var renda = {
            cpf: cpf
        };

        ctrl.salvar(renda);

        $rootScope.$apply();

        expect(rendaFactory.salvar).toHaveBeenCalledWith(cpf, renda);
        expect(ctrl.erros).not.toBeDefined();
    });
});
