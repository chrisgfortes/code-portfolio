describe('Router: renda', function () {

    var rendas,
        $route,
        $injector,
        linksFactory,
        $q,
        rotaService,
        HOST;

    beforeEach(module('app'));

    beforeEach(inject(function (_$route_, _rendas_, _$injector_, _linksFactory_, _HOST_, _$q_, _rotaService_) {
        $route = _$route_;
        rendas = _rendas_;
        $injector = _$injector_;
        linksFactory = _linksFactory_;
        HOST = _HOST_;
        $q = _$q_,
        rotaService = _rotaService_;
    }));

    describe('Rota de Terceiro', function () {
        testarRota('terceiro');
    });

    function testarRota(nomeDaRota) {
        it('Deve testar renda-router', function() {
            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/renda'];

            expect(rota.controller).toBe('rendasController');
            expect(rota.templateUrl).toBe('./modules/rendas/rendas.html');
        });

        it('Deve chamar rendas.buscar()', inject(function($httpBackend) {
            $httpBackend.whenGET(/\.html$/).respond('');
            var cpf = '123456789123';

            $httpBackend
                .expectGET(HOST[nomeDaRota] + cpf + '/rascunho/renda')
                .respond(200, {rendas:[]});

            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/renda'];

            $route = {current: { params: { cpf: cpf } } };

            var retorno = rota.resolve.rendasInfo(rendas, $route);
            $httpBackend.flush();
            expect(retorno.$$state.value).toEqual({rendas:[]});
        }));

        it('não encontra Rendas', inject(function($httpBackend) {
            $httpBackend.whenGET(/\.html$/).respond('');
            var cpf = '123456789123';

            $httpBackend
                .expectGET(HOST[nomeDaRota] + cpf + '/rascunho/renda')
                .respond(404, {rendas:[]});

            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/renda'];

            $route = {current: { params: { cpf: cpf } } };

            var retorno = rota.resolve.rendasInfo(rendas, $route);
            $httpBackend.flush();
            expect(retorno.$$state.value).toEqual({rendas:[]});
        }));

        it('Deve carregar urls', function () {
            var cpf = '123456789123';
            var compare = mapearRetorno(linksFactory(nomeDaRota), cpf);
            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/renda'];

            var retorno = rota.resolve.urls(linksFactory);
            retorno = mapearRetorno(retorno, cpf);

            expect(retorno).toEqual(compare);
        });

        it('Deve definir a factory', function () {
            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/renda'];

            var factory = rendas(nomeDaRota);
            var retorno = rota.resolve.rendaFactory(rendas);

            expect(Object.getOwnPropertyNames(retorno)).toEqual(Object.getOwnPropertyNames(factory));
        });

        it('deve verificar se é terceiro', function() {
            var cpf = '93994468248';

            var deferred = $q.defer();
            deferred.resolve({});
            spyOn(rotaService, 'isTerceiro').and.returnValue(deferred.promise);

            var rota = $route.routes['/cadastro/'+ nomeDaRota +'/:cpf/renda'];
            $route = {
                current: {
                    params: {
                        cpf: cpf
                    }
                }
            }

            rota.resolve.isTerceiro($route, rotaService);
            expect(rotaService.isTerceiro).toHaveBeenCalledWith(cpf);
        });
    }

    function mapearRetorno(urls, cpf) {
        urls = urls.map(function (item) {
            item.url = item.url(cpf);
            return item
        })
        return urls;
    }
});
