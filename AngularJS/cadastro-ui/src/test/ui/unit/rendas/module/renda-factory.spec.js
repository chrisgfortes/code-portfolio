describe('factory: renda', function () {

    var renda,
        moment,
        HOST;

    beforeEach(module('app'));

    beforeEach(inject(function (_rendas_, _moment_, _HOST_) {
        renda = _rendas_('terceiro');
        moment = _moment_;
        HOST = _HOST_;
    }));

    it('.buscar(cpf)', inject(function($httpBackend) {
        $httpBackend.whenGET(/\.html$/).respond('');
        var cpf = '83084412545';
        var dataAtual = moment()
        var dadosFake = rendaFake(dataAtual);
        var rendaResponse = rendaPost(dataAtual);

        $httpBackend
            .expectGET(HOST.terceiro + cpf + '/rascunho/renda')
            .respond(200, dadosFake);

        renda
            .buscar(cpf)
            .then(function (response) {
                var dados = response;
                expect(dados).toEqual(rendaResponse);
            });

        $httpBackend.flush();
    }));

    it('.salvar(cpf, renda)', inject(function($httpBackend) {
        var cpf = '83084412545';
        var dataAtual = moment()
        var dadosFake = rendaFake(dataAtual);
        var rendaResponse = rendaPost(dataAtual);

        $httpBackend
            .expectPUT(HOST.terceiro + cpf + '/rascunho/renda')
            .respond(201, dadosFake);

        renda
            .salvar(cpf, rendaResponse)
            .then(function (response) {
                var dados = response.data;

                expect(dados).toEqual(dadosFake);
            });

        $httpBackend.flush();
    }));

    function rendaFake(dataAtual) {
        return {
            dataAtualizacaoRenda: dataAtual,
            rendas: [
                {
                    data: '2017-05-01',
                    dataAdmissao: '2000-05-01'
                }
            ]}
        }

        function rendaPost(dataAtual) {
            return {
                dataAtualizacaoRenda:dataAtual,
                rendas: [
                    {
                        data: moment('2017-05-01'),
                        dataAdmissao: moment('2000-05-01')
                    }
                ]}
            }
});
