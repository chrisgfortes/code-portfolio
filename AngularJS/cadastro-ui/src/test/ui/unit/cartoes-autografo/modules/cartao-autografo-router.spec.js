describe('Router: cartao autografo', function () {

    var cartaoAutografo,
        $route,
        $q,
        $rootScope,
        HOST,
        rotaService,
        linksFactory;

    beforeEach(module('app'));

    beforeEach(inject(function (_$route_, _cartaoAutografoFactory_, _$rootScope_, _$q_, _HOST_, _linksFactory_, _rotaService_) {
        $route = _$route_;
        cartaoAutografo = _cartaoAutografoFactory_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        HOST = _HOST_;
        linksFactory = _linksFactory_;
        rotaService = _rotaService_;
    }));

    describe('Rota de Terceiro', function () {
        testarRota('terceiro');
    });

    function testarRota(nomeDaRota) {

        it('Deve testar cartao autografo', function () {
            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/cartao-autografo'];

            expect(rota.controller).toBe('cartoesAutografoController');
            expect(rota.templateUrl).toBe('./modules/cartoes-autografo/cartoes-autografo.html');
        });

        it('Deve chamar cartaoAutografo.buscar()', inject(function($httpBackend) {
            $httpBackend.whenGET(/\.html$/).respond('');

            var cpf = '123456789123';

            $httpBackend
                .expectGET(HOST[nomeDaRota] + cpf + '/rascunho/cartaoautografo')
                .respond(200, [{}]);

            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/cartao-autografo'];
            $route.current = {
                params: {
                    cpf: cpf
                }
            };

            var retorno = rota.resolve.cartoesAutografoInfo(cartaoAutografo, $route, HOST);
            $httpBackend.flush();
            expect(retorno.$$state.value.data).toEqual([{}]);
        }));

        it('não encontra o cartão autografo', inject(function($httpBackend) {
            $httpBackend.whenGET(/\.html$/).respond('');

            var cpf = '123456789123';

            $httpBackend
                .expectGET(HOST[nomeDaRota] + cpf + '/rascunho/cartaoautografo')
                .respond(404, []);

            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/cartao-autografo'];
            $route.current = {
                params: {
                    cpf: cpf
                }
            };

            var retorno = rota.resolve.cartoesAutografoInfo(cartaoAutografo, $route, HOST);
            $httpBackend.flush();
            expect(retorno.$$state.value.data).toEqual([]);
        }));

        it('Deve carregar urls', function () {
            var cpf = '123456789123';
            var compare = mapearRetorno(linksFactory(nomeDaRota), cpf);
            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/cartao-autografo'];

            var retorno = rota.resolve.urls(linksFactory);
            retorno = mapearRetorno(retorno, cpf);

            expect(retorno).toEqual(compare);
        });

        it('Deve definir a factory', function () {
            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/cartao-autografo'];

            var retorno = rota.resolve.hostFactory(HOST);

            expect(retorno).toEqual(HOST[nomeDaRota]);
        });

        it('deve verificar se é terceiro', function() {
            var cpf = '93994468248';

            var deferred = $q.defer();
            deferred.resolve({});
            spyOn(rotaService, 'isTerceiro').and.returnValue(deferred.promise);

            var rota = $route.routes['/cadastro/'+ nomeDaRota +'/:cpf/cartao-autografo'];
            $route = {
                current: {
                    params: {
                        cpf: cpf
                    }
                }
            }

            rota.resolve.isTerceiro($route, rotaService);
            expect(rotaService.isTerceiro).toHaveBeenCalledWith(cpf);
        });

        function mapearRetorno(urls, cpf) {
            urls = urls.map(function (item) {
                item.url = item.url(cpf);
                return item
            })
            return urls;
        }
    }
});
