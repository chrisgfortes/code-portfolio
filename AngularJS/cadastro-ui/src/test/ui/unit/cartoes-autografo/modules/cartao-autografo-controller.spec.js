describe('controller: cartoesAutografoController', function () {

    var ctrl, $rootScope, cartaoAutografoFactory, $q, linksFactory, HOST;

    var cpf = '12345678912';
    var erros = [{}];
    var cartaoAutografo = {}

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_$controller_, _$rootScope_, _$q_, _linksFactory_, _HOST_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        urls = _linksFactory_('terceiro');
        HOST = _HOST_;

        var deferred = $q.defer();
        deferred.resolve({
            data: cartaoAutografo
        });

        ctrl = $controller('cartoesAutografoController', {
            $routeParams: { cpf: '12345678912' },
            cartoesAutografoInfo: [],
            urls: urls,
            hostFactory: HOST.terceiro
        });
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('deve setar a variável salvoComSucesso para true', function () {
        ctrl.salvoComSucesso = false;
        ctrl.feedbackSucesso(true, "Salvo com sucesso!");
        expect(ctrl.salvoComSucesso).toBe(true);
    });

    it('deve setar a variável salvoComSucesso para false', function () {
        ctrl.salvoComSucesso = true;
        ctrl.feedbackSucesso(false, "Falha ao salvar!");
        expect(ctrl.salvoComSucesso).toBe(false);
    });
});
