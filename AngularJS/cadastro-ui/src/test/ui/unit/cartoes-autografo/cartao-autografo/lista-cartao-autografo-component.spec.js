describe('Component: listaCartaoAutografo', function () {

    var $componentController,
        ctrl;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_) {
        $componentController = _$componentController_;

        var bindings = {
        };

        ctrl = $componentController('listaCartaoAutografo', null, bindings);

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o cartoesAutografo no bindings', function () {
        expect(ctrl.cartoesAutografo).toBeDefined();
        expect(ctrl.cartoesAutografo.length).toBe(0);
    });

    it('deve formatar a descricao do cartão', function () {
        var nome = 'João da Silva';
        var descricao = 'Descricao';

        ctrl.cartoesAutografo = [{
            pessoa: {
                nome: nome,
                tipo: {
                    descricao: descricao
                }
            }
        }];

        ctrl.$onInit();

        expect(ctrl.cartoesAutografo[0].pessoa.descricao).toBe(descricao + ' - ' + nome);
        expect(ctrl.cartoesAutografo.length).toBe(1);
    });

    it('deve formatar a descricao(sem nome) do cartão', function () {
        var nome = undefined;
        var descricao = 'Descricao';

        ctrl.cartoesAutografo = [{
            pessoa: {
                nome: nome,
                tipo: {
                    descricao: descricao
                }
            }
        }];

        ctrl.$onInit();

        expect(ctrl.cartoesAutografo[0].pessoa.descricao).toBe(descricao + ' - (não informado)');
        expect(ctrl.cartoesAutografo.length).toBe(1);
    });

    it('deve chamar o evento onNovo', function () {
        var onNovoSpy = jasmine.createSpy('onNovo');

        var bindings = {
            onNovo: onNovoSpy
        };

        var ctrl = $componentController('listaCartaoAutografo', null, bindings);

        ctrl.novo();

        expect(onNovoSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onRemover', function () {
        var onRemoverSpy = jasmine.createSpy('onRemover');

        var bindings = {
            onRemover: onRemoverSpy
        };

        var ctrl = $componentController('listaCartaoAutografo', null, bindings);

        var cartaoAutografo = {
            modoExclusao: true
        };

        ctrl.remover(cartaoAutografo);

        expect(onRemoverSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onEditar', inject(function (cartaoAutografoFactory, $q, $rootScope) {

        var onEditarSpy = jasmine.createSpy('onEditar');

        var bindings = {
            onEditar: onEditarSpy,
            cartaoAutografo: cartaoAutografoFactory
        };

        var cpf = '12345678911';

        var ctrl = $componentController('listaCartaoAutografo', {
            $routeParams: { cpf: cpf }
        }, bindings);

        ctrl.$onInit();

        var deferred = $q.defer();
        deferred.resolve({ data: new Blob});
        spyOn(ctrl.factory, 'buscarImagemAssinatura').and.returnValue(deferred.promise);

        ctrl.cartoesAutografo = [{ modoEdicao: false }];

        var _cartaoAutografo = {
            modoEdicao: true
        };

        ctrl.editar(_cartaoAutografo);

        $rootScope.$apply();

        var cartaoAutografoEsperada = ctrl.cartoesAutografo[0];

        expect(ctrl.factory.buscarImagemAssinatura).toHaveBeenCalledWith(cpf, _cartaoAutografo);
        expect(onEditarSpy).toHaveBeenCalled();
    }));
});
