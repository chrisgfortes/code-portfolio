describe('Component: dadosCartaoAutografo', function() {

  var $componentController,
    $scope,
    cartaoAutografo,
    ctrl,
    $routeParams,
    $q,
    httpBackend,
    cpf = '12345678911',
    HOST;

  beforeEach(module('app'));

  beforeEach(inject(function(_$componentController_, _$rootScope_, _cartaoAutografoFactory_, _$q_, _$routeParams_, _HOST_, $httpBackend) {
    $componentController = _$componentController_;
    $rootScope = _$rootScope_;
    $routeParams = {
      cpf: '12345678911'
    };
    cartaoAutografo = _cartaoAutografoFactory_;
    $q = _$q_;
    HOST = _HOST_;
    httpBackend = $httpBackend;

    $scope = $rootScope.$new();

    var bindings = {
      cartaoAutografo: {
        pessoa: {
            cpfCnpj: cpf
        }
      },
      host: HOST.terceiro
    };

    ctrl = $componentController('dadosCartaoAutografo', {
      $scope: $scope,
      $routeParams: $routeParams
    }, bindings);



  }));


  it('deve estar definido', function() {
    expect($componentController).toBeDefined();
  });

  it('deve buscar imagem assinatura', function() {
    var pessoas = {
      data: [{
        descricao: "descricao",
        nome: "nome",
        tipo: {
          descricao: "tipodescricao"
        }
      }]
    };

    var pessoasEsperada = [{
      descricao: "tipodescricao - nome",
      nome: "nome",
      tipo: {
        descricao: "tipodescricao"
      }
    }];

    var assinaturaEsperada = undefined;

    httpBackend
        .expectGET(HOST.terceiro + cpf + '/rascunho/cartaoautografo/' + cpf + '/imagem')
        .respond(200);

    httpBackend
        .expectGET(HOST.terceiro + cpf + '/rascunho/cartaoautografo/pessoa')
        .respond(200, pessoas.data);

    ctrl.$onInit();
    httpBackend.flush();

    expect(ctrl.pessoas).toEqual(pessoasEsperada);
    expect(ctrl.cartaoAutografo.assinatura).toEqual(assinaturaEsperada);
  });

  it('deve converter pessoas', function() {
    var pessoas = {
      data: [{
        descricao: "descricao",
        nome: "nome",
        tipo: {
          descricao: "tipodescricao"
        }
      }]
    };

    var pessoasEsperada = [{
      descricao: "tipodescricao - nome",
      nome: "nome",
      tipo: {
        descricao: "tipodescricao"
      }
    }];

    httpBackend
        .expectGET(HOST.terceiro + cpf + '/rascunho/cartaoautografo/pessoa')
        .respond(200, pessoas.data);

    ctrl.cartaoAutografo.pessoa = undefined;

    ctrl.$onInit();
    $rootScope.$apply();
    httpBackend.flush();

    expect(ctrl.pessoas).toEqual(pessoasEsperada);
  });

  it('deve converter pessoas sem nome informado', function() {
    var pessoas = {
        data: [{
            descricao: "descricao",
            tipo: {
                descricao: "tipodescricao"
            }
        }]
    };

    var pessoasEsperada = [{
        descricao: "tipodescricao - (não informado)",
        nome: "(não informado)",
        tipo: {
            descricao: "tipodescricao"
        }
    }];

    httpBackend
        .expectGET(HOST.terceiro + cpf + '/rascunho/cartaoautografo/pessoa')
        .respond(200, pessoas.data);

    ctrl.cartaoAutografo.pessoa = undefined;

    ctrl.$onInit();

    httpBackend.flush();

    expect(ctrl.pessoas).toEqual(pessoasEsperada);
  });

  it('deve definir o cartaoAutografo no bindings', function() {
    expect(ctrl.cartaoAutografo).toBeDefined();
  });

  it('deve chamar o evento onSalvar', function() {
    var onSalvarSpy = jasmine.createSpy('onSalvar');

    var _cartaoAutografo = {
      pessoa: {},
      assinatura: {},
      poderes: {}
    };

    var bindings = {
      cartaoAutografo: _cartaoAutografo,
      onSalvar: onSalvarSpy
    };

    var ctrl = $componentController('dadosCartaoAutografo', null, bindings);

    ctrl.cartaoAutografo = _cartaoAutografo;
    ctrl.salvar(_cartaoAutografo);

    expect(onSalvarSpy).toHaveBeenCalled();
  });

  it('deve chamar o evento onCancelar', function() {
    var onCancelarSpy = jasmine.createSpy('onCancelar');

    var bindings = {
      onCancelar: onCancelarSpy
    };

    var ctrl = $componentController('dadosCartaoAutografo', null, bindings);

    ctrl.cancelar();

    expect(onCancelarSpy).toHaveBeenCalled();
  });
});
