describe('Component: sessaoCartaoAutografo', function () {

    var $componentController,
        dialogs,
        $q,
        $rootScope,
        cpf = cpfCnpj = '12345678911',
        cartaoAutografo,
        ctrl,
        HOST,
        httpBackend;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _dialogs_, _$q_, _$rootScope_, _cartaoAutografoFactory_, _HOST_, $httpBackend) {
        $componentController = _$componentController_;
        dialogs = _dialogs_;
        $q = _$q_;
        $rootScope = _$rootScope_;
        cartaoAutografo = _cartaoAutografoFactory_;
        HOST = _HOST_;
        httpBackend = $httpBackend;

        var bindings = {
            cartoesAutografo: [],
            host: HOST.terceiro
        };

        ctrl = $componentController('sessaoCartaoAutografo',
            {
                dialogs: _dialogs_,
                cartaoAutografo: _cartaoAutografoFactory_,
                $routeParams: {
                    cpf: cpf
                }
            }, bindings);

    }));

    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o cartoesAutografo no bindings', function () {
        expect(ctrl.cartoesAutografo).toBeDefined();
    });

    it('deve SALVAR', function () {

        httpBackend
            .expectPUT(HOST.terceiro + cpf + '/rascunho/cartaoautografo')
            .respond(201, [{}]);

        var onNotificacaoSpy = jasmine.createSpy('onNotificacao');
        var bindings = {
            cartoesAutografo: [],
            onNotificacao: onNotificacaoSpy,
            host: HOST.terceiro
        }

        ctrl = $componentController('sessaoCartaoAutografo', {
            $routeParams: {
                cpf: cpf
            },
        }, bindings);

        ctrl.$onInit();

        var _cartaoAutografo = {
            nome: 'teste',
            modoEdicao: false,
            poderes: {}
        };

        ctrl.mostrarCartaoAutografoForm = true;

        ctrl.salvar(_cartaoAutografo);

        httpBackend.flush();

        expect(onNotificacaoSpy).toHaveBeenCalledWith({ status: true });
        expect(ctrl.cartoesAutografo.length).toBe(1);
        expect(ctrl.cartoesAutografo[0].poderes).not.toBeDefined();
        expect(ctrl.mostrarCartaoAutografoForm).toBe(false);
    });

    it('deve notificar erro na falha ao SALVAR', function () {
        httpBackend
            .expectPUT(HOST.terceiro + cpf + '/rascunho/cartaoautografo')
            .respond(404, {body:[]});

        var onNotificacaoSpy = jasmine.createSpy('onNotificacao');

        var bindings = {
            cartoesAutografo: [],
            onNotificacao: onNotificacaoSpy,
            host: HOST.terceiro
        }

        ctrl = $componentController('sessaoCartaoAutografo', {
            $routeParams: {
                cpf: cpf
            },
        }, bindings);

        ctrl.$onInit();

        var _cartaoAutografo = {
            nome: 'teste',
            modoEdicao: false
        };

        ctrl.mostrarCartaoAutografoForm = true;

        ctrl.salvar(_cartaoAutografo);

        httpBackend.flush();

        expect(onNotificacaoSpy).toHaveBeenCalledWith({ status: false, mensagem: [] });
        expect(ctrl.cartoesAutografo.length).toBe(0);
        expect(ctrl.mostrarCartaoAutografoForm).toBe(true);
    });

    it('não deve SALVAR se já existe cnpjCpf cadastrado', function () {
        var deferred = $q.defer();
        deferred.resolve({});
        spyOn(dialogs, 'notify').and.returnValue(deferred.promise);

        var _cartaoAutografo = {
            nome: 'teste',
            modoEdicao: false,
            pessoa: { cpfCnpj: '12345678911' }
        };

        ctrl.cartoesAutografo = [{ pessoa: { cpfCnpj: '12345678911' } }];

        ctrl.salvar(_cartaoAutografo);

        expect(dialogs.notify).toHaveBeenCalledWith("Atenção", "Essa pessoa já foi adicionada.");
    });

    it('deve SALVAR IMAGEM', inject(function (Upload) {

        httpBackend
            .expectPUT(HOST.terceiro + cpf + '/rascunho/cartaoautografo')
            .respond(201, [{}]);

        httpBackend
            .expectPOST(HOST.terceiro + cpf + '/rascunho/cartaoautografo/' + cpf + '/imagem')
            .respond(201, [{}]);

        var onNotificacaoSpy = jasmine.createSpy('onNotificacao');
        var bindings = {
            cartoesAutografo: [],
            onNotificacao: onNotificacaoSpy,
            host: HOST.terceiro
        }

        ctrl = $componentController('sessaoCartaoAutografo', {
            $routeParams: {
                cpf: cpf
            },
        }, bindings);

        ctrl.$onInit();

        var image = new Blob();
        image.filename = 'imagem.jpeg'

        var _cartaoAutografo = {
            nome: 'teste',
            modoEdicao: false,
            pessoa: { cpfCnpj: cpf },
            assinatura: image
        };

        ctrl.mostrarCartaoAutografoForm = true;

        ctrl.salvar(_cartaoAutografo);

        httpBackend.flush();

        expect(onNotificacaoSpy).toHaveBeenCalledWith({ status: true });
        expect(ctrl.cartoesAutografo.length).toBe(1);
        expect(ctrl.mostrarCartaoAutografoForm).toBe(false);
    }));

    it('deve notificar falha ao tentar SALVAR IMAGEM', inject(function (Upload) {

        var onNotificacaoSpy = jasmine.createSpy('onNotificacao');
        var bindings = {
            cartoesAutografo: [],
            onNotificacao: onNotificacaoSpy,
            host: HOST.terceiro
        }

        ctrl = $componentController('sessaoCartaoAutografo', {
            $routeParams: {
                cpf: cpf
            },
        }, bindings);

        ctrl.$onInit();

        var deferred = $q.defer();
        deferred.resolve({});
        spyOn(ctrl.factory, 'salvar').and.returnValue(deferred.promise);

        var deferredReject = $q.defer();
        deferredReject.reject({});
        spyOn(Upload, 'upload').and.returnValue(deferredReject.promise);

        var image = new Blob();
        image.filename = 'imagem.jpeg'

        var _cartaoAutografo = {
            nome: 'teste',
            modoEdicao: false,
            pessoa: { cpfCnpj: '123456789' },
            assinatura: image
        };

        ctrl.mostrarCartaoAutografoForm = true;

        ctrl.salvar(_cartaoAutografo);

        $rootScope.$apply();

        expect(Upload.upload).toHaveBeenCalled();
        expect(ctrl.cartoesAutografo.length).toBe(1);
        expect(ctrl.mostrarCartaoAutografoForm).toBe(false);
    }));

    it('deve EDITAR', function () {

        httpBackend
            .expectPUT(HOST.terceiro + cpf + '/rascunho/cartaoautografo')
            .respond(201, [{}]);

        spyOn($rootScope, '$broadcast').and.callThrough();

        var onNotificacaoSpy = jasmine.createSpy('onNotificacao');
        var bindings = {
            cartoesAutografo: [],
            onNotificacao: onNotificacaoSpy,
            host: HOST.terceiro
        }

        ctrl = $componentController('sessaoCartaoAutografo', {
            $routeParams: {
                cpf: cpf
            },
        }, bindings);

        ctrl.$onInit();

        var _cartaoAutografo = {
            nome: 'teste',
            modoEdicao: true
        };

        var cartaoAutografoEditada = {
            nome: 'teste alterado',
            modoEdicao: true
        };

        var coadijuvante = {
            nome: 'coadijuvante',
            modoEdicao: false
        };

        ctrl.cartoesAutografo.push(_cartaoAutografo);
        ctrl.cartoesAutografo.push(coadijuvante);

        ctrl.mostrarCartaoAutografoForm = true;

        ctrl.salvar(cartaoAutografoEditada);

        httpBackend.flush();

        expect(ctrl.cartoesAutografo[0].nome).toBe('teste alterado');
        expect(ctrl.cartoesAutografo[0].modoEdicao).toBe(false);
        expect(ctrl.cartoesAutografo[1]).toEqual(coadijuvante);
        expect(ctrl.cartoesAutografo.length).toBe(2);
        expect(ctrl.mostrarCartaoAutografoForm).toBe(false);
        expect($rootScope.$broadcast).toHaveBeenCalled();

    });


    it('deve CANCELAR nova/edição', function () {
        var cartaoAutografo = {
            nome: 'teste',
            modoEdicao: true
        };
        ctrl.mostrarCartaoAutografoForm = true;

        ctrl.cartoesAutografo.push(cartaoAutografo);

        ctrl.cancelar();

        expect(ctrl.cartoesAutografo.length).toBe(1);
        expect(ctrl.cartoesAutografo[0].modoEdicao).toBe(false);
        expect(ctrl.mostrarCartaoAutografoForm).toBe(false);
    });

    it('deve NOVO formulário', function () {
        ctrl.mostrarCartaoAutografoForm = false;

        ctrl.novo();

        expect(ctrl.mostrarCartaoAutografoForm).toBe(true);
    });

    it('deve ficar em MODO EDIÇÃO', function () {
        spyOn($rootScope, '$broadcast').and.callThrough();

        ctrl.mostrarCartaoAutografoForm = false;
        ctrl.cartaoAutografo = null;

        var cartaoAutografo = {
            nome: 'teste',
            modoEdicao: true,
            poderes: {}
        };

        ctrl.editar(cartaoAutografo);

        expect($rootScope.$broadcast).not.toHaveBeenCalledWith();
        expect(ctrl.mostrarCartaoAutografoForm).toBe(true);
        expect(ctrl.cartaoAutografo).toEqual(cartaoAutografo);
    });

    it('deve ficar em MODO EDIÇÃO, adicionar poderes', function () {
        ctrl.cartaoAutografo = null;

        var cartaoAutografo = {
            nome: 'teste',
            modoEdicao: true
        };

        ctrl.editar(cartaoAutografo);

        cartaoAutografoEsperado = {
            nome: 'teste',
            modoEdicao: true,
            poderes: {}
        };

        expect(ctrl.mostrarCartaoAutografoForm).toBe(true);
        expect(ctrl.cartaoAutografo).toEqual(cartaoAutografoEsperado);
    });

    it('deve REMOVER', function () {
        ctrl.$onInit();

        var deferred = $q.defer();
        deferred.resolve('yes');
        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });
        spyOn(ctrl.factory, 'excluir').and.returnValue(deferred.promise);

        var _cartaoAutografo = {
            nome: 'teste',
            modoEdicao: false,
            modoExclusao: false
        };

        var coadijuvante = {
            nome: 'coadijuvante',
            modoExclusao: true
        };

        ctrl.cartoesAutografo.push(_cartaoAutografo);
        ctrl.cartoesAutografo.push(coadijuvante);

        ctrl.remover(coadijuvante);

        $rootScope.$apply();

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.factory.excluir).toHaveBeenCalledWith(cpf, coadijuvante);
        expect(ctrl.cartoesAutografo.length).toBe(1);
        expect(ctrl.cartoesAutografo[0]).toEqual(_cartaoAutografo);

    });

    it('deve CANCELAR remoção', function () {
        var deferred = $q.defer();
        deferred.reject();
        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var cartaoAutografo = {
            nome: 'teste',
            modoEdicao: false,
            modoExclusao: false
        };

        var coadijuvante = {
            nome: 'coadijuvante',
            modoExclusao: true
        };

        ctrl.cartoesAutografo.push(cartaoAutografo);
        ctrl.cartoesAutografo.push(coadijuvante);

        ctrl.remover(coadijuvante);

        $rootScope.$apply();

        var todosDesabilitados = ctrl.cartoesAutografo
            .some(function (cartaoAutografo) {
                return !cartaoAutografo.modoExclusao;
            });

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.cartoesAutografo.length).toBe(2);
        expect(todosDesabilitados).toBe(true);
    });
});
