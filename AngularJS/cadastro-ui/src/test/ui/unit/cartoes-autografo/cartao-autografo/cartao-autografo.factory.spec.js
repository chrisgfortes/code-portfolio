describe('factory: cartaoAutografo', function () {

    var cartaoAutografo,
        moment,
        HOST,
        httpBackend;

    beforeEach(module('app'));

    beforeEach(inject(function ($httpBackend, _cartaoAutografoFactory_, _moment_, _HOST_) {
        cartaoAutografo = _cartaoAutografoFactory_(_HOST_.terceiro);
        httpBackend = $httpBackend;
        HOST = _HOST_;
        moment = _moment_;

        $httpBackend.whenGET(/\.html$/).respond('');
    }));

    afterEach(function () {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.buscar(cpf)', function () {
        var cpf = '83084412545';
        var cartoesAutografosResponse = cartoesAutografosPost(moment);

        httpBackend
            .expectGET(HOST.terceiro + cpf + '/rascunho/cartaoautografo')
            .respond(200, cartoesAutografosResponse);

        cartaoAutografo
            .buscar(cpf)
            .then(function (response) {
                var dados = response.data;
                expect(dados).toEqual(cartoesAutografosResponse);
            });

        httpBackend.flush();
    });

    it('.salvar(cpf, cartoesAutografos)', function () {
        var cpf = '83084412545';
        var cartoesAutografosResponse = cartoesAutografosPost(moment);
        var dadosFake = cartoesAutografosFake();

        httpBackend
            .expectPUT(HOST.terceiro + cpf + '/rascunho/cartaoautografo')
            .respond(201, dadosFake);

        cartaoAutografo
            .salvar(cpf, cartoesAutografosResponse)
            .then(function (response) {
                var dados = response.data;

                expect(dados).toEqual(dadosFake);
            });

        httpBackend.flush();
    });

    it('.buscarImagemAssinatura(cpf, cartaoAutografo)', function () {
        var cpf = '83084412545';
        var cpfCnpj = '1111111111111111';
        var cartoesAutografosResponse = cartoesAutografosPost(moment);

        httpBackend
            .expectGET(HOST.terceiro + cpf + '/rascunho/cartaoautografo/' + cpfCnpj + '/imagem')
            .respond(200, cartoesAutografosResponse);

        var _cartaoAutografo = {
            pessoa: { cpfCnpj: cpfCnpj }
        };

        cartaoAutografo
            .buscarImagemAssinatura(cpf, _cartaoAutografo)
            .then(function (response) {
                var dados = response.data;
                expect(dados).toEqual(cartoesAutografosResponse);
            });

        httpBackend.flush();
    });

    it('.salvarImagemAssinatura(cpf, cartaoAutografo, file)', function () {
        var cpf = '83084412545';
        var cpfCnpj = '1111111111111111';
        var cartoesAutografosResponse = cartoesAutografosPost(moment);

        httpBackend
            .expectPOST(HOST.terceiro + cpf + '/rascunho/cartaoautografo/' + cpfCnpj + '/imagem')
            .respond(201, cartoesAutografosResponse);

        var _cartaoAutografo = {
            pessoa: { cpfCnpj: cpfCnpj }
        };

        cartaoAutografo
            .salvarImagemAssinatura(cpf, _cartaoAutografo, {})
            .then(function (response) {
                var dados = response.data;
                expect(dados).toEqual(cartoesAutografosResponse);
            });

        httpBackend.flush();
    });

    it('.buscarPessoa(cpf)', function () {
        var cpf = '83084412545';
        var cpfCnpj = '1111111111111111';
        var cartoesAutografosResponse = cartoesAutografosPost(moment);

        httpBackend
            .expectGET(HOST.terceiro + cpf + '/rascunho/cartaoautografo/pessoa')
            .respond(200, cartoesAutografosResponse);

        var _cartaoAutografo = {
            pessoa: { cpfCnpj: cpfCnpj }
        };

        cartaoAutografo
            .buscarPessoa(cpf)
            .then(function (response) {
                var dados = response.data;
                expect(dados).toEqual(cartoesAutografosResponse);
            });

        httpBackend.flush();
    });

    it('.tipoPoder()', function () {
        var tiposPoder = tiposPoderGet();

        httpBackend
            .expectGET(HOST.pessoa + '/fisica/cartao-autografo/permissao-poder')
            .respond(200, tiposPoder);

        cartaoAutografo
            .tipoPoder()
            .then(function (response) {
                var dados = response.data;
                expect(dados).toEqual(tiposPoder);
            });

        httpBackend.flush();
    });

    it('.excluir(cpf, cartaoAutografo)', function () {
        var modelResponse = {};

        var cpf = '83084412545';
        var cpfCnpj = '1111111111111111';

        httpBackend
            .expectDELETE(HOST.terceiro + cpf + '/rascunho/cartaoautografo/' + cpfCnpj)
            .respond(204, modelResponse);

        var _cartaoAutografo = {
            pessoa: { cpfCnpj: cpfCnpj }
        };

        cartaoAutografo
            .excluir(cpf, _cartaoAutografo)
            .then(function (response) {
                var dados = response.data;
                expect(dados).toEqual(modelResponse);
            });

        httpBackend.flush();
    });
});

function tiposPoderGet() {
    return [
        "CONJUNTA",
        "INDIVIDUAL",
        "PREJUDICADO"
    ]
}

function cartoesAutografosFake() {
    return {

    }
}

function cartoesAutografosPost(moment) {
    return {

    }
}
