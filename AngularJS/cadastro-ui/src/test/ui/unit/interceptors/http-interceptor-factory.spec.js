describe('Service: httpInterceptor', function () {

    var httpInterceptor;
    var moment;
    var localStorage;
    var tokenValid = 'eyJhbGciOiJIUzI1NiJ9.eyJjZFVzdWFyaW8iOiJjYW1pbGEuMDU2NiIsImNkVXN1YXJpb1Npc3RlbWEiOiIiLCJjZENvb3BlcmF0aXZhIjoiMDU2NiIsImNkQ29vcGVyYXRpdmFDZW50cmFsIjo1MDcsInN1YiI6IlByZS1hcHJvdmFkbyJ9.lSWPGnj0Oug2jMlpWNkm0glf3_5RSTh60sQjy7KLnCc';

    var cooperativaValid = 566;
    var permissoes = ["Cadastro Aberto", "Cadastro Inserir"];
    var userPermissions = [{"nmPerfil":"Cadastro Aberto"},{"nmPerfil":"Cadastro Inserir"}]

    beforeEach(module('app'));

    beforeEach(inject(function (_httpInterceptor_, _moment_, _$window_) {
        httpInterceptor = _httpInterceptor_;
        moment = _moment_;

        localStorage = _$window_.localStorage;

        spyOn(localStorage, 'getItem').and.callFake(function (key) {
            if (key == 'session') {
                return tokenValid;
            }

            if (key == 'action') {
                return cooperativaValid;
            }

            if (key == 'permissoes') {
                return permissoes;
            }
        });
    }));

    it('deve estar definido', function () {
        expect(httpInterceptor).toBeDefined();
    });

    it('deve ter request definido', function () {
        expect(angular.isFunction(httpInterceptor.request)).toBeDefined();
    });


    it('deve configurar token, cooperativa e permissões', inject(function (AuthFactory) {
        var cooperativa = 566;

        spyOn(AuthFactory, 'buscarToken').and.returnValue(tokenValid);
        spyOn(AuthFactory, 'buscarActionCooperativa').and.returnValue(cooperativaValid);
        spyOn(AuthFactory, 'buscarPermissoes').and.returnValue(userPermissions);

        var $config = {
            headers: {
                token: undefined,
                cooperativa: undefined,
                Authorization: undefined,
                permissoes_acesso: undefined
            }
        };

        httpInterceptor.request($config);

        expect($config.headers.token).toBe(tokenValid);
        expect($config.headers.cooperativa).toBe(cooperativa);
        expect($config.headers.Authorization).toBe('Bearer '+ tokenValid);
        expect($config.headers['X-Unicred-Permissoes']).toEqual(permissoes);
    }));
});
