describe('Service: errorInterceptor', function () {

    var errorInterceptor,
        $rootScope;

    beforeEach(module('app'));

    beforeEach(inject(function (_errorInterceptor_, _$rootScope_, _$q_) {
        errorInterceptor = _errorInterceptor_;
        $rootScope = _$rootScope_;
    }));

    it('deve estar definido', function () {
        expect(errorInterceptor).toBeDefined();
    });

    it('deve ter responseError definido', function () {
        expect(angular.isFunction(errorInterceptor.responseError)).toBe(true);
    });

    it('deve habilitar o toast ao chamar responseError com status 500', function () {
        $rootScope.displayToast = false;
        var rejection = {
            status: 500
        };

        errorInterceptor.responseError(rejection);

        expect($rootScope.displayToast).toBe(true);
    });
});
