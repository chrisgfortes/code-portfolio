describe('Service: loadingInterceptor', function () {

    var loadingInterceptor;
    var moment;
    var $rootScope;

    beforeEach(module('app'));

    beforeEach(inject(function (_loadingInterceptor_, _moment_, _$rootScope_) {
        loadingInterceptor = _loadingInterceptor_;
        moment = _moment_;
        $rootScope = _$rootScope_;
    }));

    it('deve estar definido', function () {
        expect(loadingInterceptor).toBeDefined();
    });

    it('deve ter requestError definido', function () {
        expect(angular.isFunction(loadingInterceptor.requestError)).toBe(true);
    });

    it('deve desabilitar o loading ao chamar requestError', function () {
        $rootScope.loading = true;
        var rejection = {};

        loadingInterceptor.requestError(rejection);

        expect($rootScope.loading).toBe(false);
    });

    it('deve ter responseError definido', function () {
        expect(angular.isFunction(loadingInterceptor.responseError)).toBe(true);
    });
});