function permissoes() {
    return [
        {
            tipoDePermissao: 'Cadastro Inserir',
            rotas: [
                "/cadastro",
                "/cadastro/terceiro/:cpf/dados-pessoais/",
                "/cadastro/terceiro/:cpf/dados-complementares",
                "/cadastro/terceiro/:cpf/bens",
                "/cadastro/terceiro/:cpf/cartao-autografo",
                "/cadastro/terceiro/:cpf/renda",
                "/cadastro/terceiro/:cpf/analise-cadastral",
                "/cadastro/terceiro/:cpf/resumo",
                "/cadastro/associado/:cpf/matricula",
                "/cadastro/correntista/:cpf/conta-corrente",
                "/cadastro/correntista/:cpf/agendamento-capital",
                "/cadastro/associado/:cpf/limite-cheque-especial",
                "/cadastro/associado/:cpf/limite-cartao-credito"
            ]
        },
        {
            tipoDePermissao: 'Cadastro Atualizar',
            rotas: [
                "/alteracao/:cpf",
                "/alteracao/pessoa-fisica/:cpf/renda",
                "/alteracao/pessoa-fisica/:cpf/endereco",
                "/alteracao/pessoa-fisica/:cpf/dependentes",
                "/alteracao/pessoa-fisica/:cpf/estado-civil",
                "/alteracao/pessoa-fisica/:cpf/imoveis",
                "/alteracao/pessoa-fisica/:cpf/veiculos",
                "/alteracao/pessoa-fisica/:cpf/contatos"
            ]
        }
    ]
}
