describe('CONTROLE DE ACESSO', function () {
    beforeEach(module('app'));

    describe('Rotas com validações de permissão - \'Cadastro Inserir\'', function () {
        var _permissoes = permissoes();
        var tipoDePermissao = 'Cadastro Inserir';

        var rotas = _permissoes.filter(temPerfil(tipoDePermissao))[0].rotas

        rotas.forEach(rotasComPermissao(tipoDePermissao))
    });

    describe('Rotas com validações de permissão - \'Cadastro Atualizar\'', function () {
        var _permissoes = permissoes();
        var tipoDePermissao = 'Cadastro Atualizar';

        var rotas = _permissoes.filter(temPerfil(tipoDePermissao))[0].rotas

        rotas.forEach(rotasComPermissao(tipoDePermissao))
    });

    describe('controle-de-acesso-factory', function () {
        it('.temAcesso(perfis) - OK 200', inject(function (AuthFactory, HttpCodes, $q, controleDeAcesso, $rootScope) {
            var permissoes = [
                { nmPerfil: "Cadastro Inserir" },
                { nmPerfil: "Cadastro Atualizar" }
            ];
            spyOn(AuthFactory, 'buscarPermissoes').and.returnValue(permissoes);

            controleDeAcesso
                .temAcesso('Cadastro Inserir')
                .then(function (res) {
                    expect(res).toBe(HttpCodes.OK);
                });

            $rootScope.$apply();
        }));

        it('.temAcesso(perfis), ARRAY - OK 200', inject(function (AuthFactory, HttpCodes, $q, controleDeAcesso, $rootScope) {
            var permissoes = [
                { nmPerfil: "Cadastro Inserir" },
                { nmPerfil: "Cadastro Atualizar" }
            ];
            spyOn(AuthFactory, 'buscarPermissoes').and.returnValue(permissoes);

            controleDeAcesso
                .temAcesso(['Cadastro Inserir', 'OUTRA PERMISSAO'])
                .then(function (res) {
                    expect(res).toBe(HttpCodes.OK);
                });

            $rootScope.$apply();
        }));

        it('.temAcesso(perfis) - FORBIDDEN 403', inject(function (AuthFactory, HttpCodes, $q, controleDeAcesso, $rootScope) {
            var permissoes = [
                { nmPerfil: "Cadastro Inserir" },
                { nmPerfil: "Cadastro Atualizar" }
            ];
            spyOn(AuthFactory, 'buscarPermissoes').and.returnValue(permissoes);

            controleDeAcesso
                .temAcesso('NAO EXISTE')
                .catch(function (erro) {
                    expect(erro).toBe(HttpCodes.FORBIDDEN);
                })

            $rootScope.$apply();
        }));
    });

    function temPerfil(tipoDePermissao) {
        return function (permissao) {
            return permissao.tipoDePermissao == tipoDePermissao;
        }
    }

    function rotasComPermissao(tipoDePermissao) {
        return function (urlRota) {
            it('--> ' + urlRota,
                inject(function (controleDeAcesso, $q, $rootScope, $route) {

                    var deferred = $q.defer();
                    deferred.resolve({});

                    var rota = $route.routes[urlRota];

                    spyOn(controleDeAcesso, 'temAcesso').and.returnValue(deferred.promise);

                    rota.resolve.temPermissao(controleDeAcesso);
                    $rootScope.$apply();

                    expect(controleDeAcesso.temAcesso).toHaveBeenCalledWith(tipoDePermissao);
                }));
        }
    }
});
