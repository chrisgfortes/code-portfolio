describe('controller: alteracaoController', function () {

    var ctrl, updateLinks, $routeParams;

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_$controller_, _updateLinks_) {
        $controller = _$controller_;
        updateLinks = _updateLinks_;

        ctrl = $controller('alteracaoController', {
            $routeParams: {cpf:12345678912}
        });
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });
});
