
describe('Service: dadosProfissionaisConvert', function () {

    var dadosProfissionaisConvert;
    var moment;

    beforeEach(module('app'));

    beforeEach(inject(function (_dadosProfissionaisConvert_, _moment_) {
        dadosProfissionaisConvert = _dadosProfissionaisConvert_;
        moment = _moment_;
    }));

    it('deve estar definido', function () {
        expect(dadosProfissionaisConvert).toBeDefined();
    });

    it('deve converter para fazer POST', function () {

        var dadosProfissional = {
            grauInstrucao: "ENSINO_FUNDAMENTAL_INCOMPLETO",
            profissao: {
                id: 640,
                nome: "ABASTECEDOR"
            },
            especialidade: {
                id: 246,
                nome: "ADM EMPRESAS"
            },
            nrRegistroProfissional: 123456,
            cdOrgaoResponsavel: {
                id: 37,
                nome: "ASS"
            },
            ufRegistro: "AC",
            inicioProfissional:true,
            mesesProfissao: 207
        };

        var dadosProfissionaisConvertido = dadosProfissionaisConvert.post(dadosProfissional);

        var dadosProfissionalEsperado = {
            grauInstrucao: "ENSINO_FUNDAMENTAL_INCOMPLETO",
            cdProfissao: 640,
            cdEspecialidade: 246,
            nrRegistroProfissional: 123456,
            cdOrgaoResponsavel: 37,
            ufRegistro: "AC",
            mesesProfissao: 207
        };

        expect(dadosProfissionaisConvertido).toEqual(dadosProfissionalEsperado)
    });

    it('deve converter para fazer POST, sem data inicioProfissional', function () {

        var dadosProfissional = {
            grauInstrucao: "ENSINO_FUNDAMENTAL_INCOMPLETO",
            profissao: {
                id: 640,
                nome: "ABASTECEDOR"
            },
            especialidade: {
                id: 246,
                nome: "ADM EMPRESAS"
            },
            nrRegistroProfissional: 123456,
            cdOrgaoResponsavel: {
                id: 37,
                nome: "ASS"
            },
            ufRegistro: "AC"
        };

        var dadosProfissionaisConvertido = dadosProfissionaisConvert.post(dadosProfissional);

        var dadosProfissionalEsperado = {
            grauInstrucao: "ENSINO_FUNDAMENTAL_INCOMPLETO",
            cdProfissao: 640,
            cdEspecialidade: 246,
            nrRegistroProfissional: 123456,
            cdOrgaoResponsavel: 37,
            ufRegistro: "AC"
        };

        expect(dadosProfissionaisConvertido).toEqual(dadosProfissionalEsperado)
    });
});
