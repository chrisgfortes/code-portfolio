describe('factory: dadosProfissional', function () {

    var dadosProfissional,
        httpBackend,
        HOST;

    beforeEach(module('app'));

    beforeEach(inject(function ($httpBackend, _dadosProfissional_, _HOST_) {
        dadosProfissional = _dadosProfissional_;
        HOST = _HOST_;
        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));

    afterEach(function () {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.especialidades()', function () {
        var modelsResponse = [{}];

        httpBackend
            .expectGET('../../../sau-us/cadastro/cooperado/v1/especialidades')
            .respond(200, modelsResponse);

        dadosProfissional
            .especialidades()
            .then(function (response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.profissoes()', function () {
        var modelsResponse = [{}];

        httpBackend
            .expectGET('../../../sau-us/cadastro/cooperado/v1/profissoes')
            .respond(200, modelsResponse);

        dadosProfissional
            .profissoes()
            .then(function (response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.tipoVinculo()', function () {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.pessoa + '/analise-cadastral/tipos-vinculo')
            .respond(200, modelsResponse);

        dadosProfissional
            .tipoVinculo()
            .then(function (response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.grauInstrucao()', function () {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.pessoa + '/dados-profissionais/graus-instrucao')
            .respond(200, modelsResponse);

        dadosProfissional
            .grauInstrucao()
            .then(function (response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.orgaosprofissionais()', function () {
        var modelsResponse = [{}];

        httpBackend
            .expectGET('../../../sau-us/cadastro/cooperado/v1/pessoa/fisica/orgaosprofissionais')
            .respond(200, modelsResponse);

        dadosProfissional
            .orgaosprofissionais()
            .then(function (response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });
});