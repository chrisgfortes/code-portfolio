describe('Component: dadosProfissionais', function () {

    var $componentController,
        ctrl,
        cepFactory,
        dadosProfissional
        moment;

    var especialidades = [{ cdEspecialidade: 134 }];
    var profissoes = [{ cdProfissao: 254 }];
    var orgaosprofissionais = [{ cdOrgaoResponsavel: 18 }];
    var grauInstrucao = [];
    var estados = [];

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _cepFactory_, _moment_, _dadosProfissional_) {
        $componentController = _$componentController_;
        cepFactory = _cepFactory_;
        moment = _moment_;
        dadosProfissional = _dadosProfissional_;

        var bindings = {
            profissional: {},
            nomeComponente: 'dadosProfissionais',
            readonly: true,
            form: {}
        }

        spyOn(dadosProfissional, 'especialidades').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: especialidades }); }
            };
        });
        spyOn(dadosProfissional, 'profissoes').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: profissoes }); }
            };
        });
        spyOn(dadosProfissional, 'grauInstrucao').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: grauInstrucao }); }
            };
        });
        spyOn(cepFactory, 'estados').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: estados }); }
            };
        });
        spyOn(dadosProfissional, 'orgaosprofissionais').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: orgaosprofissionais }); }
            };
        });

        ctrl = $componentController('dadosProfissionais', {
            cepFactory: _cepFactory_,
            dadosProfissional: _dadosProfissional_,
            moment: _moment_
        }, bindings);

        ctrl.profissional.cdEspecialidade = 134;
        ctrl.profissional.cdProfissao = 254;
        ctrl.profissional.cdOrgaoResponsavel = 18;
        ctrl.profissional.inicioProfissional = "2010-07-22";

        ctrl.$onInit();
    }));


    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o profissional no bindings', function () {
        expect(ctrl.profissional).toBeDefined();
    });

    it('deve definir um valor padrão para o profissional', function () {
        ctrl.profissional = undefined;

        ctrl.$onInit();

        expect(ctrl.profissional).toEqual({});
    });

    it('deve definir o profissional no bindings', function () {
        expect(ctrl.profissional).toBeDefined();
    });

    it('deve chamar dadosProfissional.especialidades()', function () {
        expect(dadosProfissional.especialidades).toHaveBeenCalled();
    });

    it('deve chamar dadosProfissional.profissoes()', function () {
        expect(dadosProfissional.profissoes).toHaveBeenCalled();
    });

    it('deve chamar dadosProfissional.grauInstrucao()', function () {
        expect(dadosProfissional.grauInstrucao).toHaveBeenCalled();
    });

    it('deve chamar cepFactory.estados()', function () {
        expect(cepFactory.estados).toHaveBeenCalled();
    });

    it('deve calclular o tempo de profissão', function () {
        var data = moment().subtract(6, 'month');
        var dataFormatada = data.format('DD/MM/YYYY');
        var seisMeses = moment().diff(data, 'months');

        ctrl.calcularTempoDeProfissao(dataFormatada);

        expect(ctrl.profissional.mesesProfissao).toBe(seisMeses);
    });

    it('não deve aceitar o mês inválido', function () {
        var dataFormatadaInvalida = '15/2015';

        ctrl.calcularTempoDeProfissao(dataFormatadaInvalida);

        expect(ctrl.mesesDeProfissao).not.toBeDefined();
    });

    it('deve aceitar data valida em validaData()', function () {
        var data = moment().subtract(6, 'month');
        ctrl.validarData(data);
        expect(ctrl.profissional.inicioProfissional).not.toBeUndefined();
    });

    it('não deve aceitar data inválida em validaData()', function () {
        var data = "87/18/7898";
        ctrl.validarData(data);
        expect(ctrl.profissional.inicioProfissional).toEqual(null);
    });

});
