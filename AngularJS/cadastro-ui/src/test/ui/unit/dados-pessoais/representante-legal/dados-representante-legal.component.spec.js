describe('Componente DADOS REPRESENTANTE LEGAL', function () {

    var $componentController,
        ctrl,
        dialogs,
        cepFactory,
        $rootScope,
        $routeParams,
        documento,
        $q;

    var identificacoes = [];
    var estados = [];
    var orgaosExpeditores = [];

    var representante = {
        cpf: "26820160658",
        dataEmissao: "2017-03-09",
        dataNascimento: "1980-12-31",
        endereco: {},
        nomeCompleto: "MARIO CATURRITA",
        numeroIdentificacao: "882428482",
        orgaoExpedidor: "DETRAN",
        tipoIdentificacao: "CARTEIRA_MOTORISTA",
        ufExpedidor: "AC"
    }

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _cepFactory_, _dialogs_, _$q_, _$rootScope_, _$routeParams_, _documento_) {
        $componentController = _$componentController_;
        cepFactory = _cepFactory_;
        dialogs = _dialogs_;
        $q = _$q_;
        $rootScope = _$rootScope_;
        $routeParams = _$routeParams_;
        documento = _documento_;

        spyOn(documento, 'identificacoes').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: identificacoes }); }
            };
        });

        spyOn(cepFactory, 'estados').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: estados }); }
            };
        });

        spyOn(documento, 'orgaoemissor').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: orgaosExpeditores }); }
            };
        });

        var bindings = {
            representante: representante
        };

        ctrl = $componentController(
            'dadosRepresentanteLegal',
            {
                cepFactory: _cepFactory_,
                dialogs: _dialogs_,
            },
            bindings);

        ctrl.$onInit();
    }));

    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o representante no bindings', function () {
        expect(ctrl.representante).toBeDefined();
    });

    it('deve chamar documento.identificacoes()', function () {
        expect(documento.identificacoes).toHaveBeenCalled();
    });

    it('deve chamar cepFactory.estados()', function () {
        expect(cepFactory.estados).toHaveBeenCalled();
    });

    it('deve chamar $onDestroy', function () {
        ctrl.representante = {};

        ctrl.$onDestroy();

        expect(ctrl.representante).toBeUndefined();
    });

    it('deve definir o representante se undefined', function () {
        ctrl.representante = undefined;

        ctrl.$onInit();

        expect(ctrl.representante).toBeDefined();
    });

    it('deve aceitar DATA DE NASCIMENTO válida', function () {
        var dataNascimento = moment('2000-01-01');
        var dataEmissao = moment('2000-01-01');
        ctrl.maxDataNascimento = moment();

        ctrl.onChange(dataNascimento, dataEmissao);

        expect(ctrl.representante.dataNascimento).not.toEqual({});
    });

    it('deve limpar DATA DE EMISSAO se data de nascimento for maior que a mesma', function () {
        var dataNascimento = moment('2000-05-01');
        var dataEmissao = moment('2000-01-01');

        ctrl.onChange(dataNascimento, dataEmissao);

        expect(ctrl.representante.dataEmissao).toEqual({});
    });

    it('não deve aceitar DATA DE NASCIMENTO inválida', function () {
        var dataNascimento = '2000-34-52';
        var dataEmissao = moment('2000-01-01');

        ctrl.onChange(dataNascimento);
        
        expect(ctrl.representante.dataNascimento).toEqual({});
    });

    it('deve aceitar DATA DE EMISSAO válida', function () {
        var dataEmissao = '01/01/2000';
        ctrl.representante.dataEmissao = undefined;
        
        ctrl.validarDataEmissao(dataEmissao);
        
        expect(ctrl.representante.dataEmissao).not.toEqual({});
    });

    it('não deve aceitar DATA DE EMISSAO inválida', function () {
        var dataEmissao = '52/34/2000';
        ctrl.validarDataEmissao(dataEmissao);
        expect(ctrl.representante.dataEmissao).toEqual({});
    });

    it('deve pedir confirmação se for MENOR DE IDADE', function () {
        spyOn(dialogs, 'confirm').and.callThrough();

        var dataNascimento = moment().subtract(17, 'years').format('DD/MM/YYYY');

        ctrl.confirmaMenorDeIdade(dataNascimento);

        expect(dialogs.confirm).toHaveBeenCalled();
    });

    it('não deve pedir confirmação se for MAIOR DE IDADE', function () {
        spyOn(dialogs, 'confirm').and.callThrough();

        var dataNascimento = moment().subtract(20, 'years').format('DD/MM/YYYY');

        ctrl.confirmaMenorDeIdade(dataNascimento);

        expect(dialogs.confirm).not.toHaveBeenCalled();
    });

    it('deve limpar DATA DE NASCIMENTO de representante legal se não for confirmado', function () {
        var deferred = $q.defer();
        deferred.reject();
        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var dataNascimento = moment().format('DD/MM/YYYY');

        ctrl.confirmaMenorDeIdade(dataNascimento);
        $rootScope.$apply();

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.representante.dataNascimento).toEqual({});
    });
});
