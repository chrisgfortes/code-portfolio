describe('factory: dadosPessoais', function () {

    var dadosPessoais,
        moment,
        HOST;

    beforeEach(module('app'));

    beforeEach(inject(function (_dadosPessoais_, _moment_, _HOST_) {
        dadosPessoais = _dadosPessoais_('terceiro');
        moment = _moment_;
        HOST = _HOST_;
    }));

    it('.buscar(cpf)', inject(function($httpBackend) {
        $httpBackend.whenGET(/\.html$/).respond('');

        var cpf = '83084412545';
        var dadosPessoaisResponse = dadosPessoaisPost(moment);

        $httpBackend
            .expectGET(HOST.terceiro + cpf + '/rascunho/dadospessoais')
            .respond(200, dadosPessoaisResponse);

        dadosPessoais
            .buscar(cpf)
            .then(function (response) {
                var dados = response;
                expect(dados).toEqual(dadosPessoaisResponse);
            });

        $httpBackend.flush();
    }));

    it('.salvar(cpf, dadosPessoais)', inject(function($httpBackend) {
        $httpBackend.whenGET(/\.html$/).respond('');

        var cpf = '83084412545';
        var dadosPessoaisResponse = dadosPessoaisPost(moment);
        var dadosFake = dadosPessoaisFake();

        $httpBackend
            .expectPUT(HOST.terceiro + cpf + '/rascunho/dadospessoais')
            .respond(201, dadosFake);

        dadosPessoais
            .salvar(dadosPessoaisResponse)
            .then(function (response) {
                var dados = response.data;

                expect(dados).toEqual(dadosFake);
            });

        $httpBackend.flush();
    }));
});

function dadosPessoaisFake() {
    return {
        "cooperativa": 566,
        "cpf": "83084412545",
        "nomeCompleto": "dariano soares",
        "nomeSucinto": "dariano soares",
        "documento": {
            "tipoIdentificacao": "CARTEIRA_IDENTIDADE",
            "numeroIdentificacao": "12456789",
            "ufExpedidor": "AC",
            "orgaoExpedidor": "SSP",
            "dataEmissao": "1990-03-22T03:00:00.000Z",
            "dataNascimento": "1980-02-01T03:00:00.000Z",
            "modalidadeRepresentanteLegal": "PF_MENOR_IDADE",
            "sexo": "MASCULINO",
            "nacionalidade": "Argentino(a)"
        },
        "representanteLegal": {
            "cpf": "00360741010",
            "nomeCompleto": "dariano",
            "tipoIdentificacao": "CARTEIRA_IDENTIDADE",
            "numeroIdentificacao": "456",
            "ufExpedidor": "AC",
            "orgaoExpedidor": "SSP",
            "dataEmissao": "1988-01-01T02:00:00.000Z",
            "dataNascimento": "1980-01-01T03:00:00.000Z",
            "endereco": {
                "tipoEndereco": "RESIDENCIAL",
                "cep": 94090330,
                "logradouro": "Rua Botafogo",
                "numero": "123",
                "enderecoSemNumero": false,
                "caixaPostal": 9409033,
                "complemento": "casa",
                "bairro": "Vera Cruz",
                "estado": "RS",
                "codigoCidade": 7748,
                "situacaoEndereco": "PROPRIA",
                "resideDesde": "2017-03-01T03:00:00.000Z"
            }
        },
        "filiacao": {
            "nomeMaeNaoDeclarado": true,
            "nomePaiNaoDeclarado": true
        },
        "estadoCivil": "SOLTEIRO",
        "conjuge": {},
        "dependentes": [
            {
                "cpf": "00360741010",
                "nomeCompleto": "minha filha",
                "dataNascimento": "2000-10-01",
                "tipoDependencia": "CONJUGE",
                "valorRenda": 1234.56,
                "valorPensao": 7894.56
            }
        ],
        "enderecos": [
            {
                "empostamento": true,
                "principal": true,
                "tipoEndereco": "RESIDENCIAL",
                "cep": 11121312,
                "logradouro": "gfsdgf",
                "numero": "0",
                "enderecoSemNumero": false,
                "bairro": "gdsfgsdf",
                "estado": "AC",
                "codigoCidade": 1,
                "situacaoEndereco": "PROPRIA",
                "resideDesde": "2012-01-01",
                "cidade": "Acrelândia"
            }
        ],
        "contatos": {
            "telefones": [
                {
                    "tipoTelefone": "CELULAR",
                    "ddd": "51",
                    "numero": "99999999",
                    "observacao": "fsdfsafsdfasdf"
                },
                {
                    "tipoTelefone": "RESIDENCIAL",
                    "ddd": "51",
                    "numero": "454564564",
                    "observacao": "dsfafasfasfasfsadfasfdsafsa"
                }
            ],
            "emails": []
        },
        "dadosProfissionais": {
            "grauInstrucao": "ENSINO_FUNDAMENTAL_INCOMPLETO",
            "cdProfissao": 234,
            "cdEspecialidade": 89,
            "ufRegistro": "RS",
            "inicioProfissional": "2010-03-22T03:00:00.000Z",
            "mesesProfissao": 84
        }
    }
}

function dadosPessoaisPost(moment) {
    return {
        cooperativa: 566,
        cpf: "83084412545",
        nomeCompleto: "dariano soares",
        nomeSucinto: "dariano soares",
        documento: {
            tipoIdentificacao: "CARTEIRA_IDENTIDADE",
            numeroIdentificacao: "12456789",
            ufExpedidor: "AC",
            orgaoExpedidor: "SSP",
            dataEmissao: "1990-03-22T03:00:00.000Z",
            dataNascimento: "1980-02-01T03:00:00.000Z",
            modalidadeRepresentanteLegal: {
                selected:{
                    valor: "PF_MENOR_IDADE",
                    descricao: "Menor de Idade"
                }
            },
            sexo: "MASCULINO",
            nacionalidade: "Argentino(a)"
        },
        representanteLegal: {
            cpf: "00360741010",
            nomeCompleto: "dariano",
            tipoIdentificacao: "CARTEIRA_IDENTIDADE",
            numeroIdentificacao: "456",
            ufExpedidor: "AC",
            orgaoExpedidor: "SSP",
            dataEmissao: "1988-01-01T02:00:00.000Z",
            dataNascimento: "1980-01-01T03:00:00.000Z",
            endereco: {
                tipoEndereco: "RESIDENCIAL",
                cep: 94090330,
                logradouro: "Rua Botafogo",
                numero: "123",
                enderecoSemNumero: false,
                caixaPostal: 9409033,
                complemento: "casa",
                bairro: "Vera Cruz",
                estado: "RS",
                codigoCidade: 7748,
                situacaoEndereco: "PROPRIA",
                resideDesde: "2017-03-01T03:00:00.000Z"
            }
        },
        filiacao: {
            nomeMaeNaoDeclarado: true,
            nomePaiNaoDeclarado: true
        },
        estadoCivil: "SOLTEIRO",
        conjuge: {},
        dependentes: [
            {
                cpf: "00360741010",
                nomeCompleto: "minha filha",
                dataNascimento: moment("2000-10-01"),
                tipoDependencia: "CONJUGE",
                valorRenda: 1234.56,
                valorPensao: 7894.56
            }
        ],
        enderecos: [
            {
                empostamento: true,
                principal: true,
                tipoEndereco: "RESIDENCIAL",
                cep: 11121312,
                logradouro: "gfsdgf",
                numero: "0",
                enderecoSemNumero: false,
                bairro: "gdsfgsdf",
                estado: "AC",
                codigoCidade: 1,
                situacaoEndereco: "PROPRIA",
                resideDesde: "2012-01-01",
                cidade: "Acrelândia"
            }
        ],
        contatos: {
            telefones: [
                {
                    tipoTelefone: "CELULAR",
                    ddd: "51",
                    numero: "99999999",
                    observacao: "fsdfsafsdfasdf"
                },
                {
                    tipoTelefone: "RESIDENCIAL",
                    ddd: "51",
                    numero: "454564564",
                    observacao: "dsfafasfasfasfsadfasfdsafsa"
                }
            ],
            emails: []
        },
        dadosProfissionais: {
            grauInstrucao: "ENSINO_FUNDAMENTAL_INCOMPLETO",
            cdProfissao: 234,
            cdEspecialidade: 89,
            ufRegistro: "RS",
            inicioProfissional: "2010-03-22T03:00:00.000Z",
            mesesProfissao: 84
        }
    }
}
