describe('Router: dadosPessoais-router', function () {

    var dadosPessoais,
        $route,
        $injector,
        HOST,
        $q,
        rotaService,
        linksFactory;

    beforeEach(module('app'));

    beforeEach(inject(function (_$route_, _dadosPessoais_, _$injector_, _HOST_, _linksFactory_, _$q_, _rotaService_) {
        $route = _$route_;
        dadosPessoais = _dadosPessoais_;
        $injector = _$injector_;
        HOST = _HOST_;
        linksFactory = _linksFactory_;
        $q = _$q_;
        rotaService = _rotaService_;
    }));

    describe('Rota de Terceiro', function () {
        testarRota('terceiro');
    });

    function testarRota(nomeDaRota) {
        it('Deve testar dadosPessoais-router', function() {
            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/dados-pessoais/'];

            expect(rota.controller).toBe('dadosPessoaisController');
            expect(rota.templateUrl).toBe('./modules/dados-pessoais/dados-pessoais.html');
        });

        it('Deve chamar dadosPessoais.buscar()', inject(function($httpBackend) {
            $httpBackend.whenGET(/\.html$/).respond('');
            var cpf = '123456789123';

            $httpBackend
                .expectGET(HOST[nomeDaRota] + cpf + '/rascunho/dadospessoais')
                .respond(200, {cpf:cpf});

            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/dados-pessoais/'];

            $route = {current: { params: { cpf: cpf } } };

            var retorno = rota.resolve.dadosPessoaisInfo(dadosPessoais, $route);
            $httpBackend.flush();
            expect(retorno.$$state.value).toEqual({cpf:cpf});
        }));

        it('Deve chamar dadosPessoais.buscar() e adicionar cpf se for nulo', inject(function($httpBackend) {
            $httpBackend.whenGET(/\.html$/).respond('');
            var cpf = '123456789123';

            $httpBackend
                .expectGET(HOST[nomeDaRota] + cpf + '/rascunho/dadospessoais')
                .respond(200, {});

            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/dados-pessoais/'];

            $route = {current: { params: { cpf: cpf } } };

            var retorno = rota.resolve.dadosPessoaisInfo(dadosPessoais, $route);
            $httpBackend.flush();
            expect(retorno.$$state.value).toEqual({cpf:cpf});
        }));

        it('não encontra dados pessoais', inject(function($httpBackend) {
            $httpBackend.whenGET(/\.html$/).respond('');
            var cpf = '123456789123';

            $httpBackend
                .expectGET(HOST[nomeDaRota] + cpf + '/rascunho/dadospessoais')
                .respond(404, {});

            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/dados-pessoais/'];

            $route = {current: { params: { cpf: cpf } } };

            var retorno = rota.resolve.dadosPessoaisInfo(dadosPessoais, $route);
            $httpBackend.flush();
            expect(retorno.$$state.value).toEqual({cpf:cpf});
        }));

        it('Deve carregar urls', function () {
            var cpf = '123456789123';
            var compare = mapearRetorno(linksFactory(nomeDaRota), cpf);
            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/dados-pessoais/'];

            var retorno = rota.resolve.urls(linksFactory);
            retorno = mapearRetorno(retorno, cpf);

            expect(retorno).toEqual(compare);
        });

        it('Deve definir a factory', function () {
            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/dados-pessoais/'];

            var factory = dadosPessoais(nomeDaRota);
            var retorno = rota.resolve.dadosPessoaisFactory(dadosPessoais);

            expect(Object.getOwnPropertyNames(retorno)).toEqual(Object.getOwnPropertyNames(factory));
        });

        it('deve verificar se é terceiro', function() {
            var cpf = '93994468248';

            var deferred = $q.defer();
            deferred.resolve({});
            spyOn(rotaService, 'isTerceiro').and.returnValue(deferred.promise);

            var rota = $route.routes['/cadastro/'+ nomeDaRota +'/:cpf/dados-pessoais/'];
            $route = {
                current: {
                    params: {
                        cpf: cpf
                    }
                }
            }

            rota.resolve.isTerceiro($route, rotaService);
            expect(rotaService.isTerceiro).toHaveBeenCalledWith(cpf);
        });
    }

    function mapearRetorno(urls, cpf) {
        urls = urls.map(function (item) {
            item.url = item.url(cpf);
            return item
        })
        return urls;
    }

});
