describe('controller: dadosPessoaisController', function () {

    var ctrl,
        $rootScope,
        dadosPessoaisFactory,
        $q,
        $scope,
        urls,
        moment;

    var cpf = '12345678912';
    var erros = [{}];
    var dadosPessoais = {}

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_$controller_, _$rootScope_, _dadosPessoais_, _$q_, _linksFactory_, _moment_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        dadosPessoaisFactory = _dadosPessoais_('terceiro');
        urls = _linksFactory_('terceiro');
        $q = _$q_,
        moment= _moment_;

        ctrl = $controller('dadosPessoaisController', {
            $scope: $scope,
            dadosPessoaisFactory: dadosPessoaisFactory,
            urls: urls,
            $routeParams: { cpf: '12345678912' },
            dadosPessoaisInfo: {}
        });
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('deve chamar .salvar(dadosPessoais)', function () {
        var deferred = $q.defer();
        deferred.resolve({});

        spyOn(dadosPessoaisFactory, 'salvar')
            .and
            .returnValue(deferred.promise);

        var dadosPessoais = dadosPessoaisMock(cpf);

        ctrl.salvar(dadosPessoais);

        $rootScope.$apply();

        expect(dadosPessoaisFactory.salvar).toHaveBeenCalledWith(dadosPessoais);
    });

    it('deve mostrar messagem de validação, envida pelo body', function () {
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                body: erros
            }
        });

        spyOn(dadosPessoaisFactory, 'salvar')
            .and
            .returnValue(deferred.promise);

        var dadosPessoais = dadosPessoaisMock(cpf);

        ctrl.salvar(dadosPessoais);

        $rootScope.$apply();

        expect(dadosPessoaisFactory.salvar).toHaveBeenCalledWith(dadosPessoais);
        expect(ctrl.erros).toEqual(erros);
    });

    it('deve mostrar messagem de validação, envida pelo content', function () {
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                content: erros
            }
        });

        spyOn(dadosPessoaisFactory, 'salvar')
            .and
            .returnValue(deferred.promise);

        var dadosPessoais = dadosPessoaisMock(cpf);

        ctrl.salvar(dadosPessoais);

        $rootScope.$apply();

        expect(dadosPessoaisFactory.salvar).toHaveBeenCalledWith(dadosPessoais);
        expect(ctrl.erros).toEqual(erros);
    });

    it('não deve mostrar messagem de validação', function () {
        var deferred = $q.defer();
        deferred.reject({
            status: 400,
            data: {
                content: erros
            }
        });

        spyOn(dadosPessoaisFactory, 'salvar')
            .and
            .returnValue(deferred.promise);

        var dadosPessoais = dadosPessoaisMock(cpf);

        ctrl.salvar(dadosPessoais);

        $rootScope.$apply();

        expect(dadosPessoaisFactory.salvar).toHaveBeenCalledWith(dadosPessoais);
        expect(ctrl.erros).not.toBeDefined();
    });

    it('deve mostrar o representante legal', function () {
        var representante = {};
        ctrl.mostrarRepresentante = false;
        ctrl.dadosPessoais.representanteLegal = {};

        ctrl.representanteLegalSelecionado(representante, true);

        expect(ctrl.mostrarRepresentante).toBe(true);
        expect(ctrl.dadosPessoais.representanteLegal).toBeDefined();
    });

    it('deve mostrar o representante legal com endereço vazio', function () {
        var representante = {};
        ctrl.mostrarRepresentante = false;

        ctrl.representanteLegalSelecionado(representante, true);

        expect(ctrl.mostrarRepresentante).toBe(true);
        expect(ctrl.dadosPessoais.representanteLegal).toBeDefined();
    });

    it('deve remover o representante legal', function () {
        var representante = {};
        ctrl.mostrarRepresentante = false;
        ctrl.dadosPessoais.representanteLegal = {};

        ctrl.representanteLegalSelecionado(representante, false);

        expect(ctrl.mostrarRepresentante).toBe(false);
        expect(ctrl.dadosPessoais.representanteLegal).not.toBeDefined();
    });

    function dadosPessoaisMock(cpf) {
        return {
          "cooperativa": null,
          "cpf": cpf,
          "nomeCompleto": "Lancelot de Jesus",
          "nomeSucinto": "Lancelot Jesus",
          "documento": {
            "tipoIdentificacao": "REGISTRO_PROFISSIONAL",
            "protocoloBRSafe": null,
            "numeroIdentificacao": "84531458",
            "ufExpedidor": "AM",
            "orgaoExpedidor": "OAB",
            "dataEmissao": moment("2017-06-06"),
            "dataNascimento": moment("1950-01-01"),
            "modalidadeRepresentanteLegal": {selected:{valor:"NENHUM"}},
            "sexo": "MASCULINO",
            "nacionalidade": "Brasileiro(a)",
            "uf": "MG",
            "naturalidade": "Abadia dos Dourados"
          },
          "representanteLegal": null,
          "filiacao": {
            "nomePai": "Pai Amado",
            "nomeMae": "",
            "nomeMaeNaoDeclarado": true,
            "nomePaiNaoDeclarado": false
          },
          "estadoCivil": "SOLTEIRO",
          "regimeCasamento": null,
          "conjuge": null,
          "dependentes": [
            {
              "id": null,
              "idPessoa": null,
              "cpf": "87211443464",
              "nomeCompleto": "MARIA",
              "dataNascimento": moment("0030-01-01"),
              "tipoDependencia": "FILHO_FILHA",
              "valorRenda": 153.15,
              "valorPensao": 3513.51
            }
          ],
          "enderecos": [
            {
              "id": null,
              "empostamento": true,
              "principal": true,
              "tipoEndereco": "RESIDENCIAL",
              "cep": 96745000,
              "logradouro": "Rua da Cruz",
              "numero": "135153",
              "enderecoSemNumero": false,
              "caixaPostal": null,
              "complemento": null,
              "bairro": "AFP",
              "estado": "RS",
              "codigoCidade": 7585,
              "situacaoEndereco": "PROPRIA",
              "valorAluguel": null,
              "resideDesde": moment("0001-01-01"),
              "idEmpostamento": null
            }
          ],
          "contatos": {
            "telefones": [
              {
                "id": null,
                "tipoTelefone": "RESIDENCIAL",
                "ddd": "51",
                "numero": "987651112",
                "observacao": null
              }
            ],
            "emails": []
          },
          "dadosProfissionais": {
            "id": null,
            "idPessoaFisica": null,
            "grauInstrucao": "ENSINO_FUNDAMENTAL_INCOMPLETO",
            "cdProfissao": 433,
            "cdEspecialidade": 204,
            "nrRegistroProfissional": "134131435",
            "cdOrgaoResponsavel": 43,
            "ufRegistro": "PB",
            "inicioProfissional": moment("2017-06-07")
          }
        }
    }
});
