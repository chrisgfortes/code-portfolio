describe('Component: enderecoLista', function () {

    var $componentController,
        $rootScope,
        dialogs,
        $q,
        ctrl;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _$rootScope_, _dialogs_, _$q_) {
        $componentController = _$componentController_;
        $rootScope = _$rootScope_;
        dialogs = _dialogs_;
        $q = _$q_;

        var bindings = {
            enderecos: [],
            readonly: false,
            esconderBotaoNovo: false
        }

        ctrl = $componentController('enderecoLista', {
            $rootScope: _$rootScope_,
            dialogs: _dialogs_
        }, bindings);

        ctrl.$onInit();
    }));

    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o bindings', function () {
        expect(ctrl.enderecos).toBeDefined();
        expect(ctrl.readonly).toBeDefined();
        expect(ctrl.esconderBotaoNovo).toBeDefined();
    });

    it('deve definir endereços', function () {
        ctrl.enderecos = undefined;

        ctrl.$onInit();

        expect(ctrl.enderecos).toBeDefined();
    });

    it('deve chamar o evento onNovo', function () {
        var onNovoSpy = jasmine.createSpy('onNovo');

        var bindings = {
            onNovo: onNovoSpy
        };

        var ctrl = $componentController('enderecoLista', null, bindings);

        ctrl.novo();

        expect(onNovoSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onEditar', function () {
        var onEditarSpy = jasmine.createSpy('onEditar');

        var bindings = {
            onEditar: onEditarSpy
        };

        var ctrl = $componentController('enderecoLista', null, bindings);

        ctrl.enderecos = [{ modoEdicao: false }];

        var principal = {
            modoEdicao: false
        };

        var esperado = {
            endereco: {
                modoEdicao: true
            }
        };

        ctrl.editar(principal);

        expect(onEditarSpy).toHaveBeenCalledWith(esperado);
        expect(principal.modoEdicao).toBe(true);
    });

    it('deve chamar o evento onRemover', function () {
        var onRemoverSpy = jasmine.createSpy('onRemover');

        var bindings = {
            onRemover: onRemoverSpy
        };

        var ctrl = $componentController('enderecoLista', null, bindings);

        var principal = {
            modoExclusao: false
        };

        var esperado = {
            endereco: {
                modoExclusao: true
            }
        };

        ctrl.remover(principal);

        expect(onRemoverSpy).toHaveBeenCalledWith(esperado);
        expect(principal.modoExclusao).toBe(true);
    });
});
