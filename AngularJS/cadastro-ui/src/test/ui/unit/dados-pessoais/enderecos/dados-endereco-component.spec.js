describe('Component: dadosEndereco', function () {

    var $componentController,
        ctrl,
        endereco,
        cepFactory,
        $httpBackend,
        $q,
        moment;

    var estados = [{
        label: "Acre",
        value: "AC"
    }];
    var tiposEndereco = [{
        descricao: "Residencial",
        valor: "RESIDENCIAL"
    }];
    var situacoesEndereco = [{
        descricao: "Própria",
        valor: "PROPRIA"
    }];

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _endereco_, _cepFactory_,
        _moment_, _$httpBackend_, _$q_) {

        $componentController = _$componentController_;
        endereco = _endereco_;
        cepFactory = _cepFactory_;
        moment = _moment_;
        $httpBackend = _$httpBackend_;
        $q = _$q_;

        var bindings = {
            ocultarCampos: ['tipoEndereco', 'caixaPostal'],
            nomeComponente: 'Endereco'
        };

        spyOn(endereco, 'tipos').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: tiposEndereco }); }
            };
        });

        spyOn(endereco, 'situacoes').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: situacoesEndereco }); }
            };
        });

        spyOn(cepFactory, 'estados').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: estados }); }
            };
        });

        ctrl = $componentController('dadosEndereco', {
            endereco: _endereco_,
            cepFactory: _cepFactory_,
            moment: _moment_
        }, bindings);

        ctrl.$onInit();
        ctrl.$onChanges();
    }));


    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o endereco no bindings', function () {
        expect(ctrl.endereco).toBeDefined();
    });

    it('deve definir o ocultarCampos no bindings', function () {
        expect(ctrl.ocultarCampos).toBeDefined();
    });

    it('deve definir o nomeComponente no bindings', function () {
        expect(ctrl.nomeComponente).toBeDefined();
    });

    it('deve chamar endereco.tipos()', function () {
        expect(endereco.tipos).toHaveBeenCalled();
        expect(ctrl.tiposEndereco).toBe(tiposEndereco);

    });

    it('deve chamar endereco.situacoes()', function () {
        expect(endereco.situacoes).toHaveBeenCalled();
        expect(ctrl.situacoesEndereco).toBe(situacoesEndereco);
    });

    it('deve chamar cepFactory.estados()', function () {
        expect(cepFactory.estados).toHaveBeenCalled();
        expect(ctrl.estados).toBe(estados);
    });

    it('deve desabilitar o campo número e limpa-lo', function () {
        ctrl.endereco = {
            numero: '123',
            enderecoSemNumero: true
        };

        ctrl.semNumero();

        expect(ctrl.endereco.enderecoSemNumero).toBe(true);
        expect(ctrl.endereco.numero).toBeNull();
    });

    it('deve habilitar o campo número', function () {
        ctrl.endereco = {
            numero: '',
            enderecoSemNumero: false
        };

        ctrl.semNumero();

        expect(ctrl.endereco.enderecoSemNumero).toBe(false);
        expect(ctrl.endereco.numero).toBeNull();
    });

    it("deve ocultar os campos passado para .ocultarCampos=['tipoEndereco', 'caixaPostal']", function () {
        expect(ctrl.ocultar.tipoEndereco).toBe(true);
        expect(ctrl.ocultar.caixaPostal).toBe(true);
        expect(ctrl.ocultar.cep).toBeUndefined();
    });

     it('não deve ter campos ocultos', function () {
        var bindings = {
            ocultarCampos: []
        };

        var ctrl = $componentController('dadosEndereco', null, bindings);
        ctrl.$onInit();

        expect(ctrl.ocultar).toEqual({ });
    });

    it("deve chamar a cepFactory.cidades()", function () {
        var cidades = [];

        spyOn(cepFactory, 'cidades').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: cidades }); }
            };
        });

        ctrl.cityAutocomplete('RS');

        expect(cepFactory.cidades).toHaveBeenCalled();
        expect(ctrl.cidades).toBe(cidades);
    });

    it("não deve chamar a cepFactory.cidades()", function () {
        spyOn(cepFactory, 'cidades').and.callThrough();

        ctrl.cityAutocomplete(undefined);

        expect(cepFactory.cidades).not.toHaveBeenCalled();
    });

    it('deve calclular o tempo de residência', function () {
        var data = moment().subtract(6, 'month');
        var seisMeses = moment().diff(data, 'months');

        ctrl.calcularTempoDeResidencia(data);

        expect(ctrl.mesesDeResidencia).toBe(seisMeses);
    });

    it("deve carregar o Endereço pelo CEP", function () {
        var endereco = {
            tipoLogradouro: 'dsfasdf',
            logradouro: 'dsfsdfafsda',
            bairro: 'bairro',
            uf: 'RS',
            cidade: {
                codigoCidade: 22
            }
        };

        spyOn(cepFactory, 'cep').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: endereco }); }
            };
        });

        var cep = '94090330';

        ctrl.buscarEnderecoPeloCEP(cep);

        expect(cepFactory.cep).toHaveBeenCalled();
    });

    it("não deve carregar o Endereço pelo CEP", function () {
        var cep = undefined;

        spyOn(cepFactory, 'cep').and.callThrough();

        ctrl.buscarEnderecoPeloCEP(cep);

        expect(cepFactory.cep).not.toHaveBeenCalled();
    });

    it("deve habilitar o campo VALOR ALUGUEL/FINCIAMENTO", function () {
        var cep = '94090330';

        var endereco = { situacaoEndereco: 'ALUGADA' };
        ctrl.situacaoEnderecoAlterado(endereco);
        expect(ctrl.deveHabilitarValorAluguel).toBe(true);

        var endereco = { situacaoEndereco: 'FINANCIADA' };
        ctrl.situacaoEnderecoAlterado(endereco);
        expect(ctrl.deveHabilitarValorAluguel).toBe(true);
    });

    it("não deve habilitar o campo VALOR ALUGUEL/FINCIAMENTO", function () {
        var cep = '94090330';

        var endereco = { situacaoEndereco: '' };
        ctrl.situacaoEnderecoAlterado(endereco);
        expect(ctrl.deveHabilitarValorAluguel).toBe(false);

        var endereco = { situacaoEndereco: 'PROPRIA' };
        ctrl.situacaoEnderecoAlterado(endereco);
        expect(ctrl.deveHabilitarValorAluguel).toBe(false);
    });

    it(".isNumeroRequired()", function () {
        ctrl.endereco = {
            enderecoSemNumero: false
        }

        var esperado = ctrl.isNumeroRequired();

        expect(esperado).toBe(true);
    });

    it('deve chamar o evento onSalvar', function () {
        var onSalvarSpy = jasmine.createSpy('onSalvar');

        var bindings = {
            endereco: {},
            onSalvar: onSalvarSpy
        };

        var ctrl = $componentController('dadosEndereco', null, bindings);
        ctrl.cidades = [
            {codigoCidade: 7388}
        ];

        ctrl.endereco = {
            codigoCidade: 7388
        };

        ctrl.salvar(ctrl.endereco);

        expect(onSalvarSpy).toHaveBeenCalled();
    });

    it(".onCancelar()", function () {
        var onCancelarSpy = jasmine.createSpy('onCancelar');

        var bindings = {
            onCancelar: onCancelarSpy
        };
        var ctrl = $componentController('dadosEndereco', null, bindings);

        ctrl.cancelar();

        expect(onCancelarSpy).toHaveBeenCalled();
    });

    it("não deve calcular mesesDeResidencia", function () {
        ctrl.validarResideDesde(undefined);

        expect(ctrl.mesesDeResidencia).toBeUndefined();
    });

    it("deve calcular mesesDeResidencia", function () {
        var data = moment().add(-10, 'months')
        ctrl.validarResideDesde(data);

        expect(ctrl.mesesDeResidencia).toBe(10);
    });

    it('deve calcular mesesDeResidencia ao iniciar', function () {
        var bindings = {
            endereco: { resideDesde:  moment().add(-11, 'months') }
        };

        var ctrl = $componentController('dadosEndereco', null, bindings);
        ctrl.$onInit();

        expect(ctrl.mesesDeResidencia).toBe(11);
    });

    it('deve carregar as cidades ao iniciar', function () {
        var bindings = {
            endereco: { estado: 'RS'}
        };

        spyOn(cepFactory, 'cidades').and.callThrough();

        var ctrl = $componentController('dadosEndereco', null, bindings);
        ctrl.$onInit();

        expect(cepFactory.cidades).toHaveBeenCalled();
    });

    it('não deve carregar as cidades ao iniciar', function () {
        var bindings = {
            endereco: { estado: undefined }
        };

        spyOn(cepFactory, 'cidades').and.callThrough();

        var ctrl = $componentController('dadosEndereco', null, bindings);
        ctrl.$onInit();

        expect(cepFactory.cidades).not.toHaveBeenCalled();
    });

    it('deve setar endereço padrão se não for definido', function () {
        var bindings = {
            endereco: undefined
        };

        var enderecoPadrao = {
            tipoEndereco: 'RESIDENCIAL',
            principal: true,
            empostamento: true
        }

        ctrl = $componentController('dadosEndereco', null, bindings);
        ctrl.$onInit();

        expect(ctrl.endereco).toEqual(enderecoPadrao);
    });

    it("deve limpar campos quando selecionar endereço Principal", function () {
        ctrl.limparCamposPrincipal();

        expect(ctrl.deveHabilitarValorAluguel).toBe(false);
        expect(ctrl.endereco.resideDesde).toBe(null);
        expect(ctrl.endereco.situacaoEndereco).toBe(null);
        expect(ctrl.endereco.valorAluguel).toBe(null);
    });

});
