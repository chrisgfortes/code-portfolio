describe('Component: sessaoEndereco', function () {

    var $componentController,
        dialogs,
        $q,
        $rootScope,
        ctrl,
        cepFactory;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _dialogs_, _$q_, _$rootScope_, _cepFactory_) {
        $componentController = _$componentController_;
        dialogs = _dialogs_;
        $q = _$q_;
        $rootScope = _$rootScope_;
        cepFactory = _cepFactory_;

        var bindings = {
            enderecos: [],
            readonly: false
        };

        ctrl = $componentController('sessaoEndereco',
            {
                dialogs: _dialogs_
            }, bindings);

        ctrl.$onInit();
    }));

    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o bindings', function () {
        expect(ctrl.enderecos).toBeDefined();
        expect(ctrl.readonly).toBeDefined();
    });

    it('deve carregarCidade', function () {
        var enderecos = [
            { codigoCidade:7585 }
        ];

        var bindings = {
            enderecos: enderecos
        };

        ctrl = $componentController('sessaoEndereco',null, bindings);

        var cidades = [{
            codigoCidade:7585,
            nomeCidade: 'Charqueadas'
        }];

        spyOn(cepFactory, 'cidades').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: cidades }); }
            };
        });

        ctrl.$onInit();

        expect(ctrl.enderecos[0].cidade).toBeDefined();
    });

    it('deve definir o endereços', function () {
        ctrl.enderecos = undefined;

        ctrl.$onInit();

        expect(ctrl.enderecos).toBeDefined();
    });

    it('deve SALVAR', function () {
        var princial = {
            cep: '123456789',
            modoEdicao: false
        };

        ctrl.mostrarEnderecoForm = true;

        ctrl.salvar(princial);

        expect(ctrl.enderecos.length).toBe(1);
        expect(ctrl.mostrarEnderecoForm).toBe(false);
    });

    it('deve EDITAR', function () {
        var principal = {
            cep: '9999999999',
            modoEdicao: true
        };

        var principalEditado = {
            cep: '11111111111',
            modoEdicao: true
        };

        var coadijuvante = {
            cep: '00000000000',
            modoEdicao: false
        };

        ctrl.enderecos.push(principal);
        ctrl.enderecos.push(coadijuvante);

        ctrl.mostrarEnderecoForm = true;

        ctrl.salvar(principalEditado);

        expect(ctrl.enderecos[0].cep).toBe('11111111111');
        expect(ctrl.enderecos[0].modoEdicao).toBe(false);
        expect(ctrl.enderecos[1]).toEqual(coadijuvante);
        expect(ctrl.enderecos.length).toBe(2);
        expect(ctrl.mostrarEnderecoForm).toBe(false);
    });

    it('deve CANCELAR', function () {
        var principal = {
            cpe: '12345789',
            modoEdicao: true
        };

        ctrl.mostrarEnderecoForm = true;

        ctrl.enderecos.push(principal);

        ctrl.cancelar();

        expect(ctrl.enderecos.length).toBe(1);
        expect(ctrl.enderecos[0].modoEdicao).toBe(false);
        expect(ctrl.mostrarEnderecoForm).toBe(false);
    });

    it('deve NOVO formulário', function () {
        ctrl.mostrarEnderecoForm = false;

        ctrl.novo();

        expect(ctrl.mostrarEnderecoForm).toBe(true);
    });

    it('deve ficar em MODO EDIÇÃO', function () {
        ctrl.mostrarEnderecoForm = false;
        ctrl.endereco = null;

        var principal = {
            cpe: '123457891',
            modoEdicao: true
        };

        ctrl.editar(principal);

        expect(ctrl.mostrarEnderecoForm).toBe(true);
        expect(ctrl.endereco).toEqual(principal);
    });

    it('deve REMOVER', function () {
        var deferred = $q.defer();
        deferred.resolve('yes');

        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var princial = {
            cep: '9999999999',
            modoEdicao: false,
            modoExclusao: false
        };

        var coadijuvante = {
            cep: '1111111111',
            modoExclusao: true
        };

        ctrl.enderecos.push(princial);
        ctrl.enderecos.push(coadijuvante);

        ctrl.remover(coadijuvante);

        $rootScope.$apply();

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.enderecos.length).not.toBe(2);
        expect(ctrl.enderecos.length).toBe(1);
        expect(ctrl.enderecos[0]).toEqual(princial);
    });

    it('deve cancelar a REMOÇÃO', function () {
        var deferred = $q.defer();
        deferred.reject();

        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var princial = {
            cep: '9999999999',
            modoEdicao: false,
            modoExclusao: false
        };

        var coadijuvante = {
            cep: '1111111111',
            modoExclusao: true
        };

        ctrl.enderecos.push(princial);
        ctrl.enderecos.push(coadijuvante);

        ctrl.remover(coadijuvante);

        $rootScope.$apply();

        var todosDesabilitados = ctrl.enderecos
            .some(function (item) {
                return !item.modoExclusao;
            });

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.enderecos.length).toBe(2);
        expect(todosDesabilitados).toBe(true);
    });

    it('deve DESABILITAR principal', function () {
        var principal = {
            cep: '9999999999',
            modoEdicao: true,
            principal: false
        };

        var coadijuvante = {
            cep: '00000000000',
            modoEdicao: false,
            principal: true,
            situacaoEndereco: 'Própria',
            resideDesde: '2017-01-01'
        };

        var principalEditado = {
            cep: '11111111111',
            modoEdicao: true,
            principal: true,
            situacaoEndereco: 'Própria',
            resideDesde: '2017-02-02'
        };

        ctrl.enderecos.push(principal);
        ctrl.enderecos.push(coadijuvante);

        ctrl.salvar(principalEditado);

        expect(ctrl.enderecos[0].principal).toBe(true);
        expect(ctrl.enderecos[0].situacaoEndereco).toBe('Própria');
        expect(ctrl.enderecos[0].resideDesde).toBe('2017-02-02');
        expect(ctrl.enderecos[1].principal).toBe(false);
        expect(ctrl.enderecos[1].situacaoEndereco).toBeNull();
        expect(ctrl.enderecos[1].resideDesde).toBeNull();
        expect(ctrl.enderecos.length).toBe(2);
    });

    it('deve DESABILITAR principal', function () {
        var principal = {
            cep: '9999999999',
            modoEdicao: true,
            empostamento: false
        };

        var coadijuvante = {
            cep: '00000000000',
            modoEdicao: false,
            empostamento: true
        };

        var principalEditado = {
            cep: '11111111111',
            modoEdicao: true,
            empostamento: true
        };

        ctrl.enderecos.push(principal);
        ctrl.enderecos.push(coadijuvante);

        ctrl.salvar(principalEditado);

        expect(ctrl.enderecos[0].empostamento).toBe(true);
        expect(ctrl.enderecos[1].empostamento).toBe(false);
        expect(ctrl.enderecos.length).toBe(2);
    });
});
