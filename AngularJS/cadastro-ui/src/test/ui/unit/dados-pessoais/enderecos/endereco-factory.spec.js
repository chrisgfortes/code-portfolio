describe('factory: endereco', function() {

    var endereco,
        httpBackend,
        HOST;

    beforeEach(module('app'));

    beforeEach(inject(function($httpBackend, _endereco_, _HOST_) {
        endereco = _endereco_;
        HOST = _HOST_;
        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));

    afterEach(function() {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.tipos()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.pessoa + '/endereco/tipos')
            .respond(200, modelsResponse);

        endereco
            .tipos()
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.situacoes()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.pessoa + '/endereco/situacoes-endereco')
            .respond(200, modelsResponse);

        endereco
            .situacoes()
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });
});