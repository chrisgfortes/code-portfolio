describe('Componente DADOS FILIACAO', function() {

    var $componentController,
        ctrl;

    beforeEach(module('app'));

    beforeEach(inject(function(_$componentController_) {
        $componentController = _$componentController_;

        ctrl = $componentController('dadosFiliacao');

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o filiacao no bindings', function() {
        expect(ctrl.filiacao).toBeDefined();
    });

    it('deve definir valor padrão para a filiacao', function() {
        ctrl.filiacao = undefined;

        ctrl.$onInit();

        expect(ctrl.filiacao).toEqual({ nomePaiNaoDeclarado: false, nomeMaeNaoDeclarado: false });
    });

    it('deve habilitar campo nome pai', function() {
        expect(ctrl.filiacao.nomePaiNaoDeclarado).toBe(false);
    });

    it('deve habilitar campo nome mae', function() {
        expect(ctrl.filiacao.nomeMaeNaoDeclarado).toBe(false);
    });

    it('deve limpar o atributo nome pai ao desabilitar', function() {
        ctrl.filiacao.nomePai = "Nome Teste";
        ctrl.filiacao.nomePaiNaoDeclarado = true;
        
        ctrl.desabilitarNomeDoPai();
        
        expect(ctrl.filiacao.nomePai).toBe("");
    });

    it('não deve limpar o atributo nome pai', function() {
        ctrl.filiacao.nomePai = "Nome Teste";
        ctrl.filiacao.nomePaiNaoDeclarado = false;

        ctrl.desabilitarNomeDoPai();
        
        expect(ctrl.filiacao.nomePai).toBe("Nome Teste");
    });

    it('deve limpar o atributo nome mãe ao desabilitar', function() {
        ctrl.filiacao.nomeMae = "Nome Teste";
        ctrl.filiacao.nomeMaeNaoDeclarado = true;
        ctrl.desabilitarNomeDaMae();
        expect(ctrl.filiacao.nomeMae).toBe("");
    });

    it('não deve limpar o atributo nome mãe', function() {
        ctrl.filiacao.nomeMae = "Nome da mãe";
        ctrl.filiacao.nomeMaeNaoDeclarado = false;
       
        ctrl.desabilitarNomeDaMae();
       
        expect(ctrl.filiacao.nomeMae).toBe("Nome da mãe");
    });
});
