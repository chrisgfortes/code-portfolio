
describe('Service: dependenteConvert', function () {

    var dependenteConvert;
    var moment;

    beforeEach(module('app'));

    beforeEach(inject(function (_dependenteConvert_, _moment_) {
        dependenteConvert = _dependenteConvert_;
        moment = _moment_;
    }));

    it('deve estar definido', function () {
        expect(dependenteConvert).toBeDefined();
    });

    it('deve converter para fazer POST', function () {

        var dependente = {
            cpf: "00360741010",
            nomeCompleto: "minha filha",
            dataNascimento: moment("2010-02-01T02:00:00.000Z"),
            tipoDependencia: "FILHO_FILHA",
            valorRenda: 4560,
            valorPensao: 2000
        };

        var dependenteConvertido = dependenteConvert.post(dependente);

        var dependenteEsperado = {
            cpf: "00360741010",
            nomeCompleto: "minha filha",
            dataNascimento: "2010-02-01T02:00:00.000Z",
            tipoDependencia: "FILHO_FILHA",
            valorRenda: 4560,
            valorPensao: 2000
        };

        expect(dependenteConvertido).toEqual(dependenteEsperado)
    });
});