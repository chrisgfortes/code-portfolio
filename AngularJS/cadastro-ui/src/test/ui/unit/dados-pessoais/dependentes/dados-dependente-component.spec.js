describe('Component: dadosDependente', function () {

    var $componentController,
        dependente,
        ctrl,
        moment;

    var tiposDependencia = [{}];

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _dependente_, _moment_) {
        $componentController = _$componentController_;
        dependente = _dependente_;
        moment = _moment_;

        var bindings = {
            dependente: {},
            readonly: false
        };

        spyOn(dependente, 'tipos').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: tiposDependencia }); }
            };
        });

        ctrl = $componentController('dadosDependente', {
            dependente: _dependente_
        }, bindings);

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o bindings', function () {
        expect(ctrl.dependente).toBeDefined();
    });

    it('deve inicialiar o dependente', function () {
        ctrl.dependente = undefined;

        ctrl.$onInit();
        
        expect(ctrl.dependente).toBeDefined();
    });

    it('deve chamar dependente.tipos()', function () {
        expect(dependente.tipos).toHaveBeenCalled();
        expect(ctrl.tiposDependencia).toEqual(tiposDependencia);
    });

    it('deve chamar o evento onSalvar', function () {
        var onSalvarSpy = jasmine.createSpy('onSalvar');

        var bindings = {
            onSalvar: onSalvarSpy
        };

        var ctrl = $componentController('dadosDependente', null, bindings);

        ctrl.salvar();

        expect(onSalvarSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onCancelar', function () {
        var onCancelarSpy = jasmine.createSpy('onCancelar');

        var bindings = {
            onCancelar: onCancelarSpy
        };

        var ctrl = $componentController('dadosDependente', null, bindings);

        ctrl.cancelar();

        expect(onCancelarSpy).toHaveBeenCalled();
    });

    it('deve passar uma data valida', function () {
        var data = moment('1980-01-08');
        ctrl.dependente.dataNascimento = moment()

        ctrl.validarData(data);

        expect(ctrl.dependente.dataNascimento).not.toEqual({});
    });

    it('deve validar a data de nascimento', function () {
        var data = '1980-02-50';

        ctrl.validarData(data);

        expect(ctrl.dependente.dataNascimento).toEqual({});
    });
});
