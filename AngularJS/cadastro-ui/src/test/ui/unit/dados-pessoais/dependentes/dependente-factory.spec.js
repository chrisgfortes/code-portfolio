describe('factory: dependente', function() {

    var dependente,
        HOST,
        httpBackend;

    beforeEach(module('app'));

    beforeEach(inject(function($httpBackend, _dependente_, _HOST_) {
        dependente = _dependente_;
        HOST = _HOST_;

        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));

    afterEach(function() {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.tipos()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.pessoa + '/fisica/dependente/tipos')
            .respond(200, modelsResponse);

        dependente
            .tipos()
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });
});