describe('Component: listaDependentes', function () {

    var $componentController,
        ctrl;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_) {
        $componentController = _$componentController_;

        var bindings = {
            dependentes: [],
            readonly: false,
            esconderBotaoNovo: false
        };

        ctrl = $componentController('listaDependentes',
            null,
            bindings
        );

        ctrl.$onInit();
    }));

    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o bindings', function () {
        expect(ctrl.dependentes).toBeDefined();
        expect(ctrl.readonly).toBeDefined();
        expect(ctrl.esconderBotaoNovo).toBeDefined();
    });

    it('deve chamar o evento onNovo', function () {
        var onNovoSpy = jasmine.createSpy('onNovo');

        var bindings = {
            onNovo: onNovoSpy
        };

        var ctrl = $componentController('listaDependentes', null, bindings);

        ctrl.novo();

        expect(onNovoSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onEditar', function () {
        var onEditarSpy = jasmine.createSpy('onEditar');

        var bindings = {
            onEditar: onEditarSpy
        };

        var ctrl = $componentController('listaDependentes', null, bindings);

        ctrl.dependentes = [{ modoEdicao: false }];

        var principal = {
            modoEdicao: false
        };

        var esperado = {
            dependente: {
                modoEdicao: true
            }
        };

        ctrl.editar(principal);

        expect(onEditarSpy).toHaveBeenCalledWith(esperado);
        expect(principal.modoEdicao).toBe(true);
    });

    it('deve chamar o evento onRemover', function () {
        var onRemoverSpy = jasmine.createSpy('onRemover');

        var bindings = {
            onRemover: onRemoverSpy
        };

        var ctrl = $componentController('listaDependentes', null, bindings);

        var principal = {
            modoExclusao: false
        };

        var esperado = {
            dependente: {
                modoExclusao: true
            }
        };

        ctrl.remover(principal);

        expect(onRemoverSpy).toHaveBeenCalledWith(esperado);
        expect(principal.modoExclusao).toBe(true);
    });
});