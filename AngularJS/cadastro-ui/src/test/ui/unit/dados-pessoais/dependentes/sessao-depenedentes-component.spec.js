describe('Component: sessaoDependentes', function () {

    var $componentController,
        dialogs,
        $q,
        $rootScope,
        $routeParams,
        ctrl;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _dialogs_, _$q_, _$rootScope_, _$routeParams_) {
        $componentController = _$componentController_;
        dialogs = _dialogs_;
        $q = _$q_;
        $rootScope = _$rootScope_;
        $routeParams = _$routeParams_;

        var bindings = {
            dependentes: [],
            readonly: false
        };

        ctrl = $componentController('sessaoDependentes',
            {
                dialogs: _dialogs_
            }, bindings);

        ctrl.$onInit();
    }));

    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o bindings', function () {
        expect(ctrl.dependentes).toBeDefined();
        expect(ctrl.readonly).toBeDefined();
    });

    it('deve fazer o build de dependentes', function () {
        ctrl.dependentes = [{ dataNascimento: '2000-01-01'}];

        ctrl.$onInit();

        var esperado = moment.isMoment(ctrl.dependentes[0].dataNascimento);

        expect(esperado).toBe(true);
    });

    it('deve inicializar os dependentes', function () {
        ctrl.dependentes = undefined;

        ctrl.$onInit();

        expect(ctrl.dependentes).toBeDefined();
    });

    it('deve SALVAR', function () {
        var princial = {
            cpf: '123456789',
            modoEdicao: false
        };

        ctrl.mostrarDependenteForm = true;

        ctrl.salvar(princial);

        expect(ctrl.dependentes.length).toBe(1);
        expect(ctrl.mostrarDependenteForm).toBe(false);
    });

    it('deve EDITAR', function () {
        var principal = {
            cpf: '9999999999',
            modoEdicao: true
        };

        var principalEditado = {
            cpf: '11111111111',
            modoEdicao: true
        };

        var coadijuvante = {
            cpf: '00000000000',
            modoEdicao: false
        };

        ctrl.dependentes.push(principal);
        ctrl.dependentes.push(coadijuvante);

        ctrl.mostrarDependenteForm = true;

        ctrl.salvar(principalEditado);

        expect(ctrl.dependentes[0].cpf).toBe('11111111111');
        expect(ctrl.dependentes[0].modoEdicao).toBe(false);
        expect(ctrl.dependentes[1]).toEqual(coadijuvante);
        expect(ctrl.dependentes.length).toBe(2);
        expect(ctrl.mostrarDependenteForm).toBe(false);
    });

    it('deve CANCELAR', function () {
        var principal = {
            cpf: '12345789',
            modoEdicao: true
        };

        ctrl.mostrarDependenteForm = true;

        ctrl.dependentes.push(principal);

        ctrl.cancelar();

        expect(ctrl.dependentes.length).toBe(1);
        expect(ctrl.dependentes[0].modoEdicao).toBe(false);
        expect(ctrl.mostrarDependenteForm).toBe(false);
    });

    it('deve NOVO formulário', function () {
        ctrl.mostrarDependenteForm = false;

        ctrl.novo();

        expect(ctrl.mostrarDependenteForm).toBe(true);
    });

    it('deve ficar em MODO EDIÇÃO', function () {
        ctrl.mostrarDependenteForm = false;
        ctrl.dependente = null;

        var principal = {
            cpf: '123457891',
            modoEdicao: true
        };

        ctrl.editar(principal);

        expect(ctrl.mostrarDependenteForm).toBe(true);
        expect(ctrl.dependente).toEqual(principal);
    });

    it('deve REMOVER', function () {
        var deferred = $q.defer();
        deferred.resolve('yes');

        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var princial = {
            cpf: '9999999999',
            modoEdicao: false,
            modoExclusao: false
        };

        var coadijuvante = {
            cpf: '1111111111',
            modoExclusao: true
        };

        ctrl.dependentes.push(princial);
        ctrl.dependentes.push(coadijuvante);

        ctrl.remover(coadijuvante);

        $rootScope.$apply();

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.dependentes.length).not.toBe(2);
        expect(ctrl.dependentes.length).toBe(1);
        expect(ctrl.dependentes[0]).toEqual(princial);
    });

    it('deve cancelar a REMOÇÃO', function () {
        var deferred = $q.defer();
        deferred.reject();

        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var princial = {
            cpf: '9999999999',
            modoEdicao: false,
            modoExclusao: false
        };

        var coadijuvante = {
            cpf: '1111111111',
            modoExclusao: true
        };

        ctrl.dependentes.push(princial);
        ctrl.dependentes.push(coadijuvante);

        ctrl.remover(coadijuvante);

        $rootScope.$apply();

        var todosDesabilitados = ctrl.dependentes
            .some(function (item) {
                return !item.modoExclusao;
            });

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.dependentes.length).toBe(2);
        expect(todosDesabilitados).toBe(true);
    });

    it('deve validar o CPF', function () {
        spyOn(dialogs, 'notify').and.callFake(function () {
            return {
                result: { then: function (callback) { return callback({  }); }}
            };
        });

        var principal = {
            cpf: '53437555340',
            modoEdicao: true
        };

        var principalEditado = {
            cpf: '07458207000',
            modoEdicao: true
        };

        var coadijuvante = {
            cpf: '07458207000',
            modoEdicao: false
        };

        ctrl.dependentes.push(principal);
        ctrl.dependentes.push(coadijuvante);

        ctrl.salvar(principalEditado);

        expect(dialogs.notify).toHaveBeenCalledWith('Atenção', 'O CPF já foi cadastrado');
    });
});
