describe('factory: contato', function() {

    var contato,
        HOST,
        httpBackend;

    beforeEach(module('app'));

    beforeEach(inject(function($httpBackend, _contato_, _HOST_) {
        contato = _contato_;
        HOST = _HOST_;

        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));

    afterEach(function() {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.tiposEmail()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.pessoa + '/contato/tipos-email')
            .respond(200, modelsResponse);

        contato
            .tiposEmail()
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.referenciaContatos()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.pessoa + '/contato/tipos-telefone')
            .respond(200, modelsResponse);

        contato
            .referenciaContatos()
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });
});