describe('Component: listaContato', function () {

    var $componentController,
        ctrl;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_) {
        $componentController = _$componentController_;

        var bindings = {
            contatos: [],
            readonly: false,
            esconderBotaoNovo: false
        };

        ctrl = $componentController('listaContato',
            null,
            bindings
        );

        ctrl.$onInit();
    }));

    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o bindings', function () {
        expect(ctrl.contatos).toBeDefined();
        expect(ctrl.readonly).toBeDefined();
        expect(ctrl.esconderBotaoNovo).toBeDefined();
    });

    it('deve chamar o evento onNovo', function () {
        var onNovoSpy = jasmine.createSpy('onNovo');

        var bindings = {
            onNovo: onNovoSpy
        };

        var ctrl = $componentController('listaContato', null, bindings);

        ctrl.novo();

        expect(onNovoSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onRemover', function () {
        var onRemoverSpy = jasmine.createSpy('onRemover');

        var bindings = {
            onRemover: onRemoverSpy
        };

        var ctrl = $componentController('listaContato', null, bindings);

        var contato = {
            modoExclusao: false
        };

        var esperado = {
            contato: {
                modoExclusao: true
            }
        };

        ctrl.remover(contato);

        expect(onRemoverSpy).toHaveBeenCalledWith(esperado);
        expect(contato.modoExclusao).toBe(true);
    });

    it('deve chamar o evento onEditar', function () {
        var onEditarSpy = jasmine.createSpy('onEditar');

        var bindings = {
            onEditar: onEditarSpy
        };

        var ctrl = $componentController('listaContato', null, bindings);

        ctrl.contatos = {
            emails: [{ modoEdicao : false }],
            telefones: [{ modoEdicao : false }]
        };

        var contato = ctrl.contatos.emails[0];

        ctrl.editar(contato);

        expect(onEditarSpy).toHaveBeenCalled();
        expect(contato.modoEdicao).toBe(true);
    });
});
