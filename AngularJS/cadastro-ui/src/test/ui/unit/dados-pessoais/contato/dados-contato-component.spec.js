describe('Component: dadosContato', function () {

    var $componentController,
        ctrl,
        contato;

    var tiposEmail = [{}]

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _contato_) {
        $componentController = _$componentController_;
        contato = _contato_;

        var bindings = {
            contato: { tipoContato: 'Email' },
            nome: ''
        }

        spyOn(contato, 'tiposEmail').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: tiposEmail }); }
            };
        });

        ctrl = $componentController('dadosContato' , {
            contato : _contato_
        }, bindings);

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o model no bindings', function () {
        expect(ctrl.contato).toBeDefined();
        expect(ctrl.tipoSelecionado).toBe('Email');
        expect(ctrl.nome).toBeDefined();
    });

    it('deve definir o valor padrão para tipoSelecionado', function () {
        ctrl.contato = undefined;
        ctrl.tipoSelecionado = undefined;

        ctrl.$onInit();

        expect(ctrl.tipoSelecionado).toBe('telefone');
    });

    it('deve chamar contato.tiposEmail()', function () {
        expect(contato.tiposEmail).toHaveBeenCalled();
    });

    it('deve chamar o evento onSalvar', function () {
        var onSalvarSpy = jasmine.createSpy('onSalvar');

        var bindings = {
            onSalvar: onSalvarSpy
        };

        var ctrl = $componentController('dadosContato', null, bindings);

        contato = {
            tipoContato: 'telefone'
        };

        ctrl.salvar(contato);

        expect(onSalvarSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onCancelar', function () {
        var onCancelarSpy = jasmine.createSpy('onCancelar');

        var bindings = {
            onCancelar: onCancelarSpy
        };

        var ctrl = $componentController('dadosContato', null, bindings);

        ctrl.cancelar();

        expect(onCancelarSpy).toHaveBeenCalled();
    });
});
