describe('Component: sessaoContato', function () {

    var $componentController,
        dialogs,
        $q,
        $rootScope,
        focus,
        ctrl;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _dialogs_, _$q_, _$rootScope_, _focus_) {
        $componentController = _$componentController_;
        dialogs = _dialogs_;
        $q = _$q_;
        $rootScope = _$rootScope_;
        focus = _focus_;

        var bindings = {
            contatos: {
                telefones: [],
                emails: []
            },
            readonly: false
        };

        ctrl = $componentController('sessaoContato',
            {
                dialogs: _dialogs_,
                focus: _focus_
            }, bindings);

        ctrl.$onInit();
    }));

    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o bindings', function () {
        expect(ctrl.contatos).toBeDefined();
        expect(ctrl.contatos.telefones).toBeDefined();
        expect(ctrl.contatos.emails).toBeDefined();
        expect(ctrl.readonly).toBeDefined();
    });

    it('deve SALVAR telefone', function () {
        var contato = {
            tipoTelefone: 'RESIDENCIAL',
            numero: '5143801010',
            modoEdicao: false
        };

        ctrl.mostrarContatoForm = true;

        ctrl.salvar(contato);

        expect(ctrl.contatos.telefones.length).toBe(1);
        expect(ctrl.mostrarContatoForm).toBe(false);
    });

    it('deve SALVAR telefone em MODO DE EDIÇÃO', function () {
        var contato = {
            tipoTelefone: 'RESIDENCIAL',
            numero: '5143801010',
            modoEdicao: true
        };
        var contatoSecundario = {
            tipoTelefone: 'RESIDENCIAL',
            numero: '5143801010',
            modoEdicao: false
        };

        ctrl.contatos.telefones = [ contato, contatoSecundario ];

        ctrl.mostrarContatoForm = true;

        ctrl.salvar(contato);

        expect(ctrl.contatos.telefones.length).toBe(2);
        expect(ctrl.mostrarContatoForm).toBe(false);
    });

    it('deve SALVAR email', function () {
        var contato = {
            tipoEmail: 'PESSOAL',
            email: 'teste@hotmail.com',
            modoEdicao: false
        };

        ctrl.mostrarContatoForm = true;

        ctrl.salvar(contato);

        expect(ctrl.contatos.emails.length).toBe(1);
        expect(ctrl.mostrarContatoForm).toBe(false);
    });

    it('deve SALVAR email em MODO DE EDIÇÃO', function () {
        var contato = {
            tipoEmail: 'PESSOAL',
            email: 'teste@hotmail.com',
            modoEdicao: true
        };
        var contatoSecundario = {
            tipoEmail: 'PESSOAL',
            email: 'teste@hotmail.com',
            modoEdicao: false
        };

        ctrl.contatos.emails = [ contato, contatoSecundario ];

        ctrl.mostrarContatoForm = true;

        ctrl.salvar(contato);

        expect(ctrl.contatos.emails.length).toBe(2);
        expect(ctrl.mostrarContatoForm).toBe(false);
    });

    it('deve CANCELAR', function () {
        var contato = {
            tipoEmail: 'PESSOAL',
            email: 'teste@hotmail.com'
        };
        var contatoSecundario = {
            tipoTelefone: 'RESIDENCIAL',
            numero: '5143801010'
        };

        ctrl.mostrarContatoForm = true;

        ctrl.contatos.emails.push(contato);
        ctrl.contatos.telefones.push(contatoSecundario);

        ctrl.cancelar();

        expect(ctrl.contatos.emails.length).toBe(1);
        expect(ctrl.contatos.telefones.length).toBe(1);
        expect(ctrl.mostrarContatoForm).toBe(false);
    });

    it('deve chamar NOVO formulário', function () {
        ctrl.mostrarContatoForm = false;

        ctrl.novo();

        expect(ctrl.mostrarContatoForm).toBe(true);
    });

    it('deve REMOVER telefone', function () {
        var deferred = $q.defer();
        deferred.resolve('yes');
        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var contato = {
            tipoTelefone: 'RESIDENCIAL',
            numero: '5143801010',
            modoExclusao: false
        };

        var coadijuvante = {
            tipoTelefone: 'RESIDENCIAL',
            numero: '5143801010',
            modoExclusao: true
        };

        ctrl.contatos.telefones.push(contato);
        ctrl.contatos.telefones.push(coadijuvante);

        ctrl.remover(coadijuvante);

        $rootScope.$apply();

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.contatos.telefones.length).not.toBe(2);
        expect(ctrl.contatos.telefones.length).toBe(1);
        expect(ctrl.contatos.telefones[0]).toEqual(contato);
    });

    it('deve cancelar a REMOVOÇÃO do telefone', function () {
        var deferred = $q.defer();
        deferred.reject();
        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var contato = {
            tipoTelefone: 'RESIDENCIAL',
            numero: '5143801010',
            modoExclusao: false
        };

        var coadijuvante = {
            tipoTelefone: 'RESIDENCIAL',
            numero: '5143801010',
            modoExclusao: true
        };

        ctrl.contatos.telefones.push(contato);
        ctrl.contatos.telefones.push(coadijuvante);

        ctrl.remover(coadijuvante);

        $rootScope.$apply();

        var todosDesabilitados = ctrl.contatos
            .telefones
            .some(function (telefone) {
                return !telefone.modoExclusao;
            });

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.contatos.telefones.length).toBe(2);
        expect(todosDesabilitados).toBe(true);
    });

    it('deve REMOVER email', function () {
        var deferred = $q.defer();
        deferred.resolve('yes');
        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var contato = {
            tipoEmail: 'PESSOAL',
            email: 'teste@hotmail.com',
            modoEdicao: false
        };

        var coadijuvante = {
            tipoEmail: 'PESSOAL',
            email: 'teste@hotmail.com',
            modoExclusao: true
        };

        ctrl.contatos.emails.push(contato);
        ctrl.contatos.emails.push(coadijuvante);

        ctrl.remover(coadijuvante);

        $rootScope.$apply();

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.contatos.emails.length).not.toBe(2);
        expect(ctrl.contatos.emails.length).toBe(1);
        expect(ctrl.contatos.emails[0]).toEqual(contato);
    });

    it('deve cancelar a REMOVOÇÃO do email', function () {
        var deferred = $q.defer();
        deferred.reject();
        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var contato = {
            tipoEmail: 'PESSOAL',
            email: 'teste@hotmail.com',
            modoEdicao: false
        };

        var coadijuvante = {
            tipoEmail: 'PESSOAL',
            email: 'teste@hotmail.com',
            modoExclusao: true
        };

        ctrl.contatos.emails.push(contato);
        ctrl.contatos.emails.push(coadijuvante);

        ctrl.remover(coadijuvante);

        $rootScope.$apply();

        var todosDesabilitados = ctrl.contatos
            .emails
            .some(function (email) {
                return !email.modoExclusao;
            });

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.contatos.emails.length).toBe(2);
        expect(todosDesabilitados).toBe(true);
    });

    it('deve EDITAR um contato do tipo Email', function () {
        var contato = {
            tipoEmail: 'PESSOAL',
            email: 'teste@hotmail.com',
            modoEdicao: true
        };

        var contatoEditado = {
            tipoEmail: 'PESSOAL',
            email: 'testeEmailNovo@hotmail.com',
            modoEdicao: true
        };

        var contatoSecundario = {
            tipoEmail: 'PESSOAL',
            email: 'contatosecundario@teste.com',
            modoEdicao: false
        };

        ctrl.contatos.emails.push(contato);
        ctrl.contatos.emails.push(contatoSecundario);

        ctrl.mostrarContatoForm = true;

        ctrl.salvar(contatoEditado);

        expect(ctrl.contatos.emails[0].email).toBe('testeEmailNovo@hotmail.com');
        expect(ctrl.contatos.emails[0].modoEdicao).toBe(false);
        expect(ctrl.contatos.emails[1]).toEqual(contatoSecundario);
        expect(ctrl.contatos.emails.length).toBe(2);
        expect(ctrl.mostrarContatoForm).toBe(false);
    });

    it('deve EDITAR um contato do tipo Telefone', function () {
        var contato = {
            tipoTelefone: 'RESIDENCIAL',
            numero: '43801010',
            ddd: '51',
            modoEdicao: true
        };

        var contatoEditado = {
            tipoTelefone: 'RESIDENCIAL',
            numero: '5187000000',
            modoEdicao: true
        };

        var contatoSecundario = {
            tipoTelefone: 'COMERCIAL',
            numero: '98746543',
            ddd: '51',
            modoEdicao: false
        };

        ctrl.contatos.telefones.push(contato);
        ctrl.contatos.telefones.push(contatoSecundario);

        ctrl.mostrarContatoForm = true;

        ctrl.salvar(contatoEditado);

        expect(ctrl.contatos.telefones[0].ddd).toBe('51');
        expect(ctrl.contatos.telefones[0].numero).toBe('87000000');
        expect(ctrl.contatos.telefones[0].modoEdicao).toBe(false);
        expect(ctrl.contatos.telefones[1]).toEqual(contatoSecundario);
        expect(ctrl.contatos.telefones.length).toBe(2);
        expect(ctrl.mostrarContatoForm).toBe(false);
    });

    it('deve ficar em MODO EDIÇÃO o contato tipo Email', function () {
        ctrl.mostrarContatoForm = false;

        var contato = {
            tipoEmail: 'PESSOAL',
            email: 'teste@hotmail.com',
            modoEdicao: true
        };

        ctrl.editar(contato);

        expect(ctrl.mostrarContatoForm).toBe(true);
        expect(ctrl.contato).toEqual(contato);
    });

    it('deve ficar em MODO EDIÇÃO o contato tipo Telefone', function () {
        ctrl.mostrarContatoForm = false;

        var contato = {
            tipoTelefone: 'RESIDENCIAL',
            numero: '5143801010',
            modoExclusao: true
        };

        ctrl.editar(contato);

        expect(ctrl.mostrarContatoForm).toBe(true);
        expect(ctrl.contato).toEqual(contato);
    });
});
