describe('Componente DADOS ESTADO CIVIL', function () {

    var $componentController,
        ctrl,
        pessoa,
        dadosProfissional,
        documento,
        cepFactory;

    var estadoCivil = 'CASADO';
    var conjuge = {
            contatos: {},
            cpf: "25557213805",
            dataEmissao: "2017-03-03",
            dataNascimento: "1994-11-10",
            empresa: "Friboi",
            filiacao: { nomePai: "", nomeMae: "", nomeMaeNaoDeclarado: true, nomePaiNaoDeclarado: true },
            nomeCompleto: "Stephanie Hunt",
            numeroIdentificacao: "5464825",
            orgaoExpedidor: "DETRAN",
            profissao: "ASSISTENTE ADM",
            tipoIdentificacao: "CARTEIRA_MOTORISTA",
            ufExpedidor: "RS",
            valorRenda: 12358.46
        };

    var identificacoes = [];
    var regimeCasamento = [];
    var profissoes = [];
    var estados = [];
    var orgaosExpeditores = [];
    var dialogs;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _pessoa_, _cepFactory_, _dialogs_, _dadosProfissional_, _documento_) {
        $componentController = _$componentController_;
        pessoa = _pessoa_;
        cepFactory = _cepFactory_;
        dialogs = _dialogs_;
        dadosProfissional = _dadosProfissional_;
        documento = _documento_;

        var bindings = {
            estadoCivil: estadoCivil,
            regimeCasamento: '',
            conjuge: {}
        };

        spyOn(documento, 'identificacoes').and.callFake(respostaFake({ data: identificacoes }));

        spyOn(pessoa, 'estadoCivil').and.callFake(respostaFake({ data: estadoCivil }));

        spyOn(pessoa, 'regimeCasamento').and.callFake(respostaFake({ data: regimeCasamento }));

        spyOn(dadosProfissional, 'profissoes').and.callFake(respostaFake({ data: profissoes }));

        spyOn(cepFactory, 'estados').and.callFake(respostaFake({ data: estados }));

        spyOn(documento, 'orgaoemissor').and.callFake(respostaFake({ data: orgaosExpeditores }));

        ctrl = $componentController(
            'dadosEstadoCivil',
            {
                pessoa: _pessoa_,
                cepFactory: _cepFactory_,
                dialogs: _dialogs_
            },
            bindings);

        ctrl.$onInit();
    }));


    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o estadoCivil no bindings', function () {
        expect(ctrl.estadoCivil).toBeDefined();
    });

    it('deve chamar documento.identificacoes()', function () {
        expect(documento.identificacoes).toHaveBeenCalled();
    });

    it('deve chamar pessoa.estadoCivil()', function () {
        expect(pessoa.estadoCivil).toHaveBeenCalled();
    });

    it('deve chamar cepFactory.estados()', function () {
        expect(cepFactory.estados).toHaveBeenCalled();
    });

    it('deve chamar pessoa.regimeCasamento()', function () {
        expect(pessoa.regimeCasamento).toHaveBeenCalled();
    });

    it('deve chamar dadosProfissional.profissoes()', function () {
        expect(dadosProfissional.profissoes).toHaveBeenCalled();
    });

    it('deve aceitar DATA DE EMISSAO válida', function () {
        var dataEmissao = moment('2017-03-09');

        ctrl.validarDataEmissao(dataEmissao);

        expect(ctrl.conjuge.dataEmissao).not.toEqual(null);
    });

    it('não deve aceitar DATA DE EMISSAO inválida', function () {
        var dataEmissao = '2017-32-40';

        ctrl.validarDataEmissao(dataEmissao);

        expect(ctrl.conjuge.dataEmissao).toEqual(null);
    });

    it('deve aceitar DATA DE NASCIMENTO válida', function () {
        var dataNascimento = moment('2000-01-01');

        ctrl.validarDataNascimento(dataNascimento);

        expect(ctrl.conjuge.dataNascimento).not.toEqual(null);
    });

    it('não deve aceitar DATA DE NASCIMENTO inválida', function () {
        var dataNascimento = '2000-01-01';
        var dataEmissao = '2017-12-10';

        ctrl.validarDataNascimento(dataNascimento, dataEmissao);

        expect(ctrl.conjuge.dataNascimento).toEqual(null);
    });

    it('não deve aceitar DATA DE NASCIMENTO menor que a DATA EMISSÃO', function () {
        var dataNascimento = moment('1981-01-01');
        var dataEmissao = moment('1980-01-01');

        ctrl.conjuge.dataEmissao = moment('1980-01-01');
        ctrl.maxDataNascimento = moment();

        ctrl.validarDataNascimento(dataNascimento, dataEmissao);

        expect(ctrl.conjuge.dataEmissao).toEqual(null);
    });

    it('não deve aceitar DATA DE NASCIMENTO se menor de 16 anos', inject(function ($rootScope, $q) {
        var deferred = $q.defer();
        deferred.resolve('yes');
        spyOn(dialogs, 'notify').and.returnValue({ result: deferred.promise });

        var dataNascimento = moment();
        ctrl.maxDataNascimento = moment().subtract(16, 'years');

        ctrl.validarDataNascimento(dataNascimento);

        $rootScope.$apply();

        expect(dialogs.notify).toHaveBeenCalledWith('Atenção', 'O conjuge tem que ser maior de 16 anos.');
        expect(ctrl.conjuge.dataNascimento).toEqual(null);
    }));

    it('deve converter as DATAS', function () {
        var dataNascimento = '2017-98-34';
        ctrl.conjuge = {
            dataEmissao: '1980-01-01',
            dataNascimento: '1981-01-01'
        };

        ctrl.$onInit();

        expect(moment.isMoment(ctrl.conjuge.dataEmissao)).toBe(true);
        expect(moment.isMoment(ctrl.conjuge.dataNascimento)).toBe(true);
    });

    it('deve chamar dadosProfissional.profissoes()', function () {
        ctrl.conjuge = {
            tipoIdentificacao: 'CASADO'
        };

        ctrl.$onInit();

        expect(dadosProfissional.profissoes).toHaveBeenCalled();
        expect(ctrl.orgaosExpeditores).toEqual(orgaosExpeditores);
    });

    it('deve continuar indefinido, o conjuge', function () {
        ctrl.conjuge = undefined;

        ctrl.$onInit();

        expect(ctrl.conjuge).not.toBeDefined();
    });

    it('deve deletar o conjuge e regimeCasamento', function () {
        ctrl.conjuge = {};
        ctrl.regimeCasamento = 'COMUNHAO_BENS';

        ctrl.alterarEstadoCivil('SOLTEIRO');

        expect(ctrl.conjuge).not.toBeDefined();
        expect(ctrl.regimeCasamento).not.toBeDefined();
    });

    it('não deve deletar o conjuge e regimeCasamento', function () {
        ctrl.conjuge = {};
        ctrl.regimeCasamento = 'COMUNHAO_BENS';

        ctrl.alterarEstadoCivil('CASADO');

        expect(ctrl.conjuge).toBeDefined();
        expect(ctrl.regimeCasamento).toBeDefined();

        ctrl.alterarEstadoCivil('UNIAO_ESTAVEL');

        expect(ctrl.conjuge).toBeDefined();
        expect(ctrl.regimeCasamento).toBeDefined();
    });

    it('deve atribuir resultado da pesquisa do cpf ao objeto conjuge', function () {
        var conjuge = {
                contatos: {},
                cpf: "25557213805",
                dataEmissao: "2017-03-03",
                dataNascimento: "1994-11-10",
                empresa: "Friboi",
                filiacao: { nomePai: "", nomeMae: "", nomeMaeNaoDeclarado: true, nomePaiNaoDeclarado: true },
                nomeCompleto: "Stephanie Hunt",
                numeroIdentificacao: "5464825",
                orgaoExpedidor: "DETRAN",
                profissao: "ASSISTENTE ADM",
                tipoIdentificacao: "CARTEIRA_MOTORISTA",
                ufExpedidor: "RS",
                valorRenda: 12358.46
            }

        var conjugeConvertido = {
                contatos: {},
                cpf: "25557213805",
                dataEmissao: moment("2017-03-03"),
                dataNascimento: moment("1994-11-10"),
                empresa: "Friboi",
                filiacao: { nomePai: "", nomeMae: "", nomeMaeNaoDeclarado: true, nomePaiNaoDeclarado: true },
                nomeCompleto: "Stephanie Hunt",
                numeroIdentificacao: "5464825",
                orgaoExpedidor: "DETRAN",
                profissao: "ASSISTENTE ADM",
                tipoIdentificacao: "CARTEIRA_MOTORISTA",
                ufExpedidor: "RS",
                valorRenda: 12358.46
            }

        ctrl.conjuge = {};

        ctrl.atribuirResultado(conjuge);

        expect(ctrl.conjuge).toBeDefined();
        expect(ctrl.conjuge).toEqual(conjugeConvertido);
    });

    it('deve atribuir resultado da pesquisa do cpf ao objeto conjuge', function () {
        var cpf= "25557213805";
        var notFoundObj = {
            notFound:true,
            cpf:cpf
        }
        var conjugePadraoObj = {
            cpf:cpf,
            contatos: {
                telefones: [],
                emails: []
            }
        }
        ctrl.conjuge = {};

        ctrl.atribuirResultado(notFoundObj);

        expect(ctrl.conjuge).toBeDefined();
        expect(ctrl.conjuge).toEqual(conjugePadraoObj);
        expect(ctrl.conjuge.cpf).toEqual(notFoundObj.cpf);
    });
});
