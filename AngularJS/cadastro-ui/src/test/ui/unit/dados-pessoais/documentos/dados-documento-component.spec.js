describe('Componente Dados Documento', function () {

    var $componentController,
        ctrl,
        representanteLegal,
        cepFactory,
        $scope,
        dialogs,
        focus,
        documento,
        moment;

    var representantes = [
        {
            "valor": "NENHUM",
            "descricao": "Nenhum"
        },
        {
            "valor": "PF_MENOR_IDADE",
            "descricao": "Pessoa Física Menor de Idade"
        },
        {
            "valor": "PF_MENOR_EMANCIPADO",
            "descricao": "Pessoa Física Menor Emancipado"
        },
        {
            "valor": "PF_ECONOMICAMENTE_DEPENDENTE",
            "descricao": "Pessoa Física Econ. Dependente"
        },
        {
            "valor": "PF_INCAPAZ",
            "descricao": "Pessoa Física Incapaz"
        }
    ];

    var _documento = {
        dataEmissao:"2017-03-09",
        dataNascimento:"1992-12-31",
        modalidadeRepresentanteLegal:"PF_INCAPAZ",
        nacionalidade:"Brasileiro(a)",
        naturalidade:"Porto Alegre",
        numeroIdentificacao:"5486516",
        orgaoExpedidor:"SSP",
        protocoloBRSafe:65498486,
        sexo:"MASCULINO",
        tipoIdentificacao:"CARTEIRA_IDENTIDADE",
        uf:"RS",
        ufExpedidor:"RS"
    }

    var identificacoes = [];
    var representantesLegal = [];
    var estados = [];
    var paises = [];
    var cidades = [];
    var orgaoemissor = {};
    var onRepresentanteSelecionadoSpy;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _representanteLegal_, _cepFactory_, _moment_,
        _$rootScope_, _dialogs_, _focus_, _documento_) {

        $componentController = _$componentController_;
        representanteLegal = _representanteLegal_;
        cepFactory = _cepFactory_;
        moment = _moment_;
        $scope = _$rootScope_.$new();
        dialogs = _dialogs_;
        focus = _focus_;
        documento = _documento_;

        onRepresentanteSelecionadoSpy = jasmine.createSpy('onRepresentanteSelecionado');

        var bindings = {
            documento: _documento,
            ocultarCampos: ['campo1', 'campo2'],
            onRepresentanteSelecionado: onRepresentanteSelecionadoSpy
        };

        spyOn(documento, 'identificacoes').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: identificacoes }); }
            };
        });
        spyOn(representanteLegal, 'modalidades').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: representantesLegal }); }
            };
        });
        spyOn(documento, 'orgaoemissor').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: orgaoemissor }); }
            };
        });
        spyOn(cepFactory, 'estados').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: estados }); }
            };
        });
        spyOn(cepFactory, 'paises').and.callFake(function () {
            return {
                then: function (callback) {
                    var pais = {descricaoNacionalidade: 'Brasileiro'};
                    paises.push(pais);
                    return callback({ data: paises }); }
            };
        });
        spyOn(cepFactory, 'cidades').and.callFake(function () {
            return {
                then: function (callback) {
                    var cidade = {nomeCidade: 'Porto Alegre'};
                    cidades.push(cidade);
                    return callback({ data: cidades }); }
            };
        });

        ctrl = $componentController('dadosDocumento', {
            representanteLegal: _representanteLegal_,
            cepFactory: _cepFactory_,
            moment: _moment_,
            $scope: $scope,
            dialogs: _dialogs_,
            focus: _focus_,
            documento: _documento_
        }, bindings);

        ctrl.$onInit();
    }));


    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o model no bindings', function () {
        expect(ctrl.documento).toBeDefined();
    });

    it('deve definir o nomeComponente no bindings', function () {
        expect(ctrl.nomeComponente).toBeDefined();
    });

    it('deve chamar documento.identificacoes()', function () {
        expect(documento.identificacoes).toHaveBeenCalled();
    });

    it('deve chamar representanteLegal.modalidades()', function () {
        expect(representanteLegal.modalidades).toHaveBeenCalled();
    });

    it('deve chamar cepFactory.estados()', function () {
        expect(cepFactory.estados).toHaveBeenCalled();
    });

    it('deve chamar cepFactory.paises()', function () {
        expect(cepFactory.paises).toHaveBeenCalled();
    });

    it('deve validar se é MAIOR de idade', function () {
        var dataNascimento = moment().add(-19, 'y').format('DD/MM/YYYY');

        var maiorDeIdade = ctrl.confirmaMenorDeIdade(dataNascimento);

        expect(maiorDeIdade).toBe(true);
    });

    it('deve recusar DATA INVÁLIDA ao validar se é MENOR de idade', function () {
        var dataNascimento = "54/76/2233";
        ctrl.confirmaMenorDeIdade(dataNascimento);
        expect(ctrl.dataInvalida).toBe(true);
    });

    it('deve validar se é MENOR de idade e REPRESENTANTE LEGAL', function () {
        ctrl.representantesLegal = representantes;

        var dataNascimento = moment().add(-10, 'y').format('DD/MM/YYYY');

        var maiorDeIdade = ctrl.confirmaMenorDeIdade(dataNascimento);

        expect(maiorDeIdade).toBe(false);
    });

    it('deve selecionar o REPRESENTANTE LEGAL', function () {
        var dataNascimento = moment().add(-10, 'y').format('DD/MM/YYYY');

        ctrl.representantesLegal = representantes;

        var maiorDeIdade = ctrl.confirmaMenorDeIdade(dataNascimento);

        expect(ctrl.documento.modalidadeRepresentanteLegal.selected.valor).toBe('PF_MENOR_IDADE');
    });

    it('deve liberar acesso do Estado e Nacionalidades', function () {
        var nacionalidade = {
            "id": 7,
            "nomePais": "Brasil",
            "descricaoNacionalidade": "Brasileiro(a)",
            "siglaPais": "BRA"
        };

        ctrl.defineNacionalidade(nacionalidade);

        expect(ctrl.brasileiro).toBe(true);
    });

    it('não deve liberar acesso do Estado e Nacionalidades', function () {
        var nacionalidade = {
            "id": 8,
            "nomePais": "Chile",
            "descricaoNacionalidade": "Chileno(a)",
            "siglaPais": "CHI"
        };

        ctrl.defineNacionalidade(nacionalidade);

        expect(ctrl.brasileiro).toBe(false);
    });

    it('deve disparar evento quando o REPRESENTANTE LEGAL for selecionado', function () {
        var representanteLegal = {
            descricao: 'Pessoa Física Menor de Idade',
            valor: 'PF_MENOR_IDADE'
        };

        ctrl.ocultar = [];
        ctrl.representanteLegalSelecionado(representanteLegal);

        expect(onRepresentanteSelecionadoSpy).toHaveBeenCalledWith({ representante: representanteLegal, mostrarRepresentante: true });
    });

    it('deve aceitar DATA DE NASCIMENTO válida', function () {
        var dataNascimento = moment('2000-01-01');
        var dataEmissao = moment('2000-01-02');
        
        ctrl.onChange(dataNascimento, dataEmissao);
        
        expect(ctrl.documento.dataNascimento).not.toBeUndefined();
    });

    it('não deve aceitar DATA DE NASCIMENTO inválida', function () {
        var dataNascimento = moment('2000-66-98');
        var dataEmissao = moment('2000-01-02');
        ctrl.onChange(dataNascimento, dataEmissao);
        expect(ctrl.documento.dataNascimento).toBeUndefined();
    });

    it('deve limpar DATA DE EMISSAO se data de nascimento for maior que a mesma', function () {
        var dataNascimento = moment('2000-05-01');
        var dataEmissao = moment('2000-01-01');
        ctrl.onChange(dataNascimento, dataEmissao);
        expect(ctrl.documento.dataEmissao).toBeUndefined();
    });

    it('deve aceitar DATA DE EMISSAO válida', function () {
        ctrl.documento.dataEmissao = moment();
        ctrl.validarDataEmissao(ctrl.documento.dataEmissao);
        expect(ctrl.documento.dataEmissao).not.toBeUndefined();
    });

    it('não deve aceitar DATA DE EMISSAO inválida', function () {
        var dataEmissao = moment('2000-23-56');
        ctrl.validarDataEmissao(dataEmissao);
        expect(ctrl.documento.dataEmissao).toBeUndefined();
    });

    it('não deve buscarOrgaosExpeditores se ID for INVÁLIDA', function () {
        ctrl.orgaosExpeditores = [];
        var id = false;
        ctrl.buscarOrgaosExpeditores(id);
        expect(ctrl.orgaosExpeditores.length).toBe(0);
    });

    it('deve buscarOrgaosExpeditores se ID for VÁLIDA', function () {
        ctrl.orgaosExpeditores = [];
        var id = 'CARTEIRA_MOTORISTA';
        ctrl.documento.dataNascimento = moment();
        ctrl.buscarOrgaosExpeditores(id);
        expect(ctrl.orgaosExpeditores.length).not.toBe(0);
    });

    it('não deve carregarCidades se UF for INVÁLIDA', function () {
        ctrl.documento.naturalidade = {};
        var uf = false;
        ctrl.carregarCidades(uf);
        expect(ctrl.documento.naturalidade).toEqual({});
    });

    it('deve carregarCidades se UF for VÁLIDA', function () {
        ctrl.cidades= [{
            nomeCidade: 'Porto Alegre'
        }];

        var uf = 'RS';
        ctrl.carregarCidades(uf);
        expect(ctrl.documento.naturalidade).not.toEqual({});
    });

    it('não deve buscaNacionalidades se pesquisa for vazia', function () {
        ctrl.nacionalidades = [{descricaoNacionalidade: 'Brasileiro'}];
        var select = false;
        ctrl.buscaNacionalidades(select);
        expect(ctrl.nacionalidades.length).toBe(0);
    });

    it('deve buscaNacionalidades se pesquisa não for vazia', function () {
        ctrl.paises= [
            { descricaoNacionalidade: 'Brasileiro' },
            { descricaoNacionalidade: 'Argentino' }
        ];
        var select = {search:'Argentino'};
        ctrl.buscaNacionalidades(select);
        expect(ctrl.nacionalidades.length).toBe(1);
    });

    it('não deve buscaNaturalidade se pesquisa for vazia', function () {
        ctrl.nacionalidades = [{descricaoNacionalidade: 'Brasileiro'}];
        var select = false;
        ctrl.buscaNaturalidade(select);
        expect(ctrl.naturalidades.length).toBe(0);
    });

    it('deve buscaNaturalidade se pesquisa não for vazia', function () {
        ctrl.cidades= [
            { nomeCidade: 'Porto Alegre' },
            { nomeCidade: 'Charqueadas' }
        ];
        var select = {search:'Porto Alegre'};
        ctrl.buscaNaturalidade(select);
        expect(ctrl.naturalidades.length).toBe(1);
    });

    it('deve ocultarCampos se ocultarCampos[] não for vazio', function () {
        expect(ctrl.ocultar['campo1']).toBe(true);
        expect(ctrl.ocultar['campo2']).toBe(true);
    });

    it('não deve ocultarCampos se ocultarCampos[] for vazio', function () {
        ctrl.ocultarCampos = undefined;
        ctrl.$onInit();
        expect(ctrl.ocultar['campo1']).toBe(undefined);
        expect(ctrl.ocultar['campo2']).toBe(undefined);
    });

    it('não deve aceitar se documento for CARTEIRA DE MOTORISTA e DATA DE NASCIMENTO menor de 18 anos', inject(function ($rootScope, $q) {
        var deferred = $q.defer();
        deferred.resolve('yes');
        spyOn(dialogs, 'notify').and.returnValue({ result: deferred.promise });

        ctrl.documento.dataNascimento = moment();
        var id = 'CARTEIRA_MOTORISTA';

        ctrl.buscarOrgaosExpeditores(id);
        $rootScope.$apply();
        expect(dialogs.notify).toHaveBeenCalledWith('Atenção', 'Para possuir carteira de motorista o cadastrado deve ser maior de 18 anos.');
    }));

});
