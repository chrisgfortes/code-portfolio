describe('factory: documento', function() {

    var documento,
        HOST,
        httpBackend;

    beforeEach(module('app'));

    beforeEach(inject(function($httpBackend, _documento_, _HOST_) {
        documento = _documento_;
        HOST = _HOST_;

        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));

    afterEach(function() {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.identificacoes()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.pessoa + '/documento/identificacoes')
            .respond(200, modelsResponse);

        documento
            .identificacoes()
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.orgaoemissor(id)', function() {
        var modelsResponse = [{}];
        var id = 'ID';

        httpBackend
            .expectGET(HOST.pessoa + '/documento/orgaos-emissores?documento=' + id)
            .respond(200, modelsResponse);

        documento
            .orgaoemissor(id)
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });
});