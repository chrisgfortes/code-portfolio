
describe('Service: documentoConvert', function () {

    var documentoConvert;
    var moment;

    beforeEach(module('app'));

    beforeEach(inject(function (_documentoConvert_, _moment_) {
        documentoConvert = _documentoConvert_;
        moment = _moment_;
    }));

    it('deve estar definido', function () {
        expect(documentoConvert).toBeDefined();
    });

    it('deve converter para fazer POST', function () {

        var documento = {
            modalidadeRepresentanteLegal: {
                selected: {
                    valor: "PF_MENOR_EMANCIPADO",
                    descricao: "Pessoa Física Menor Emancipado"
                }
            },
            nacionalidade: {
                id: 2,
                nomePais: "Argentina",
                descricaoNacionalidade: "Argentino(a)",
                siglaPais: "ARG"
            },
            tipoIdentificacao: "CARTEIRA_IDENTIDADE",
            numeroIdentificacao: "123",
            orgaoExpedidor: "SSP",
            ufExpedidor: "AC",
            dataNascimento: "1980-01-01T03:00:00.000Z",
            dataEmissao: "1981-01-01T03:00:00.000Z",
            protocoloBRSafe: 123,
            sexo: "MASCULINO",
            uf: "RS",
            naturalidade: {
                selected: {
                    uf: "RS",
                    codigoCidade: 7748,
                    nomeCidade: "Gravataí"
                }
            }
        };

        var documentoConvertido = documentoConvert.post(documento);

        var documentoEsperado = {
            modalidadeRepresentanteLegal: "PF_MENOR_EMANCIPADO",
            nacionalidade: "Argentino(a)",
            tipoIdentificacao: "CARTEIRA_IDENTIDADE",
            numeroIdentificacao: "123",
            orgaoExpedidor: "SSP",
            ufExpedidor: "AC",
            dataNascimento: "1980-01-01T03:00:00.000Z",
            dataEmissao: "1981-01-01T03:00:00.000Z",
            protocoloBRSafe: 123,
            sexo: "MASCULINO"
        };

        expect(documentoConvertido).toEqual(documentoEsperado)
    });

    it('deve converter para fazer POST, selecionar naturalidade', function () {

        var documento = {
            modalidadeRepresentanteLegal: {
                selected: {
                    valor: "PF_MENOR_EMANCIPADO",
                    descricao: "Pessoa Física Menor Emancipado"
                }
            },
            nacionalidade: {
                id: 7,
                nomePais: "Brasil",
                descricaoNacionalidade: "Brasileiro(a)",
                siglaPais: "BRA"
            },
            tipoIdentificacao: "CARTEIRA_IDENTIDADE",
            numeroIdentificacao: "123",
            orgaoExpedidor: "SSP",
            ufExpedidor: "AC",
            dataNascimento: "1980-01-01T03:00:00.000Z",
            dataEmissao: "1981-01-01T03:00:00.000Z",
            protocoloBRSafe: 123,
            sexo: "MASCULINO",
            uf: "RS",
            naturalidade: {
                selected: {
                    uf: "RS",
                    codigoCidade: 7748,
                    nomeCidade: "Gravataí"
                }
            }
        };

        var documentoConvertido = documentoConvert.post(documento);

        var documentoEsperado = {
            modalidadeRepresentanteLegal: "PF_MENOR_EMANCIPADO",
            nacionalidade: "Brasileiro(a)",
            tipoIdentificacao: "CARTEIRA_IDENTIDADE",
            numeroIdentificacao: "123",
            orgaoExpedidor: "SSP",
            ufExpedidor: "AC",
            dataNascimento: "1980-01-01T03:00:00.000Z",
            dataEmissao: "1981-01-01T03:00:00.000Z",
            protocoloBRSafe: 123,
            sexo: "MASCULINO",
            uf: "RS",
            naturalidade: "Gravataí"
        };

        expect(documentoConvertido).toEqual(documentoEsperado)
    });
});