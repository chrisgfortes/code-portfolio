describe('Router: cadastroController', function () {

    var $route,
        secao,
        $rootScope;

    beforeEach(module('app'));

    beforeEach(inject(function (_$route_, _secao_, _$rootScope_) {
        $route = _$route_;
        $rootScope = _$rootScope_;
        secao = _secao_;

        rota = $route.routes['/cadastro'];
    }));

    it('Deve testar alteracaoRouter', function() {
        expect(rota.controller).toBe('cadastroController');
        expect(rota.templateUrl).toBe('views/pages/cadastro.html');
    });

    it('Deve chamar resolve', function () {
        rota.resolve.section(secao);
        $rootScope.$apply();

        expect(secao.nome).toBeDefined();
    });
});