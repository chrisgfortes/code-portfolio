describe('Router: defaultRouter', function () {

    var AuthFactory, $route, $q, usuarioFactory, $location;

    beforeEach(module('app'));

    beforeEach(inject(function (_$rootScope_, _AuthFactory_, _$route_, _$q_, _usuarioFactory_, _$location_) {
        $scope = _$rootScope_.$new();
        AuthFactory = _AuthFactory_;
        $route = _$route_;
        $q = _$q_;
        usuarioFactory = _usuarioFactory_;
        $location = _$location_;

        spyOn(usuarioFactory, 'token').and.callFake(function () {
            return userToken();
        });

        spyOn(usuarioFactory, 'tokenLegacy').and.callFake(function () {
            return userTokenLegacy();
        });

        spyOn(usuarioFactory, 'cooperativa').and.callFake(function () {
            return {data:{value:566}};
        });

        spyOn(usuarioFactory, 'autorizacao').and.callFake(respostaFake({ data: permissoesFake() }));
    }));

    it('Deve testar defaultRouter para rota inexistente', inject(function ($location, $rootScope, $route) {
        $location.path('/rota-nao-existente');

        $rootScope.$digest();

        expect($route.current.originalPath).toBe('/404');
    }));

    it('Deve testar rota /', function () {
        var rota = $route.routes['/'];

        spyOn(AuthFactory, 'salvarToken').and.callThrough();
        spyOn(AuthFactory, 'salvarTokenLegacy').and.callThrough();
        spyOn(AuthFactory, 'salvarCooperativa').and.callThrough();
        spyOn($location, 'path').and.callThrough();

        rota.resolve.salvarInformacoes(usuarioFactory, AuthFactory, $q, $location);

        $scope.$apply();

        expect(AuthFactory.salvarToken).toHaveBeenCalledWith(userToken().data.value);
        expect(AuthFactory.salvarTokenLegacy).toHaveBeenCalledWith(userTokenLegacy().data.value);
        expect(AuthFactory.salvarCooperativa).toHaveBeenCalledWith(566);

        expect(AuthFactory.buscarToken()).toEqual(userToken().data.value);
        expect(AuthFactory.buscarTokenLegacy()).toEqual(userTokenLegacy().data.value);
        expect(AuthFactory.buscarCooperativa()).toEqual('566');

        expect($location.path).toHaveBeenCalledWith('/home');

    });

    it('Deve testar rota /login', function () {
        var rota = $route.routes['/login'];

        spyOn(AuthFactory, 'salvarToken').and.callThrough();
        spyOn(AuthFactory, 'salvarTokenLegacy').and.callThrough();
        spyOn(AuthFactory, 'salvarCooperativa').and.callThrough();
        spyOn($location, 'path').and.callThrough();

        rota.resolve.salvarInformacoes(usuarioFactory, AuthFactory, $q, $location);

        $scope.$apply();

        expect(AuthFactory.salvarToken).toHaveBeenCalledWith(userToken().data.value);
        expect(AuthFactory.salvarTokenLegacy).toHaveBeenCalledWith(userTokenLegacy().data.value);
        expect(AuthFactory.salvarCooperativa).toHaveBeenCalledWith(566);

        expect(AuthFactory.buscarToken()).toEqual(userToken().data.value);
        expect(AuthFactory.buscarTokenLegacy()).toEqual(userTokenLegacy().data.value);
        expect(AuthFactory.buscarCooperativa()).toEqual('566');

        expect($location.path).toHaveBeenCalledWith('/home');
    });

    function userToken() {
        return {data:{"value":"eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICIyTWhzS0xHS2ZRbHJxNlkyc3pFcHVtVm1rR2Y4MmZXTVN6NlJLcnhac3BBIn0."
        + "eyJqdGkiOiIyOTc5YmU1Mi03NTc3LTQzMmEtYmIyYi00Yzg5Y2RkZGRjMzMiLCJleHAiOjE1MDA5ODY4OTIsIm5iZiI6MCwiaWF0IjoxNTAwOTg2MjkyLCJpc3MiOiJodHRwOi8vb2F1"
        + "dGgtdHN0LmUtdW5pY3JlZC5jb20uYnIvYXV0aC9yZWFsbXMvVW5pY3JlZFJlYWxtIiwiYXVkIjoidHN0LWNsaWVudCIsInN1YiI6ImRjNGM5ZGNhLWM3ODEtNGI4OC1hZmFhLTZjNGZjYmEwN"
        + "zkyZCIsInR5cCI6IkJlYXJlciIsImF6cCI6InRzdC1jbGllbnQiLCJhdXRoX3RpbWUiOjE1MDA5ODYyOTIsInNlc3Npb25fc3RhdGUiOiI1MzAzMDc5YS0yMWRkLTRiMDMtOTcwMS1lY2IzYW"
        + "M5YmZkMDkiLCJhY3IiOiIxIiwiY2xpZW50X3Nlc3Npb24iOiIxMmMyM2MwMC03ZDRkLTQzOWItODhkNS05OWU4Y2E0Y2I2YzIiLCJhbGxvd2VkLW9yaWdpbnMiOltdLCJyZWFsbV9hY2Nlc3MiOnsi"
        + "cm9sZXMiOlsiUk9MRV9BRE1JTiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7fSwibmFtZSI6IiIsImRpc3Rpbmd1aXNoZWROYW1lIjoiQ049Y2FtaWxhLE9VPTA1NjYsT1U9U2l0ZTEsT1U9UHJvZHVjYW"
        + "8sT1U9VXN1YXJpb3MsT1U9VFMsREM9ZS11bmljcmVkaG9tb2xvZyxEQz1jb20sREM9YnIiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJjYW1pbGEuMDU2NiJ9.YtpJgpZTD4QJEUW12WJ2qFeDLvTJV9"
        + "kmt44MzYej8MlTvIaxbwUJoMLxaoAITnUOV94C7Cl5DI1NMeeT8dAYtMPYvqj5yztfYl1NpjlKX6VF-zN66domw4FIJ5xvOBFcOVk8MgNu0RmyS1hE_Rm1oEKQs3xeyP4Q-_K6eSiuFtfHErORhbHb"
        + "l2YjlpL8Dg4uldxNTEXZJdRy0qokZ9eruEFEU8BJlBboMDmtb-Sykmlb78duya2f2k310jO3UEX_MwyDJE2n5Bs4S4wfQfe4nRn2B3VUVtZGVJh5xZpSF2sXMfUZZ52lC8"
        + "5YS_BD4x7ckQyPQB3QuRYWGOq5Kpk0jA"}}
    }

    function userTokenLegacy() {
        return {data:{"value":"eyJhbGciOiJIUzI1NiJ9.eyJjZFVzdWFyaW8iOiJjYW1pbGEuMDU2NiIsImNkQ29vcGVyYXRpdmEiOjU2NiwiY2RDb29wZXJhdGl2Y"
                + "UNlbnRyYWwiOjU2Niwic3ViIjoiUHJlLWFwcm92YWRvIn0.ybqQt9_0fBqnNIQG3tgEByV64ieDnDXkpNWAAgWziwQ"}}
    }

    function permissoesFake() {
        return [{"nmPerfil":"Cadastro Inserir"},{"nmPerfil":"Cadastro Atualizar"}]
    }

});
