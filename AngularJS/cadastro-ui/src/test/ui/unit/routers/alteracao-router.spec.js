describe('Router: alteracaoRouter', function () {

    var $route,
        secao,
        $q,
        rotaService,
        $rootScope;

    beforeEach(module('app'));

    beforeEach(inject(function (_$route_, _secao_, _$rootScope_, _$q_, _rotaService_) {
        $route = _$route_;
        $rootScope = _$rootScope_;
        secao = _secao_;
        $q = _$q_;
        rotaService = _rotaService_;

        rota = $route.routes['/alteracao/:cpf'];
    }));

    it('Deve testar alteracaoRouter', function() {
        var rota = $route.routes['/alteracao/:cpf'];

        expect(rota.controller).toBe('alteracaoController');
        expect(rota.templateUrl).toBe('views/pages/alteracao.html');
    });

    it('Deve chamar resolve', function () {
        rota.resolve.section(secao);
        $rootScope.$apply();

        expect(secao.nome).toBeDefined();
    });

    it('deve verificar se é terceiro', function() {
        var cpf = '93994468248';

        var deferred = $q.defer();
        deferred.resolve({});
        spyOn(rotaService, 'isNotTerceiro').and.returnValue(deferred.promise);

        rota = $route.routes['/alteracao/:cpf'];
        $route = {
            current: {
                params: {
                    cpf: cpf
                }
            }
        }

        rota.resolve.isNotTerceiro($route, rotaService);
        expect(rotaService.isNotTerceiro).toHaveBeenCalledWith(cpf);
    });
});
