describe('Router: homeController', function () {

    var $route,
        secao,
        $rootScope;

    beforeEach(module('app'));

    beforeEach(inject(function (_$route_, _secao_, _$rootScope_) {
        $route = _$route_;
        $rootScope = _$rootScope_;
        secao = _secao_;

        rota = $route.routes['/home'];
    }));

    it('Deve testar alteracaoRouter', function() {
        expect(rota.controller).toBe('homeController');
        expect(rota.templateUrl).toBe('views/pages/home.html');
    });

    it('Deve chamar resolve', function () {
        rota.resolve.section(secao);
        $rootScope.$apply();

        expect(secao.nome).toBeDefined();
    });
});