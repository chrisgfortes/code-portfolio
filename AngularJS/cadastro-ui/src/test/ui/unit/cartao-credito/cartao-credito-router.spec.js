describe('Router: Limite Cartão Credito', function() {

    var $route,
        rota,
        $rootScope;

    beforeEach(module('app'));

    beforeEach(inject(function(_$route_, _$rootScope_) {
        $route = _$route_;
        $rootScope = _$rootScope_;

        rota = $route.routes['/cadastro/associado/:cpf/limite-cartao-credito'];
    }));

    it('deve validar as configurações', function() {
        expect(rota.templateUrl).toBe('./modules/cartao-credito/cartao-credito.html');
        expect(rota.controller).toBe('cartaoCreditoController');
        expect(rota.controllerAs).toBe('ctrl');
        expect(rota.params.isContaCorrente).toBe(true);
    });
});