describe('Factory: cartaoCredito', function() {

    var cartaoCredito,
        HOST;

    beforeEach(module('app'));

    beforeEach(inject(function(_cartaoCredito_, _HOST_) {
        cartaoCredito = _cartaoCredito_;
        HOST = _HOST_;
    }));

     it('.linhasDeCredito()', inject(function($httpBackend) {
        var dadosFake = { data: {}};

        $httpBackend
            .expectGET(HOST.linhaCredito + '?modalidade=CARTAO_ROTATIVO&ativo=true&tipo-credito=EMPRESTIMO')
            .respond(200, dadosFake);

        cartaoCredito
            .linhasDeCredito()
            .then(function(linhasDeCredito) {
                expect(linhasDeCredito).toEqual(dadosFake);
            });

        $httpBackend.flush();
    }));


    it('.tiposVencimento()', inject(function($httpBackend) {
        var dadosFake = { data: {}};

        $httpBackend
            .expectGET(HOST.contaCorrente + 'tipos-vencimento-proposta')
            .respond(200, dadosFake);

        cartaoCredito
            .tiposVencimento()
            .then(function(tiposVencimento) {
                expect(tiposVencimento).toEqual(dadosFake);
            });

        $httpBackend.flush();
    }));

    it('.salvar(cartaoCredito)', inject(function($httpBackend) {
        var cartaoCreditoResponse = {};
        var dadosFake = { data: {}};

        $httpBackend
            .expectPOST(HOST.contaCorrente + 'limite-cartao-credito')
            .respond(201, dadosFake);

        cartaoCredito
            .salvar(cartaoCreditoResponse)
            .then(function(response) {
                var dados = response.data;

                expect(dados).toEqual(dadosFake);
            });

        $httpBackend.flush();
    }));
});