describe('controller: cartaoCreditoController', function () {
    
    var ctrl, $rootScope, $q;
    
    var cpf = '12345678912';
    var erros = [{}];
    var cartaoCredito = {}
    
    beforeEach(angular.mock.module('app'));
    
    beforeEach(inject(function (_$controller_, _$rootScope_, _cartaoCredito_, _$q_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        cartaoCredito = _cartaoCredito_;
    
        ctrl = $controller('cartaoCreditoController', {});
    }));
    
    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });
    
    it('deve chamar .salvar(cartaoCredito)', function () {
        var deferred = $q.defer();
        deferred.resolve({});
    
        spyOn(cartaoCredito, 'salvar').and.returnValue(deferred.promise);
    
        var cartaoCreditoInfo = {}
    
        ctrl.salvar(cartaoCreditoInfo);
    
        $rootScope.$apply();
    
        expect(cartaoCredito.salvar).toHaveBeenCalledWith(cartaoCreditoInfo);
        expect(ctrl.sucessos[0]).toEqual({ message: 'Os dados Limite de Credito foram salvos com sucesso.' });
    });
    
    it('deve mostrar messagem de validação, vinda no details', function () {
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            details: erros
        });
    
        spyOn(cartaoCredito, 'salvar').and.returnValue(deferred.promise);
    
        var cartaoCreditoInfo = {}
    
        ctrl.salvar(cartaoCreditoInfo);
    
        $rootScope.$apply();
    
        expect(cartaoCredito.salvar).toHaveBeenCalledWith(cartaoCreditoInfo);
        expect(ctrl.erros).toEqual(erros);
    });
    
    it('deve mostrar messagem de validação, vinda no content', function () {
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {}
        });
    
        spyOn(cartaoCredito, 'salvar')
            .and
            .returnValue(deferred.promise);
    
        var cartaoCreditoInfo = {}
    
        ctrl.salvar(cartaoCreditoInfo);
    
        $rootScope.$apply();
    
        expect(cartaoCredito.salvar).toHaveBeenCalledWith(cartaoCreditoInfo);
        expect(ctrl.erros).toEqual([{}]);
    });
    
    it('não deve mostrar messagem de validação', function () {
        var deferred = $q.defer();
        deferred.reject({
            status: 400,
            data: {
                body: erros
            }
        });
    
        spyOn(cartaoCredito, 'salvar')
            .and
            .returnValue(deferred.promise);
    
        var cartaoCreditoInfo = {}
    
        ctrl.salvar(cartaoCreditoInfo);
    
        $rootScope.$apply();
    
        expect(cartaoCredito.salvar).toHaveBeenCalledWith(cartaoCreditoInfo);
        expect(ctrl.erros).toEqual([{ message: 'Ocorreu um erro no servidor, tente novamente.' }])
    });
});