describe('Component: dadosLimiteCartaoCredito', function () {
    
    var $rootScope,
        ctrl,
        $q;
    
    beforeEach(angular.mock.module('app'));
    
    beforeEach(inject(function (_$componentController_, _$rootScope_, _$q_) {
        $componentController = _$componentController_;
        $rootScope = _$rootScope_;
        $q = _$q_;
    
        ctrl = $componentController('dadosLimiteCartaoCredito', {
        });
    }));
    
    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });
    
    it('deve estar definido', function () {
        ctrl.linhasDeCredito = undefined;
    
        ctrl.$onInit();
    
        expect(ctrl.linhasDeCredito).toBeDefined();
    });

    it('deve inicializar com os dados', inject(function (cartaoCredito) {
        ctrl.linhasDeCredito = undefined;
        var linhasDeCredito = [{}]
        var defered = $q.defer()
        defered.resolve(linhasDeCredito)

        spyOn(cartaoCredito, 'linhasDeCredito').and.returnValue(defered.promise);
        spyOn(cartaoCredito, 'tiposVencimento').and.returnValue(defered.promise);
    
        ctrl.$onInit();

        $rootScope.$apply();
    
        expect(cartaoCredito.linhasDeCredito).toHaveBeenCalled();
        expect(ctrl.linhasDeCredito).toEqual(linhasDeCredito);
        expect(cartaoCredito.tiposVencimento).toHaveBeenCalled();
        expect(ctrl.tiposVencimento).toEqual(linhasDeCredito);
    }));
});