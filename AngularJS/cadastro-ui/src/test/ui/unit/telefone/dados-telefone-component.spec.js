describe('Component: dadosTelefone', function() {

    var $componentController,
        contato,
        ctrl;
    var referenciasTelefone = [{
        ddd: ''
    }];
    
    var mockTelefoneResidencial = {
		tipoTelefone:'RESIDENCIAL'
	};
    var mockTelefoneCelular = {
		tipoTelefone:'CELULAR'
	};

    beforeEach(module('app'));

    beforeEach(inject(function(_$componentController_, _contato_) {
        $componentController = _$componentController_;
        contato = _contato_;

        var bindings = {
            telefone: {}
        };

        spyOn(contato, 'referenciaContatos').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: referenciasTelefone }); }
            };
        });

        ctrl = $componentController('dadosTelefone', null, bindings);

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o telefone no bindings', function() {
        expect(ctrl.telefone).toBeDefined();
    });

    it('deve chamar contato.referenciaContatos()', function () {
        expect(contato.referenciaContatos).toHaveBeenCalled();
        expect(ctrl.referenciasTelefone).toEqual(referenciasTelefone);
    });
});
    