describe('Component: sessaoVeiculos', function() {

    var $componentController,
        dialogs,
        $q,
        $rootScope,
        ctrl,
        veiculo,
        veiculosUpdate;

    var tiposVeiculos = [{
        valor: 'AUTOMOTIVO',
        descricao: 'Automotivo'
    },{
        valor: 'VEICULO',
        descricao: 'Veículo'
    },{
        valor: 'OUTROS',
        descricao: 'Outros'
    }];

    beforeEach(module('app'));

    beforeEach(inject(function(_$componentController_, _dialogs_, _$q_, _$rootScope_, _veiculo_, _veiculosUpdate_) {
        $componentController = _$componentController_;
        dialogs = _dialogs_;
        $q = _$q_;
        $rootScope = _$rootScope_;
        veiculoFactory = _veiculo_;
        veiculosUpdate = _veiculosUpdate_;

        var bindings = {
            veiculos: []
        };

        ctrl = $componentController('sessaoVeiculos', {
            dialogs: _dialogs_,
            veiculosUpdate: veiculosUpdate,
            $routeParams: {cpf: '32145698701'}
        }, bindings);

        spyOn(veiculoFactory, 'tipos').and.callFake(respostaFake({ data: tiposVeiculos }));

        ctrl.$onInit();
    }));

    it('deve estar definido', function() {
        expect($componentController).toBeDefined();
    });

    it('deve adicionar descricaoTipoDeVeiculo para cada veículo', function() {
        ctrl.veiculos = [{
            tipoVeiculo: "VEICULO",
            situacaoVeiculo: "LIVRE",
            modelo: "Gol",
            marca: "Volks",
            anoFabricacao: 2016,
            valorVeiculo: 20000,
            anoModelo: 2016
        }];
        ctrl.$onInit();
        expect(ctrl.veiculos[0].descricaoTipoDeVeiculo).toBeDefined();
    });

    it('deve adicionar descricaoTipoDeVeiculo para cada veículo ao atualizar', function() {
        var veiculos = [{
            tipoVeiculo: "VEICULO",
            situacaoVeiculo: "LIVRE",
            modelo: "Gol",
            marca: "Volks",
            anoFabricacao: 2016,
            valorVeiculo: 20000,
            anoModelo: 2016
        }];

        $rootScope.$broadcast('atualizar-descricao-tipo-veiculo', veiculos);

        expect(ctrl.veiculos[0].descricaoTipoDeVeiculo).toBeDefined();
        expect(ctrl.veiculos[0].descricaoTipoDeVeiculo).toEqual('Veículo');
    });

    it('deve adicionar descricaoTipoDeVeiculo padrão (Automotivo) caso tipoVeiculo seja nulo', function() {
        ctrl.veiculos = [{
            tipoVeiculo: null,
            situacaoVeiculo: "LIVRE",
            modelo: "Gol",
            marca: "Volks",
            anoFabricacao: 2016,
            valorVeiculo: 20000,
            anoModelo: 2016
        }];
        ctrl.$onInit();
        expect(ctrl.veiculos[0].descricaoTipoDeVeiculo).toBeDefined();
        expect(ctrl.veiculos[0].descricaoTipoDeVeiculo).toEqual('Automotivo');
        expect(ctrl.veiculos[0].tipoVeiculo).toEqual('AUTOMOTIVO');
    });

    it('deve definir o veiculo no bindings', function() {
        expect(ctrl.veiculos).toBeDefined();
    });

    it('deve SALVAR', function() {
        var veiculo = {
            "tipoVeiculo": "AUTOMOTIVO",
            "situacaoVeiculo": "LIVRE",
            "modelo": "Gol",
            "marca": "Volks",
            "anoFabricacao": 2016,
            "valorVeiculo": 20000,
            "anoModelo": 2016,
            "modoEdicao": false
        };

        ctrl.mostrarVeiculoForm = true;

        ctrl.salvar(veiculo);

        expect(ctrl.veiculos.length).toBe(1);
        expect(ctrl.mostrarVeiculoForm).toBe(false);
    });

    it('deve EDITAR', function() {
        var veiculo = {
            "tipoVeiculo": "AUTOMOTIVO",
            "situacaoVeiculo": "LIVRE",
            "modelo": "Gol",
            "marca": "Volks",
            "anoFabricacao": 2016,
            "valorVeiculo": 20000,
            "anoModelo": 2016,
            "modoEdicao": true
        };

        var veiculoEditado = {
            "tipoVeiculo": "AUTOMOTIVO",
            "situacaoVeiculo": "LIVRE",
            "modelo": "Gol",
            "marca": "Volks",
            "anoFabricacao": 2015,
            "valorVeiculo": 25000,
            "anoModelo": 2016,
            "modoEdicao": true
        };

        var coadijuvante = {
            "tipoVeiculo": "AUTOMOTIVO",
            "situacaoVeiculo": "LIVRE",
            "modelo": "Gol",
            "marca": "Volks",
            "anoFabricacao": 2010,
            "valorVeiculo": 10000,
            "anoModelo": 2010,
            "modoEdicao": false
        };

        ctrl.veiculos.push(veiculo);
        ctrl.veiculos.push(coadijuvante);

        ctrl.mostrarVeiculoForm = true;

        ctrl.salvar(veiculoEditado);

        expect(ctrl.veiculos[0].valorVeiculo).toBe(25000);
        expect(ctrl.veiculos[0].modoEdicao).toBe(false);
        expect(ctrl.veiculos[1]).toEqual(coadijuvante);
        expect(ctrl.veiculos.length).toBe(2);
        expect(ctrl.mostrarVeiculoForm).toBe(false);
    });

    it('deve CANCELAR', function() {
        var veiculo = {
            "tipoVeiculo": "AUTOMOTIVO",
            "situacaoVeiculo": "LIVRE",
            "modelo": "Gol",
            "marca": "Volks",
            "anoFabricacao": 2016,
            "valorVeiculo": 20000,
            "anoModelo": 2016,
            "modoEdicao": true
        };

        ctrl.mostrarVeiculoForm = true;

        ctrl.veiculos.push(veiculo);

        ctrl.cancelar();

        expect(ctrl.veiculos.length).toBe(1);
        expect(ctrl.veiculos[0].modoEdicao).toBe(false);
        expect(ctrl.mostrarVeiculoForm).toBe(false);
    });

    it('deve NOVO formulário', function() {
        ctrl.mostrarVeiculoForm = false;

        ctrl.novo();

        expect(ctrl.mostrarVeiculoForm).toBe(true);
    });

    it('deve ficar em MODO EDIÇÃO', function() {
        ctrl.mostrarVeiculoForm = false;
        ctrl.veiculo = null;

        var veiculo = {
            "tipoVeiculo": "AUTOMOTIVO",
            "situacaoVeiculo": "LIVRE",
            "modelo": "Gol",
            "marca": "Volks",
            "anoFabricacao": 2016,
            "valorVeiculo": 20000,
            "anoModelo": 2016,
            "modoEdicao": true
        };

        ctrl.editar(veiculo);

        expect(ctrl.mostrarVeiculoForm).toBe(true);
        expect(ctrl.veiculo).toEqual(veiculo);
    });

    it('deve VISUALIZAR', function() {
        ctrl.mostrarVeiculoForm = false;
        ctrl.apenasLeitura = false;
        ctrl.veiculo = null;

        var veiculo = {
            "tipoVeiculo": "AUTOMOTIVO",
            "situacaoVeiculo": "LIVRE",
            "modelo": "Gol",
            "marca": "Volks",
            "anoFabricacao": 2016,
            "valorVeiculo": 20000,
            "anoModelo": 2016,
            "modoEdicao": true
        };

        ctrl.visualizar(veiculo);

        expect(ctrl.mostrarVeiculoForm).toBe(true);
        expect(ctrl.apenasLeitura).toBe(true);
    });

    it('deve REMOVER', function() {
        var deferred = $q.defer();
        deferred.resolve('yes');

        spyOn(dialogs, 'confirm').and.returnValue({
            result: deferred.promise
        });

        var veiculo = {
            "tipoVeiculo": "AUTOMOTIVO",
            "situacaoVeiculo": "LIVRE",
            "modelo": "Gol",
            "marca": "Volks",
            "anoFabricacao": 2016,
            "valorVeiculo": 20000,
            "anoModelo": 2016,
            "modoEdicao": false,
            "modoExclusao": false
        };

        var coadijuvante = {
            "tipoVeiculo": "AUTOMOTIVO",
            "situacaoVeiculo": "LIVRE",
            "modelo": "Gol",
            "marca": "Volks",
            "anoFabricacao": 2016,
            "valorVeiculo": 20000,
            "anoModelo": 2016,
            "modoEdicao": false,
            "modoExclusao": true
        };

        ctrl.veiculos.push(veiculo);
        ctrl.veiculos.push(coadijuvante);

        ctrl.remover(coadijuvante);

        $rootScope.$apply();

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.veiculos.length).not.toBe(2);
        expect(ctrl.veiculos.length).toBe(1);
        expect(ctrl.veiculos[0]).toEqual(veiculo);
    });

    it('deve REMOVER - Update', function() {
        var deferred = $q.defer();
        deferred.resolve('yes');
        var deferredExcluir = $q.defer();
        deferredExcluir.resolve();

        ctrl.isUpdate = true;

        spyOn(dialogs, 'confirm').and.returnValue({
            result: deferred.promise
        });

        spyOn(veiculosUpdate, 'excluir').and.returnValue(deferred.promise);

        var veiculo = {
            "id": 1,
            "tipoVeiculo": "AUTOMOTIVO",
            "situacaoVeiculo": "LIVRE",
            "modelo": "Gol",
            "marca": "Volks",
            "anoFabricacao": 2016,
            "valorVeiculo": 20000,
            "anoModelo": 2016,
            "modoEdicao": false,
            "modoExclusao": false
        };

        var coadijuvante = {
            "id": 2,
            "tipoVeiculo": "AUTOMOTIVO",
            "situacaoVeiculo": "LIVRE",
            "modelo": "Gol",
            "marca": "Volks",
            "anoFabricacao": 2016,
            "valorVeiculo": 20000,
            "anoModelo": 2016,
            "modoEdicao": false,
            "modoExclusao": true
        };

        ctrl.veiculos.push(veiculo);
        ctrl.veiculos.push(coadijuvante);

        ctrl.remover(coadijuvante);

        $rootScope.$apply();

        expect(veiculosUpdate.excluir).toHaveBeenCalledWith('32145698701', 2);
        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.veiculos.length).not.toBe(2);
        expect(ctrl.veiculos.length).toBe(1);
        expect(ctrl.veiculos[0]).toEqual(veiculo);
    });

    it('deve cancelar a REMOVOÇÃO de um veiculo', function() {
        var deferred = $q.defer();
        deferred.reject();
        spyOn(dialogs, 'confirm').and.returnValue({
            result: deferred.promise
        });

        var veiculo = {
            "tipoVeiculo": "AUTOMOTIVO",
            "situacaoVeiculo": "LIVRE",
            "modelo": "Gol",
            "marca": "Volks",
            "anoFabricacao": 2016,
            "valorVeiculo": 20000,
            "anoModelo": 2016,
            "modoEdicao": false,
            "modoExclusao": false
        };

        var coadijuvante = {
            "tipoVeiculo": "AUTOMOTIVO",
            "situacaoVeiculo": "LIVRE",
            "modelo": "Gol",
            "marca": "Volks",
            "anoFabricacao": 2016,
            "valorVeiculo": 20000,
            "anoModelo": 2016,
            "modoExclusao": true
        };

        ctrl.veiculos.push(veiculo);
        ctrl.veiculos.push(coadijuvante);

        ctrl.remover(coadijuvante);

        $rootScope.$apply();

        var todosDesabilitados = ctrl.veiculos
            .some(function(veiculo) {
                return !veiculo.modoExclusao;
            });

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.veiculos.length).toBe(2);
        expect(todosDesabilitados).toBe(true);
    });
});
