describe('Componente LISTA VEICULOS', function () {

    var $componentController,
        $rootScope,
        ctrl,
        dialogs,
        $q;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _$rootScope_,_dialogs_, _$q_) {
        $componentController = _$componentController_;
        $rootScope = _$rootScope_;
        dialogs = _dialogs_;
        $q = _$q_;

        var bindings = {
            readonly: true,
            veiculos: []
        };

        ctrl = $componentController('listaVeiculos',
            null,
            bindings
        );

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o veiculos no bindings', function () {
        expect(ctrl.veiculos).toBeDefined();
    });

    it('deve definir o valor padrão para veiculos', function () {
        ctrl.veiculos = undefined;

        ctrl.$onInit();

        expect(ctrl.veiculos).toBeDefined();
    });

    it('deve chamar o evento onNovo', function () {
        var onNovoSpy = jasmine.createSpy('onNovo');

        var bindings = {
            veiculos: {},
            onNovo: onNovoSpy
        };

        var ctrl = $componentController('listaVeiculos', null, bindings);

        ctrl.novo();

        expect(onNovoSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onEditar', function () {
        var onEditarSpy = jasmine.createSpy('onEditar');

        var bindings = {
            onEditar: onEditarSpy
        };

        var ctrl = $componentController('listaVeiculos', null, bindings);

        ctrl.veiculos = [{ modoEdicao : false }];
        var veiculo = ctrl.veiculos[0];

        ctrl.editar(veiculo);

        expect(onEditarSpy).toHaveBeenCalled();
        expect(veiculo.modoEdicao).toBe(true);
    });

    it('deve chamar o evento onVisualizar', function () {
        var onVisualizarSpy = jasmine.createSpy('onVisualizar');

        var bindings = {
            onVisualizar: onVisualizarSpy
        };

        var ctrl = $componentController('listaVeiculos', null, bindings);

        ctrl.veiculos = [{ modoVisualizacao : false }];
        var veiculo = ctrl.veiculos[0];

        ctrl.visualizar(veiculo);

        expect(onVisualizarSpy).toHaveBeenCalled();
        expect(veiculo.modoVisualizacao).toBe(true);
    });

    it('deve chamar o evento onRemover', function () {
        var onRemoverSpy = jasmine.createSpy('onRemover');

        var bindings = {
            onRemover: onRemoverSpy
        };

        var ctrl = $componentController('listaVeiculos', null, bindings);

        ctrl.veiculos = [{ modoExclusao : false }];
        var veiculo = ctrl.veiculos[0];

        ctrl.remover(veiculo);

        expect(onRemoverSpy).toHaveBeenCalled();
    });

});
