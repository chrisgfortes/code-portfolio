describe('factory: veiculo', function() {
    
    var veiculo,
        httpBackend;
    
    beforeEach(module('app'));
    
    beforeEach(inject(function($httpBackend, _veiculo_) {
        veiculo = _veiculo_;
    
        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));
    
    afterEach(function() {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });
    
    it('.tipos()', function() {
        var modelsResponse = [{}];
    
        httpBackend
            .expectGET('../../../pessoa-us/cadastro/pessoa/v1/fisica/veiculo/tipos')
            .respond(200, modelsResponse);
    
        veiculo
            .tipos()
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });
    
        httpBackend.flush();
    });

    it('.situacoes()', function() {
        var modelsResponse = [{}];
    
        httpBackend
            .expectGET('../../../pessoa-us/cadastro/pessoa/v1/fisica/veiculo/situacoes')
            .respond(200, modelsResponse);
    
        veiculo
            .situacoes()
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });
    
        httpBackend.flush();
    });
});