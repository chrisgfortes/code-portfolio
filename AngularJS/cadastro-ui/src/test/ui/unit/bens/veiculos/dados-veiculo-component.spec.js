describe('Component: dadosVeiculo', function() {

    var $componentController,
        ctrl;

    var tipoVeiculo = [{
        valor: 'VEICULO',
        descricao: 'Veículo'
    }];

    var situacoes = [{
        valor: 'TIPO_SITUACAO',
        descricao: 'Tipo descrição situacao'
    }];

    beforeEach(module('app'));

    beforeEach(inject(function(_$componentController_, _veiculo_) {
        $componentController = _$componentController_;
        veiculo = _veiculo_;

        var bindings = {
            veiculo: {}
        };

        spyOn(veiculo, 'tipos').and.callFake(function() {
            return {
                then: function(callback) {
                    return callback({
                        data: tipoVeiculo
                    });
                }
            };
        });

        spyOn(veiculo, 'situacoes').and.callFake(function() {
            return {
                then: function(callback) {
                    return callback({
                        data: situacoes
                    });
                }
            };
        });

        ctrl = $componentController('dadosVeiculo', null, bindings);

        ctrl.$onInit();
    }));

    it('deve estar definido', function() {
        expect($componentController).toBeDefined();
    });

    it('deve definir o veiculo no bindings', function() {
        expect(ctrl.veiculo).toBeDefined();
    });

    it('deve chamar veiculo.tipos()', function() {
        expect(veiculo.tipos).toHaveBeenCalled();
        expect(ctrl.tipos).toEqual(tipoVeiculo);
    });

    it('deve chamar veiculo.situacoes()', function() {
        expect(veiculo.situacoes).toHaveBeenCalled();
    });

    it('deve chamar o evento onSalvar', function() {
        var onSalvarSpy = jasmine.createSpy('onSalvar');

        var bindings = {
            veiculo: {},
            onSalvar: onSalvarSpy
        };

        var ctrl = $componentController('dadosVeiculo', null, bindings);
        
        ctrl.tipos = [{
            valor: 'VEICULO',
            descricao: 'Veículo'
        }];
        ctrl.veiculo = {
            tipoVeiculo: "VEICULO",
            situacaoVeiculo: "LIVRE",
            modelo: "Gol",
            marca: "Volks",
            anoFabricacao: 2016,
            valorVeiculo: 20000,
            anoModelo: 2016
        };

        ctrl.salvar();

        expect(onSalvarSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onCancelar', function() {
        var onCancelarSpy = jasmine.createSpy('onCancelar');

        var bindings = {
            onCancelar: onCancelarSpy
        };

        var ctrl = $componentController('dadosVeiculo', null, bindings);

        ctrl.cancelar();

        expect(onCancelarSpy).toHaveBeenCalled();
    });
});
