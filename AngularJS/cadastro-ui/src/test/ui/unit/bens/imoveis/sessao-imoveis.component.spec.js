describe('Component: sessaoImoveis', function () {

    var $componentController,
        dialogs,
        $q,
        $rootScope,
        ctrl,
        imovel,
        imoveisUpdate;

        var tiposImovel = [{
            valor: "CASA", descricao: "Casa"
        },{
            valor: "APARTAMENTO", descricao: "Apartamento"
        },{
            valor: 'OUTROS', descricao: 'Outros'
        }];

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _dialogs_, _$q_, _$rootScope_, _imovel_, _imoveisUpdate_) {
        $componentController = _$componentController_;
        dialogs = _dialogs_;
        $q = _$q_;
        $rootScope = _$rootScope_;
        imovel = _imovel_;
        imoveisUpdate = _imoveisUpdate_;

        var bindings = {
            imoveis: []
        };

        ctrl = $componentController('sessaoImoveis',
            {
                dialogs: _dialogs_,
                imoveisUpdate: imoveisUpdate,
                $routeParams: {cpf: '32145698701'}
            }, bindings);



        spyOn(imovel, 'tipos').and.callFake(respostaFake({ data: tiposImovel }));

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve adicionar descricaoTipoDeImovel para cada imóvel', function() {
        ctrl.imoveis = [{
            tipo:"APARTAMENTO",
            situacao:"HIPOTECADO",
            destinacao:"RESIDENCIAL",
            valorAtual:9878.95,
            vendedor:{
                cpfCnpj:"95741154000140",
                nome:"Maria"}
            }];
        ctrl.$onInit();
        expect(ctrl.imoveis[0].descricaoTipoDeImovel).toBeDefined();
    });

    it('deve adicionar descricaoTipoDeImovel para cada imóvel ao atualizar', function() {
        var imoveis = [{
            tipo:"APARTAMENTO",
            situacao:"HIPOTECADO",
            destinacao:"RESIDENCIAL",
            valorAtual:9878.95,
            vendedor:{
                cpfCnpj:"95741154000140",
                nome:"Maria"}
            }];

        $rootScope.$broadcast('atualizar-descricao-tipo-imovel', imoveis);

        expect(ctrl.imoveis[0].descricaoTipoDeImovel).toBeDefined();
        expect(ctrl.imoveis[0].descricaoTipoDeImovel).toEqual('Apartamento');
    });

    it('deve definir o imoveis no bindings', function () {
        expect(ctrl.imoveis).toBeDefined();
    });

    it('deve SALVAR um imóvel', function () {
        var imovel = {
            tipo: "CASA",
            situacao: "LIVRE",
            destinacao: "RESIDENCIAL",
            valorAtual: 10000,
            modoEdicao: false
        };

        ctrl.mostrarImovelForm = true;

        ctrl.salvar(imovel);

        expect(ctrl.imoveis.length).toBe(1);
        expect(ctrl.mostrarImovelForm).toBe(false);
    });

    it('deve EDITAR um imóvel', function () {
        var imovel = {
            tipo: "CASA",
            situacao: "LIVRE",
            destinacao: "RESIDENCIAL",
            valorAtual: 10000,
            modoEdicao: true
        };

        var imovelEditado = {
            tipo: "CASA",
            situacao: "LIVRE",
            destinacao: "RESIDENCIAL",
            valorAtual: 20000,
            modoEdicao: true
        };

        var imovelSecundario = {
            tipo: "CASA",
            situacao: "LIVRE",
            destinacao: "RESIDENCIAL",
            valorAtual: 10000,
            modoEdicao: false
        };

        ctrl.imoveis.push(imovel);
        ctrl.imoveis.push(imovelSecundario);

        ctrl.mostrarImovelForm = true;

        ctrl.salvar(imovelEditado);

        expect(ctrl.imoveis[0].valorAtual).toBe(20000);
        expect(ctrl.imoveis[0].modoEdicao).toBe(false);
        expect(ctrl.imoveis[1]).toEqual(imovelSecundario);
        expect(ctrl.imoveis.length).toBe(2);
        expect(ctrl.mostrarImovelForm).toBe(false);
    });

    it('deve CANCELAR nova/edição imóvel', function () {
        var imovel = {
            tipo: "CASA",
            situacao: "LIVRE",
            destinacao: "RESIDENCIAL",
            valorAtual: 10000,
            modoEdicao: true
        };
        ctrl.mostrarImovelForm = true;

        ctrl.imoveis.push(imovel);

        ctrl.cancelar();

        expect(ctrl.imoveis.length).toBe(1);
        expect(ctrl.imoveis[0].modoEdicao).toBe(false);
        expect(ctrl.mostrarImovelForm).toBe(false);
    });

    it('deve NOVO um imóvel', function () {
        ctrl.mostrarImovelForm = false;
        ctrl.novo();
        expect(ctrl.mostrarImovelForm).toBe(true);
    });

    it('deve ficar em MODO EDIÇÃO o imóvel', function () {
        ctrl.mostrarImovelForm = false;
        ctrl.imovel = null;

        var imovel = {
            tipo: "CASA",
            situacao: "LIVRE",
            destinacao: "RESIDENCIAL",
            valorAtual: 10000,
            modoEdicao: true
        };

        ctrl.editar(imovel);

        expect(ctrl.mostrarImovelForm).toBe(true);
        expect(ctrl.imovel).toEqual(imovel);
    });

    it('deve VISUALIZAR', function() {
        ctrl.mostrarImovelForm = false;
        ctrl.apenasLeitura = false;
        ctrl.imovel = null;

        var imovel = {
            tipo: "CASA",
            situacao: "LIVRE",
            destinacao: "RESIDENCIAL",
            valorAtual: 10000,
            modoEdicao: true
        };

        ctrl.visualizar(imovel);

        expect(ctrl.mostrarImovelForm).toBe(true);
        expect(ctrl.apenasLeitura).toBe(true);
    });

    it('deve REMOVER um imóvel', function () {
        var deferred = $q.defer();
        deferred.resolve('yes');
        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var imovel = {
            tipo: "CASA",
            situacao: "LIVRE",
            destinacao: "RESIDENCIAL",
            valorAtual: 10000,
            modoExclusao: true
        };

        var imovelSecundario = {
            tipo: "CASA",
            situacao: "LIVRE",
            destinacao: "RESIDENCIAL",
            valorAtual: 10000,
            modoExclusao: false
        };

        ctrl.imoveis.push(imovel);
        ctrl.imoveis.push(imovelSecundario);

        ctrl.remover(imovel);

        $rootScope.$apply();

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.imoveis.length).toBe(1);
    });

    it('deve REMOVER - Update', function() {
        var deferred = $q.defer();
        deferred.resolve('yes');
        var deferredExcluir = $q.defer();
        deferredExcluir.resolve();

        ctrl.isUpdate = true;

        spyOn(dialogs, 'confirm').and.returnValue({
            result: deferred.promise
        });

        spyOn(imoveisUpdate, 'excluir').and.returnValue(deferredExcluir.promise);

        var imovel = {
            id: 1,
            tipo: "CASA",
            situacao: "LIVRE",
            destinacao: "RESIDENCIAL",
            valorAtual: 10000,
            modoExclusao: false
        };

        var imovelSecundario = {
            id: 2,
            tipo: "CASA",
            situacao: "LIVRE",
            destinacao: "RESIDENCIAL",
            valorAtual: 10000,
            modoExclusao: true
        };

        ctrl.imoveis.push(imovel);
        ctrl.imoveis.push(imovelSecundario);

        ctrl.remover(imovelSecundario);

        $rootScope.$apply();

        expect(imoveisUpdate.excluir).toHaveBeenCalledWith('32145698701', 2);
        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.imoveis.length).not.toBe(2);
        expect(ctrl.imoveis.length).toBe(1);
        expect(ctrl.imoveis[0]).toEqual(imovel);
    });

    it('deve cancelar a REMOVOÇÃO do imóvel', function () {
        var deferred = $q.defer();
        deferred.reject();
        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var imovel = {
            tipo: "CASA",
            situacao: "LIVRE",
            destinacao: "RESIDENCIAL",
            valorAtual: 10000,
            modoExclusao: true
        };

        var imovelSecundario = {
            tipo: "CASA",
            situacao: "LIVRE",
            destinacao: "RESIDENCIAL",
            valorAtual: 10000,
            modoExclusao: false
        };

        ctrl.imoveis.push(imovel);
        ctrl.imoveis.push(imovelSecundario);

        ctrl.remover(imovelSecundario);

        $rootScope.$apply();

        var todosDesabilitados = ctrl.imoveis
            .some(function (imovel) {
                return !imovel.modoExclusao;
            });

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.imoveis.length).toBe(2);
        expect(todosDesabilitados).toBe(true);
    });
});
