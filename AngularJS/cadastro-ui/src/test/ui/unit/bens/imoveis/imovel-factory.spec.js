describe('factory: imovel', function() {
    
    var imovel,
        httpBackend;
    
    beforeEach(module('app'));
    
    beforeEach(inject(function($httpBackend, _imovel_) {
        imovel = _imovel_;
    
        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));
    
    afterEach(function() {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });
    
    it('.tipos()', function() {
        var modelsResponse = [{}];
    
        httpBackend
            .expectGET('../../../pessoa-us/cadastro/pessoa/v1/fisica/imovel/tipos')
            .respond(200, modelsResponse);
    
        imovel
            .tipos()
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });
    
        httpBackend.flush();
    });

    it('.tiposDestinacoes()', function() {
        var modelsResponse = [{}];
    
        httpBackend
            .expectGET('../../../pessoa-us/cadastro/pessoa/v1/fisica/imovel/destinacoes')
            .respond(200, modelsResponse);
    
        imovel
            .tiposDestinacoes()
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });
    
        httpBackend.flush();
    });

    it('.tiposSituacao()', function() {
        var modelsResponse = [{}];
    
        httpBackend
            .expectGET('../../../pessoa-us/cadastro/pessoa/v1/fisica/imovel/situacoes')
            .respond(200, modelsResponse);
    
        imovel
            .tiposSituacao()
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });
    
        httpBackend.flush();
    });
});