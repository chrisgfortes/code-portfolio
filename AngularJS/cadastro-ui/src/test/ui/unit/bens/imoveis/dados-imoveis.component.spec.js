describe('Component: dadosImoveis', function () {

    var $componentController,
        moment,
        $rootScope,
        ctrl,
        imovel,
        $q,
        dialogs,
        $routeParams;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _moment_, _imovel_, _$q_, _$rootScope_, _dialogs_) {
        $componentController = _$componentController_;
        moment = _moment_;
        imovel = _imovel_;
        $q = _$q_;
        $rootScope = _$rootScope_;
        dialogs = _dialogs_;

        var bindings = {
            imovel: {}
        };

        ctrl = $componentController('dadosImoveis',
            {
                $routeParams: {cpf: '02339708001'}
            },
            bindings
        );

        var tiposImovel = [{valor: "CASA", descricao: "Casa"},
                           {valor: "APARTAMENTO", descricao: "Apartamento"}];
        var tiposDestImovel = [{}];
        var tiposSituacaoImovel = [{}];

        spyOn(imovel, 'tipos').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: tiposImovel }); }
            };
        });

        spyOn(imovel, 'tiposDestinacoes').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: tiposDestImovel }); }
            };
        });

        spyOn(imovel, 'tiposSituacao').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: tiposSituacaoImovel }); }
            };
        });

        ctrl.$onInit();
    }));

    it('deve chamar imovel.tipos()', function () {
        expect(imovel.tipos).toHaveBeenCalled();
    });

    it('deve chamar imovel.tiposDestinacoes()', function () {
        expect(imovel.tiposDestinacoes).toHaveBeenCalled();
    });

    it('deve chamar imovel.tiposSituacao()', function () {
        expect(imovel.tiposSituacao).toHaveBeenCalled();
    });

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o imovel no bindings', function () {
        expect(ctrl.imovel).toBeDefined();
    });

    it('deve chamar o evento onSalvar', function () {
        var onSalvarSpy = jasmine.createSpy('onSalvar');

        var bindings = {
            imovel: {},
            onSalvar: onSalvarSpy
        };

        var ctrl = $componentController('dadosImoveis', null, bindings);
        ctrl.imovel = {tipo: "CASA"}

        ctrl.tiposImovel = [{valor: "CASA", descricao: "Casa"},
                           {valor: "APARTAMENTO", descricao: "Apartamento"}];

        ctrl.salvar();

        expect(onSalvarSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onCancelar', function () {
        var onCancelarSpy = jasmine.createSpy('onCancelar');

        var bindings = {
            imovel: {},
            onCancelar: onCancelarSpy
        };

        var ctrl = $componentController('dadosImoveis', null, bindings);

        ctrl.cancelar();

        expect(onCancelarSpy).toHaveBeenCalled();
    });

    it('deve VALIDAR o CPF', function () {
        var deferred = $q.defer();
        deferred.resolve('yes');
        spyOn(dialogs, 'notify').and.returnValue({ result: deferred.promise });

        var cpf = "02339708001";

        ctrl.imovel = {
            vendedor: {
                cpfCnpj: "02339708001"
            }
        }

        ctrl.validarCpf(cpf);
        $rootScope.$apply();
        expect(dialogs.notify).toHaveBeenCalledWith('Atenção', 'O CPF não pode ser igual o do cadastrado');

    });

    it('validar vendedor apenas com nome', function () {
        ctrl.imovel = {
            vendedor:{
                nome:"Jamaicano do Funk"
            }
        };
        ctrl.isRequired = true;

        ctrl.validarObrigatoriedade();

        expect(ctrl.isRequired).toBe(true);
    });

    it('validar vendedor apenas com cpfCnpj', function () {
        ctrl.imovel = {
            vendedor:{
                cpfCnpj:"02339708001"
            }
        };
        ctrl.isRequired = true;

        ctrl.validarObrigatoriedade();

        expect(ctrl.isRequired).toBe(true);
    });

    it('validar vendedor sem informacoes', function () {
        ctrl.imovel = {
            vendedor:{}
        };
        ctrl.isRequired = true;

        ctrl.validarObrigatoriedade();

        expect(ctrl.isRequired).toBe(false);
    });

    it('validar vendedor inválido', function () {
        ctrl.imovel = {};
        ctrl.isRequired = false;

        ctrl.validarObrigatoriedade();

        expect(ctrl.isRequired).toBe(false);
    });

});
