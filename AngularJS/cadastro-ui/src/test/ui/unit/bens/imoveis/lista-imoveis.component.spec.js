describe('Componente LISTA IMÓVEIS', function () {

    var $componentController,
        $rootScope,
        ctrl,
        dialogs,
        $q;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _$rootScope_,_dialogs_, _$q_) {
        $componentController = _$componentController_;
        $rootScope = _$rootScope_;
        dialogs = _dialogs_;
        $q = _$q_;

        var bindings = {
            readonly: true,
            imoveis: []
        };

        ctrl = $componentController('listaImoveis',
            null,
            bindings
        );

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o imoveis no bindings', function () {
        expect(ctrl.imoveis).toBeDefined();
    });

    it('deve definir valor padrão de imoveis', function () {
        ctrl.imoveis = undefined;

        ctrl.$onInit();

        expect(ctrl.imoveis).toBeDefined();
    });

    it('deve chamar o evento onNovo', function () {
        var onNovoSpy = jasmine.createSpy('onNovo');

        var bindings = {
            imoveis: {},
            onNovo: onNovoSpy
        };

        var ctrl = $componentController('listaImoveis', null, bindings);

        ctrl.novo();

        expect(onNovoSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onEditar', function () {
        var onEditarSpy = jasmine.createSpy('onEditar');

        var bindings = {
            onEditar: onEditarSpy
        };

        var ctrl = $componentController('listaImoveis', null, bindings);

        ctrl.imoveis = [{ modoEdicao : false }];
        var imovel = ctrl.imoveis[0];

        ctrl.editar(imovel);

        expect(onEditarSpy).toHaveBeenCalled();
        expect(imovel.modoEdicao).toBe(true);
    });

    it('deve chamar o evento onVisualizar', function () {
        var onVisualizarSpy = jasmine.createSpy('onVisualizar');

        var bindings = {
            onVisualizar: onVisualizarSpy
        };

        var ctrl = $componentController('listaImoveis', null, bindings);

        ctrl.imoveis = [{ modoVisualizacao : false }];
        var imovel = ctrl.imoveis[0];

        ctrl.visualizar(imovel);

        expect(onVisualizarSpy).toHaveBeenCalled();
        expect(imovel.modoVisualizacao).toBe(true);
    });

    it('deve chamar o evento onRemover', function () {
        var onRemoverSpy = jasmine.createSpy('onRemover');

        var bindings = {
            onRemover: onRemoverSpy
        };

        var ctrl = $componentController('listaImoveis', null, bindings);

        ctrl.imoveis = [{ modoExclusao : false }];
        var imovel = ctrl.imoveis[0];

        ctrl.remover(imovel);

        expect(onRemoverSpy).toHaveBeenCalled();
    });

});
