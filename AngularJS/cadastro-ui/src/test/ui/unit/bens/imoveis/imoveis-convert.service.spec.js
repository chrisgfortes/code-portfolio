
describe('Service: imovelConvert', function () {

    var imovelConvert;

    beforeEach(module('app'));

    beforeEach(inject(function (_imovelConvert_) {
        imovelConvert = _imovelConvert_;
    }));

    it('deve estar definido', function () {
        expect(imovelConvert).toBeDefined();
    });

    it('deve converter objeto para fazer POST se vendedor sem cpf ou nome', function () {

        var imoveisEsperado = [{
            descricaoTipoDeImovel:"Casa",
            destinacao:"RESIDENCIAL",
            situacao:"LIVRE",
            tipo:"CASA",
            valorAtual:531351.53
        }];

        var imoveis = [{
            descricaoTipoDeImovel:"Casa",
            destinacao:"RESIDENCIAL",
            situacao:"LIVRE",
            tipo:"CASA",
            valorAtual:531351.53,
            vendedor:{cpfCnpj: null, nome: ""}
        }];

        var imoveisConvertido = imovelConvert.convertPost(imoveis);

        expect(imoveisConvertido).toEqual(imoveisEsperado)
    });

    it('deve converter para fazer POST', function () {

        var imoveisNaoEsperado = [{
            descricaoTipoDeImovel:"Casa",
            destinacao:"RESIDENCIAL",
            situacao:"LIVRE",
            tipo:"CASA",
            valorAtual:531351.53
        }];

        var imoveis = [{
            descricaoTipoDeImovel:"Casa",
            destinacao:"RESIDENCIAL",
            situacao:"LIVRE",
            tipo:"CASA",
            valorAtual:531351.53,
            vendedor:{cpfCnpj: "02339708001", nome: "Teste"}
        }];

        var imoveisConvertido = imovelConvert.convertPost(imoveis);


        expect(imoveisConvertido).not.toEqual(imoveisNaoEsperado);
        expect(imoveisConvertido).toEqual(imoveis);
    });

});
