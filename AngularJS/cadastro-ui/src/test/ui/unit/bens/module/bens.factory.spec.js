describe('BENS PF FACTORY', function () {

    var bens,
        moment,
        veiculoConvert,
        imovelConvert,
        HOST;

    beforeEach(module('app'));

    beforeEach(inject(function (_bens_, _moment_, _veiculoConvert_, _imovelConvert_, _HOST_) {
        bens = _bens_('terceiro');
        moment = _moment_;
        veiculoConvert = _veiculoConvert_;
        imovelConvert = _imovelConvert_;
        HOST = _HOST_;
    }));

    it('.buscar(cpf)', inject(function($httpBackend) {
        $httpBackend.whenGET(/\.html$/).respond('');

        var cpf = '08094498625';
        var bensResponse = bensPost(moment);

        $httpBackend
            .expectGET(HOST.terceiro + cpf + '/rascunho/bens')
            .respond(200, bensResponse);

        bens
            .buscar(cpf)
            .then(function (response) {
                var dados = response;

                expect(dados).toEqual(bensResponse);
            });

        $httpBackend.flush();
    }));

    it('deve adicionar valores padão .buscar(cpf)', inject(function($httpBackend) {
        $httpBackend.whenGET(/\.html$/).respond('');

        var cpf = '08094498625';
        var bensResponse = {
                veiculos: undefined,
                imoveis: undefined
        };

        $httpBackend
            .expectGET(HOST.terceiro + cpf + '/rascunho/bens')
            .respond(200, bensResponse);

        bens
            .buscar(cpf)
            .then(function (response) {
                var dados = response;

                var esperado = {
                    veiculos: [],
                    imoveis: []
                }

                expect(dados).toEqual(esperado);
            });

        $httpBackend.flush();
    }));

    it('.salvar(cpf, bens)', inject(function($httpBackend) {
        $httpBackend.whenGET(/\.html$/).respond('');

        var cpf = '08094498625';
        var bensResponse = bensPost(moment);
        var dadosFake = bensFake();

        $httpBackend
            .expectPUT(HOST.terceiro + cpf + '/rascunho/bens')
            .respond(201, dadosFake);

        bens
            .salvar(cpf, bensResponse)
            .then(function (response) {
                var dados = response.data;

                expect(dados).toEqual(dadosFake);
            });

        $httpBackend.flush();
    }));
});

function bensFake() {
    return {
        "veiculos": [{
            "tipoVeiculo": "AUTOMOTIVO",
            "situacaoVeiculo": "LIVRE",
            "modelo": "Gol",
            "marca": "Volks",
            "anoFabricacao": 2016,
            "valorVeiculo": 20000,
            "anoModelo": 2016
        }, {
            "tipoVeiculo": "AUTOMOTIVO",
            "situacaoVeiculo": "LIVRE",
            "modelo": "Palio",
            "marca": "Fiat",
            "anoFabricacao": 2013,
            "valorVeiculo": 25000,
            "anoModelo": 2013
        }],
        "imoveis": []
    }
}

function bensPost(moment) {
    return {
        "veiculos": [{
            "tipoVeiculo": "AUTOMOTIVO",
            "situacaoVeiculo": "LIVRE",
            "modelo": "Gol",
            "marca": "Volks",
            "anoFabricacao": moment("2016", "YYYY-MM-DD"),
            "valorVeiculo": 20000,
            "anoModelo": moment("2016", "YYYY-MM-DD")
        }, {
            "tipoVeiculo": "AUTOMOTIVO",
            "situacaoVeiculo": "LIVRE",
            "modelo": "Palio",
            "marca": "Fiat",
            "anoFabricacao": moment("2013", "YYYY-MM-DD"),
            "valorVeiculo": 25000,
            "anoModelo": moment("2013", "YYYY-MM-DD")
        }],
        "imoveis": []
    }
}
