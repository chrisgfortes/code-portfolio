describe('bensController', function () {

    var ctrl,
        $rootScope,
        $routeParams,
        bensFactory,
        moment,
        $q;

    var cpf = '12345678912';
    var bens = {};
    var erros = [{}];

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_$controller_, _$rootScope_, _$routeParams_, _linksFactory_, _bens_, _$q_, _moment_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $scope = _$rootScope_.$new();
        $routeParams = _$routeParams_;
        bensFactory = _bens_('terceiro');
        urls = _linksFactory_('terceiro');
        $q = _$q_;
        moment = _moment_;

        ctrl = $controller('bensController', {
            bensFactory: bensFactory,
            urls: urls,
            $routeParams: { cpf: '93994468248' },
            bensInfo: []
        });


    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('deve chamar bens.salvar(cpf, bens)', function () {
        var deferred = $q.defer();
        deferred.resolve({});

        spyOn(bensFactory, 'salvar')
            .and
            .returnValue(deferred.promise);

        var bensObj = bensPost();

        ctrl.salvar(bensObj);

        $rootScope.$apply();

        expect(bensFactory.salvar).toHaveBeenCalledWith(ctrl.cpf, bensObj);
    });

    it('deve mostrar messagem de validação, vinda pelo content', function () {
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                content: erros
            }
        });

        spyOn(bensFactory, 'salvar')
            .and
            .returnValue(deferred.promise);

        var bensObj = {
            veiculos: [],
            imoveis: []
        };

        ctrl.salvar(bensObj);

        $rootScope.$apply();

        expect(bensFactory.salvar).toHaveBeenCalledWith(ctrl.cpf, bensObj);
        expect(ctrl.erros).toEqual(erros);
    });

    it('deve mostrar messagem de validação, vinda pelo body', function () {
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                body: erros
            }
        });

        spyOn(bensFactory, 'salvar')
            .and
            .returnValue(deferred.promise);

        var bensObj = {
            veiculos: [],
            imoveis: []
        };

        ctrl.salvar(bensObj);

        $rootScope.$apply();

        expect(bensFactory.salvar).toHaveBeenCalledWith(ctrl.cpf, bensObj);
        expect(ctrl.erros).toEqual(erros);
    });

    it('não deve mostrar messagem de validação', function () {
        var deferred = $q.defer();
        deferred.reject({
            status: 400,
            data: {
                body: erros
            }
        });

        spyOn(bensFactory, 'salvar')
            .and
            .returnValue(deferred.promise);

        var bensObj = {
            veiculos: [],
            imoveis: []
        };

        ctrl.salvar(bensObj);

        $rootScope.$apply();

        expect(bensFactory.salvar).toHaveBeenCalledWith(ctrl.cpf, bensObj);
        expect(ctrl.erros).not.toBeDefined();
    });

    function bensPost() {
        return {
            "veiculos": [{
                "tipoVeiculo": "AUTOMOTIVO",
                "situacaoVeiculo": "LIVRE",
                "modelo": "Gol",
                "marca": "Volks",
                "anoFabricacao": moment("2016", "YYYY-MM-DD"),
                "valorVeiculo": 20000,
                "anoModelo": moment("2016", "YYYY-MM-DD")
            }, {
                "tipoVeiculo": "AUTOMOTIVO",
                "situacaoVeiculo": "LIVRE",
                "modelo": "Palio",
                "marca": "Fiat",
                "anoFabricacao": moment("2013", "YYYY-MM-DD"),
                "valorVeiculo": 25000,
                "anoModelo": moment("2013", "YYYY-MM-DD")
            }],
            "imoveis": []
        }
    }
});
