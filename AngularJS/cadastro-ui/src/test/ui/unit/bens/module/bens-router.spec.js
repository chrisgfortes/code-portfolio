describe('Router: bens-router', function () {

    var bens,
        $route,
        $injector,
        HOST,
        rotaService,
        $q,
        linksFactory;

    beforeEach(module('app'));

    beforeEach(inject(function (_$route_, _bens_, _$injector_, _HOST_, _linksFactory_, _rotaService_, _$q_) {
        $route = _$route_;
        bens = _bens_;
        $injector = _$injector_;
        HOST = _HOST_;
        linksFactory = _linksFactory_;
        rotaService = _rotaService_;
        $q = _$q_;
    }));

    describe('Rota de Terceiro', function () {
        testarRota('terceiro');
    });

    function testarRota(nomeDaRota) {
        it('Deve testar bens-router', function() {
            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/bens'];

            expect(rota.controller).toBe('bensController');
            expect(rota.templateUrl).toBe('./modules/bens/bens.html');
        });

        it('Deve chamar bens.buscar()', inject(function($httpBackend) {
            $httpBackend.whenGET(/\.html$/).respond('');

            var cpf = '123456789123';
            var retornoEsperado = { veiculos: [],
                                    imoveis: [] };

            $httpBackend
                .expectGET(HOST[nomeDaRota] + cpf + '/rascunho/bens')
                .respond(200, {});

            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/bens'];

            $route = {current: { params: { cpf: cpf } } };

            var retorno = rota.resolve.bensInfo(bens, $route);
            $httpBackend.flush();
            expect(retorno.$$state.value).toEqual(retornoEsperado);
        }));

        it('não encontra bens', inject(function($httpBackend) {
            $httpBackend.whenGET(/\.html$/).respond('');
            var cpf = '123456789123';

            $httpBackend
                .expectGET(HOST[nomeDaRota] + cpf + '/rascunho/bens')
                .respond(404, {});

            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/bens'];

            $route = {current: { params: { cpf: cpf } } };

            var retorno = rota.resolve.bensInfo(bens, $route);
            $httpBackend.flush();
            expect(retorno.$$state.value).toEqual({});
        }));

        it('Deve carregar urls', function () {
            var cpf = '123456789123';
            var compare = mapearRetorno(linksFactory(nomeDaRota), cpf);
            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/bens'];

            var retorno = rota.resolve.urls(linksFactory);
            retorno = mapearRetorno(retorno, cpf);

            expect(retorno).toEqual(compare);
        });

        it('Deve definir a factory', function () {
            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/bens'];

            var factory = bens(nomeDaRota);
            var retorno = rota.resolve.bensFactory(bens);

            expect(Object.getOwnPropertyNames(retorno)).toEqual(Object.getOwnPropertyNames(factory));
        });

        it('deve verificar se é terceiro', function() {
            var cpf = '93994468248';

            var deferred = $q.defer();
            deferred.resolve({});
            spyOn(rotaService, 'isTerceiro').and.returnValue(deferred.promise);

            var rota = $route.routes['/cadastro/terceiro/:cpf/bens'];
            $route = {
                current: {
                    params: {
                        cpf: cpf
                    }
                }
            }

            rota.resolve.isTerceiro($route, rotaService);
            expect(rotaService.isTerceiro).toHaveBeenCalledWith(cpf);
        });
    }

    function mapearRetorno(urls, cpf) {
        urls = urls.map(function (item) {
            item.url = item.url(cpf);
            return item
        })
        return urls;
    }
});
