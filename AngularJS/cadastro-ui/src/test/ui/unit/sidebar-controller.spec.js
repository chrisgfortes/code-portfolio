describe('controller: sidebarController', function () {

    var ctrl,
        $rootScope,
        $location,
        AuthFactory,
        VERSION,
        pessoa,
        urlService,
        $window,
        baseURI = '#/home';

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_$controller_, _$rootScope_, _$location_, _AuthFactory_, _VERSION_, _pessoa_, _urlService_, _$window_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $window = _$window_;
        $location = _$location_;
        AuthFactory = _AuthFactory_;
        VERSION = _VERSION_;
        pessoa = _pessoa_;
        urlService = _urlService_;

        spyOn(AuthFactory, 'getUsuario').and.returnValue({ nome: 'camila', cooperativa: 566 });

        ctrl = $controller('sidebarController', {
            AuthFactory: _AuthFactory_,
            VERSION: _VERSION_,
            pessoa: _pessoa_,
            urlService: _urlService_,
            $window: {
                location: {
                    hash: '#/home'
                }
            }
        });
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('testa os emits', function () {
        ctrl.isBaseUrl = false;

        $rootScope.$emit('sidebar-active', true);
        $rootScope.$broadcast('$stateChangeSuccess');

        expect(ctrl.isActive).toBe(true);
        expect(ctrl.isBaseUrl).toBe(true);
    });

    it('Deve chamar o preventDefault', function () {
        var event = {
            preventDefault: function(){}
        }

        spyOn(event, 'preventDefault');
        ctrl.menuAction(event, false);
        expect(event.preventDefault).toHaveBeenCalled();
    });

    it('Não deve chamar o preventDefault', function () {
        var event = {
            preventDefault: function(){}
        }

        spyOn(event, 'preventDefault');
        ctrl.menuAction(event, true);
        expect(event.preventDefault).not.toHaveBeenCalled();
    });

    it('Deve testar o menu clicado', function () {
        spyOn($location, 'path').and.callFake(returnSidebarFake('/cadastro'));

        var rotas = ['/cadastro'];

        var rotaUrl = ctrl.isMenuActive(rotas);
        expect(rotaUrl).toBe(true);
    });

    it('Deve testar o menu clicado e se false', function () {
        spyOn($location, 'path').and.callFake(returnSidebarFake('/alteracao'));

        var rotas = ['/cadastro'];

        var rotaUrl = ctrl.isMenuActive(rotas);

        expect(rotaUrl).toBe(false);
    });

    it('Deve testar o menu clicado e não é lista de urls', function () {
        spyOn($location, 'path').and.callFake(returnSidebarFake('/cadastro'));
        var rotas = '/cadastro';
        var rotaUrl = ctrl.isMenuActive(rotas);
        expect(rotaUrl).toBe(true);
    });

    it('Deve mostrar menu de acordo com permissão - inserir', inject(function (controleDeAcesso) {
        spyOn(controleDeAcesso, 'temPermissao').and.returnValue(true);

        var podeInserir = ctrl.isAllowedToInsert();

        expect(controleDeAcesso.temPermissao).toHaveBeenCalledWith('Cadastro Inserir');
        expect(podeInserir).toBe(true);
    }));

    function returnSidebarFake(location) {
        return function () {
            return location;
        };
    }
});
