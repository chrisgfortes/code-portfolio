describe('Directive: uppercase', function () {

    var compile;
    var scope;

    beforeEach(module('app'));

    beforeEach(inject(function (_$compile_, _$rootScope_) {
        compile = _$compile_;
        scope = _$rootScope_.$new();
    }));

    it('deve definir uppercase', function () {
        var element = getCompiledElement();
        var $inputUppercase = angular.element(element[0]);

        $inputUppercase
            .val('Some text')
            .triggerHandler('input');
        scope.$apply();

        expect($inputUppercase.attr('uppercase')).toBeDefined();
        expect($inputUppercase.attr('style')).toEqual('text-transform: uppercase;');

    });

    function getCompiledElement() {
        var element = angular.element(
             '<input uppercase type="text" ng-model="$ctrl.cpfNome.nomeCompleto" id="uppercase"'
            +'class="text-uppercase inputUppercase" maxlength="65" name="nomeCompletoCpfNome">'
        );

        var compiledElement = compile(element)(scope);
        scope.$digest();

        return compiledElement;
    }
});
