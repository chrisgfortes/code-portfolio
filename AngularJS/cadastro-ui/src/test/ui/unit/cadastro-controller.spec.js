describe('controller: cadastroController', function () {

    var ctrl, $rootScope, urlService, $q;
    var $location, $scope;


    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_$controller_, _$rootScope_, _urlService_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        urlService = _urlService_;

        ctrl = $controller('cadastroController', {
            urlService: _urlService_
        });
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('Deve testar a abertura da modal', function () {
        var modal = {
            enable: true,
            url: 'cadastro/terceiro/:cpf/dados-pessoais/',
            titulo: 'Novo cadastro',
            isAlteracao: false,
            isContaCorrente: false
        }

        spyOn($rootScope, '$emit')

        ctrl.openModal('terceiro', modal.titulo);

        expect($rootScope.$emit).toHaveBeenCalledWith('open-modal-cpf', modal);
    });

    it('Deve testar a abertura da modal', function () {
        var modal = {
            enable: true,
            url: 'alteracao/:cpf/',
            titulo: 'Associado',
            isAlteracao: true,
            isContaCorrente: false
        }
        spyOn($rootScope, '$emit')

        ctrl.openModal('alteracoes', modal.titulo);

        expect($rootScope.$emit).toHaveBeenCalledWith('open-modal-cpf', modal);
    });

    it('Deve mostrar menu de acordo com permissão - update', inject(function (controleDeAcesso) {
        spyOn(controleDeAcesso, 'temPermissao').and.returnValue(true);

        var podeAtualizar = ctrl.isAllowedToUpdate();

        expect(controleDeAcesso.temPermissao).toHaveBeenCalledWith('Cadastro Atualizar');
        expect(podeAtualizar).toBe(true);
    }));

});
