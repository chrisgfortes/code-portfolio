describe('Component: dadosCpf', function () {

    var $componentController,
        dialogs,
        $routeParams,
        $rootScope,
        $q,
        ctrl,
        pessoa;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _dialogs_, _$routeParams_, _$rootScope_, _$q_, _pessoa_) {
        $componentController = _$componentController_;
        dialogs = _dialogs_;
        $routeParams = _$routeParams_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        pessoa = _pessoa_;

        var bindings = {
            cpf:'12345789123'
        }

        ctrl = $componentController('dadosCpf', {
            dialogs: _dialogs_,
            $routeParams: _$routeParams_
        }, bindings);

        ctrl.$onInit();
    }));

    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o cpfNome no bindings', function () {
        expect(ctrl.cpf).toBeDefined();
    });

    it('deve notificar que o cpf é o mesmo do cadastrado', function () {
        var deferred = $q.defer();
        deferred.resolve('yes');

        spyOn(dialogs, 'notify')
            .and
            .returnValue({ result: deferred.promise });

        ctrl.cpf = '24187541209';
        $routeParams.cpf = '24187541209';

        ctrl.validarCPF(ctrl.cpf);

        $rootScope.$apply();

        expect(dialogs.notify)
            .toHaveBeenCalledWith('Atenção', 'O cpf não pode ser o mesmo do cadastrado.');
        expect(ctrl.cpf).toEqual("");
    });

    it('não deve notificar que o cpf é o mesmo do cadastrado', function () {
        spyOn(dialogs, 'notify').and.callThrough();

        var cpf = '12345678912';
        $routeParams.cpf = '24187541209';

        ctrl.validarCPF(cpf);

        expect(dialogs.notify).not.toHaveBeenCalled();
    });

    it('deve buscar pessoa para autocompletar campos - conjuge', function () {
        var deferred = $q.defer();
        deferred.resolve( {data: conjugeFake()} );
        spyOn(pessoa,'buscarCadastrado').and.returnValue(deferred.promise);

        var onAtribuirResultadoSpy = jasmine.createSpy('onAtribuirResultado');
        var bindings = { onAtribuirResultado: onAtribuirResultadoSpy };
        var ctrl = $componentController('dadosCpf', null, bindings);

        ctrl.isSearchable = true;
        ctrl.searchType = 'conjuge';
        ctrl.previous = undefined;

        var cpf = '12345678912';
        $routeParams.cpf = '24187541209';

        ctrl.validarCPF(cpf);

        $rootScope.$apply();

        expect(onAtribuirResultadoSpy).toHaveBeenCalledWith( { model: conjugeFake() } );
        expect(ctrl.previous).toEqual(cpf);
    });

    it('se não encontrar pessoa, retorna objeto padrão', function () {
        var deferred = $q.defer();
        deferred.reject();
        spyOn(pessoa,'buscarCadastrado').and.returnValue(deferred.promise);

        var onAtribuirResultadoSpy = jasmine.createSpy('onAtribuirResultado');
        var bindings = { onAtribuirResultado: onAtribuirResultadoSpy };
        var ctrl = $componentController('dadosCpf', null, bindings);

        ctrl.isSearchable = true;
        ctrl.searchType = 'conjuge';
        ctrl.previous = undefined;

        var cpf = '12345678912';
        $routeParams.cpf = '24187541209';
        var notFoundObj = {
            notFound: true,
            cpf:cpf
        }

        ctrl.validarCPF(cpf);

        $rootScope.$apply();

        expect(onAtribuirResultadoSpy).toHaveBeenCalledWith( { model: notFoundObj } );
        expect(ctrl.previous).toEqual(cpf);
    });

    function conjugeFake() {
        return {
                contatos: {},
                cpf: "25557213805",
                dataEmissao: "2017-03-03",
                dataNascimento: "1994-11-10",
                empresa: "Friboi",
                filiacao: { nomePai: "", nomeMae: "", nomeMaeNaoDeclarado: true, nomePaiNaoDeclarado: true },
                nomeCompleto: "Stephanie Hunt",
                numeroIdentificacao: "5464825",
                orgaoExpedidor: "DETRAN",
                profissao: "ASSISTENTE ADM",
                tipoIdentificacao: "CARTEIRA_MOTORISTA",
                ufExpedidor: "RS",
                valorRenda: 12358.46
            }
    }
});
