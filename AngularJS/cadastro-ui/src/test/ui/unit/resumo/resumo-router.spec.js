describe('Router: resumo-router', function () {

    var $route,
        resumoFactory,
        $injector;

    beforeEach(module('app'));

    beforeEach(inject(function (_$route_, resumo, _$injector_) {
        $route = _$route_;
        $injector = _$injector_;

    }));

    describe('terceiro', function () {
       testarRota('terceiro');
    });

    function testarRota(nomeDaRota) {
        it('Deve testar resumo-router', function () {
            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/resumo'];

            expect(rota.templateUrl).toBe('./modules/resumo/resumo.html');
            expect(rota.controller).toBe('resumoController');
            expect(rota.controllerAs).toBe('ctrl');
        });

        it('deve chamar retornar resumoInfo com sucesso', inject(function ($httpBackend, HOST) {
            var cpf = '123456789123';

            $httpBackend.whenGET(/\.html$/).respond('');

            $httpBackend
                .expectGET(HOST[nomeDaRota] + cpf + '/rascunho')
                .respond(200, {});

            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/resumo'];

            $route.current = {
                params: {
                    cpf: cpf
                }
            };

            var resposta = $injector.invoke(rota.resolve.resumoInfo);

            $httpBackend.flush();

            expect(resposta).toEqual(resposta);
        }));

        it('deve tratar resumoInfo com erro', inject(function ($httpBackend, HOST) {
            var cpf = '123456789123';

            $httpBackend.whenGET(/\.html$/).respond('');

            $httpBackend
                .expectGET(HOST[nomeDaRota] + cpf + '/rascunho')
                .respond(404, {});

            var rota = $route.routes['/cadastro/'+nomeDaRota+'/:cpf/resumo'];

            $route.current = {
                params: {
                    cpf: cpf
                }
            };

            var resposta = $injector.invoke(rota.resolve.resumoInfo);

            $httpBackend.flush();

            expect(resposta).toEqual(resposta);
        }));

        it('deve retornar factory resumo', inject(function (resumo) {
            var resumoFactory = resumo(nomeDaRota);
            var rota = $route.routes['/cadastro/'+nomeDaRota+'/:cpf/resumo'];

            var factory =  $injector.invoke(rota.resolve.resumoFactory);

            expect(angular.isFunction(factory['resumoGeral'])).toBe(true);
            expect(angular.isFunction(factory['salvarCadastroNoSAU'])).toBe(true);
        }));

        it('deve retornar titulo da página para modal', inject(function (resumo) {
            var titulo = (nomeDaRota === 'associado') ? 'Associado' : 'Pessoa Física';
            var rota = $route.routes['/cadastro/'+nomeDaRota+'/:cpf/resumo'];

            var retornoTitulo =  $injector.invoke(rota.resolve.tituloPagina);

            expect(retornoTitulo).toEqual(titulo);
        }));

        it('deve retornar URLs', inject(function (linksFactory) {
            var cpf = '123456789123';
            var urls = mapearRetorno(linksFactory(nomeDaRota), cpf);
            var rota = $route.routes['/cadastro/'+ nomeDaRota +'/:cpf/resumo'];

            var resposta =  $injector.invoke(rota.resolve.urls);
            resposta = mapearRetorno(resposta, cpf)

            expect(resposta).toEqual(urls);
        }));

        it('deve verificar se é terceiro', inject(function($q, rotaService) {
            var cpf = '93994468248';

            var deferred = $q.defer();
            deferred.resolve({});
            spyOn(rotaService, 'isTerceiro').and.returnValue(deferred.promise);

            var rota = $route.routes['/cadastro/'+ nomeDaRota +'/:cpf/resumo'];
            $route = {
                current: {
                    params: {
                        cpf: cpf
                    }
                }
            }

            rota.resolve.isTerceiro($route, rotaService);
            expect(rotaService.isTerceiro).toHaveBeenCalledWith(cpf);
        }));
    }

    function mapearRetorno(urls, cpf) {
        urls = urls.map(function (item) {
            item.url = item.url(cpf);
            return item
        })
        return urls;
    }

});
