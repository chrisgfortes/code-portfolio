describe('controller: resumoController', function () {

    var ctrl, $rootScope, resumoFactory, $q, $scope;

    var cpf = '12345678912';
    var erros;
    var sucesso = [{ message: 'Cadastro salvo com sucesso.' }];

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_$controller_, _$rootScope_, resumo, _$q_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        resumoFactory = resumo('associado');
        $q = _$q_;
        erros = [{}]

        ctrl = $controller('resumoController', {
            $scope: $scope,
            resumoFactory: resumoFactory,
            $routeParams: { cpf: '12345678912' },
            resumoInfo: {},
            tituloPagina: 'Terceiro',
            urls: []
        });
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('deve chamar .saveCadastro(cpf)', function () {
        var deferred = $q.defer();
        deferred.resolve({});

        spyOn(resumoFactory, 'salvarCadastroNoSAU')
            .and
            .returnValue(deferred.promise);

        spyOn($rootScope, '$emit').and.callThrough();

        ctrl.sucessos = [];
        ctrl.resumo = {
            dadosPessoais: {
                nomeCompleto: 'Nome'
            }
        };

        ctrl.salvar(cpf);

        $rootScope.$apply();

        var modal = {
            enable: true,
            cpf: ctrl.cpf,
            nome: ctrl.resumo.dadosPessoais.nomeCompleto,
            titulo: 'Terceiro',
            tipo: 'terceiro'
        };

        expect(resumoFactory.salvarCadastroNoSAU).toHaveBeenCalledWith(cpf);
        expect(ctrl.sucessos).not.toEqual([]);
        expect(ctrl.sucessos).toEqual(sucesso);
        expect($rootScope.$emit).toHaveBeenCalledWith('usuario-pendente', modal);
    });

    it('deve mostrar messagem de validação, vinda pelo error.data.content', function () {
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                content: erros
            }
        });

        spyOn(resumoFactory, 'salvarCadastroNoSAU')
            .and
            .returnValue(deferred.promise);

        ctrl.salvar(cpf);

        $rootScope.$apply();

        expect(resumoFactory.salvarCadastroNoSAU).toHaveBeenCalledWith(cpf);
        expect(ctrl.erros).toEqual(erros);
    });

    it('deve mostrar messagem padrão de validação, vinda pelo body', function () {
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                body: erros
            }
        });

        spyOn(resumoFactory, 'salvarCadastroNoSAU')
            .and
            .returnValue(deferred.promise);

        ctrl.salvar(cpf);

        $rootScope.$apply();

        expect(resumoFactory.salvarCadastroNoSAU).toHaveBeenCalledWith(cpf);
        expect(ctrl.erros).toEqual(erros);
    });

    it('deve mostrar messagem de validação, vinda pelo error.data.message', function () {
        var mensagem = 'É necessário salvar o rascunho para cadastrar o Terceiro.';
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                message: mensagem
            }
        });

        spyOn(resumoFactory, 'salvarCadastroNoSAU')
            .and
            .returnValue(deferred.promise);

        ctrl.salvar(cpf);

        $rootScope.$apply();

        expect(resumoFactory.salvarCadastroNoSAU).toHaveBeenCalledWith(cpf);
        expect(ctrl.erros).toEqual([{message: mensagem}]);
    });

    it('não deve mostrar messagem padrão de validação', function () {
        var deferred = $q.defer();
        deferred.reject({
            status: 400,
            data: {
                body: erros
            }
        });

        spyOn(resumoFactory, 'salvarCadastroNoSAU')
            .and
            .returnValue(deferred.promise);

        ctrl.salvar(cpf);

        $rootScope.$apply();

        expect(resumoFactory.salvarCadastroNoSAU).toHaveBeenCalledWith(cpf);
        expect(ctrl.erros).not.toBeDefined();
    });

    it('deve mostrar o representante legal', function () {
        var representante = {};
        ctrl.mostrarRepresentante = false;

        ctrl.representanteLegalSelecionado(representante, true);

        expect(ctrl.mostrarRepresentante).toBe(true);
    });
});
