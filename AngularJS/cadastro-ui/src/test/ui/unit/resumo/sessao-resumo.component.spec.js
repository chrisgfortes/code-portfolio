describe('Componente SESSAO RESUMO', function () {

    var $componentController,
        ctrl;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_) {
        $componentController = _$componentController_;

        ctrl = $componentController('sessaoResumo');

        ctrl.$onInit();
    }));

    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o titulo no bindings', function () {
        expect(ctrl.titulo).toBeDefined();
    });

    it('por padrão não deve mostrar a sessao', function () {
        expect(ctrl.mostraSessao).toBe(false);
    });

     it('deve mostrar a sessao', function () {
        ctrl.mostrar();

        expect(ctrl.mostraSessao).toBe(true);
    });
});
