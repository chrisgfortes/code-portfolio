describe('factory: resumoFactory', function () {

    var resumoFactory,
        moment,
        httpBackend,
        caminhoBase = 'associado',
        HOST;

    beforeEach(module('app'));

    beforeEach(inject(function ($httpBackend, resumo, _moment_, _HOST_) {
        resumoFactory = resumo(caminhoBase);
        moment = _moment_;
        HOST = _HOST_;

        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));

    afterEach(function () {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('deve estar definido no HOST, associado e terceiro', function () {
        expect(HOST.associado).toBeDefined();
        expect(HOST.terceiro).toBeDefined();
    });

    it('.resumoGeral(cpf)', function () {
        var cpf = '83084412545';
        var resumoResponse = {};

        httpBackend
            .expectGET(HOST[caminhoBase] + cpf + '/rascunho')
            .respond(200, resumoResponse);

        resumoFactory
            .resumoGeral(cpf)
            .then(function (response) {
                expect(response).toEqual(resumoResponse);
            });

        httpBackend.flush();
    });

    it('.salvarCadastroNoSAU(cpf)', function () {
        var cpf = '83084412545';
        var resumoResponse = resumoPost(moment);
        var dadosFake = resumoFake();

        httpBackend
            .expectPOST(HOST[caminhoBase] + cpf)
            .respond(201, dadosFake);

        resumoFactory
            .salvarCadastroNoSAU(cpf)
            .then(function (response) {
                var dados = response.data;

                expect(dados).toEqual(dadosFake);
            });

        httpBackend.flush();
    });
});

function resumoFake() {
    return {

    }
}

function resumoPost(moment) {
    return {

    }
}
