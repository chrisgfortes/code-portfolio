(function () {
    'use strict';

    cpfController.$inject = ['$routeParams', 'dialogs', 'focus', 'cpfService', 'pessoa'];

    angular
        .module('app')
        .component('dadosCpf', {
            bindings: {
                cpf: '=',
                disabled: '<',
                required: '<',
                isSearchable: '<',
                searchType: '<',
                onAtribuirResultado: '&',
                focusId: '@',
                nome: '@'
            },
            controller: cpfController,
            templateUrl: './components/dados-cpf-nome/dados-cpf.html'
        });

    function cpfController($routeParams, dialogs, focus, cpfService, pessoa) {
        var vm = this;

        vm.$onInit = init;
        vm.validarCPF = validarCPF;
        vm.buscarCadastrado = buscarCadastrado;
        vm.atribuirResultado = atribuirResultado;

        function init() {
            vm.previous = vm.cpf;
        }

        function validarCPF(cpf) {
            if ($routeParams.cpf == cpf) {
                var notificar = dialogs
                    .notify('Atenção', 'O cpf não pode ser o mesmo do cadastrado.');

                vm.cpf = "";

                notificar
                    .result
                    .then(function (btn) {
                        focus(vm.focusId);
                    });
            } else if (!!cpf && vm.isSearchable && vm.previous != cpf){
                vm.buscarCadastrado(cpf)
                    .then(function (retorno) {
                        vm.atribuirResultado(retorno);
                    });

                vm.previous = cpf;
            }
        }

        function atribuirResultado(model) {
            vm.onAtribuirResultado({ model: angular.copy(model) });
        }

        function buscarCadastrado(cpf) {
            return pessoa
                .buscarCadastrado(cpf, vm.searchType)
                .then(function (resultado) {
                    return resultado.data;
                })
                .catch(function (err) {
                    var notFoundObj = {
                        notFound: true,
                        cpf:cpf
                    }
                    return notFoundObj;
                });
        }
    }

}());
