describe('Directive: ValidaData', function () {

    var compile;
    var scope;

    beforeEach(module('app'));

    beforeEach(inject(function (_$compile_, _$rootScope_) {
        compile = _$compile_;
        scope = _$rootScope_.$new();        
    }));

    it('deve passar data valida', inject(function (moment) {
        scope.model = moment('2017-01-01')

        var element = getCompiledElement();
        var $input = angular.element(element[0]);

        var model = $input.controller('ngModel');
        
        scope.$apply();

        expect(model.$viewValue).toBeDefined();

    }));

    it('não deve passar data invalida', inject(function (moment) {
        scope.model = moment('2017-02-31')

        var element = getCompiledElement();
        var $input = angular.element(element[0]);

        var model = $input.controller('ngModel');

        $input.triggerHandler('blur');
        
        scope.$apply();

        expect(model.$viewValue).not.toBeDefined();

    }));

    function getCompiledElement() {

        var element = angular.element(
            "<input class=\"form-control formData\" " +
            "       ng-model=\"model\" " +
            "       ui-mask=\"99/99/9999\" " +
            "       model-view-value=\"true\" " +
            "       required-message=\"'Campo obrigatório'\" " +
            "       ng-required=\"true\" " +
            "       invalid-message=\"'Data inválida'\" " +
            "       validate-on=\"blur\" " +
            "       moment-picker=\"model\" " +
            "       format=\"DD/MM/YYYY\" " +
            "       start-view=\"month\" " +
            "       locale=\"pt-br\" " +
            "       valida-data " +
            "       > "
        );

        var compiledElement = compile(element)(scope);
        scope.$digest();

        return compiledElement;
    }
});
