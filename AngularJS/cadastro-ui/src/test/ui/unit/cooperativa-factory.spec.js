describe('factory: cooperativasFactory', function() {

    var cooperativasFactory,
         moment,
         HOST,
        httpBackend;

    beforeEach(module('app'));

    beforeEach(inject(function($httpBackend, _cooperativasFactory_, _moment_, _HOST_) {
        cooperativasFactory = _cooperativasFactory_;
         moment = _moment_;
         HOST = _HOST_;

        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));

    afterEach(function() {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.all()', function() {
        var cooperativaResponse = [];

        httpBackend
            .expectGET(HOST.auth + "/autenticacao/cooperativas")
            .respond(200, cooperativaResponse);

        cooperativasFactory
            .all()
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(cooperativaResponse);
            });

        httpBackend.flush();
    });

    it('.postos()', function() {
        var cooperativaResponse = [];
        var codigoCooperativa  = 566;

        httpBackend
            .expectGET(HOST.cooperativa + '/' + codigoCooperativa + '/postos')
            .respond(200, cooperativaResponse);

        cooperativasFactory
            .postos(codigoCooperativa)
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(cooperativaResponse);
            });

        httpBackend.flush();
    });
});