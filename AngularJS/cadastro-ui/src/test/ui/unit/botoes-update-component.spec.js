describe('Component: botoesUpdate', function () {

    var $componentController,
        ctrl,
        cpf = '53576565418';

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_) {
        $componentController = _$componentController_;

        var bindings = {
            model: {}
        }

        ctrl = $componentController('botoesUpdate', null, bindings);

    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o bindings', function () {
        expect(ctrl.model).toBeDefined();
    });

    it('deve chamar o evento onUpdate', function () {
        var onUpdateSpy = jasmine.createSpy('onUpdate');

        var bindings = {
            onUpdate: onUpdateSpy
        };

        var ctrl = $componentController('botoesUpdate', null, bindings);

        var model = {}

        ctrl.update(model);

        expect(onUpdateSpy).toHaveBeenCalled();
    });

});
