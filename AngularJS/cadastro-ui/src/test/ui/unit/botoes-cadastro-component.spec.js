describe('Component: botoesCadastro', function () {

    var $componentController,
        ctrl,
        cpf = '53576565418',
        $location;

    var links = getLinks();

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _$location_) {
        $componentController = _$componentController_;
        $location = _$location_;

        var bindings = {
            model: {},
            links: links
        }

        ctrl = $componentController('botoesCadastro', {
            $routeParams: {
                cpf: cpf
            }
        }, bindings);

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o bindings', function () {
        expect(ctrl.model).toBeDefined();
        expect(ctrl.links).toBeDefined();
    });

    it('deve chamar o evento onSalvarRascunho', function () {
        var onSalvarRascunhoSpy = jasmine.createSpy('onSalvarRascunho');

        var bindings = {
            onSalvarRascunho: onSalvarRascunhoSpy
        };

        var ctrl = $componentController('botoesCadastro', null, bindings);

        var model = {}

        ctrl.salvarRascunho(model);

        expect(onSalvarRascunhoSpy).toHaveBeenCalled();
    });

    it('deve chamar a função proximo(model)', function () {
        spyOn(ctrl, 'salvarRascunho').and.callFake(respostaFake({}));
        spyOn($location, 'path').and.returnValue('/cadastro/pessoa-fisica/' + cpf + '/dados-complementares')

        var model = {}

        ctrl.proximo(model);

        expect($location.path).toHaveBeenCalledWith('/cadastro/pessoa-fisica/' + cpf + '/bens');
        expect(ctrl.salvarRascunho).toHaveBeenCalledWith(model);
    });

    it('deve chamar a função próximo sem a model', function () {
        spyOn(ctrl, 'salvarRascunho').and.callFake(respostaFake({}));
        spyOn($location, 'path').and.returnValue('/cadastro/pessoa-fisica/' + cpf + '/dados-complementares');

        ctrl.proximo(null);

        expect(ctrl.salvarRascunho).not.toHaveBeenCalled();
        expect($location.path).toHaveBeenCalledWith('/cadastro/pessoa-fisica/' + cpf + '/bens');
    });
});


function getLinks(){
    return [
        {
            ativo: true,
            descricao: 'Dados<br>Pessoais',
            url: function (cpf) {
                return '#/cadastro/pessoa-fisica/' + cpf + '/dados-pessoais/'
            }
        },
        {
            ativo: true,
            descricao: 'Dados<br>Complementares',
            url: function (cpf) {
                return '#/cadastro/pessoa-fisica/' + cpf + '/dados-complementares'
            }
        },
        {
            ativo: true,
            descricao: 'Bens',
            url: function (cpf) {
                return '#/cadastro/pessoa-fisica/' + cpf + '/bens'
            }
        },
        {
            ativo: true,
            descricao: 'Cartão<br>Autógrafo',
            url: function (cpf) {
                return '#/cadastro/pessoa-fisica/' + cpf + '/cartao-autografo';
            }
        },
        {
            ativo: true,
            descricao: 'Renda',
            url: function (cpf) {
                return '#/cadastro/pessoa-fisica/' + cpf + '/renda';
            }
        },
        {
            ativo: true,
            descricao: 'Análise<br>Cadastral',
            url: function (cpf) {
                return '#/cadastro/pessoa-fisica/' + cpf + '/analise-cadastral';
            }
        },
        {
            ativo: true,
            descricao: 'Resumo',
            url: function (cpf) {
                return '#/cadastro/pessoa-fisica/' + cpf + '/resumo';
            }
        }
    ];
}
