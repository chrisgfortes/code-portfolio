describe('Component: Page-Title', function () {

    var $componentController,
        cpf = '53576565418',
        $routeParams,
        $rootScope,
        AuthFactory,
        pessoa,
        $q,
        $location,
        associado,
        contaCorrente,
        ctrl,
        user = {
            cpfCnpj: '53576565418',
            cooperativa: '566'
        }

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _$routeParams_, _$rootScope_, _AuthFactory_, _pessoa_, _$q_, _$location_, _associado_, _contaCorrente_) {
        $componentController = _$componentController_;
        $routeParams = _$routeParams_;
        $rootScope = _$rootScope_;
        AuthFactory = _AuthFactory_;
        pessoa = _pessoa_;
        $q = _$q_;
        $location = _$location_;
        associado = _associado_;
        contaCorrente = _contaCorrente_;

        var bindings = {
            titulo: 'RESUMO',
            subtitulo: 'NOVO CADASTRO'
        }

        spyOn(AuthFactory, 'getUsuario').and.returnValue({ nome: 'camila', cooperativa: 566 });

        ctrl = $componentController('pageTitle', {
            $routeParams: {
                cpf: cpf
            }
        }, bindings);

    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it("deve carregar dados do header - TERCEIRO", function() {
        var status = getStatus();

        var deferred = $q.defer();
        deferred.resolve({ data: status });
        spyOn(pessoa, 'status').and.returnValue(deferred.promise);

        var deferredTipos = $q.defer();
        deferredTipos.resolve(['TERCEIRO']);
        spyOn(pessoa, 'buscarTipos').and.returnValue(deferredTipos.promise);

        ctrl.$onInit();
        $rootScope.$apply();

        expect(pessoa.status).toHaveBeenCalledWith(cpf, 'terceiro');
        expect(ctrl.user).toBeDefined();
        expect(ctrl.user.cpfCnpj).toEqual(cpf);
        expect(ctrl.isAssociado).toBe(false);
        expect(ctrl.isCorrentista).toBe(false);
        expect(ctrl.matriculaAssociado).toBeUndefined();
        expect(ctrl.contaCorrentista).toBeUndefined();
    });

    it("deve carregar dados do header - ASSOCIADO", function() {
        var status = getStatus();

        var deferred = $q.defer();
        deferred.resolve({ data: status });
        spyOn(pessoa, 'status').and.returnValue(deferred.promise);

        var deferredTipos = $q.defer();
        deferredTipos.resolve(['TERCEIRO', 'ASSOCIADO']);
        spyOn(pessoa, 'buscarTipos').and.returnValue(deferredTipos.promise);

        var deferredMatricula = $q.defer();
        deferredMatricula.resolve( associadoFake() );
        spyOn(associado, 'buscarMatricula').and.returnValue(deferredMatricula.promise);

        ctrl.$onInit();
        $rootScope.$apply();

        expect(pessoa.status).toHaveBeenCalledWith(cpf, 'terceiro');
        expect(ctrl.user).toBeDefined();
        expect(ctrl.user.cpfCnpj).toEqual(cpf);
        expect(ctrl.isAssociado).toBe(true);
        expect(ctrl.isCorrentista).toBe(false);
        expect(ctrl.matriculaAssociado).toEqual(146706);
        expect(ctrl.contaCorrentista).toBeUndefined();
    });

    it("deve carregar dados do header - CORRENTISTA", function() {
        var status = getStatus();

        var deferred = $q.defer();
        deferred.resolve({ data: status });
        spyOn(pessoa, 'status').and.returnValue(deferred.promise);

        var deferredTipos = $q.defer();
        deferredTipos.resolve(['TERCEIRO', 'ASSOCIADO', 'CORRENTISTA']);
        spyOn(pessoa, 'buscarTipos').and.returnValue(deferredTipos.promise);

        var deferredMatricula = $q.defer();
        deferredMatricula.resolve( associadoFake() );
        spyOn(associado, 'buscarMatricula').and.returnValue(deferredMatricula.promise);

        var deferredContaCorrente = $q.defer();
        deferredContaCorrente.resolve( correntistaFake() );
        spyOn(contaCorrente, 'buscarPeloCpf').and.returnValue(deferredContaCorrente.promise);

        ctrl.$onInit();
        $rootScope.$apply();

        expect(pessoa.status).toHaveBeenCalledWith(cpf, 'terceiro');
        expect(ctrl.user).toBeDefined();
        expect(ctrl.user.cpfCnpj).toEqual(cpf);
        expect(ctrl.isAssociado).toBe(true);
        expect(ctrl.isCorrentista).toBe(true);
        expect(ctrl.matriculaAssociado).toEqual(146706);
        expect(ctrl.contaCorrentista).toEqual(2066459);
    });

    it('deve verificar se página é alteracao', function () {
        $location.path('/alteracao/pessoa-fisica/14545420982/renda')
        var retorno = ctrl.isPaginaAlteracao()
        expect(retorno).toBe(true);

        $location.path('/cadastro/terceiro/02339708001/dados-pessoais')
        var retorno = ctrl.isPaginaAlteracao()
        expect(retorno).toBe(false);
    });

    it('deve mostrar update de data análise apenas se página for alteração', function () {
        $location.path('/alteracao/pessoa-fisica/14545420982/renda')

        var deferred = $q.defer();
        deferred.resolve({});
        spyOn(pessoa, 'status').and.returnValue(deferred.promise);

        var deferredTipos = $q.defer();
        deferredTipos.resolve(['TERCEIRO']);
        spyOn(pessoa, 'buscarTipos').and.returnValue(deferredTipos.promise);

        ctrl.$onInit();
        $rootScope.$apply();

        expect(ctrl.isAlteracao).toBe(true);
        expect(pessoa.status).toHaveBeenCalledWith(cpf, 'terceiro');
        expect(ctrl.user).toBeDefined();
    });

    it('carregar dados mesmo sem encontrar pessoa', function () {
        var deferred = $q.defer();
        deferred.reject( { status:404 } );
        spyOn(pessoa, 'status').and.returnValue(deferred.promise);

        var deferredTipos = $q.defer();
        deferredTipos.resolve([]);
        spyOn(pessoa, 'buscarTipos').and.returnValue(deferredTipos.promise);

        ctrl.$onInit();
        $rootScope.$apply();

        expect(ctrl.isAlteracao).toBe(false);
        expect(pessoa.status).toHaveBeenCalledWith(cpf, 'terceiro');
        expect(ctrl.user).toBeDefined();
    });

    function getStatus() {
        return {cpfCnpj: cpf, nome: "João Marcos", status: "OK"}
    }

    function associadoFake() {
        return {
            matricula: {
                posto: 0,
                matricula: 146706,
                dadosCadastrais: {}
            }
        }
    }

    function correntistaFake() {
        return [{
            compePropria: true,
            numero: 2066459,
            tipo: 2,
            modalidade: "CONJUNTA",
            centralizaSaldo: true,
            gerenteConta: "AG00MENOR",
            clienteDesde: "2017-08-08",
            assinatura: "SOLIDARIA",
            cpmf: "ALIQUOTA_NORMAL",
            situacao: "LIVRE",
            posto: 2,
            empostamento: 1,
            tipoEnderecoEmpostamento: "RESIDENCIAL",
            cpfCnpjPrimeiroTitular: "74252598699",
            valorLimiteCredito: 0,
            tarifas: {
              isento: false
            }
          }]
    }

});
