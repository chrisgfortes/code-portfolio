describe('Component: loading', function () {

    var $componentController,
        enable,
        ctrl;

    var tiposProcuracao = [{}];

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_) {
        $componentController = _$componentController_;

        var bindings = {
            enable: true
        };

        ctrl = $componentController('loading', {
            enable: enable
        }, bindings);

    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });
});
