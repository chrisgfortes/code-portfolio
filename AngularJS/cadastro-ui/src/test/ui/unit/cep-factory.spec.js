describe('factory: cepFactory', function() {
    
    var cepFactory,
        httpBackend;
    
    beforeEach(module('app'));
    
    beforeEach(inject(function($httpBackend, _cepFactory_) {
        cepFactory = _cepFactory_;
    
        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));
    
    afterEach(function() {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });
    
    it('.cep(cep)', function() {
        var cep = '83084412545';
        var cepResponse = [{}];
    
        httpBackend
            .expectGET('../../../localidade-us/cadastro/localidade/v1/localidades/' + cep)
            .respond(200, cepResponse);
    
        cepFactory
            .cep(cep)
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(cepResponse);
            });

        httpBackend.flush();
    });

    it('.cidades(uf)', function() {
        var uf = 'RS';
        var cidadesResponse = [{}];
    
        httpBackend
            .expectGET('../../../localidade-us/cadastro/localidade/v1/estados/' + uf + '/cidades')
            .respond(200, cidadesResponse);
    
        cepFactory
            .cidades(uf)
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(cidadesResponse);
            });

        httpBackend.flush();
    });

    it('.estados()', function() {
        var estadosResponse = [{}];
    
        httpBackend
            .expectGET('../../../localidade-us/cadastro/localidade/v1/estados')
            .respond(200, estadosResponse);
    
        cepFactory
            .estados()
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(estadosResponse);
            });

        httpBackend.flush();
    });

    it('.paises(uf)', function() {
        var uf = 'RS';
        var paisesResponse = [{}];
    
        httpBackend
            .expectGET('../../../localidade-us/cadastro/localidade/v1/paises/')
            .respond(200, paisesResponse);
    
        cepFactory
            .paises(uf)
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(paisesResponse);
            });

        httpBackend.flush();
    });
});