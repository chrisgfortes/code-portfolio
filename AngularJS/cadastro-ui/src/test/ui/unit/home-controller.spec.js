describe('controller: homeController', function () {

    var $controller,
        $rootScope,
        ctrl;

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_$controller_, _$rootScope_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;

        ctrl = $controller('homeController');
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
        expect($rootScope.sectionName).toBe('Cadastro Unicred');
    });
});