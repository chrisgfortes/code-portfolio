describe('Component: ModalStatus', function() {

    var $componentController,
        urlService,
        $rootScope,
        $location,
        $interval,
        $timeout,
        ctrl,
        $q,
        pessoa,
        controleDeAcesso,
        associado,
        $timeout,
        cpf = '93994468248';

    beforeEach(module('app'));

    beforeEach(inject(function(_$componentController_, _$location_, _$rootScope_, _$q_, _pessoa_, _$interval_, _controleDeAcesso_, _associado_, _$timeout_) {
        $componentController = _$componentController_;
        $location = _$location_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        pessoa = _pessoa_;
        $interval = _$interval_;
        controleDeAcesso = _controleDeAcesso_;
        associado = _associado_;
        $timeout = _$timeout_;

        var bindings = {
            open: true,
            titulo: '',
            cpf: '93994468248'
        };

        ctrl = $componentController('modalStatus', {
            controleDeAcesso: _controleDeAcesso_
        }, bindings);

        ctrl.$onInit();
    }));

    it('deve estar definido', function() {
        expect(ctrl).toBeDefined();
    });

    it('deve fechar modal', function() {
        ctrl.modal.close(cpf);

        expect(ctrl.open).toEqual(false);
    });

    it('deve iniciar pulling do status', function() {
        var modal = {
            enable: true,
            cpf: cpf,
            nome: 'Lancelot de Jesus',
            tipo: 'terceiro'
        };

        $rootScope.$emit('usuario-pendente', modal);

        expect(ctrl.open).toBe(true);
        expect(ctrl.pendente).toBe(true);
        expect(ctrl.sucesso).toBe(false);
        expect(ctrl.erro).toBe(false);
        expect(ctrl.nome).toEqual(modal.nome);
        expect(ctrl.cpfCnpj).toEqual(modal.cpf);
    });

    it('deve iniciar pulling do status, com titulo', function() {
        var modal = {
            enable: true,
            cpf: cpf,
            nome: 'Lancelot de Jesus',
            titulo: 'titulo'
        };

        $rootScope.$emit('usuario-pendente', modal);

        expect(ctrl.open).toBe(true);
        expect(ctrl.pendente).toBe(true);
        expect(ctrl.sucesso).toBe(false);
        expect(ctrl.erro).toBe(false);
        expect(ctrl.nome).toEqual(modal.nome);
        expect(ctrl.titulo).toEqual('Confirmando Cadastro de ' + modal.titulo);
        expect(ctrl.cpfCnpj).toEqual(modal.cpf);
    });

    it('deve confirmar cadastro', function() {
        ctrl.isAssociado = true;
        ctrl.sucesso = false;
        ctrl.pendente = true;
        var cpf = "93994468248";
        var modalSteps = {
            enable: true,
            cpf: cpf
        };
        var statusPessoa = {
            cpfCnpj: cpf,
            nome: "Lancelot de Jesus",
            status: "OK",
            tipo: "associado"
        };

        var deferred = $q.defer();
        deferred.resolve(statusPessoa);
        spyOn(pessoa, 'status').and.returnValue(deferred.promise);

        spyOn($rootScope, '$emit').and.callThrough();

        ctrl.startPulling(cpf, statusPessoa.tipo)

        $interval.flush(3000);
        $timeout.flush();
        $rootScope.$apply();

        expect(ctrl.sucesso).toBe(true);
        expect(ctrl.pendente).toBe(false);
        expect(ctrl.open).toBe(false);
        expect(pessoa.status).toHaveBeenCalled();
        expect($rootScope.$emit).toHaveBeenCalledWith('show-modal-steps', modalSteps);
    });

    it('deve confirmar cadastro de correntista', function() {
        ctrl.isAssociado = false;
        ctrl.sucesso = false;
        ctrl.pendente = true;
        var cpf = "93994468248";
        var modalSteps = {
            enable: true,
            cpf: cpf
        };

        spyOn($rootScope, '$emit').and.callThrough();

        ctrl.startPulling(cpf, 'correntista')

        $interval.flush(3000);
        $timeout.flush();
        $rootScope.$apply();

        expect(ctrl.sucesso).toBe(true);
        expect(ctrl.pendente).toBe(false);
        expect(ctrl.open).toBe(false);
        expect($rootScope.$emit).toHaveBeenCalledWith('show-modal-steps', modalSteps);
    });

    it('deve verificar se cadastro deu ERRO', function() {
        ctrl.isAssociado = true;
        ctrl.pendente = true;
        ctrl.erro = false;
        var statusPessoa = {
            cpfCnpj: "93994468248",
            nome: "Lancelot de Jesus",
            status: "ERRO"
        };
        var deferred = $q.defer();
        deferred.resolve(statusPessoa);

        spyOn(pessoa, 'status')
            .and
            .returnValue(deferred.promise);

        ctrl.startPulling(cpf)

        $interval.flush(3000);
        $rootScope.$apply();

        expect(ctrl.pendente).toBe(false);
        expect(ctrl.erro).toBe(true);
        expect(pessoa.status).toHaveBeenCalled();
    });

});
