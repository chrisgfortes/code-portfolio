describe('factory: modalCPFFactory', function () {
    var tokenValid = 'eyJhbGciOiJIUzI1NiJ9.eyJjZFVzdWFyaW8iOiJjYW1pbGEuMDU2NiIsImNkVXN1YXJpb1Npc3RlbWEiOiIiLCJjZENvb3BlcmF0aXZhIjoiMDU2NiIsImNkQ29vcGVyYXRpdmFDZW50cmFsIjo1MDcsInN1YiI6IlByZS1hcHJvdmFkbyJ9.lSWPGnj0Oug2jMlpWNkm0glf3_5RSTh60sQjy7KLnCc';

    var dependentesUpdate,
        httpBackend,
        $httpParamSerializer,
        modalCPFFactory,
        AuthFactory,
        HOST;

    beforeEach(module('app'));

    beforeEach(inject(function ($httpBackend, _HOST_, _$httpParamSerializer_, _modalCPFFactory_, _AuthFactory_) {
        $httpParamSerializer = _$httpParamSerializer_;
        modalCPFFactory = _modalCPFFactory_;
        AuthFactory = _AuthFactory_;
        HOST = _HOST_;

        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));

    afterEach(function () {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.cooperativasModal()', function () {
        spyOn(AuthFactory, 'buscarCooperativa').and.returnValue(566);

        var cooperativa = AuthFactory.buscarCooperativa();
        var url = HOST.cadastro + '/cooperativas/' + cooperativa + '/hierarquias';
        var cooperativasModalResponse = cooperativasModalPost();


        httpBackend
            .expectGET(url)
            .respond(200, cooperativasModalResponse);

        modalCPFFactory
            .cooperativasModal()
            .then(function(response) {
                var dados = response;
                expect(dados).toEqual(cooperativasModalResponse);
            });

        httpBackend.flush();
    });

    function cooperativasModalPost() {
        return [{
            "sigla": "Unicred Sul Catarinense",
            "nomeCooperativa": null,
            "endereco": null,
            "cep": null,
            "bairro": null,
            "cnpj": null,
            "uf": null,
            "cidade": null,
            "telefone1": null,
            "telefone2": null,
            "telefoneOuvidoria": null,
            "emailOuvidoria": null,
            "codContaPresidente": null,
            "codContaVice": null,
            "codContaDiretor": null,
            "codContaDiretorFinanceiro": null,
            "codContaGerente": null,
            "codContaResponsavel": null,
            "nomeResponsavel": null,
            "cooperativa": 566,
            "cooperativaHierarquia": 507
        }]
    };
});
