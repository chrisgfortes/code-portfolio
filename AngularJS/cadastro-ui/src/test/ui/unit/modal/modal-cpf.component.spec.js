describe('Component: ModalCpf', function() {

    var $componentController,
        urlService,
        $rootScope,
        $location,
        modalCPFFactory,
        cpfService,
        AuthFactory,
        pessoa,
        $q,
        $timeout,
        ctrl,
        cpf = '93994468248';

    beforeEach(module('app'));

    beforeEach(inject(function(_$componentController_, _$location_, _urlService_, _modalCPFFactory_, _cpfService_, _$q_, _$rootScope_, _AuthFactory_, _$timeout_, _pessoa_) {
        $componentController = _$componentController_;
        $location = _$location_;
        AuthFactory = _AuthFactory_;
        $q = _$q_;
        urlService = _urlService_;
        modalCPFFactory = _modalCPFFactory_;
        cpfService = _cpfService_;
        $rootScope = _$rootScope_;
        $timeout = _$timeout_;
        pessoa = _pessoa_;

        var bindings = {
            open: true,
            url: '#/alteracao/pessoa-fisica/:cpf/dados-pessoais',
            titulo: ''
        };

        ctrl = $componentController('modalCpf', {
            urlService: _urlService_,
            modalCPFFactory: _modalCPFFactory_,
            cpfService: _cpfService_
        }, bindings);
    }));

    it('deve estar definido', function() {
        expect(ctrl).toBeDefined();
    });

    it('deve abrir modal', function() {
        var modal = {
            enable: true,
            url: 'cadastro/terceiro/:cpf/dados-pessoais/',
            titulo: 'Novo cadastro'
        }

        $rootScope.$emit('open-modal-cpf', modal);
        $timeout.flush();

        expect(ctrl.open).toBe(true);
        expect(ctrl.titulo).toEqual(modal.titulo);
        expect(ctrl.url).toEqual(modal.url);
    });

    it('deve urlService e retornar a url parseada', function() {
        var urlRedirect = urlService.parse(ctrl.url, {
            cpf: cpf
        });

        expect(urlRedirect).toEqual('#/alteracao/pessoa-fisica/' + cpf + '/dados-pessoais');
    });

    it('Cpf deve ser menor do que a quantidade de numeros', function() {
        ctrl.filter = 'CPF';
        ctrl.dadoCpf = '9399446824';
        spyOn($location, 'path');

        ctrl.buscar();
        expect(ctrl.open).toEqual(true);
        expect($location.path).not.toHaveBeenCalledWith('#/alteracao/pessoa-fisica/' + cpf + '/dados-pessoais');
    });

    it('Init() - Accept modalCPFFactory.cooperativasModal()', function() {
        var obj = _mockAuthUserCooperativa();
        var deferred = $q.defer();
        deferred.resolve(obj);

        ctrl.cooperativa = {
            selected: {}
        }

        spyOn(AuthFactory, 'getUsuario').and.returnValue({
            nome: 'camila',
            cooperativa: 566
        });
        spyOn(modalCPFFactory, 'cooperativasModal').and.returnValue(deferred.promise);

        ctrl.$onInit();
        $rootScope.$apply();

        expect(ctrl.filter).toEqual('CPF');
        expect(ctrl.cooperativa.selected).toEqual(obj);
        expect(modalCPFFactory.cooperativasModal).toHaveBeenCalled();
    });

    it('Init() - Reject modalCPFFactory.cooperativasModal()', function() {
        var deferred = $q.defer();
        deferred.reject();

        spyOn(AuthFactory, 'getUsuario').and.returnValue({
            nome: 'camila',
            cooperativa: 566
        });
        spyOn(modalCPFFactory, 'cooperativasModal').and.returnValue(deferred.promise);

        ctrl.$onInit();
        $rootScope.$apply();

        expect(ctrl.filter).toEqual('CPF');
        expect(modalCPFFactory.cooperativasModal).toHaveBeenCalled();
    });

    it('deve buscar por matrícula', function() {
        ctrl.filter = 'MATRICULA';
        ctrl.isAlteracao = true;
        ctrl.dadoMatriculaConta = 107166;
        ctrl.cooperativa = {
            selected: {
                codigo: 566
            }
        };
        var cpf = "00727084925";

        var deferred = $q.defer();
        deferred.resolve({
            data: cadastradoFake()
        });
        spyOn(pessoa, 'dadosCadastrado').and.returnValue(deferred.promise);

        var deferredTipos = $q.defer();
        deferredTipos.resolve(['TERCEIRO', 'ASSOCIADO', 'CORRENTISTA']);
        spyOn(pessoa, 'buscarTipos').and.returnValue(deferredTipos.promise);

        spyOn($location, 'path').and.callFake(respostaFake({
            status: 200
        }));
        spyOn(AuthFactory, 'salvarActionCooperativa').and.callThrough();

        ctrl.buscar();
        $rootScope.$apply();

        expect($location.path).toHaveBeenCalledWith('alteracao/' + cpf);
        expect(AuthFactory.salvarActionCooperativa).toHaveBeenCalledWith(566);
        expect(ctrl.open).toEqual(false);
    });

    it('deve permitir novo cadastro', function() {
        ctrl.filter = 'CPF';
        ctrl.dadoCpf = "00727084925";
        ctrl.cooperativa = {
            selected: {
                codigo: 566
            }
        };

        var deferredTipos = $q.defer();
        deferredTipos.resolve([]);
        spyOn(pessoa, 'buscarTipos').and.returnValue(deferredTipos.promise);

        spyOn($location, 'path').and.callFake(respostaFake({
            status: 200
        }));
        spyOn(AuthFactory, 'salvarActionCooperativa').and.callThrough();

        ctrl.buscar();
        $rootScope.$apply();

        expect($location.path).toHaveBeenCalledWith('cadastro/terceiro/' + ctrl.dadoCpf + '/dados-pessoais');
        expect(AuthFactory.salvarActionCooperativa).toHaveBeenCalledWith(566);
        expect(ctrl.open).toEqual(false);
    });

    it('deve redirecionar para cadastro caso tente fazer alteração e CPF não tenha sido confirmado', function() {
        ctrl.filter = 'CPF';
        ctrl.dadoCpf = "00727084925";
        ctrl.isAlteracao = true;
        ctrl.cooperativa = {
            selected: {
                codigo: 566
            }
        };
        var mensagem = {
            titulo: 'CPF não cadastrado',
            descricao: 'O cadastro do CPF ' + ctrl.dadoCpf + ' ainda não foi confirmado.'
        }

        var deferredTipos = $q.defer();
        deferredTipos.resolve([]);
        spyOn(pessoa, 'buscarTipos').and.returnValue(deferredTipos.promise);

        spyOn($location, 'path').and.callFake(respostaFake({
            status: 200
        }));
        spyOn(AuthFactory, 'salvarActionCooperativa').and.callThrough();
        spyOn($rootScope, '$emit').and.callThrough();

        ctrl.buscar();
        $rootScope.$apply();

        expect($location.path).toHaveBeenCalledWith('cadastro/terceiro/' + ctrl.dadoCpf + '/dados-pessoais');
        expect(AuthFactory.salvarActionCooperativa).toHaveBeenCalledWith(566);
        expect(ctrl.open).toEqual(false);
        expect($rootScope.$emit).toHaveBeenCalledWith('mostrar-mensagem', mensagem)
    });

    it('deve ir para alteração cadastral caso cadastro esteja 100% concluído', function() {
        ctrl.filter = 'CPF';
        ctrl.dadoCpf = "00727084925";
        ctrl.cooperativa = {
            selected: {
                codigo: 566
            }
        };

        var deferred = $q.defer();
        deferred.resolve({
            data: cadastradoFake()
        });
        spyOn(pessoa, 'dadosCadastrado').and.returnValue(deferred.promise);

        var deferredTipos = $q.defer();
        deferredTipos.resolve(['TERCEIRO', 'ASSOCIADO', 'CORRENTISTA', 'AGENDAMENTO_CAPITAL', 'PROD_SERV']);
        spyOn(pessoa, 'buscarTipos').and.returnValue(deferredTipos.promise);

        spyOn($location, 'path').and.callFake(respostaFake({
            status: 200
        }));
        spyOn(AuthFactory, 'salvarActionCooperativa').and.callThrough();

        ctrl.buscar();
        $rootScope.$apply();

        expect($location.path).toHaveBeenCalledWith('alteracao/' + ctrl.dadoCpf);
        expect(AuthFactory.salvarActionCooperativa).toHaveBeenCalledWith(566);
        expect(ctrl.open).toEqual(false);
    });

    it('deve abrir modal de steps caso cadastro NÃO esteja 100% concluído', function() {
        ctrl.filter = 'CPF';
        ctrl.dadoCpf = "00727084925";
        ctrl.isAlteracao = false;
        ctrl.cooperativa = {
            selected: {
                codigo: 566
            }
        };
        var modal = {
            enable: true,
            cpf: ctrl.dadoCpf
        };

        var deferred = $q.defer();
        deferred.resolve({
            data: cadastradoFake()
        });
        spyOn(pessoa, 'dadosCadastrado').and.returnValue(deferred.promise);

        var deferredTipos = $q.defer();
        deferredTipos.resolve(['TERCEIRO', 'ASSOCIADO', 'CORRENTISTA']);
        spyOn(pessoa, 'buscarTipos').and.returnValue(deferredTipos.promise);

        spyOn($location, 'path').and.callThrough();
        spyOn(AuthFactory, 'salvarActionCooperativa').and.callThrough();
        spyOn($rootScope, '$emit').and.callThrough();

        ctrl.buscar();
        $rootScope.$apply();

        expect(AuthFactory.salvarActionCooperativa).toHaveBeenCalledWith(566);
        expect(ctrl.open).toEqual(false);
        expect($rootScope.$emit).toHaveBeenCalledWith('show-modal-steps', modal);
    });

    it('deve não encontrar matrícula', function() {
        ctrl.filter = 'MATRICULA';
        ctrl.dadoMatriculaConta = 107166;
        ctrl.cooperativa = {
            selected: {
                codigo: 566
            }
        };

        var deferred = $q.defer();
        deferred.reject({
            status: 404
        });

        spyOn(pessoa, 'dadosCadastrado').and.returnValue(deferred.promise);

        ctrl.buscar();
        $rootScope.$apply();

        expect(ctrl.open).toEqual(true);
        expect(ctrl.erroBuscaCpf).toBe(true);
        expect(ctrl.dadoMatriculaConta).toBeDefined();
        expect(ctrl.dadoMatriculaConta).toEqual(107166);

    });

    it('deve alterar filtro para CPF', function() {
        ctrl.filter = 'MATRICULA';
        ctrl.dadoMatriculaConta = 107166;
        ctrl.dadoCpf = '02339708001';

        ctrl.changeFilter('CPF');

        expect(ctrl.dadoMatriculaConta).toBe(null);
        expect(ctrl.dadoCpf).toBe(null);
        expect(ctrl.erroBuscaCpf).toBe(false);
    });

    it('deve alterar filtro para MATRICULA/CONTA', function() {
        ctrl.filter = 'CPF';
        ctrl.dadoMatriculaConta = 107166;
        ctrl.dadoCpf = '02339708001';

        ctrl.changeFilter('MATRICULA');

        expect(ctrl.dadoMatriculaConta).toBe(null);
        expect(ctrl.dadoCpf).toBe(null);
        expect(ctrl.erroBuscaCpf).toBe(false);
    });

    it('deve resetar modal', function() {
        ctrl.filter = 'CONTA';
        ctrl.erroBuscaCpf = true;

        ctrl.resetarModal();

        expect(ctrl.filter).toBe('CPF');
        expect(ctrl.erroBuscaCpf).toBe(false);
    });

    function _mockAuthUserCooperativa() {
        return [{
            "sigla": "Unicred Sul Catarinense",
            "nomeCooperativa": null,
            "endereco": null,
            "cep": null,
            "bairro": null,
            "cnpj": null,
            "uf": null,
            "cidade": null,
            "telefone1": null,
            "telefone2": null,
            "telefoneOuvidoria": null,
            "emailOuvidoria": null,
            "codContaPresidente": null,
            "codContaVice": null,
            "codContaDiretor": null,
            "codContaDiretorFinanceiro": null,
            "codContaGerente": null,
            "codContaResponsavel": null,
            "nomeResponsavel": null,
            "cooperativa": 566,
            "cooperativaHierarquia": 507
        }]
    };

    function cadastradoFake() {
        return {
            cpfCnpj: "00727084925",
            nome: "Cooperado Joao38739",
            dataNascimento: "1981-10-02"
        }
    }
});
