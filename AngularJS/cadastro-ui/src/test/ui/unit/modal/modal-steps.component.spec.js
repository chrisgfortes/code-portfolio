describe('Component: ModalSteps', function() {

    var $componentController,
        urlService,
        $rootScope,
        $location,
        $timeout,
        ctrl,
        $q,
        pessoa,
        associado,
        contaCorrente,
        cpf = '93994468248';

    beforeEach(module('app'));

    beforeEach(inject(function(_$componentController_, _$location_, _$rootScope_, _$q_, _pessoa_, _associado_, _contaCorrente_) {
        $componentController = _$componentController_;
        $location = _$location_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        pessoa = _pessoa_;
        associado = _associado_;
        contaCorrente = _contaCorrente_;

        var bindings = {
            open: true,
            titulo: '',
            cpf: cpf
        };

        ctrl = $componentController('modalSteps', null, bindings);

    }));

    it('deve estar definido', function() {
        expect(ctrl).toBeDefined();
    });

    it('deve fechar modal', function() {
        ctrl.modal.close(cpf);

        expect(ctrl.open).toEqual(false);
    });

    it('deve redirecionar para criar associado', function() {
        spyOn($location, 'path').and.callThrough();
        ctrl.cpf = '12345678912'

        ctrl.criarAssociado()

        expect($location.path).toHaveBeenCalledWith('/cadastro/associado/' + ctrl.cpf + '/matricula');
    });

    it('deve redirecionar para criar conta corrente', function() {
        spyOn($location, 'path').and.callThrough();
        ctrl.cpf = '12345678912'
        ctrl.isAssociado = true;

        ctrl.criarContaCorrente()

        expect($location.path).toHaveBeenCalledWith('/cadastro/correntista/' + ctrl.cpf + '/conta-corrente');
    });

    it('deve redirecionar para criar agendamento capital', function() {
        spyOn($location, 'path').and.callThrough();
        ctrl.cpf = '12345678912'
        ctrl.isCorrentista = ctrl.isAssociado = true;

        ctrl.criarAgendamentoCapital()

        expect($location.path).toHaveBeenCalledWith('/cadastro/correntista/' + ctrl.cpf + '/agendamento-capital');
    });

    it('deve redirecionar para criar produtos e serviços', function() {
        spyOn($location, 'path').and.callThrough();
        ctrl.cpf = '12345678912'
        ctrl.isCorrentista = true;

        ctrl.criarProdServ()

        expect($location.path).toHaveBeenCalledWith('/cadastro/correntista/' + ctrl.cpf + '/produtos-e-servicos');
    });

    it('deve redirecionar para atualizar associado', function() {
        spyOn($location, 'path').and.callThrough();
        ctrl.cpf = '12345678912'

        ctrl.alteracaoCadastral()

        expect($location.path).toHaveBeenCalledWith('/alteracao/' + ctrl.cpf);
    });

    it('deve carregar modal com steps - TERCEIRO', function() {
        var cpf = "00727084925";

        var deferred = $q.defer();
        deferred.resolve({
            data: cadastradoFake()
        });
        spyOn(pessoa, 'status').and.returnValue(deferred.promise);

        var deferredTipos = $q.defer();
        deferredTipos.resolve(['TERCEIRO']);
        spyOn(pessoa, 'buscarTipos').and.returnValue(deferredTipos.promise);

        var modal = {
            enable: true,
            cpf: cpf
        };

        $rootScope.$emit('show-modal-steps', modal);
        $rootScope.$apply();

        expect(ctrl.cpf).toEqual(cpf);
        expect(ctrl.isAssociado).toBe(false, 'isAssociado');
        expect(ctrl.isCorrentista).toBe(false, 'isCorrentista');
        expect(ctrl.isAgCapital).toBe(false, 'isAgCapital');
        expect(ctrl.isProdServ).toBe(false, 'isProdServ');
    });

    it('deve carregar modal com steps - ASSOCIADO', function() {
        var cpf = "00727084925";

        var deferred = $q.defer();
        deferred.resolve({
            data: cadastradoFake()
        });
        spyOn(pessoa, 'status').and.returnValue(deferred.promise);

        var deferredTipos = $q.defer();
        deferredTipos.resolve(['TERCEIRO', 'ASSOCIADO']);
        spyOn(pessoa, 'buscarTipos').and.returnValue(deferredTipos.promise);

        var deferredMatricula = $q.defer();
        deferredMatricula.resolve( associadoFake() );
        spyOn(associado, 'buscarMatricula').and.returnValue(deferredMatricula.promise);

        var modal = {
            enable: true,
            cpf: cpf
        };

        $rootScope.$emit('show-modal-steps', modal);
        $rootScope.$apply();

        expect(ctrl.cpf).toEqual(cpf);
        expect(ctrl.isAssociado).toBe(true, 'isAssociado');
        expect(ctrl.isCorrentista).toBe(false, 'isCorrentista');
        expect(ctrl.isAgCapital).toBe(false, 'isAgCapital');
        expect(ctrl.isProdServ).toBe(false, 'isProdServ');
    });

    it('deve carregar modal com steps - ASSOCIADO - catch()', function() {
        var cpf = "00727084925";

        var deferred = $q.defer();
        deferred.resolve({
            data: cadastradoFake()
        });
        spyOn(pessoa, 'status').and.returnValue(deferred.promise);

        var deferredTipos = $q.defer();
        deferredTipos.resolve(['TERCEIRO', 'ASSOCIADO']);
        spyOn(pessoa, 'buscarTipos').and.returnValue(deferredTipos.promise);

        var deferredMatricula = $q.defer();
        deferredMatricula.reject( { status:404 } );
        spyOn(associado, 'buscarMatricula').and.returnValue(deferredMatricula.promise);

        var modal = {
            enable: true,
            cpf: cpf
        };

        $rootScope.$emit('show-modal-steps', modal);
        $rootScope.$apply();

        expect(ctrl.cpf).toEqual(cpf);
        expect(ctrl.isAssociado).toBe(true, 'isAssociado');
        expect(ctrl.isCorrentista).toBe(false, 'isCorrentista');
        expect(ctrl.isAgCapital).toBe(false, 'isAgCapital');
        expect(ctrl.isProdServ).toBe(false, 'isProdServ');

        expect(ctrl.matriculaAssociado).toBeUndefined();
        expect(ctrl.erroBuscarMatricula).toBe(true);
    });

    it('deve carregar modal com steps - CORRENTISTA', function() {
        var cpf = "00727084925";

        var deferred = $q.defer();
        deferred.resolve({
            data: cadastradoFake()
        });
        spyOn(pessoa, 'status').and.returnValue(deferred.promise);

        var deferredTipos = $q.defer();
        deferredTipos.resolve(['TERCEIRO', 'ASSOCIADO', 'CORRENTISTA']);
        spyOn(pessoa, 'buscarTipos').and.returnValue(deferredTipos.promise);

        var deferredMatricula = $q.defer();
        deferredMatricula.resolve( associadoFake() );
        spyOn(associado, 'buscarMatricula').and.returnValue(deferredMatricula.promise);

        var deferredContaCorrente = $q.defer();
        deferredContaCorrente.resolve( correntistaFake() );
        spyOn(contaCorrente, 'buscarPeloCpf').and.returnValue(deferredContaCorrente.promise);

        var modal = {
            enable: true,
            cpf: cpf
        };

        $rootScope.$emit('show-modal-steps', modal);
        $rootScope.$apply();

        expect(ctrl.cpf).toEqual(cpf);
        expect(ctrl.isAssociado).toBe(true, 'isAssociado');
        expect(ctrl.isCorrentista).toBe(true, 'isCorrentista');
        expect(ctrl.isAgCapital).toBe(false, 'isAgCapital');
        expect(ctrl.isProdServ).toBe(false, 'isProdServ');
    });

    it('deve carregar modal com steps - CORRENTISTA - catch()', function() {
        var cpf = "00727084925";

        var deferred = $q.defer();
        deferred.resolve({
            data: cadastradoFake()
        });
        spyOn(pessoa, 'status').and.returnValue(deferred.promise);

        var deferredTipos = $q.defer();
        deferredTipos.resolve(['TERCEIRO', 'ASSOCIADO', 'CORRENTISTA']);
        spyOn(pessoa, 'buscarTipos').and.returnValue(deferredTipos.promise);

        var deferredMatricula = $q.defer();
        deferredMatricula.resolve( associadoFake() );
        spyOn(associado, 'buscarMatricula').and.returnValue(deferredMatricula.promise);

        var deferredContaCorrente = $q.defer();
        deferredContaCorrente.reject( { status:404 } );
        spyOn(contaCorrente, 'buscarPeloCpf').and.returnValue(deferredContaCorrente.promise);

        var modal = {
            enable: true,
            cpf: cpf
        };

        $rootScope.$emit('show-modal-steps', modal);
        $rootScope.$apply();

        expect(ctrl.cpf).toEqual(cpf);
        expect(ctrl.isAssociado).toBe(true, 'isAssociado');
        expect(ctrl.isCorrentista).toBe(true, 'isCorrentista');
        expect(ctrl.isAgCapital).toBe(false, 'isAgCapital');
        expect(ctrl.isProdServ).toBe(false, 'isProdServ');

        expect(ctrl.contaCorrentista).toBeUndefined();
        expect(ctrl.erroBuscarConta).toBe(true);
    });

    function cadastradoFake() {
        return {
            cpfCnpj: "00727084925",
            nome: "Lancelot de Jesus",
            status: "OK"
        }
    }

    function associadoFake() {
        return {
            matricula: {
                posto: 0,
                matricula: 146706,
                dadosCadastrais: {}
            }
        }
    }

    function correntistaFake() {
        return [{
            compePropria: true,
            numero: 2066459,
            tipo: 2,
            modalidade: "CONJUNTA",
            centralizaSaldo: true,
            gerenteConta: "AG00MENOR",
            clienteDesde: "2017-08-08",
            assinatura: "SOLIDARIA",
            cpmf: "ALIQUOTA_NORMAL",
            situacao: "LIVRE",
            posto: 2,
            empostamento: 1,
            tipoEnderecoEmpostamento: "RESIDENCIAL",
            cpfCnpjPrimeiroTitular: "74252598699",
            valorLimiteCredito: 0,
            tarifas: {
              isento: false
            }
          }]
    }
});
