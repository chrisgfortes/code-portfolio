describe('Component: mensagemSucesso', function() {
    
    var $componentController,
        ctrl;
    
    beforeEach(module('app'));
    
    beforeEach(inject(function(_$componentController_) {
        $componentController = _$componentController_;
    
        var bindings = {
            mensagens: []
        };
    
        ctrl = $componentController('mensagemSucesso', null, bindings);
    
        ctrl.$onInit();
    }));
    
    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });
    
    it('deve definir o mensagens no bindings', function() {
        expect(ctrl.mensagens).toBeDefined();
    });
});