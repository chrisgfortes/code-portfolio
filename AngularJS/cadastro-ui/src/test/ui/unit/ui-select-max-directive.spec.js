describe('Directive: uiSelectMax', function () {

    var compile;
    var scope;


    beforeEach(module('app'));

    beforeEach(inject(function (_$compile_, _$rootScope_) {
        compile = _$compile_;
        scope = _$rootScope_.$new();
    }));

    it('deve definir ui-select', function () {
        var element = getCompiledElement();

        var $uiSelect = angular.element(element[0].querySelector('.ui-select-search'));

        expect($uiSelect).toBeDefined();
    });

    it('deve definir ui-select-max', function () {
        var element = getCompiledElement();

        var $uiSelect = angular.element(element[0].querySelector('.ui-select-search'));

        expect($uiSelect.attr('maxlength')).toBe('65');
    });

    function getCompiledElement() {
        var element = angular.element(
            '<ui-select ui-select-max="65" ng-model="$ctrl.fontePagadora" >'
                + '<ui-select-match>'
                + '</ui-select-match >'
                + '<ui-select-choices repeat="fontePagadora in  $ctrl.fontesPagadoras | filter:$select.search">'
                    + '<span></span>'
                + '</ui-select-choices>'
            + '</ui-select>'
        );

        var compiledElement = compile(element)(scope);
        scope.$digest();

        return compiledElement;
    }
});
