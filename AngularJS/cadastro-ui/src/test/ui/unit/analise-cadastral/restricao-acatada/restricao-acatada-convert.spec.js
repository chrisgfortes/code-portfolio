describe('Service: restricoesAcatadasConvert', function () {

    var restricoesAcatadasConvert,
        moment;

    beforeEach(module('app'));

    beforeEach(inject(function (_restricoesAcatadasConvert_, _moment_) {
        restricoesAcatadasConvert = _restricoesAcatadasConvert_;
        moment = _moment_;
    }));

    it('deve estar definido', function () {
        expect(restricoesAcatadasConvert).toBeDefined();
    });

    it('deve converter as datas para objeto Moment no GET', function () {
        var restricoesAcatadas = [
            {data: "2017-01-01"},
            {data: "2017-01-02"},
            {data: "2017-01-03"}
        ];
        var restricoesAcatadasAlterado = [
            {data: moment("2017-01-01")},
            {data: moment("2017-01-02")},
            {data: moment("2017-01-03")}
        ];

        var convertido = restricoesAcatadasConvert.convertGet(restricoesAcatadas);

        expect(convertido).toEqual(restricoesAcatadasAlterado);
    });
});
