describe('Component: dadosRestricaoAcatada', function () {

    var $componentController,
        restricaoAcatada,
        AuthFactory,
        moment,
        dialogs,
        $rootScope,
        ctrl;

    var tipoRestricao = [{}];

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _restricaoAcatada_, _AuthFactory_, _moment_, _dialogs_, _$rootScope_) {
        $componentController = _$componentController_;
        restricaoAcatada = _restricaoAcatada_;
        AuthFactory = _AuthFactory_;
        moment = _moment_;
        dialogs = _dialogs_;
        $rootScope = _$rootScope_;

        var bindings = {
            restricoesAcatadas: [],
            nomeComponente: ""
        };

        spyOn(restricaoAcatada, 'buscar').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: tipoRestricao }); }
            };
        });

        spyOn(AuthFactory, 'getUsuario').and.returnValue({ nome: 'camila', cooperativa: 566 });

        ctrl = $componentController('dadosRestricaoAcatada', {
            restricaoAcatada: _restricaoAcatada_,
            AuthFactory: _AuthFactory_,
            dialogs: _dialogs_,
            moment: _moment_,
            $rootScope: _$rootScope_
        }, bindings);

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir os bindings', function () {
        expect(ctrl.restricoesAcatadas).toBeDefined();
        expect(ctrl.nomeComponente).toBeDefined();
    });

    it('deve chamar restricaoAcatada.buscar()', function () {
        expect(restricaoAcatada.buscar).toHaveBeenCalled();
        expect(ctrl.tipoRestricao).toEqual(tipoRestricao);
    });

    it('deve chamar o evento onSalvar', function () {
        var onSalvarSpy = jasmine.createSpy('onSalvar');

        var bindings = {
            onSalvar: onSalvarSpy
        };

        var ctrl = $componentController('dadosRestricaoAcatada', null, bindings);

        ctrl.salvar();

        expect(onSalvarSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onCancelar', function () {
        var onCancelarSpy = jasmine.createSpy('onCancelar');

        var bindings = {
            onCancelar: onCancelarSpy
        };

        var ctrl = $componentController('dadosRestricaoAcatada', null, bindings);

        ctrl.cancelar();

        expect(onCancelarSpy).toHaveBeenCalled();
    });
});
