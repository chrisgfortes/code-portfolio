describe('factory: restricaoAcatada', function() {

    var restricaoAcatada,
        HOST,
        httpBackend;

    beforeEach(module('app'));

    beforeEach(inject(function($httpBackend, _restricaoAcatada_, _HOST_) {
        restricaoAcatada = _restricaoAcatada_;
        HOST = _HOST_;

        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));

    afterEach(function() {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.buscar()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.pessoa + '/analise-cadastral/restricoes-acatadas')
            .respond(200, modelsResponse);

        restricaoAcatada
            .buscar()
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });
});