describe('Component: sessaoRestricaoAcatada', function () {

    var $componentController,
        dialogs,
        $q,
        $rootScope,
        ctrl;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _dialogs_, _$q_, _$rootScope_) {
        $componentController = _$componentController_;
        dialogs = _dialogs_;
        $q = _$q_;
        $rootScope = _$rootScope_;

        var bindings = {
            restricoesAcatadas: []
        };

        ctrl = $componentController('sessaoRestricaoAcatada',
            {
                dialogs: _dialogs_
            }, bindings);

        ctrl.$onInit();
    }));

    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o procuradores no bindings', function () {
        expect(ctrl.restricoesAcatadas).toBeDefined();
    });

    it('deve SALVAR', function () {
        var restricaoAcatada = {
            data: '2017-01-01',
            observacao: 'nome completo',
            modoEdicao: false
        };

        ctrl.mostrarRestricaoAcatadaForm = true;

        ctrl.salvar(restricaoAcatada);

        expect(ctrl.restricoesAcatadas.length).toBe(1);
        expect(ctrl.mostrarRestricaoAcatadaForm).toBe(false);
    });

    it('deve EDITAR', function () {
        var restricaoAcatada = {
            data: '2017-01-01',
            observacao: 'observacao',
            modoEdicao: true
        };

       var restricaoAcatadaEditado = {
            data: '2017-01-01',
            observacao: 'observacao editada',
            modoEdicao: true
        };

        var coadijuvante = {
            data: '2017-01-01',
            observacao: 'observacao coadijuvante',
            modoEdicao: false
        };

        ctrl.restricoesAcatadas.push(restricaoAcatada);
        ctrl.restricoesAcatadas.push(coadijuvante);

        ctrl.mostrarRestricaoAcatadaForm = true;

        ctrl.salvar(restricaoAcatadaEditado);

        expect(ctrl.restricoesAcatadas[0].observacao).toBe('observacao editada');
        expect(ctrl.restricoesAcatadas[0].modoEdicao).toBe(false);
        expect(ctrl.restricoesAcatadas[1]).toEqual(coadijuvante);
        expect(ctrl.restricoesAcatadas.length).toBe(2);
        expect(ctrl.mostrarRestricaoAcatadaForm).toBe(false);
    });

    it('deve CANCELAR nova/edição', function () {
        var restricaoAcatada = {
            data: '2017-01-01',
            observacao: 'observacao',
            modoEdicao: true
        };

        ctrl.mostrarRestricaoAcatadaForm = true;

        ctrl.restricoesAcatadas.push(restricaoAcatada);

        ctrl.cancelar();

        expect(ctrl.restricoesAcatadas.length).toBe(1);
        expect(ctrl.restricoesAcatadas[0].modoEdicao).toBe(false);
        expect(ctrl.mostrarRestricaoAcatadaForm).toBe(false);
    });

    it('deve NOVO formulário', function () {
        ctrl.mostrarRestricaoAcatadaForm = false;

        ctrl.novo();

        expect(ctrl.mostrarRestricaoAcatadaForm).toBe(true);
    });

    it('deve ficar em MODO EDIÇÃO', function () {
        ctrl.mostrarRestricaoAcatadaForm = false;
        ctrl.referencia = null;

        var restricaoAcatada = {
            data: '2017-01-01',
            observacao: 'observacao',
            modoEdicao: true
        };

        ctrl.editar(restricaoAcatada);

        expect(ctrl.mostrarRestricaoAcatadaForm).toBe(true);
        expect(ctrl.restricaoAcatada).toEqual(restricaoAcatada);
    });

    it('deve REMOVER', function () {
        var deferred = $q.defer();
        deferred.resolve('yes');
        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var restricaoAcatada = {
            data: '2017-01-01',
            observacao: 'observacao',
            modoEdicao: false,
            modoExclusao: false
        };

        var coadijuvante = {
            cpf: '123456789',
            nomeCompleto: 'observacao',
            modoExclusao: true
        };

        ctrl.restricoesAcatadas.push(restricaoAcatada);
        ctrl.restricoesAcatadas.push(coadijuvante);

        ctrl.remover(coadijuvante);

        $rootScope.$apply();

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.restricoesAcatadas.length).not.toBe(2);
        expect(ctrl.restricoesAcatadas.length).toBe(1);
        expect(ctrl.restricoesAcatadas[0]).toEqual(restricaoAcatada);
    });

    it('deve cancelar a REMOVOÇÃO de uma restrição acatada', function () {
        var deferred = $q.defer();
        deferred.reject();
        spyOn(dialogs, 'confirm').and.returnValue({ result: deferred.promise });

        var restricaoAcatada = {
            data: '2017-01-01',
            observacao: 'observacao',
            modoEdicao: false,
            modoExclusao: false
        };

        var coadijuvante = {
            cpf: '123456789',
            nomeCompleto: 'observacao',
            modoExclusao: true
        };

        ctrl.restricoesAcatadas.push(restricaoAcatada);
        ctrl.restricoesAcatadas.push(coadijuvante);

        ctrl.remover(coadijuvante);

        $rootScope.$apply();

        var todosDesabilitados = ctrl.restricoesAcatadas
            .some(function (restricao) {
                return !restricao.modoExclusao;
            });

        expect(dialogs.confirm).toHaveBeenCalled();
        expect(ctrl.restricoesAcatadas.length).toBe(2);
        expect(todosDesabilitados).toBe(true);
    });
});
