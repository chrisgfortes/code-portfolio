describe('Component: listaRestricaoAcatada', function () {

    var $componentController,
        ctrl;

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_) {
        $componentController = _$componentController_;

        var bindings = {
            participacoesSocietarias: []
        };

        ctrl = $componentController('listaRestricaoAcatada',
            null,
            bindings
        );

        ctrl.restricoesAcatadas = [{
            data:"2017-03-22",
            observacao:"Teste Obs",
            responsavel:"Mario Carlos",
            tipo:"ACATADO",
            usuario:"camila",
            valor:879.82
        }];

        ctrl.$onInit();
    }));

    it('deve está definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir o restricoesAcatadas no bindings', function () {
        expect(ctrl.restricoesAcatadas).toBeDefined();
    });

    it('deve definir o valor padrão para restricoesAcatadas', function () {
        ctrl.restricoesAcatadas = undefined;

        ctrl.$onInit();

        expect(ctrl.restricoesAcatadas).toBeDefined();
    });

    it('deve chamar o evento onNovo', function () {
        var onNovoSpy = jasmine.createSpy('onNovo');

        var bindings = {
            onNovo: onNovoSpy
        };

        var ctrl = $componentController('listaRestricaoAcatada', null, bindings);

        ctrl.novo();

        expect(onNovoSpy).toHaveBeenCalled();
    });

    it('deve chamar o evento onEditar', function () {
        var onEditarSpy = jasmine.createSpy('onEditar');

        var bindings = {
            onEditar: onEditarSpy
        };

        var ctrl = $componentController('listaRestricaoAcatada', null, bindings);

        ctrl.restricoesAcatadas = [{ modoEdicao: false }];

        var restricaoAcatada = {
            modoEdicao: false
        };

        var esperado = {
            restricaoAcatada: {
                modoEdicao: true
            }
        };

        ctrl.editar(restricaoAcatada);

        expect(onEditarSpy).toHaveBeenCalledWith(esperado);
        expect(restricaoAcatada.modoEdicao).toBe(true);
    });

    it('deve chamar o evento onRemover', function () {
        var onRemoverSpy = jasmine.createSpy('onRemover');

        var bindings = {
            onRemover: onRemoverSpy
        };

        var ctrl = $componentController('listaRestricaoAcatada', null, bindings);

        var restricaoAcatada = {
            modoExclusao: false
        };

        var esperado = {
            restricaoAcatada: {
                modoExclusao: true
            }
        };

        ctrl.remover(restricaoAcatada);

        expect(onRemoverSpy).toHaveBeenCalledWith(esperado);
        expect(restricaoAcatada.modoExclusao).toBe(true);
    });
});
