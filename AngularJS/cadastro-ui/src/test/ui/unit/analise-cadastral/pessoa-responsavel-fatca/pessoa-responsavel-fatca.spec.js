describe('Component: pessoaResponsavel', function () {

    var $componentController,
        ctrl;


    var pessoaResponsavel = {
        possuiAutorizacaoResidenciaValidaExterior: true,
        possuiDomicilioFiscalAlemDeclarado: true,
        possuiGreenCardEParticipaEmpresaCapitalSuperior10PorCento: true,
        usPerson: true
    }

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_) {
        $componentController = _$componentController_;

        var bindings = {
            pessoaResponsavel: pessoaResponsavel,
            readonly: true
        };

        ctrl = $componentController('pessoaResponsavel', {}, bindings);

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve montar o objeto pessoaResponsavel onInit', function () {
        ctrl.pessoaResponsavel = undefined;
        ctrl.$onInit();
        expect(ctrl.pessoaResponsavel).not.toBe(undefined);
    });

    it('deve trocar o booleano dos atributos de FALSE para string "NAO"', function () {
        ctrl.pessoaResponsavel = {};
        ctrl.$onInit();
        expect(ctrl.pessoaResponsavel).not.toEqual({});
    });

    it('deve trocar o booleano dos atributos de TRUE para string "SIM"', function () {
        var pessoaResponsavelInicial = {
            possuiAutorizacaoResidenciaValidaExterior: true,
            possuiDomicilioFiscalAlemDeclarado: true,
            possuiGreenCardEParticipaEmpresaCapitalSuperior10PorCento: true,
            usPerson: true
        };

        ctrl.pessoaResponsavel = {
            possuiAutorizacaoResidenciaValidaExterior: true,
            possuiDomicilioFiscalAlemDeclarado: true,
            possuiGreenCardEParticipaEmpresaCapitalSuperior10PorCento: true,
            usPerson: true
        };
        ctrl.$onInit();
        expect(ctrl.pessoaResponsavel).not.toEqual(pessoaResponsavelInicial);
    });
});
