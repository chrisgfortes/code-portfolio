describe('Service: pessoaResponsavelConvert', function () {

    var pessoaResponsavelConvert;

    beforeEach(module('app'));

    beforeEach(inject(function (_pessoaResponsavelConvert_) {
        pessoaResponsavelConvert = _pessoaResponsavelConvert_;
    }));

    it('deve estar definido', function () {
        expect(pessoaResponsavelConvert).toBeDefined();
    });

    it('deve converter os valores "SIM" para booleano "TRUE" no POST', function () {
        var pessoaResponsavel = {
            possuiAutorizacaoResidenciaValidaExterior: "SIM",
            possuiDomicilioFiscalAlemDeclarado: "SIM",
            possuiGreenCardEParticipaEmpresaCapitalSuperior10PorCento: "SIM",
            usPerson: "SIM"
        };

        var pessoaResponsavelPost = {
            possuiAutorizacaoResidenciaValidaExterior: true,
            possuiDomicilioFiscalAlemDeclarado: true,
            possuiGreenCardEParticipaEmpresaCapitalSuperior10PorCento: true,
            usPerson: true
        };

        var convertido = pessoaResponsavelConvert.convertPost(pessoaResponsavel);

        expect(convertido).toEqual(pessoaResponsavelPost);
    });

    it('deve converter os booleanos "TRUE" para String "SIM" no GET', function () {
        var pessoaResponsavel = {
            possuiAutorizacaoResidenciaValidaExterior: true,
            possuiDomicilioFiscalAlemDeclarado: true,
            possuiGreenCardEParticipaEmpresaCapitalSuperior10PorCento: true,
            usPerson: true
        };

        var pessoaResponsavelGet = {
            possuiAutorizacaoResidenciaValidaExterior: "SIM",
            possuiDomicilioFiscalAlemDeclarado: "SIM",
            possuiGreenCardEParticipaEmpresaCapitalSuperior10PorCento: "SIM",
            usPerson: "SIM"
        };

        var convertido = pessoaResponsavelConvert.convertGet(pessoaResponsavel);

        expect(convertido).toEqual(pessoaResponsavelGet);
    });

    it('deve converter os valores "NAO" para booleano "FALSE" no POST', function () {
        var pessoaResponsavel = {
            possuiAutorizacaoResidenciaValidaExterior: "NAO",
            possuiDomicilioFiscalAlemDeclarado: "NAO",
            possuiGreenCardEParticipaEmpresaCapitalSuperior10PorCento: "NAO",
            usPerson: "NAO"
        };

        var pessoaResponsavelPost = {
            possuiAutorizacaoResidenciaValidaExterior: false,
            possuiDomicilioFiscalAlemDeclarado: false,
            possuiGreenCardEParticipaEmpresaCapitalSuperior10PorCento: false,
            usPerson: false
        };

        var convertido = pessoaResponsavelConvert.convertPost(pessoaResponsavel);

        expect(convertido).toEqual(pessoaResponsavelPost);
    });

    it('deve converter os booleanos "FALSE" para String "NAO" no GET', function () {
        var pessoaResponsavel = {
            possuiAutorizacaoResidenciaValidaExterior: false,
            possuiDomicilioFiscalAlemDeclarado: false,
            possuiGreenCardEParticipaEmpresaCapitalSuperior10PorCento: false,
            usPerson: false
        };

        var pessoaResponsavelGet = {
            possuiAutorizacaoResidenciaValidaExterior: "NAO",
            possuiDomicilioFiscalAlemDeclarado: "NAO",
            possuiGreenCardEParticipaEmpresaCapitalSuperior10PorCento: "NAO",
            usPerson: "NAO"
        };

        var convertido = pessoaResponsavelConvert.convertGet(pessoaResponsavel);

        expect(convertido).toEqual(pessoaResponsavelGet);
    });
});
