describe('Service: analiseCadastralConverter', function () {

    var analiseCadastralPFConverter;

    beforeEach(module('app'));

    beforeEach(inject(function (_analiseCadastralConverter_) {
        analiseCadastralConverter = _analiseCadastralConverter_;
    }));

    it('deve estar definido', function () {
        expect(analiseCadastralConverter).toBeDefined();
    });

    it('deve converter os valores "SIM" para booleano "TRUE" no POST', function () {
        var analise = {
            restricoesAcatadas: [],
            matriculaVinculada: {}
        }

        var result = {
            restricoesAcatadas: null,
            matriculaVinculada: null
        }

        var convertido = analiseCadastralConverter.get(analise);

        expect(convertido).toEqual(result);

    });
});
