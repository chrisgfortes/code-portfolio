describe('controller: analiseCadastralController', function () {

    var ctrl, $rootScope, analiseCadastralFactory, $q, urls;

    var cpf = '12345678912';
    var erros = [{}];
    var analiseCadastral = {}

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_$controller_, _$rootScope_, _analiseCadastral_, _$q_, _linksFactory_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        analiseCadastralFactory = _analiseCadastral_('terceiro');
        urls = _linksFactory_('terceiro');
        $q = _$q_;

        ctrl = $controller('analiseCadastralController', {
            analiseCadastralFactory: analiseCadastralFactory,
            urls:urls,
            $routeParams: { cpf: '12345678912' },
            analiseCadastralInfo: []
        });
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('deve chamar .salvar(cpf, analiseCadastral)', function () {
        var deferred = $q.defer();
        deferred.resolve({});

        spyOn(analiseCadastralFactory, 'salvar')
            .and
            .returnValue(deferred.promise);

        ctrl.cpf = cpf;

        var analiseCadastralIn = {
            cpf: cpf
        };

        ctrl.salvar(analiseCadastralIn);

        $rootScope.$apply();

        expect(analiseCadastralFactory.salvar).toHaveBeenCalledWith(cpf, analiseCadastralIn);
    });

    it('deve mostrar messagem de validação, vinda no body', function () {
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                body: erros
            }
        });

        spyOn(analiseCadastralFactory, 'salvar')
            .and
            .returnValue(deferred.promise);

        ctrl.cpf = cpf;

        var analiseCadastralIn = {
            cpf: cpf
        };

        ctrl.salvar(analiseCadastralIn);

        $rootScope.$apply();

        expect(analiseCadastralFactory.salvar).toHaveBeenCalledWith(cpf, analiseCadastralIn);
        expect(ctrl.erros).toEqual(erros);
    });

    it('deve mostrar messagem de validação, vinda no content', function () {
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {
                content: erros
            }
        });

        spyOn(analiseCadastralFactory, 'salvar')
            .and
            .returnValue(deferred.promise);

        ctrl.cpf = cpf;

        var analiseCadastralIn = {
            cpf: cpf
        };

        ctrl.salvar(analiseCadastralIn);

        $rootScope.$apply();

        expect(analiseCadastralFactory.salvar).toHaveBeenCalledWith(cpf, analiseCadastralIn);
        expect(ctrl.erros).toEqual(erros);
    });

    it('não deve mostrar messagem de validação', function () {
        var deferred = $q.defer();
        deferred.reject({
            status: 400,
            data: {
                body: erros
            }
        });

        spyOn(analiseCadastralFactory, 'salvar')
            .and
            .returnValue(deferred.promise);

        ctrl.cpf = cpf;

        var analiseCadastralIn = {
            cpf: cpf
        };

        ctrl.salvar(analiseCadastralIn);

        $rootScope.$apply();

        expect(analiseCadastralFactory.salvar).toHaveBeenCalledWith(cpf, analiseCadastralIn);
        expect(ctrl.erros).not.toBeDefined();
    });
});
