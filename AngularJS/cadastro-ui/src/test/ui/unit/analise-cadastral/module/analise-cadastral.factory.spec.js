describe('Factory: Análise Cadastral', function() {

    var analiseCadastral,
        moment,
        pessoaResponsavelConvert,
        informacoesComplementaresConvert,
        HOST;

    beforeEach(module('app'));

    beforeEach(inject(function(_analiseCadastral_, _moment_, _pessoaResponsavelConvert_, _informacoesComplementaresConvert_, _HOST_) {
        analiseCadastral = _analiseCadastral_('terceiro');
        moment = _moment_;
        pessoaResponsavelConvert = _pessoaResponsavelConvert_;
        informacoesComplementaresConvert = _informacoesComplementaresConvert_;
        HOST = _HOST_;
    }));

    it('.buscar(cpf)', inject(function($httpBackend) {
        $httpBackend.whenGET(/\.html$/).respond('');

        var cpf = '83084412545';
        var analiseCadastralResponse = analiseCadastralPost(moment);

        $httpBackend
            .expectGET(HOST.terceiro + cpf + '/rascunho/analisecadastral')
            .respond(200, analiseCadastralResponse);

        analiseCadastral
            .buscar(cpf)
            .then(function(response) {
                var dados = response;
                expect(dados).toEqual(analiseCadastralResponse);
            });

        $httpBackend.flush();
    }));

    it('.salvar(cpf, analiseCadastral)', inject(function($httpBackend) {
        $httpBackend.whenGET(/\.html$/).respond('');

        var cpf = '93994468248';
        var analiseCadastralResponse = analiseCadastralPost(moment);
        var dadosFake = analiseCadastralFake();

        $httpBackend
            .expectPUT(HOST.terceiro + cpf + '/rascunho/analisecadastral')
            .respond(201, dadosFake);

        analiseCadastral
            .salvar(cpf, analiseCadastralResponse)
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(dadosFake);
            });

        $httpBackend.flush();
    }));
});

function analiseCadastralFake() {
    return {
       "cpf":"93994468248",
       "informacoesComplementares":{
          "dataAnalise":"2017-03-24",
          "observacaoContrato":"6646",
          "observacao":"teste",
          "codigoPosto":1,
          "codigoTipoPessoa":3,
          "avaliacaoGerente":"OTIMO"
       },
       "restricoesAcatadas":[
          {
             "data":"2017-03-22",
             "observacao":"Teste Obs",
             "valor":879.82,
             "responsavel":"Paulo Virote",
             "usuario":"camila",
             "tipo":"ACATADO"
          },
          {
             "data":"2017-04-21",
             "observacao":"Mah oi",
             "valor":54.88,
             "responsavel":"Péricles Antunes",
             "tipo":"DESACATADO"
          }
       ],
       "matriculaVinculada":{
          "numeroMatricula":400,
          "nomeAssociado":"Cooperada Maria52"
       },
       "pessoaResponsavelFATCA":{
          "possuiAutorizacaoResidenciaValidaExterior":true,
          "possuiDomicilioFiscalAlemDeclarado":false,
          "possuiGreenCardEParticipaEmpresaCapitalSuperior10PorCento":false,
          "usPerson":true
       }
    }
}

function analiseCadastralPost(moment) {
    return {
       "cpf":"93994468248",
       "informacoesComplementares":{
          "dataAnalise": moment("2017-03-24"),
          "observacaoContrato":"6646",
          "observacao":"teste",
          "codigoPosto":'posto1',
          "codigoTipoPessoa":3,
          "avaliacaoGerente":"OTIMO"
       },
       "restricoesAcatadas":[
          {
             "data":moment("2017-03-22"),
             "observacao":"Teste Obs",
             "valor":879.82,
             "responsavel":"Paulo Virote",
             "usuario":"camila",
             "tipo":"ACATADO"
          },
          {
             "data":moment("2017-04-21"),
             "observacao":"Mah oi",
             "valor":54.88,
             "responsavel":"Péricles Antunes",
             "tipo":"DESACATADO"
          }
       ],
       "matriculaVinculada":{
          "numeroMatricula":400,
          "nomeAssociado":"Cooperada Maria52"
       },
       "pessoaResponsavelFATCA":{
          "possuiAutorizacaoResidenciaValidaExterior":true,
          "possuiDomicilioFiscalAlemDeclarado":false,
          "possuiGreenCardEParticipaEmpresaCapitalSuperior10PorCento":false,
          "usPerson":true
       }
    }
}
