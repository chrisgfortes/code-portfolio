describe('Router: analiseCadastral-router', function () {

    var $componentController,
        ctrl,
        analiseCadastral,
        $route,
        $injector,
        $q,
        HOST,
        linksFactory,
        $rootScope;

    beforeEach(module('app'));

    beforeEach(inject(function (_$route_, _analiseCadastral_, _$injector_, _$q_, _$rootScope_, _HOST_, _linksFactory_) {
        $route = _$route_;
        analiseCadastral = _analiseCadastral_;
        $injector = _$injector_;
        $q = _$q_;
        $rootScope = _$rootScope_;
        HOST = _HOST_;
        linksFactory = _linksFactory_;

    }));

    describe('Rota de Terceiro', function () {
        testarRota('terceiro');
    });

    function testarRota(nomeDaRota) {
        it('Deve testar analiseCadastral-router', function () {
            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/analise-cadastral'];

            expect(rota.controller).toBe('analiseCadastralController');
            expect(rota.templateUrl).toBe('./modules/analise-cadastral/analise-cadastral.html');
        });

        it('Deve chamar analiseCadastral.buscar()', inject(function($httpBackend) {
            $httpBackend.whenGET(/\.html$/).respond('');

            var cpf = '123456789123';

            $httpBackend
                .expectGET(HOST[nomeDaRota] + cpf + '/rascunho/analisecadastral')
                .respond(200, {});

            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/analise-cadastral'];

            $route = {current: { params: { cpf: cpf } } };

            var retorno = rota.resolve.analiseCadastralInfo(analiseCadastral, $route);
            $httpBackend.flush();
            expect(retorno.$$state.value).toEqual({});
        }));

        it('não encontra análise cadastral', inject(function($httpBackend) {
            var cpf = '123456789123';

            $httpBackend
                .expectGET(HOST[nomeDaRota] + cpf + '/rascunho/analisecadastral')
                .respond(404, {});

            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/analise-cadastral'];

            $route = {current: { params: { cpf: cpf } } };

            var retorno = rota.resolve.analiseCadastralInfo(analiseCadastral, $route);
            $httpBackend.flush();
            expect(retorno.$$state.value).toEqual({});
        }));

        it('Deve carregar urls', function () {
            var cpf = '123456789123';
            var compare = mapearRetorno(linksFactory(nomeDaRota), cpf);
            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/analise-cadastral'];

            var retorno = rota.resolve.urls(linksFactory);
            retorno = mapearRetorno(retorno, cpf);

            expect(retorno).toEqual(compare);
        });

        it('Deve definir a factory', function () {
            var rota = $route.routes['/cadastro/' + nomeDaRota + '/:cpf/analise-cadastral'];

            var factory = analiseCadastral(nomeDaRota);
            var retorno = rota.resolve.analiseCadastralFactory(analiseCadastral);

            expect(Object.getOwnPropertyNames(retorno)).toEqual(Object.getOwnPropertyNames(factory));
        });
    }

    function mapearRetorno(urls, cpf) {
        urls = urls.map(function (item) {
            item.url = item.url(cpf);
            return item
        })
        return urls;
    }
});
