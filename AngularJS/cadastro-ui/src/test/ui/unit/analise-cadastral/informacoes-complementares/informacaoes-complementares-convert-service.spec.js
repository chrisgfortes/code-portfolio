describe('Service: informacoesComplementaresConvert', function () {

    var informacoesComplementaresConvert;
    var moment;

    beforeEach(module('app'));

    beforeEach(inject(function (_informacoesComplementaresConvert_, _moment_) {
        informacoesComplementaresConvert = _informacoesComplementaresConvert_;
        moment = _moment_;
    }));

    it('deve está definido', function () {
        expect(informacoesComplementaresConvert).toBeDefined();
    });

    it('deve converter a data de analise', function() {
        var informacoesComplementares = {
            dataAnalise: '2017-05-01'
        };

        var convertido = informacoesComplementaresConvert.convertGet(informacoesComplementares);

        expect(moment.isMoment(convertido.dataAnalise)).toBe(true);
    });

    it('deve converter posto GET', function() {
        var informacoesComplementares = {
            codigoPosto: 0
        };

        var convertido = informacoesComplementaresConvert.convertGet(informacoesComplementares);

        expect(convertido.codigoPosto).toEqual('posto0');
    });

    it('deve converter posto POST', function() {
        var informacoesComplementares = {
            codigoPosto: 'posto0'
        };

        var convertido = informacoesComplementaresConvert.convertPost(informacoesComplementares);

        expect(convertido.codigoPosto).toEqual('0');
    });

    it('deve chamar convert POST', function() {
        var informacoesComplementares = {
            dataAnalise: '2017-05-01'
        };

        var convertido = informacoesComplementaresConvert.convertPost(informacoesComplementares);

        expect(convertido.dataAnalise).toEqual(informacoesComplementares.dataAnalise);
    });

    it('não deve converter a data de analise', function() {
        var informacoesComplementares = {
            dataAnalise: undefined
        };

        var convertido = informacoesComplementaresConvert.convertGet(informacoesComplementares);

        expect(convertido.dataAnalise).not.toBeDefined();
    });

});
