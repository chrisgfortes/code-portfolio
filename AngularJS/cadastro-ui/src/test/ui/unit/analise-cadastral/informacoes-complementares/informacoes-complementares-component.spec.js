describe('Component: informacoesComplementares', function () {

    var $componentController,
        avaliacaoGeral,
        AuthFactory,
        moment,
        pessoa,
        cooperativasFactory,
        ctrl;

    var codigoTipoPessoa = [{
        ddd: ''
    }];

    var postosCoopSAU = [{}];

    var avaliacoes = [{}];

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _avaliacaoGeral_, _AuthFactory_, _moment_, _pessoa_, _cooperativasFactory_) {
        $componentController = _$componentController_;
        avaliacaoGeral = _avaliacaoGeral_;
        AuthFactory = _AuthFactory_;
        moment = _moment_;
        pessoa = _pessoa_;
        cooperativasFactory = _cooperativasFactory_

        var bindings = {
            informacoesComplementares: [],
            readonly: false
        };

        spyOn(pessoa, 'tiposDePessoa').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: codigoTipoPessoa }); }
            };
        });

        spyOn(cooperativasFactory, 'postos').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: postosCoopSAU }); }
            };
        });

        spyOn(avaliacaoGeral, 'buscar').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: avaliacoes }); }
            };
        });

        spyOn(AuthFactory, 'getUsuario').and.returnValue({ nome: 'camila', cooperativa: 566 });

        ctrl = $componentController('informacoesComplementares', {
            avaliacaoGeral: _avaliacaoGeral_,
            AuthFactory: _AuthFactory_,
            moment: _moment_,
            cooperativasFactory: _cooperativasFactory_
        }, bindings);

        ctrl.informacoesComplementares = {
            avaliacaoGerente:"OTIMO",
            codigoPosto:1,
            codigoTipoPessoa:3,
            dataAnalise:"2017-03-24",
            observacao:"teste",
            observacaoContrato:"6646"
        };

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir os bindings', function () {
        expect(ctrl.informacoesComplementares).toBeDefined();
        expect(ctrl.readonly).toBeDefined();
    });

    it('deve definir o valor padrão das informacoesComplementares', function () {
        ctrl.informacoesComplementares = undefined;

        ctrl.$onInit();

        expect(moment.isMoment(ctrl.informacoesComplementares.dataAnalise)).toBe(true);
    });

    it('deve chamar pessoa.tiposDePessoa()', function () {
        expect(pessoa.tiposDePessoa).toHaveBeenCalled();
        expect(ctrl.codigoTipoPessoa).toEqual(codigoTipoPessoa);
    });

    it('deve chamar cooperativasFactory.postos()', function () {
        expect(cooperativasFactory.postos).toHaveBeenCalled();
        expect(ctrl.postosCoopSAU).toEqual(postosCoopSAU);
    });

    it('deve chamar avaliacaoGeral.buscar()', function () {
        expect(avaliacaoGeral.buscar).toHaveBeenCalled();
        expect(ctrl.avaliacoes).toEqual(avaliacoes);
    });
});
