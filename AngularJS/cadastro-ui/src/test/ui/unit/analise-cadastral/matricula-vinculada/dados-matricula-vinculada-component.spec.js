describe('Component: matriculaVinculada', function () {

    var $componentController,
        pessoa,
        dialogs,
        $scope,
        dadosProfissional,
        ctrl;
    var deferred;

    var tiposVinculos = [{}];
    var associados = [{ nome: 'JOÃO SILVA' }];

    beforeEach(module('app'));

    beforeEach(inject(function (_$componentController_, _pessoa_, _dialogs_, _$q_, _$rootScope_, _dadosProfissional_) {
        $componentController = _$componentController_;
        pessoa = _pessoa_;
        dialogs = _dialogs_;
        $scope = _$rootScope_.$new();
        dadosProfissional = _dadosProfissional_;

        var bindings = {
            vinculo: {
                nomeAssociado: ''
            },
            nomeComponente: ""
        };

        spyOn(dadosProfissional, 'tipoVinculo').and.callFake(function () {
            return {
                then: function (callback) { return callback({ data: tiposVinculos }); }
            };
        });

        deferred = _$q_.defer();

        spyOn(pessoa, 'buscar').and.returnValue(deferred.promise);

        ctrl = $componentController('matriculaVinculada', {
            pessoa: _pessoa_,
            dialogs: _dialogs_
        }, bindings);

        ctrl.$onInit();
    }));

    it('deve estar definido', function () {
        expect($componentController).toBeDefined();
    });

    it('deve definir os bindings', function () {
        expect(ctrl.vinculo).toBeDefined();
        expect(ctrl.nomeComponente).toBeDefined();
    });

    it('deve chamar dadosProfissional.tipoVinculo()', function () {
        expect(dadosProfissional.tipoVinculo).toHaveBeenCalled();
        expect(ctrl.tiposVinculos).toEqual(tiposVinculos);
    });

    it('.isVinculoValido()', function () {
        ctrl.vinculo = {
            nomeAssociado: 'JOÃO SILVA'
        }

        var estaVinculado = ctrl.isVinculoValido();

        expect(estaVinculado).toBe(true);
    });

    it('não deve buscar a matricula', function () {
        var matricula = '';

        ctrl.buscaPorMatricula(matricula);

        expect(pessoa.buscar).not.toHaveBeenCalled();
    });

    it('deve buscar a matricula', function () {
        deferred.resolve({ data: associados });
        var matricula = '123456';

        var associadoEsperado = associados[0];

        ctrl.buscaPorMatricula(matricula);

        $scope.$apply();

        expect(pessoa.buscar).toHaveBeenCalledWith(matricula);
        expect(ctrl.vinculo.nomeAssociado).toEqual(associadoEsperado.nome);
    });

    it('deve notificar que a matricula não foi encontrada', function () {
        deferred.reject();
        spyOn(dialogs, 'notify').and.callThrough();

        var matricula = '123456';

        var associadoEsperado = associados[0];

        ctrl.buscaPorMatricula(matricula);

        $scope.$apply();

        expect(pessoa.buscar).toHaveBeenCalledWith(matricula);
        expect(dialogs.notify).toHaveBeenCalled();
        expect(ctrl.vinculo).toEqual({});
    });
});
