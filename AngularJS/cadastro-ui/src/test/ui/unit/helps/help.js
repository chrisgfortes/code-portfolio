function respostaFake(itens) {
    return function () {
        return {
            then: function (callback) { return callback(itens); }
        };
    };
}
