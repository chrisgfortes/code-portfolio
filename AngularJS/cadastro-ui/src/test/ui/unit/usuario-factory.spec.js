describe('factory: usuarioFactory', function () {

    var usuarioFactory,
        httpBackend,
        AuthFactory,
        HOST;


    var tokenValid = 'eyJhbGciOiJIUzI1NiJ9.eyJjZFVzdWFyaW8iOiJjYW1pbGEuMDU2NiIsImNkVXN1YXJpb1Npc3RlbWEiOiIiLCJjZENvb3BlcmF0aXZhIjoiMDU2NiIsImNkQ29vcGVyYXRpdmFDZW50cmFsIjo1MDcsInN1YiI6IlByZS1hcHJvdmFkbyJ9.lSWPGnj0Oug2jMlpWNkm0glf3_5RSTh60sQjy7KLnCc';

    beforeEach(module('app'));

    beforeEach(inject(function ($httpBackend, _usuarioFactory_, _HOST_, _AuthFactory_) {
        usuarioFactory = _usuarioFactory_;
        HOST = _HOST_;
        AuthFactory = _AuthFactory_;

        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));

    it('.autorizacao()', function () {
        httpBackend
            .expectPOST(HOST.auth + '/autorizacao')
            .respond(200, autorizacao);

        spyOn(AuthFactory, 'buscarTokenLegacy').and.returnValue(userTokenLegacy);

        usuarioFactory
            .autorizacao()
            .then(function (response) {
                var token = response.data;
                expect(token).toEqual(autorizacao);
            });

        httpBackend.flush();

        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.getTokenLegacy()', function () {
        httpBackend
            .expectGET(HOST.authorizationLegacy)
            .respond(200, userTokenLegacy);

        usuarioFactory
            .tokenLegacy()
            .then(function (response) {
                var token = response.data;
                expect(token).toEqual(userTokenLegacy);
            });

        httpBackend.flush();

        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.getToken()', function () {
        httpBackend
            .expectGET(HOST.authorizationLegacy)
            .respond(200, userToken);

        usuarioFactory
            .tokenLegacy()
            .then(function (response) {
                var token = response.data;
                expect(token).toEqual(userToken);
            });

        httpBackend.flush();

        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.permissoes()', inject(function (HOST, $httpParamSerializer) {

        var permissoesResponse = permissoes();
        var URL = (HOST.cadastro + '/perfis');
        var DATA = {"Accept":"application/json, text/plain, */*"}
        var CONFIG = {
            headers: {
                token: tokenValid
            }
        };


        httpBackend.expectGET(URL, DATA, CONFIG).respond(200, permissoesResponse);

        usuarioFactory.permissoes().then(function(response) {
            var dados = response;
            expect(dados.data).toEqual(permissoesResponse);
        });

        httpBackend.flush();
    }));

    function autorizacao() {
        return {
            "cdUsuario": "1051",
            "nome": "Camila Souza Oliveira",
            "perfis": [
                {
                    "nmPerfil": "Cadastro Inserir"
                }
            ],
            "cooperativas": [
                {
                    "cdCooperativa": 566,
                    "dsSigla": "Unicred Sul Catarinense",
                    "cdCooperativaHierarquia": 507
                }
            ],
            "allCooperativaHierarquia": [
                {
                    "cdCooperativa": 507,
                    "dsSigla": "Unicred Central SC",
                    "cdCooperativaHierarquia": 9008
                },
                {
                    "cdCooperativa": 566,
                    "dsSigla": "Unicred Sul Catarinense",
                    "cdCooperativaHierarquia": 507
                },
                {
                    "cdCooperativa": 9008,
                    "dsSigla": "Confederação Brasil",
                    "cdCooperativaHierarquia": null
                }
            ],
            "token": userTokenLegacy()
        }
    }

    function permissoes(){
        return [{
            "chave": "Cadastro Inserir",
            "telas": [
              "/cadastro",
              "/cadastro/terceiro/:cpf/dados-pessoais/",
              "/cadastro/terceiro/:cpf/dados-complementares",
              "/cadastro/terceiro/:cpf/bens",
              "/cadastro/terceiro/:cpf/cartao-autografo",
              "/cadastro/terceiro/:cpf/renda",
              "/cadastro/terceiro/:cpf/analise-cadastral",
              "/cadastro/terceiro/:cpf/resumo",
              "/cadastro/associado/:cpf/dados-pessoais/",
              "/cadastro/associado/:cpf/dados-complementares",
              "/cadastro/associado/:cpf/bens",
              "/cadastro/associado/:cpf/cartao-autografo",
              "/cadastro/associado/:cpf/renda",
              "/cadastro/associado/:cpf/matricula",
              "/cadastro/associado/:cpf/analise-cadastral",
              "/cadastro/associado/:cpf/resumo"
            ]
          }]
    }

    function userToken() {
        return {"value":"eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICIyTWhzS0xHS2ZRbHJxNlkyc3pFcHVtVm1rR2Y4MmZXTVN6NlJLcnhac3BBIn0."
        + "eyJqdGkiOiIyOTc5YmU1Mi03NTc3LTQzMmEtYmIyYi00Yzg5Y2RkZGRjMzMiLCJleHAiOjE1MDA5ODY4OTIsIm5iZiI6MCwiaWF0IjoxNTAwOTg2MjkyLCJpc3MiOiJodHRwOi8vb2F1"
        + "dGgtdHN0LmUtdW5pY3JlZC5jb20uYnIvYXV0aC9yZWFsbXMvVW5pY3JlZFJlYWxtIiwiYXVkIjoidHN0LWNsaWVudCIsInN1YiI6ImRjNGM5ZGNhLWM3ODEtNGI4OC1hZmFhLTZjNGZjYmEwN"
        + "zkyZCIsInR5cCI6IkJlYXJlciIsImF6cCI6InRzdC1jbGllbnQiLCJhdXRoX3RpbWUiOjE1MDA5ODYyOTIsInNlc3Npb25fc3RhdGUiOiI1MzAzMDc5YS0yMWRkLTRiMDMtOTcwMS1lY2IzYW"
        + "M5YmZkMDkiLCJhY3IiOiIxIiwiY2xpZW50X3Nlc3Npb24iOiIxMmMyM2MwMC03ZDRkLTQzOWItODhkNS05OWU4Y2E0Y2I2YzIiLCJhbGxvd2VkLW9yaWdpbnMiOltdLCJyZWFsbV9hY2Nlc3MiOnsi"
        + "cm9sZXMiOlsiUk9MRV9BRE1JTiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7fSwibmFtZSI6IiIsImRpc3Rpbmd1aXNoZWROYW1lIjoiQ049Y2FtaWxhLE9VPTA1NjYsT1U9U2l0ZTEsT1U9UHJvZHVjYW"
        + "8sT1U9VXN1YXJpb3MsT1U9VFMsREM9ZS11bmljcmVkaG9tb2xvZyxEQz1jb20sREM9YnIiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJjYW1pbGEuMDU2NiJ9.YtpJgpZTD4QJEUW12WJ2qFeDLvTJV9"
        + "kmt44MzYej8MlTvIaxbwUJoMLxaoAITnUOV94C7Cl5DI1NMeeT8dAYtMPYvqj5yztfYl1NpjlKX6VF-zN66domw4FIJ5xvOBFcOVk8MgNu0RmyS1hE_Rm1oEKQs3xeyP4Q-_K6eSiuFtfHErORhbHb"
        + "l2YjlpL8Dg4uldxNTEXZJdRy0qokZ9eruEFEU8BJlBboMDmtb-Sykmlb78duya2f2k310jO3UEX_MwyDJE2n5Bs4S4wfQfe4nRn2B3VUVtZGVJh5xZpSF2sXMfUZZ52lC8"
        + "5YS_BD4x7ckQyPQB3QuRYWGOq5Kpk0jA"}
    }

    function userTokenLegacy() {
        return {"value":"eyJhbGciOiJIUzI1NiJ9.eyJjZFVzdWFyaW8iOiJjYW1pbGEuMDU2NiIsImNkQ29vcGVyYXRpdmEiOjU2NiwiY2RDb29wZXJhdGl2Y"
                + "UNlbnRyYWwiOjU2Niwic3ViIjoiUHJlLWFwcm92YWRvIn0.ybqQt9_0fBqnNIQG3tgEByV64ieDnDXkpNWAAgWziwQ"}
    }
});
