describe('Router: Limite Cheque Especial', function() {

    var $route,
        rota,
        $rootScope;

    beforeEach(module('app'));

    beforeEach(inject(function(_$route_, _$rootScope_) {
        $route = _$route_;
        $rootScope = _$rootScope_;

        rota = $route.routes['/cadastro/associado/:cpf/limite-cheque-especial'];
    }));

    it('deve validar as configurações', function() {
        expect(rota.templateUrl).toBe('./modules/cheque-especial/cheque-especial.html');
        expect(rota.controller).toBe('chequeEspecialController');
        expect(rota.controllerAs).toBe('ctrl');
        expect(rota.params.isContaCorrente).toBe(true);
    });
});