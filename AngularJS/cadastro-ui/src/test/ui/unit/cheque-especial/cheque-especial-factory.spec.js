describe('Factory: Cheque Especial', function() {

    var chequeEspecial,
        moment,
        HOST;

    beforeEach(module('app'));

    beforeEach(inject(function(_chequeEspecial_, _moment_,_HOST_) {
        chequeEspecial = _chequeEspecial_;
        moment = _moment_;
        HOST = _HOST_;
    }));

    it('.linhasDeCredito()', inject(function($httpBackend) {
        var dadosFake = { data: {}};

        $httpBackend
            .expectGET(HOST.linhaCredito + '?modalidade=CHEQUE_ESPECIAL&ativo=true&tipo-credito=EMPRESTIMO')
            .respond(200, dadosFake);

        chequeEspecial
            .linhasDeCredito()
            .then(function(linhasDeCredito) {
                expect(linhasDeCredito).toEqual(dadosFake);
            });

        $httpBackend.flush();
    }));

    it('.tiposVencimento()', inject(function($httpBackend) {
        var dadosFake = { data: {}};

        $httpBackend
            .expectGET(HOST.contaCorrente + 'tipos-vencimento-proposta')
            .respond(200, dadosFake);

        chequeEspecial
            .tiposVencimento()
            .then(function(tiposVencimento) {
                expect(tiposVencimento).toEqual(dadosFake);
            });

        $httpBackend.flush();
    }));

    it('.salvar(chequeEspecial)', inject(function($httpBackend) {
        var chequeEspecialResponse = {};
        var dadosFake = { data: {}};

        $httpBackend
            .expectPOST(HOST.contaCorrente + 'limite-cheque-especial')
            .respond(201, dadosFake);

        chequeEspecial
            .salvar(chequeEspecialResponse)
            .then(function(response) {
                var dados = response.data;

                expect(dados).toEqual(dadosFake);
            });

        $httpBackend.flush();
    }));
});