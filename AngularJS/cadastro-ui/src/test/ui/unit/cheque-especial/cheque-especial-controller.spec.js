describe('controller: chequeEspecialController', function () {

    var ctrl, $rootScope, $q, urls;

    var cpf = '12345678912';
    var erros = [{}];
    var chequeEspecial = {}

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_$controller_, _$rootScope_, _chequeEspecial_, _$q_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        chequeEspecial = _chequeEspecial_;

        ctrl = $controller('chequeEspecialController', {});
    }));

    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });

    it('deve chamar .salvar(chequeEspecial)', function () {
        var deferred = $q.defer();
        deferred.resolve({});

        spyOn(chequeEspecial, 'salvar').and.returnValue(deferred.promise);

        var chequeEspecialInfo = {}

        ctrl.salvar(chequeEspecialInfo);

        $rootScope.$apply();

        expect(chequeEspecial.salvar).toHaveBeenCalledWith(chequeEspecialInfo);
        expect(ctrl.sucessos[0]).toEqual({ message: 'Os dados Limite de Credito foram salvos com sucesso.' });
    });

    it('deve mostrar messagem de validação, vinda no details', function () {
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            details: erros
        });

        spyOn(chequeEspecial, 'salvar').and.returnValue(deferred.promise);

        var chequeEspecialInfo = {}

        ctrl.salvar(chequeEspecialInfo);

        $rootScope.$apply();

        expect(chequeEspecial.salvar).toHaveBeenCalledWith(chequeEspecialInfo);
        expect(ctrl.erros).toEqual(erros);
    });

    it('deve mostrar messagem de validação, vinda no content', function () {
        var deferred = $q.defer();
        deferred.reject({
            status: 422,
            data: {}
        });

        spyOn(chequeEspecial, 'salvar')
            .and
            .returnValue(deferred.promise);

        var chequeEspecialInfo = {}

        ctrl.salvar(chequeEspecialInfo);

        $rootScope.$apply();

        expect(chequeEspecial.salvar).toHaveBeenCalledWith(chequeEspecialInfo);
        expect(ctrl.erros).toEqual([{}]);
    });

    it('não deve mostrar messagem de validação', function () {
        var deferred = $q.defer();
        deferred.reject({
            status: 400,
            data: {
                body: erros
            }
        });

        spyOn(chequeEspecial, 'salvar')
            .and
            .returnValue(deferred.promise);

        var chequeEspecialInfo = {}

        ctrl.salvar(chequeEspecialInfo);

        $rootScope.$apply();

        expect(chequeEspecial.salvar).toHaveBeenCalledWith(chequeEspecialInfo);
        expect(ctrl.erros).toEqual([{ message: 'Ocorreu um erro no servidor, tente novamente.' }])
    });
});
