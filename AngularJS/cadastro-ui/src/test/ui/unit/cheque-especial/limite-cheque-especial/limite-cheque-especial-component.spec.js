describe('Component: dadosLimiteChequeEspecial', function () {
    
    var $rootScope,
        chequeEspecial,
        ctrl,
        $q;
    
    beforeEach(angular.mock.module('app'));
    
    beforeEach(inject(function (_$componentController_, _$rootScope_, _$q_, _chequeEspecial_) {
        $componentController = _$componentController_;
        $rootScope = _$rootScope_;
        $q = _$q_;
        chequeEspecial = _chequeEspecial_;
    
        ctrl = $componentController('dadosLimiteChequeEspecial', {
            chequeEspecial: _chequeEspecial_,
            limiteChequeEspecial: {}
        });
    }));
    
    it('deve estar definido', function () {
        expect(ctrl).toBeDefined();
    });
    
    it('deve iniciar carragando esses dados', function () {
        ctrl.linhasDeCredito = undefined;
        ctrl.tiposVencimento = undefined;
        var deferred = $q.defer();
        deferred.resolve([{}])

        spyOn(chequeEspecial, 'linhasDeCredito').and.returnValue(deferred.promise);
        spyOn(chequeEspecial, 'tiposVencimento').and.returnValue(deferred.promise);
    
        ctrl.$onInit();

        $rootScope.$apply()
    
        expect(chequeEspecial.linhasDeCredito).toHaveBeenCalled();
        expect(ctrl.linhasDeCredito.length).toBe(1)
        expect(chequeEspecial.tiposVencimento).toHaveBeenCalled();
        expect(ctrl.tiposVencimento.length).toBe(1)
    });
});