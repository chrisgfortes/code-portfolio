describe('factory: pessoa', function() {

    var pessoa,
        HOST,
        httpBackend;

    beforeEach(module('app'));

    beforeEach(inject(function($httpBackend, _pessoa_, _HOST_) {
        pessoa = _pessoa_;
        HOST = _HOST_;

        $httpBackend.whenGET(/\.html$/).respond('');
        httpBackend = $httpBackend;
    }));

    afterEach(function() {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('.buscar()', function() {
        var modelsResponse = [{}];
        var matricula = '789456';

        httpBackend
            .expectGET(HOST.sau + '/pessoa/terceiro/' + matricula  +'/associados')
            .respond(200, modelsResponse);

        pessoa
            .buscar(matricula)
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.estadoCivil()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.pessoa + '/fisica/estados-civis')
            .respond(200, modelsResponse);

        pessoa
            .estadoCivil()
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.regimeCasamento()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.pessoa + '/fisica/regimes-casamento')
            .respond(200, modelsResponse);

        pessoa
            .regimeCasamento()
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.tiposDePessoa()', function() {
        var modelsResponse = [{}];

        httpBackend
            .expectGET(HOST.sau + '/pessoa/fisica/tipos')
            .respond(200, modelsResponse);

        pessoa
            .tiposDePessoa()
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(modelsResponse);
            });

        httpBackend.flush();
    });

    it('.status()', function() {
        var usuario = {
          "cpfCnpj": "26139411548",
          "nome": "TESTE TSET SET",
          "status": []
        };
        var cpf = 26139411548;

        httpBackend
            .expectGET(HOST.terceiro + cpf + '/status')
            .respond(200, usuario);

        pessoa
            .status(cpf, 'terceiro')
            .then(function(response){
                expect(response).toEqual(usuario);
            });

        httpBackend.flush();
    });

    it('.buscarCadastrado(cpf, tipo) - conjuge', function() {
        var cpf = 26139411548;
        var conjuge = conjugeFake();

        httpBackend
            .expectGET(HOST.dadosPessoais + '/' + cpf + '/conjuge')
            .respond(200, conjuge);

        pessoa
            .buscarCadastrado(cpf, 'conjuge')
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(conjuge);
            });

        httpBackend.flush();
    });

    it('.dadosCadastrado(tipo, valor)', function() {
        var valor = 107166;
        var tipo = 'MATRICULA';
        var cadastrado = cadastradoFake();

        httpBackend
            .expectGET(HOST.terceiro + 'terceiro?tipo=' + tipo + '&valor=' + valor)
            .respond(200, cadastrado);

        pessoa
            .dadosCadastrado(tipo, valor)
            .then(function(response) {
                var dados = response.data;
                expect(dados).toEqual(cadastrado);
            });

        httpBackend.flush();
    });

    it('.buscarTipos(cpf)', function() {
        var cpf = '26139411548';
        var tipos = tiposFake();

        httpBackend
            .expectGET(HOST.terceiro + 'terceiro/' + cpf + '/tipos-cadastro')
            .respond(200, tipos);

        pessoa
            .buscarTipos(cpf)
            .then(function(response) {
                expect(response).toEqual(tipos);
            });

        httpBackend.flush();
    });

    function tiposFake() {
        return ['TERCEIRO', 'ASSOCIADO', 'CORRENTISTA'];
    }

    function conjugeFake() {
        return {
                contatos: {},
                cpf: "25557213805",
                dataEmissao: "2017-03-03",
                dataNascimento: "1994-11-10",
                empresa: "Friboi",
                filiacao: { nomePai: "", nomeMae: "", nomeMaeNaoDeclarado: true, nomePaiNaoDeclarado: true },
                nomeCompleto: "Stephanie Hunt",
                numeroIdentificacao: "5464825",
                orgaoExpedidor: "DETRAN",
                profissao: "ASSISTENTE ADM",
                tipoIdentificacao: "CARTEIRA_MOTORISTA",
                ufExpedidor: "RS",
                valorRenda: 12358.46
            }
    }

    function cadastradoFake() {
        return {
              cpfCnpj: "00727084925",
              nome: "Cooperado Joao38739",
              dataNascimento: "1981-10-02"
            }
    }
});
