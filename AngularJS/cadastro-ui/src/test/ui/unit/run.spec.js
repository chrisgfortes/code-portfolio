describe('Run.js', function () {

    var $location, $routeParams, $rootScope, pessoa, $q, cpfService, $route;

    beforeEach(angular.mock.module('app'));

    beforeEach(inject(function (_$rootScope_, _$routeParams_, _AuthFactory_) {
        $rootScope = _$rootScope_;
        $routeParams = _$routeParams_;
        AuthFactory = _AuthFactory_;

    }));

    it('$routeChangeSuccess', inject(function (controleDeAcesso) {
        spyOn(AuthFactory, 'isAuthenticated').and.returnValue(true);
        spyOn($rootScope, '$emit').and.callThrough();

        $routeParams = { cpf:'38948522671' };
        $rootScope.$broadcast('$routeChangeSuccess');
        $rootScope.$apply();

        expect($rootScope.$emit).toHaveBeenCalled();
    }));

});
