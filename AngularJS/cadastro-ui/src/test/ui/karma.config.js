module.exports = function (config) {
    config.set({
        basePath: './../',
        frameworks: ['jasmine'],
        plugins: [
          'karma-jasmine',
          'karma-phantomjs-launcher',
          'karma-coverage',
          'karma-spec-reporter'
        ],
        files: [
            './../../src/main/webapp/bower_components/angular/angular.js',
            './../../src/main/webapp/bower_components/angular-mocks/angular-mocks.js',
            './../../src/main/webapp/bower_components/jquery/dist/jquery.js',
            './../../src/main/webapp/bower_components/oclazyload/dist/ocLazyLoad.js',
            './../../src/main/webapp/bower_components/angular-route/angular-route.js',
            './../../src/main/webapp/bower_components/angular-ui-select/dist/select.js',
            './../../src/main/webapp/bower_components/angular-sanitize/angular-sanitize.js',
            './../../src/main/webapp/bower_components/angular-base64/angular-base64.js',
            './../../src/main/webapp/bower_components/angular-steps/dist/angular-steps.js',
            './../../src/main/webapp/bower_components/angular-mass-autocomplete/massautocomplete.min.js',
            './../../src/main/webapp/bower_components/angular-ui-mask/dist/mask.js',
            './../../src/main/webapp/bower_components/bootstrap/dist/js/bootstrap.js',
            './../../src/main/webapp/bower_components/tg-angular-validator/dist/angular-validator.js',
            './../../src/main/webapp/bower_components/angular-input-masks/angular-input-masks-standalone.js',
            './../../src/main/webapp/bower_components/moment/moment.js',
            './../../src/main/webapp/bower_components/angular-moment/angular-moment.js',
            './../../src/main/webapp/bower_components/angular-bootstrap-checkbox/angular-bootstrap-checkbox.js',
            './../../src/main/webapp/bower_components/angular-animate/angular-animate.js',
            './../../src/main/webapp/bower_components/angular-dialog-service/dist/dialogs.js',
            './../../src/main/webapp/bower_components/angular-dialog-service/dist/dialogs-default-translations.js',
            './../../src/main/webapp/bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
            './../../src/main/webapp/bower_components/angular-translate/angular-translate.js',
            './../../src/main/webapp/bower_components/angular-moment-picker/dist/angular-moment-picker.min.js',
            './../../src/main/webapp/bower_components/angular-br-filters/release/angular-br-filters.min.js',
            './../../src/main/webapp/bower_components/angular-base64-upload/dist/angular-base64-upload.min.js',
            './../../src/main/webapp/bower_components/ng-file-upload/ng-file-upload.min.js',
            './../../src/main/webapp/bower_components/angular-http-status/angular-http-status.js',
            './../../src/main/webapp/bower_components/gerador-validador-cpf/dist/js/CPF.js',
            './../../src/main/webapp/bower_components/angular-locale-pt-br/angular-locale_pt-br.js',

            './../../src/main/webapp/public/javascript/config/app.js',
            './../../src/main/webapp/public/javascript/config/constants.js',
            './../../src/main/webapp/public/javascript/config/host.js',
            './../../src/main/webapp/public/javascript/config/config.js',
            './../../src/main/webapp/public/javascript/config/run.js',

            './../../src/main/webapp/public/javascript/**/*.js',
            './../../src/main/webapp/components/**/*.js',
            './../../src/main/webapp/modules/**/*.js',

            './ui/unit/helps/*.js',
            './ui/unit/controle-de-acesso/rotas.js',
            './ui/unit/**/*spec.js'
        ],
        exclude: [],
        reporters: ['spec', 'coverage'],
        preprocessors: {
            './../../src/main/webapp/public/**/*.js': ['coverage'],
            './../../src/main/webapp/components/**/*.js': ['coverage'],
            './../../src/main/webapp/modules/**/*.js': ['coverage']
        },
        coverageReporter: {
          type : 'html',
          dir : 'ui/coverage/'
        },
        specReporter: {
            // maxLogLines: 5,             // limit number of lines logged per test
            suppressErrorSummary: false, // do not print error summary
            suppressFailed: false,      // do not print information about failed tests
            suppressPassed: false,      // do not print information about passed tests
            suppressSkipped: true,      // do not print information about skipped tests
            showSpecTiming: false,      // print the time elapsed for each spec
            failFast: true              // test would finish with error when a first fail occurs.
        },
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        browsers: ['PhantomJS'],
        concurrency: Infinity
    })
}
