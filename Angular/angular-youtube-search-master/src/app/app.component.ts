import { Component, OnInit } from '@angular/core';
import { YoutubeService } from '../services/youtube/youtube.service';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	providers: [
		YoutubeService
	]
})

export class AppComponent implements OnInit {
	public selected: any;
	public notFound: Object = false;
	public videos: Array<Object>;
	public isContentLoading: Object = false;
	public videoUrl: SafeResourceUrl;
	private oldVideo: Object = {};

	constructor(
		private youtube: YoutubeService,
		private sanitizer: DomSanitizer
	){
		this.selected = {
			'snippet': {
				'channelTitle': '',
				'description': '',
				'publishedAt': '',
				'title': ''
			}
		};
	}

	ngOnInit() {
		this.search('Imagine Dragons');
	}

	playVideo(video: Object, e){
		if (video['id']){
			const videoId = video['id']['videoId'];
			let URL = ('https://youtube.com/embed/' + videoId + '?enablejsapi=1');

			if (e) {
				e.preventDefault();

				if (this.oldVideo.hasOwnProperty('isPlay')) {
					this.oldVideo['isPlay'] = false;
				}

				video['isPlay'] = !video['isPlay'];
				URL = URL.concat('&autoplay=1');
			}

			const sameVideoId = (this.oldVideo.hasOwnProperty('id') && videoId !== this.oldVideo['id']['videoId']);

			if (sameVideoId || !this.oldVideo.hasOwnProperty('id')){
				this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(URL);
				this.oldVideo = video;
				this.selected = video;
			}

			setTimeout(() => {
				this.isContentLoading = false;
			}, 500);
		}else{
			this.isContentLoading = false;
			this.notFound = true;
		}
	}

	search(term){
		this.isContentLoading = true;
		this.notFound = false;

		this.youtube.search(term)
			.then((res) => {
				this.videos = res.items;
				this.playVideo((res.items.length > 0 ? res.items[0] : {}), null);
			});
	}
}
