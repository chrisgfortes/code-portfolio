import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import qs from 'qs';

@Injectable()
export class YoutubeService {
	constructor(private http: Http) {}

	private query = (term, qtd) => {
		const URL = {
			base: 'https://www.googleapis.com/youtube/v3/search?',
			query: {
				part: 'snippet',
				q: term,
				type: 'video',
				key: 'AIzaSyD-oDco5bSrsQUwmJcwuVgOFXyaFfo6AW4',
				maxResults: qtd
			}
		};

		const request = URL.base.concat(qs.stringify(URL.query));

		return request;
	}

	public search(term) {
		const request = this.query(term, 10);
		return this.http.get(request).toPromise().then((response) => response.json());
	}
}
