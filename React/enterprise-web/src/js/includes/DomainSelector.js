/**
 * Not sure this is used any longer
 */
import Cookie from '../utilities/util-cookies';
import ReservationCursors from '../cursors/ReservationCursors';

var DomainSelector = {
  cookieStr: 'defaultDomain',
  akamaiCookieStr: 'Akamai-Edgescape',
  init() {
    var defaultDomain,
      akamaiCookieInfo;

    defaultDomain = this.doesDefaultDomainCookieExist()

    if (!defaultDomain) {
      akamaiCookieInfo = this.doesAkamaiDomainCookieExist();
      if (akamaiCookieInfo) {
        defaultDomain = this.parseAkamaiCookie(akamaiCookieInfo);
      }
    }

    if (defaultDomain) {
      this.checkCurrentDomain(defaultDomain);
    }
  },
  doesDefaultDomainCookieExist() {
    return Cookie.get(this.cookieStr);
  },
  doesAkamaiDomainCookieExist() {
    return Cookie.get(this.akamaiCookieStr);
  },
  parseAkamaiCookie(cookie) {
    var countryCode,
      regionCode,
      match,
      domain = null;

    countryCode = this.parseCookieFor(cookie, 'country_code');
    regionCode = this.parseCookieFor(cookie, 'continent');

    domain = enterprise.domainRedirect.countryCodeToDomainMap[countryCode];

    if (!domain) {
      if (enterprise.domainRedirect.regionCodeToDomainMap) {
        domain = enterprise.domainRedirect.regionCodeToDomainMap[regionCode];
      }
    }

    return domain;

  },
  parseCookieFor(cookie, key) {
    var match = cookie.match(new RegExp(key + '=(.*?),'));
    if (match && match.length == 2) {
      return match[1];
    }
    return null;
  },
  checkCurrentDomain(defaultDomain) {
    if ((window.location.host).indexOf(defaultDomain) < 0) {
      ReservationStateTree.select(ReservationCursors.redirect).set('type', 'ip');
      ReservationStateTree.select(ReservationCursors.redirect).set('country', defaultDomain);
      ReservationStateTree.select(ReservationCursors.redirect).set('modal', true);
    }
  },
  setDefaultDomainCookie(domain) {
    Cookie.set(this.cookieStr, window.location.host)
  },
  removeDefaultDomainCookie() {
    Cookie.remove(this.cookieStr);
  }
};

module.exports = DomainSelector;
