import '../controllers/LocaleController';
import ReservationCursors from '../cursors/ReservationCursors';
import ReservationStateTree from '../stateTrees/ReservationStateTree';
import Settings from '../settings';
import Environment from '../env';
import Validator from '../mixins/Validator';
import RedirectController from '../controllers/RedirectController';
import { Debugger } from '../utilities/util-debug';
import Analytics from '../modules/Analytics';
import { MessageLogger } from '../controllers/MessageLogController';
import { Enterprise } from '../modules/Enterprise';
import { GLOBAL } from '../constants';

// @todo: temporary until we get a better application entry point
import Ecom from '../main';

// @todo: temp until better application entry point
Ecom.init({debug: __CONSOLE__});

// debugging only, see in "setup()" below
// @todo move this together with other flags
let CursorListeners;
if (__DEVELOPMENT__) {
  CursorListeners = require('../modules/CursorListeners');
}

// @todo: move these to global exposure tool in modules with the below
import Cookie from '../utilities/util-cookies';
import i18n from '../utilities/util-i18n';
import ZoomableImgs from '../modules/zoomableImgs';
import a11yClick from '../utilities/util-a11yClick';
import ResponsiveUtil from '../utilities/util-responsive';

// @todo: move these to global exposure tool in modules with the above
window.Enterprise = Enterprise;
window.Cookie = Cookie;
window.i18n = i18n;
window.a11yClick = a11yClick;
window.ResponsiveUtil = ResponsiveUtil;
window.initZoomableImgs = ZoomableImgs; // Implementation of ECR-10843

const Defaults = {
  setup () {
    Analytics.init();
    // Settings is an object instance so we must keep it as itself, since it has getters and setters
    enterprise.settings = Object.assign(Settings, enterprise.settings); // NOTE: this is an OBJECT INSTANCE MUST KEEP AS SELF

    let wal = window.addEventListener;
    if (wal) {
      wal('error', (e) => Debugger.use('enterprise.log').error('Listener heard an error: ', e) );
    }

    if (__DEVELOPMENT__) {
      CursorListeners.listeners(false); // set true to enable debugging
    }
    if (__CONSOLE__) {
      Debugger.use('enterprise.log').log('Boot Startup: 03: Console enabled.');
      // Debugger.use('enterprise.log').log('Running jQuery Version:', $.fn.jquery);
    }
    Debugger.use('enterprise.log').log('Boot Startup: 04: Development mode flag is:', __DEVELOPMENT__);

    enterprise.environment = Environment(enterprise);

    // temporary workaround for changed API name
    Modernizr.touch = Modernizr.touchevents;

    // RedirectController queues up browser and global gateway redirects
    RedirectController.initiate();
  },
  touchInit () {
    // https://github.com/facebook/react/pull/3442
    // https://github.com/facebook/react/issues/1169
    // removed in 0.14.x
    if (React.initializeTouchEvents) {
      React.initializeTouchEvents(true);
    } else {
      // Debugger.use('enterprise.log').log('no React.initializeTouchEvents()');
    }
  },
  corsInit () {
    $.support.cors = true;
  },
  utilities () {
    enterprise.log = (message) => {
      // @todo use derived, global settings instead
      // let enabled = window.location.href.indexOf('xqa') < 0;
      let enabled = false;

      if (enabled && typeof console !== 'undefined') {
        // @todo retire this
        Debugger.use('enterprise.log').warn(message);
      }
    };
    //THIS SHOULD BE MOVED INTO UTILITIES
    enterprise.utilities.capitalize = function (str) {
      return str.charAt(0).toUpperCase() + str.substr(1).toLowerCase();
    };
    enterprise.utilities.capitalizeAll = function (str, separator = ' ') {
      if (str) {
        let parts = str.split(separator);
        parts = parts.map(i => {
          return enterprise.utilities.capitalize(i);
        });
        return parts.join(separator);
      }
    };
    enterprise.utilities.isMaskedField = (val) => {
      const maskRegex = new RegExp('[' + GLOBAL.V1_MASK_CHAR + GLOBAL.V2_MASK_CHAR + ']');
      return maskRegex.test(val);
    }
  },
  extendJQuery () {
    jQuery.extend(jQuery.expr[':'], {
      focusable (el) {
        let $el = $(el);
        if ($el.is( ':hidden' ) || $el.is( ':disabled' ) || $el.css('visibility') === 'hidden') {
          return false;
        }

        let tabIndex = +$el.attr( 'tabindex' );
        tabIndex = isNaN( tabIndex ) ? -1 : tabIndex;
        return $el.is(':input, a[href], area[href], iframe') || tabIndex > -1;
      }
    });
  },
  prepay () {
    enterprise.cardSuccess = function () {
      Analytics.trigger('html', 'analytics-naprepay-response');
      const PaymentModelController = require('../controllers/PaymentModelController');

      if (ReservationStateTree.select(ReservationCursors.paymentProcessor).get() !== 'PANGUI') {
        ReservationStateTree.select(ReservationCursors.verification).set('registerCardSuccess', true);
      }

      if (arguments.length > 0 && typeof arguments[0] === 'object') {
        PaymentModelController.processResponse(arguments[0], enterprise.settings.pangui.responseKey);
        Analytics.trigger('html', 'analytics-naprepay-cardAdded-modalClosed');
      } else if (arguments.length === 3 && typeof arguments[0] === 'boolean') {
        let warningString = [{ defaultMessage: 'Failure to process payment method. Please close the modal and try again.' }];
        warningString.push({
          defaultMessage: arguments[1]
        });
        PaymentModelController.setComponentErrors(warningString);
      }
    };

    // @todo: Move all methods to a clear place when we refactor enterprise object
    enterprise.tools = {};
    enterprise.tools.spsFailure = function () {
      console.error('default.js: SPS Registration Failure. Logged.');
      let spsError = new MessageLogger();
      spsError.addMessage(enterprise.settings.frontEndMessageLogCodes.spsRegistrationFailed);
      const PaymentModelController = require('../controllers/PaymentModelController');
      PaymentModelController.setPrepayError(spsError, 'verification');
    };

    enterprise.threeDSSuccess = function (token) {
      console.log('threeDSSuccess: token?', token);
      if (token) {
        ReservationStateTree.select(ReservationCursors.threeDS).set('token', token);
        ReservationStateTree.select(ReservationCursors.verification).set('addCardSuccess', true);
      }
      ReservationStateTree.select(ReservationCursors.verification).set('cardModal', false);
    };
  },
  validatorHooks () {
    Validator.hooks.validateAll.push(function (valid, errors) {
      if (!valid) {
        Analytics.trigger('html', 'validationErrors', [{errors: errors}]);
      }
    });
  },
  sessionStart () {
    // ECR-9091 code specific to Canada
    if (enterprise.countryCode === 'CA') {
      $(function () {
        // has session expired or time limit expired
        const isDeeplinkUrl = new RegExp(GLOBAL.DEEPLINK_URL).test(window.location.href);
        if ((!Cookie.get('session-started') || !Cookie.get('adchoice-maxage')) && !isDeeplinkUrl) {
          $('body').addClass('show-adchoice');
          Cookie.set('session-started', true);
          Cookie.set('adchoice-maxage', true, {maxAge: (60 * 60 * 24)});
        }
      });
    }
  },
  highContrast () {
    Modernizr.addTest('highcontrast', function () {
      const el = document.createElement('i');
      let strColor;
      el.style.color = 'rgb(31, 41, 59)';
      document.documentElement.appendChild(el);
      strColor = document.defaultView ? document.defaultView.getComputedStyle(el, null).color : el.currentStyle.color;
      strColor = strColor.replace(/ /g, '');
      document.documentElement.removeChild(el);
      if (strColor !== 'rgb(31,41,59)') {
        if (window.getComputedStyle(document.body)['backgroundColor'] == 'rgb(0, 0, 0)') {
          document.body.className += ' high-contrast-black';
        }
        if (window.getComputedStyle(document.body)['backgroundColor'] == 'rgb(255, 255, 255)') {
          document.body.className += ' high-contrast-white';
        }
      }
      return strColor !== 'rgb(31,41,59)';
    });
  },
  start () {
    this.setup();
    this.touchInit();
    this.corsInit();
    this.extendJQuery();
    this.utilities();
    this.validatorHooks();
    this.prepay();
    this.sessionStart();
    this.highContrast();
  }
};

// export default Defaults.start();
export default Defaults;
