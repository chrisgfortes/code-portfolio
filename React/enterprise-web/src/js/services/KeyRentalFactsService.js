import EnterpriseServices from '../services/EnterpriseServices';
import KeyRentalFactsActions from '../actions/KeyRentalFactsActions';
import ReservationCursors from '../cursors/ReservationCursors';
import ReservationActions from '../actions/ReservationActions';
import { SERVICE_ENDPOINTS } from '../constants';

const KeyRentalFactsService = {
  //Not currently used. Current implementation in AccountService to consolidate AJAX calls.
  getKeyFactsDisputesContactInfo: function () {
    let pickupCountryCode = ReservationStateTree.select(ReservationCursors.pickupCountryCode).get();
    let countries = ReservationStateTree.select(ReservationCursors.countries).get();

    if (countries && countries.length) {
      KeyRentalFactsActions.setDisputesContactInfo(countries, pickupCountryCode);
    } else {
      return EnterpriseServices.GET(SERVICE_ENDPOINTS.LOCATION_COUNTRIES,
        {
          callback: (response) => {
            if (response.error) {
              enterprise.log(response.error);
            } else {
              KeyRentalFactsActions.setDisputesContactInfo(response.countries, pickupCountryCode);
            }
          }
        });
    }
  },
  getTermsAndConditions: function () {
    return EnterpriseServices.GET(SERVICE_ENDPOINTS.TERMS_AND_CONDITIONS, {
      callback: function (response) {
        if (response.rental_terms_and_conditions) {
          ReservationActions.setRentalTerms(response.rental_terms_and_conditions);
        }
      }
    });
  }
}

module.exports = KeyRentalFactsService;
