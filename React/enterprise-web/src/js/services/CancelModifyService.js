import ReservationActions from '../actions/ReservationActions';

import EnterpriseServices from '../services/EnterpriseServices';

import ServiceController from '../controllers/ServiceController';
import ServiceFactory from '../factories/ServiceFactory';

import { debug } from '../utilities/util-debug';
import { SERVICE_ENDPOINTS } from '../constants';

const defaultServiceParams = {
  onFailure: (err) => {
    // @todo: figur out which component
    // ErrorActions.setErrorsForComponent(messages, '');
    ServiceController.serviceOnFailure('CancelModifyService .onFailure()', err);
    return Promise.reject(err);
  }
};

const CancelModifyService = debug({
  isDebug: false,
  logger: {},
  name: 'CancelModifyService.js',

  /**
   * @function getAvailableFields
   * @memberOf CancelModifyService
   * @return {Promise} Promise wrapped ajax
   */
  getAvailableFields (...args) {
    this.logger.warn('getAvailableFields()');
    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: () => EnterpriseServices.GET(SERVICE_ENDPOINTS.RESERVATION_SHOWFIELDS)
    })(...args)
  },

  cancelReservation (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: (confirmationNumber, isRetrieveAndCancel) => {
        return EnterpriseServices.POST(isRetrieveAndCancel ? SERVICE_ENDPOINTS.RESERVATION_RETRIEVE_CANCEL
            : SERVICE_ENDPOINTS.RESERVATION_CANCEL,
            {data: JSON.stringify({confirmation_number: confirmationNumber})})
      }
    })(...args)
  },

  getVehicleDetails: ServiceFactory.createService({
    ...defaultServiceParams,
    request: (vehicle) => EnterpriseServices.GET(
      SERVICE_ENDPOINTS.RESERVATION_VEHICLE_DETAILS,
      { data: {'carClassCode': vehicle.code} }
    )
  }),

  initiateModify (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: () => EnterpriseServices.GET(SERVICE_ENDPOINTS.RESERVATION_MODIFY_INIT)
    })(...args)
  },
  abandonModify (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      onFailure: () => {
        ReservationActions.setLoadingClassName(null);
      },
      request: () => EnterpriseServices.GET(SERVICE_ENDPOINTS.RESERVATION_MODIFY_ABANDON)
    })(...args)
  },
  getReservationEligibility (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: () => EnterpriseServices.GET(SERVICE_ENDPOINTS.RESERVATION_AFTERCOMMIT_DETAILS)
    })(...args);
  }
});

module.exports = CancelModifyService;
