import ServiceFactory from '../factories/ServiceFactory';

import EnterpriseServices from '../services/EnterpriseServices';

import { exists } from '../utilities/util-predicates';
import { SERVICE_ENDPOINTS } from '../constants';

const CorporateService = {
  /**
   * Pull contract details from endpoint.
   *
   * @param      {Array}   args    The arguments
   * @return     {Promise}  Promise wrapped ajax
   */
  contractDetails (...args) {
    return ServiceFactory.createService({
      request: (contract) => EnterpriseServices.GET(SERVICE_ENDPOINTS.CONTRACT_DETAILS(contract)),
      onSuccess: (response) => response.contract_details,
      validate: (response, responseStatus) => {
        if (!exists(response.contract_details)) {
          throw new Error('No Contract Details found! (ERROR PLACEHOLDER)');
        }
        ServiceFactory.validate(response, responseStatus);
      }
    })(...args)
  }
};

module.exports = CorporateService;
