import EnterpriseServices from '../services/EnterpriseServices';
import ErrorActions from '../actions/ErrorActions';
import ServiceFactory from '../factories/ServiceFactory';
import { SERVICE_ENDPOINTS } from '../constants';

const defaultServiceParams = {
  onSuccess: (response) => {
    ErrorActions.clearErrorsForComponent('verification');
    return response;
  },
  onFailure: (messages) => {
    console.error('ExpeditedService::onFailure()', messages);
    ErrorActions.setErrorsForComponent(messages, 'verification');
    return Promise.reject(messages);
  }
};

const ExpeditedService = {
  searchProfile: ServiceFactory.createService({
    ...defaultServiceParams,
    request: (data) => EnterpriseServices.POST(
      SERVICE_ENDPOINTS.RESERVATION_EXPEDITED,
      { data: JSON.stringify(data) }
    )
  }),
  enroll: ServiceFactory.createService({
    ...defaultServiceParams,
    validate: ServiceFactory.baseValidate,
    request: (data) => EnterpriseServices.POST(
      SERVICE_ENDPOINTS.PROFILE_CREATE,
      { data: JSON.stringify(data) }
    )
  })
}

module.exports = ExpeditedService;
