/**
 * @module
 * THIS SHOULD BE HTTP TRANSPORT LAYER ONLY
 * @Todo: National moved from jQuery to Axios ...
 */
import RedirectActions from '../actions/RedirectActions';
import CorporateActions from '../actions/CorporateActions';

import AppController from '../controllers/AppController';

import ReservationCursors from '../cursors/ReservationCursors';

import ReservationStateTree from '../stateTrees/ReservationStateTree';

import TimeStamp from '../modules/TimeStamp';

import urlUtils from '../utilities/util-url';
import Cookie from '../utilities/util-cookies';
import { debug } from '../utilities/util-debug';

/**
 * ECR-13850 this will only fire with TRANSPORT LEVEL ISSUES
 * @todo: This is temporary for debugging errors
 */
if (__DEVELOPMENT__) {
  $(document).ajaxError((event, jqxhr, settings, thrownError) => {
    if (thrownError === 'abort') {
      return true;
    } else {
      console.error('jQuery ajaxError(event, jqxhr, settings, thrownError)', event, jqxhr, settings, thrownError);
    }
  })
}

const EnterpriseServices = debug({
  isDebug: false,
  logger: {},
  name: 'EnterpriseServices',
  // ErrorMiddleware: null, // @todo: @GBOv2: not sure what this is
  cacheQueryString () {
    ReservationStateTree.select(ReservationCursors.queryStringData).set(urlUtils.getParameters());
  },
  GET (url, options = {}) {
    let prefix = enterprise.services.path;
    let data = {};
    let credentials = null;
    let params;
    let cacheBust = '';

    if ('prefix' in options) {
      prefix = options.prefix;
    }

    credentials = (prefix !== enterprise.solr.path);

    if ('data' in options) {
      data = options.data;
    }

    if (!data.locale && _.isObject(data)) {
      data.locale = enterprise.locale;
    }

    if (!('disableCacheBust' in options)) {
      cacheBust = `&now=${Date.now()}`;
    }

    params = $.param(data);

    let XDRParams = `${prefix}${url}?${params}${cacheBust}`;
    let ajaxParams = `${prefix}${url}?${cacheBust}`;

    this.logger.log('issuing GET request', ajaxParams, data, options);

    if (Modernizr.cors) {
      return $.ajax({
        url: ajaxParams,
        type: 'GET',
        data: data,
        xhrFields: {
          withCredentials: credentials
        },
        done: (response) => {
          enterprise.log('AJAX GET ERROR', response);
          if (response && response.messages && options.errorCallback) {
            options.errorCallback(response);
          }
        },
        success: (response, textStatus, request) => {
          EnterpriseServices.logger.warn('0001 GET generic success', request, response);
          if (AppController.dataIsRedirect(response)) {
          // if (response && response.type == 'HREF') {
            if (response.look_and_feel && response.code === '#landingPage') {
              // @todo this is should prob not be here
              if (enterprise.lookAndFeelForwardMap) {
                RedirectActions.go(enterprise.lookAndFeelForwardMap[response.look_and_feel]);
              }
            } else if (!options.ignoreStep) {
              EnterpriseServices.logger.log('goToReservationStep()', response.code);
              RedirectActions.goToReservationStep(response.code);
            }
          }
          if (response && response.messages && options.errorCallback) {
            options.errorCallback(response);
          }
          if (options.callback) {
            EnterpriseServices.logger.warn('0002 firing options.callback');
            options.callback(response, textStatus, request);
          }
        }
      });
    } else if (window.XDomainRequest) {
      return this.XDR(XDRParams, 'get', function (responseText) {
        const responseJSON = JSON.parse(responseText);

        if (responseJSON.type == 'HREF') {

          // need to clear cookie
          if (responseJSON.code == '#timedout') {
            Cookie.remove('redis');
          }

          RedirectActions.goToReservationStep(responseJSON.code);
        }
        if (options.callback) {
          options.callback(responseJSON);
        }
      });
    } else {
      console.error('CORS Unsupported!');
    }
  },
  POST (url, options = {}) {
    TimeStamp.mark('EnterpriseServices:POST');
    const session = ReservationStateTree.select(ReservationCursors.reservationSession).get();
    let sessionId = session.sessionId;
    // In case we hadn't requested a currentSession yet
    if (Cookie.get('redis')) {
      sessionId = Cookie.get('redis');
    }

    let prefix = enterprise.services.path;
    let data = {};
    let queryObj = {};
    let queryString = `?${$.param(queryObj)}&${Date.now()}`;
    let credentials = null;

    if (options.hasOwnProperty('disableCacheBust')) {
      queryString = `?${$.param(queryObj)}`;
    }

    if (options.hasOwnProperty('prefix')) {
      prefix = options.prefix;
    }

    if (options.hasOwnProperty('data')) {
      data = options.data;
    }

    queryObj.locale = enterprise.locale;

    credentials = (prefix === enterprise.services.path);

    let postUrl = prefix + url + queryString;

    this.logger.log('issuing POST request: url:', postUrl, 'data:', data);

    if (Modernizr.cors) {
      return $.ajax({
        crossDomain: true,
        url: postUrl,
        contentType: "application/json; charset=UTF-8",
        type: "POST",
        headers: {
          'redis': sessionId
        },
        data: data,
        xhrFields: {
          withCredentials: credentials
        },
        error: (response) => {
          enterprise.log('AJAX POST ERROR', response);
          if (response.messages && options.errorCallback) {
            options.errorCallback(response);
          }
        },
        success: (response, textStatus, request) => {
          TimeStamp.stop('EnterpriseServices:POST');
          // console.log('POST', request, response);
          if (response && response.type == 'HREF') {
            RedirectActions.goToReservationStep(response.code);
          }
          if (response && response.messages) {
            response.messages.forEach(message => {
              if (message.code === 'CROS_LOGIN_SYSTEM_ERROR') {
                // Remember what we were trying to do and failed, so we can try again after authentication.
                // @todo - Refactory this part, because this do that the old url be called, without passed for methods,
                // doing that sometimes, the code doesn't work
                ReservationStateTree.select(ReservationCursors.callbackAfterAuthenticate).set(() => {
                  this.POST(url, options);
                });
                CorporateActions.showModalInState('authenticate');
              }
            });

            if (options.errorCallback) {
              options.errorCallback(response);
            }
          }
          if (options.callback) {
            options.callback(response, textStatus, request);
          }
        }
      });
    } else if (window.XDomainRequest) {
      return this.XDR(prefix + url + queryString, 'post', function (responseText) {
        let responseJSON = JSON.parse(responseText);
        window.XDRDebug = responseText;
        if (responseJSON.type == 'HREF') {
          RedirectActions.goToReservationStep(responseJSON.code);
        }
        if (options.callback) {
          options.callback(responseJSON);
        }
      }, data);
    } else {
      console.error('CORS Unsupported!');
    }
  },
  HEAD (url, options) {
    TimeStamp.mark('EnterpriseServices:HEAD');

    this.logger.log('sending HEAD request', url);

    if (Modernizr.cors) {
      return $.ajax({
        url: url,
        type: "HEAD",
        error: (response) => {
          enterprise.log('AJAX POST ERROR', response);
          if (response.messages && options.errorCallback) {
            options.errorCallback(response);
          }
        },
        success: (response, textStatus, request) => {
          TimeStamp.stop('EnterpriseServices:HEAD');
          if (options.callback) {
            options.callback(response, textStatus, request);
          }
        }
      });
    } else if (window.XDomainRequest) {
      return this.XDR(url, 'get', function (responseText) {
        window.XDRDebug = responseText;
        if (options.callback) {
          options.callback(responseText);
        }
      });
    } else {
      console.error('CORS Unsupported!');
    }
  },
  XDR: function (request, type, callback, data) {
    let xdr = new XDomainRequest(),
      requestString = request;

    const session = ReservationStateTree.select(ReservationCursors.reservationSession).get();
    let sessionId = session.sessionId;
    // In case we hadn't requested a currentSession yet
    if (Cookie.get('redis')) {
      sessionId = Cookie.get('redis');
    }

    if (requestString.indexOf('?') === -1) {
      requestString += '?';
    } else {
      requestString += '&';
    }

    requestString += 'redis=' + sessionId;

    let deferred = $.Deferred();
    $.extend(xdr, deferred.promise());

    xdr.onload = function () {
      callback(xdr.responseText);
      deferred.resolve();
    };

    //these blank handlers need to be set to fix ie9 http://cypressnorth.com/programming/internet-explorer-aborting-ajax-requests-fixed/
    xdr.onprogress = function () {
    };
    xdr.ontimeout = function () {
    };

    xdr.open(type, requestString);

    //do it, wrapped in timeout to fix ie9
    setTimeout(function () {

      if (type == 'post' && !$.isEmptyObject(data)) {
        xdr.send(data);
      } else {
        xdr.send();
      }

    }, 0);

    //xdr.send();
    return xdr;
  }
});

export default EnterpriseServices;
