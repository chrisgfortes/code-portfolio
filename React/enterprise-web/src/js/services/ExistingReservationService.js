import EnterpriseServices from '../services/EnterpriseServices';
import ServiceFactory from "../factories/ServiceFactory";
import ErrorActions from '../actions/ErrorActions';
import { SERVICE_ENDPOINTS } from '../constants';

const defaultServiceParams = {
  onFailure: messages => {
    ErrorActions.setErrorsForComponent(messages, "existingReservations");
    console.error(messages)
    return Promise.reject(messages);
  }
};

const ExistingReservationService = {
  getReservation: ServiceFactory.createService({
    ...defaultServiceParams,
    request: data =>
      EnterpriseServices.POST(SERVICE_ENDPOINTS.RESERVATION_RETRIEVE, {
        data: JSON.stringify(data)
      })
  }),
  reservationDetails: ServiceFactory.createService({
    ...defaultServiceParams,
    request: (confirmation, data) =>
      EnterpriseServices.GET(
        SERVICE_ENDPOINTS.RESERVATION_DETAILS_PREFIX +
          confirmation +
          SERVICE_ENDPOINTS.RESERVATION_DETAILS_SUFFIX,
        {
          data: data
        }
      )
  }),
  getMyTrips: ServiceFactory.createService({
    ...defaultServiceParams,
    request: (type, query) =>
      EnterpriseServices.GET(
        SERVICE_ENDPOINTS.PROFILE_MYTRIPS.concat(type || ""),
        {
          data: (query || '')
        }
      )
  })
};

export default ExistingReservationService;
