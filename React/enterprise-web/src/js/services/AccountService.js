import EnterpriseServices from '../services/EnterpriseServices';
import SessionController from '../controllers/SessionController';

import { SERVICE_ENDPOINTS } from '../constants';

import ErrorActions from '../actions/ErrorActions';
import ServiceFactory from '../factories/ServiceFactory';

/**
 * @memberOf AccountService
 * Used for ServiceFactory as onFailure error callback.
 * @type {Object}
 */
const defaultServiceParams = {
  onFailure: (err, component) => {
    // @todo: add setErrorsForComponent if necessary
    ErrorActions.setErrorsForComponent(err, component);
    // @todo: add this back in when merged with robs branch
    //ServiceController.serviceOnFailure('AccountService .onFailure()', err);
    console.warn('AccountService.onFailure():', err);
    return Promise.reject(err);
  }
};

const AccountService = {
  login (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      validate: ServiceFactory.baseValidate,
      request: (brand, params, data) => EnterpriseServices.POST(SERVICE_ENDPOINTS.LOGIN(brand),
      { data: JSON.stringify(data) }
      )
    })(...args)
  },
  logout (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      validate: ServiceFactory.baseValidate,
      request: () => EnterpriseServices.POST(SERVICE_ENDPOINTS.LOGOUT)
    })(...args)
  },
  updateProfile: {
    contact (...args) {
      return ServiceFactory.createService({
        ...defaultServiceParams,
        onFailure: (messages) => {
          return defaultServiceParams.onFailure(messages, 'accountContact');
        },
        request: (data) => EnterpriseServices.POST(SERVICE_ENDPOINTS.PROFILE_UPDATE,
          { data: JSON.stringify( data ) }
        )
      })(...args)
    },
    license (...args) {
      return ServiceFactory.createService({
        ...defaultServiceParams,
        onFailure: (messages) => {
          return defaultServiceParams.onFailure(messages, 'accountDriver');
        },
        request: (data) => EnterpriseServices.POST(SERVICE_ENDPOINTS.PROFILE_UPDATE,
          { data: JSON.stringify( data ) }
        )
      })(...args)
    },
    password (...args) {
      return ServiceFactory.createService({
        ...defaultServiceParams,
        onFailure: (messages) => {
          return messages;
        },
        request: (params) => EnterpriseServices.POST(SERVICE_ENDPOINTS.PASSWORD_UPDATE,
          { data: JSON.stringify({new_password: params.password, confirm_new_password: params.passwordConfirm }) }
        )
      })(...args)
    }
  },
  getCountries (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: () => EnterpriseServices.GET(SERVICE_ENDPOINTS.LOCATION_COUNTRIES)
    })(...args)
  },
  getSubdivisions (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: (countryCode = 'US') => EnterpriseServices.GET(SERVICE_ENDPOINTS.LOCATION_STATESPROV_PREFIX + countryCode + SERVICE_ENDPOINTS.LOCATION_STATESPROV_SUFFIX)
    })(...args)
  },
  getIssueCountrySubdivisions (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: (countryCode = 'US') => EnterpriseServices.GET(SERVICE_ENDPOINTS.LOCATION_STATESPROV_PREFIX + countryCode + SERVICE_ENDPOINTS.LOCATION_STATESPROV_SUFFIX)
    })(...args)
  },
  createProfile (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      onFailure: (messages) => {
        return defaultServiceParams.onFailure(messages, 'enroll');
      },
      request: (data) => EnterpriseServices.POST(SERVICE_ENDPOINTS.PROFILE_CREATE,
        { data: JSON.stringify( data ) }
      )
    })(...args)
  },
  //create password needs testing on activate.html $ error right now
  createLogin (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      onFailure: (messages) => {
        return defaultServiceParams.onFailure(messages, 'createPassword');
      },
      request: (data, createPassword) => EnterpriseServices.POST(SERVICE_ENDPOINTS.PROFILE_ACTIVATE(createPassword.profileId),
        { data: JSON.stringify( data ) }
      )
    })(...args)
  },
  searchProfile(...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      onFailure: (messages) => {
        return defaultServiceParams.onFailure(messages, 'createPassword');
      },
      request: (data) => EnterpriseServices.POST(SERVICE_ENDPOINTS.PROFILE_MEMBER_SEARCH,
        { data: JSON.stringify( data ) }
      )
    })(...args)
  },
  getTerms (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: () => EnterpriseServices.GET(SERVICE_ENDPOINTS.CONTENT_TERMS_EPLUS)
    })(...args)
  },
  removePayment(...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: (reference) => EnterpriseServices.GET(SERVICE_ENDPOINTS.PAYMENT_DELETE + reference)
    })(...args)
  },
  getSession (callback) {
    return SessionController.callGetSession(true, false, true, callback);
  }
  /*Dont think this is being used
   forgot (recover, callback) {
   let data = {
   'username': recover
   };
   return EnterpriseServices.POST(SERVICE_ENDPOINTS.PASSWORD_RESET,
   {
   data: JSON.stringify(data),
   callback: (response) => {
   ErrorActions.clearErrorsForComponent('loginForgot');
   if (response === true) {
   //console.log('success!');
   } else {
   ErrorActions.setErrorsForComponent(response, 'loginForgot');
   enterprise.log(response.error);
   }
   callback(response);
   }
   });
   },*/
};

module.exports = AccountService;
