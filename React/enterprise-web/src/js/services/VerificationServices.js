import EnterpriseServices from '../services/EnterpriseServices';
import ErrorActions from '../actions/ErrorActions';
import ServiceFactory from '../factories/ServiceFactory';
import { SERVICE_ENDPOINTS } from '../constants';

import { debug } from '../utilities/util-debug';
const logger = debug({isDebug: true, name: 'VerificationServices'}).logger;

const defaultServiceParams = {
  onFailure: (messages) => {
    ErrorActions.setErrorsForComponent(messages, 'verification');
    console.error('VerificationServices::onFailure()', messages);
    return Promise.reject(messages);
  }
};

const VerificationService = {
  /**
   * @function linkAccount
   * @memberOf VerificationService
   * @return {Object} Promise - ajax promise
   */
  linkAccount (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      validate: ServiceFactory.baseValidate, // special treatment for messages
      request: (data) => EnterpriseServices.POST(
        SERVICE_ENDPOINTS.RESERVATION_ASSOCIATE,
        { data: JSON.stringify(data) }
      )
    })(...args)
  },

  /**
   * @function submitSave
   * @memberOf VerificationServices
   * Used for RE-USING Session Reservation Data (fedex, etc)
   * @param {object} data - data to be posted
   * @return {Promise}    - ajax promise
   * @todo  the response to this service call is "OK" ... really ...
   */
  submitSave (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      validate: ServiceFactory.baseValidate,
      request: (data) => EnterpriseServices.POST(SERVICE_ENDPOINTS.RESERVATION_COMMIT_SAVE,
        { data: JSON.stringify(data) }
      )
    })(...args);
  },

  /**
   * @function submit
   * @memberOf VerificationServices
   * @param  {...Array} args - arguments forwarded to anonymous service
   * @return {Promise}   promise wrapped ajax
   */
  submit (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      validate: ServiceFactory.baseValidate, // special treatment for messages
      request: (data) => EnterpriseServices.POST(SERVICE_ENDPOINTS.RESERVATION_COMMIT,
        { data: JSON.stringify(data) }
      )
    })(...args);
  },

  /**
   * @function commitWithClearedDNC
   * @memberOf VerificationService
   * @param  {...array} args arguments to foward to ServiceFactory
   * @return {Promise}  promise wrapped ajax
   */
  commitWithClearedDNC (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: (data) => EnterpriseServices.POST(SERVICE_ENDPOINTS.RESERVATION_COMMIT,
        { data: JSON.stringify(data) }
      )
    })(...args)
  },

  /**
   * @function fetchPrepayPolicy
   * @return {Promise}   - Ajax promise
   */
  fetchPrepayPolicy (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: () => EnterpriseServices.GET(SERVICE_ENDPOINTS.CONTENT_TERMS_PREPAY)
    })(...args)
  },

  /**
   * @function fetchDisclaimer
   * @memberOf VerificationServices
   * @return {Promise} - Ajax promise
   */
  fetchDisclaimer (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: () => EnterpriseServices.GET(SERVICE_ENDPOINTS.CONTENT_PRIVACY)
    })(...args);
  },

  /**
   * @function fetchTaxesFeesDisclaimer
   * @memberOf VerificationServices
   * @return {Promise}    ajax wrapped promise
   */
  fetchTaxesFeesDisclaimer (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: () => EnterpriseServices.GET(SERVICE_ENDPOINTS.CONTENT_TAXES)
    })(...args);
  }
};

module.exports = VerificationService;
