import EnterpriseServices from '../services/EnterpriseServices';
import Settings from '../settings';
import { SERVICE_ENDPOINTS } from '../constants';

const EmailService = {
  getSetting () {
    // @todo change this
    return _.get(Settings, '_collection[0]str.defaultEmailSubscription', '');
  },
  submit (params, callback) {
    let data = {
      first_name: params.firstName,
      last_name: params.lastName,
      email: params.email,
      email_source_code: (params.source && params.source !== '') ? params.source : this.getSetting(),
      country_of_residence: params.country,
      opt_in: params.opt_in ? params.opt_in : 'true'
    };

    if (params.birthDate) {
      data.date_of_birth = params.birthDate;
    }

    if (params.location) {
      data.location_preference = params.location;
    }

    if (params.postal) {
      data.zip_code = params.postal;
    }

    return EnterpriseServices.POST(SERVICE_ENDPOINTS.OPTIONAL_EXTRAS,
      {
        data: JSON.stringify(data),
        callback (response) {
          callback(response);
        }
      });
  }
};

module.exports = EmailService;
