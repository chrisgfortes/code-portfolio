/**
 * @module LocationInputService
 */
import { SERVICE_ENDPOINTS } from '../constants';
import ServiceFactory from '../factories/ServiceFactory';
import ServiceController from '../controllers/ServiceController';
import { Enterprise } from '../modules/Enterprise';
import EnterpriseServices from '../services/EnterpriseServices';

let enterprise = Enterprise.global;

const defaultServiceParams = {
  onFailure: (err) => {
    // @todo: figur out which component
    // ErrorActions.setErrorsForComponent(messages, '');
    ServiceController.serviceOnFailure('LocationInputService::onFailure()', err);
    return Promise.reject(err);
  }
};

const LocationInputService = {
  blockLocationsRequest: false,
  getLocationsRequest: null,

  /**
   * Same set up for both
   * @memberOf LocationInputService
   * @returns {boolean}
   */
  setLocationSearchSetup () {
    if (LocationInputService.blockLocationsRequest) { // actually not sure this is used ... (?)
      LocationInputService.blockLocationsRequest = false;
      return false;
    }

    if (LocationInputService.getLocationsRequest) {
      LocationInputService.getLocationsRequest.abort();
    }
  },

  /**
   * Fetch FEDEX locations
   * Called by ReservationFlowModelController (and others?)
   * @memberOf LocationInputService
   * @param query
   * @param type (deprecated param no longer used)
   * @param fedexType
   * @returns {boolean}
   */
  fetchFedexLocations (...args) {
    LocationInputService.setLocationSearchSetup();
    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: (query, type, fedexType) => {
        let endpointName;
        let ajaxData = {locale: enterprise.locale};
        if (fedexType === 'byCity' ) {
          endpointName = 'fedexPostalByCity';
          ajaxData.city = query;
        } else {
          endpointName = 'fedexPostalByAlphaId';
          ajaxData.alphaId = query;
        }
        return LocationInputService.getLocationsRequest = EnterpriseServices.GET(SERVICE_ENDPOINTS.LOCATION_SEARCH_FEDEX(endpointName), {
          prefix: enterprise.solr.path,
          disableCacheBust: true,
          data: ajaxData
        })
      }
    })(...args)
  },

  /**
   * Fetch Locations from SOLR.
   * Called by ReservationFlowModelController (and others?)
   * @memberOf LocationInputService
   * @param {string} query goes into url
   * @returns {Promise}
   */
  fetchLocations (...args) {
    LocationInputService.setLocationSearchSetup();
    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: (query) => {
        return LocationInputService.getLocationsRequest = EnterpriseServices.GET(SERVICE_ENDPOINTS.LOCATION_SEARCH + query, {
          prefix: enterprise.solr.path,
          disableCacheBust: true,
          data: {
            countryCode: enterprise.countryCode,
            brand: 'ENTERPRISE',
            locale: enterprise.locale
          }
        });

      }
    })(...args)
  }
};

module.exports = LocationInputService;

