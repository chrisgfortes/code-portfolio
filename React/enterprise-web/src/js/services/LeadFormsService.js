/**
 * @todo: This may no longer be used ... ?
 */
import EnterpriseServices from '../services/EnterpriseServices';
import { SERVICE_ENDPOINTS } from '../constants';

const LeadFormsService = {
  submit (params, callback) {
    let data = {
      contact_info: {
        contact_name: params.contactName,
        company_name: params.companyName,
        contact_phone_number: params.phone,
        contact_email_address: params.email
      }
    };

    let endPoint = null;

    if (params.country) {data.contact_info.country = params.country;}
    if (params.address) {data.contact_info.company_address = params.address;}
    if (params.addressTwo) {data.contact_info.company_address2 = params.addressTwo;}
    if (params.city) {data.contact_info.town_or_city = params.city;}
    if (params.subdivision) {data.contact_info.county = params.subdivision;}
    if (params.postal) {data.contact_info.postal = params.postal;}

    if (params.currentView === 'entertainmentRental') {
      endPoint = SERVICE_ENDPOINTS.LEADFORMS_ENTERTAINMENT;
    } else if (params.currentView === 'meetingAndGroupsRental') {
      endPoint = SERVICE_ENDPOINTS.LEADFORMS_MEETING;

      if (params.eventLocation) {data.location = params.eventLocation;}
      if (params.eventSubdivision) {data.location_state_or_province = params.eventSubdivision;}
      if (params.eventCenterName) {data.name_of_convention = params.eventCenterName;}
      if (params.eventLength) {data.length = params.eventLength;}
      if (params.durationUnit) {data.duration_units = params.durationUnit;}
      if (params.attendees) {data.number_of_attendees = params.attendees;}
    }

    return EnterpriseServices.POST(endPoint,
      {
        data: JSON.stringify(data),
        callback (response) {
          callback(response);
        }
      });
  }
};

module.exports = LeadFormsService;

