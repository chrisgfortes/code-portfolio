import EnterpriseServices from '../services/EnterpriseServices';
import ServiceFactory from '../factories/ServiceFactory';
import ErrorActions from '../actions/ErrorActions';
import { SERVICE_ENDPOINTS } from '../constants';

const defaultServiceParams = {
  onFailure: (messages) => {
    ErrorActions.setErrorsForComponent(messages, 'existingReservations');
    console.error('ReceiptService::onFailure()', messages);
    return Promise.reject(messages);
  }
};

const ReceiptService = {
  getReceipt: ServiceFactory.createService({
    ...defaultServiceParams,
    request: (invoiceNumber) => EnterpriseServices.GET(
      SERVICE_ENDPOINTS.PROFILE_TRIPS_RECEIPTS,
      { data: {invoiceNumber: invoiceNumber} }
    )
  })
}

export default ReceiptService;
