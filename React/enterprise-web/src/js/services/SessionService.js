/* globals ReservationStateTree */
import EnterpriseServices from '../services/EnterpriseServices';
import ServiceFactory from '../factories/ServiceFactory';

import Cookie from '../utilities/util-cookies';
import SessionTimeoutController from '../controllers/SessionTimeoutController';
import TimeStamp from '../modules/TimeStamp';
import { SERVICE_ENDPOINTS} from '../constants';
import { debug } from '../utilities/util-debug';
const logger = debug({isDebug: false, name: 'SessionService'}).logger;

const defaultServiceParams = {
  onFailure: (messages) => {
    logger.error('onFailure(messages)', messages);
    return Promise.reject(messages);
  }
}

const SessionService = {
  inResFlow: false, // I think this is only needed in the SessionController ?

  /**
   * @memberof   SessionService
   * Used to fetch the current session, the "heartbeat" of the res flow.
   * @function fetchSession
   * @param      {boolean} setTree boolean to update the tree or not
   * @param      {boolean} hash do we use the location#hash ?
   * @param      {boolean} getDetailLocationData do we get location data afterwards?
   * @param      {function} callback do we run a callback afterwards? DEPRECATED
   * So many of these arguments are no used at this layer. Consider removing since most
   * are only valid for SessionController.callGetSession()
   */
  fetchSession(setTree = true, hash = false, getDetailLocationData = true, callback = () => {}) {
    logger.info('fetchSession()');
    TimeStamp.mark('getSession()');
    let data = {};
    if (hash) {
      data.route = hash;
    } else if (enterprise.currentView === 'landingPage') {
      // Special case where we're not really inside the reservation flow, but backend should think
      //   we are when a deeplink falls in it.
      data.route = 'book';
    }
    if (this.inResFlow) {
      data.reservationFlow = true;
    }

    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: (data) => EnterpriseServices.GET(SERVICE_ENDPOINTS.SESSION_CURRENT, {
        data: data
      }),
      onSuccess: (response) => {
        logger.warn('001: fetchSession().onSuccess() call back with response data.');

        if (response && response.reservationSession) {

          logger.warn('002: fetchSession.onSuccess() reservationSession:', response);

          if (callback) {
            callback();
          }

          // Initialize FE session control. Since the final countdown modal is defaulted to count
          //  to 5 minutes, we reduce it from the total session timeout.
          SessionTimeoutController.init(response.sessionTimeout);

          //IE 9 fix
          if (window.XDomainRequest) {
            if (!Cookie.get('redis')) {
              Cookie.set('redis', response.reservationSession.sessionId);
            }
          }
        }
        TimeStamp.stop('getSession()');
        return response;
      }
    })(data)
  },

  /**
   * @memberOf SessionService
   * @function
   * @param callback
   * @returns {object} XHR Promise
   */
  fetchUpdated (cb) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: () => EnterpriseServices.GET(SERVICE_ENDPOINTS.SESSION_UPDATE),
      onSuccess: (response) => {
        if (cb) cb(response);
        return response;
      }
    })(cb)
  },

  /**
   * Clears the renter's session.
   * @memberOf SessionService
   * @param {object} options
   * @returns {object} XHR Promise
   *
   * The `options` object may contain the following keys:
   *   {boolean}  removeCID                 - default value: false
   *   {boolean}  removeDatetimeAndLocation - default value: false
   *   {boolean}  removeUnauthParams        - default value: true
   *   {boolean}  keepCommitRequest         - default value: false
   *   {[string]} additionalInfoToKeep      - default value: []
   */
  clearSession (options = {}) {
    return EnterpriseServices.POST(SERVICE_ENDPOINTS.SESSION_CLEAR,
      {
        data: JSON.stringify(options),
        callback: (response, textStatus, request) => {
          if (response.error) {
            console.log(response.error);
          }
        }
      });
  }

};

export default SessionService;

