import LocationSearchActions from '../actions/LocationSearchActions';
import ErrorActions from '../actions/ErrorActions';
import ReservationActions from '../actions/ReservationActions';
import EnterpriseServices from '../services/EnterpriseServices';
import ServiceController from '../controllers/ServiceController';
import ServiceFactory from '../factories/ServiceFactory';
import { SERVICE_ENDPOINTS, PAGEFLOW } from '../constants';
import { debug } from '../utilities/util-debug';

const logger = debug({ isDebug: true, name: 'BookingWidgetService' }).logger;

/**
 * @memberOf BookingWidgetService
 * Used for ServiceFactory as onFailure error callback.
 * @type {Object}
 */
const defaultServiceParams = {
  onFailure: (err) => {
    ReservationActions.setLoadingClassName(null);
    ErrorActions.setErrorsForComponent(err, PAGEFLOW.COMPONENT_LOCATIONSEARCH);
    ErrorActions.setErrorsForComponent(err, PAGEFLOW.COMPONENT_BOOKING);
    ServiceController.serviceOnFailure('BookingWidgetService .onFailure()', err);
    console.warn('BookingWidgetService.onFailure():', err);
    return Promise.reject(err);
  }
}

const BookingWidgetService = {
  /**
   * @memberOf BookingWidgetService
   * @param {boolean} bool true/false
   * @deprecated Use ReservationActions.setSameLocation() instead
   * @todo remove
   */
  setSameLocation (bool) {
    logger.warn('Used deprecated method setSameLocation(). Please update to use ReservationActions.setSameLocation()', bool);
    LocationSearchActions.setSameLocation(bool)
  },

  /**
   * Submit the BookingWidget. Called from ResInitiateController
   * @param {data} data to post
   * @return {Object}   Ajax wrapped promise
   */
  submit (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      validate: ServiceFactory.baseValidate, // special treatment for messages
      request: (data) => EnterpriseServices.POST(SERVICE_ENDPOINTS.RESERVATION_INIT, {
        data: JSON.stringify(data)
      })
    })(...args)
  }
};

module.exports = BookingWidgetService;
