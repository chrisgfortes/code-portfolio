/**
 * Module for handling prepay and payment related services (ajax/xhr) calls.
 * Consolidation of Verification Prepay and Account Prepay code.
 * Intent is to move away from an "account" or "verification" specific
 *  home for this code as the code is used in both places.
 *
 * This consolidation is a work in progress -- and incomplete as of 2016-10-03 / ECR-22
 * Note: There are tickets in Sprint 2 of 1.8 that will cover more of this.
 */
import EnterpriseServices from '../services/EnterpriseServices';
import ErrorActions from '../actions/ErrorActions';
import ServiceFactory from '../factories/ServiceFactory';
import { SERVICE_ENDPOINTS } from '../constants';

const defaultServiceParams = {
  onFailure: (messages) => {
    ErrorActions.setErrorsForComponent(messages, 'paymentMethods');
    console.error('PaymentService::onFailure()', messages);
    return Promise.reject(messages);
  }
};

const PaymentService = {
  /**
   * Very similar to the current VerificationServices.initPrepay()
   * Different context and endpoint though.
   * Applies to: PAN GUI NA PREPAY
   *  - User Must be logged in
   * @param {Function} callback
   * @returns {Promise}
   */
  getPrepayToken (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: () => EnterpriseServices.GET(SERVICE_ENDPOINTS.PAYMENT_CARDSUBMIT_KEY)
    })(...args)
  },
  /**
   * Add a payment Method
   * Applies to: PAN GUI NA PREPAY
   * Hits /addpayment endpoint
   *  - User Must be logged in
   *  - Needs the payment identifier and various other bits from Pan GUI
   * payment_service_context_reference_identifier === payment_reference_id
   * payment_type = "CREDIT_CARD"
   * @returns {Promise}
   */

  postPaymentProfile (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: (payload) => EnterpriseServices.POST(SERVICE_ENDPOINTS.PAYMENT_ADD,
        { data: JSON.stringify(payload) }
      )
    })(...args)
  },
  /**
   * Update a payment Method
   * Applies to: PAN GUI NA PREPAY
   * Hits /updatepayment/{payment_reference_id} EWT/GMA endpoint
   *  - User Must be logged in
   * @returns {Promise}
   */
  postPaymentUpdate (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: (payload) => EnterpriseServices.POST(SERVICE_ENDPOINTS.PROFILE_UPDATE,
        { data: JSON.stringify(payload) }
      )
    })(...args)
  },
  /**
   * Update a payment Method
   * Applies to: PAN GUI NA PREPAY
   * Hits /deletepayment/{payment_reference_id} EWT/GMA endpoint
   *  - User Must be logged in
   *  - Needs the payment identifier
   * @returns {Promise}
   * @todo: ECR-22 we are currently using AccountServices.removePayment()
   */
  // doPaymentDelete () {
  //
  // },
  /**
   * @param {Function} callback
   */
  initPrepay (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      onFailure: (messages) => {
        ErrorActions.setErrorsForComponent(messages, 'verification');
        return defaultServiceParams.onFailure(messages);
      },
      request: () => EnterpriseServices.GET(SERVICE_ENDPOINTS.PREPAY_INIT)
    })(...args)
  },
  /**
   * Stub for VerificationServices.registerCardStatus()
   * Prepare to move code later. We may want to point to Verification code from over here.
   * @param {String} status
   * @param {Function} callback
   * @returns {Promise}
   */
  registerCardStatus (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      onFailure: (messages) => {
        ErrorActions.setErrorsForComponent(messages, 'verification');
        return defaultServiceParams.onFailure(messages);
      },
      request: (status) => EnterpriseServices.GET(SERVICE_ENDPOINTS.PREPAY_REGISTRATION + status)
    })(...args)
  },
  /**
   * Stub for VerificationServices.clearRegisterCardStatus()
   * Prepare to move code later. We may want to point to Verification code from over here.
   */
  clearRegisterCardStatus (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      onFailure: (messages) => {
        ErrorActions.setErrorsForComponent(messages, 'verification');
        return defaultServiceParams.onFailure(messages);
      },
      request: () => EnterpriseServices.GET(SERVICE_ENDPOINTS.PREPAY_REGISTRATION + false)
    })(...args)
  },

  /**
   * @function switchPayment
   * @return {Promise} Promise wrapped ajax
   */
  switchPayment() {
    return ServiceFactory.createService({
      defaultServiceParams: {
        onFailure: (messages) => {
          ErrorActions.setErrorsForComponent(messages, 'verification');
          return Promise.reject(messages);
        }
      },
      request: () => EnterpriseServices.GET(SERVICE_ENDPOINTS.PREPAY_FLOP)
    })()
  },

  /**
   * Stub for VerificationServices.ThreeDSCheck()
   * Prepare to move code later. We may want to point to Verification code from over here.
   */
  ThreeDSCheck (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      onFailure: (messages) => {
        ErrorActions.setErrorsForComponent(messages, 'verification');
        return defaultServiceParams.onFailure(messages);
      },
      request: () => EnterpriseServices.GET(SERVICE_ENDPOINTS.PREPAY_3DS)
    })(...args)
  }
};

module.exports = PaymentService;
