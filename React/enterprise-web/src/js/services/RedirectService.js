import { SERVICE_ENDPOINTS } from '../constants';

import ServiceFactory from '../factories/ServiceFactory';

import EnterpriseServices from '../services/EnterpriseServices';

const defaultServiceParams = {
  onFailure: (err) => {
    console.error('RedirectService.onFailure():', err);
    return Promise.reject(err);
  }
}

const RedirectService = {
  /**
   * Gets the browser preferred languages.
   * @function getBrowserPreferredLanguages
   * @memberOf RedirectService
   * @return     {Promise}  Promise which returns the browser preferred languages.
   */
  getBrowserPreferredLanguages () {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: ()=> {
        return EnterpriseServices.GET(SERVICE_ENDPOINTS.SESSION_LANGUAGE)
      }
    })()
  },
  /**
   * Not converted to use SessionFactory as the need for using getResponseHeader
   * @function getAkamaiHeaders
   * @memberOf RedirectService
   * @return     {Promise}    The akamai headers.
   */
  getAkamaiHeaders (callback) {
    return EnterpriseServices.HEAD(window.location.href,
      {
        callback (response, textStatus, request) {
          if (callback) {
            callback(response, textStatus, request);
          }
        },
        errorCallback (response) {
          console.warn(response);
        }
      }
    );
  }//,
  // getAkamaiHeaders () {
  //   return ServiceFactory.createService({
  //     ...defaultServiceParams,
  //     request: ()=> {
  //       return EnterpriseServices.HEAD(window.location.href)
  //     },
  //     onSuccess: ()=>
  //   })()
  // }
};

module.exports = RedirectService;
