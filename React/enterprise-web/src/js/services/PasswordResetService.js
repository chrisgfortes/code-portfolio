import EnterpriseServices from '../services/EnterpriseServices';
import ErrorActions from '../actions/ErrorActions';
import { SERVICE_ENDPOINTS } from '../constants';

const PasswordResetService = {
  retrievePassword: function (values, callback) {
    let data = {
      first_name: values.firstname,
      last_name: values.lastname,
      email: values.username
    };

    return EnterpriseServices.POST(SERVICE_ENDPOINTS.PASSWORD_RESET,
      {
        data: JSON.stringify(data),
        callback: (response) => {
          if (response.messages && response.messages.length) {
            ErrorActions.setErrorsForComponent(response, 'resetPassword');
            callback('error');
          } else {
            callback(response);
          }
        }
      });
  }
};

module.exports = PasswordResetService;
