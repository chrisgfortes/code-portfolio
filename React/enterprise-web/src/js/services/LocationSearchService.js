/**
 * @module LocationSearchService
 * Location and Location Search related service calls
 * LocationController for Location Entities
 * LocaionSearchController for Location Search activities
 */
import EnterpriseServices from '../services/EnterpriseServices';
import ReservationCursors from '../cursors/ReservationCursors';
import DateTimeActions from '../actions/DateTimeActions';

import ServiceController from '../controllers/ServiceController';
import ServiceFactory from '../factories/ServiceFactory';

import { SERVICE_ENDPOINTS } from '../constants';

import { debug } from '../utilities/util-debug';
const logger = debug({name: 'LocationSearchService', isDebug: false}).logger;

/**
 * @memberOf LocationSearchService
 * Used for ServiceFactory as onFailure error callback.
 * @type {Object}
 */
const defaultServiceParams = {
  onFailure: (err) => {
    ServiceController.serviceOnFailure('LocationSearchService .onFailure()', err);
    return Promise.reject(err);
  }
}

/**
 * @namespace Deprecated
 * Include below to do copmarison testing (then remove)
 * @type {Object}
 */
const Deprecated = {

  /**
   * @function getDateTimeValidityWithLocation
   * @memberOf LocationSearchService
   * @param  {object} location location object
   * @param  {string} type     pickup dropoff etc.
   * @return {void}
   * @deprecated (maybe not used any longer?)
   */
  getDateTimeValidityWithLocation(location, type) {
    logger.warn('DEPRECATED getDateTimeValidityWithLocation() CALL');
    const date = ReservationStateTree.select(ReservationCursors[type + 'Date']).get();//.format('YYYY-MM-DD');
    const time = ReservationStateTree.select(ReservationCursors[type + 'TimeValue']).get();//.format('HH:mm');

    if (date && time && location) {
      return EnterpriseServices.GET(SERVICE_ENDPOINTS.LOCATION_VALIDITY + location.key, {
        prefix: enterprise.solr.path,
        disableCacheBust : true,
        data: {
          date: date.format('YYYY-MM-DD'),
          time: time.format('HH:mm'),
          type: type ? type.toUpperCase() : ''
        },
        callback: (res) => {
          DateTimeActions.parseDateTimeValidity(res, type);
        }
      });
    } else {
      const deferred = $.Deferred();
      console.warn('missing date/time/location');
      deferred.resolve();
      return deferred.promise();
    }
  }
}

const LocationSearchService = {
  pickupClosedDateXHR: null,
  dropoffClosedDateXHR: null,

  ...Deprecated,

  /**
   * @function fetchClosedHoursForLocation
   * @memberOf LocationSearchService
   * @param  {string} type pickup, dropoff, etc.
   * @param {locationId} locationId
   * @param {object} data
   * @return {Promise}
   */
  fetchClosedHoursForLocation(...args) {
    logger.warn('fetchClosedHoursForLocation()');
    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: (type, locationId, data) => {
        logger.log('fetchClosedHoursForLocation() NEW NEW with RETURN createService()', type, locationId, data);
        if (LocationSearchService[type + 'ClosedDateXHR']) {
          LocationSearchService[type + 'ClosedDateXHR'].abort();
        }
        return LocationSearchService[type + 'ClosedDateXHR'] = EnterpriseServices.GET(SERVICE_ENDPOINTS.LOCATION_HOURS + locationId, {
          prefix: enterprise.solr.path,
          disableCacheBust: true,
          data: data
        })
      }
    })(...args);
  },

  /**
   * @function fetchAgeRangesForLocation
   * @memberOf LocationSearchService
   * Get Age Ranges for a Country or Location
   * @param  {string} peoplesoftIdOrCountryCode - ID for Location or a Country Code
   * @param  {boolen} branchSearch              - are we searching on a specific location, or regionally?
   * @return {Promise}                          - Promise with request method
   * moved from BookingWidgetService
   */
  fetchAgeRangesForLocation (peoplesoftIdOrCountryCode, branchSearch = true) {
    let keyId = peoplesoftIdOrCountryCode;
    logger.warn('fetchAgeRangesForLocation(id, branchSearch)', keyId, branchSearch);

    let searchPath = (branchSearch) ? SERVICE_ENDPOINTS.LOCATION_AGE_SEARCH : SERVICE_ENDPOINTS.LOCATION_AGE_SEARCH_BYCOUNTRY;
    const SEARCHPATH = searchPath + keyId;

    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: () => {
        logger.warn('SEARCHPATH:', SEARCHPATH);
        return EnterpriseServices.GET(SEARCHPATH, {
          prefix: enterprise.solr.path,
          disableCacheBust: true
        })
      }
    })(SEARCHPATH)
  },

  /**
   * @function fetchLocationDetails
   * @memberOf LocationSearchService
   * @param  {object} location location object
   * @param  {string} type     dropoff|pickup|both
   * @return {Promise}          ajax promise
   */
  fetchLocationDetails (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: (location) => {
        logger.log('fetchLocationDetails() from createService()', location);
        return EnterpriseServices.GET(SERVICE_ENDPOINTS.LOCATION_DETAILS + location)
      }
    })(...args)
  },

  /**
   * @function selectQuery
   * @memberOf LocationSearchService
   */
  fetchSelectQuery(...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: (data) => {
        return EnterpriseServices.GET(SERVICE_ENDPOINTS.LOCATION_SELECT, {
          prefix: enterprise.solr.path,
          disableCacheBust: true,
          data: {
            q: 'peopleSoftId:' + data
          }
        });
      }
    })(...args)
  },

  /**
   * @function getAfterHoursPolicy
   * @memberOf LocationSearchService
   * @param  {object} location location object
   * @return {Promise} Promise wrapped Ajax
   */
  getAfterHoursPolicy (...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: (location) => {
        logger.log('getAfterHoursPolicy()');
        return EnterpriseServices.GET(SERVICE_ENDPOINTS.LOCATION_HOURS_POLICY + location)
      }
    })(...args)
  },

  /**
   * Used by Location Search screen to load map results
   * @param  {string} query     coordinate system lat/long
   * @param  {Object} data search parameters
   * @return {Promise} Promise wrapped ajax
   */
  spatialSearch(...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: (query, data) => EnterpriseServices.GET(SERVICE_ENDPOINTS.LOCATION_SPATIAL(query), {
        prefix: enterprise.solr.path,
        disableCacheBust : true,
        data: data
      })
    })(...args)
  },

  /**
   * Used by Location Search screen to get Country Branches
   * @param  {string} query Country Code
   * @param  {object} pickup/dropoff date-time object
   * @return {Promise} Promise wrapped ajax
   */
  getCountryBranches(...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: (query, data) => EnterpriseServices.GET(SERVICE_ENDPOINTS.LOCATION_SEARCH_BRANCHES(query), {
        prefix: enterprise.solr.path,
        disableCacheBust : true,
        data: data
      })
    })(...args)
  },

  /**
   * @todo.  compare with LocationController.getClosedHoursForLocation()
   * @param  {string} type pickup | dropoff
   * @return {object}      xhr promise object
   * @todo use ServiceFactory.createService()
   */
  getDateTimeValidity(...args) {
    return ServiceFactory.createService({
      ...defaultServiceParams,
      request: (data, type) => {
        if (data && data.date && data.time && data.location && data.location.details) {
          return EnterpriseServices.GET(SERVICE_ENDPOINTS.LOCATION_VALIDITY(data.location.details.peopleSoftId), {
            prefix: enterprise.solr.path,
            disableCacheBust: true,
            data: {
              date: data.date.format('YYYY-MM-DD'),
              time: data.time && data.time.format('HH:mm'),
              type: type.toUpperCase()
            }
          })
        } else {
          // return Promise.reject('missing date/time/location.details');
          const deferred = $.Deferred();
          console.warn('missing date/time/location.details');
          deferred.resolve();
          return deferred.promise();
        }
      }
    })(...args)
  },
  /**
   * @memberOf LocationSearchService
   * @functiongetWeeksHoursForLocation
   * @param  {string} type         pickup/dropoff/etc
   * @param  {date} startDate    moment date object
   * @param  {string} peopleSoftId id for location
   * @return {object}              jquery ajx xhr promise
   * @todo use ServiceFactory.createService()
   */
  getWeeksHoursForLocation (type, startDate, peopleSoftId) {
    logger.warn('getWeekHoursForLocation() PROPER SERVICE TPYE???', type, startDate, peopleSoftId);
    const locationId = peopleSoftId || ReservationStateTree.select(ReservationCursors[type + 'Location']).get();

    if (this[type + 'ClosedDateXHR']) {
      this[type + 'ClosedDateXHR'].abort();
    }
    if (locationId) {
      let data = {
        from: startDate.clone().startOf('week').format('YYYY-MM-DD'),
        to: startDate.clone().startOf('week').add(1, 'week').format('YYYY-MM-DD')
      };

      this[type + 'ClosedDateXHR'] = EnterpriseServices.GET(SERVICE_ENDPOINTS.LOCATION_HOURS + locationId,
        {
          prefix: enterprise.solr.path,
          disableCacheBust : true,
          data: data,
          callback: (res) => {
            DateTimeActions.setWeeklyHours(type, res.data);
          }
        });
    } else {
      logger.warn('!! Missing query parameters for LocationSearchService.getWeeksHoursForLocation()');
    }
  }
};

module.exports = LocationSearchService;
