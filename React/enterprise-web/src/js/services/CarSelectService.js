import EnterpriseServices from '../services/EnterpriseServices';
import ServiceFactory from '../factories/ServiceFactory';

import ErrorActions from '../actions/ErrorActions';

import { SERVICE_ENDPOINTS } from '../constants';

const defaultServiceParams = {
  /**
   * @todo Have some concerns about the error handling here.
   * The changes in ServiceFactory may be preventing messages from showing.
   */
  onFailure: (messages) => {
    ErrorActions.setErrorsForComponent(messages, 'viewCarSelect');
    ErrorActions.stopLoading();
    console.error(messages);
    return Promise.reject(messages);
  }
};

const CarSelectService = {
  getVehicles: ServiceFactory.createService({
    ...defaultServiceParams,
    request: () => EnterpriseServices.GET(
      SERVICE_ENDPOINTS.RESERVATION_VEHICLE_AVAILABILITY,
      { ignoreStep: true }
    ),
    onSuccess: (response) => response.availablecars || [] // @todo - throw an error if there's no available cars??
  }),
  getVehicleDetails: ServiceFactory.createService({
    ...defaultServiceParams,
    request: (vehicle) => EnterpriseServices.GET(
      SERVICE_ENDPOINTS.RESERVATION_VEHICLE_DETAILS,
      { data: {'carClassCode': vehicle.code} }
    )
  }),
  getAvailableUpgrades: ServiceFactory.createService({
    ...defaultServiceParams,
    request: () => EnterpriseServices.GET(SERVICE_ENDPOINTS.RESERVATION_VEHICLE_UPGRADES)
  }),
  upgradeVehicle: ServiceFactory.createService({
    ...defaultServiceParams,
    request: (classCode) => EnterpriseServices.POST(
      SERVICE_ENDPOINTS.RESERVATION_VEHICLE_UPGRADE_SELECT,
      { data: JSON.stringify({ car_class_code: classCode }) }
    )
  }),
  clearRedemptionVehicle: ServiceFactory.createService({
    ...defaultServiceParams,
    request: () => EnterpriseServices.GET(SERVICE_ENDPOINTS.RESERVATION_VEHICLE_CLEAR)
  }),
  selectRedemptionVehicle: ServiceFactory.createService({
    ...defaultServiceParams,
    request: ({carClass, days}) => EnterpriseServices.POST(
      SERVICE_ENDPOINTS.RESERVATION_VEHICLE_REDEMPTION,
      { data: JSON.stringify({ "car_class_code": carClass, "redemption_day_count": days }) }
    )
  }),
  selectVehicle: ServiceFactory.createService({
    ...defaultServiceParams,
    request: ({carClass, isPrepay}) => EnterpriseServices.POST(
      SERVICE_ENDPOINTS.RESERVATION_VEHICLE_SELECTCARCLASS,
      { data: JSON.stringify({'car_class_code': carClass, 'prepay_selected': isPrepay}) }
    )
  })
};

module.exports = CarSelectService;
