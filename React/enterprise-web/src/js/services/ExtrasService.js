import ErrorActions from '../actions/ErrorActions';
import EnterpriseServices from '../services/EnterpriseServices';
import ServiceFactory from '../factories/ServiceFactory';
import { SERVICE_ENDPOINTS } from '../constants';

const defaultServiceParams = {
  onSuccess: () => {
    ErrorActions.clearErrorsForComponent('extrasView');
  },
  onFailure: (messages) => {
    ErrorActions.setErrorsForComponent(messages, 'extrasView');
    console.error('ExtrasService::onFailure()', messages);
    return Promise.reject(messages);
  }
};

const ExtrasService = {
  endPoints: {
    route: SERVICE_ENDPOINTS.RESERVATION_EXTRAS_SUBMIT,
    update: SERVICE_ENDPOINTS.RESERVATION_EXTRAS_UPDATE
  },
  finishExtras: ServiceFactory.createService({
    ...defaultServiceParams,
    request: () => EnterpriseServices.POST(
      SERVICE_ENDPOINTS.RESERVATION_EXTRAS_SUBMIT,
      {}
    )
  }),
  updateSelectExtras: ServiceFactory.createService({
    ...defaultServiceParams,
    request: (selectedExtras) => EnterpriseServices.POST(
      SERVICE_ENDPOINTS.RESERVATION_EXTRAS_UPDATE,
      {
        data: JSON.stringify({
          'extras': selectedExtras
        })
      }
    )
  })
};

export default ExtrasService;
