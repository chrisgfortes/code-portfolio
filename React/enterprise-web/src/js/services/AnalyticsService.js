import EnterpriseServices from './EnterpriseServices';
import Analytics from '../modules/Analytics';
import { SERVICE_ENDPOINTS } from '../constants';

const AnalyticsService = {
  getAnalytics() {
    return EnterpriseServices.GET(SERVICE_ENDPOINTS.SESSION_ANALYTICS,
      {
        data: data,
        callback: (response, textStatus, request) => {
          if (response.error) {
            console.log(response.error);
          } else {
            if (response && response.analytics) {
              //Combining EWT analytics object with AEM Analytics Object
              window._analytics = response.analytics;
              window._analytics.pageInfo = enterprise.analytics;
              window._analytics.ready = true;
              //console.log('analytics back from EWT');
              Analytics.trigger('html', 'analiytics');
            }
          }
        }
      }
    );
  }
}

module.exports = AnalyticsService;
