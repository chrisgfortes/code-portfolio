import ReservationCursors from '../cursors/ReservationCursors';

const ModifyActions = {
  toggleConfrimationModal(bool) {
    ReservationStateTree.select(ReservationCursors.viewModify).set('modal', bool);
  },
  toggleInflightModifyModal(bool) {
    ReservationStateTree.select(ReservationCursors.inflightModify).set('modal', bool);
  },
  toggleCancelConfirmationModal(bool) {
    ReservationStateTree.select(ReservationCursors.viewModifyCancel).set('modal', bool);
  },
  // rebookCancel comes from GMA and means reservations/modify/init has been created
  toggleModifyFlag(bool) {
    ReservationStateTree.select(ReservationCursors.viewModify).set('rebookCancel', bool);
  },
  toggleCancelReservationModal(bool) {
    ReservationStateTree.select(ReservationCursors.viewCancel).set('modal', bool);
  },
  toggleRetrieveAndCancel(bool) {
    ReservationStateTree.select(ReservationCursors.viewCancel).set('retrieveAndCancel', bool);
  },
  /**
   * Determines if inflight modify.
   * @todo change to use ReservationStatusFlags
   * @return     {boolean}  True if inflight modify, False otherwise.
   */
  isInflightModify() {
    return ReservationStateTree.select(ReservationCursors.inflightModifyIsInFlight).get();
  },
  togglePrepayTerms (bool) {
    ReservationStateTree.select(ReservationCursors.showPrepayTerms).set(bool);
  },
  setCancelRebook (bool) {
    if(typeof bool === "boolean") {
      ReservationStateTree.select(ReservationCursors.cancelRebook).set(bool);
    }
  },
  hasCancelRebook() {
    // view / modify / cancel_rebook
    return ReservationStateTree.get(ReservationCursors.cancelRebook) !== null;
  }
};

module.exports = ModifyActions;
