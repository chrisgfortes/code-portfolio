import ReservationCursors from '../cursors/ReservationCursors';
import { debug } from '../utilities/util-debug';

const ReceiptActions = debug({
  isDebug: false,
  logger: {},
  name: 'ReceiptActions.js',
  toggleModal (bool) {
    this.logger.warn('toggleModal()', bool);
    ReservationStateTree.select(ReservationCursors.receipt).set('modal', bool);
    if (!bool) {
      this.logger.log('!bool', bool, !bool);
      this.setReceiptLoading(true); // why do we set this to true if it's closing?
      ReservationStateTree.select(ReservationCursors.receipt).set('details', null);
    }
  },
  fail () {
    this.logger.warn('fail()');
    ReservationStateTree.select(ReservationCursors.receipt).set('modal', false);
  },
  setReceiptDetails(receipt){
    ReservationStateTree.select(ReservationCursors.receipt).set('details', receipt);
  },
  setReceiptLoading(bool){
    ReservationStateTree.select(ReservationCursors.receipt).set('loading', bool);
  }
});

module.exports = ReceiptActions;
