import AppActions from './AppActions'; // temporary while moving things
import LocationSearchActions from './LocationSearchActions';
import { TimePickerActions } from './TimePickerActions';
import DateTimeActions from './DateTimeActions';

import { GLOBAL, LOCATION_SEARCH } from '../constants';

import ReservationCursors from '../cursors/ReservationCursors';

// import PricingController from '../controllers/PricingController';

import LocationFactory from '../factories/LocationFactory';
import LocationCleanFactory from '../factories/Location/LocationCleanFactory';
import LocationModel from '../factories/Location';

// import { SelectedLocationUpdateModel } from '../classes/Location';

import ReservationStateTree from '../stateTrees/ReservationStateTree';

import { debug } from '../utilities/util-debug';
const logger = debug({isDebug: false, name: 'ReservationActions'}).logger;

const modelCursor = ReservationStateTree.select('model');
// const viewCursor = ReservationStateTree.select('view'); not used yay!

const ReservationActions = {
  // @todo use set / get namespaces

  get: {
    reservationStatusFlags () {
      return ReservationStateTree.select(ReservationCursors.reservationStatusFlags).get();
    }
  },
  /**
   * @namespace  {object} ReservationActions.set
   */
  set: {
    reservationStatusFlags (data) {
      ReservationStateTree.select(ReservationCursors.reservationStatusFlags).set(data);
    },
    loyaltyLastName (val) {
      ReservationStateTree.select(ReservationCursors.loyaltyLastName).set(val);
    },
    membershipId(val) {
      ReservationStateTree.select(ReservationCursors.memberShipId).set(val);
    },
    /**
     * @function   ReservationActions.set.personalFromDriver()
     * @param {object} driverInformation is from reservationSession.driverInformation
     */
    personalFromDriver (driverInformation) {
      logger.warn('personalFromDriver(driverInformation)', driverInformation);
      // debugger;
      const personal = ReservationStateTree.select(ReservationCursors.personal);
      const personal_obj = personal.get(); // eslint-disable-line camelcase

      personal.set(Object.assign({}, personal_obj, {
        firstName: personal_obj.firstName || driverInformation.first_name,
        lastName: personal_obj.lastName || driverInformation.last_name,
        phoneNumber: personal_obj.phoneNumber || _.get(driverInformation, 'phone.phone_number'),
        email: personal_obj.email || driverInformation.email_address
      }));
    }
  },
  update: {},

  /**
   * @param {object} updateObj - Baobab.js $set object
   * @todo we should remove this and go by cursor. this makes finding ReservationCursors too difficult.
   */
  updateModel (updateObj) {
    logger.warn('updateModel()', updateObj);
    // console.groupCollapsed('ReservationActions.updateModel()')
    logger.log('---------------------- updating directly from MODEL ----------------------');
    logger.log(updateObj);
    logger.log('---------------------- updating directly from MODEL ----------------------');
    // console.groupEnd()
    modelCursor.update(updateObj);
  },

  /**
   * When a BookingWidget user selects a round trip (default) location this is called
   * Also used by ReservationFlowModelController when setting the location from session
   * @param {object} location Location object
   * @todo : instead of calling getLocationType twice here, call it in getSelectedLocationUpdateModel
   */
  setLocationForAll (location) {
    const updateObj = {
      pickup: {
        // @todo really we should just pass getSelectedLocationUpdateModel the location object, right?
        location: LocationFactory.getSelectedLocationUpdateModel({
          id: location.key,
          locationName: LocationModel.getLocationName(location),
          locationType: LocationModel.getLocationType(location),
          type: LocationModel.getLocationType(location),
          lat: location.lat,
          longitude: location.longitude,
          countryCode: location.countryCode,
          airportCode: location.airportCode,
          costCenter: location.costCenter
        })
      },
      dropoff: {
        // @todo really we should just pass getSelectedLocationUpdateModel the location object, right?
        location: LocationFactory.getSelectedLocationUpdateModel({
          id: location.key,
          locationName: LocationModel.getLocationName(location),
          locationType: LocationModel.getLocationType(location),
          type: LocationModel.getLocationType(location),
          lat: location.lat,
          longitude: location.longitude,
          countryCode: location.countryCode,
          airportCode: location.airportCode,
          costCenter: location.costCenter
        })
      }
    };

    logger.log('setLocationForAll()', location, updateObj);

    modelCursor.update(updateObj);
  },

  /**
   * called by BookingWidgetModelController or ReservationFlowModelController
   * @memberOf ReservationActions
   * @param {object} location location object
   * @param {string} type     pickup, dropoff, etc.
   */
  setLocation (location, type) {
    let updateObj = {};
    updateObj[type] = {
      // @todo send getSelectedLocationUpdateModel just the location object
      location: LocationFactory.getSelectedLocationUpdateModel({
        id: location.key,
        locationName: LocationModel.getLocationName(location),
        locationType: LocationModel.getLocationType(location),
        type: LocationModel.getLocationType(location),
        lat: location.lat,
        longitude: location.longitude,
        countryCode: location.countryCode,
        airportCode: location.airportCode,
        costCenter: location.costCenter
      })
    };
    ReservationStateTree.select(ReservationCursors.reservationSteps).set(type + 'Location', LocationModel.getLocationName(location));

    modelCursor.update(updateObj);
  },

  /**
   * @memberOf ReservationActions
   * @param  {string} type pickup/dropoff/both
   * @return {void}
   * @deprecated This should be done from LocationSearchActions
   * @todo update to use other method and use classes / factories
   */
  clearLocation (type) {
    logger.warn('Used deprecated method clearLocation() in ReservationActions.');
    const updateObj = LocationCleanFactory.toState(type);

    this.clearPolicies(type);

    ReservationStateTree.options.autoCommit = false;
    this.clearAfterHour(type);

    modelCursor.update(updateObj);
    this.setTime(type, 'closed', null);
    LocationSearchActions.clearMapResults();
    LocationSearchActions.setMapTargetDetails(type, null);

    ReservationStateTree.commit();
    ReservationStateTree.options.autoCommit = true;
  },
  setPaymentProcessor (processor) {
    ReservationStateTree.select(ReservationCursors.paymentProcessor).set(processor);
  },
  setBlockModifyPickupLocation (flag) {
    ReservationStateTree.select(ReservationCursors.blockModifyPickupLocation).set(flag);
  },
  setBlockModifyDropoffLocation (flag) {
    ReservationStateTree.select(ReservationCursors.blockModifyDropoffLocation).set(flag);
  },
  setCollectNewModifyPaymentCard (flag) {
    ReservationStateTree.select(ReservationCursors.collectNewModifyPaymentCard).set(flag);
  },
  /**
   * @function setLocationSearchType
   * @memberOf ReservationActions
   * sets ReservationStateTree.data.view.locationSelect to be showing
   * @param {string} type type of location select (view/locationSelect)
   */
  setLocationSearchType (type) {
    ReservationStateTree.select(ReservationCursors.locationSelect).set('showType', type);
  },
  getLocationSearchType () {
    return ReservationStateTree.select(ReservationCursors.locationSelect).get('showType');
  },
  setBookingWidgetExpanded (expanded) {
    ReservationStateTree.select('view').set('bookingWidgetExpanded', !!expanded);
  },
  getBookingWidgetExpanded () {
    return ReservationStateTree.get(ReservationCursors.bookingWidgetExpanded);
  },
  setBookingWidgetLoadingState (state) {
    ReservationStateTree.select(ReservationCursors.bookingWidget).set('loading', state);
  },
  setBookingWidgetUpdatedError (error) {
    ReservationStateTree.select(ReservationCursors.bookingWidgetErrors).set(error);
  },
  setLoyaltyBrand (brand) {
    ReservationStateTree.select(ReservationCursors.loyaltyBook).set('loyalty_brand', brand);
  },
  closeNoVehicleAvailabilityModal () {
    ReservationStateTree.select(ReservationCursors.noVehicleAvailability).set(false);
  },
  closeDNRModal () {
    ReservationStateTree.select(ReservationCursors.dnr).set(false);
  },
  setRightPlaceModal () {
    ReservationStateTree.select(ReservationCursors.rightPlaceModal).set(true);
  },
  setRightPlaceType (hash) {
    ReservationStateTree.select(ReservationCursors.rightPlaceType).set(hash);
  },
  setRightPlaceRefer (hashModel) {
    ReservationStateTree.select(ReservationCursors.rightPlaceRefer).set(hashModel);
  },
  closeRightPlaceModal () {
    ReservationStateTree.select(ReservationCursors.rightPlaceModal).set(false);
  },
  // @todo: remove this please
  setCancelTotal (payData) {
    logger.warn('setCancelTotal()', payData);
    let cancelDetails = ReservationStateTree.select(ReservationCursors.cancellationDetails).get();
    if (cancelDetails) {
      ReservationStateTree.select(ReservationCursors.cancellationDetails).set('total', payData);
    } else {
      logger.log('no cancelation details to set total on.');
    }
  },

  /**
   * @todo should probably set this using a DriverInformation Model Factory
   * @param {object} driver sets driver information from cros (UserFactory driverSourceData)
   */
  setFromCrosDriverInfo (driver) {
    // debugger;
    logger.warn('setFromCrosDriverInfo(driver)', driver);
    ReservationStateTree.options.autoCommit = false;
    ReservationStateTree.select(ReservationCursors.driverInformation).set(driver);
    // ReservationStateTree.select(ReservationCursors.driverInfoIdentifier).set(driver.individual_indentifier);
    // ReservationStateTree.select(ReservationCursors.driverInfoLoyalty).set(driver.loyalty_program_type);

    // let driverPay = driver.payment_profile;
    // if (driverPay && driverPay.payment_methods) {
    //   let payMethods = driverPay.payment_methods;
    //   if (payMethods && payMethods.length > 0) {
    //     ReservationStateTree.select(ReservationCursors.driverInfoPaymentProfile).set(payMethods);
    //   }
    // }
    ReservationStateTree.commit();
    ReservationStateTree.options.autoCommit = true;
  },

  /**
   * @memberOf ReservationActions
   * @return {void} updates state tree
   */
  clearLocationsForAll () {
    let pickupModel = LocationCleanFactory.toState(LOCATION_SEARCH.PICKUP);
    let dropoffModel = LocationCleanFactory.toState(LOCATION_SEARCH.DROPOFF);
    const updateObj = {
      pickup: pickupModel.pickup,
      dropoff: dropoffModel.dropoff
    };

    ReservationStateTree.options.autoCommit = false;

    this.clearAfterHour(LOCATION_SEARCH.PICKUP);
    this.clearAfterHour(LOCATION_SEARCH.DROPOFF);

    modelCursor.update(updateObj);
    //TODO: Refactor below
    this.setTime(LOCATION_SEARCH.PICKUP, 'closed', null);
    this.setTime(LOCATION_SEARCH.DROPOFF, 'closed', null);
    LocationSearchActions.clearMapResults();

    this.clearPolicies(LOCATION_SEARCH.BOTH);
    ReservationStateTree.commit();
    ReservationStateTree.options.autoCommit = true;
  },
  clearLocationResults (type) {
    let updateObj = LocationFactory.getLocationDirectionResults(type);
    modelCursor.update(updateObj);
  },
  setTimeArray (type) {
    if (type) {
      TimePickerActions.setTimeArray(type);
    } else {
      TimePickerActions.setTimeArray(LOCATION_SEARCH.PICKUP);
      TimePickerActions.setTimeArray(LOCATION_SEARCH.DROPOFF);
    }
  },
  clearClosedTimes (type) {
    ReservationStateTree.options.autoCommit = false;

    ReservationStateTree.select(ReservationCursors[type + 'TimeInitial']).set(true);
    ReservationStateTree.select(ReservationCursors[type + 'ModelDate']).set('closedDates', null);
    ReservationStateTree.select(ReservationCursors[type + 'Time']).set('closed', null);
    ReservationStateTree.select(ReservationCursors.model).select(type).set('locationHours', null);
    this.setTimeArray(type);

    ReservationStateTree.commit();
    ReservationStateTree.options.autoCommit = true;
  },
  clearDateTime (type) {
    logger.log('clearDateTime()', type);
    ReservationStateTree.options.autoCommit = false;
    if (!type) {
      logger.log('01');
      //clear closed hours information
      ReservationStateTree.select(ReservationCursors.pickupModelDate).set('closedDates', null);
      ReservationStateTree.select(ReservationCursors.dropoffModelDate).set('closedDates', null);
      ReservationStateTree.select(ReservationCursors.pickupTime).set('closed', null);
      ReservationStateTree.select(ReservationCursors.dropoffTime).set('closed', null);
      ReservationStateTree.select(ReservationCursors.pickupModel).set('locationHours', null);
      ReservationStateTree.select(ReservationCursors.dropoffModel).set('locationHours', null);
    } else {
      logger.log('02');

      ReservationStateTree.select(ReservationCursors[type + 'ModelDate']).set('momentDate', null);
      ReservationStateTree.select(ReservationCursors[type + 'Time']).set('value', moment().hour(12).minute(0).second(0));
      ReservationStateTree.select(ReservationCursors[type + 'TimeInitial']).set(true);

      ReservationStateTree.select(ReservationCursors[type + 'ModelDate']).set('closedDates', null);
      ReservationStateTree.select(ReservationCursors[type + 'Time']).set('closed', null);
      ReservationStateTree.select(ReservationCursors.model).select(type).set('locationHours', null);
    }
    ReservationStateTree.commit();
    ReservationStateTree.options.autoCommit = true;
  },
  setInvalidDate (bool) {
    ReservationStateTree.select(ReservationCursors.invalidDate).set(bool);
  },
  showDatePickerModalOfType (type) {
    ReservationStateTree.select(ReservationCursors.locationSelect).set('showDatePickerModalOfType', type);
  },

  // @todo consider integrating with DateTimeField
  setDate (date, type) {
    let obj = {};
    obj[type] = {
      'date': {
        'momentDate': {
          $set: date
        }
      }
    };
    modelCursor.update(obj);
    ReservationStateTree.select(ReservationCursors.reservationSteps).set(type + 'Date', moment(date));
  },
  // different from below?
  setTempDate (date, type) {
    ReservationStateTree.select(ReservationCursors[type + 'View']).set('tempDate', date);
  },
  // different from above?
  setViewDate (date, type) {
    ReservationStateTree.select(ReservationCursors[type + 'View']).set('date', date);
  },
  setTime (type, variable, value) {
    const obj = {
      [variable]: {
        $set: value
      }
    };

    ReservationStateTree.select(ReservationCursors[type + 'Time']).update(obj);
  },
  setAfterHour (type, value) {
    ReservationStateTree.select(ReservationCursors.model).select(type).set('afterHour', value);
  },
  clearAfterHour (type) {
    ReservationStateTree.select(ReservationCursors.model).select(type).set('afterHour', {
      allowed: null,
      phone: null,
      message: null
    });
  },

  setClosedDates: DateTimeActions.setClosedDates.bind(DateTimeActions),

  setAgeRange (ageRange) {
    ReservationStateTree.select(ReservationCursors.model).set('ageRange', ageRange);
  },
  setAgeLabel (ageLabel) {
    ReservationStateTree.select(ReservationCursors.model).set('ageLabel', ageLabel);
  },
  setLocationHours (data, type) {
    // logger.warn('setLocationHours()', data, type);
    ReservationStateTree.select(ReservationCursors.model).select(type).set('locationHours', data);
  },
  setClosedTimesForLocationAndDate (type) {
    // logger.warn('setClosedTimesForLocationAndDate()', type);
    const locationHours = ReservationStateTree.select(ReservationCursors[type + 'LocationHours']).get();
    const date = ReservationStateTree.select(ReservationCursors[type + 'Date']).get();

    // @todo this is not good, what does this belong to?
    if (this[type + 'ClosedTimeXHR']) {
      this[type + 'ClosedTimeXHR'].abort();
    }

    if (locationHours && Object.keys(locationHours).length && locationHours[date.format('YYYY-MM-DD')]) {
      this.setTime(type, 'closed', locationHours[date.format('YYYY-MM-DD')].STANDARD.hours);
      TimePickerActions.setTimeArray(type);
    }
  },
  /**
   * @memberOf ReservationActions
   * setPolicies is called after getting location details, because that's when we get policy data back
   * @param {string} type     location type dropoff|pickup|both
   * @param {array} policies  array of policies we got back
   */
  // setPoliciesOnLocations
  setPolicies (type, policies) {
    logger.log('setPolicies (usually after getLocationDetails call');
    if (type === LOCATION_SEARCH.BOTH) {
      ReservationStateTree.select(ReservationCursors.pickupModel).set('policies', policies);
      ReservationStateTree.select(ReservationCursors.dropoffModel).set('policies', policies);
    } else {
      ReservationStateTree.select(ReservationCursors.model).select(type).set('policies', policies);
    }
  },

  /**
   * @memberOf ReservationActions
   * @param  {string} type pickup, dropoff, both
   * @return {void}
   */
  // clearPoliciesOnLocations
  clearPolicies (type) {
    if (type === LOCATION_SEARCH.BOTH) {
      ReservationStateTree.select(ReservationCursors.pickupModel).set('policies', null);
      ReservationStateTree.select(ReservationCursors.dropoffModel).set('policies', null);
    } else {
      ReservationStateTree.select(ReservationCursors.model).select(type).set('policies', null);
    }
  },
  setAge (value) {
    logger.log('setAge()', value);
    ReservationStateTree.select(ReservationCursors.age).set(value);
  },
  setCoupon (value) {
    ReservationStateTree.select(ReservationCursors.model).set('coupon', value);
  },
  setExpeditedCoupon (value) {
    ReservationStateTree.select(ReservationCursors.model).set('expeditedCoupon', value);
  },
  setCouponLabel (value) {
    ReservationStateTree.select(ReservationCursors.model).set('couponLabel', value);
  },
  setEmployeeNumber (value) {
    ReservationStateTree.select(ReservationCursors.employeeNumber).set(value);
  },
  setCostCenter (value) {
    ReservationStateTree.select(ReservationCursors.costCenter).set(value);
  },
  setFedexRequisitionNumber (value) {
    ReservationStateTree.select(ReservationCursors.fedexRequisitionNumber).set(value);
  },
  setPrepopulatedCoupon (contract_details) {
    ReservationStateTree.select(ReservationCursors.prepopulatedCoupon).set('contract_number', contract_details.contract_number);
    ReservationStateTree.select(ReservationCursors.prepopulatedCoupon).set('contract_name', contract_details.contract_name);
    ReservationStateTree.select(ReservationCursors.prepopulatedCoupon).set('allow_account_removal_indicator', contract_details.allow_account_removal_indicator);
  },

  setTimeFormat (value) {
    ReservationStateTree.select(ReservationCursors.view).set('timeFormat', value);
  },
  setDateFormat (value) {
    ReservationStateTree.select(ReservationCursors.view).set('dateFormat', value);
  },
  /**
   * This is actually just setting the name of the selected type in the reservationSteps (for the nav?)
   * @method setCarClassDetails
   * @memberOf ReservationActions
   * @param value
   */
  setCarClassDetails (carDetails) {
    let carName = carDetails.name;

    ReservationStateTree.options.autoCommit = false;

    ReservationStateTree.select(ReservationCursors.reservationSteps).set('carClass', carName);
    ReservationStateTree.select(ReservationCursors.reservationSteps).set('showTotals', true);
    ReservationStateTree.select(ReservationCursors.selectedCar).set(carDetails);

    ReservationStateTree.commit();
    ReservationStateTree.options.autoCommit = true;
  },
  clearCarClassDetails () {
    ReservationStateTree.options.autoCommit = false;

    ReservationStateTree.select(ReservationCursors.reservationSteps).set('carClass', null);
    ReservationStateTree.select(ReservationCursors.reservationSteps).set('showTotals', false);
    ReservationStateTree.select(ReservationCursors.selectedCar).set(null);

    ReservationStateTree.commit();
    ReservationStateTree.options.autoCommit = true;
  },

//////// next few below are app stuff
// @deprecated moving these all to AppActions (ECR-13850)
// @todo remove these!
  setCurrentView: AppActions.setCurrentView.bind(AppActions),
  changeView: AppActions.changeView.bind(AppActions),
  setLoadingView: AppActions.setLoadingView.bind(AppActions),
  finishedLoading: AppActions.finishedLoading.bind(AppActions),
  triggerError: AppActions.triggerError.bind(AppActions),
  clearErrors: AppActions.clearErrors.bind(AppActions),
  setReservationSession: AppActions.setReservationSession.bind(AppActions),
  setCurrentHash: AppActions.setCurrentHash.bind(AppActions),
  getCurrentHash: AppActions.getCurrentHash.bind(AppActions),
  locationDataReady: AppActions.locationDataReady.bind(AppActions),
  initialDataReady: AppActions.initialDataReady.bind(AppActions),
  setLoadingClassName: AppActions.setLoadingClassName.bind(AppActions),
  setComponentToRender: AppActions.setComponentToRender.bind(AppActions),
  hasDataReady: AppActions.hasDataReady.bind(AppActions),
  hasDataReadyByCursor: AppActions.hasDataReadyByCursor.bind(AppActions),
  setHasDataReady: AppActions.setHasDataReady.bind(AppActions),
  getLastCompleteStepHash: AppActions.getLastCompleteStepHash.bind(AppActions),
/////////////// app stuff above

  revertDateTime () {
    const pickupDate = ReservationStateTree.select(ReservationCursors.pickupPreviousDate).get();
    const dropoffDate = ReservationStateTree.select(ReservationCursors.dropoffPreviousDate).get();
    const pickupTime = ReservationStateTree.select(ReservationCursors.pickupPreviousTime).get();
    const dropoffTime = ReservationStateTree.select(ReservationCursors.dropoffPreviousTime).get();

    ReservationStateTree.options.autoCommit = false;

    this.setDate(pickupDate, LOCATION_SEARCH.PICKUP);
    this.setDate(dropoffDate, LOCATION_SEARCH.DROPOFF);

    this.setTime(LOCATION_SEARCH.PICKUP, 'value', pickupTime);
    this.setTime(LOCATION_SEARCH.DROPOFF, 'value', dropoffTime);

    ReservationStateTree.commit();
    ReservationStateTree.options.autoCommit = true;
  },
  backupCurrentDateTime () {
    const pickupDate = ReservationStateTree.select(ReservationCursors.pickupDate).get();
    const dropoffDate = ReservationStateTree.select(ReservationCursors.dropoffDate).get();
    const pickupTime = ReservationStateTree.select(ReservationCursors.pickupTimeValue).get();
    const dropoffTime = ReservationStateTree.select(ReservationCursors.dropoffTimeValue).get();

    ReservationStateTree.options.autoCommit = false;

    ReservationStateTree.select(ReservationCursors.pickupModelDate).set('previousDate', pickupDate);
    ReservationStateTree.select(ReservationCursors.dropoffModelDate).set('previousDate', dropoffDate);
    ReservationStateTree.select(ReservationCursors.pickupTime).set('previousTime', pickupTime);
    ReservationStateTree.select(ReservationCursors.dropoffTime).set('previousTime', dropoffTime);

    ReservationStateTree.commit();
    ReservationStateTree.options.autoCommit = true;
  },
  toggleModify (bool) {
    ReservationStateTree.select(ReservationCursors.view).set('modify', bool);
  },
  toggleInflightModify (bool) {
    // console.warn('toggleInflightModify(bool)', bool);
    ReservationStateTree.select(ReservationCursors.inflightModify).set('isInflight', bool);
    ReservationStateTree.select(ReservationCursors.pickupTimeInitial).set(!bool);
    ReservationStateTree.select(ReservationCursors.dropoffTimeInitial).set(!bool);
  },
  showPolicyModal (type) {
    ReservationStateTree.select(ReservationCursors.view).select(type).select('policies').set('modal', true);
  },
  showPolicy (code, type) {
    ReservationStateTree.select(ReservationCursors.view.concat([type])).select('policies').set('showPolicy', code);
  },
  setPreSelectedCarClass (code) {
    ReservationStateTree.select(ReservationCursors.modelCarSelect).set('preSelectedCarClass', code);
  },

  /**
   * @method setLocationDetails
   * @memberOf ReservationActions
   * @param {object} location details details to be used in model
   * @param {string} type    'pickup', 'dropoff', both
   */
  setLocationDetails (details, type) {
    if (type === LOCATION_SEARCH.BOTH) {
      ReservationStateTree.select(ReservationCursors.pickupLocationObj).set('details', details);
      ReservationStateTree.select(ReservationCursors.dropoffLocationObj).set('details', details);
    } else {
      ReservationStateTree.select(ReservationCursors[type + 'LocationObj']).set('details', details);
    }
  },

  /**
   * Location (Input) Search Results
   * @param type
   * @param bool
   */
  setNoResults (type, bool) {
    logger.log('setNoResults() should use classes');
    let update = {};
    update[type] = {
      noResults: {
        $set: bool
      }
    };

    this.updateModel(update);
  },
  setRedemptionDays (days) {
    ReservationStateTree.select(ReservationCursors.redemption).set('days', days);
  },

  setRentalTerms (response) {
    if (response[0]) {
      this.setRentalTermsData(response, GLOBAL.RENTAL_TERMS.CODE, i18n('EU_commission_0002') || 'Rental Terms and Conditions');
      this.setRentalTermsDetailedText(response[0].rental_terms_and_conditions_text);
    } else {
      this.setRentalTermsData(null, null, null);
      this.setRentalTermsDetailedText(null);
    }
  },

  setRentalTermsData (data, code, desc) {
    ReservationStateTree.select(ReservationCursors.rentalTerms).set('policy_text', data);
    ReservationStateTree.select(ReservationCursors.rentalTerms).set('code', code);
    ReservationStateTree.select(ReservationCursors.rentalTerms).set('description', desc);
  },

  setRentalTermsDetailedText (data) {
    ReservationStateTree.select(ReservationCursors.rentalTermsText).set('detailedText', data);
  },
  setFedexSequenceMapping (sequenceMapping) {
    ReservationStateTree.select(ReservationCursors.fedexSequence).set(sequenceMapping);
  },
  clearEmployeeNumberError () {
    const errors = ReservationStateTree.select(ReservationCursors.bookingWidgetErrors).get();

    if (Array.isArray(errors)) {
      let filteredErrors = errors.reduce((result, error) => {
        if (error.code !== 'required_field') {
          result.push(error);
        }
        return result;
      }, []);

      ReservationStateTree.select(ReservationCursors.bookingWidgetErrors).set(filteredErrors);
    }
  },
  setEligibility (eligibility) {
    ReservationStateTree.select(ReservationCursors.reservationEligibility).set(eligibility);
  },
  setExpediteEligibility (eligibility) {
    ReservationStateTree.select(ReservationCursors.expediteEligibility).set(eligibility);
  },
  setPaymentInformation (payment) {
    ReservationStateTree.select(ReservationCursors.paymentInformation).set(payment);
  },
  /**
   * Initially called by SessionController current-session callback
   * @param {boolean} hasNoMarketingMessage
   */
  setMarketingFlag(hasNoMarketingMessage) {
    ReservationStateTree.select(ReservationCursors.isDoNotMarketCID).set(hasNoMarketingMessage);
  },
  showHomeModal (onConfirmModal) {
    ReservationStateTree.select(ReservationCursors.homeModal).set('modal', true);
    ReservationStateTree.select(ReservationCursors.homeModal).set('onConfirmModal', onConfirmModal);
  },
  getDefaultAmount () {
    return ReservationStateTree.select(ReservationCursors.defaultAmount).get();
  },
  showAgePolicyModal (bool) {
    ReservationStateTree.select(ReservationCursors.agePolicyModal).set(bool);
  },
  getIsModify() {
    return ReservationStateTree.select(ReservationCursors.modify).get();
  },
  getChargeType () {
    return ReservationStateTree.select(ReservationCursors.chargeType).get()
  },
  getProfile () {
    return ReservationStateTree.select(ReservationCursors.profile).get();
  },
  getSession () {
    return ReservationStateTree.select(ReservationCursors.reservationSession).get();
  }
};


export default ReservationActions;
