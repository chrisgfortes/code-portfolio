import ReservationCursors from '../cursors/ReservationCursors';

const ExistingReservationActions = {
  set: {
    existingReservations(reservations) {
      ReservationStateTree
        .select(ReservationCursors.existingReservations)
        .set("reservations", reservations);
    },
    reservationSearchVisibility(bool) {
      ReservationStateTree.select(
        ReservationCursors.existingReservationsSearch
      ).set("visibility", bool);
    },
    myTrips(myTrips) {
      ReservationStateTree.select(ReservationCursors.existingReservations).set(
        "myTrips",
        myTrips
      );
    },
    myTripsOfType(trips, type, more) {
      if (more) {
        let previousTrips = ReservationStateTree.select(
          ReservationCursors.myTrips
        ).get(type);
        trips = previousTrips.concat(trips);
      }
      ReservationStateTree.select(ReservationCursors.myTrips).set(type, trips);
    },
    myTripsLoadingOfType(loadingClass, type) {
      ReservationStateTree.select(ReservationCursors.myTripsLoadingType).set(
        type,
        loadingClass
      );
    },
    loadingState(bool) {
      ReservationStateTree.select(ReservationCursors.existingReservations).set(
        "loading",
        bool
      );
    },
    moreMyTripsOfType(bool, type) {
      ReservationStateTree.select(ReservationCursors.myTripsMore).set(
        type,
        bool
      );
    },
    startRecordOfType(current, type) {
      ReservationStateTree.select(ReservationCursors.myTripsStart).set(
        type,
        current
      );
    },
    rentalDetailsModal(bool) {
      ReservationStateTree.select(ReservationCursors.rentalDetailsModal).set(
        bool
      );
    }
  },
  get: {
    reservationHasLength() {
      return ReservationStateTree.select(
        ReservationCursors.existingReservationsReservations
      ).get().length;
    },
    startRecordOfType(type) {
      return ReservationStateTree.select(ReservationCursors.myTripsStart).get(
        type
      );
    },
    supportPhoneNumberOfType: function(type) {
      let session = ReservationStateTree.select(
          ReservationCursors.reservationSession
        ).get(),
        supportLinks = session.supportLinks,
        phoneObj = null;

      if (supportLinks) {
        supportLinks.support_phone_numbers.forEach(phone => {
          if (phone.phone_type === type) {
            phoneObj = phone;
          }
        });
      }
      return phoneObj;
    },
    employeeNumber(){
      return ReservationStateTree.select(
        ReservationCursors.employeeNumber
      ).get();
    },
    fedexRequisitionNumber(){
      return ReservationStateTree.select(
        ReservationCursors.fedexRequisitionNumber
      ).get();
    }
  },
  update: {
    resetMyTrips() {
      const updateObj = {
              myTrips: {
                $set: {
                  startRecord: {
                    UPCOMING: 0,
                    CURRENT: 0,
                    PAST: 0
                  },
                  more: {
                    UPCOMING: false,
                    CURRENT: false,
                    PAST: false
                  },
                  loading: {
                    UPCOMING: false,
                    CURRENT: false,
                    PAST: false
                  },
                  UPCOMING: [],
                  CURRENT: [],
                  PAST: []
                }
              }
            };

      ReservationStateTree.select(
        ReservationCursors.existingReservations
      ).update(updateObj);
    }
  }
};

export default ExistingReservationActions;
