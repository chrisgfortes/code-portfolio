import ReservationCursors from '../cursors/ReservationCursors';
import ReservationStateTree from '../stateTrees/ReservationStateTree';

const AccountHelpers = {
  secedeJamaica (subdivisions = []) {
    for (let i = 0, len = subdivisions.length; i < len; i++) {
      if (subdivisions[i].country_subdivision_code === 'JM') {
        subdivisions.splice(i, 1);
        break;
      }
    }
    return subdivisions;
  }
};

const AccountActions = {
  /**
   * @namespace AccountActions.get
   * @memberOf AccountActions
   * @type {Object}
   */
  get: {
    loyaltyType () {
      return ReservationStateTree.select(ReservationCursors.basicProfileLoyalty).get('loyalty_program');
    },
    createPassword() {
      return ReservationStateTree.select(ReservationCursors.createPassword).get();
    },
    loggedIn () {
      return ReservationStateTree.select(ReservationCursors.userLoggedIn).get();
    },
    profileModel () {
      // @todo consider wrapping result in ProfileFactory.toState() ???
      return ReservationStateTree.select(ReservationCursors.profile).get();
    },
    personalModel () {
      return ReservationStateTree.select(ReservationCursors.personal).get();
    },
    accountModel () {
      return ReservationStateTree.select(ReservationCursors.account).get();
    },
    driverInfoModel () {
      return ReservationStateTree.select(ReservationCursors.driverInfo).get();
    },
    licenseProfile () {
      return ReservationStateTree.select(ReservationCursors.licenseProfile).get();
    },
    dnrFlag () {
      let dnr = AccountActions.get.licenseProfile();
      return _.get(dnr, 'do_not_rent_indicator');
    },
    basicProfileCid () {
      let customerDetails = ReservationStateTree.select(ReservationCursors.basicCustomerDetails).get();
      return _.get(customerDetails, 'contract_name') || '';
    },
    basicProfileCidNumber () {
      let customerDetails = ReservationStateTree.select(ReservationCursors.basicCustomerDetails).get();
      return _.get(customerDetails, 'contract_number') || '';
    }
  },
  // set: @todo set up "set" namespace

  /**
   * @namespace  {object} set for setter actions
   */
  set: {
    profileModel(model) {
      // this.logger.log('set.profileModel(model)', model);
      ReservationStateTree.select(ReservationCursors.userProfile).set(model);
    }
  },

  /**
   * called by SessionController when session updates. This and the other Loyalty items in this file need work.
   * @param {object} loyaltyObject
   */
  setLoyaltyBasicsFromSession (loyaltyObject) {
    // console.warn('setLoyaltyBasicsFromSession()', loyaltyObject);
    ReservationStateTree.select(ReservationCursors.loyaltyBook).set(loyaltyObject);
  },

  setAccountTab (tab) {
    ReservationStateTree.select(ReservationCursors.account).set('tab', tab);
  },
  setModifyPayment (payment) {
    ReservationStateTree.select(ReservationCursors.account).set('modifyPayment', payment);
  },
  setPangui (toggle) {
    console.debug('Supposed to NOW WE ARE TOGGLING PANGUI FLAG toggle setPangui(toggle):', toggle);
    ReservationStateTree.select(ReservationCursors.account).set('pangui', toggle);
    ReservationStateTree.select(ReservationCursors.paymentMethods).set('pangui', toggle);
    ReservationStateTree.select(ReservationCursors.paymentMethods).set('context', enterprise.currentView);
  },
  setAccountEditModal (modal) {
    ReservationStateTree.select(ReservationCursors.account).set('editModal', modal);
  },
  setCountries (countries) {
    ReservationStateTree.select(ReservationCursors.account).set('countries', countries);
  },
  setSubdivisions (subdivisions) {
    AccountHelpers.secedeJamaica(subdivisions);
    ReservationStateTree.select(ReservationCursors.account).set('subdivisions', subdivisions);
  },
  setIssueCountrySubdivisions (subdivisions) {
    AccountHelpers.secedeJamaica(subdivisions);
    ReservationStateTree.select(ReservationCursors.account).set('issueCountrySubdivisions', subdivisions);
  },
  cleanFormData (rawFormData) {
    const formData = $.extend(true, {}, rawFormData);

    $.each(formData, function (key, value) {
      if (value === '' || value === null) {
        delete formData[key];
      }
    });

    //delete formData['session'];

    return formData;
  },
  setEditModalNickname (nickname) {
    if (typeof nickname === 'string' && nickname.length === 0) {
      ReservationStateTree.select(ReservationCursors.editCardOrBIllingErrors).set([{
        defaultMessage: 'Nickname cannot be blank',
        code: 'ERROR_NICKNAME_LENGTH'
      }]);
    } else {
      ReservationStateTree.select(ReservationCursors.editCardOrBIllingErrors).set(null);
    }

    ReservationStateTree.select(ReservationCursors.modifyPayment).set('alias', nickname);
  },
  // @todo: compare with set.profileModel above
  setProfile (profile) {
    ReservationStateTree.select(ReservationCursors.profile).set(profile);
  },
  setUpdateExpirationDate(apiDate){
    return ReservationStateTree
            .select(ReservationCursors.modifyPayment)
            .set('expiration_date', apiDate);
  }
};

module.exports = AccountActions;
