import ReservationCursors from '../cursors/ReservationCursors';
import ReservationStateTree from '../stateTrees/ReservationStateTree';

const CancellationActions = {
  setModifyCancelModal ( bool ) {
    ReservationStateTree.select(ReservationCursors.viewModifyCancel).set('modal', bool);
  },

  setCancelDetail (details) {
    ReservationStateTree.select(ReservationCursors.cancellationDetails).set(details);
  },

  getRetrieveAndCancel () {
    return ReservationStateTree.select(ReservationCursors.viewCancel).get('retrieveAndCancel');
  }
};

module.exports = CancellationActions;
