/**
 * @module LocationActions
 * Actions specific to Location Entities
 * @see LocationSearchActions for Location Search steps related actions
 */
import ReservationCursors from '../cursors/ReservationCursors';
import ReservationActions from './ReservationActions';
import { LOCATION_SEARCH } from '../constants';

import { debug } from '../utilities/util-debug';

const logger = debug({name: 'LocationActions', isDebug: false}).logger;
const TYPES = LOCATION_SEARCH.STRICT_TYPE;

/**
 * @module LocationActions
 * @type {Object}
 */
const LocationActions = {
  /**
   * Get pickup/dropoff location
   * @param {string} type pickup/dropoff
   * @return {object} location
   */
  getLocation(type) {
    return ReservationStateTree.select(ReservationCursors[type + 'Target']).get();
  },
  /**
   * Save location input searches data
   * @param  {object} obj data returned from getLocations, usually ReservationFlowModelController
   * @return {void}
   * @todo @gbov2 determine if the LocationSearchResults path is viable
   */
  saveGetLocations(obj) {
    logger.log('saveGetLocations(locationObject)', obj);
    ReservationActions.updateModel(obj);
    // experimental StateTree.js
    ReservationStateTree.select(ReservationCursors.LocationSearchResults).update(obj);
  },

  /**
   * @function getLocationDateTimeData
   * @memberOf LocationActions
   * used by LocationConroller to prepare getDateTimeValidity
   * @param  {string} type string direction, pickup, dropoff, etc.
   * @return {object}      object data
   */
  getLocationDateTimeData(type) {
    const date = ReservationStateTree.select(ReservationCursors[type + 'Date']).get();
    const time = ReservationStateTree.select(ReservationCursors[type + 'TimeValue']).get();
    const sameLocation = ReservationStateTree.select(ReservationCursors.sameLocation).get();
    const location = ReservationStateTree.select(ReservationCursors[(sameLocation ? 'pickup' : type) + 'Target']).get();
    return {
      date,
      time,
      sameLocation,
      location
    }
  },

  /**
   * @function getLocationsSelected
   * @param  {Boolean} get : should we get the cursor result object or value
   * @return {object}      : object with dropoff or pickup
   */
  getLocationsSelected(get = false) {
    let d = ReservationStateTree.select(ReservationCursors.dropoffLocationObj);
    let p = ReservationStateTree.select(ReservationCursors.pickupLocationObj);
    return {
      dropoff: (get) ? d.get() : d,
      pickup: (get) ? p.get() : p
    }
  },

  /**
   * @function getLocationSelectTypes
   * @memberOf LocationActions
   * @return {object} dropoff and pickup location objects
   */
  getLocationSelectTypes() {
    return {
      dropoffType: LocationActions.getLocationsSelected().dropoff.get('locationType'),
      pickupType: LocationActions.getLocationsSelected().pickup.get('locationType')
    }
  },

  /**
   * @function setGlobalLocationDetail
   * Set a value for enterprise.locationDetail
   * @param {object} resultItem
   * @todo: This should be somewhere else. We should have a way to update globals or something like module/Enterprise
   * I don't like us hitting a global directly and outright like that
   */
  setGlobalLocationDetail(resultItem) {
    logger.log('moved from ReservationFlowModelController');
    logger.warn('setGlobalLocationDetail(resultItem) should use factory / classes', resultItem);
    enterprise.locationDetail = {
      countryCode: resultItem.countryCode,
      lat: resultItem.latitude,
      'long': resultItem.longitude,
      locationName: resultItem.name,
      type: TYPES.BRANCH,
      locationType: TYPES.BRANCH,
      locationId: enterprise.branchID
    };
  },
  /**
   * Sets the location object coordinates.
   *
   * @param      {string}  type    pickup|dropoff|etc.
   * @param      {LocationCoordinatesModel}  data    Baobab update object with lat and longitude specified
   */
  setLocationObjCoordinates (type, data) {
    ReservationStateTree.select(ReservationCursors[type + 'LocationObj']).update(data);
  },
  /**
  * Sets location object for featured city map
  * @param {string} type pickup/dropoff
  * @param {object} pickup/dropoff location
  */
  setLocationObject (type, obj) {
    ReservationStateTree.select(ReservationCursors[type + 'LocationObj']).set(obj);
  }
}

module.exports = LocationActions;
