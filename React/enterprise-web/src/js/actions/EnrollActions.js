import ReservationCursors from '../cursors/ReservationCursors';

const AccountActions = {
  setEnrollSection (section) {
    ReservationStateTree.select(ReservationCursors.enroll).set('section', section);
  },
  setSuccessID (ID) {
    ReservationStateTree.select(ReservationCursors.enroll).set('successID', ID);
  },
  setEnrollModal (state) {
    ReservationStateTree.select(ReservationCursors.enroll).set('modal', state);
  },
  setIssueCountry (data) {
    ReservationStateTree.select(ReservationCursors.enroll).set('issueCountry', data);
  },
  setOriginCountry (data) {
    ReservationStateTree.select(ReservationCursors.enroll).set('originCountry', data);
  },
  setEnrollSuccess (success) {
    ReservationStateTree.select(ReservationCursors.enroll).set('success', success);
  },
  mergeProfileDetails (value) { // ECR-14250
    ReservationStateTree.select(ReservationCursors.enrollProfile).merge(value);
  },
  setCreatePasswordDetails (attribute, value) {
    ReservationStateTree.select(ReservationCursors.createPassword).set(attribute, value);
  },
  setCreatePassword (bool) {
    ReservationStateTree.select(ReservationCursors.setPassword).set(bool);
  },
  setEmailModal(bool) {
    ReservationStateTree.select(ReservationCursors.setEmailModal).set(bool);
  },
  setProfileDetails (attribute, value) { // ECR-14250
    ReservationStateTree.select(ReservationCursors.enrollProfile).set(attribute, value);
  },
  // ECR-14250
  setSuccessProfile (value) {
    ReservationStateTree.select(ReservationCursors.enroll).set('profile', value);
  },
  setMemberid (bool) {
    ReservationStateTree.select(ReservationCursors.enroll).set('useMemberid', bool);
  },
  getCountryCode () {
    return ReservationStateTree.select(ReservationCursors.pickupCountryCode).get();
  },
  getCountriesList () {
    return ReservationStateTree.select(ReservationCursors.account).get().countries;
  },
  getEnrollProfile () { // ECR-14250
    return ReservationStateTree.select(ReservationCursors.enrollProfile).get();
  },
  getEnroll () {
    return ReservationStateTree.select(ReservationCursors.enroll).get();
  }
};

module.exports = AccountActions;

