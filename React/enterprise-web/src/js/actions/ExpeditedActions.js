import ReservationCursors from '../cursors/ReservationCursors';
import AccountActions from './AccountActions';
import {
  getClearExpedited,
  getPersonalMapper,
  getProfileMapper
} from '../factories/User';

const ExpeditedActions = {
  get: {
    restartStatus() {
      return ReservationStateTree.select(ReservationCursors.expedited).get().restart;
    },
    // @todo use ReservationActions.getSession() instead
    reservationSession() {
      return ReservationStateTree.select(ReservationCursors.reservationSession).get();
    },
    currentStep(){
      return ReservationStateTree.get().view.componentToRender;
    },
    isDeepLink(){
      return ReservationStateTree.select(ReservationCursors.deepLinkReservation).get();
    },
    createdPasswordModel(){
      return ReservationStateTree.select(ReservationCursors.createPassword).get();
    },
    expeditedModel(){
      return ReservationStateTree.select(ReservationCursors.expedited).get();
    },
    sessionExpedited() {
      return ReservationStateTree.select(ReservationCursors.sessionExpedited).get();
    },
    personalModel(){
      return ReservationStateTree.select(ReservationCursors.personal).get();
    },
    enrollModel(){
      return ReservationStateTree.select(ReservationCursors.enroll).get();
    }
  },
  set: {
    clearProfile() {
      let profileMapper = getClearExpedited();
      ReservationStateTree.select(ReservationCursors.expedited).update(profileMapper);
    },
    clearLicense() {
      ReservationStateTree.select(ReservationCursors.expedited).set('license', null);
    },
    input(variable, value){
      // console.warn('ExpeditedActions.input(variable, value)', variable, value);
      ReservationStateTree.select(ReservationCursors.expedited).set(variable, value);
    },
    enroll(profile) { // ECR-14250
      ReservationStateTree.select(ReservationCursors.enrollProfile).set('profile', profile);
    },
    expeditedFields() {
      let personalField = AccountActions.get.personalModel();
      let account = AccountActions.get.accountModel();
      let profile = AccountActions.get.profileModel();

      // console.warn('ExpeditedActions.expeditedFields()', personalField, account, profile);

      if (profile) {
        let setCountryResidence = false;
        let setCountryIssue = false;
        let countryCode = _.get(profile, 'address_profile.country_code');
        for (let i = 0, len = account.countries.length; i < len; i++) {
          if (account.countries[i].country_code === countryCode) {
            ExpeditedActions.set.input('countryResidence', account.countries[i]);
            setCountryResidence = true;
          }
          if (account.countries[i].country_code === profile.license_profile.country_code) {
            ExpeditedActions.set.input('countryIssue', account.countries[i]);
            setCountryIssue = true;
          }
          if (setCountryResidence && setCountryIssue) {
            break;
          }
        }

        // ECR-14250 model?
        let personalMapper = getPersonalMapper(personalField, profile);

        let profileMapper = getProfileMapper(profile);

        ReservationStateTree.select(ReservationCursors.personal).update(personalMapper);
        ReservationStateTree.select(ReservationCursors.expedited).update(profileMapper);
      }
    }
  }
}

module.exports = ExpeditedActions;

