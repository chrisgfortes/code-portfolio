import ReservationCursors from '../cursors/ReservationCursors';
import ReservationStateTree from '../stateTrees';

import { hasProtocol } from '../utilities/util-url';

const JSONtoQueryString = (data) => (
  Object
    .keys(data)
    .map((prop) => `${prop}=${data[prop]}`)
    .join('&')
);

const RedirectActions = {
  get: {
    preferredLanguage() {
      return ReservationStateTree.select(ReservationCursors.redirectPreferredLang).get();
    },
    type() {
      return ReservationStateTree.select(ReservationCursors.redirectType).get();
    },
    country() {
      return ReservationStateTree.select(ReservationCursors.redirectCountry).get();
    },
    destinationLang() {
      return ReservationStateTree.select(ReservationCursors.redirectDestinationLang).get();
    }
  },
  set: {
    preferredLanguage(v) {
      ReservationStateTree.select(ReservationCursors.redirectPreferredLang).set(v);
    },
    type(v) {
      ReservationStateTree.select(ReservationCursors.redirectType).set(v);
    },
    country(v) {
      ReservationStateTree.select(ReservationCursors.redirectCountry).set(v);
    },
    destinationLang(v) {
      ReservationStateTree.select(ReservationCursors.redirectDestinationLang).set(v);
    },
    modal(bool) {
      ReservationStateTree.select(ReservationCursors.redirectModal).set(bool)
    }
  },

  goDomainRedirect (redirectUrl, internalUrl) {
    if(hasProtocol(redirectUrl) || internalUrl) {
      window.location.href = redirectUrl;
    } else {
      window.location.href = window.location.protocol + '//' + redirectUrl;
    }
  },
  goBrowserRedirect (url, replace) {
    if (enterprise.currentView !== enterprise.settings.viewBrowserNotice) {
      if (replace) {
        window.location.replace(url);
      } else {
        window.location.href = url;
      }
    }
  },
  go (inputHref = '') {
    const href = inputHref.replace(enterprise.aem.path, '').replace('.html', '') + '.html';
    window.location = `${enterprise.aem.path}/${href}`;
  },
  goToReservationStep (inputHash = 'book') {
    const hash = '#' + inputHash.replace('#', '');

    enterprise.pageTitleOverride = null;
    if (enterprise.aem.reservePath) {
      window.location = enterprise.aem.reservePath + hash;
    } else if (enterprise.currentView === 'reserve') {
      window.location.hash = hash;
    } else {
      window.location = enterprise.aem.defaultReservePath + hash;
    }
  },
  goToHash(hash = ''){
    window.location.hash = hash;
  },
  goToDeepLink (inputData = {}) {
    const data = Object.assign({ v: 2 }, inputData);
    window.location = `${enterprise.aem.path}/deeplink.html?${JSONtoQueryString(data)}`;
  },
  goHome () {
    if (enterprise.aem.homePath) {
      // ECR-12487
      window.location = enterprise.aem.homePath;
    } else {
      const localHost = document.location.hostname === 'localhost';
      window.location = localHost ? enterprise.aem.path + '/home.html' : enterprise.aem.path + '.html';
    }
  }
};

module.exports = RedirectActions;
