const PriceActions = {
  formatRatePrice (extra) {
    const rateTypeMap = {
      'HOUR': enterprise.i18nReservation.resflowcarselect_0071,
      'HOURLY': enterprise.i18nReservation.resflowcarselect_0071,
      'DAY': enterprise.i18nReservation.resflowcarselect_0072,
      'DAILY': enterprise.i18nReservation.resflowcarselect_0072,
      'WEEKLY': enterprise.i18nReservation.resflowcarselect_0073,
      'MONTHLY': enterprise.i18nReservation.resflowcarselect_0074,
      'RENTAL': enterprise.i18nReservation.resflowextras_9008
    };
    let returnValue = null;
    if (extra && extra.rate_amount_view && extra.rate_amount_view.format) {
      if (rateTypeMap[extra.rate_type]) {
        returnValue = rateTypeMap[extra.rate_type].replace('#{rate}', extra.rate_amount_view.format);
      }
    }
    return returnValue;
  }
};

module.exports = PriceActions;
