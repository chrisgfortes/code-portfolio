/**
 * Actions to interact with the PaymentMethods Cursors etc.
 * Works with PaymentServices.js and PaymentModelController.js
 * Consolidate Accounts and Verification Payment Methods...
 * As is convention foo"Actions" talk to the model layer/state tree/cursors.
 *
 * This consolidation is a work in progress -- and incomplete as of 2016-10-03 / ECR-22
 * Note: There are tickets in Sprint 2 of 1.8 that will cover more of this.
 */
import ReservationCursors from '../cursors/ReservationCursors';
import AccountActions from './AccountActions';
import ErrorActions from './ErrorActions';
import VerificationActions from './VerificationActions';

const PaymentMethodActions = {
  getChargeType () {
    return ReservationStateTree.select(ReservationCursors.chargeType).get();
  },
  /**
   * Toggle that the processor got the card information.
   * It's important to note that doesn't always mean that the process is finished, hence the
   * two-stage process (registerCardSuccess + addCardSuccess)
   * @param {string} token
   */
  saveProcessorID (token) {
    ReservationStateTree.select(ReservationCursors.paymentMethods).set('id', token);
    ReservationStateTree.select(ReservationCursors.paymentMethods).set('registerCardSuccess', true);
  },
  /**
   * When we go to add a card, we want a clean slate. These may be the defaults, but
   * the user may be opening the modal again so we can reset.
   */
  resetCardStatus () {
    ReservationStateTree.select(ReservationCursors.paymentMethods).set('registerCardSuccess', false);
    ReservationStateTree.select(ReservationCursors.paymentMethods).set('addCardSuccess', false);
    ReservationStateTree.select(ReservationCursors.paymentMethods).set('responseData', {});
    ReservationStateTree.select(ReservationCursors.paymentMethods).set('id', null);
  },
  /**
   * Actually save the data received from the card processor to the cursor we use to add
   * the payment information to the user's account.
   * @param {object} obj
   */
  saveProcessorResponse (obj) {
    ReservationStateTree.select(ReservationCursors.paymentMethods).set('responseData', obj);
  },
  /**
   * Update the user's payment profile with the payment methods we got back.
   * @param {Array} data
   */
  updateProfilePaymentMethods (data) {
    // console.debug('updating payment_methods on profile:', data, 'PaymentMethodActions.js');
    ReservationStateTree.select(ReservationCursors.profilePaymentMethods).set(data);
  },
  /**
   * Toggle a view state property outside of React that we can reference inside of React.
   * "Submitting" is just use posting to our server once we heard back.
   * @param {boolean} state
   */
  setLoadingState (state) {
    ReservationStateTree.select(ReservationCursors.paymentMethods).set('submitting', state);
  },
  /**
   * Once "registerCardSuccess" has been fired after we get the response from the payment processor
   * we save the data (elsewhere) and then tell the front-end the card was successfully added.
   * @param {boolean} added
   */
  addCardSuccess ( added = false ) {
    let cardSuccess = ReservationStateTree.select(ReservationCursors.paymentMethods).get('registerCardSuccess');
    // @todo: ECR-22 and after: this will need a better set of conditions
    if (cardSuccess && added) {
      // @todo: ECR-22 and after: at some point this will have to be either
      // (1) the AccountActions Modal or
      // (2) the Verification Prepay Modal
      AccountActions.setAccountEditModal(null);
      ReservationStateTree.select(ReservationCursors.paymentMethods).set('cardModal', false);
      ReservationStateTree.select(ReservationCursors.paymentMethods).set('addCardSuccess', true);
    } else {
      console.debug('addCardSuccess truth test failed. PaymentMethodActions.js');
    }
  },
  /**
   * Store errors in view state so React may reference it.
   * NOTE: I don't think this is correct usage, and we aren't even using it (?)
   * @param {boolean|null} tf
   */
  setErrors (tf) {
    ReservationStateTree.select(ReservationCursors.paymentMethods).set('errors', tf);
  },
  /**
   * Set RegisterCardSuccess status
   * @param {boolean}
   */
  setRegisterCardSuccess(bool) {
    ReservationStateTree.select(ReservationCursors.verification).set('registerCardSuccess', bool);
  },
  setThreeDSToken () {
    ReservationStateTree.select(ReservationCursors.threeDS).set('token', null);
  },
  setCardModal () {
    ReservationStateTree.select(ReservationCursors.verification).set('cardModal', false);
  },
  getRegisterCardSuccess () {
    return ReservationStateTree.select(ReservationCursors.paymentMethods).select('registerCardSuccess');
  },
  getPayload () {
    return ReservationStateTree.select(ReservationCursors.paymentMethods).get('responseData');
  },
  getModifyPayment () {
    return ReservationStateTree.select(ReservationCursors.modifyPayment).get();
  },
  getPaymentErrors () {
    return ReservationStateTree.select(ReservationCursors.paymentMethods).get('errors');
  },
  getSavePaymentForLater () {
    return ReservationStateTree.select(ReservationCursors.model).get('savePaymentForLater');
  },
  getIsExpedited () {
    return ReservationStateTree.select(ReservationCursors.model).get('expedited');
  },
  getPangui () {
    return ReservationStateTree.select(ReservationCursors.paymentProcessor).get();
  },
  selectRegisterCardSuccess() {
    return ReservationStateTree.select(ReservationCursors.verification).select('registerCardSuccess');
  },
  getIsLoggedIn () {
    return ReservationStateTree.select(ReservationCursors.reservationSession).get('loggedIn');
  },

  // moved from VerificationService
  setPrepayError (messages, component) {
    ErrorActions.setErrorsForComponent(messages, component);
    VerificationActions.setPrepayModal(false);
  },
  setPrepayCardRegSuccess () {
    ReservationStateTree.select(ReservationCursors.reservationSession).set('prepayCardRegistrationSuccessful', false);
  },
  // moved from VerificationService
  clearPrepayError () {
    ErrorActions.clearErrorsForComponent('verification');
  }

};

module.exports = PaymentMethodActions;
