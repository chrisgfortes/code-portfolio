import ReservationCursors from '../cursors/ReservationCursors';

import ReservationStateTree from '../stateTrees/ReservationStateTree';

// import { debug } from '../utilities/util-debug';
// const logger = debug({isDebug: false, name: 'ResInitiateActions'}).logger;

const ResInitiateActions = {
  getAdditionalInfo() {
    return ReservationStateTree.select(ReservationCursors.additionalInfo).get();
  },
  getCid () {
    let resRequest = ReservationStateTree.select(ReservationCursors.resRequest).get();
    return _.get(resRequest, 'contract_number') || '';
  },
  getInitData() {
    let resSession = ReservationStateTree.select(ReservationCursors.reservationSession).get();

    return {
      // removed from this call because the data needed to be updated and called again but wasn't
      // even needed before being updated ...
      // additionalInformation : ReservationStateTree.select(ReservationCursors.additionalInfo).get(),
      renterAge : ReservationStateTree.select(ReservationCursors.age).get(),
      contractDetails : ReservationStateTree.select(ReservationCursors.contractDetails).get(),
      costCenterValue : ReservationStateTree.select(ReservationCursors.costCenter).get(),
      coupon : ReservationStateTree.select(ReservationCursors.coupon).get(),
      employeeNumber : ReservationStateTree.select(ReservationCursors.employeeNumber).get(),
      loyaltyBook : ReservationStateTree.select(ReservationCursors.loyaltyBook).get(),
      dropoffDate : ReservationStateTree.select(ReservationCursors.dropoffDate).get(),
      dropoffLocationObj : ReservationStateTree.select(ReservationCursors.dropoffLocationObj).get(),
      dropoffTime : ReservationStateTree.select(ReservationCursors.dropoffTimeValue).get(),
      pickupDate : ReservationStateTree.select(ReservationCursors.pickupDate).get(),
      pickupLocationObj : ReservationStateTree.select(ReservationCursors.pickupLocationObj).get(),
      pickupTime : ReservationStateTree.select(ReservationCursors.pickupTimeValue).get(),
      pin : ReservationStateTree.select(ReservationCursors.pin).get(),
      preSelectedCarClass : ReservationStateTree.select(ReservationCursors.preSelectedCarClass).get(),
      queryStringData : ReservationStateTree.select(ReservationCursors.queryStringData).get(),
      sameLocation : ReservationStateTree.select(ReservationCursors.sameLocation).get(),
      // session : resSession,
      purpose : resSession.travel_purpose ? resSession.travel_purpose : ReservationStateTree.select(ReservationCursors.model).get().purpose,
      fedexSequence: ReservationStateTree.select(ReservationCursors.fedexSequence).get()
    }
  }
}

export default ResInitiateActions;
