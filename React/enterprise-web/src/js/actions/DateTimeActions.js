/**
 * @module DateTimeActions
 * Actions against state that have to do with Dates and Times
 */
import ReservationCursors from '../cursors/ReservationCursors';
import EcomError from '../classes/EcomError';
import { LOCATION_SEARCH } from '../constants';
import { debug } from '../utilities/util-debug';

const logger = debug({name: 'DateTimeActions', isDebug: false }).logger;

const DateTimeActions = {
  /**
   * Used by BookingWidgetModelController and LocationFactory to get data necessary for hours call
   * @memberOf DateTimeActions
   * @param  {string} type         pickup/dropoff etc.
   * @param  {string} peopleSoftId location id
   * @return {object}              object to reference
   */
  selectLocationHoursObjects(type, peopleSoftId) {
    return {
      locationId: peopleSoftId || ReservationStateTree.select(ReservationCursors[type + 'Location']).get(),
      locationObj: ReservationStateTree.select(ReservationCursors[type + 'LocationObj']).get(),
      date: ReservationStateTree.select(ReservationCursors[type + 'ViewDate']).get()
    }
  },
  setClosedDates(closedDates, type) {
    let cursor = (type !== LOCATION_SEARCH.DROPOFF) ? ReservationCursors.pickupModelDate : ReservationCursors.dropoffModelDate;
    logger.warn('setClosedDates()', type, cursor, closedDates);
    ReservationStateTree.select(cursor).set('closedDates', closedDates);
  },
  setInvalidTime(type, bool) {
    logger.log('setInvalidTime()');
    ReservationStateTree.select(ReservationCursors.locationSelect.concat([type])).set('invalidTime', bool);
  },
  setInvalidDate(type, bool) {
    logger.log('setInvalidDate()');
    ReservationStateTree.select(ReservationCursors.locationSelect.concat([type])).set('invalidDate', bool);
  },
  setInvalidDateTime(type, bool) {
    logger.log('setInvalidDateTime()');
    ReservationStateTree.options.autoCommit = false;

    DateTimeActions.setInvalidTime(type, bool);
    DateTimeActions.setInvalidDate(type, bool);

    ReservationStateTree.commit();
    ReservationStateTree.options.autoCommit = true;
  },
  setSameLocationInvalidDateTime(bool) {
    logger.log('setSameLocationInvalidDateTime()');
    ReservationStateTree.options.autoCommit = false;

    DateTimeActions.setInvalidTime(LOCATION_SEARCH.PICKUP, bool);
    DateTimeActions.setInvalidTime(LOCATION_SEARCH.DROPOFF, bool);
    DateTimeActions.setInvalidDate(LOCATION_SEARCH.PICKUP, bool);
    DateTimeActions.setInvalidDate(LOCATION_SEARCH.DROPOFF, bool);

    ReservationStateTree.commit();
    ReservationStateTree.options.autoCommit = true;
  },
  setValidHours(type, hours) {
    logger.log('setValidHours()');
    ReservationStateTree.select(ReservationCursors.locationSelect.concat([type])).set('validHours', hours);
  },
  setWeeklyHours(type, value) {
    logger.log('setWeeklyHours()');
    ReservationStateTree.select(ReservationCursors[type + 'Target']).update({
      weeklyHours: {
        $set: value
      }
    });
  },
  setSelectedDate(type, value) {
    logger.log('setSelectedDate()');
    ReservationStateTree.select(ReservationCursors[type + 'Target']).update({
      selectedDate: {
        $set: value
      }
    });
  },
  getPickupDate() {
    return ReservationStateTree.select(ReservationCursors.pickupDate).get();
  },
  getDropoffDate() {
    return ReservationStateTree.select(ReservationCursors.dropoffDate).get();
  },
  getInvalidDate() {
    return ReservationStateTree.select(ReservationCursors.invalidDate).get();
  },
  /**
   * @memberOf DateTimeActions
   * @function parseDateTimeValidity
   * Used by LocationController.getDateTimeValidity()
   * @param  {string} responseData validity strings from server
   * @return {void}              just sets invalid/valid times/dates
   */
  parseDateTimeValidity(responseData, type, hours) {
    if (!responseData || !responseData.validityType) {
      return false;
    }
    logger.log('parseDateTimeValidity() USE FACTORY CLASSES', responseData, type, hours);
    let resValidityType = responseData.validityType;
    let resHours = responseData.locationHours;
    switch (resValidityType) {
      case LOCATION_SEARCH.VALIDITY.VALID_STANDARD_HOURS:
        DateTimeActions.setInvalidDate(type, false);
        DateTimeActions.setInvalidTime(type, false);
        break;
      case LOCATION_SEARCH.VALIDITY.VALID_AFTER_HOURS:
        DateTimeActions.setInvalidDate(type, false);
        DateTimeActions.setInvalidTime(type, false);
        break;
      case LOCATION_SEARCH.VALIDITY.INVALID_ALL_DAY:
        DateTimeActions.setInvalidDate(type, true);
        DateTimeActions.setValidHours(type, null);
        break;
      case LOCATION_SEARCH.VALIDITY.INVALID_AT_THAT_TIME:
        DateTimeActions.setValidHours(type, resHours);
        DateTimeActions.setInvalidDate(type, false);
        DateTimeActions.setInvalidTime(type, true);
        break;
      default:
        throw new EcomError(resValidityType + ': is not a valid location validity type.');
    }
  }
}

module.exports = DateTimeActions;
