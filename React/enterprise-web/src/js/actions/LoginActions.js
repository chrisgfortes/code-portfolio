import ReservationCursors from '../cursors/ReservationCursors';

const LoginActions = {
  setLoginWidgetActive: function (state) {
    ReservationStateTree.select(ReservationCursors.loginWidget).set('active', state);
  },
  setLoginModal(state) {
    ReservationStateTree.select(ReservationCursors.loginWidget).set('modal', state);
  },
  setTerms(terms){
    ReservationStateTree.select(ReservationCursors.loginWidget).set('terms', terms);
  },
  /**
   * Sets the login credentials.
   * @function   LoginActions.setLoginCredentials
   * @param      {object}  obj     The baobab update object
   */
  setLoginCredentials(obj) {
    ReservationStateTree.select(ReservationCursors.login).update(obj);
  },
  isUserLoggedIn () {
    return !!ReservationStateTree.select(ReservationCursors.userLoggedIn).get();
  },
  getLoggedInCursor () {
    return ReservationStateTree.select(ReservationCursors.userLoggedIn);
  },
  setLogoutModal(bool) {
    ReservationStateTree.select(ReservationCursors.logout).set('modal', bool);
  },
  getCallbackAfterAuthenticate () {
    return ReservationStateTree.select(ReservationCursors.callbackAfterAuthenticate).get();
  },
  setCallbackAfterAuthenticate (data) {
    return ReservationStateTree.select(ReservationCursors.callbackAfterAuthenticate).set(data);
  },
  getSavePaymentForLater () {
    return ReservationStateTree.select(ReservationCursors.model).get('savePaymentForLater');
  },

  /**
   * @namespace  {object}  LoginActions.set
   * @param      {boolean}  bool  set logged in or logged out flag
   */
  set: {
    loggedInState (bool) {
      ReservationStateTree.select(ReservationCursors.userLoggedIn).set(bool)
    }
  }
};

module.exports = LoginActions;
