import ReservationCursors from '../cursors/ReservationCursors';

export const TimePickerActions = {
  setTimeArray (type) {
    let tree = ReservationStateTree.get(),
      params = {
        type,
        closedTimes: tree.model[type].time.closed,
        afterHour: tree.model[type].afterHour.allowed
      },
      timeArray = TimePickerHelpers.generateTimeArray(params);

    ReservationStateTree.select(ReservationCursors[type + 'Time']).set('array', timeArray);
  }
};

export const TimePickerHelpers = {
  generateTimeArray (params) {

    var closedTimes = params.closedTimes,
      allowAfterHour = params.afterHour,
      timeArray = [];

    for (let i = 0; i < 48; i++) {
      let hour = Math.floor(i / 2),
        minutes = (i % 2 === 0) ? '00' : '30';
      timeArray.push({
        state: null,
        time: moment(hour + ':' + minutes, 'H:mm')
      });

    }

    if (closedTimes && closedTimes.length > 0) {
      timeArray = this.setTimeArraySegments(timeArray, closedTimes, allowAfterHour);
    }

    return timeArray;
  },
  setTimeArraySegments (timeArray, closedTimes, afterHour) {
    let openCloseTimes = [],
      dropoffAfterHour = afterHour;

    if (closedTimes.length === 1) {
      if (closedTimes[0].open === '0' && closedTimes[0].close === '0') {
        closedTimes = [{
          open: '2359',
          close: '0000'
        }];
      }
    }

    for (let i = 0, len = closedTimes.length; i < len; ++i) {

      let openTime = moment(closedTimes[i].open, "H:mm"),
        closeTime = closedTimes[i].close === '00:00' ? moment("23:59", "H:mm") : moment(closedTimes[i].close, "H:mm"),
        openTimeIndex = (openTime.minute() >= 30) ? 2 * openTime.hour() + 1 : 2 * openTime.hour(),
        closeTimeIndex = (closeTime.minute() >= 30) ? 2 * closeTime.hour() + 1 : 2 * closeTime.hour();

      for (let j = openTimeIndex, end = closeTimeIndex; j <= end; j++) {
        timeArray[j].state = 'enabled';
      }
      openCloseTimes.push(openTimeIndex, closeTimeIndex);
    }

    if (openCloseTimes.length > 0) {
      openCloseTimes[openCloseTimes.length - 1] < 47 && openCloseTimes.push(48, 0);

      for (let i = 0, len = timeArray.length; i < len; i++) {
        if (timeArray[i].state !== 'enabled') {
          timeArray[i].state = dropoffAfterHour ? 'afterHour' : 'disabled';
        }
      }
    }

    return timeArray;
  }
};
