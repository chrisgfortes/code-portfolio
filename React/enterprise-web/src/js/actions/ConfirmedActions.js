import Images from '../utilities/util-images';
import ReservationCursors from '../cursors/ReservationCursors';
import EnterpriseServices from '../services/EnterpriseServices';
import { SERVICE_ENDPOINTS } from '../constants';

const ConfirmedActions = {
  getConfirmationHeader: () => {
    let session = ReservationStateTree.get(ReservationCursors.reservationSession);
    let details = ReservationStateTree.get(ReservationCursors.selectedCar);

    if (!details || !session) {
      return {
        image: null,
        confirmationNumber: null
      };
    }

    let header = {
      image: Images.getUrl(details.images.ThreeQuarter.path, 480, 'high'),
      confirmationNumber: session.confirmationNumber
    };
    return header;
  },
  getDetailDate: (pickupTime) => {
    let obj = {};

    if (pickupTime) {
      obj.date = i18n('resflowviewmodifycancel_0026', {date: moment(pickupTime).format(enterprise.i18nUnits.shortdateformat)});
    }
    return obj;
  },
  getSummary: () => {
    const session = ReservationStateTree.get(ReservationCursors.reservationSession);
    let summary = {
      time: {
        pickup: '',
        dropoff: ''
      },
      location: {
        pickup: '',
        dropoff: ''
      }
    };
    if (session.pickup_time && session.return_time && session.pickupLocationWithDetail) {
      summary = {
        date: {
          pickup: moment(session.pickup_time).format('ll'),
          dropoff: moment(session.return_time).format('ll')
        },
        time: {
          pickup: moment(session.pickup_time).format(enterprise.i18nUnits.timeformat),
          dropoff: moment(session.return_time).format(enterprise.i18nUnits.timeformat)
        },
        location: {
          pickup: session.pickupLocationWithDetail.name || session.pickupLocationWithDetail.address.street_addresses[0],
          dropoff: (session.returnLocationWithDetail) ? (session.returnLocationWithDetail.name || session.returnLocationWithDetail.address.street_addresses[0]) : (session.pickupLocationWithDetail.name || session.pickupLocationWithDetail.address.street_addresses[0])
        }
      };
    }
    return summary;
  },
  getConfirmationThanks: () => {
    const session = ReservationStateTree.get(ReservationCursors.reservationSession);
    let thanks = {
      header: '',
      headerBody: [],
      secondaryHeader: '',
      body: '',
      locationInfo: '',
      phone: ''
    };
    let component = ReservationStateTree.get(['view', 'componentToRender']);
    let location;
    let phoneNumber;

    if (component === 'Cancelled') {
      location = phoneNumber = session.pickupLocationWithDetail.name || '';
      phoneNumber = session.pickupLocationWithDetail.phones.filter((number) => number.phone_type === 'OFFICE');
      if (phoneNumber && phoneNumber.length) {
        phoneNumber = phoneNumber[0].phone_number;
      }
    }

    switch (component) {
      case 'Cancelled':
        if (session.chargeType === 'PREPAY') {
          const cancellationDetails = ReservationStateTree.select(ReservationCursors.cancellationDetails).get();

          thanks.header = i18n('resflowviewmodifycancel_0048') + ' ' + _.get(cancellationDetails, 'refund_amount.format');
          if (cancellationDetails.refund_amount.amount === session.paymentInformation.amount.amount) {
            thanks.headerBody.push(
              <p>
                {i18n('resflowviewmodifycancel_0049')}
              </p>
            );
          } else {
            thanks.headerBody.push(
              <p>
                {i18n('resflowviewmodifycancel_0059').replace(
                  '#{original_amount}',
                  session.paymentInformation.amount.format).replace('#{$value}', cancellationDetails.fee_amount.format)}
              </p>
            );
          }
        } else {
          thanks.header = i18n('resflowviewmodifycancel_1017');
        }
        thanks.secondaryHeader = i18n('resflowviewmodifycancel_0060');
        thanks.body = i18n('resflowviewmodifycancel_0061');
        thanks.locationInfo = location + ' ' + phoneNumber;
        thanks.phone = phoneNumber;
        break;
      default:
        if (session.driverInformation && session.pickup_time && session.pickupLocationWithDetail) {
          let confirmMessage = i18n('resflowconfirmation_0020');
          thanks.header = confirmMessage.replace('#{firstName}', session.driverInformation.first_name).replace('#{lastName}', session.driverInformation.last_name).replace('#{time}', moment(session.pickup_time, 'YYYYMMDD').format('LL'));
        }
    }
    return thanks;
  },
  getLocationDetails: () => {
    const session = ReservationStateTree.get(ReservationCursors.reservationSession);
    let locationDetails = {};

    let clientId = enterprise.googleMapsClientID || '';
    let channelName = enterprise.googleMapsChannel || '';
    let strWithKeys = '&clientid=' + clientId + '&channel=' + channelName;

    //Please refactor below...
    if (session.returnLocationWithDetail) {
      locationDetails = {
        pickup: {
          name: session.pickupLocationWithDetail.name,
          address: session.pickupLocationWithDetail.address,
          phones: session.pickupLocationWithDetail.phones,
          afterHoursPickup: session.pickupLocationWithDetail.after_hours_pickup,
          hours: session.pickupLocationWithDetail.hours,
          date: moment(session.pickup_time).format(enterprise.i18nUnits.dateformat),
          time: moment(session.pickup_time).format(enterprise.i18nUnits.timeformat),
          businessTypes: session.pickupLocationWithDetail.business_types,
          gps: {
            latitude: session.pickup_location.gps.latitude,
            longitude: session.pickup_location.gps.longitude
          },
          imgSrc: 'https://maps.google.com/maps/api/staticmap?center=' + session.pickup_location.gps.latitude + ',' + session.pickup_location.gps.longitude +
          '&zoom=12&size=400x225&scale=2&&markers=color:0x169a5a%7Clabel:K%7C' + session.pickup_location.gps.latitude + ',' + session.pickup_location.gps.longitude + strWithKeys,
          wayfinding: session.pickupLocationWithDetail.wayfindings && session.pickupLocationWithDetail.wayfindings.length > 0 ? session.pickupLocationWithDetail.wayfindings[0].text : null
        },
        dropoff: {
          name: session.returnLocationWithDetail.name,
          address: session.returnLocationWithDetail.address,
          phones: session.returnLocationWithDetail.phones,
          afterHoursDropoff: session.returnLocationWithDetail.after_hours_return,
          hours: session.returnLocationWithDetail.hours,
          date: moment(session.return_time).format(enterprise.i18nUnits.dateformat),
          time: moment(session.return_time).format(enterprise.i18nUnits.timeformat),
          gps: {
            latitude: session.return_location.gps.latitude,
            longitude: session.return_location.gps.longitude
          },
          imgSrc: 'https://maps.google.com/maps/api/staticmap?center=' + session.return_location.gps.latitude + ',' + session.return_location.gps.longitude +
          '&zoom=12&size=400x225&scale=2&&markers=color:0x169a5a%7Clabel:K%7C' + session.return_location.gps.latitude + ',' + session.return_location.gps.longitude + strWithKeys,
          wayfinding: session.returnLocationWithDetail.wayfindings && session.returnLocationWithDetail.wayfindings.length > 0 ? session.returnLocationWithDetail.wayfindings[0].text : null
        }
      };
    } else {
      locationDetails = {
        pickup: {
          name: session.pickupLocationWithDetail.name,
          address: session.pickupLocationWithDetail.address,
          phones: session.pickupLocationWithDetail.phones,
          afterHoursPickup: session.pickupLocationWithDetail.after_hours_pickup,
          hours: session.pickupLocationWithDetail.hours,
          date: moment(session.pickup_time).format(enterprise.i18nUnits.dateformat),
          time: moment(session.pickup_time).format(enterprise.i18nUnits.timeformat),
          businessTypes: session.pickupLocationWithDetail.business_types,
          gps: {
            latitude: session.pickup_location.gps.latitude,
            longitude: session.pickup_location.gps.longitude
          },
          imgSrc: 'https://maps.google.com/maps/api/staticmap?center=' + session.pickup_location.gps.latitude + ',' + session.pickup_location.gps.longitude +
          '&zoom=12&size=400x225&scale=2&&markers=color:0x169a5a%7Clabel:K%7C' + session.pickup_location.gps.latitude + ',' + session.pickup_location.gps.longitude + strWithKeys,
          wayfinding: session.pickupLocationWithDetail.wayfindings && session.pickupLocationWithDetail.wayfindings.length > 0 ? session.pickupLocationWithDetail.wayfindings[0].text : null
        },
        dropoff: {
          name: session.return_location.name,
          address: session.return_location.address,
          phones: session.return_location.phones,
          afterHoursDropoff: session.return_location.after_hours_return,
          hours: session.return_location.hours,
          date: moment(session.return_time).format(enterprise.i18nUnits.dateformat),
          time: moment(session.return_time).format(enterprise.i18nUnits.timeformat),
          gps: {
            latitude: session.pickup_location.gps.latitude,
            longitude: session.pickup_location.gps.longitude
          },
          imgSrc: 'https://maps.google.com/maps/api/staticmap?center=' + session.return_location.gps.latitude + ',' + session.return_location.gps.longitude +
          '&zoom=12&size=400x225&scale=2&&markers=color:0x169a5a%7Clabel:K%7C' + session.return_location.gps.latitude + ',' + session.return_location.gps.longitude + strWithKeys,
          wayfinding: session.pickupLocationWithDetail.wayfindings && session.pickupLocationWithDetail.wayfindings.length > 0 ? session.pickupLocationWithDetail.wayfindings[0].text : null
        }
      };
    }
    return locationDetails;
  },
  getLocationHoursByDay: function (type, startDate) {
    const session = ReservationStateTree.select(ReservationCursors.reservationSession).get();
    let selectType = type === 'pickup' ? 'pickup' : 'return';
    let location = session[selectType + '_location'];
    let locationId = location.id;
    let from = startDate.clone().startOf('day').format('YYYY-MM-DD');
    let to = startDate.clone().endOf('day').format('YYYY-MM-DD');

    if (this[type + 'ClosedDateXHR']) {
      this[type + 'ClosedDateXHR'].abort();
    }

    if (!!locationId) {
      let data = {
        from,
        to
      };

      this[type + 'ClosedDateXHR'] = EnterpriseServices.GET(SERVICE_ENDPOINTS.LOCATION_HOURS + locationId,
        {
          prefix: enterprise.solr.path,
          data: data,
          callback: (res) => {
            if (res.data && res.data[from]) {
              let hours = res.data[from].STANDARD.hours;
              hours.forEach(i => {
                i.open = moment(i.open, 'H:mm').format('LT');
                i.close = moment(i.close, 'H:mm').format('LT');
              });
              ReservationStateTree.select(ReservationCursors[type + 'Hours']).set(hours);
            }
          }
        });
    } else {
      console.log('Missing query parameters. ConfirmedActions.js:getLocationHoursByDay');
    }
  },
  getSupportPhoneNumberOfType: function (type) {
    const session = ReservationStateTree.select(ReservationCursors.reservationSession).get();
    const supportLinks = session.supportLinks;
    let phoneObj = null;

    if (supportLinks) {
      supportLinks.support_phone_numbers.forEach((phone) => {
        if (phone.phone_type === type) {
          phoneObj = phone;
        }
      });
    }
    return phoneObj;
  }
};

module.exports = ConfirmedActions;
