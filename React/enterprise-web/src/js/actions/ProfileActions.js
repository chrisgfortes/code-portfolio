import ReservationCursors from '../cursors/ReservationCursors';
import {
  getClearProfile
} from '../factories/User';

const ProfileActions = {
  get: {},
  set: {
    clearProfile() {
      let profileMapper = getClearProfile();
      ReservationStateTree.select(ReservationCursors.profile).update(profileMapper);
    }
  }
}

export default ProfileActions;
