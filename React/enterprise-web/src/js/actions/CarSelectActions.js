import ReservationCursors from '../cursors/ReservationCursors';

const CarSelectActions = {
  toggleVehiclePriceModal (bool){
    ReservationStateTree.select(ReservationCursors.vehiclePriceModalControls).set(bool);
  },
  updatePayType (type) {
    ReservationStateTree.select(ReservationCursors.carPayType).set(type);
  },
  getPayType () {
    return ReservationStateTree.select(ReservationCursors.carPayType).get();
  },
  setAvailablePayTypes (availablePayTypes) {
    ReservationStateTree.select(ReservationCursors.availablePayTypes).set(availablePayTypes);
  },
  setIsNAPrepayEnabled (bool) {
    ReservationStateTree.select(ReservationCursors.isNAPrepayEnabled).set(bool);
  },
  setAvailableFilters (filters) {
    ReservationStateTree.select(ReservationCursors.carFilters).set(filters);
  },
  getAvailableVehicles () {
    return ReservationStateTree.select(ReservationCursors.availableVehicles).get();
  },
  setAvailableVehicles (vehicles) {
    ReservationStateTree.select(ReservationCursors.availableVehicles).set(vehicles);
  },
  getFilteredVehicles () {
    return ReservationStateTree.select(ReservationCursors.filteredVehicles).get();
  },
  setFilteredVehicles (vehicles) {
    ReservationStateTree.select(ReservationCursors.filteredVehicles).set(vehicles);
  },
  getPreSelectedCarClass () {
    return ReservationStateTree.select(ReservationCursors.preSelectedCarClass).get();
  },
  setPreSelectedCarClass (carName) {
    ReservationStateTree.select(ReservationCursors.preSelectedCarClass).set(carName);
  },
  setMetaPreSelectedCarClass (carClass) {
    ReservationStateTree.select(ReservationCursors.preSelectedMeta).set(carClass);
  },
  toggleLearnPayNowModal (bool) {
    ReservationStateTree.select(ReservationCursors.learnPayNowModal).set(bool);
  },
  getVehicleByCode (code) {
    return CarSelectActions.getAvailableVehicles()
                           .find(v => v.code === code);
  },
  getSelectedCarCode () {
    return ReservationStateTree.select(ReservationCursors.selectedCarCode).get();
  },
  setVehicleDetails (details, code) {
    // @todo - Refactor this mess when possible
    let availableCursor = ReservationStateTree.select(ReservationCursors.availableVehicles);
    let filteredCursor = ReservationStateTree.select(ReservationCursors.filteredVehicles);
    const availableVehicles = availableCursor.get() || [];
    const filteredVehicles = filteredCursor.get() || [];
    availableVehicles.forEach((v, i) => {
      if(v.code === code) {
        availableCursor.select(i).set('details', details);
      }
    });
    filteredVehicles.forEach((v, i) => {
      if (v.code === code) {
        filteredCursor.select(i).set('details', details);
      }
    });
  },
  setAvailableUpgrades (upgrades) {
    ReservationStateTree.select(ReservationCursors.availableUpgrades).set(upgrades);
  },
  setUpgradedStatus (status) {
    ReservationStateTree.select(ReservationCursors.vehicleUpgraded).set(status);
  },
  getUpgradedStatus () {
    return ReservationStateTree.select(ReservationCursors.vehicleUpgraded).get();
  },
  setPreviousCarClass (car) {
    ReservationStateTree.select(ReservationCursors.previousCarClass).set(car);
  },
  selectVehicle (carClass, carName, car) {
    ReservationStateTree.select(ReservationCursors.selectedCarCode).set(carClass);
    ReservationStateTree.select(ReservationCursors.viewSelectedCar).set(carName);
    ReservationStateTree.select(ReservationCursors.viewSelectedCarDetails).set(car);
    ReservationStateTree.select(ReservationCursors.reservationCarStep).set(carName);
  },
  clearCarStep () {
    ReservationStateTree.select(ReservationCursors.reservationCarStep).set(false);
  },
  preSelectVehicle (car) {
    ReservationStateTree.select(ReservationCursors.preSelectedVehicle).set(car);
  },
  carSelectDataReady (bool) {
    ReservationStateTree.select(ReservationCursors.carSelectDataReady).set(bool);
  },
  toggleTaxesAndFeesModal (bool) {
    ReservationStateTree.select(ReservationCursors.taxesAndFeesModal).set(bool);
  },
  setVanModal (bool, description, payType) {
    ReservationStateTree.select(ReservationCursors.vanModalControl).set(bool);

    if (description) {
      ReservationStateTree.select(ReservationCursors.vanModalDescription).set(description);
    }

    if (payType) {
      CarSelectActions.updatePayType(payType);
    }
  },
  setVanModalConfirm (bool) {
    ReservationStateTree.select(ReservationCursors.vanModalConfirm).set(bool);
  },
  toggleRequestModal (bool) {
    ReservationStateTree.select(ReservationCursors.requestModalModal).set(bool);
  },
  setRequestModalOrigin (bool, origin) {
    ReservationStateTree.select(ReservationCursors.requestModalModal).set(bool);
    ReservationStateTree.select(ReservationCursors.requestModalOrigin).set(origin);
  },
  setRequestModalConfirm (bool) {
    ReservationStateTree.select(ReservationCursors.requestModalConfirm).set(bool);
  },
  setDetailsCar (code) {
    ReservationStateTree.select(ReservationCursors.detailsCar).set(code);
  },
  setCarRateCompare (car) {
    ReservationStateTree.select(ReservationCursors.carRateCompare).set(car);
  },
  toggleRateComparisonModal (bool) {
    ReservationStateTree.select(ReservationCursors.rateComparisonModal).set(bool);
  },
  getSessionSelectedCar () {
    return ReservationStateTree.select(ReservationCursors.selectedCar).get();
  },
  getSelectedFilters () {
    return ReservationStateTree.select(ReservationCursors.selectedCarFilters).get();
  },
  setAllFilters (filters) {
    ReservationStateTree.select(ReservationCursors.selectedCarFilters).set(filters);
  },
  setFilter (filter, value) {
    ReservationStateTree.select(ReservationCursors.selectedCarFilters).set(filter, value);
  },
  addToFilter (filter, value) {
    ReservationStateTree.select(ReservationCursors.selectedCarFilters).push(filter, value);
  },
  removeFromFilter (filter, value) {
    const currentFilter = ReservationStateTree.select(ReservationCursors.selectedCarFilters).get(filter);
    ReservationStateTree.select(ReservationCursors.selectedCarFilters).set(filter, currentFilter.filter(i => i !== value));
  },
  setBandState (band) {
    ReservationStateTree.select(ReservationCursors.filterBandState).set(band);
  },
  setTransmissionBandState (band) {
    ReservationStateTree.select(ReservationCursors.transmissionBandState).set(band);
  },
  closeNotAvailableModal () {
    ReservationStateTree.select(ReservationCursors.notAvailableModal).set(false);
  }
};

module.exports = CarSelectActions;