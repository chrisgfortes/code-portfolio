/**
 * @module LocationSearchActions
 * LocationController for Location Entities
 * LocaionSearchController for Location Search activities
 */
import ReservationActions from '../actions/ReservationActions';
import LocationActions from './LocationActions';

import { LOCATION_SEARCH } from '../constants/';
import ReservationCursors from '../cursors/ReservationCursors';

// @todo: get this controller out of this ACTION FILE!!
// This was/is a recursive issue and is "required" below
import LocationController from '../controllers/LocationController';

import Storage from '../utilities/util-localStorage';

import { debug } from '../utilities/util-debug';
const logger = debug({name:'LocationSearchActions', isDebug: false}).logger;

const createScrollOffsetKey = (key) => `scrollOffset-${key}`;

const cursors = {
  pickupMap: ReservationCursors.pickupMap,
  dropoffMap: ReservationCursors.dropoffMap,
  pickupMapModel: ReservationCursors.pickupMapModel,
  dropoffMapModel: ReservationCursors.dropoffMapModel,
  pickupTarget: ReservationCursors.pickupTarget,
  dropoffTarget: ReservationCursors.dropoffTarget,
  pickupLocationObj: ReservationCursors.pickupLocationObj,
  dropoffLocationObj: ReservationCursors.dropoffLocationObj,
  locationOpenForMyTimesFilter: ReservationCursors.locationOpenForMyTimesFilter,
  locationSelect: ReservationCursors.locationSelect,
  afterHourMessage: ReservationCursors.afterHourMessage,
  showAfterHoursModal: ReservationCursors.showAfterHoursModal
}

/**
 * @namespace Components: Component related actions
 * @memberOf LocationSearchActions
 * @type {Object}
 */
const Components = {
  // @todo CHANGE NAME TO getLocationDisplayResults
  /**
   * @function getResults
   * @memberOf LocationSearchActions
   * @param  {string}  resultType  pickup, dropoff etc.
   * @param  {Boolean} resultArray what type of results to return
   * @return {object|array}
   */
  getResults (resultType, resultArray = false) {
    logger.warn('getResults() getting Cursors.locationDisplayResults', resultType, resultArray);
    let returnObj;
    let nodeList;
    let rawResults = ReservationStateTree.select(ReservationCursors.locationDisplayResults);
    if (resultArray) {
      nodeList = rawResults.get();
      returnObj = nodeList;
    } else {
      returnObj = rawResults;
    }
    return returnObj;
  },
  getScrollOffset (key) {
    return Storage.SessionStorage.get(createScrollOffsetKey(key));
  },
  cacheScrollOffset (key, value) {
    // I am starting to feel like this is just noise pollution in the state tree
    // ReservationStateTree.select(cursors.locationSelect).set('scrollOffset', value);
    if (value === null) {
      Storage.SessionStorage.remove(createScrollOffsetKey(key));
    } else {
      Storage.SessionStorage.set(createScrollOffsetKey(key), value);
    }
  },
  getOpenForMyTimesFilter () {
    return ReservationStateTree.select(cursors.locationOpenForMyTimesFilter).get();
  },
  setOpenForMyTimesFilter (isFiltering) {
    ReservationStateTree.select(cursors.locationOpenForMyTimesFilter).set(isFiltering);
  },
  //We need to differentiate Pin and ResultItem so we don't change the state of our component from the onHover
  // which trigger an invariant error on React
  setPinHighlightLocation: function (index) {
    ReservationStateTree.select(ReservationCursors.highlightPinLocation).set(index);
  },
  setHighlightLocation: function (index) {
    ReservationStateTree.select(ReservationCursors.highlightLocation).set(index);
  },
  setAfterHoursModal( bool ) {
    ReservationStateTree.select(cursors.showAfterHoursModal).set(bool);
  },
  setAfterHoursMessage( message ) {
    ReservationStateTree.select(cursors.afterHourMessage).set(message);
  },
  setDetailsPage (type, detail) {
    ReservationStateTree.select(cursors[type + 'Map']).set('detailsPage', detail);
  },
  clearDetailsByType (type) {
    ReservationStateTree.select(cursors[type + 'Target']).set('details', null);
  },
  setPreventDetails (bool) {
    ReservationStateTree.select(cursors.locationSelect).set('preventDetails', bool);
  },
  setSelectionActive (bool) {
    logger.log('setLocationActive(bool)', bool);
    ReservationStateTree.select(cursors.locationSelect).set('selectionActive', bool);
  },
  setSearchRadius: function (type, radius) {
    ReservationStateTree.select(cursors[type + 'Map']).set('searchRadius', radius);
  },
  setMapResults (type, results) {
    logger.log('setMapResults() called twice?', results);
    const obj = {
      results: {
        $set: results
      }
    };
    ReservationStateTree.select(cursors[type + 'MapModel']).update(obj);
  },
  setMapTarget (type, coordinates) {
    // @todo use LocationCoordinatesModelFactory but not from inside Actions
    const obj = {
      longitude: {
        $set: coordinates.longitude
      },
      lat: {
        $set: coordinates.lat
      }
    };
    ReservationStateTree.select(cursors[type + 'Target']).update(obj);
  },
  setMapBearing (type, data) {
    const southwest = {
      longitude: {
        $set: data.southwest.longitude
      },
      lat: {
        $set: data.southwest.latitude
      }
    };
    const northeast = {
      longitude: {
        $set: data.northeast.longitude
      },
      lat: {
        $set: data.northeast.latitude
      }
    };
    ReservationStateTree.select(ReservationCursors[type + 'TargetNortheast']).update(northeast);
    ReservationStateTree.select(ReservationCursors[type + 'TargetSouthwest']).update(southwest);
  },
  setMapTargetDetails (type, details) {
    const obj = {
      details: {
        $set: details
      }
    };
    ReservationStateTree.select(cursors[type + 'Target']).update(obj);
  },
  clearMapResults () {
    const obj = {
      results: {
        $set: []
      }
    };
    //TODO: Refactor so we don't do two updates
    ReservationStateTree.select(cursors.pickupMapModel).update(obj);
    ReservationStateTree.select(cursors.dropoffMapModel).update(obj);
  },
  setLocationFilter (type, filter) {
    const obj = {
      filter: {
        $set: filter
      }
    };
    ReservationStateTree.select(cursors[type + 'Map']).update(obj);
  },
  setMapZoom (type, zoom) {
    const obj = {
      zoom: {
        $set: zoom
      }
    };
    ReservationStateTree.select(cursors[type + 'Map']).update(obj);
  },
  setMapMobileTab (type, tab) {
    const obj = {
      mobileTab: {
        $set: tab
      }
    };
    ReservationStateTree.select(ReservationCursors[type + 'Map']).update(obj);
  },
  setShowMobileMap (type, showValue) {
    const obj = {
      showMobileMap: {
        $set: showValue
      }
    };
    ReservationStateTree.select(ReservationCursors[type + 'Map']).update(obj);
  },
  setShowMobileFilter (type, showValue) {
    const obj = {
      showMobileFilter: {
        $set: showValue
      }
    };
    ReservationStateTree.select(ReservationCursors[type + 'Map']).update(obj);
  },
  setSearchAttribute (type, attribute) {
    const obj = {
      searchAttribute: {
        $set: attribute
      }
    };
    ReservationStateTree.select(ReservationCursors[type + 'Map']).update(obj);
  }
}

/**
 * @namespace ServiceData: Service and data handling for Location Search
 * @type {Object}
 */
const ServiceData = {
  /**
   * Save location input searches data
   * @param  {object} obj data returned from getLocations, usually ReservationFlowModelController
   * @return {void}
   * @deprecated Please use LocationActions.saveGetLocations()
   * @todo @gbov2 determine if the LocationSearchResults path is viable
   */
  saveGetLocations(obj) {
    logger.warn('saveGetLocations(locationObject) DEPRECATED use LocationActions.saveGetLocations instead', obj);
    LocationActions.saveGetLocations(obj);
  }
}

const LocationSearchActions = {
  ...Components,
  ...ServiceData,

  get: {
    hasPickupLocation() {
      const flags = ReservationStateTree.select(ReservationCursors.reservationStatusFlags).get();
      return flags.locations && flags.locations.hasPickupLocation;
    },
    hasReturnLocation() {
      const flags = ReservationStateTree.select(ReservationCursors.reservationStatusFlags).get();
      return flags.locations && flags.locations.hasReturnLocation;
    }
  },

  // @todo: use classes / factories
  clearFedexResults (type) {
    logger.warn('clearActions(): please use classes and factories for model template.')
    const obj = {
      [type]: {
        location: {
          results: {
            $set: {
              fedex: []
            }
          }
        }
      }
    };
    ReservationStateTree.select(ReservationCursors.model).update(obj);
  },

  // ['view', 'locationView']
  setLocationView (type) {
    logger.log('setLocationView(type)', type);
    ReservationStateTree.select(ReservationCursors.locationView).set(type);
  },

  /**
   * @deprecated Please use ReservationActions
   * @param {void} type
   */
  setCurrentView (type) {
    logger.warn('deprecated method — called setCurrentView(type). Please ', type);
    ReservationActions.setCurrentView(type);
  },

  // @TODO this may not be used anywhere. remove?
  setLocationSelectValidDate (type, valid) {
    logger.warn('setLocationSelectValidDate(type, valid)', type, valid);
    const obj = {
      validDate: {
        $set: valid
      }
    };
    ReservationStateTree.select(ReservationCursors[type + 'LocationSelect']).update(obj);
  },
  // @TODO this may not be used anywhere. remove?
  setLocationSelectValidTime (type, valid) {
    logger.warn('setLocationSelectValidTime(type, valid)', type, valid)
    const obj = {
      validtime: {
        $set: valid
      }
    };
    ReservationStateTree.select(ReservationCursors[type + 'LocationSelect']).update(obj);
  },

  setLocationType (type, locationType) {
    const obj = {
      locationType: {
        $set: locationType
      }
    };
    ReservationStateTree.select(ReservationCursors[type + 'Map']).update(obj);
  },

  setInitialAirportSearch (type, value) {
    logger.log('setInitialAirportSearch()');
    ReservationStateTree.select(cursors[type + 'Map']).set('initialAirportSearch', value);
  },

  /**
   * @memberOf LocationSearchActions
   * Preferred method to set Same Location, nice and at home in LocationSearchActions
   * @param {boolean} bool true/false are we using the same location for pickup/dropoff
   */
  setSameLocation (bool) {
    logger.log('setSameLocation()');
    ReservationStateTree.select(ReservationCursors.sameLocation).set(bool);
  },
  /**
   * moved from ReservationActions
   * Not sure why this is "from session" ...
   * @param {boolean} bool true/false etc.
   * @deprecated Use setSameLocation
   */
  setSameLocationFromSession (bool) {
    logger.warn('Deprecated method setSameLocationFromSession called. Please use setSameLocation.');
    ReservationStateTree.select(ReservationCursors.sameLocation).set(bool);
  },
  /**
   * @memberOf LocationSearchActions
   * moved from bookingwidgetservice cuz ... uh, not a service
   * @param {boolean} bool
   */
  setSameLocationWithDates(bool) {
    logger.warn('setSameLocationWithDates()');
    ReservationStateTree.options.autoCommit = false;
    ReservationStateTree.select(ReservationCursors.sameLocation).set(bool);
    //this.clearDateTime(LOCATION_SEARCH.DROPOFF);
    // Need to change dropoff location to pickup location and clear dropoff times
    if (bool) {
      let pickupLocationId = ReservationStateTree.select(ReservationCursors.pickupLocation).get();
      let dropoffLocationId = ReservationStateTree.select(ReservationCursors.dropoffLocation).get();
      ReservationStateTree.select(ReservationCursors.dropoffModel).set('location', ReservationStateTree.select(ReservationCursors.pickupLocationObj).get());

      //We are getting rid of a different dropoff location so should clear dropoff calendar and times.
      if (pickupLocationId != dropoffLocationId) {
        ReservationActions.clearDateTime(LOCATION_SEARCH.DROPOFF);
        // @todo: GET THIS CONTROLLER OUT OF THIS ACTION FILE
        LocationController.getClosedHoursForLocation(LOCATION_SEARCH.DROPOFF);
        // this.getClosedHoursForLocation(LOCATION_SEARCH.DROPOFF);
        // BookingWidgetModelController.getClosedHoursForLocation(LOCATION_SEARCH.DROPOFF);
      }
    } else {
      ReservationActions.clearLocation(LOCATION_SEARCH.DROPOFF);
      ReservationActions.clearPolicies(LOCATION_SEARCH.DROPOFF);
      ReservationActions.clearClosedTimes(LOCATION_SEARCH.DROPOFF);
    }
    ReservationStateTree.commit();
    ReservationStateTree.options.autoCommit = true;
  },

  getClosedHoursforLocation(type) {
    logger.warn('getClosedHoursforLocation() NEED TO FIGURE OUT SCOPE ISSUE', type);
  },

  /**
   * @function isUsingSameLocation
   * @memberOf LocationSearchActions
   * Just check to see if it's true or false
   * @todo this is called too often...
   * @return {Boolean} boolean
   */
  isUsingSameLocation () {
    // console.count('LocationSearchActions.isUsingSameLocation()');
    return ReservationStateTree.select(ReservationCursors.sameLocation).get();
  },

  setMyLocation (type, lat, lng) {
    ReservationStateTree.select(cursors[type + 'LocationObj']).set('myLocation', true);
    ReservationStateTree.select(cursors[type + 'LocationObj']).set('longitude', lng);
    ReservationStateTree.select(cursors[type + 'LocationObj']).set('lat', lat);
    ReservationStateTree.select(cursors[type + 'LocationObj']).set('locationType', 'MY_LOCATION');
    ReservationStateTree.select(cursors[type + 'LocationObj']).set('locationName', enterprise.i18nReservation.resflowlocations_0074);
  }
};

module.exports = LocationSearchActions;
