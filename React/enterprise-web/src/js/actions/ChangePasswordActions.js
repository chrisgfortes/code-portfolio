let ReservationCursors = require('../cursors/ReservationCursors');

let ChangePasswordActions = {
  toggleModal (bool) {
    ReservationStateTree.select(ReservationCursors.changePassword).set('modal', bool);
    if (bool === false) {
      ReservationStateTree.select(ReservationCursors.loginWidget).set('errors', null);
    }
  }
};

module.exports = ChangePasswordActions;
