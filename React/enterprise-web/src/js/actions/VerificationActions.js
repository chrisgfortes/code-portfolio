import ReservationCursors from '../cursors/ReservationCursors';
import CorporateActions from './CorporateActions';
import AccountActions from './AccountActions';
import { debug } from '../utilities/util-debug';

const logger = debug({isDebug: __DEVELOPMENT__, name: 'VerificationActions'}).logger;

let VerificationActions = {
  setModalCursor ( cursor, bool ) {
    ReservationStateTree.set( cursor, bool );
  },
  setConflictAccountModal( bool ) {
    ReservationStateTree.select(ReservationCursors.conflictAccountModalRoot).set('modal', bool);
  },
  setLocationSearchType: function (type) {
    ReservationStateTree.select(ReservationCursors.locationSelect).set('showType', type);
  },
  setShowOptionalForm: function (state) {
    ReservationStateTree.select(ReservationCursors.verification).set('showOptionalForm', state);
  },
  setBasicPrepayModal() {
    ReservationStateTree.select(ReservationCursors.verification).set('modal', 'prepay');
  },
  setPersonalFields: function (variable, value) {
    ReservationStateTree.select(ReservationCursors.personal).set(variable, value);
  },
  setPrepayModal: function (modal) {
    ReservationStateTree.select(ReservationCursors.verification).set('cardModal', modal);
  },
  setReviewPageCardDetails: function (data) {
    ReservationStateTree.select(ReservationCursors.verification).set('authReviewCardDetails', data);
  },
  setCardModifyConfirmationModal: function (modal) {
    ReservationStateTree.select(ReservationCursors.verification).set('cardModifyConfirmationModal', modal);
  },
  setAddCardSuccess: function (status) {
    ReservationStateTree.select(ReservationCursors.verification).set('addCardSuccess', status);
  },
  setIssueCountry: function (country) {
    ReservationStateTree.select(ReservationCursors.verification).set('issueCountry', country);
  },
  setAssociateAccountStatus: function (status) {
    ReservationStateTree.select(ReservationCursors.verification).set('associateAccount', status);
  },
  // setConflictAccountModal: function (state) {
  //   ReservationStateTree.select(ReservationCursors.verification).set(['conflictAccountModal', 'modal'], state);
  // },
  setRegisterCardSuccess: function (status) {
    ReservationStateTree.select(ReservationCursors.verification).set('registerCardSuccess', status);
  },
  setDNCModal: function (state) {
    ReservationStateTree.select(ReservationCursors.verification).set(['DNCModal', 'modal'], state);
  },
  setThreeDS (response) {
    let obj = {
      url: {
        $set: response ? response.url : null
      },
      paReq: {
        $set: response ? response.paReq : null
      }
    };

    ReservationStateTree.select(ReservationCursors.threeDS).update(obj);
  },
  clearThreeDS () {
    let obj = {
      url: {
        $set: null
      },
      paReq: {
        $set: null
      },
      token: {
        $set: null
      }
    };

    ReservationStateTree.select(ReservationCursors.threeDS).update(obj);
  },
  setRequestPromotions: function (bool) {
    ReservationStateTree.select(ReservationCursors.personal).set('requestPromotion', bool);
  },
  setModifyInformation: function () {
    let driverInformation = AccountActions.get.driverInfoModel();
    let airlineInformation = ReservationStateTree.select(ReservationCursors.reservationSession).get().airlineInformation;
    let personal = AccountActions.get.personalModel();
    let profile = AccountActions.get.profileModel();

    if (driverInformation) {
      profile = {
        basic_profile: {
          first_name: driverInformation.first_name ? driverInformation.first_name : null,
          last_name: driverInformation.last_name ? driverInformation.last_name : null
        },
        contact_profile: {
          email: driverInformation.email_address ? driverInformation.email_address : null,
          phones: [{
            phone_number: driverInformation.phone && driverInformation.phone.phone_number
              ? driverInformation.phone.phone_number : null
          }]
        },
        preference: {
          email_preference: {
            special_offers: driverInformation.request_email_promotions ? driverInformation.request_email_promotions : null
          }
        }
      }
    }

    // ECR-14250 model?
    let obj = {
      firstName: {
        $set: personal.firstName ? personal.firstName : profile ? profile.basic_profile.first_name : null
      },
      lastName: {
        $set: personal.lastName ? personal.lastName : profile ? profile.basic_profile.last_name : null
      },
      email: {
        $set: personal.email ? personal.email : profile ? profile.contact_profile.email : null
      },
      phoneNumber: {
        $set: personal.phoneNumber ? personal.phoneNumber : profile ? profile.contact_profile.phones[0].phone_number : null
      },
      requestPromotion: {
        $set: personal.requestPromotion ? personal.requestPromotion : profile && profile.preference ? profile.preference.email_preference.special_offers : null
      }
    };

    ReservationStateTree.select(ReservationCursors.personal).update(obj);

    if (airlineInformation) {
      let obj = {
        airlineCode: {
          $set: airlineInformation.code ? airlineInformation.code : null
        },
        flightNumber: {
          $set: airlineInformation.flight_number ? airlineInformation.flight_number : null
        }
      };

      ReservationStateTree.select(ReservationCursors.personal).update(obj);
    }
  },
  setPersonalInformation: function () {
    // @todo use actions - we have several for getSession
    const session = ReservationStateTree.select(ReservationCursors.reservationSession).get();
    // @todo use actions - we have several for get profile
    const sessionProfile = session.profile;
    // @todo use actions - we should have something for profile
    const personal = ReservationStateTree.select(ReservationCursors.personal).get();
    const commitRequest = session.commitRequest;
    let profile = sessionProfile;
    let savedEmail;

    if (commitRequest && commitRequest.driver_info) {
      logger.warn('setPersonalInformation() is setting commitRequest data...');
      // @todo this should be a modifier method as it's a left turn in this method designed for Setting Personal Info
      // @todo ProfileFactory or similar
      profile = {
        basic_profile: {
          first_name: commitRequest.driver_info.first_name,
          last_name: commitRequest.driver_info.last_name
        },
        contact_profile: {
          email: commitRequest.driver_info.email_address,
          phones: [{
            phone_number: commitRequest.driver_info.phone && commitRequest.driver_info.phone.phone_number
          }]
        },
        preference: {
          email_preference: {
            special_offers: commitRequest.driver_info.request_email_promotions === 'true'
          }
        }
      };
    }

    // @todo change to use PersonalFactory (?)
    let obj = {
      firstName: {
        $set: personal.firstName || (profile && profile.basic_profile.first_name)
      },
      lastName: {
        $set: personal.lastName || (profile && profile.basic_profile.last_name)
      },
      email: {
        $set: savedEmail || personal.email || (profile && profile.contact_profile.email)
      },
      phoneNumber: {
        $set: personal.phoneNumber || _.get(profile, 'contact_profile.phones[0].phone_number')
      },
      requestPromotion: {
        $set: personal.requestPromotion || _.get(profile, 'preference.email_preference.special_offers')
      }
    };
    logger.warn('setting Personal Information: ', obj);
    ReservationStateTree.select(ReservationCursors.personal).update(obj);
  },
  setOtherInformation () {
    logger.warn('setting setOtherInformation');
    let commitRequest = ReservationStateTree.select(ReservationCursors.reservationSession).get().commitRequest;

    let obj = {
      airlineCode: {
        $set: commitRequest && commitRequest.airline_information ? commitRequest.airline_information.code : null
      },
      flightNumber: {
        $set: commitRequest && commitRequest.airline_information ? commitRequest.airline_information.flight_number : null
      }
    };

    if (commitRequest && commitRequest.privacyChecked) {
      ReservationStateTree.select(ReservationCursors.verification).set('privacyChecked', commitRequest.privacyChecked);
    }

    ReservationStateTree.select(ReservationCursors.personal).update(obj);

    if (commitRequest && commitRequest.additional_information) {
      CorporateActions.setAdditionalInfoFromArray(commitRequest.additional_information);
    }
  },
  //getAgeRange: function (age) {
  //
  //    let range = null,
  //        locationAge = ReservationStateTree.get().model.ageRange;
  //
  //    if (!age) {
  //        return null
  //    }
  //    for (let i = 0, len = locationAge.length; i < len; i++) {
  //        if (locationAge[i].value === age.toString()) {
  //            range = locationAge[i].label;
  //        }
  //    }
  //
  //    return range;
  //},
  setAvailableFields: function (fields) {
    ReservationStateTree.select(ReservationCursors.verification).set('fields', fields);
  },
  setModal: function (state) {
    ReservationStateTree.select(ReservationCursors.verification).set('modal', state);
  },
  setOverallPolicy: function (bool) {
    ReservationStateTree.select(ReservationCursors.verification).set('overallPolicy', bool);
  },
  setPrivacyPolicy: function (disclaimer) {
    ReservationStateTree.select(ReservationCursors.verification).set('privacyPolicy', disclaimer);
  },
  setPrepayPolicy: function (policy) {
    ReservationStateTree.select(ReservationCursors.verification).set('prepayPolicy', policy);
  },
  setPrepayChecked: function ( bool ) {
    ReservationStateTree.select(ReservationCursors.verification).set('prepayChecked', bool);
  },
  setPrivacyChecked: function ( bool ) {
    ReservationStateTree.select(ReservationCursors.verification).set('privacyChecked', bool);
  },
  setTaxesFeesDisclaimer: function (disclaimer) {
    ReservationStateTree.select(ReservationCursors.verification).set('taxesFeesDisclaimer', disclaimer);
  },
  setKeyFacts: function (facts) {
    ReservationStateTree.select(ReservationCursors.verification).set(['keyFacts', 'content'], facts);
  },
  setKeyFactsModal: function (bool) {
    ReservationStateTree.select(ReservationCursors.verification).set(['keyFacts', 'modal'], bool);
  },
  setReservationPolicyModalContent: function (item) {
    ReservationStateTree.select(ReservationCursors.verification).set(['policyModal', 'header'], item.description);
    ReservationStateTree.select(ReservationCursors.verification).set(['policyModal', 'content'], item.policy_text);
  },
  setPolicyModal: function (bool) {
    ReservationStateTree.select(ReservationCursors.verification).set(['policyModal', 'modal'], bool);
  },
  setPaymentReferenceID (payment) {
    logger.warn('update setPaymentReferenceID to not use model path');
    ReservationStateTree.select(ReservationCursors.model).set('paymentReferenceID', payment);
  },
  setSavePaymentForLater (bool) {
    logger.warn('update setSavePaymentForLater to not use model path');
    ReservationStateTree.select(ReservationCursors.model).set('savePaymentForLater', bool);
  },
  setShowPaymentList (bool) {
    logger.warn('update showPaymentList to not use model path');
    ReservationStateTree.select(ReservationCursors.model).set('showPaymentList', bool);
  },
  clearPreviousErrors: function () {
    ReservationStateTree.select(ReservationCursors.extrasView).set('errors', null);
  },
  setDestinationPriceInfoModal: function (bool) {
    logger.warn('update setDestinationPriceInfoModal to not use model path');
    ReservationStateTree.select(ReservationCursors.model).set('destinationPriceInfoModal', bool);
  },
  setCannotModifyModal: function (bool) {
    logger.warn('update setCannotModifyModal to not use model path');
    ReservationStateTree.select(ReservationCursors.model).set('cannotModifyModal', bool);
  },
  setVerificationErrors (errors) {
    ReservationStateTree.select(ReservationCursors.verification).set('errors', errors);
  },
  getVerification () {
    return ReservationStateTree.get(ReservationCursors.verification);
  },
  getModelTree () { // this probably shouldn't exist
    logger.warn('WE NEED TO STOP USING THIS !!!! ASAP !!!!');
    return ReservationStateTree.get(ReservationCursors.model);
  }
};

module.exports = VerificationActions;
