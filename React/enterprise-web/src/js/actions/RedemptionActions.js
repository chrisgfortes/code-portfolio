import ReservationCursors from '../cursors/ReservationCursors';

const RedemptionActions = {
  toggleRedemptionModal(bool) {
    ReservationStateTree.select(ReservationCursors.redemptionModal).set(bool);
  },
  toggleRedemptionModalLoading(bool) {
    ReservationStateTree.select(ReservationCursors.redemptionLoading).set(bool);
  },
  addDay () {
    ReservationStateTree.select(ReservationCursors.redemptionDays)
                        .apply((days) => days + 1);
  },
  subtractDay () {
    ReservationStateTree.select(ReservationCursors.redemptionDays)
                        .apply((days) => days - 1);
  },
  setDays (days = 0) {
    ReservationStateTree.select(ReservationCursors.redemptionDays).set(days);
  },
  getDays () {
    return ReservationStateTree.select(ReservationCursors.redemptionDays).get();
  },
  setPreviousDays (days = 0) {
    ReservationStateTree.select(ReservationCursors.redemptionPreviousDays).set(days);
  },
  setPayLaterPrice(price) {
    ReservationStateTree.select(ReservationCursors.redemptionPayLaterPrice).set(price);
  }
};

module.exports = RedemptionActions;

