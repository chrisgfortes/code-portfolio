
import ReservationStateTree from '../stateTrees/ReservationStateTree';
import ReservationCursors from '../cursors/ReservationCursors';
import { getMessagesFormat } from '../factories/MessagesFactory';
import { RESERVATION } from '../constants';
import Analytics from '../modules/Analytics';

const { SPECIAL_ERROR_MAP } = RESERVATION;

const ErrorActions = {
  stopLoading () { // stop all activity?
    ReservationStateTree.select(ReservationCursors.loadingClass).set(null);
  },
  setErrorsForComponent: function (messages, component, request) {
    // console.warn('setErrorsForComponent(messages, component)', messages, component);

    // MessagesFactory
    const errors = getMessagesFormat(messages);

    if (errors.length > 0){
      // console.log('errors: ', errors);
      errors.forEach((error) => {
        // console.log('...error...', error);
        ErrorActions.triggerAlertMessage(error, request);
      });

      Analytics.trigger('html', 'errors', [{
        errors: errors
      }]);

      ReservationStateTree.select(ReservationCursors[component]).set('errors', errors);
    }else{
      console.error('ErrorActions.js::setErrorsForComponent():', errors, component);
    }

    ErrorActions.stopLoading(); // maybe we should _always_ do this?
  },
  triggerAlertMessage(message, request) {
    if (SPECIAL_ERROR_MAP.hasOwnProperty(message.displayAs)) {
      ReservationStateTree.select(ReservationCursors.viewSpecialError.concat(SPECIAL_ERROR_MAP[message.displayAs])).set('modal', true);
      if (request) ReservationStateTree.select(ReservationCursors.viewSpecialError.concat(SPECIAL_ERROR_MAP[message.displayAs])).set('request', request);
    } else if (SPECIAL_ERROR_MAP.hasOwnProperty(message.code)) {
      ReservationStateTree.select(ReservationCursors.viewSpecialError.concat(SPECIAL_ERROR_MAP[message.code])).set('modal', true);
      if (request) ReservationStateTree.select(ReservationCursors.viewSpecialError.concat(SPECIAL_ERROR_MAP[message.code])).set('request', request);
    }
  },
  clearErrorsForComponent: function (component) {
    ReservationStateTree.select(ReservationCursors[component]).set('errors', null);
  }
};

module.exports = ErrorActions;
