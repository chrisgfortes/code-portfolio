import ReservationCursors from '../cursors/ReservationCursors';
import PriceActions from './PriceActions';

const KeyRentalFactsHelpers = {
  splitKeyFacts (keyFacts = []) {
    let keyFactsObj = {
      protections: [],
      equipment: [],
      minimumRequirements: [],
      additional: []
    };

    keyFacts.forEach((element) => {
      switch (element.key_facts_section) {
        case 'PROTECTIONS':
          keyFactsObj.protections.push(element);
          break;
        case 'EQUIPMENT':
          keyFactsObj.equipment.push(element);
          break;
        case 'MINIMUM_REQUIREMENTS':
          keyFactsObj.minimumRequirements.push(element);
          break;
        case 'ADDITIONAL':
          keyFactsObj.additional.push(element);
          break;
        default:
          break;
      }
    });

    return keyFactsObj;
  },
  splitIncluded (policies) {
    return {
      included: policies.filter((policy) => policy.key_facts_included),
      optional: policies.filter((policy) => !policy.key_facts_included)
    };
  },
  mapExtrasEquipmentToKeyFacts (equipment) {
    return equipment.map((extra) => {
      return {
        code: extra.code,
        policy_description: extra.name,
        policy_text: extra.detailed_description,
        description: extra.name,
        formattedRate: extra.status === 'NOT_WEB_BOOKABLE' ? null : this.formatRatePrice(extra),
        key_facts_included: extra.status === 'INCLUDED'
      };
    });
  },
  combinePoliciesWithRates (policies, type, extras) {
    return policies.map((policy) => {
      const policesRates = extras[type] || [];
      let extra = policesRates.filter((item) => {
        return item.code === policy.code;
      });
      let policyExtra = extra.length ? extra[0] : null;

      if (policyExtra) {
        policy.formattedRate = this.formatRatePrice(policy);
      }

      return policy;
    })
  },
  getChargeType () {
    const chargeType = ReservationStateTree.select(ReservationCursors.chargeType).get();
    if (chargeType === 'PAYNOW') {
      return 'PAYNOW';
    } else {
      return 'PAYLATER';
    }
  },
  formatRatePrice (extra) {
    PriceActions.formatRatePrice(extra);
  }
};

var KeyRentalFactsActions = {
  toggleKeyRentalFactsModal (bool) {
    ReservationStateTree.select(ReservationCursors.keyFactsModal).set('modal', bool);
  },
  toggleDetailView (bool, code) {
    const keyFactsPolicies = ReservationStateTree.select(ReservationCursors.keyFactsPoliciesFromReservation).get();
    const equipment = ReservationStateTree.select(ReservationCursors.equipment).get();


    let data = keyFactsPolicies.find((policy) => code === policy.code) ||
      equipment.included.find((policy) => code === policy.code) ||
      equipment.optional.find((policy) => code === policy.code);

    ReservationStateTree.select(ReservationCursors.keyFactsDetail).set('show', bool);
    ReservationStateTree.select(ReservationCursors.keyFactsDetail).set('data', data);
  },
  toggleDetailViewForExclusion (bool, code) {
    const keyFactsPolicies = ReservationStateTree.select(ReservationCursors.keyFactsPoliciesFromReservation).get();

    const data = keyFactsPolicies.find((policy) => code === policy.code);

    ReservationStateTree.select(ReservationCursors.keyFactsDetail).set('show', bool);
    ReservationStateTree.select(ReservationCursors.keyFactsDetail).set('data', data.policy_exclusions[0]);
  },
  setKeyFactsBrand (brand) {
    ReservationStateTree.select(ReservationCursors.keyFactsBrand).set(brand);
  },
  setKeyFacts () {
    // I don't really know how to refactor this
    let extras = {};
    let keyFactsPolicies = [];

    if (!extras || !extras.equipment){
      keyFactsPolicies = ReservationStateTree.select(ReservationCursors.keyFactsPoliciesFromReservation).get();
      extras = ReservationStateTree.select(ReservationCursors.extrasView).get();
    }

    const keyFactsObj = KeyRentalFactsHelpers.splitKeyFacts(keyFactsPolicies);
    const protections = KeyRentalFactsHelpers.splitIncluded(keyFactsObj.protections);
    let equipment;

    if (extras) {
      protections.optional = KeyRentalFactsHelpers.combinePoliciesWithRates(protections.optional, 'insurance', extras);

      if (extras.equipment) {
        equipment = KeyRentalFactsHelpers.mapExtrasEquipmentToKeyFacts(extras.equipment);
        equipment = KeyRentalFactsHelpers.splitIncluded(equipment);
      }
    }

    const additional = keyFactsObj.additional;

    const updateObj = {
      policies: {
        protections: {
          included: {
            $set: protections.included
          },
          optional: {
            $set: protections.optional
          }
        },
        equipment: {
          included: {
            $set: (equipment ? equipment.included : [])
          },
          optional: {
            $set: (equipment ? equipment.optional : [])
          }
        },
        minimumRequirements: {
          $set: keyFactsObj.minimumRequirements
        },
        additional: {
          $set: additional
        }
      }
    };

    ReservationStateTree.select(ReservationCursors.keyFactsModal).update(updateObj);
  },
  setDisputesContactInfo (data, countryCode) {
    let disputesCursor = ReservationStateTree.select(ReservationCursors.disputes);

    let countryArray = data.filter((country) => country.country_code === countryCode);

    if (!countryArray.length) return false;

    let contactInfo = countryArray[0];

    let updateObj = {
      email: {
        $set: contactInfo.key_facts_dispute_email || null,
      },
      phone: {
        $set: contactInfo.key_facts_dispute_phone || null
      }
    };

    disputesCursor.update(updateObj);
  }
};

module.exports = KeyRentalFactsActions;
