import ReservationCursors from '../cursors/ReservationCursors';

const ExtrasActions = {
  push: {
    updateQueue: (currentQueueLength) => {
      ReservationStateTree
        .select(ReservationCursors.updateQueue)
        .push(currentQueueLength);
    }
  },
  get: {
    updateQueue: () => {
      return ReservationStateTree
              .select(ReservationCursors.updateQueue)
              .get();
    },
    protectionsRequired: () => {
      return ReservationStateTree
              .select(ReservationCursors.extrasProtections)
              .get('protectionsRequiredAtCounter');
    },
    insuranceExtras: () => {
      return ReservationStateTree
              .select(ReservationCursors.insuranceExtras)
              .get();
    },
    cursorBySelect: (cursor, enterOneLevelTree) => {
      return ReservationStateTree
              .select(ReservationCursors[cursor])
              .get((enterOneLevelTree || ''))
    }
  },
  set: {
    blockSubmit (bool) {
      ReservationStateTree
        .select(ReservationCursors.extrasView)
        .set('blockSubmit', bool);
    },
    reservationStepCommit (bool) {
      ReservationStateTree
        .select(ReservationCursors.reservationSteps)
        .set('commit', bool);
    },
    updateQueue (valueToSet) {
      ReservationStateTree
        .select(ReservationCursors.extrasView)
        .set('updateQueue', valueToSet);
    },
    showExtrasProtectionModal (hasInsuranceExtras) {
      ReservationStateTree
        .select(ReservationCursors.showExtrasProtectionModal)
        .set(hasInsuranceExtras);
    },
    selectedExtra (extra, selected) {
      extra.set('selected', selected);
    },
    exclusionExtrasModal (bool, code) {
      ReservationStateTree.select(ReservationCursors.exclusionExtrasModal).set('modal', bool);
      ReservationStateTree.select(ReservationCursors.exclusionExtrasModal).set('code', code);
    },
    protectionsRequiredAtCounter (requiredArray, text) {
      ReservationStateTree.select(ReservationCursors.extrasProtections).set('protectionsRequiredAtCounter', requiredArray);
      ReservationStateTree.select(ReservationCursors.extrasProtections).set('protectionsRequiredAtCounterText', text);
    },
    showExtraDetailsModal (bool) {
      ReservationStateTree.select(ReservationCursors.showExtraDetailsModal).set(bool);
    },
    highlightedExtra (extra) {
      ReservationStateTree.select(ReservationCursors.highlightedExtra).set(extra);
    },
    selectCursorByIndex (cursor, index){
      return ReservationStateTree
              .select(ReservationCursors[cursor])
              .select(index)
    },
    addExtras (type, value){
      ReservationStateTree.select(ReservationCursors.extrasView).set(type, value);
    }
  }
};

export default ExtrasActions;
