import ReservationCursors from '../cursors/ReservationCursors';

const MessageActions = {
  setMessages(messages) {
    ReservationStateTree.select(ReservationCursors.model).set('messages', messages);
  }
};

module.exports = MessageActions;
