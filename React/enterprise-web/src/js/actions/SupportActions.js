import ReservationCursors from '../cursors/ReservationCursors';
import ReservationStateTree from '../stateTrees/ReservationStateTree';

const SupportActions = {
  getSupportPhoneNumberOfType (type) {
    let session = ReservationStateTree.select(ReservationCursors.reservationSession).get(),
      supportLinks = session.supportLinks,
      phoneObj = null;

    if (supportLinks) {
      supportLinks.support_phone_numbers.forEach((phone) => {
        if (phone.phone_type === type) {
          phoneObj = phone;
        }
      })
    }
    return phoneObj;
  }
};

module.exports = SupportActions;
