import CorporateController from '../controllers/CorporateController';

import ReservationCursors from '../cursors/ReservationCursors';

const DeepLinkActions = {
  setDeepLinkInfo (info) {
    ReservationStateTree.select(ReservationCursors.deepLink).set(info);
    // ReservationStateTree.select(ReservationCursors.model).set('deepLink', info);
  },
  setDeepLinkReservation (deepLinkReservation) {
    ReservationStateTree.select(ReservationCursors.deepLinkReservation).set(deepLinkReservation);
    // ReservationStateTree.select(ReservationCursors.model).set('deepLinkReservation', deepLinkReservation);
  },
  setModal (modal) {
    ReservationStateTree.select(ReservationCursors.deepLinkView).set('modal', modal);
  },
  setDeepLinkErrors (messages) {
    ReservationStateTree.select(ReservationCursors.deepLinkView).set('errors', messages);
    CorporateController.errorTriggers(messages, true);
  }
};

module.exports = DeepLinkActions;
