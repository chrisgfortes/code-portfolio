import ReservationCursors from '../cursors/ReservationCursors';
// import RedirectActions from './RedirectActions';
// import LocationSearchController from '../controllers/LocationSearchController';
// import { LOCATION_SEARCH, PAGEFLOW } from '../constants';
// import ErrorActions from './ErrorActions';

const CorporateActions = {
  getContractDetails () {
    return ReservationStateTree.select(ReservationCursors.contractDetails).get();
  },
  setContractDetails (model) {
    ReservationStateTree.select(ReservationCursors.contractDetails).set(model);
  },
  removeContractDetails () {
    ReservationStateTree.unset(ReservationCursors.contractDetails);
  },
  setCorporateState (state) {
    ReservationStateTree.select(ReservationCursors.corporate).set('state', state);
  },
  setTravelPurpose (purpose) {
    ReservationStateTree.select(ReservationCursors.model).set('purpose', purpose);
  },
  setBillingAuthorization (state) {
    ReservationStateTree.select(ReservationCursors.model).set('billingAuthorized', state);
  },
  setPaymentID (payment) {
    ReservationStateTree.select(ReservationCursors.model).set('paymentID', payment);
  },
  setPaymentType (type) {
    ReservationStateTree.select(ReservationCursors.model).set('paymentType', type);
  },
  setDefaultPaymentType (type) {
    ReservationStateTree.select(ReservationCursors.model).set('defaultPaymentType', type);
  },
  setError (error) {
    ReservationStateTree.select(ReservationCursors.corporate).set('error', error);
  },
  setModal (state) {
    ReservationStateTree.select(ReservationCursors.corporate).set('modal', state);
  },
  viewTerms () {
    ReservationStateTree.select(ReservationCursors.viewTermsModalParent).set('modal', true);
  },
  setLockedCIDModal (bool) {
    ReservationStateTree.select(ReservationCursors.lockedCIDModal).set('modal', bool);
  },
  setPin (pin) {
    ReservationStateTree.select(ReservationCursors.model).set('pin', pin);
  },
  matchAdditionalSequences (findSequenceIn, needleSequence, additionalInfoArray) {
    let sequenceValue;
    let foundSequence;
    let infoId;
    // this is terrible code, but this is a questionable feature...
    _.forEach(needleSequence, (sequenceIndex, keyname) => {
      foundSequence = _.find(additionalInfoArray, { sequence: sequenceIndex});
      if (foundSequence) {
        // id of additional_info
        infoId = foundSequence.id;
        // find our key from query string collection
        if (findSequenceIn[keyname] && findSequenceIn[keyname].length > 0) {
          sequenceValue = findSequenceIn[keyname][0];
        }
        if (sequenceValue !== null) {
          this.setAdditionalInfo({
            id: infoId,
            value: sequenceValue
          });
        }
      }
    });
  },
  setCommitRequestData (value) {
    ReservationStateTree.select(ReservationCursors.commitRequestData).set(value);
  },
  setAdditionalInfoFromArray (newFields) {
    if (newFields && newFields.forEach) {
      newFields.forEach(i => this.setAdditionalInfo(i));
    }
  },
  setAdditionalInfo (newField) {
    let fields = ReservationStateTree.select(ReservationCursors.additionalInfo).get();
    fields = fields.filter(field => field && field.id !== newField.id)
                   .concat(newField);
    ReservationStateTree.select(ReservationCursors.additionalInfo).set(fields);
  },
  clearAdditionalInfo () {
    ReservationStateTree.select(ReservationCursors.additionalInfo).set([]);
  },
  setDelivery (variable, value) {
    const obj = {
      [variable]: {
        $set: value
      }
    };

    ReservationStateTree.select(ReservationCursors.delivery).update(obj);
  },
  setCollection (variable, value) {
    const obj = {
      [variable]: {
        $set: value
      }
    };

    ReservationStateTree.select(ReservationCursors.collection).update(obj);
  },
  setCollectionSameAsDelivery (wipe = false) {
    const delivery = ReservationStateTree.select(ReservationCursors.delivery).get();
    console.log('setCollectionSameAsDelivery() - should use a MODEL FACTORY HERE ***************** ');
    const obj = {
      streetAddress: {
        $set: wipe ? null : delivery.streetAddress
      },
      city: {
        $set: wipe ? null : delivery.city
      },
      postal: {
        $set: wipe ? null : delivery.postal
      },
      phone: {
        $set: wipe ? null : delivery.phone
      },
      comments: {
        $set: wipe ? null : delivery.comments
      }
    };

    ReservationStateTree.select(ReservationCursors.collection).update(obj);
  },
  checkForExistingCorporateCode () {
    const corporate = ReservationStateTree.select(ReservationCursors.corporate).get();
    let coupon = ReservationStateTree.select(ReservationCursors.coupon).get();
    const session = ReservationStateTree.select(ReservationCursors.reservationSession).get();
    const loggedIn = session.loggedIn;
    const isDeepLink = ReservationStateTree.select(ReservationCursors.deepLinkReservation).get();
    // use coupon here instead
    // ECR-14251
    let contractNumber = (
      _.get(session, 'profile.basic_profile.customer_details.contract_number') ||
      _.get(session, 'contract_details.contract_number')
    );
    const currentStep = ReservationStateTree.select(ReservationCursors.componentToRender).get();

    if (coupon) {
      coupon = coupon.toLowerCase();
    }
    if (contractNumber) {
      contractNumber = contractNumber.toLowerCase();
    }

    if (window.globalCID) {
      window.globalCID = window.globalCID.toLowerCase();
    }

    // this should only show up for logged users and if he purposely changed the code (not deeplink)
    if ((loggedIn && !isDeepLink) && currentStep !== 'Confirmed' &&
      coupon && (
        (window.globalCID && window.globalCID !== coupon) ||
        (contractNumber && contractNumber !== coupon)
      )
    ) {
      //Checking if no modal exists, this modal takes the LOWEST priority out of all corporate modals
      if (!corporate.modal) {
        CorporateActions.showModalInState('manageCID');
      }
      return false;
    }
    return true;
  },
  showModalInState (state) {
    this.setModal(true);
    this.setCorporateState(state);
  },
  /**
   * This method seems to be called only when retrieving vehicles (@see `CarSelectController.retrieveVehicles`)
   * but this cursor is only mostly on Verification and Confirmation components... we need to check that out
   */
  setPromoCodeApplicable (bool) {
    ReservationStateTree.select(ReservationCursors.codeApplicable).set(bool);
  }
};

module.exports = CorporateActions;
