import ReservationCursors from '../cursors';
import ReservationStateTree from '../stateTrees';
import { PAGEFLOW, LOCATION_SEARCH } from '../constants';
import { debug } from '../utilities/util-debug';
const logger = debug({isDebug: false, name: 'AppActions'}).logger;

/**
 * Actions specific to overall application status/config/conditions/views
 * @module     {object} AppActions
 */

export default {
  /**
   * @namespace  {object} AppActions.get
   */
  get: {

  },
  /**
   * @namespace  {object} AppActions.set
   */
  set: {

  },
  /**
   * @namespace  {object} AppActions.update
   */
  update: {

  },

  /**
   * Sets the current view.
   * @param      {string}  type    the current view flag, usually from enterprise.currentView
   */
  setCurrentView(type) {
    ReservationStateTree.select(ReservationCursors.currentView).set(type);
  },
  changeView (toView, isLoading) {
    logger.log('changeView(toView, isLoading)', toView, isLoading);
    if (isLoading) {
      this.setLoadingView(toView);
    }
    ReservationStateTree.select(ReservationCursors.view).set('currentView', toView);
  },
  setLoadingView (view) {
    let updateObj = {};
    updateObj[view] = {
      'loading': {
        $set: true
      }
    };
    ReservationStateTree.select(ReservationCursors.view).update(updateObj);
  },
  finishedLoading (view) {
    let updateObj = {};
    updateObj[view] = {
      'loading': {
        $set: false
      }
    };
    ReservationStateTree.select(ReservationCursors.view).update(updateObj);
  },
  triggerError (type, view) {
    let updateObj = {};
    updateObj[type][view] = {
      'error': {
        $set: true
      }
    };

    ReservationStateTree.select(ReservationCursors.view).update(updateObj);
  },

  /**
   * Appears to be location related?
   * I can't find this being used anywhere
   * @return {*}
   */
  clearErrors () {
    logger.warn('clearErrors() is using pickup / dropoff');
    const types = [LOCATION_SEARCH.PICKUP, LOCATION_SEARCH.DROPOFF];
    for (let type in types) {
      const keys = ReservationStateTree.select(ReservationCursors.view).select(type).get();
      for (let key in keys) {
        ReservationStateTree.select(ReservationCursors.view).select(type).select(key).apply(function (item) {
          if (item.hasOwnProperty('error')) {
            item.error = false;
          }
          return item;
        });
      }
    }
  },
  setReservationSession (session) {
    logger.warn('setReservationSession()', session);
    ReservationStateTree.select(ReservationCursors.model).set('reservationSession', session);
  },
  setCurrentHash (hash) {
    ReservationStateTree.select(ReservationCursors.currentHash).set(hash);
  },
  getCurrentHash () {
    return ReservationStateTree.select(ReservationCursors.currentHash).get();
  },
  locationDataReady (bool) {
    ReservationStateTree.select(ReservationCursors.locationSelect).set('dataReady', bool);
  },
  initialDataReady (bool) {
    ReservationStateTree.select(ReservationCursors.initialDataReady).set(bool);
  },
  setLoadingClassName (className) {
    ReservationStateTree.select(ReservationCursors.view).set('loadingClass', className);
  },
  /**
   * @todo rename this to componentRendered as it is past tense ...
   * Basically if you're looking at componentToRender for what WILL RENDER you WILL BE WRONG
   * @param      {string}  component  The component
   */
  setComponentToRender (component) {
    // debugger;
    logger.warn('setComponentToRender()')
    ReservationStateTree.select(ReservationCursors.view).set('componentToRender', component);
  },
  hasDataReady (component) {
    return !!ReservationStateTree.select(ReservationCursors.view).select(component).get('dataReady');
  },
  hasDataReadyByCursor (cursor) {
    return !!ReservationStateTree.select(cursor).get();
  },
  setHasDataReady (component, bool) {
    return ReservationStateTree.select(ReservationCursors.view).select(component).set('dataReady', bool);
  },
  getLastCompleteStepHash () {
    const steps = ReservationStateTree.select(ReservationCursors.reservationSteps).get();
    if (steps.commit == true) {
      return PAGEFLOW.COMMIT;
    } else if (steps.carClass !== null) {
      return PAGEFLOW.EXTRAS;
    } else {
      return PAGEFLOW.CARS;
    }
  }
}
