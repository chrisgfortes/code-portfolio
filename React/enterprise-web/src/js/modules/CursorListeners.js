/**
 * Listen to change events on our Cursors for Baobab. The "get" event doesn't
 * seem to fire, but the "update" works nicely.
 *
 * Primarily, possibly a debugging tool, but it could be extended for much more.
 *
 * Example: go down to listeners() method and enable for a specific cursor
 *  The more specific, the better the results.
 *
 * @module CursorListeners
 *
 * To enable, toggle flag in default.js
 *     if (__DEVELOPMENT__) {
 *        CursorListeners.listeners(true); // <-- set true to enable debugging
 *
 * Properties
 * @property {bool} isDebug locally turn console logging on/off
 * @property {bool} isVerbose turn verbose logging (echo .get() on the tree)
 */
import ReservationStateTree from '../stateTrees/ReservationStateTree';
import ReservationCursors from '../cursors/ReservationCursors';
import ObjectUtil from '../utilities/util-object';
import { Debugger } from '../utilities/util-debug';

const CursorListeners = {
  init (debug, useCache = false) {
    this.isDebug = debug;
    this.useCache = useCache;
    if (this.useCache) {
      window._stateCache = this.cache;
    }
    return this;
  },
  isDebug: true, // disable CursorListener.logger.log and friends
  isVerbose: false, // enable/disable verbose output (echo .get()) to console
  logger: {},
  useCache: false,
  cache: [],
  cacheEvent (obj) {
    if (this.useCache) {
      this.cache.push(obj);
    }
  },
  // despite what the docs say, "get" does not seem to fire
  // getListener (cursor) {
  //   ReservationStateTree.select(ReservationCursors[cursor]).on('get', function(ev) {
  //     CursorListeners.logger.log('get:', ev);
  //   });
  // },
  updateListener (cursor, isExp = false) {
    ReservationStateTree.select(ReservationCursors[cursor]).on('update', function(ev) {
      // let logger = CursorListeners.logger;
      let path = ev.target.path.join('.');
      let name = cursor + ' :: ' + path;
      if (isExp) {
        console.group('Updated: ' + name); // logger not working?
      } else {
        console.groupCollapsed('Updated: ' + name); // logger not working?
      }
      CursorListeners.logger.warn('Update Event:', ev.data); // logger not working?
      let curr = ev.data.data;
      let prev = ev.data.previousData;
      let diff = ObjectUtil.diff(curr, prev, true); // true param outputs the diff
      console.groupEnd(); // logger not working?

      CursorListeners.cacheEvent({
        timestamp: new Date(),
        update: ev.data,
        diff,
        name
      });
    });
  },
  verbose (data = false) {
    ReservationStateTree.on('get', function(e) {
      CursorListeners.logger.log('.get():', e.data.path);
      if (data) CursorListeners.logger.log('Target data:', e.data.data);
    });
  },
  listeners (exe = true) {
    if (!exe) {
      return true;
    }
    if (this.isVerbose) {
      CursorListeners.logger.warn('******* Logger set to verbose. Will output get() events as well. *******');
      this.verbose();
    }
    // models
    // this.updateListener('model');

    // this.updateListener('pickupModel');
    // this.updateListener('pickupMapModel');

    // this.updateListener('dropoffMapModel');
    // this.updateListener('locationSearchType');
    // this.updateListener('pickupTime');
    // this.updateListener('pickupDate');
    // this.updateListener('dropoffTime');
    // this.updateListener('dropoffDate');
    // // this.updateListener('pickupTempDate');
    // this.updateListener('pickupModelDate');
    // this.updateListener('dropoffModelDate');
    // this.updateListener('pickupClosedTime');
    // this.updateListener('dropoffClosedTime');
    // this.updateListener('modelCarSelect');
    // this.updateListener('carSelect'); // both point at view
    // this.updateListener('viewCarSelect'); // both point at view
    // this.updateListener('modelCarSelect');
    // this.updateListener('pickupLocationObj');
    // this.updateListener('dropoffLocationObj');
    this.updateListener('personal');
    // this.updateListener('expedited');
    // this.updateListener('profile');
    // this.updateListener('account');

    // this.updateListener('driverInfo'); // ECR-14250 I don't know the difference, do you?
    // this.updateListener('driverInformation'); // ECR-14250 I don't know the difference, do you?

    // this.updateListener('inflightModify');

    // this.updateListener('contractDetails');
    // this.updateListener('contractDetailsTEMP');
    // this.updateListener('user');
    // this.updateListener('userProfile');
    // this.updateListener('loyaltyBook');

    // this.updateListener('locationSelect');
    this.updateListener('reservationSession');
    this.updateListener('deepLinkReservation');
    this.updateListener('deepLink');
    // this.updateListener('cancellationDetails');
    this.updateListener('componentToRender');
    // this.updateListener('currentView');
    // this.updateListener('currentHash');

    // this.updateListener('filterDropdown');
    // views
    // this.updateListener('view');
    // this.updateListener('locationSelect');
    // this.updateListener('pickupLocationSelect');
    // this.updateListener('dropoffLocationSelect');
    // this.updateListener('pickupTarget');
    // this.updateListener('dropoffTarget');
    // this.updateListener('pickupInvalidTime');
    // this.updateListener('dropoffInvalidTime');
    // this.updateListener('pickupInvalidDate');
    // this.updateListener('dropoffInvalidDate');
    // this.updateListener('currentView');
    // this.updateListener('viewCarSelect');
  }
};

CursorListeners.logger = new Debugger(window._isDebug, CursorListeners, true, 'CursorListeners');

module.exports = CursorListeners.init(true, true);
