/**
 * Event Management
 * Usually CustomEvents
 * @type {Object}
 */

// @todo: temporary ... listening for global errors
$('html').on('errors', function () {
  console.warn('Error triggered:', arguments);
  window._err.push(arguments);
});

const Events = {
  names: [],
  listener (scope, name, callback) {
    [scope].addEventListener(name, callback);
  },
  trigger (scope, customEvent) {
    [scope].dispatchEvent(customEvent);
  },
  customEvent: {
    make (name, data) {
      return new CustomEvent(name, {
        detail: data,
        bubbles: true,
        cancelable: false
      });
    }
  },
  // load : {
  //   now() {
  //
  //   },
  //   domload() {
  //
  //   },
  //   pageload() {
  //
  //   },
  //   deferredload() {
  //
  //   }
  // },
  // init(scope) {
  // @todo: use something like this for management of page lifecycle
  init() {
    // load events
    // this.load.now();
    // this.load.domload();
    // this.load.pageload();
    // this.load.deferredload();

    // global events
    // scoped events
    // etc.
  }
};

export default Events;
