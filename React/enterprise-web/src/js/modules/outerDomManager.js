import Loader from './LazyLoad';
import { FILEPATHS } from '../constants';
import Scroll from '../utilities/util-scroll';
/**
 * @module Outer DOM Manager
 * Exactly what it sounds like. A module to manage the DOM outside our application.
 * Usually, this means outside the React Components' scope.
 */

const pageMethods = {
  /**
   * Modal anchor links click call for dangerouslyset HTML
   *@memberOf pageMethods
   */
  modalAnchorClick(element) {
    let parentElement = document.getElementsByClassName(element)[0];
    let anchorTags = parentElement.getElementsByTagName('A');
    for(let i=0; i<anchorTags.length; i++) {
      let anchorHref = anchorTags[i].getAttribute('href');
      if(anchorHref && anchorHref.includes('#')) {
        anchorTags[i].onclick = function (event) {
          event.preventDefault();
          Scroll.scrollTo(`[name="${anchorHref.substr(1)}"]`, null, true);
        };
      }
    }
  },
  /**
   * OnClick call
   * @memberOf pageMethods
   */
  onClick(element, className, method) {
    $(element).on('click', className, method);
  },
  /**
   * OffClick call
   * @memberOf pageMethods
   */
  offClick(element, className, method) {
    $(element).off('click', className, method);
  },
  /**
  * Set Expedited Heading focus
  * @memberOf pageMethods
  */
  setExpeditedHeadingFocus(expeditedHeading) {
    expeditedHeading.setAttribute('tabIndex', '-1');
    expeditedHeading.focus();
  },
  /**
   * Call Expedited Enroll Continue Button
   * @memberOf pageMethods
   */
  callExpediteEnrollBtn() {
    $('#enroll .preferences .form-actions .btn.continue').trigger('click');
  },
  /**
   * Call Review Submit Button
   * @memberOf pageMethods
   */
  callReviewBtn() {
    $('#reviewSubmit').trigger('click');
  },
  /**
   * Toggle Login Widget
   * @memberOf pageMethods
   */
  loginToggleMenu() {
    $('.primary-nav > li, .utility-nav > li').removeClass('active');
    document.getElementById('mobile-nav').classList.remove('active');
  },
  /**
   * Reset portions of the page based on Fedex Settings
   * @see fedexResetDefaults
   * @returns {boolean}
   * @memberOf pageMethods
   */
  fedexPage() {
    // from session service/controller does stuff
    $('.header-logo-link').prop('href', enterprise.aem.path + FILEPATHS.fedexCorpFlow);
    $('#utility-nav').addClass('hide-header'); //ECR-11405
    $('#mobile-nav').addClass('hide-header'); //ECR-11310
    $('body').addClass('is-fedex'); // ECR-11310
    return true;
  },
  /**
   * clean up the settings from fedexPage()
   * @see fedexPage()
   * @returns {boolean}
   * @memberOf pageMethods
   */
  fedexResetDefaults() {
    // from session service/controller does stuff
    $('#utility-nav').removeClass('hide-header'); //ECR-11405
    $('#mobile-nav').removeClass('hide-header'); //ECR-11310
    // really should have started with this approach
    $('body').removeClass('is-fedex'); // ECR-11310  }
    return true;
  },
  /**
   * setting up marketing and loyalty page elements
   * SessionCongtroller fires this on page load, and
   * @param {array} args
   * args[0] {boolean} do-no-market (ReservationStatusFlags.hasMarketingMessage)
   * args[1] {boolean} loyalty-sign-up eligible (ReservationStatusFlags.loyaltySignUpEligible)
   * @memberOf pageMethods
   * @todo: drop jQuery
   */
  marketingAndLoyalty(...args) {
    // console.log('pageMethods: ', args[0], args[1], $(DomCache.$body));
    // what's the point of caching a lookup if we cast it into jQuery, right?
    $(DomCache.$body)
      .toggleClass('do-not-market', args[0])
      .toggleClass('loyalty-not-available', args[1]);
  },
  /**
   * Called by SessionController current-session
   * ECR-10273 + ECR-11325
   * Fallback for the pages that don't use the ReservationHeader React component
   * @todo Find out why pages like Confirmation Page are not using the react component, which
   *   leads to not showing the Home Modal when clicking on the logo.
   * @todo: drop jQuery
   */
  resetHeaderBook() {
    $(DomCache.$body).find('#primaryHeader .logo a').attr('href', '#book');
  },
  /**
   * Called when Inflight Modal is toggled
   */
  toggleInflightModal(isModalOpen) {
    const element = DomCache.$body;

    if(isModalOpen){
      element.setAttribute('aria-hidden', 'true');
      element.setAttribute('style', 'overflow: hidden;');
    }else{
      element.removeAttribute('aria-hidden');
      element.removeAttribute('style');
    }
  }
};

export function LazyLoadUpdate() {
  setTimeout(() => {
    Loader.update();
  }, 250);
}

// have mixed feelings about exporting this...
export let DomCache = {
  $head: (()=>{
    return document.getElementsByTagName('head')[0];
  })(),
  $body: (()=>{
    return document.getElementsByTagName('body')[0];
  })()
};

export let DomFlags = {
  $ecLoyaltyPage: (()=>{
    return $('body').hasClass('ecloyaltypage');
  })()
};

export let DomQueryElement = {
  $getQueryElement: (element)=>{
    return document.querySelector(element);
  }
};

export default {
  trigger(category, ...rest) {
    if (pageMethods[category]) {
      pageMethods[category](...rest);
      return true;
    } else {
      throw Error(`Unknown outerDomManager.trigger()${category}`);
    }
  }
}
