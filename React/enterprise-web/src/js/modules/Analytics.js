/**
 * Note the usage of window.CustomEvent in this file.
 * We include a polyfill in our prod-libs.js file.
 * @module Analytics
 * @type {Object}
 */
import { Enterprise } from './Enterprise';
import { debug } from '../utilities/util-debug';

let lytics = Enterprise.global.pages.analytics;
const JQUERY = true;
const CUSTOM = false;

const Analytics = debug({
  name: 'Analytics.js',
  isDebug: false,
  logger: {},
  /**
   * Initialize our Analytics Event System
   */
  init() {
    // this.logger.log('analytics init()', lytics.length);
    // this.logger.log(lytics);
    if (JQUERY && lytics.length) {
      this.patchEvents(lytics);
    }
  },

  /**
   * Update the analytics object with solr search results
   * @param  {data} solr results
   * @todo: change to use CustomEvent
   */
  trackLocations(locations) {
    if (window._analytics) {
      window._analytics.solr = {};
      Object.assign(window._analytics.solr, locations);
      $('html').trigger('analytics-locations-searched');
    }
  },

  /**
   * Update the global analytics, usually from the session
   * @param analyticsData
   * @param enterpriseData
   */
  ready(analyticsData, enterpriseData) {

    this.logger.log('Analytics.ready() analyticsData:', analyticsData);
    this.logger.log('Analytics.ready() enterpriseData', enterpriseData);

    let $html = $(document.documentElement);
    window._analytics = analyticsData;

    this.logger.log('Analytics Module: window._analytics:', window._analytics);

    window._analytics.pageInfo = enterpriseData;
    window._analytics.ready = true;
    this.trigger($html, 'analytics');
  },

  /**
   * Check if object has these properties required for events to fire.
   * @param obj
   * @returns {boolean}
   */
  hasProps(obj) {
    let result = false;
    if ((obj.hasOwnProperty('el')) && (obj.hasOwnProperty(('trigger'))) && (obj.hasOwnProperty('callback'))) {
      result = true;
    }
    return result;
  },

  /**
   * Here we patch CustomEvent with jQuery-based events.
   * @param lytics enterprise.pages.analytics array (basepage/headlibs.html)
   */
  patchEvents(lytics) {
    lytics.forEach((curr) => {
      this.logger.log('patchEvents()', curr);
      if (this.hasProps(curr)) {
        // this sets a jQuery listener
        $(curr.el).on(curr.trigger, curr.callback);
      } else {
        this.logger.warn('props missing?');
      }
    });
  },

  /**
   * Create a CustomEvent with the given name (and optionally data)
   * @param evtName event name
   * @param data data in format { details: value }
   * @returns {CustomEvent}
   */
  createEvent(evtName, data) {
    return new CustomEvent(evtName, data);
  },

  /**
   * Actually fire the CustomEvent
   * @param scope (element)
   * @param evt event name
   */
  fire(scope, evt) {
    scope.dispatchEvent(evt);
  },

  /**
   * Check if a given object is a jQuery object
   * @param el
   * @returns bool
   */
  elementIsjQuery(el) {
    let res = false;
    if (el.selector || el.context) {
      res = true;
    }
    return res;
  },

  /**
   * Trigger an Analytics / Metrics event
   * We backwards compat cover for jQuery-based DTM events based on the JQUERY constant.
   * @param el element that has the event bound
   * @param evt event name
   * @param data event data.detail (detail defaults to null)
   */
  trigger(el, evt, data = null) {
    // this.logger.group('Trigger analytics event:');
    // this.logger.log(el);
    // this.logger.log(evt);
    // this.logger.log(data);
    // this.logger.groupEnd();

    let eventToFire;
    let element;

    if (JQUERY) {
      this.logger.log(`JQUERY fired event on ${el} to send trigger "${evt}"`);
      if (typeof data !== 'undefined') {
        $(el).trigger(evt, data);
      } else {
        $(el).trigger(evt);
      }
    }

    if (CUSTOM) eventToFire = this.createEvent(evt, { detail: data });

    if (typeof el === 'object' && this.elementIsjQuery(el)) {
      this.logger.log('was a jquery object');
      element = el.get(0);
    } else if (typeof el === 'string') {
      this.logger.log('was NOT a jquery object [string]');
      element = document.getElementsByTagName(el)[0];
    } else {
      this.logger.log('was NOT a jquery object');
      element = el;
    }

    if (CUSTOM) this.fire(element, eventToFire);
  }
});

export default Analytics;
