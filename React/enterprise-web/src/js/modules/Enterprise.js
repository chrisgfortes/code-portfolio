/**
 * @Todo: Begin to remove references to window.enterprise in the code base (see Enterprise.js)
 *          PLEASE REFERENCE main.js FOR LONGER TERM WORK
 * Ideally, one day, this becomes the ONE AND ONLY PLACE that we reference window.enterprise
 */
// import Settings
// import Environment
// import Globals
// import anything else we want to extend everywhere

const Enterprise = {
  init(ent) {
    // same just for convenience (and migration)
    this.enterprise = ent;
    this.global = ent;
    return this;
  },
  enterprise: {},
  global: {}
}.init(window.enterprise);

export { Enterprise }
