/**
 * FocusManager
 * Manage focus on various types of elements.
 * There are few cases where we need to manage focus state using
 * any sort of React.js feature set or even context, so here we
 * use native DOM methods in most cases.
 * Please avoid jQuery if you're extending this.
 * @type {Object}
 */
const FocusManager = {
  /**
   * focus()
   * Take a DOM element and focus it.
   * @param  {DOMElement} domEl [native DOM element reference]
   * @return {void}
   */
  focus (domEl) {
    domEl.focus();
  },

  /**
   * @return {[type]} [first element found by selector]
   */
  narrowBySelector (selector) {
    let found = document.querySelector(selector);
    return found;
  },

  /**
   * Given a list of selectors, find the first one found
   * @param  {string} selectors comma separated list of selectors bound for
   *                            document.querySelector()
   * @return {DOM Element}           Returns found DOM element, if available.
   */
  focusByFirstFoundElement (selectors) {
    let found = this.narrowBySelector(selectors);
    if (found !== null) {
      this.focus(found);
      return found;
    } else return null;
  },

  /**
   * @param {nodeList} node list
   * @return {[type]} [Returns found DOM element, if available]
   */
  byEnabledElement (nodeList) {
    let inputArray = [].slice.call(nodeList);
    let inputToFocus = inputArray.filter((input) => !input.hasAttribute('disabled'))[0];
    this.focus(inputToFocus);
    return inputToFocus;
  }
}

export default FocusManager;
