import Cookie from '../utilities/util-cookies';
import BookingWidgetModelController from '../controllers/BookingWidgetModelController';

const HashModalIngest = {
  ingest() {
    let hash = location.hash.replace(/\?.*$/, '');
    hash = hash.replace('#', '');
    let hashModal = enterprise.hashModal;

    if (!Cookie.get(hash)) {
      if (hash === 'burnttree' || hash === 'atesa' || hash === 'citer' || (hashModal && hashModal.hasOwnProperty(hash))) {
        if(hashModal[hash]) {
          let hashModalObject = hashModal[hash];
          //set type as AEM so that switch picks up on it
          BookingWidgetModelController.setRightPlaceType('aem');
          //put entire object into refer, can also individually set them
          BookingWidgetModelController.setRightPlaceRefer(hashModalObject);
          //only show if enable modal is true
          BookingWidgetModelController.setRightPlaceModal(hashModalObject.enableModal);
        } else {
          BookingWidgetModelController.setRightPlaceType('type', hash);
          BookingWidgetModelController.setRightPlaceModal('modal', true);
        }
      }
    }
  }
};

export default HashModalIngest;
