/**
 * Globals are evil and should be treated as such.
 * One day we hope this file goes away.
 * This seems excessive, but there could be a need to manipulate these values in
 * various ways, e.g. Isomorphic rendering, Unit Tests, etc.
 *
 * Longer term we need ways to combine scopes with AEM sourced code,
 * better than what we have now. Data attributes etc.
 *
 * This will also be used to help monkey-patch (not in the literal sense) dependencies when
 *  prod-libs/external-libs are moved to managed dependencies
 *
 */

const Scopes = {
  init (obj) {
    this.global = obj;
    return this;
  },
  global: {},
  modules: {},
  export (pubName, obj) {
    this.modules[pubName] = obj;
    this.global[pubName] = obj;
    return obj;
  }
};

// module.exports = Scopes.init(window);
module.exports = Scopes;
