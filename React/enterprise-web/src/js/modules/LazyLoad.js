import { debug } from '../utilities/util-debug';
const logger = debug({isDebug: false, name: 'LazyLoad'}).logger;
/**
 * We can lazy load images that are scrolled way off the screen.
 * http://www.andreaverlicchi.eu/lazyload/#api
 */
let lazyInstance;

// @private internal for testing only
function showSize(el) {
  return (el.src.indexOf('1280') !== -1 || el.src.indexOf('680') !== -1)
}

let baseSettings = {
  elements_selector: ".lazy",
  threshold: 300,
  throttle: 150,
  class_loading: "lazy-loading",
  class_loaded: "lazy-loaded",
  class_error: "lazy-error",
  class_initial: "lazy-initial",
  skip_invisible: false//,
  // callback_set(el) {
  //   // logger.log('element found:', arguments[0].src);
  //   // logger.log('element processed: ', el);
  // },
  // callback_processed() {
  //   // logger.log('callback_processed');
  // },
  // callback_load() {
  //   logger.log('...loaded!', arguments[0].src.split('/').pop());
  // }
}

const Loader = {
  lazyLoader: null,
  instances: [],
  baseLazyInit() {
    if(window.top.CQ) {//don't lazy load if not in edit mode
      this.addListeners();
      return this.authorFix();
    }
    lazyInstance = new Loader.lazyLoader(baseSettings);
    Loader.instances.push(lazyInstance);
  },
  init(lazyLoader) {
    Loader.lazyLoader = lazyLoader;
    logger.log('init()');
    Loader.baseLazyInit();
    // Loader.sliderInit();
    Loader.sliderWatcher();
  },
  update() {
    logger.log('...calling update by proxy');
    lazyInstance.update(); // works against the last one init'd
  },
  sliderWatcher() {
    logger.log('sliderWatcher()');
    // a hack described in the API docs to watch for an arbitrary element and instantiate another lazy loader
    new Loader.lazyLoader({
      elements_selector: 'div.carousel-container',
      threshold: 300,
      callback_set() {
        logger.log('sliderWatcher.callback_set()')
        Loader.sliderInit()
      }
    })
  },
  // sliderInit(el = document.querySelector('div.slide-show')) {
  sliderInit(el = document.querySelector('div.carousel-container')) {
    logger.log('LazyLoader.sliderInit()')

    let slideConfig = Object.assign({}, {
      container: el,
      threshold: 1300,
      skip_invisible: false
      // callback_set(el) {
      //   if (showSize(el)) {
      //     logger.log('—— slider:: element found:', arguments[0].src);
      //   }
      // },
      // callback_processed() {
      //   logger.log('—— slider:: callback_processed');
      // },
      // callback_load(el) {
      //   if (showSize(el)) {
      //     logger.log('—— slider:: loaded!', arguments[0].src.split('/').pop());
      //   }
      // }
    })
    lazyInstance = new Loader.lazyLoader(slideConfig);
    Loader.instances.push(lazyInstance);
    logger.log('instances ... ', Loader.instances.length);
  },
  initSliders() {
    logger.log('initSliders()');
    Loader.sliderInit();
  },
  authorFix() {
    let img= Array.from(document.getElementsByTagName('img'));
    img.map(i => i.src = i.getAttribute('data-original') ? i.getAttribute('data-original') : i.src);
  },
  addListeners() {
    /*
     * ECR-13579 && ECR-13608 - this function adds a listener onto the iframe in the
     * AEM authoring context that contains the whole site in author mode (this is what document.body does)
     * If there're changes involving an image with lazy loading applied, it runs authorFix() after the changes
     * are made and the element is in the DOM
     */
    let m = new MutationObserver(mutations => {
      //check type childList to listen for addition/removal of node children, match target with lazy to run authorFix as little as possible
      if(mutations.some(mutation => mutation.type=== "childList" && mutation.target.innerHTML.match(/lazy/))) {
        return this.authorFix();
      }
    });
    //listen on document.body to listen for changes inside of containing iframe
    m.observe(document.body, {
      childList: true,
      subtree: true
    });
  }
};

export default Loader;
