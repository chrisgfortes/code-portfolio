/**
 * This should be done a different way, I think.
 * @requires   jQuery
 * @return {Function} Called by some AEM components.
 * For the time of this feature's development, we handle two types of image-containers accross
 *   article pages:
 *   1. Simple <img> tags
 *   2. Containers with background-image
 * The page must have an element with a class of .canZoom to trigger the feature to work
 */
const zoomHoverBtn = '<button class=\"zoom-button\" aria-label=\"\"><i class=\"icon icon-ico-enlarge-white\"></i></button>';

const tempScroll = {
  scrollTo (id, timing = 200) {
    let element = $(id);
    if (element.length) {
      if (timing === 0) {
        $('html, body').scrollTop(element.offset().top);
      } else {
        $('html, body').animate({
          scrollTop: element.offset().top
        }, timing);
      }
    }
  },
  getTopScroll () {
    return window.pageYOffset || document.documentElement.scrollTop;
  },
  scrollToOffset(scrollOffset) {
    window.scrollTo(0, scrollOffset);
  },
  scrollTop (element, type) {
    $(element).animate({scrollTop: 0}, type);
  }
}

const ZoomableImgs = {
  toggleFullscreenImg(event) {
    // console.log('toggle full screen Img');
    let baseScroll = $(window).scrollTop();
    // console.log('01 $(window).scrollTop():', baseScroll);
    if (event) {
      event.preventDefault();
      event.stopPropagation();
    }

    let $this = $(this);
    let $imgContainer = ($this.hasClass('img-container') || $this.hasClass('image-container')) ? $this : $this.parent();
    let $body = $('body');
    const isZoomingIn = !$imgContainer.hasClass('zoomed-in');
    let $imgSizeReference = $imgContainer.children('.zoomable-thumbnail-imgsizereference');
    let $bigImg = $imgContainer.children('.zoomable-full-img');

    //  Make sure the zoom button disappears
    $imgContainer.find('.zoom-button').removeClass('onHover');
    $imgContainer.toggleClass('zoomed-in');
    // $body.toggleClass('zoomed-in');

    if (isZoomingIn) {
      // Trigger image request
      $bigImg.attr('src', $bigImg.attr('data-src'));

      // Resize and centralize image
      const imgAspect = $imgSizeReference.height() / $imgSizeReference.width();
      const windowAspect = $(window).height() / $(window).width();
      let leftCentralizeDelta;
      // let topCentralizeDelta;
      if (imgAspect > windowAspect) {
        // Since we probably haven't loaded the full img yet we project how big it'll be
        //  looking at the small image aspect ratio.
        $bigImg.css({height: '100vh', width: 'auto'});
        const projectedImgWidth = $(window).height() / imgAspect;
        leftCentralizeDelta = Math.abs($(window).width() - projectedImgWidth) / 2;
        // topCentralizeDelta = 0;
      } else {
        $bigImg.css({width: '100vw', height: 'auto'});
        // const projectedImgHeight = $(window).width() * imgAspect;
        // topCentralizeDelta = Math.abs($(window).height() - projectedImgHeight) / 2;
        leftCentralizeDelta = 0;
      }
      // $bigImg.css('top', $(window).scrollTop() + topCentralizeDelta);
      $bigImg.css('top', $(window).scrollTop());
      $bigImg.css('left', leftCentralizeDelta);

      // Add close button only once, next time it's just hidden
      if ($imgContainer.children('.close-modal').length === 0) {
        let closeBtnStr = '<button class=\"close-modal\" aria-label=\"' + i18n('resflowcarselect_0065') + '\"><i class=\"icon icon-close-x-white\"></i></button>';
        $imgContainer.append(closeBtnStr);
        let closeBtn = $imgContainer.children('.close-modal');
        closeBtn.on('click.toggleThumb', ZoomableImgs.toggleFullscreenImg);

        // Trap TAB focus on the button
        closeBtn.on('blur', function () {
          $(this).focus();
        });
      }

      // Accessibility
      $imgContainer.children('.close-modal').focus();

      // Move node to uppermost parent
      let $spot = $imgContainer;
      $imgContainer = $imgContainer.clone(true);
      $body.prepend($imgContainer);
      $spot.replaceWith('<span id="imgContainerMemory"/>');

      // Exit zoomed view upon scrolling out
      const scrollOrigin = $(window).scrollTop();
      setTimeout(()=> {
        $(window).on('scroll.zoomableCB', function () {
          const currentScroll = $(window).scrollTop();
          if (Math.abs(scrollOrigin - currentScroll) > 100) {
            ZoomableImgs.toggleFullscreenImg.call($imgContainer);
          }
        });
      }, 500);
    } else {
      // Move the node back to its original place
      let $memoryNode = $('#imgContainerMemory');
      $memoryNode.replaceWith($imgContainer);

      $(window).off('scroll.zoomableCB');
    }

    // Trigger animations only in the end
    // ECR-13728 this causes the position to jump as it has not applied the positioning until this is applied
    // $imgContainer.toggleClass('zoomed-in');
    $body.toggleClass('zoomed-in');
    window.scrollTo(0, baseScroll);
  },
  init() {
    // console.log('ZoomableImgs.init()');
    // let imgs = this.setupImgs();
    this.setupImgs();
    // let bgs = this.setupBgs();
    this.setupBgs();
    // if (imgs && bgs) {
    this.callbacks();
    // }
  },
  reinit() {
    let $canZoom = $('.canZoom');
    if ($canZoom.length > 0) {
      this.init();
    }
  },
  getSrc($img) {
    let $src = $img.attr('src');
    let src;
    // console.group();
    // console.log($img);
    if ($src.indexOf('data') === -1) {
      // console.log('src: ', $src);
      src = $src;
    } else {
      // console.warn('no src on img')
      $src = $img.attr('data-original');
      if ($src) {
        // console.warn('...but we have a data original!', $src);
        src = $src;
      } else {
        console.error('...possible broken Zoomable Image!');
      }
    }
    // console.groupEnd();
    return src;
  },
  srcImg($v) {
    let $src = ZoomableImgs.getSrc($v);
    // console.log($src);
    $v
      .clone()
      .addClass('zoomable-full-img')
      .attr('data-src', $src.replace('1280.720', '1920.1080'))
      .attr('src', '')
      .insertAfter($v);

    $v.addClass('zoomable-thumbnail');
    $v.addClass('zoomable-thumbnail-imgsizereference');
    // console.log('zoomHoverBtn', zoomHoverBtn);
    $v.after(zoomHoverBtn);
  },
  // CASE 1: Set up classes and attributes for img tags.
  setupImgs() {
    $('.image-container img').each(function (i, v) {
      let $v = $(v);
      ZoomableImgs.srcImg($v);
    });
    return true;
  },
  // CASE 2: Set up classes and attributes for tags with background-image.
  setupBgs() {
    $('.image-container').each(function (i, v) {
      let $v = $(v);
      ZoomableImgs.bgImg($v);
    });
    return true;
  },
  bgImg($v) {
    let imgUrl = $v.css('background-image');
    if (imgUrl !== 'none') {
      // Remove url() or in case of Chrome url("")
      imgUrl = imgUrl.match(/^url\("?(.+?)"?\)$/)[1];

      // Append full-sized image
      $('<img />', {
          'data-src': imgUrl.replace('1280.720', '1920.1080'),
          src: '',
          'class': 'zoomable-full-img',
          alt: $v.attr('alt')
        })
        .appendTo($v);

      // Append small sized image just for checking its size, since we can't know the actual aspect
      //   ratio based on a background-image with "background-size: cover".
      $('<img />', {
          'src': imgUrl,
          'class': 'zoomable-thumbnail-imgsizereference',
          'style': 'display: none;'
        })
        .appendTo($v);

      $v.addClass('zoomable-thumbnail');
      $v.append(zoomHoverBtn);
    }
  },
  callbacks() {
    // Register click callbacks on everyone
    let zoomables = $('.zoomable-thumbnail, .zoomable-full-img');
    zoomables.off('click.toggleThumb');
    zoomables.on('click.toggleThumb', this.toggleFullscreenImg);

    // Register hover effects on thumbnails
    let zoomablesThumbs = $('.zoomable-thumbnail');
    zoomablesThumbs.off('mouseenter.zoom-button, mouseleave.zoom-button');
    zoomablesThumbs.on('mouseenter.zoom-button, mouseleave.zoom-button', function (e) {
      if (e.type === 'mouseenter') {
        $(this).parent().find('.zoom-button').addClass('onHover');
      } else if (e.type === 'mouseleave') {
        $(this).parent().find('.zoom-button').removeClass('onHover');
      }
    });
  }
};

export default ZoomableImgs;

