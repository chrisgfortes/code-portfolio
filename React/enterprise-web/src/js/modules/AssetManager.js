/* global navigator */
/**
 * AssetManager.js
 * For loading assets.
 * NOTE: Constants usually we import from constants but this file is
 * included in prod-libs/external-libs, so to save bytes ...
 */
import FILEPATHS from '../constants/filepaths';
import { debug } from '../utilities/util-debug';
import TimeStamp from './TimeStamp';

/**
 * Literally could not figure out how to get Filament Group's onloadCSS to work with Browserify.
 * @todo: do something better
 * NOTE: Note my code hence linting issues
 */
/* eslint-disable */
function onloadCSS( ss, callback ) {
  var called;
  function newcb(){
    if( !called && callback ){
      called = true;
      callback.call( ss );
    }
  }
  if( ss.addEventListener ){
    ss.addEventListener( "load", newcb );
  }
  if( ss.attachEvent ){
    ss.attachEvent( "onload", newcb );
  }

  // This code is for browsers that don’t support onload
  // No support for onload (it'll bind but never fire):
  //	* Android 4.3 (Samsung Galaxy S4, Browserstack)
  //	* Android 4.2 Browser (Samsung Galaxy SIII Mini GT-I8200L)
  //	* Android 2.3 (Pantech Burst P9070)

  // Weak inference targets Android < 4.4
  if( "isApplicationInstalled" in navigator && "onloadcssdefined" in ss ) {
    ss.onloadcssdefined( newcb );
  }
}
/* eslint-enable */

const AssetManager = debug({
  name: 'AssetManager',
  isDebug: false,
  logger: {},
  oncssload: onloadCSS,
  // deprecated
  getCSSPath(folder) {
    let clientlibs = FILEPATHS.clientlibs;
    let dist = FILEPATHS.dist;
    let result = '';
    if (folder === 'clientlibs') {
      result = clientlibs;
    } else {
      result = dist;
    }
    return result;
  },
  pathClientLibs(category, fileType) {
    let cssPaths = enterprise.clientlibscss;
    let jsPaths = enterprise.clientlibsjs;
    let result = '';
    if (fileType === 'css') {
      result = cssPaths['ecom.' + category];
    } else if (fileType === 'js') {
      result = jsPaths['ecom.' + category];
    }
    return result;
  },
  // new code
  // using clientlibs, we strip the '.css' to get the clientlibs category name
  fileOrCategory: [
    'main',
    'webfonts',
    'cyrillicfonts',
    'icons',
    'static.css'
  ],
  cacheBuster() {
    const versionStringEscaped = unescape(enterprise.buildVersion.replace('\n', ''));
    return '?v=' + versionStringEscaped;
  },
  getFilePath(folder, fileCategory, fileType) {
    let dist = FILEPATHS.dist;
    let result = '';
    if (folder === 'clientlibs') {
      result = this.pathClientLibs(fileCategory, fileType);
    } else {
      result = dist + fileCategory + this.cacheBuster();
    }
    this.logger.log('path to file:', result);
    return result;
  },
  // @temporary (ECR-12359) until some JS rework next sprint (ECR-12360)
  // @todo make this real, based on events
  load(assetGroup, assetLoader = window.loadCSS) {
    if (assetGroup === 'init') {
      TimeStamp.mark('main.css');
      TimeStamp.mark('fonts:css');
      const loadInitCSS = assetLoader(this.getFilePath('clientlibs', this.fileOrCategory[0], 'css'));
      const loadFonts = !enterprise.loadCyrillicFonts ? assetLoader(this.getFilePath('clientlibs', this.fileOrCategory[1], 'css')) : assetLoader(this.getFilePath('clientlibs', this.fileOrCategory[2], 'css'));
      this.oncssload(loadInitCSS, () => {
        TimeStamp.stop('main.css');
      });
      this.oncssload(loadFonts, () => {
        TimeStamp.stop('fonts:css');
      });
    } else if (assetGroup === 'css') {
      assetLoader(this.getFilePath('clientlibs', this.fileOrCategory[3], 'css'));
      assetLoader(this.getFilePath('dist', this.fileOrCategory[4], 'css'));
    }
  },
  /**
   * @todo REMOVE THIS AND FIGURE OUT A BETTER WAY TO DO THIS
   * We should NOT BE using a bunch of random CSS/JS in dist
   * This is here because featurephotoslideshowband.html and photoslideshowband.html had these linked inline
   */
  dangerousList: {
    // note: there's also a JS file
    'slickCSS': FILEPATHS.distRoot + 'vendor/slick/slick.css',
    'slickJS': FILEPATHS.distRoot + 'vendor/slick/slick.min.js'
  },
  dangerouslyLoadJSDependency(dep) {
    if (dep in this.dangerousList) {
      return $.getScript(this.dangerousList[dep]);
    }
  },
  dangerouslyLoadCSSDependency(dep, callback, assetLoader = window.loadCSS) {
    let cb;
    if (dep in this.dangerousList) {
      cb = assetLoader(this.dangerousList[dep] + this.cacheBuster());
      this.oncssload(cb, ()=> {
        callback();
      })
    }
  }
});

module.exports = AssetManager;
