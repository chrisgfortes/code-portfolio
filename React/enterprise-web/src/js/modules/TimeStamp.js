/* global window.performance, performance */
/**
 * User Timing stuff for instrumenting our code base.
 * Contains code by nolanlawson on https://github.com/nolanlawson/marky licensed under the Apache 2.0 license
 * v1.2.0 :: https://github.com/nolanlawson/marky/tree/v1.2.0
 * @todo FOSS request
 * @todo: debug is not working for some reason (hence using Debug.use('enterprise.log')
 * @todo: Enable locationStorage enterprise.settings HACKAGE
 */
import { Debugger, debug } from '../utilities/util-debug';

/**
 * Test for native window.performance object
 * @type {*}
 */
const perform = typeof performance !== 'undefined' && window.performance;
// const perform = false; // disable timing features

/**
 * Do we want to use redundant console.timeStamp values?
 * @type {boolean}
 */
// const REDUNDANT = (!perform);
const REDUNDANT = true;
function noop(){}
let m;

function bam(){
  try {
    return performance.now();
  } catch (e) {
    //
  }
}

let TimeStamp = debug({
  name: 'TimeStamp',
  isDebug: false,
  logger: {},
  logOutput () {
    if (REDUNDANT) {
      if (this.isDebug) Debugger.use('enterprise.log').timeStamp(m);
      // this.logger.timeStamp(m);
    }
    if (this.isDebug) Debugger.use('enterprise.log').warn('TimeStamp: ', m, bam());
    // this.logger.log(m);
  }
});

if (!perform || !perform.mark) {
  TimeStamp.mark = noop;
  TimeStamp.stop = noop;
  TimeStamp.clear = noop;
} else {
  Object.assign(TimeStamp, {
    mark (name) {
      m = `Start ${name}`;
      perform.mark(m);
      this.logOutput(m);
    },
    stop (name, useReturn = false) {
      m = `End ${name}`;
      perform.mark(m);
      perform.measure(name, `Start ${name}`, m);
      this.logOutput(m);
      let res = [];
      if (useReturn) {
        let entries = perform.getEntriesByName(name);
        res = entries[entries.length - 1];
      }
      return res;
    },
    clear () {
      perform.clearMarks(); // native
      perform.clearMeasures(); // native
    }
  })
};

export default TimeStamp;
