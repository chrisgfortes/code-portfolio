import {Enterprise} from './Enterprise';
import TimeStamp from './TimeStamp';
import { Debugger, debug } from '../utilities/util-debug';
import ResponsiveUtil from '../utilities/util-responsive';
import Analytics from './Analytics';
import ReservationCursors from '../cursors/ReservationCursors';
const USEDEBUG = false;

/**
 * Moved from nav.js clientlibs root file nav.js
 * @todo: determine if this is better as a part of content.js / main.js
 * @type {Object}
 */
const headerFunction = function (component) {
  Debugger.use('Navigation.js').log('calling enterprise.utilities.header()', component);
  switch (component) {
    case 'BookingWidget':
      $("#primaryHeader").css({display: "block"});
      $("#reservationHeader").css({display: "none"}).find(".reservation-steps").addClass("collapsed");
      $("#lobLinks").css({display: "none"});
      break;
    case 'DateTimeWidget':
      $("#primaryHeader").css({display: "none"});
      $("#reservationHeader").css({display: "block"});
      $("#lobLinks").css({display: "none"});
      break;
    case 'LocationSearch':
      $("#primaryHeader").css({display: "none"});
      $("#reservationHeader").css({display: "block"}).find(".reservation-steps").removeClass("collapsed");
      $("#lobLinks").css({display: "none"});
      break;
    case 'PickupLocationSearch':
      $("#primaryHeader").css({display: "none"});
      $("#reservationHeader").css({display: "block"}).find(".reservation-steps").removeClass("collapsed");
      $("#lobLinks").css({display: "none"});
      break;
    case 'DropoffLocationSearch':
      $("#primaryHeader").css({display: "none"});
      $("#reservationHeader").css({display: "block"}).find(".reservation-steps").removeClass("collapsed");
      $("#lobLinks").css({display: "none"});
      break;
    case 'CarSelect':
      $("#primaryHeader").css({display: "none"});
      $("#reservationHeader").css({display: "block"}).find(".reservation-steps").removeClass("collapsed");
      $("#lobLinks").css({display: "none"});
      break;
    case 'Addons':
      $("#primaryHeader").css({display: "none"});
      $("#reservationHeader").css({display: "block"}).find(".reservation-steps").removeClass("collapsed");
      $("#lobLinks").css({display: "none"});
      break;
    case 'Verification':
      $("#primaryHeader").css({display: "none"});
      $("#reservationHeader").css({display: "block"}).find(".reservation-steps").addClass("collapsed");
      $("#lobLinks").css({display: "none"});
      break;
    case 'Confirmed':
      $("#primaryHeader").css({display: "block"});
      $("#primaryHeader").find('.currency').css({display: "none"});
      $("#reservationHeader").css({display: "none"}).find(".reservation-steps").removeClass("collapsed");
      $("#lobLinks").css({display: "block"});
      break;
    case 'Modify':
      $("#primaryHeader").css({display: "block"});
      $("#primaryHeader").find('.currency').css({display: "none"});
      $("#reservationHeader").css({display: "none"}).find(".reservation-steps").removeClass("collapsed");
      $("#lobLinks").css({display: "block"});
      break;
    case 'Details':
      $("#primaryHeader").css({display: "block"});
      $("#primaryHeader").find('.currency').css({display: "none"});
      $("#reservationHeader").css({display: "none"}).find(".reservation-steps").removeClass("collapsed");
      $("#lobLinks").css({display: "block"});
      break;
    case 'Cancelled':
      $("#primaryHeader").css({display: "block"});
      $("#primaryHeader").find('.currency').css({display: "none"});
      $("#reservationHeader").css({display: "none"}).find(".reservation-steps").removeClass("collapsed");
      $("#lobLinks").css({display: "block"});
      break;
    default:
      $("#primaryHeader").css({display: "block"});
      $("#reservationHeader").css({display: "none"});
      $("#lobLinks").css({display: "none"});
      break;
  }
};

// Extending Enterprise.global.utilities will extend window.enterprise)
// @todo: have a better way to do this, e.g. an expose() method etc.
Object.assign(Enterprise.global.utilities, { header: headerFunction });

let headerToRender = Enterprise.global.utilities.headerToRender;
if (typeof headerToRender === 'string') {
  Enterprise.global.utilities.header(headerToRender);
}

export default debug({
  name: 'Navigation.js',
  isDebug: USEDEBUG,
  logger: {},
  init() {
    this.logger.log('running Navigation.js');
    TimeStamp.mark('Navigation.js');

    // Caching these for events is a bad idea, we don't know if
    // the React DOM will be rendered before this script executes.
    // It's better to use delegation.
    let $document = $(document);
    let $mobileToggle = $('#mobile-toggle');
    let $primaryNavToggle = $('#primary-nav');
    let $primaryNavSection = $('.primarynav.section');
    let $utilityButtons = $('.utility-nav > li .utility-nav-button');
    let $navItems = $('.primary-nav > li, .utility-nav > li');
    let $navItemTitle = $('.primary-nav .title');
    let $activeSubNavItem = [];
    let $mobileNav = $('#mobile-nav');
    let $footerAccordions = $('.footermenu .title-interactable');
    let $countryLabels = $('.country-select > label');
    let $languages = $('.language-select > div');
    let $modalContainer = $('.modal-container');
    let $modalContent = $modalContainer.find('.modal-body');
    let $modalClose = $modalContainer.find('button.close-modal');
    let $mobileContainer = $('#login-container-mobile');
    let $countryDomainLinks = $('.country-language-href');
    let $navSection = $('.primary-nav .nav-section');
    let $loginWidget = $('.login-widget');
    let $loginButton = $('.login-widget > div.utility-nav-label');
    let $showMoreButtons = $('.show-more-button');

    $countryDomainLinks.on('click', function (e) {
      e.preventDefault();
      if (window.location.host !== this.host) {
        ReservationStateTree.select(ReservationCursors.redirect).set('type', 'click');
        ReservationStateTree.select(ReservationCursors.redirect).set('country', this.href);
        ReservationStateTree.select(ReservationCursors.redirect).set('modal', true);
      } else {
        window.location = this.href + window.location.hash;
      }
    });

//ON TITLE CLICK - intended for mobile
    responsiveToggleNavBar();

    $(window).resize(() => {
      responsiveToggleNavBar();
    });

    function responsiveToggleNavBar(){
      if (ResponsiveUtil.isMobile('(max-width:885px)')) {
        $navSection.attr('aria-expanded', 'false');
        $navItemTitle.on('click', function (e) {
          let $this = $(this);
          let $parent = $this.parent();
          let isSecondClick = $activeSubNavItem[0] === $parent[0];
          e.stopPropagation();

          //are any sections open? -> yup!
          if ($activeSubNavItem.length) {
            $activeSubNavItem.removeClass('open').attr('aria-expanded', false);
            //click on title for sub nav that is already open (second click to close)
            if (isSecondClick) {
              $activeSubNavItem = [];
            } else {
              //first click on title (sub nav currently closed, this will open it)
              $activeSubNavItem = $parent;
              $parent.addClass('open').attr('aria-expanded', true);
            }
          } else {
            //are any sections open? -> nope...
            $parent.addClass('open').attr('aria-expanded', true);
            $activeSubNavItem = $parent;
          }
        });
      }
    }

    let menuTransitionDelay = 300;
    let hideMenuItem = function hideMenuItem($this) {
      $this.closest('li').removeClass('active');
      $this.attr('aria-expanded', 'false');
      setTimeout(function () {
        $this.closest('li').removeClass('visible').trigger('focus');
      }, menuTransitionDelay);
    };
    let showMenuItem = function showMenuItem($this) {
      $this.closest('li').addClass('visible');
      $this.attr('aria-expanded', 'true');
      setTimeout(function () {
        $this.closest('li').addClass('active');
      }, 0);
    };
    let hideAllItemsAndShowItem = function hideAllItemsAndShowItem($this) {
      $navItems.removeClass('active');
      $utilityButtons.attr('aria-expanded', 'false');
      setTimeout(function () {
        $navItems.removeClass('visible');
        showMenuItem($this);
      }, menuTransitionDelay);
    };

//ON LIST ITEM CLICK - for mobile and desktop
    if (typeof a11yClick !== 'undefined') {
      $document.on('click keypress', '.utility-nav > li .utility-nav-button', a11yClick(function (e) {
        let $this = $(this);
        let isMobile = Modernizr.mq('(max-width: 885px)');
        if ($this.hasClass('login') || e.target !== this && !$(e.target).closest('.utility-nav-label').length && !$(e.target).closest('.primary-nav-label').length) {
          return;
        }

        if ($this.attr('aria-expanded') === 'true') {
          hideMenuItem($this);
        } else {
          if (!isMobile) {
            hideAllItemsAndShowItem($this);
          } else {
            showMenuItem($this);
          }
        }
      }));

      $document.on('click keypress', '.currency-button', a11yClick(function () {
        $utilityButtons.attr('aria-expanded', 'false');
        hideMenuItem($(this));
      }));

      $document.on('click keypress', '.primary-nav > li[role="menuitem"]', a11yClick(function () {
        $navItems.not(this).removeClass('active');
        $(this).toggleClass('active');
      }));

      $document.on('keypress', 'label[tabindex]', a11yClick(function (e) {
        if (e.target === this) {
          $(this).trigger('click');
        }
      }));
      $document.on('keypress', '.utility-nav > li div.utility-nav-button', a11yClick(function () {
        if ($loginWidget.hasClass('active')) {
          $loginButton.click();
        }
      }));
    }

    $document.on('keyup', '.utility-nav > li .utility-nav-button', function (e) {
      if (e.keyCode === 27) {
        hideMenuItem($(this));
      }
    });

    $document.on('keyup', '.primary-nav > li[role="menuitem"]', function (e) {
      if (e.keyCode === 27) {
        $(this).removeClass('active').focus();
      }
    });

    if (!Modernizr.touch) {
      $document.on({
        mouseover: function mouseover() {
          $(this).addClass('active');
        },
        mouseleave: function mouseleave() {
          $(this).removeClass('active');
        },
        close: function close() {
          $modalContainer.removeClass('active closable');
          if ($modalContent.children().get(0)) {
            React.unmountComponentAtNode($modalContent.children().get(0));
          }
          $modalContent.empty();
        },
        focus: function focus() {
          $(this).addClass('active');
        },
        blur: function blur() {
          $(this).removeClass('active');
        }
      }, '.reservation-utility-nav .res-utility-nav-label');
    } else {
      if (Modernizr.mq('(max-width: 1024px)')) {
        $document.on('click', function (e) {
          let $target = $(e.target);

          if ($('.primary-item').hasClass('active') && !$target.hasClass('primary-nav-label')) {
            $('.primary-item').removeClass('active');
          }
        });
      }
      $document.on('click', '.reservation-utility-nav .res-utility-nav-label', function (e) {
        let $this = $(this);
        let $target = $(e.target);
        let $resNavItems = $('.reservation-utility-nav .res-utility-nav-label');

        if ($this.hasClass('active') && !$target.closest('.res-utility-nav-content').length) {
          $resNavItems.removeClass('active');
        } else {
          $resNavItems.removeClass('active');
          $this.addClass('active');
        }
      });
    }

    $mobileToggle.on('click', function () {
      if ($mobileToggle.attr('aria-expanded') === 'true') {
        $mobileToggle.attr('aria-expanded', 'false');
      } else {
        $mobileToggle.attr('aria-expanded', 'true');
      }

      $mobileNav.toggleClass('active');
      $primaryNavToggle.toggleClass('active');
      $mobileContainer.removeClass('active');
      $primaryNavSection.removeClass('section');
    });

    $loginWidget.on('click', function () {
      if ($mobileToggle.attr('aria-expanded') === 'true') {
        $mobileToggle.trigger('click');
      }
    });

// Initializes aria-expanded value on footer links list
    let media = window.matchMedia('(max-width: 715px)');
    $footerAccordions.attr('aria-expanded', !media.matches);
    media.addListener(data => {
      $footerAccordions.attr('aria-expanded', !data.matches);
    });

    if (typeof a11yClick !== 'undefined') {
      $footerAccordions.on('click keypress', a11yClick(function () {
        let $this = $(this);
        $this.next('ul').toggleClass('active');

        if ($this.attr('aria-expanded') === 'true') {
          $this.attr('aria-expanded', false);
        } else {
          $this.attr('aria-expanded', true);
        }
      }));
    }

    $countryLabels.on('click', function (e) {
      let $this = $(this);
      let country = $this.data('country');
      let $selectedLanguage = $('.language-select > div[data-country="" + country + ""]');

      e.stopPropagation();
      $countryLabels.removeClass('selected');
      $this.addClass('selected');
      $languages.removeClass('active');
      $selectedLanguage.addClass('active');
    });

    enterprise.utilities.modal = {
      open: function open(htmlString, closable) {
        $modalContent.html(htmlString);
        if (closable) {
          $modalContainer.addClass('active closable');
        } else {
          $modalContainer.addClass('active');
        }
        Analytics.trigger('html', 'modal-open');
      },
      close: function close() {
        $modalContainer.removeClass('active closable');
        if ($modalContent.children().get(0)) {
          React.unmountComponentAtNode($modalContent.children().get(0));
        }
        $modalContent.empty();
      }
    };

    $modalClose.on('click', enterprise.utilities.modal.close);

    enterprise.utilities.symbolMap = {
      'GBP': '£',
      '$': '$',
      'EUR': '€'
    };

    $document.on('authenticated', function (e, data) {
      let searchUrl = 'https://enterprise.custhelp.com';
      let url = [searchUrl, '/app/ask/first_name/', data.first_name, '/last_name/', data.last_name, '/ePlus_number/', data.loyalty_data.loyalty_number].join('');

      $document.find('a[href*="' + searchUrl + '"]').attr('href', url);
    });

// ### Print detection
// Sometimes you need to print specific content or content that overflows
// their parent, which won't be properly printed because of nesting,
// css-overflow and css-position rules, and also because of cross-browser
// and cross-platform issues.
//
// This extracts the content placing a .print-override box on the <body>
// Just switch the css-display to hide/show elements on @media print
//
// Using jQuery custom event because the print dialog won't wait for
// matchmedia listener or onbeforeprint resolution.

    let $html = $('html');
    let $printOverride = [];
    let $activeModal;

    $(window).on('modal-open', function () {
      $activeModal = $document.find('.modal-container.active').first().html();

      if ($activeModal) {
        // print modal content when there's an active modal

        if (!$printOverride.length) {
          $printOverride = $('<div class="print-override"></div>');
          $document.find('body').append($printOverride);
        }

        $printOverride.html($activeModal);
      }
    });

// Disclosure button on content pages
    $showMoreButtons.on('click', function (e) {
      $(e.target).parent().addClass('expanded');
    });
    TimeStamp.stop('Navigation.js');
  }
})
