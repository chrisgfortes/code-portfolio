import { Debugger } from '../utilities/util-debug';

export const carouselContainerHandle = {
  init() {
    Debugger.use('content:init').log('15 carousel');
    let carouselContainer = require('./Carousel/index');
    let $carouselContainer = $('.basic-carousel');
    //DOCUMENT READY
    $(function () {
      if ($carouselContainer.length > 0) {
        let CarouselContainer = Object.create(carouselContainer);
        CarouselContainer.init();
      }
    });
  }
};
