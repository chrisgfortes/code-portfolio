import { Debugger } from '../utilities/util-debug';

export const serviceBandHandle = {
  init() {
    Debugger.use('content:init').log('21 services4upBand');
    let serviceBand = require('./ServiceBand/index');
    let $serviceBand = $('.service-item');
    //DOCUMENT READY
    $(function () {
      if ($serviceBand.length > 0) {
        let ServiceBand = Object.create(serviceBand);
        ServiceBand.init($serviceBand);
      }
    });
  }
}
