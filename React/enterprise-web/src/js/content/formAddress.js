import { Debugger } from '../utilities/util-debug';

export const formAddress = {
  init() {
    Debugger.use('content:init').log('37 Form Address');
    let formAddress = require('./Forms/index');
    let $formAddress = document.getElementById('form-address');

    $(function(){
      if ( $formAddress ) {
        let FormAddress = Object.create(formAddress);
        FormAddress.init($formAddress);
      }
    })
  }
};
