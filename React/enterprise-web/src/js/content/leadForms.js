/**
 * @todo: Is this even used any longer?
 * @type {*}
 */
const Landing = require('../components/LeadForms/Landing');
const FormHandle = document.getElementById('lead-forms');
const GenericBaobabMixinWrapper = require('../components/GenericBaobabMixinWrapper');
const LeadForms = {
  initialize () {
    const self = this;
    if (FormHandle) {
      self.setupLanding();
    }
  },
  setupLanding () {
    React.render(
      <GenericBaobabMixinWrapper
        tree={ReservationStateTree}
        component={Landing}/>,
      FormHandle
    );
  }
};

LeadForms.initialize();


