import { Debugger } from '../utilities/util-debug';
import { Enterprise } from '../modules/Enterprise';

export const travelWayPointsHandle = {
  init() {
    Debugger.use('content:init').log('29 TravelWayPoints');
    let TravelWaypointsBand = require('./TravelWayPointsBand/index');
    let travelWaypointsData = Enterprise.global.pages.travelWaypoints;
    $(function(){
      if (travelWaypointsData) {
        TravelWaypointsBand.init(travelWaypointsData);
      }
    });
  }
};
