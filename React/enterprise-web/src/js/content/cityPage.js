import { Debugger } from '../utilities/util-debug';
import { Enterprise } from '../modules/Enterprise';

export const cityPageHandle = {
  init() {
    Debugger.use('content:init').log('35 City Page');
    let CityPage = require('./CityPage/index');
    let cityData = Enterprise.global.pages.cityPage;
    $(function(){
      if (cityData) {
        CityPage.init(cityData);
      }
    })
  }
};

