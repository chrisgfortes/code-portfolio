import { Debugger, debug } from '../../utilities/util-debug';
import { Enterprise } from '../../modules/Enterprise';
const debugScripts = Enterprise.global.settings.debugContentScripts;

const LocationMap = debug({
  name: 'LocationMap',
  isDebug: debugScripts,
  logger: {},
  init(locationDetail, locationZoom) {
    this.logger.log('external script fired', locationDetail, locationZoom);

    document.addEventListener('readystatechange', function () {
      if (this.readyState === 'interactive') {
        $(".loading").addClass('active');
      } else if (this.readyState === 'complete') {
        $(".loading").removeClass('active');
        $("#map-canvas").addClass('active');
      }
    });

    function init() {
      Debugger.use('LocationMap').log('init() map fired');
      var map;
      var location = locationDetail;
      var suffix = "enterprise";

      var zoom = locationZoom;

      var zoomLevel = zoom ? zoom: 10;
      // var mobile = window.window.matchMedia( "(max-width: 760px)" );
      var latlng = new google.maps.LatLng(location.lat, location.long),
        mapOptions = {
          zoom: zoomLevel,
          center: latlng,
          draggable: !Modernizr.touch,
          scrollwheel: false,
          streetViewControl: false,
          mapTypeControl: false,
          animation: google.maps.Animation.DROP
        };

      map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

      new google.maps.Marker({
        position: latlng,
        map: map,
        title: location.locationName,
        icon: '/etc/designs/ecom/dist/img/icons/png/ENT-mappin-single-' + suffix + '.png'
      });
    }

    google.maps.event.addDomListener(window, 'load', init);

  }
});

module.exports = LocationMap;
