import { Debugger } from '../utilities/util-debug';

export const currencySelectorHandle = {
  init() {
    Debugger.use('content:init').log('13 currencySelector');
    let currencySelector = require('./CurrencySelector/index');
    let $currencySelector = $('#currencySelector');
    //DOCUMENT READY
    $(function () {
      if ($currencySelector.length > 0) {
        let CurrencySelector = Object.create(currencySelector);
        CurrencySelector.init();
      }
    });
  }
};
