import { Debugger } from '../utilities/util-debug';
import { Enterprise } from '../modules/Enterprise';

export const nearbyLocationHandle = {
  init() {
    Debugger.use('content:init').log('31 nearby locations');
    let NearbyLocations = require('./NearbyLocations/index');
    let locationId = Enterprise.global.pages.nearbyLocationsId;
    let locationsHeader = document.getElementById('nearbylocationsDiv');
    $(function(){
      if (locationsHeader && locationId) {
        NearbyLocations.init(locationId);
      }
    });
  }
};
