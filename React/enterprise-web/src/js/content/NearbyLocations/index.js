import { debug } from '../../utilities/util-debug';
import { Enterprise } from '../../modules/Enterprise';
const debugScripts = Enterprise.global.settings.debugContentScripts;

const NearbyLocations = debug({
  name: 'NearbyLocations',
  isDebug: debugScripts,
  logger: {},
  init(locationData) {
    this.logger.log('fetching nearby location data');
    var nearbyLocations = '';
    var locationsDiv = '';
    var locationId = locationData;
    $.ajax({
      url : window.location.href.substring(0, window.location.href.indexOf(".html"))+"/_jcr_content.mapdata.js",
      dataType : "json"
    }).done(function(data){
      nearbyLocations = data;
      var count = 0;
      for(var i = 0; i < nearbyLocations.length; i++) {
        if(nearbyLocations[i].peopleSoftId !== locationId) {
          locationsDiv += "<div class='gi'>" +
            "<div class='cf'>" +
            "<div class='nearby-details'>" +
            "<h4 class='name'>" + nearbyLocations[i].locationNameTranslation + "</h4>" +
            "<div class='address'>" + nearbyLocations[i].addressLines[0] + "</div>" +
            "</div>" +
            "<a class='btn btn-small' href='" + nearbyLocations[i].url + "'>" + enterprise.i18nReservation.resflowlocations_0014 + "</a>" +
            "</div>" +
            "</div>";

          if(++count === 4){
            break;
          }
        }

      }
      if(count > 0) {
        $('#nearbylocationsh2').html(enterprise.i18nReservation.locations_9046);
        $('#nearbylocationsDiv').html(locationsDiv);
      }
    });
  }
});

module.exports = NearbyLocations;
