import { Debugger, debug } from '../../utilities/util-debug';
import { Enterprise } from '../../modules/Enterprise';
import FormAddress from './../../components/Forms/FormAddress';
const debugScripts = Enterprise.global.settings.debugContentScripts;

const FormAddressObject = debug({
  name: 'FormAddress',
  isDebug: debugScripts,
  logger: {},
  init(FormAddressHandle) {
    this.logger.log('external script fired');
    React.render(
      <FormAddress />,
      FormAddressHandle
    );
  }
});

module.exports = FormAddressObject;
