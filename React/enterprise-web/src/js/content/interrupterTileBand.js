import { Debugger } from '../utilities/util-debug';

export const interrupterTileBandHandle = {
  init() {
    Debugger.use('content:init').log('09 interrupterTileBand');
    let interrupter = require('./InterrupterTileBand/index');
    let $interrupterBand = $('.band.lob-page-recirc-band.interrupter-tile.full-bleed');
    let $contentContainer = $('.content-container-content-wrap');
    //DOCUMENT READY (2)
    $(function () {
      if ($interrupterBand.length > 0 && $contentContainer.length > 0) {
          let Interrupter = Object.create(interrupter);
          Interrupter.init($contentContainer);
      }
    });
  }
}
