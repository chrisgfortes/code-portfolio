import { Debugger } from '../utilities/util-debug';

export const accordionHandle = {
  init() {
    Debugger.use('content:init').log('05 accordion.js');
    let accordionHandler = require('./Accordion/index');
    let $accordionHandler = $('.accordion-row');
    //DOCUMENT READY
    $(function () {
      if ($accordionHandler.length > 0) {
        let AccordionHandler = Object.create(accordionHandler);
        AccordionHandler.init();
      }
    });
  },
};

