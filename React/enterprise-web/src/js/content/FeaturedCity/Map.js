import { Debugger } from '../../utilities/util-debug';
export const Map = {
  init () {
    Debugger.use('content:init').log('01 Map.js');
    this.bind();
  },
  bind () {
    $(function(){
      $('a.mapToggle').bind('click',function(e){
        $('.mapToggleBand .map-band').toggleClass('viewMap');
      });
    });
  }
}
