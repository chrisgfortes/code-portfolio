/**
 * @todo: change to use a global media query listener
 * @todo: add checks for dom elements before binding event handlers
 */
import { Debugger } from '../../utilities/util-debug';

export const ListHideShow = {
  init () {
    Debugger.use('content:init').log('02 ListHideShow');
    // this.bind();
    // @todo: fix scope issues here and move this to bind();
    let mql = window.matchMedia('(max-width: 636px)');
    const self = this;
    // @todo: use our responsive util
    if ( $(window).width() < 636) {
      this.initListAccordion();
    }
    mql.addListener(function(data) {
      if(data.matches === true){
        self.initListAccordion();
      }else {
        self.disableListAccordion();
      }
    });
  },
  // bind() {
  //   $(function(){
  //   });
  // },
  initListAccordion () {
    $('.expandable-list').each(function() {
      let max = 6;
      let listSize = $('.expandable-list li').size();
      if (listSize > max) {
        $(this).find('li:gt('+max+')').hide();
        $('.sub-accordion').unbind('click').bind('click', function (e) {
          e.preventDefault();
          $('.expandable-list li:gt('+max+')').toggle(150);
          if ( $('.show_more').length ) {
            $('.expandable-list').addClass('expanded');
            $(this).html(`<div class="show_less"><span aria-hidden="true" role="presentation"><i class="icon icon-nav-carrot-down"></i></span>${i18n('resflowcorporatespecialaccountmessage_0003') || 'Hide All'}</div>`);
          } else {
            $('.expandable-list').removeClass('expanded');
            $(this).html(`<div class="show_more"><span aria-hidden="true" role="presentation"><i class="icon icon-nav-carrot-down"></i></span>${i18n('resflowcorporatespecialaccountmessage_0002') || 'Show All'}</div>`);
          }
        });
      }
    });
  },
  disableListAccordion () {
    $('.sub-accordion').unbind('click');
    $('.expandable-list li').show();
  }
};

