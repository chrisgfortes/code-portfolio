/**
 * @todo: change to use a global media query listener
 * @todo: add checks for dom elements before binding event handlers
 */
import {Debugger} from '../../utilities/util-debug';

export const Base = {
  init () {
    Debugger.use('content:init').log('03 Featured City Base.js');
    // this.bind();
    // @todo: fix scope issues and move to bind();
    let mql = window.matchMedia('(max-width: 636px)');
    const self = this;
    // @todo use responsive utility
    if ($(window).width() < 636) {
      this.initTopScroll();
    } else {
      self.initNeighborhoodLocations();
    }
    mql.addListener(function (data) {
      if (data.matches === true) {
        self.initTopScroll()
      } else {
        self.disableTopScroll();
        self.initNeighborhoodLocations();
      }
    });
  },
  // bind () {
  //   $(function() {
  //   })
  // },
  initTopScroll () {
    // @todo: change to global handler
    $(window).scroll(function () {
      if ($(this).scrollTop() > 300) {
        $('.go-top').fadeIn(300);
      } else {
        $('.go-top').fadeOut(300);
      }
    });

    $('.go-top').bind('click', function (e) {
      e.preventDefault();
      $('html, body').animate({scrollTop: 0}, 1000);
    });

  },
  disableTopScroll() {
    $(window).unbind('scroll');
    $('.go-top').unbind().hide();
  },
  initNeighborhoodLocations() {
    if ($('.airport-list').length) {
      $('.neighborhood-list').addClass('three-column');
    } else {
      $('.neighborhood-list').addClass('four-column');
    }
  }
};
