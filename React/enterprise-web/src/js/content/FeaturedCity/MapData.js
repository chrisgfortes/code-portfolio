import { PAGEFLOW, LOCATION_SEARCH } from '../../constants';
import { Debugger, debug } from '../../utilities/util-debug';
import LocationActions from '../../actions/LocationActions'; 

// @Todo: Refactor this into something real and controllable, please.
export const MapData = debug({
  name: 'FeaturedCityMapData',
  logger: {},
  isDebug: true,
  init() {
    Debugger.use('content:init').log('04 FeaturecCity MapData.js');
    if (!$('.map').length || enterprise.currentView === PAGEFLOW.RESERVATION_FLOW) {
      return false;
    } else {
      // this code has lived in this file for a while...
      this.logger.log('external script fired');
      const locationsDataStr = $('.map').attr('data-locations');
      let map;
      const locations = locationsDataStr ? JSON.parse(locationsDataStr) : [];
      const zoomValue = parseFloat(enterprise.locationDetail.zoom);
      const latt = enterprise.locationDetail.lat;
      const longg = enterprise.locationDetail['long'];

      function initialize() {
        Debugger.use('FeaturedCityMapData').log('initialize() map fired');
        $(".start-res-btn").click(function (e) {
          e.preventDefault();
          $('html, body').animate({
            scrollTop: $(".booking-widget").offset().top
          }, 1000);
        });

        const myLatlng = new google.maps.LatLng(latt, longg);
        const infoWindow = new google.maps.InfoWindow();

        const mapOptions = {
          zoom: zoomValue,
          center: myLatlng,
          scrollwheel: false,
          mapTypeControl: false,
          animation: google.maps.Animation.DROP,
          zoomControlOptions: {
            position: google.maps.ControlPosition.TOP_RIGHT
          },
          streetViewControl: false
        };
        let buildMap = function (opts) {
          map = new google.maps.Map(document.getElementById('map-canvas'), opts);
          if (locations.length) {
            for (let i = 0; i < locations.length; i++) {
              const location = locations[i];
              const latLong = new google.maps.LatLng(location.latitude, location.longitude);
              const icon = ['data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxNi4wLjQsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+DQo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4Ig0KCSB3aWR0aD0iMzVweCIgaGVpZ2h0PSI0MC42OHB4IiB2aWV3Qm94PSIyODguNSAzNzUgMzUgNDAuNjgiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMjg4LjUgMzc1IDM1IDQwLjY4IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxnIGlkPSJMYXllcl8xXzFfIiBkaXNwbGF5PSJub25lIj4NCgk8cG9seWdvbiBkaXNwbGF5PSJpbmxpbmUiIGZpbGw9IiMxMDYwQUMiIHBvaW50cz0iNjEyLDQ4LjI3MiAzLjQ3Nyw0OC4yNzIgMy40NzcsNjU2Ljc5NiAyMjYuMDIzLDY1Ni43OTYgMzExLjIxNiw3NDEuOTg4IA0KCQkzOTYuNDA5LDY1Ni43OTYgNjEyLDY1Ni43OTYgCSIvPg0KCTxnIGRpc3BsYXk9ImlubGluZSI+DQoJCTxwYXRoIGZpbGw9IiNGQ0VFMjEiIGQ9Ik0zNDQuMjUsMjIyLjEzNmgtOTAuNDA5TDEyNi45Miw0ODEuMTkzaDczLjAyM2wyNC4zNDEtNTIuMTU5aDE0Mi41NjhsMjYuMDc5LDUyLjE1OWg4My40NTUNCgkJCUwzNDQuMjUsMjIyLjEzNnogTTI1MC4zNjQsMzc4LjYxNGw0NS4yMDUtMTAwLjg0MWw0OC42ODIsMTAwLjg0MUgyNTAuMzY0eiIvPg0KCTwvZz4NCjwvZz4NCjxnIGlkPSJMYXllcl8yIiBkaXNwbGF5PSJub25lIj4NCgk8cG9seWdvbiBkaXNwbGF5PSJpbmxpbmUiIGZpbGw9IiMwMDcyM0MiIHBvaW50cz0iNjEyLDQ4LjI3MiAzLjQ3Nyw0OC4yNzIgMy40NzcsNjU2Ljc5NiAyMjYuMDIzLDY1Ni43OTYgMzExLjIxNiw3NDEuOTg4IA0KCQkzOTYuNDA5LDY1Ni43OTYgNjEyLDY1Ni43OTYgCSIvPg0KCTxnIGRpc3BsYXk9ImlubGluZSI+DQoJCTxwYXRoIGZpbGw9IiNCM0Q3OEEiIGQ9Ik0xNzUuNjAyLDUwOS4wMTJjLTEyLjE3LTEuNzM5LTQxLjcyNyw4LjY5Mi01My44OTctMTcuMzg3Yy02Ljk1NS0xNy4zODctMS43MzktMzEuMjk2LDguNjkzLTM5Ljk4OA0KCQkJYzEyLjE3LTEwLjQzMywzMS4yOTUtMTUuNjQ4LDU3LjM3NS01LjIxNkwxNzUuNjAyLDUwOS4wMTJMMTc1LjYwMiw1MDkuMDEyeiIvPg0KCQk8cGF0aCBmaWxsPSIjQjNENzhBIiBkPSJNMTQyLjU2OCwzNjIuOTY2YzAsMCw0My40NjYtNDUuMjA1LDE2Ni45MDksNTIuMTU5Yzk1LjYyNSw3Ni41LDE1MS4yNjEsMzYuNTEyLDE1MS4yNjEsMzYuNTEyDQoJCQlsLTguNjkyLDQ2Ljk0MmMwLDAtNDUuMjA1LDQ2Ljk0My0xNjYuOTEtNDguNjgyYy0xMDAuODQxLTc4LjIzOC0xNTEuMjYxLTQzLjQ2Ni0xNTEuMjYxLTQzLjQ2NlMxNDIuNTY4LDM2MS4yMjcsMTQyLjU2OCwzNjIuOTY2DQoJCQlMMTQyLjU2OCwzNjIuOTY2eiIvPg0KCQk8cGF0aCBmaWxsPSIjQjNENzhBIiBkPSJNMTYxLjY5MywyNzQuMjk1YzAsMCw0My40NjYtNDUuMjA1LDE2Ni45MDksNTIuMTU5Yzk1LjYyNSw3Ni41LDE1MS4yNjEsMzYuNTExLDE1MS4yNjEsMzYuNTExDQoJCQlsLTEwLjQzMiw0Ni45NDNjMCwwLTQ1LjIwNCw0Ni45NDMtMTY2LjkwOS00OC42ODJjLTEwMC44NDEtNzkuOTc3LTE1MS4yNjItNDMuNDY2LTE1MS4yNjItNDMuNDY2DQoJCQlTMTYxLjY5MywyNzAuODE4LDE2MS42OTMsMjc0LjI5NUwxNjEuNjkzLDI3NC4yOTV6Ii8+DQoJCTxwYXRoIGZpbGw9IiNCM0Q3OEEiIGQ9Ik0xNzkuMDgsMTg1LjYyNWMwLDAsNDMuNDY2LTQ1LjIwNSwxNjYuOTA5LDUyLjE1OWM5NS42MjUsNzYuNSwxNTEuMjYyLDM2LjUxMSwxNTEuMjYyLDM2LjUxMQ0KCQkJbC0xMC40MzIsNDYuOTQzYzAsMC00My40NjYsNDYuOTQzLTE2Ni45MDktNDguNjgyYy0xMDAuODQxLTc4LjIzOC0xNTEuMjYxLTQzLjQ2Ni0xNTEuMjYxLTQzLjQ2NlMxNzkuMDgsMTgyLjE0NywxNzkuMDgsMTg1LjYyNQ0KCQkJTDE3OS4wOCwxODUuNjI1eiIvPg0KCQk8cGF0aCBmaWxsPSIjQjNENzhBIiBkPSJNNDU1LjUyMiwxODIuMTQ3YzEyLjE3MSwxLjczOSw0MS43MjgtOC42OTMsNTMuODk4LDE3LjM4N2M1LjIxNiwxNy4zODYsMS43MzgsMzEuMjk1LTguNjkzLDM5Ljk4OA0KCQkJYy0xMi4xNzEsMTAuNDMyLTMxLjI5NiwxNS42NDgtNTcuMzc1LDUuMjE2TDQ1NS41MjIsMTgyLjE0N0w0NTUuNTIyLDE4Mi4xNDd6Ii8+DQoJPC9nPg0KPC9nPg0KPGcgaWQ9IkxheWVyXzMiPg0KCTxwb2x5Z29uIGZpbGw9IiMwMTk3NUEiIHBvaW50cz0iMzIzLjY2LDM3NSAyODguNSwzNzUgMjg4LjUsNDEwLjY4NCAzMDEuMzU5LDQxMC42ODQgMzA2LjI4MSw0MTUuNjggMzExLjIwMyw0MTAuNjg0IA0KCQkzMjMuNjYsNDEwLjY4NCAJIi8+DQoJPGc+DQoJCTxwYXRoIGZpbGw9Im5vbmUiIGQ9Ik0zMTMuNTE0LDM5My4zNTFjMi40MTIsMCwyLjcxNS0wLjIwNCwyLjkxNC0wLjMwNmMwLjMwMy0wLjIwMiwwLjMwMy0wLjQwOCwwLjMwMy0wLjkxNw0KCQkJYy0wLjEwNC0xLjkzNy0zLjMxNi03LjAzNS0xMC43NTEtNy4wMzVjLTcuMTMyLDAtOS43NDQsNC4xNzktMTAuNDQ3LDUuNTA1YzAsMC0wLjIwMiwwLjMwNS0wLjMwMiwwLjYxMWwwLDBoMC44MDUNCgkJCWMwLjA5OS0wLjIwNCwwLjA5OS0wLjMwNiwwLjItMC41MDhjMC42MDMtMS4xMjMsMy4yMTUtNC44OTUsOS42NDQtNC44OTVjNy40MzMsMCwxMC4wNDYsNS4wOTcsMTAuMDQ2LDYuMjE5DQoJCQljMCwwLjIwNCwwLDAuMzA2LTAuMTAyLDAuMzA2Yy0wLjE5OSwwLjIwNC0wLjUwMiwwLjMwNS0yLjMxMSwwLjMwNWgtMjQuOTEzdjAuNzE0SDMxMy41MTR6Ii8+DQoJCTxwYXRoIGZpbGw9Im5vbmUiIGQ9Ik0zMTMuMzEyLDM5MC45MDRjMC4xMDItMC4xMDIsMC0wLjMwNS0wLjEtMC42MTFjLTEuMzA3LTEuOTM2LTMuOTE4LTIuOTU2LTcuMTMyLTIuOTU2DQoJCQljLTMuMDE0LDAtNi4xMjgsMS40MjctNy40MzUsMy4zNjVjMCwwLTAuMiwwLjMwNS0wLjMwMSwwLjUwOGgxNC4wNjVDMzEyLjgxMiwzOTEuMjEsMzEzLjIxMywzOTEuMjEsMzEzLjMxMiwzOTAuOTA0eiIvPg0KCQk8cGF0aCBmaWxsPSIjRkZGRkZGIiBkPSJNMzE4LjczNiwzOTEuNzJjLTAuODAxLTMuOTc1LTUuNDI0LTguMDUzLTEyLjc1Ny04LjA1M2wwLDBjLTYuNDI5LDAtMTEuMTUxLDMuNDY2LTEyLjY1Nyw3LjAzNQ0KCQkJYzAsMC4xMDEtMC4yMDEsMC40MDYtMC4yMDEsMC41MDhsMCwwaC00LjUyMXYxLjQyN2gyNS4wMTVjMS44MDksMCwyLjEwOS0wLjEwMSwyLjMxMS0wLjMwNWMwLjEtMC4xMDIsMC4xLTAuMTAyLDAuMS0wLjMwNg0KCQkJYzAtMS4xMjMtMi42MTEtNi4yMTktMTAuMDQ2LTYuMjE5Yy02LjMyNywwLTkuMDQxLDMuNzcyLTkuNjQzLDQuODk1Yy0wLjEwMSwwLjIwMi0wLjIwMiwwLjMwNS0wLjIwMiwwLjUwOGgtMC44MDRsMCwwDQoJCQljMC4xMDEtMC4zMDYsMC4zMDEtMC42MTEsMC4zMDEtMC42MTFjMC42MDQtMS4zMjYsMy4yMTUtNS41MDUsMTAuNDQ4LTUuNTA1YzcuNDMzLDAsMTAuNzUsNS4wOTgsMTAuNzUsNy4wMzUNCgkJCWMwLDAuNDA4LTAuMSwwLjcxNC0wLjMwMywwLjkxN2MtMC4yMDEsMC4yMDUtMC41MDIsMC4zMDYtMi45MTIsMC4zMDZoLTI1LjAxNXYxLjUyOWMxMC4yNDcsMCwyNi4wMTksMCwyNi42MjIsMA0KCQkJYzEuNzA3LDAsMi45MTQsMCwzLjQxNC0wLjcxNEMzMTguOTM5LDM5My43NTksMzE5LjA0MSwzOTIuOTQzLDMxOC43MzYsMzkxLjcyeiBNMjk4LjY0NiwzOTAuNzAyDQoJCQljMS4zMDYtMS45MzgsNC40MjEtMy4zNjUsNy40MzUtMy4zNjVjMy4yMTQsMCw1LjcyNiwxLjEyMSw3LjEzMiwyLjk1NmMwLjIwMSwwLjMwNiwwLjIwMSwwLjUxLDAuMSwwLjYxMQ0KCQkJYy0wLjEsMC4yMDQtMC41LDAuMzA2LTAuOTAyLDAuMzA2aC0xNC4wNjVDMjk4LjQ0NiwzOTAuOTA0LDI5OC42NDYsMzkwLjcwMiwyOTguNjQ2LDM5MC43MDJ6Ii8+DQoJCTxwYXRoIGZpbGw9IiNGRkZGRkYiIGQ9Ik0zMTUuMjIzLDM5Ny40M2MtMi45MTIsMy42NjgtOC45NDEsMy41Ny05LjU0NSwzLjU3Yy01LjEyMy0wLjEwNC04LjYzOS0yLjc1NS05Ljk0NC01LjMwNWgtMi4zMTENCgkJCWMxLjQwNiwzLjQ2OCw1LjcyOCw2LjkzMywxMi41NTcsNi45MzNjNi45MzMsMCw5Ljc0NS0yLjQ0NSwxMS41NTQtNC44OTNjMC4xLTAuMTAzLDAuMS0wLjIwNCwwLjIwMS0wLjMwNkgzMTUuMjIzTDMxNS4yMjMsMzk3LjQzDQoJCQl6Ii8+DQoJCTxwYXRoIGZpbGw9IiNGRkZGRkYiIGQ9Ik0zMDUuNjc4LDQwMC4yODRjMC4yMDIsMCw1LjYyNywwLjIwNCw4LjY0MS0yLjg1NGgtMi45MTJjLTEuMzA3LDEuMDItMy43MTksMS4zMjUtNS40MjcsMS4zMjUNCgkJCWMtMi44MTIsMC01LjYyNS0xLjIyNC03LjAzMi0yLjk1NmgtMi4zMTFDMjk3Ljc0MywzOTcuODM3LDMwMC43NTYsNDAwLjA4LDMwNS42NzgsNDAwLjI4NHoiLz4NCgk8L2c+DQo8L2c+DQo8L3N2Zz4NCg==',
                'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxNi4wLjQsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+DQo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4Ig0KCSB3aWR0aD0iMzQuNzI1cHgiIGhlaWdodD0iNDAuNzNweCIgdmlld0JveD0iMCAwIDM0LjcyNSA0MC43MyIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgMzQuNzI1IDQwLjczIiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxnIGlkPSJMYXllcl8xXzFfIiBkaXNwbGF5PSJub25lIj4NCgk8cG9seWdvbiBkaXNwbGF5PSJpbmxpbmUiIGZpbGw9IiMxMDYwQUMiIHBvaW50cz0iNTc1LjI4OCw5MCAzOS45MzksOTAgMzkuOTM5LDYyNS4zNDggMjM1LjYwMiw2MjUuMzQ4IDMxMC42MzksNzAwLjM4NiANCgkJMzg1LjY3Nyw2MjUuMzQ4IDU3NS4yODgsNjI1LjM0OCAJIi8+DQoJPGcgZGlzcGxheT0iaW5saW5lIj4NCgkJPHBhdGggZmlsbD0iI0ZDRUUyMSIgZD0iTTMzOS42ODcsMjQyLjg5OWgtNzkuNDc3bC0xMTEuNzUsMjI3LjkzN2g2NC4xNDZsMjEuMzgyLTQ1Ljk5MWgxMjUuNDY2bDIyLjk5NSw0NS45OTFoNzMuNDI0DQoJCQlMMzM5LjY4NywyNDIuODk5eiBNMjU2Ljk4MywzODAuNDY4bDM5LjkzOC04OC43NTRsNDIuNzY0LDg4Ljc1NEgyNTYuOTgzeiIvPg0KCTwvZz4NCjwvZz4NCjxnIGlkPSJMYXllcl8yIiBkaXNwbGF5PSJub25lIj4NCgk8cG9seWdvbiBkaXNwbGF5PSJpbmxpbmUiIGZpbGw9IiMwMDcyM0MiIHBvaW50cz0iNTc1LjI4OCw5MCAzOS45MzksOTAgMzkuOTM5LDYyNS4zNDggMjM1LjYwMiw2MjUuMzQ4IDMxMC42MzksNzAwLjM4NiANCgkJMzg1LjY3Nyw2MjUuMzQ4IDU3NS4yODgsNjI1LjM0OCAJIi8+DQoJPGcgZGlzcGxheT0iaW5saW5lIj4NCgkJPHBhdGggZmlsbD0iI0IzRDc4QSIgZD0iTTE5MS4yMjUsNDk1LjQ0NWMtMTAuODk0LTEuNjE0LTM2LjcxMiw3LjY2NS00Ny42MDQtMTUuMzMxYy02LjA1MS0xNS4zMy0xLjYxMy0yNy40MzQsNy42NjYtMzUuMDk5DQoJCQljMTAuODk0LTkuMjc4LDI3LjQzNC0xMy43MTcsNTAuNDI4LTQuNDM4TDE5MS4yMjUsNDk1LjQ0NUwxOTEuMjI1LDQ5NS40NDV6Ii8+DQoJCTxwYXRoIGZpbGw9IiNCM0Q3OEEiIGQ9Ik0xNjIuMTc4LDM2Ni43NTFjMCwwLDM4LjMyNC0zOS45MzgsMTQ2Ljg0Nyw0NS45OTFjODQuMzE1LDY3LjM3MiwxMzMuMTMyLDMyLjI3MywxMzMuMTMyLDMyLjI3Mw0KCQkJbC03LjY2NSw0MS4xNDljMCwwLTM5LjkzOSw0MS4xNDktMTQ2Ljg0OC00Mi43NjRjLTg4Ljc1NC02OC45ODYtMTMzLjEzMS0zOC4zMjUtMTMzLjEzMS0zOC4zMjVTMTYyLjE3OCwzNjUuNTQxLDE2Mi4xNzgsMzY2Ljc1MQ0KCQkJTDE2Mi4xNzgsMzY2Ljc1MXoiLz4NCgkJPHBhdGggZmlsbD0iI0IzRDc4QSIgZD0iTTE3OS4xMjIsMjg4Ljg5YzAsMCwzOC4zMjYtMzkuOTM5LDE0Ni44NDgsNDUuOTljODQuMzE2LDY3LjM3MiwxMzMuMTMxLDMyLjI3NCwxMzMuMTMxLDMyLjI3NA0KCQkJbC05LjI3Nyw0MS4xNDljMCwwLTM5LjkzOSw0MS4xNDktMTQ2Ljg0OC00Mi43NjRjLTg4Ljc1NC03MC4xOTYtMTMzLjEzMi0zOC4zMjYtMTMzLjEzMi0zOC4zMjZTMTc5LjEyMiwyODUuNjYzLDE3OS4xMjIsMjg4Ljg5DQoJCQlMMTc5LjEyMiwyODguODl6Ii8+DQoJCTxwYXRoIGZpbGw9IiNCM0Q3OEEiIGQ9Ik0xOTQuNDUyLDIxMS4wMjhjMCwwLDM4LjMyNi0zOS45MzksMTQ2Ljg0OCw0NS45OTFjODQuMzE2LDY3LjM3MywxMzMuMTMyLDMyLjI3MywxMzMuMTMyLDMyLjI3Mw0KCQkJbC05LjI3OCw0MS4xNDljMCwwLTM4LjMyNiw0MS4xNDktMTQ2Ljg0OS00Mi43NjNjLTg4Ljc1NC02OC45ODYtMTMzLjEzMS0zOC4zMjYtMTMzLjEzMS0zOC4zMjZTMTk0LjQ1MiwyMDcuODAxLDE5NC40NTIsMjExLjAyOA0KCQkJTDE5NC40NTIsMjExLjAyOHoiLz4NCgkJPHBhdGggZmlsbD0iI0IzRDc4QSIgZD0iTTQzNy4zMTUsMjA3LjgwMWMxMC44OTMsMS42MTQsMzYuNzEyLTcuNjY1LDQ3LjYwNCwxNS4zMzFjNC40MzgsMTUuMzMsMS42MTQsMjcuNDM0LTcuNjY1LDM1LjA5OQ0KCQkJYy0xMC44OTMsOS4yNzgtMjcuNDMzLDEzLjcxNi01MC40MjksNC40MzhMNDM3LjMxNSwyMDcuODAxTDQzNy4zMTUsMjA3LjgwMXoiLz4NCgk8L2c+DQo8L2c+DQo8Zz4NCgk8cG9seWdvbiBmaWxsPSIjMDE5NzVBIiBwb2ludHM9IjM0LjcyMywwIDAsMCAwLDM1LjcyMyAxMi42OTIsMzUuNzIzIDE3LjU1OCw0MC43MyAyMi40MjUsMzUuNzIzIDM0LjcyMywzNS43MjMgCSIvPg0KCTxnPg0KCQk8cGF0aCBmaWxsPSIjRkZGRkZGIiBkPSJNMjkuNDg5LDUuMjVsLTEuNDY1LDAuMDgxYy0xLjMzNiwwLjA4MS0yLjU5LDAuNjczLTMuNTU5LDEuNjdsLTIuNzQ2LDIuOTA3SDkuNzYNCgkJCWMtMS4xNTEsMC0yLjA2NiwwLjk0MS0yLjA2NiwyLjFjMCwxLjMxOSwwLjczMiwxLjg1NywxLjg4NCwyLjMxNWwyLjY2OSwwLjcyNmwwLjk0MS0xLjEwM2wtMy4yNDMtMC43ODENCgkJCWMtMC43Ni0wLjI5Ni0xLjA3My0wLjQ4My0xLjA3My0xLjE1N2MwLTAuNTEyLDAuNDE4LTAuOTE2LDAuOTE2LTAuOTE2SDIwLjYyTDExLjIsMjAuOTdjLTAuNDcyLDAuNDg0LTEuMSwwLjc4LTEuNzgsMC43OEg0LjYwNg0KCQkJYy0wLjk0MSwwLTIuMTcyLDAuODA5LTIuMzAyLDEuNzc3Yy0wLjA1MywwLjMyMi0wLjAyNSwxLjE1NywxLjIyOSwxLjUzNWw2LjkwNywyLjQ3NmMxLjI4MiwwLjQ1OCwyLjgwMSwwLjA4MiwzLjc0MS0wLjkxNQ0KCQkJbDYuNzI1LTcuMDI1bDEuMTI1LDUuNzA2YzAuMzQsMS45MTIsMS40MTQsMi41ODUsMi4zMDMsMi41ODVjMS4xMjcsMCwyLjAxNi0xLjA1LDIuMDQzLTIuMzY3di0xMS4yDQoJCQljMC0wLjMyMywwLjEyOS0wLjY0NiwwLjM0LTAuODYxbDQuMjkxLTQuNDQxYzAuNjAyLTAuNjQ2LDAuNzgzLTEuNTg4LDAuNDQzLTIuNDIyQzMxLjExMiw1Ljc4OCwzMC4zNTIsNS4yNSwyOS40ODksNS4yNXoNCgkJCSBNMzAuMTY5LDguMjExbC00LjI2Niw0LjQ0MWMtMC40NDMsMC40NTgtMC42OCwxLjA1LTAuNjgsMS43MjN2MTEuMTk5YzAsMC43ODEtMC40NDUsMS4xODUtMC44NjEsMS4xODUNCgkJCWMtMC40MjIsMC0wLjk0MS0wLjQzMS0xLjE1Mi0xLjYxNWwtMS4xNzgtNi4wNTdjLTAuMTA1LTAuNDU4LTAuNTIzLTAuNzgtMC45OTYtMC43OGMtMC4yODUsMC0wLjUyMSwwLjEwNi0wLjczLDAuMzIybC02Ljk2LDcuMTYNCgkJCWMtMC42NTMsMC42NzMtMS42NzMsMC45MTYtMi41MzgsMC42MmwtNy44NS0yLjhsMS4xNzctMC41MzljMC4xNTctMC4wOCwwLjMxNC0wLjEwNiwwLjQ0NS0wLjEwNmg0Ljc4OQ0KCQkJYzAuOTk0LDAsMS45MS0wLjQwNCwyLjU5LTEuMTMxbDEzLjMyLTEzLjk5OWMwLjczLTAuNzgsMS43MjktMS4yMzcsMi43NzMtMS4yOTJsMS4zNTktMC4wODFjMC40MiwwLDAuNzU4LDAuMjQyLDAuOTE2LDAuNjE5DQoJCQlDMzAuNTEsNy4zNzYsMzAuNTEsNy44MzQsMzAuMTY5LDguMjExeiIvPg0KCTwvZz4NCjwvZz4NCjwvc3ZnPg0K'];
              const marker = new google.maps.Marker({
                position: latLong,
                map: map,
                title: location.locationNameTranslation,
                locationObj: {
                  details: {
                    country_code: location.countryCode
                  },
                  lat: location.latitude,
                  locationId: location.groupBranchNumber,
                  locationName: location.locationNameTranslation || location.addressLines[0],
                  locationType: "BRANCH",
                  'long': location.longitude,
                  results: {
                    airports: [],
                    branches: [],
                    cities: [],
                    ports: [],
                    trains: []
                  }
                },
                icon: location.locationType === LOCATION_SEARCH.STRICT_TYPE.AIRPORT ? icon[1] : icon[0]
              });
              
              google.maps.event.addListener(marker, 'click', (function (marker, infoWindow, content) {
                return function () {
                  infoWindow.setContent(content);
                  infoWindow.open(map, marker);

                  $(".if-start-res-btn").click(function (e) {

                    e.preventDefault();
                    LocationActions.setLocationObject('pickup', marker.locationObj);
                    LocationActions.setLocationObject('dropoff', marker.locationObj);
                    
                    $('html, body').animate({
                      scrollTop: $(".booking-widget").offset().top
                    }, {
                      duration: 1000,
                      complete: function () {
                        $('#reservationWidget > .inner-container').addClass('expanded');
                      }
                    });
                  });
                };
              })(marker, infoWindow, buildInfoWindow(location)));
            }
          }
        };

        let buildInfoWindow = function (location) {

          const ifHeader = `<div style='min-width: 220px; padding: 15px;'>
            <div style='border-bottom: 2px solid black; height: 26px; margin-bottom: 10px; color: black; font-size: 14px; line-height: 14px; font-weight: bold'>${enterprise.i18nReservation.locations_0046}</div>`;
          let addressLine = location.addressLines[0];

          if (location.addressLines[1]) {
            addressLine += ', ' + location.addressLines[1];
          }

          const ifBody = `<div>
            <ul style='list-style: none; margin: 0;'>
            <li style='margin: 5px 0; font-size: 17px; color: #656565'>${addressLine}</li>
            <li style='margin: 5px 0; font-size: 17px; color: #656565'>${location.city},  ${location.state} ${location.postalCode} </li>
            <li style='margin: 5px 0; font-size: 17px; color: #656565'>${location.formattedPhone}</li>
            </ul>
            </div>`;

          const ifFooter = `<div>
            <div style='padding-top: 20px; text-align: right; '>
            <a class='btn if-start-res-btn' href='#book' data-state='${location.state}' data-city='${location.city}' data-country='${location.countryCode}'>${enterprise.i18nReservation.resflowreview_0001}</a>
            <a class='btn link-style-btn' href='${location.url}' style='margin-bottom: 10px;'>${enterprise.i18nReservation.resflowlocations_0022}</a><br/>
            </div>
            </div>
            </div>`;

          return ifHeader + ifBody + ifFooter;
        };

        buildMap(mapOptions);
      }
      document.addEventListener('readystatechange', function () {
        if (this.readyState === 'interactive') {
          $(".loading").addClass('active');
        } else if (this.readyState === 'complete') {
          $(".loading").removeClass('active');
          $("#map-canvas").addClass('active');
          google.maps.event.addDomListener(window, 'load', initialize);
        }
      });
    }
  }
});

