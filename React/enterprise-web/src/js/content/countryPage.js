import { Debugger } from '../utilities/util-debug';
import { Enterprise } from '../modules/Enterprise';

export const countryPageHandle = {
  init() {
    Debugger.use('content:init').log('33 Country Page');
    let CountryPage = require('./CountryPage/index');
    let countryData = Enterprise.global.pages.countryPage;
    $(function(){
      if (countryData) {
        CountryPage.init(countryData);
      }
    })
  }
};
