import { debug } from '../../utilities/util-debug';
import { Enterprise } from '../../modules/Enterprise';
const debugScripts = Enterprise.global.settings.debugContentScripts;
import ResponsiveUtil from '../../utilities/util-responsive';

let container;

const InterrupterObject = debug({
  name: 'InterrupterTileBand',
  isDebug: debugScripts,
  logger: {},
  init ($contentContainer) {
    this.logger.log('init() fired');
    container = $contentContainer;
    const windowWidth = ResponsiveUtil.isMobile('(min-width: 636px)') && ResponsiveUtil.isMobile('(max-width: 800px)');
    if (windowWidth) {
      InterrupterObject.res(true);
      $(window).on('resize', InterrupterObject.res);
    }
  },
  res (tf = false) {
    InterrupterObject.logger.log('res()', tf, container);
    for(var i = 0; i < container.length; i++) {
      let contentHeight = container[i].clientHeight;
      let imageHeight= container.prev('.content-bg-img')[i];
      imageHeight.style.height = contentHeight+'px';
      if (ResponsiveUtil.isMobile('(min-width: 800px)')) {
        imageHeight.style.height = 0+'px';
      }
    }
  }
});

module.exports = InterrupterObject;
