import { LazyLoadUpdate } from '../../modules/outerDomManager';
import { debug } from '../../utilities/util-debug';
import { Enterprise } from '../../modules/Enterprise';
const debugScripts = Enterprise.global.settings.debugContentScripts;

const SlideshowObject = debug({
  name: 'SlideshowObject',
  isDebug: true,
  logger: {},
  init: function () {
    this.logger.log('external init() ran');
    var leftArrow = '<div class="slick-prev icon icon-arrow-left"></div>';
    var rightArrow = '<div class="slick-next icon icon-arrow-right"></div>';
    var carouselNav = $('.carousel-nav');
    var carouselContent = $('.carousel-content');

    carouselContent.slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: false,
      asNavFor: '.carousel-nav',
      prevArrow: leftArrow,
      nextArrow: rightArrow

    });
    carouselNav.slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      asNavFor: '.carousel-content',
      dots: false,
      infinite: false,
      centerMode: false,
      focusOnSelect: true,
      variableWidth: true,
      prevArrow: leftArrow,
      nextArrow: rightArrow
    });
    carouselContent.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
      LazyLoadUpdate();
      $('.start-count').html(nextSlide + 1);
    });
    $('.end-count').html(carouselNav.slick('getSlick').slideCount);
  }
});

module.exports = SlideshowObject;
