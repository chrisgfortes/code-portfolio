/* global videoPlayerArticle, playerDataArticle, pitStops */
import PursuitsTools from './PursuitsTools';

const PursuitsArticle = {
  addPlayer: function () {
    $('.loading').addClass('inactive');
    const playerDataArticle = window.playerDataArticle;
    // dynamically build & inject the player into DOM
    const playerHTMLArticle = '<video id=\"overlayPlayer\" class=\"video-js\" data-video-id=\"' + playerDataArticle.videoId + '\" data-account=\"' + playerDataArticle.accountId + '\" data-player=\"' + playerDataArticle.playerId + '\" data-embed=\"default\" aria-describedby=\"transcriptContentAria-' + playerDataArticle.playerId + '\" preload=\"auto\" controls loop style=\"width: 100%; height: 100%; position: absolute; top: 0px; bottom: 0px; right: 0px; left: 0px;\"></video>';
    const tabletPlayerHTMLArticle = '<video muted playsinline id=\"overlayPlayer\" class=\"video-js\" data-video-id=\"' + playerDataArticle.videoId + '\" data-account=\"' + playerDataArticle.accountId + '\" data-player=\"' + playerDataArticle.playerId + '\" data-embed=\"default\" aria-describedby=\"transcriptContentAria-' + playerDataArticle.playerId + '\" preload=\"auto\" controls loop style=\"width: 100%; height: 100%; position: absolute; top: 0px; bottom: 0px; right: 0px; left: 0px;\"></video>';

    if (enterprise.environment.browser.deviceClass === 'tablet') {
      document.getElementById('dynamicalPlayerDiv').innerHTML = tabletPlayerHTMLArticle;
    } else {
      document.getElementById('dynamicalPlayerDiv').innerHTML = playerHTMLArticle;
    }

    // instantiate player
    // Use the bc() method to initialize the Brightcove Player. The method can take either the player ID or the player element itself as an argument.
    window.bc(document.getElementById('overlayPlayer'));

    // Use the videojs() method to create a player instance from the Brightcove Player, identified by its ID.
    window.videoPlayerArticle = window.videojs('#overlayPlayer');
    window.videoPlayerArticle.ready(function () {
      // console.debug('videojs ready');
      // console.debug('deviceDetection:', enterprise.environment.browser.rawname);
      // Play/Pause video when starts
      this.play();

      const deviceDetection = enterprise.environment.browser;
      PursuitsTools.selectRendition(deviceDetection, window.videoPlayerArticle);

      // Fired when the player has initial duration and dimension information
      this.on('loadedmetadata', function () {
        // console.debug('videojs loadedmetadata');
        PursuitsArticle.setupPitStops(this.duration());
      });

      PursuitsArticle.videoPlugin();
    });
  },
  init: function () {
    // Check pitStop image src if it is empty
    // -------------------------------------------------------------
    $('.positionWrapper').each(function () {
      if (!$(this).children('.modalMedia').length) {
        $(this).parents('.pitStopModal').addClass('emptyImg');
      }
    });

    // pitStopButton samePage/newTab
    // -------------------------------------------------------------
    $('.modalContent a').each(function () {
      if ($(this).hasClass('newTab')) {
        $(this).attr('target', '_blank');
      }
    });

    // Fired when users click on the video and video-control
    // -------------------------------------------------------------
    $(document).on('click touchstart', '#overlayPlayer_Flash_api, #overlayPlayer_html5_api, .vjs-play-control', function () {
      PursuitsArticle.addRemoveUserPausedClass();
    });

    // Calls the checkIfVideoInView() function when the onscroll event is fired.
    // -------------------------------------------------------------
    window.onscroll = function () {
      if (window.innerWidth >= 1024) {
        const video = $('#overlayPlayer_Flash_api, #overlayPlayer_html5_api');
        if (!video.hasClass('userPaused') && enterprise.environment.browser.deviceClass === 'computer' && enterprise.environment.browser.rawname !== 'msie') {
          PursuitsArticle.checkIfVideoInView();
        }
      }
    };
  },
  covertMStoSS: function (ms, length, inPercent) {
    // Function to convert users input in AEM
    const clear = ms.split(':');
    const clearInSeconds = (+clear[0]) * 60 + (+clear[1]);
    const clearInSecondsInPercent = Math.round( clearInSeconds / length * 100 );

    if (inPercent) {
      return clearInSecondsInPercent;
    } else {
      return clearInSeconds;
    }
  },
  setupPitStops: function (lengthOfVideo) {
    // (global - pitStops) Create pitStopScrubber & pitStopButton based on pitStops array.
    pitStops.forEach(function (pitStop) {
      let pitStopLocation = pitStop.pitStopLocation;
      let pitStopLocationConverted = PursuitsArticle.covertMStoSS(pitStopLocation, lengthOfVideo);
      let pitStopLocationConvertedInPercent = PursuitsArticle.covertMStoSS(pitStopLocation, lengthOfVideo, true);
      let pitStopTimeAmount = pitStop.pitStopTimeAmount;
      let pitStopTimeAmountConverted = PursuitsArticle.covertMStoSS(pitStopTimeAmount, lengthOfVideo);
      let pitStopLocationEnd = pitStopLocationConverted + pitStopTimeAmountConverted;

      // Add/Create pitStopScrubber based on ${pitStop} values which take mm:ss in AEM.
      $('.vjs-progress-holder')
        .append('<span class="pitStopScrubber" id="dot-' + pitStop.pitStopId + '" ' +
                'data-position="' + pitStopLocationConverted + '" ' +
                'style="left: ' + pitStopLocationConvertedInPercent + '%"></span>' );

      // Create pitStopButton based on ${pitStop} values
      $('div#overlayPlayer')
        .append('<button class="pitStopButton ' + pitStop.pitStopPosition + '" ' +
                    'data-pitStopModal="pitStopModal-' + pitStop.pitStopId + '" ' +
                    'data-pitStopLocation="' + pitStopLocationConverted + '" ' +
                    'data-pitStopLocationEnd="' + pitStopLocationEnd + '"> ' +
                      '<i class="icon icon-pursuits-icon-add" role="presentation"></i>' +
                      '<span>' + pitStop.pitStopTitle + '</span>' +
                '</button>');
    });

    // After it is built, move pitStop related elements inside the dynamically build Brightcove <video>.
    // Elements have to be on the same level as <video> so it can be viewable when users go to fullscreen,
    // which brwosers push <video> on top of any elements and z-index.
    $('div#overlayPlayer').append('<div id=\"pitStopModalBg\"></div>');
    $('.pitStopModal').appendTo('div#overlayPlayer');
  },
  addRemoveUserPausedClass: function () {
    // Add/Remove class to <video> when pause is triggered by users
    if (videoPlayerArticle.paused()) {
      $('#overlayPlayer_Flash_api, #overlayPlayer_html5_api').addClass('userPaused');
    } else {
      $('#overlayPlayer_Flash_api, #overlayPlayer_html5_api').removeClass('userPaused');
    }
  },
  videoPlugin: function () {
    // VideoJS custom plugin
    window.videojs.plugin('pluginPursuits', function (options) {
      const deviceDetection = enterprise.environment.browser;
      const player = this;

      // console.debug('PursuitsArticle.js pluginPursuits');

      // Player Events: Fired when the current playback position has changed.

      player.on('timeupdate', function () {
        // console.info('currentTime: ' + player.currentTime());

        // Add CSS class on pitStopScrubber within video start & end time.
        $(options.pitStopSelector).each(function () {
          const position = $(this).data(options.pitStopDataAttr);

          if (player.currentTime() >= position) {
            if (!$(this).hasClass(options.activeClassName)) {
              $(this).addClass(options.activeClassName);
            }
          } else if ($(this).hasClass(options.activeClassName)) {
            $(this).removeClass(options.activeClassName);
          }
        });

        // Show/Hide pitStopButton within video start & end time.
        $(options.pitStopBtnSelector).each(function () {
          const start = $(this).data(options.pitStopBtnAttrStart);
          const end = $(this).data(options.pitStopBtnAttrEnd);

          if (player.currentTime() >= start && player.currentTime() <= end) {
            if (!$(this).hasClass(options.activeClassName)) {
              $(this).addClass(options.activeClassName).fadeIn();

              // Focus on newly instantiated pitStopButton unless focus is on progress slider
              if (!$(document.activeElement).hasClass('vjs-slider')) {
                $(this).focus();
              }
            }
          } else if ($(this).hasClass(options.activeClassName)) {
            $(this).removeClass(options.activeClassName).fadeOut();
          }
        });
      });

      // pitStopButton actions
      // -------------------------------------------------------------
      let animationPitStopStart = function (e, id) {
        // Hide all other buttons but keep itself on screen
        $(options.pitStopBtnSelector).hide();
        e.show();

        // Show Bg first
        $('#pitStopModalBg').fadeIn();

        // check id's css class
        const parentsHasNoImage = $('#' + id).hasClass('emptyImg');

        // animation flow
        let animation = function (left, top, callback) {
          // video
          player.pause();
          $('#overlayPlayer_Flash_api, #overlayPlayer_html5_api').addClass('userPaused');

          if (deviceDetection.rawname === 'chrome' || deviceDetection.rawname === 'firefox') {
            e.css({'width': 'auto', 'height': '0'});
            e.animate({
              left: left,
              top: top
            }, 750, function () {
              e.hide();
              $('#' + id).fadeIn('500', callback);
            });
          } else {
            e.hide();
            $('#' + id).fadeIn('250', callback);
          }
        };

        function afterAnimationCallback () {
          // Accessibility: initialize focus inside dialog
          $('#' + id + ' .pitStopModal-close').focus();

          // Accessibility: sets focus trap so it doesn't leave the dialog
          // Based on our own React mixin DialogFocusTrap.js
          let $firstFocusable = $('#' + id + ' .pitStopModal-close');
          let $lastFocusable = $('#' + id + ' a:last');
          if ($firstFocusable.length !== 0) {
            // Content might not have the link call-to-action
            if ($lastFocusable.length === 0) {
              $lastFocusable = $firstFocusable;
            }

            $firstFocusable.on('keydown.focusloop', function () {
              if (event.key === 'Tab' && event.shiftKey) {
                event.preventDefault();
                $lastFocusable.focus();
              }
            });
            $lastFocusable.on('keydown.focusloop', function () {
              if (event.key === 'Tab') {
                event.preventDefault();
                $firstFocusable.focus();
              }
            });
          }
        }

        if (parentsHasNoImage) {
          animation('46.3%', '28.8%', afterAnimationCallback);
        } else {
          animation('57.5%', '28.8%', afterAnimationCallback);
        }
      };

      $(document).on('click', options.pitStopBtnSelector, function () {
        animationPitStopStart($(this), $(this).data('pitstopmodal'));
      });

      let animationPitStopEnd = function () {
        $('.pitStopModal, #pitStopModalBg').fadeOut('500', function () {
          // button moves back to original position
          $(options.pitStopBtnSelector).removeAttr('style');
          // video
          player.play();
          $('#overlayPlayer_Flash_api, #overlayPlayer_html5_api').removeClass('userPaused');
        });
      };

      // videojs-overlay: close button on pitStop overlay
      $('.pitStopModal-close').on('click', function () {
        animationPitStopEnd();
      });
    });

    const options = {
      pitStopSelector: '.pitStopScrubber',
      pitStopDataAttr: 'position',
      pitStopBtnSelector: '.pitStopButton',
      pitStopBtnAttrStart: 'pitstoplocation',
      pitStopBtnAttrEnd: 'pitstoplocationend',
      pitStopModalSelector: '.pitStopModal',
      activeClassName: 'active'
    };

    window.videojs('#overlayPlayer').pluginPursuits(options);
  },
  isScrolledIntoView: function () {
    // Check the position of the player within the viewport
    // get the position of the viewport
    let docViewTop = $(window).scrollTop();
    // let docViewBottom = docViewTop + $(window).height();
    // get the position of the player element
    let elemTop = $('#overlayPlayer_Flash_api, #overlayPlayer_html5_api').offset().top;
    let elemBottom = elemTop + $('#overlayPlayer_Flash_api, #overlayPlayer_html5_api').height();

    // determine how the player element is in the viewport

    // Stop when player is not in the viewport
    return ((elemBottom >= docViewTop));
    // Stop when player is not FULLY in the viewport
    // return ( (elemBottom >= docViewTop) && (elemTop <= docViewBottom) && (elemBottom <= docViewBottom) && (elemTop >= docViewTop) );
  },
  checkIfVideoInView: function () {
    // Start video playback when the player is in the viewport and pauses playback when the player is not fully in the viewport.
    if (PursuitsArticle.isScrolledIntoView()) {
      videoPlayerArticle.play();
    } else {
      videoPlayerArticle.pause();
    }
  }
};

export default PursuitsArticle;
