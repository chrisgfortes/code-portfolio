/* global videoPlayer, videoMuted, videoAutoPlay, console */

const PursuitsTools = {
  // @todo change the code elsewhere in pursuits to use a modified version of this to consolidate
  detection () {
    const deviceDetection = enterprise.environment.browser;

    if (deviceDetection.deviceClass === 'mobile') {
      return 'mobile';
    } else {
      return 'desktop';
    }
  },
  // get data about the player
  getMeta (playerObject) {
    return {
      currentSrc: playerObject.currentSrc(),
      currentType: playerObject.currentType(),
      techName: playerObject.techName_,
      element: playerObject.tech_.el_.toString()
    };
  },
  // just nicely outputting the other
  // why both? the other came first...
  getMetaFormat (player) {
    let data = PursuitsTools.getMeta(player);
    return 'Source Url: ' + data.currentSrc + '\nContent Type: ' + data.currentType + '\nTechnology: ' + data.techName + '\nHTML Element: ' + data.element;
  },
  // downgrade the file for some users
  selectRendition (deviceDetection, playerObject) {
    const debugMode = ((enterprise.prod === 'false') ? true : false) || (location.hash.indexOf('video') > -1);

    let deviceMeta = deviceDetection.rawname.toLowerCase();

    /**
     * Selecting low rendition on video for Playback on IE
     *
     * @event Use the one() method to only once listen for the
     * loadstartevent and define an anonymous event handler function
     */
    if (deviceMeta === 'msie' || deviceMeta === 'ie') {
      // console.debug('loadstart event fired');

      playerObject.one('loadstart', function () {
        if (debugMode) {
          console.debug('Change to lower rendition video for user who is on ' + deviceMeta + ' ' + deviceDetection.version);
          console.debug('Original source info', PursuitsTools.getMetaFormat(playerObject));
        }

        let videoSources = playerObject.mediainfo.sources;
        let sourcesMP4 = videoSources
          .filter(rendition => rendition.container === 'MP4' && rendition.hasOwnProperty('src'))
          .sort(function (a, b) {
            return b.size - a.size;
          });

        let lowestQuality = sourcesMP4[sourcesMP4.length - 1];

        playerObject.src(lowestQuality.src);
        playerObject.play();
        if (debugMode) {
          console.debug('Updated source info', PursuitsTools.getMetaFormat(playerObject));
        }
      });
    } else {
      if (debugMode) {
        console.debug('Running default video rendition for user who is on ' + deviceMeta + ' ' + deviceDetection.version);
        console.debug(PursuitsTools.getMetaFormat(playerObject));
      }
    }
  }
};

export default PursuitsTools;

