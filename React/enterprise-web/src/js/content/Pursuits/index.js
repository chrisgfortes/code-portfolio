import PursuitsIndex from './PursuitsIndex';
import PursuitsArticle from './PursuitsArticle';

const SelectorPursuitsIndex = $('.pursuitsIndexVideo');
const SelectorPursuitsArticleContainer = $('.pursuitsInteractiveHeroVideo');
const SelectorPursuitsArticle = $('.pursuitsInteractiveHeroVideoWrapper');
const MediaQueryValue = '(min-width: 1024px)';

const Pursuits = {
  // this is fired on document ready so our *.addPlayer() methods do not need it
  init: function () {
    Pursuits.mediaListener();
    if (SelectorPursuitsIndex.length) {
      PursuitsIndex.init();
    } else if (SelectorPursuitsArticle.length) {
      PursuitsArticle.init();
    }
  },
  mediaListener: function () {
    const mediaQuery = window.matchMedia(MediaQueryValue);

    if (SelectorPursuitsIndex.length) {
      //addListener so it gets called on resize
      mediaQuery.addListener(Pursuits.toggleVideoPlayer);
      //sets the function to toggle between mobile/desktop depending on the query
      Pursuits.toggleVideoPlayer(mediaQuery);
    } else if (SelectorPursuitsArticle.length) {
      mediaQuery.addListener(Pursuits.toggleVideoPlayerArticle);
      Pursuits.toggleVideoPlayerArticle(mediaQuery);
    }
  },
  toggleVideoPlayer (query) {
    if (query.matches) {
      if (!window.videoPlayer) {
        PursuitsIndex.addPlayer();
      }

      //adds the player if it started with mobile view
      if (SelectorPursuitsIndex.hasClass('inactive')) {
        SelectorPursuitsIndex.removeClass('inactive').show();
      }
      $('.pursuitsIndexVideo .brightcove-container, .pursuitsIndexVideo .pursuits-video-overlay').addClass('active');
      $('.magazine-landing-intro-carousel').hide();
    } else {
      if (window.videoPlayer) {
        window.videoPlayer.pause();
      }

      SelectorPursuitsIndex.addClass('inactive').hide();
      PursuitsIndex.setVideoControlsState('paused');
      $('.magazine-landing-intro-carousel').show();
    }
  },
  toggleVideoPlayerArticle (query) {
    if (query.matches) {
      if (!window.videoPlayerArticle) {
        PursuitsArticle.addPlayer();
      }

      if (SelectorPursuitsArticleContainer.hasClass('inactive')) {
        SelectorPursuitsArticleContainer.removeClass('inactive').show();
      }
      $('.pursuitsArticleNav, .pursuits-transcript-section, .pursuitsInteractiveHeroVideoWrapper .brightcove-container').addClass('active').show();
    } else {
      if (window.videoPlayerArticle) {
        window.videoPlayerArticle.pause();
      }

      SelectorPursuitsArticleContainer.addClass('inactive').hide();
    }
  }
};

module.exports = Pursuits;
