/* global videoPlayer, videoMuted, videoAutoPlay */
import PursuitsTools from './PursuitsTools';

const PursuitsIndex = {
  addPlayer: function () {
    $('.loading').addClass('inactive');
    const playerData = window.playerData;
    // dynamically build & inject the player into DOM
    const playerHTML = '<video id=\"pursuitsIndex\" class=\"video-js\" data-video-id=\"' + playerData.videoId + '\" data-account=\"' + playerData.accountId + '\" data-player=\"' + playerData.playerId + '\" data-embed=\"default\" aria-describedby=\"transcriptContentAria-' + playerData.playerId + '\" preload=\"auto\" controls loop style=\"width: 100%; height: 100%; position: absolute; top: 0px; bottom: 0px; right: 0px; left: 0px;\"></video>';
    document.getElementById('dynamicalPlayerDiv').innerHTML = playerHTML;

    // instantiate player
    // Use the bc() method to initialize the Brightcove Player. The method can take either the player ID or the player element itself as an argument.
    window.bc(document.getElementById('pursuitsIndex'));
    // Use the videojs() method to create a player instance from the Brightcove Player, identified by its ID.
    window.videoPlayer = window.videojs($('#pursuitsIndex video').attr('id'), {'autoPlay': playerData.videoAutoPlay, 'muted': playerData.videoMuted}).ready(function () {
      // console.debug('videojs ready');
      this.on('loadedmetadata', function () {
        // console.debug('loadedmetadata');
        this.muted(playerData.videoMuted);

        if (playerData.videoAutoPlay) {
          this.autoplay(playerData.videoAutoPlay);
          PursuitsIndex.setPlayerState('playing');
        }

        if (playerData.videoMuted) {
          PursuitsIndex.setMuteButtonState('muted');
        } else {
          PursuitsIndex.setMuteButtonState('unmuted');
        }

        this.on('pause', function () {
          PursuitsIndex.setVideoControlsState('paused');
        });

        this.on('play', function () {
          PursuitsIndex.setVideoControlsState('playing');
        });

      });
      PursuitsIndex.setVideoQuality();
    });
  },
  init: function () {
    PursuitsIndex.setupCarousel();
    $('body').on('click', '.mute-button', function () {
      // Toggle muted states
      if (videoPlayer) {
        let wasMuted = $(this).hasClass('muted');
        PursuitsIndex.setMuteButtonState(wasMuted ? 'unmuted' : 'muted');
        videoPlayer.muted(!wasMuted);
      }
    });

    $('body').on('click', '.play-button', function () {
      // Toggle video player states
      if (videoPlayer && videoPlayer.paused()) {
        PursuitsIndex.setPlayerState('playing');
        PursuitsIndex.setVideoControlsState('playing');
      } else {
        PursuitsIndex.setPlayerState('paused');
        PursuitsIndex.setVideoControlsState('paused');
      }
    });
  },
  setPlayButtonState: function (state) {
    switch (state) {
      case 'playing':
        $('.pursuitsIndexVideo .play-button').attr('aria-label', 'Pause');
        $('.pursuitsIndexVideo .icon-pause').show();
        $('.pursuitsIndexVideo .icon-play').hide();
        break;
      case 'paused':
        $('.pursuitsIndexVideo .play-button').attr('aria-label', 'Play');
        $('.pursuitsIndexVideo .icon-pause').hide();
        $('.pursuitsIndexVideo .icon-play').show();
        break;
      default:
        console.error('Invalid state for Play Button.');
        break;
    }
  },
  setMuteButtonState: function (state) {
    switch (state) {
      case 'paused':
        $('.pursuitsIndexVideo .mute-button').addClass('paused');
        break;
      case 'muted':
        $('.pursuitsIndexVideo .mute-button').addClass('muted');
        $('.pursuitsIndexVideo .mute-button').attr('aria-label', 'Unmute');
        break;
      case 'unmuted':
        $('.pursuitsIndexVideo .mute-button').removeClass('muted');
        $('.pursuitsIndexVideo .mute-button').attr('aria-label', 'Mute');
        break;
      case 'playing':
        $('.pursuitsIndexVideo .mute-button').removeClass('paused');
        break;
      default:
        console.error('Invalid state for Mute Button.');
        break;
    }
  },
  setVideoControlsState: function (state) {
    this.setMuteButtonState(state);
    this.setPlayButtonState(state);
  },
  setPlayerState: function (state) {
    switch (state) {
      case 'playing':
        videoPlayer.play();
        break;
      case 'paused':
        videoPlayer.pause();
        break;
      default:
        console.error('Invalid state for Player.');
        break;
    }
    this.setVideoControlsState(state);
  },
  setVideoQuality: function () {
    // console.debug('running setVideoQuality');
    const deviceDetection = enterprise.environment.browser;
    // console.debug('deviceDetection:', enterprise.environment.browser.rawname);
    PursuitsTools.selectRendition(deviceDetection, window.videoPlayer);
  },
  setupCarousel: function () {
    //Configurable:
    const carouselConfig = {
      maxArticlesPerPage: 4,
      startingSlide: 0
    };

    const overlayButtonList = $('.pursuits-video-overlay-cta-list');
    const articleButtons = $('.pursuits-video-overlay-button');
    if (articleButtons.length > carouselConfig.maxArticlesPerPage) {
      this.initCarousel(carouselConfig, overlayButtonList);
    }
  },
  initCarousel: function (carouselConfig, overlayButtonList) {
    const carousel = overlayButtonList;
    carousel.addClass('carousel-active');
    carousel.data('current-slide', -1); //-1 so we are able to toggleslide to 0
    this.addCarouselIndex(carousel, carouselConfig);
    this.addControlButtons(carousel);
    this.toggleSlide(overlayButtonList, carouselConfig.startingSlide, carouselConfig.maxArticlesPerPage);
  },
  toggleSlide: function (overlayButtonList, currentSlide, maxArticlesPerPage = 4) {
    const previousSlide = overlayButtonList.data('current-slide');
    // since the first slide is 0, max slide is decreased by 1
    const maxSlide = Math.ceil(overlayButtonList.find('a').length / maxArticlesPerPage) - 1;
    if (previousSlide !== currentSlide && currentSlide >= 0 && currentSlide <= maxSlide) {
      //this iterates over the list of articleButtons and toggles active the ones that are in the slide to be shown
      overlayButtonList.find('.slide-active').removeClass('slide-active');
      overlayButtonList.find('a').map(
        (index, articleButton) => {
          if (index < maxArticlesPerPage * currentSlide + maxArticlesPerPage &&
          index >= maxArticlesPerPage * currentSlide) {
            $(articleButton).addClass('slide-active').attr('aria-hidden', 'false');
          } else {
            $(articleButton).removeClass('slide-active').attr('aria-hidden', 'true');
          }
        }
      );
      overlayButtonList.data('current-slide', currentSlide);
      overlayButtonList.find('.carousel-index').removeClass('active').parent()
                      .find(`[data-index="${currentSlide}"]`).addClass('active');
      //classes used to hide the next/prev buttons
      overlayButtonList.removeClass('last-slide first-slide');
      if (currentSlide === 0) {
        overlayButtonList.addClass('first-slide');
      } else if (currentSlide === maxSlide) {
        overlayButtonList.addClass('last-slide');
      }
    }
  },
  addControlButtons: function (carousel) {
    const prevButton = '<div class="slide-controller slide-prev icon icon-nav-carrot-white" role="button" tabIndex="0" aria-controls="carousel-active"></div>';
    const nextButton = '<div class="slide-controller slide-next icon icon-nav-carrot-white" role="button" tabIndex="0" aria-controls="carousel-active"></div>';
    carousel.append(nextButton);
    carousel.prepend(prevButton);
    $(carousel).on('click keypress', '.slide-next', function (event) {
      if (a11yClick(event)) {
        PursuitsIndex.toggleSlide(carousel, carousel.data('current-slide') + 1);
      }
    });
    $(carousel).on('click keypress', '.slide-prev', function (event) {
      if (a11yClick(event)) {
        PursuitsIndex.toggleSlide(carousel, carousel.data('current-slide') - 1);
      }
    });
  },
  addCarouselIndex: function (carousel, carouselConfig) {
    const carouselIndexHTML = '<div class="carousel-controller"></div>';
    carousel.append(carouselIndexHTML);
    const maxSlide = Math.ceil(carousel.find('a').length / carouselConfig.maxArticlesPerPage) - 1;
    const carouselIndex = carousel.find('.carousel-controller');
    for (let index = 0; index <= maxSlide; index++) {
      carouselIndex.append(`<div class="carousel-index" data-index="${index}"></div>`);
    }
    $(carousel).on('click keypress', '.carousel-index', function (event) {
      if (a11yClick(event)) {
        PursuitsIndex.toggleSlide(carousel, $(this).data('index'));
      }
    });
  }
};

export default PursuitsIndex;
