import { Debugger } from '../utilities/util-debug';
import { Enterprise } from '../modules/Enterprise';

export const locationMapHandle = {
  init() {
    Debugger.use('content:init').log('32 locationMap');
    let NearbyLocations = require('./LocationMap/index');

    // let locationDetail = Enterprise.global.pages.locationDetail;
    let locationDetail = Enterprise.global.locationDetail;
    let locationZoom = Enterprise.global.pages.locationZoom;
    if (locationDetail) {
      NearbyLocations.init(locationDetail, locationZoom);
    } else {
      // Debugger.use('content:init').log('locationmap not fired');
    }
  }
};
