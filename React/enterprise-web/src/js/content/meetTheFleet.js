import MeetTheFleet from './MeetTheFleet/index';
import { Debugger } from '../utilities/util-debug';

export const meetTheFleetHandle = {
  init() {
    Debugger.use('content:init').log('12 meetTheFleet');
    let $meetTheFleet = $('.meet-the-fleet');
    //DOCUMENT READY
    $(function () {
      if ($meetTheFleet.length > 0) {
        MeetTheFleet.init();
      }
    });
  }
}
