import { Debugger, debug } from '../../utilities/util-debug';
import { Enterprise } from '../../modules/Enterprise';
const debugScripts = Enterprise.global.settings.debugContentScripts;

const FeaturePage = debug({
  name: 'FeaturePage',
  logger: {},
  isDebug: debugScripts,
  init() {
    this.logger.log('external script fired');
    function initialize() {
      Debugger.use('FeaturePage').log('initialize() map fired');
      function getViewLargerMapBtn(url){
        var label = enterprise.i18nReservation.utopian_0004 || 'View Interactive Map';
        var btn = $('<div id="view-larger">' + label + '</div>');
        btn.bind('click', function(){
          window.open(url);
        });
        return btn[0];
      }

      $('.map-canvas').each(function(i) {
        var lm= $(this);
        lm.attr('id', 'map-canvas-'+ i);
        var map,
          zoomLevel = lm.data('zl'),
          mapCenter = new google.maps.LatLng(lm.data('lat'), lm.data('lng')),
          mapOptions = {
            zoom: zoomLevel,
            maxZoom: zoomLevel,
            minZoom: zoomLevel,
            center: mapCenter,
            scrollwheel: false,
            disableDefaultUI: true,
            draggable: false,
            panControl: false,
          };
        map = new google.maps.Map(document.getElementById('map-canvas-'+i), mapOptions);
        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(lm.data('lat'), lm.data('lng')),
          map: map,
          title: lm.data('title')
        });

        // "View Interactive Map" Button
        var googleMapsUrl = 'https://maps.google.com/maps?z=15&q='+lm.data('lat')+','+lm.data('lng');
        var viewLargerMapBtn = getViewLargerMapBtn(googleMapsUrl);
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(viewLargerMapBtn);
      });
    }

    google.maps.event.addDomListener(window, 'load', initialize);

  }
});

module.exports = FeaturePage;

