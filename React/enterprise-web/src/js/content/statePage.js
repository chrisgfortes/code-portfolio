import { Debugger } from '../utilities/util-debug';
import { Enterprise } from '../modules/Enterprise';

export const statePageHandle = {
  init() {
    Debugger.use('content:init').log('34 State Page');
    let StatePage = require('./StatePage/index');
    let stateData = Enterprise.global.pages.statePage;
    $(function(){
      if (stateData) {
        StatePage.init(stateData);
      }
    })
  }
};
