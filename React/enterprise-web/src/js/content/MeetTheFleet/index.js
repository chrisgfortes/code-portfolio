import { debug } from '../../utilities/util-debug';
import { Enterprise } from '../../modules/Enterprise';
const debugScripts = Enterprise.global.settings.debugContentScripts;

export default debug({
  name: 'MeetTheFleet',
  isDebug: debugScripts,
  logger: {},
  init() {
    this.logger.log('init() fired');
    var prevCarTimer = 0;
    var activeCarTimer = 0;
    var nextCarTimer = 0;
    var cleanUpTimer = 0;
    var fleetCycle = 0,
      onTheMove = false,
      autoCycle = true,
      fleetCycleInterval = 5000,
      leftOffStage = '120%',
      leftOnStage = '25%',//'20%',
      leftprevCar = '-25%',//'-45%',
      leftnextCar = '75%',//'85%',
      $fleet = $('.fleet-content'),
      $cars = $fleet.find(".single-car"),
      $onStageCar = $cars.first(),
      $prevCar = $cars.last(),
      $nextCar = $cars.eq(1),
      $carInfo = $fleet.find('.car-information'),
      $carInfoHeading = $fleet.find('.car-class'),
      $carInfoText = $fleet.find('.car-text');


    //START STATES CSS
    $cars.css({left: leftOffStage});
    $onStageCar.addClass('current').css({left: leftOnStage}).addClass("active");
    $prevCar.css({left: leftprevCar}).addClass("active");
    $nextCar.css({left: leftnextCar}).addClass("active");
    //start states HTML
    $carInfoHeading.html($onStageCar.data('class'));
    $carInfoText.html($onStageCar.find('p').html());


    //BUILD NAV
    var navHTML = '<div class="fleet-carousel-nav cf">';
    for (var i = 0; i < $cars.length; i++) {
      navHTML += '<button class="fleet-carousel-nav-item' + (!i ? ' current' : '') + '" aria-selected="' + !i + '" aria-controls="fleet-cars-' + i + '" aria-labelledby="fleet-cars-' + i + '"></button>';
    }
    navHTML += '</div>';
    //update DOM & cache var
    $fleet.append(navHTML);
    var $fleetNav = $('.fleet-carousel-nav');


    //ON CLICK ADVANCE THE FLEET
    function advanceFleet(prevCar, onStageCar, nextCar) {
      //prevent quick succession clicks
      if (onTheMove) {
        return;
      }
      onTheMove = true;

      //fleet auto cycle
      clearTimeout(fleetCycle);
      fleetCycle = 0;

      //local vars
      var $first = prevCar || $prevCar,
        $prev = onStageCar || $onStageCar,
        $active = nextCar || $nextCar,
        $next = $nextCar.next().length ? $nextCar.next() : $cars.first();

      //update larger scope vars
      $onStageCar = $active;
      $nextCar = $next;
      $prevCar = $prev;

      //update nav
      $fleetNav.children().removeClass('current').attr('aria-selected', false).eq($active.index()).addClass('current').attr('aria-selected', true);

      //info - update content and fade in
      $carInfoHeading.css({opacity: 0});
      $carInfoText.css({opacity: 0});
      let infoTimer = setTimeout(function () {
        $carInfoHeading.html($onStageCar.data('class')).css({opacity: 1});
        $carInfoText.html($onStageCar.find('p').html()).css({opacity: 1});
      }, 500);

      //move previous vehicle (only partially seen) off screen
      $first.addClass("active").css({left: "-165%"});

      //old "on stage" vehicle - coming off
      prevCarTimer = setTimeout(function () {
        $prev.addClass("active").css({left: leftprevCar});
        //get the next up car over to the right (off stage)
        $next.removeClass("active").css({left: leftOffStage});
      }, 100);

      //old "on deck" vehicle - coming on stage
      activeCarTimer = setTimeout(function () {
        $active.addClass("active").css({left: leftOnStage});
      }, 300);

      //new vehicle that is "on deck"
      nextCarTimer = setTimeout(function () {
        $next.addClass("active").css({left: leftnextCar});
      }, 400);

      //clean up the mess (css classes, lingering inline styles, and click blocking)
      cleanUpTimer = setTimeout(function () {
        $cars.not('.active').css({left: leftOffStage});
        $cars.removeClass('active').removeClass('current').attr('aria-selected', false);
        $active.addClass('current').attr('aria-selected', true);
        //un-block clicks
        onTheMove = false;
        //auto cycle - recurse on the function
        if (autoCycle) fleetCycle = setTimeout(advanceFleet, fleetCycleInterval);
      }, 1000);
    }


    //AUTO CYCLE
    fleetCycle = setTimeout(advanceFleet, fleetCycleInterval);


    //BIND CLICK EVENT
    $(".fleet-cars").on("click faux_click", function () {
      autoCycle = false;
      advanceFleet();
    });


    //BIND MOUSE EVENTS - to pause/resume auto cycle
    $cars.on('mouseenter', function () {
      //only hover on current car to stop carousel
      if ($(this).hasClass('current')) {
        clearTimeout(fleetCycle);
        fleetCycle = 0;
        autoCycle = false;
      }
    })
      .on('mouseleave', function () {
        //game on! start the carousel back up
        if ($(this).hasClass('current') && !fleetCycle) {
          fleetCycle = setTimeout(function () {
            advanceFleet();
          }, fleetCycleInterval);
          autoCycle = true;
        }
      });
  }
});
