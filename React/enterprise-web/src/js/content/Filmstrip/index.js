import { debug } from '../../utilities/util-debug';
import { Enterprise } from '../../modules/Enterprise';
const debugScripts = Enterprise.global.settings.debugContentScripts;

/**
 * FILM STRIP
 * This object contains all of the functionality to allow for a responsive carousel
 * Please note that this carousel requires that all of the "slides" be the same size.
 * @todo make this a class?
 */
const filmstripObject = debug({
  name: 'filmstripObject',
  isDebug: debugScripts,
  logger: {},
  base: null,
  track: null,
  items: null,
  itemWidth: null,
  itemMargin: null,
  trackWidth: null,
  next: null,
  prev: null,
  itemsPerView: null,
  advancedDistance: 0,
  resizeTimer: null,
  detailsPanel: null,
  detailsPanelTitle: null,
  clicksPerTrack: 0, // default
  clicks: null,
  offset: 0,

  //INIT
  init: function ($carousel) {
    this.logger.log('filmstrip fired');
    let t = this;

    //Update local variables
    t.base = $carousel;
    t.track = $carousel.find('.category-nav-wrapper .category-nav .track');
    t.items = $carousel.find('.track-item');
    t.itemWidth = t.items.outerWidth(true);
    t.itemMargin = parseInt(t.items.css('margin-right'), 10);
    t.trackWidth = t.itemWidth * t.items.length;
    t.next = $carousel.find('.nxt');
    t.prev = $carousel.find('.prev');
    t.itemsPerView = t.calcNumItemsPerView();
    t.detailsPanel = $carousel.find('.category-details-panel');
    t.detailsPanelTitle = t.detailsPanel.find('p.beta');
    t.detailsPanelText = t.detailsPanel.find('p.filmBody');
    t.detailsPanelBtn = t.detailsPanel.find('a.btn');
    t.detailsPanelCtaNext = t.detailsPanel.find('a.moreCtaNext');
    t.detailsPanelCtaPrev = t.detailsPanel.find('a.moreCtaPrev');
    t.offset = t.base.offset().top;
    t.clicksPerTrack = t.calculateClicks();
    t.clicks = 1;

    // Update track width
    t.track.css('width', t.trackWidth);

    // Bind events
    t.bindEvents();
    t.showNextPrev();
    // Select first item
    // if (window.matchMedia('(max-width: 760px)')) {
    //     t.onItemClick(t, t.items.first());
    // }
  },

  calculateClicks: function () {
    let t = this;
    let total = t.items.length;
    let isPrevClickMet = (t.clicks == t.clicksPerTrack) ? true : false;
    let itemsPer = t.itemsPerView;
    let work = total / itemsPer;
    let clickHigh = Math.ceil(work);
    let clickLow = Math.floor(work);
    let clickEnd;
    if (clickHigh > clickLow) {
      clickEnd = clickHigh;
    } else {
      clickEnd = clickLow;
    }

    // on resize
    // screen gets bigger
    // - we determine if we have exceeded the click threshold
    // screen gets smaller?
    // - we need to adjust if the click threshold had been met
    if (t.clicks > clickEnd || isPrevClickMet) {
      t.clicks = clickEnd;
    }

    return clickEnd;
  },

  //CALCULATE NUMBER OF ITEMS PER VIEW
  calcNumItemsPerView: function () {
    let t = this;
    let ww = $(window).outerWidth();
    let itemsPerView = Math.floor(ww / t.itemWidth);
    return itemsPerView;
  },

  /*
   * ADVANCE OR REVERSE TRACK - optionally accepts direction argument.
   *
   * @param direction (optional): If set to -1 or reverse, this will cause the
   *        track to move "backwards". If any other value is entered or this param
   *        is left blank it will default to moving forward
   */
  moveTrack: function (direction) {
    let t = this;
    let $track = t.track;
    let ww = $(window).outerWidth();
    let itemsPerView = t.itemsPerView ? t.itemsPerView : t.calcNumItemsPerView();
    let newAdvanceDistance = itemsPerView * t.itemWidth;
    let translateDistance = 0;
    let clickThreshold = t.clicks;

    //Choose direction - forward or backward (defaults to forward)
    //backward
    if (direction && (direction == 'back' || direction == 'backwards' || direction == 'reverse' || direction == -1)) {
      translateDistance = Math.max(t.advancedDistance - newAdvanceDistance, 0);
      clickThreshold = clickThreshold - 1;
    } else { // forwards
      //Window is wider than track
      if (ww >= t.trackWidth) {
        translateDistance = 0;
      }
      //Track is shorter than two window widths
      else if (t.trackWidth < newAdvanceDistance * 2) {
        translateDistance = t.trackWidth % ww + t.itemMargin;
      }
      //Last view
      else if ((t.advancedDistance + newAdvanceDistance) > (t.trackWidth - newAdvanceDistance)) {
        translateDistance = t.trackWidth - (ww - t.itemMargin);
      }
      //Plenty of space, slide in a full new view
      else {
        translateDistance = t.advancedDistance + newAdvanceDistance;

        //correct for fwd/rev switch bug - where after reaching end you revers direction twice and you see extra space
        if (translateDistance + t.itemWidth > t.trackWidth - newAdvanceDistance) translateDistance = t.trackWidth - (ww - t.itemMargin);
        //correct for fwd/rev switch - if full items are not showing (part is tucked off screen)
        else if (t.advancedDistance % newAdvanceDistance) translateDistance = translateDistance + (t.itemWidth - translateDistance % newAdvanceDistance);
      }
      clickThreshold = clickThreshold + 1;
    }
    t.clicks = clickThreshold;

    //update the CSS
    $track.css('transform', 'translate3d(-' + translateDistance + 'px, 0, 0)');
    //update the object
    t.advancedDistance = translateDistance;
    t.clicksPerTrack = t.calculateClicks();
    t.showNextPrev();
    // let activeItem = t.items.filter('.active');
    // let leftmostItem = t.items.eq(Math.ceil(translateDistance / newAdvanceDistance * itemsPerView));
  },

  showNextPrev: function () {
    let t = this;
    let $next = t.next;
    let $prev = t.prev;
    let maxSlideCalc = parseInt(t.items.length - (t.advancedDistance / t.itemWidth));
    let maxSlide = (maxSlideCalc <= t.itemsPerView) ? true : false;
    let minSlide = (t.advancedDistance === 0) ? true : false;

    // start conditions
    if (t.clicks === 1 && $prev.find(':visible') && minSlide) {
      t.prev.hide();
    } else {
      t.prev.show();
    }
    // end conditions
    if (t.clicks === t.clicksPerTrack && $next.find(':visible') && maxSlide) {
      t.next.hide();
    } else {
      t.next.show();
    }
  },

  recalculateTrackOffset: function () {
    let t = this;
    let $track = t.track;
    let ww = $(window).outerWidth();
    let distance = t.advancedDistance;
    let newAdvanceDistance = t.itemsPerView * t.itemWidth;
    let translateDistance = null;

    //Window is bigger than track OR the tack has not moved at all...
    if (ww >= t.trackWidth || distance == 0) translateDistance = 0;
    //Window got bigger & track needs to shift right
    else if ((distance + newAdvanceDistance) >= (t.trackWidth - newAdvanceDistance * 2)) translateDistance = t.trackWidth - (ww - t.itemMargin);

    //UPDATE THE POSITION
    if (translateDistance !== null) {
      //update the CSS
      $track.css('transform', 'translate3d(-' + translateDistance + 'px, 0, 0)');
      //update the object
      distance = translateDistance;
    }

    t.clicksPerTrack = t.calculateClicks();
    t.showNextPrev();
  },

  onResize: function (t) {
    //Recalculate number of items per screen
    t.itemsPerView = t.calcNumItemsPerView();
    //Update track offset
    t.recalculateTrackOffset();
    //Update the band's offset
    t.offset = t.base.offset().top;
    //Close detail
    //t.closeDetails();
  },

  // closeDetails: function () {
  //     var t = this;

  //     t.items.removeClass('active');
  //     t.detailsPanel.removeClass('active');
  // },

  //CAROUSEL ITEM CLICK
  onItemClick: function (t, $this) {
    //If details for the current item are open, close the details
//                    if ($this.hasClass('active')) {
//                        t.closeDetails();
//                    }
    //Otherwise open the details
//                    else {
    //Scoped vars
    let $details = t.detailsPanel;
    let $title = t.detailsPanelTitle;
    let $text = t.detailsPanelText;
    let $btn = t.detailsPanelBtn;
    let $ctaNext = t.detailsPanelCtaNext;
    let $ctaPrev = t.detailsPanelCtaPrev;
    let $window = $(window);
    let top = $window.scrollTop();

    //data attribute values
    let url = $this.data('url') ? (enterprise.aem.path + '/' + $this.data('url').split('/').splice(4, 6).join('/')) : '#';
    let linktext = $this.data('linktext') ? $this.data('linktext') : '';
    let nextimage = $this.data('nextimage') ? ('url(' + $this.data('nextimage') + ')') : '';
    let nexturl = $this.data('nexturl') ? (enterprise.aem.path + '/' + $this.data('nexturl').split('/').splice(4, 6).join('/')) : '#';
    let previmage = $this.data('previmage') ? ('url(' + $this.data('previmage') + ')') : '';
    let prevurl = $this.data('prevurl') ? (enterprise.aem.path + '/' + $this.data('prevurl').split('/').splice(4, 6).join('/')) : '#';

    //Update HTML
    $title.text($this.data('title'));
    $text.text($this.data('text'));
    $btn.attr('href', url).text(linktext);
    $ctaNext.css('background-image', nextimage).attr('href', nexturl);
    $ctaPrev.css('background-image', previmage).attr('href', prevurl);

    //Show the panel
    // $details.addClass('active');
    //Toggle "active" class (to show/hide active notch)
    // $this.addClass('active')
    // .siblings().removeClass('active');

    // Redirect to URL (ECR-10276)
    if (url) {
      window.location = url;
    }

    //Scroll Window
//                        if (t.offset != top) $window.scrollTop(t.offset);
//                    }

  },

  //CLICK NEXT BTN
  onNextBtnClick: function (t, $this) {
    if (!$this.hasClass('disabled')) {
      //   t.closeDetails();
      t.moveTrack('reverse');
    }
  },

  //CLICK PREV BTN
  onPrevBtnClick: function (t, $this) {
    if (!$this.hasClass('disabled')) {
      //   t.closeDetails();
      t.moveTrack();
    }
  },

  //BIND EVENTS TO DOM ELEMENTS
  bindEvents: function () {
    var t = this;

    //WINDOW RESIZE
    $(window).on('resize.carousel faux_resize.carousel', function () {
      window.clearTimeout(t.resizeTimer);
      t.resizeTimer = setTimeout(function () {
        t.onResize(t);
      }, 100);
    });
    //PREV BUTTON(S)
    t.prev.on('click', function (e) {
      e.preventDefault();
      t.onNextBtnClick(t, $(this));
    });

    //NEXT BUTTON(S)
    t.next.on('click', function (e) {
      e.preventDefault();
      t.onPrevBtnClick(t, $(this));
    });

    //ITEM CLICK
    t.items.on('click', function (e) {
      if (!e.isTrigger) {
        e.preventDefault();
        t.onItemClick(t, $(this));
      }
    });
  },
});

module.exports = filmstripObject;
