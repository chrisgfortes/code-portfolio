import { debug } from '../../utilities/util-debug';
import { Enterprise } from '../../modules/Enterprise';
const debugScripts = Enterprise.global.settings.debugContentScripts;

const MagazineCarouselObject = debug({
  name: 'MagazineCarouselObject',
  isDebug: debugScripts,
  logger: {},
  init: function () {
    this.logger.log('init() fired');
    var count = 0;
  $(".carousel-container").children().css({left: "100%", position: "absolute"}).first().css({
    left: "0%",
    position: "relative"
  }).addClass("active");
  $(".carousel-controls .next").on("click", function () {
    var $this = $(".carousel-container"), $kids = $this.children(), $active, $previous;
    count++;
    if (count === $kids.length) {
      count = 0;
    }
    $active = $kids.eq(count);
    if (count > 0) {
      $previous = $kids.eq(count - 1);
    } else {
      $previous = $kids.last();
    }
    $previous.addClass("active").css({left: "-100%"});
    $active.addClass("active").css({left: 0});
    setTimeout(function () {
      $kids.removeClass("active").css({left: "100%", position: "absolute"});
      $active.css({left: 0, position: "relative"});
    }, 400)
  });
  $(".carousel-controls .prev").on("click",
    function () {
      var $this = $(".carousel-container"), $kids = $this.children(), $active, $previous;
      count--;
      if (count === -1) {
        count = ($kids.length - 1);
      }
      $active = $kids.eq(count);
      if (count === ($kids.length - 1)) {
        $previous = $kids.eq(0);
      } else {
        $previous = $kids.eq(count + 1);
      }
      $active.css({left: "-100%", position: "absolute"});
      setTimeout(function () {
        $previous.addClass("active").css({left: "100%", position: "absolute"});
        $active.addClass("active").css({left: 0, position: "relative"});
      }, 50)
      setTimeout(function () {
        $kids.removeClass("active").css({left: "100%", position: "absolute"});
        $active.css({left: 0, position: "relative"});
      }, 450)

    });
  }
});

module.exports = MagazineCarouselObject;
