import { debug } from '../../utilities/util-debug';
import { Enterprise } from '../../modules/Enterprise';
const debugScripts = Enterprise.global.settings.debugContentScripts;

// services4upBand
const ServiceBandObject = debug({
  name: 'ServiceBandObject',
  isDebug: debugScripts,
  logger: {},
  init: function(serviceItems) {
    this.logger.log('external script fired');
    var buttons = serviceItems.find('.btn');

    buttons.click(function(e) {
      var selectedIndex = $(e.currentTarget).data('index'),
        selectedItem = $(serviceItems[selectedIndex]);

      serviceItems.removeClass('active');
      selectedItem.addClass('active');
    });
  }
});

module.exports = ServiceBandObject;
