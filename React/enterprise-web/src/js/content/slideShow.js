/**
 * @Todo: Remove the script loader and the AssetManager from this and do it right
 */
import { Debugger } from '../utilities/util-debug';
import AssetManager from '../modules/AssetManager';

export const slideShowHandle = {
  init() {
    Debugger.use('content:init').log('18 slideShow');
    let slideShow = require('./Slideshow/index');
    let $slideShow = $('.basic-photo-slider');
    //DOCUMENT READY
    $(function () {
      if ($slideShow.length > 0) {
        // console.log('slideshow?');
        AssetManager.dangerouslyLoadCSSDependency('slickCSS', () => {
          console.log('we just loaded the slideshow css file');
        });
        AssetManager.dangerouslyLoadJSDependency('slickJS')
          .done(() => {
            console.log('we just loaded the slideshow js file');
            let SlideShow = Object.create(slideShow);
            SlideShow.init();
          })
      }
    });
  }
}
