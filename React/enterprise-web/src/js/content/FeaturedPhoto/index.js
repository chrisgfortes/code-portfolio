import { LazyLoadUpdate } from '../../modules/outerDomManager';
import { Enterprise } from '../../modules/Enterprise';
import { debug } from '../../utilities/util-debug';
const debugScripts = Enterprise.global.settings.debugContentScripts;

const FeaturedPhotoObject = debug({
  name: 'FeaturedPhotoObject',
  isDebug: debugScripts,
  logger: {},
  init: function() {
    this.logger.log('init() fired');
    var count = $('.end-count').html();
    var leftArrow = '<div class="slick-prev icon icon-arrow-left"></div>';
    var rightArrow = '<div class="slick-next icon icon-arrow-right"></div>';
    var carouselNav = $('.carousel-nav');
    var carouselContent = $('.carousel-content');
    carouselContent.slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: false,
      prevArrow: leftArrow,
      nextArrow: rightArrow,
      arrows: true,
      focusOnSelect: true,
      responsive: [
        {
          breakpoint: 1000,
          settings: {
            speed: 800,
            arrows: true,
            swipeToSlide: true
          }
        },

        {
          breakpoint: 765,
          settings: {
            speed: 800,
            arrows: false,
            swipeToSlide: true
          }
        }
      ]
    });

    carouselNav.slick({
      speed: 800,
      cssEase: 'linear',
      slidesToShow: 5,
      slidesToScroll: 5,
      dots: false,
      infinite: false,
      prevArrow: leftArrow,
      nextArrow: rightArrow,
      arrows: true,
      centerMode: false,
      focusOnSelect: false,
      variableWidth: true,
      swipe: false,
      responsive: [
        {
          breakpoint: 1185,
          settings: {
            speed: 800,
            arrows: true,
            slidesToShow: 4,
            slidesToScroll: 4,
          }
        },
        {
          breakpoint: 1035,
          settings: {
            speed: 800,
            arrows: false,
            swipe: true,
            swipeToSlide: true,
          }
        },
        {
          breakpoint: 840,
          settings: {
            speed: 800,
            arrows: false,
            swipe: true,
            swipeToSlide: true,
            slidesToShow: 2,
            slidesToScroll: 6,
            cssEase: 'none',
          }
        }
      ]
    });
    carouselContent.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
      LazyLoadUpdate();
      $('.start-count').html(nextSlide + 1);
    });

    $('.end-count').html(carouselNav.slick('getSlick').slideCount);

    $('.carousel-nav div div img').on('click', function (e) {
      e.preventDefault();
      var slideIndex = $(this).parent().index();
      $('.carousel-content').slick('slickGoTo', parseInt(slideIndex));
    });

    $('.carousel-content').on('afterChange', function (event, slick, currentSlide) {
      if ($('.carousel-nav div').hasClass('slick-current')) {
        $('.carousel-nav div').removeClass('slick-current');
      }
      $('.carousel-nav .slick-slide').eq(currentSlide).trigger('click').addClass('slick-current');
    });

    var navAddClass = function () {
      var currentslide = $('.carousel-nav').slick('slickCurrentSlide');
      var current = $('.carousel-content').slick('slickCurrentSlide');
      $('.carousel-nav .slick-slide').eq(currentslide).removeClass('slick-current');
      $('.carousel-nav .slick-slide').eq(current).addClass('slick-current');
    };


    if (count > 5) {
      $('.carousel-nav div.slick-next').on('click', navAddClass);

      $('.carousel-nav div.slick-prev').on('click', navAddClass);

      $('.carousel-content div.slick-next').on('click', function (e) {
        if ($('.start-count').html() % 6 == 0) {
          $('.carousel-nav .slick-next').trigger('click');
        }
      });
      $('.carousel-content div.slick-prev').on('click', function (e) {
        if ($('.start-count').html() % 5 == 0) {
          $('.carousel-nav .slick-prev').trigger('click');
        }
      });

      $('.carousel-nav div div img').on('click', function (e) {
        if ($('.start-count').html() % 6 == 0) {
          $('.carousel-nav .slick-next').trigger('click');
        }
      });
    }

    carouselNav.on('swipe', navAddClass);

    if (window.matchMedia("(max-width: 800px)").matches) {
      carouselNav.on('swipe', function (event, slick, direction) {
        if (direction == left) {
          carouselNav.slick('slickSetOption', 'slidesToScroll', count - 1, true);
        } else if (direction == right) {
          carouselNav.slick('slickSetOption', 'slidesToScroll', count - 1, true);
        }
      });
      $('.carousel-nav div div img').on('click', navAddClass);
    }
  }
});

module.exports = FeaturedPhotoObject;
