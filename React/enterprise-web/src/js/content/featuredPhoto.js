/**
 * @Todo: Remove the script loader and the AssetManager from this and do it right
 */
import { Debugger, debug } from '../utilities/util-debug';
import { Enterprise } from '../modules/Enterprise';
import AssetManager from '../modules/AssetManager';

const debugScripts = Enterprise.global.settings.debugContentScripts;

export const featuredPhotoHandle = debug({
  name: 'featuredPhotoHandle',
  isDebug: debugScripts,
  logger: {},
  init() {
    Debugger.use('content:init').log('17 featuredPhoto');
    let featuredPhoto = require('./FeaturedPhoto/index');
    let $featuredPhoto = $('.featured-photo-slider');
    //DOCUMENT READY
    $(function () {
      // console.log('featured photo?');
      if ($featuredPhoto.length > 0) {
        AssetManager.dangerouslyLoadCSSDependency('slickCSS', () => {
          featuredPhotoHandle.logger.log('we just loaded the slick css dependency');
        });
        AssetManager.dangerouslyLoadJSDependency('slickJS')
        .done(() => {
          featuredPhotoHandle.logger.log('we just loaded the slick JS file');
          let FeaturedPhoto = Object.create(featuredPhoto);
          FeaturedPhoto.init();
        });
      }
    });
  }
});
