import { Debugger, debug } from '../../utilities/util-debug';
import { Enterprise } from '../../modules/Enterprise';
const debugScripts = Enterprise.global.settings.debugContentScripts;

const ViewfinderPage = debug({
  name: 'ViewfinderPage',
  isDebug: debugScripts,
  logger: {},
  init(listMap) {
    this.logger.log('external script fired');
    function initialize() {
      Debugger.use('ViewfinderPage').log('map initialize()');
      var lm = $(listMap);
      //locations.forEach(function(location, index){
      var map,
        zoomLevel = 15,
        mapCenter = new google.maps.LatLng(lm.data('lat'), lm.data('lng')),
        mapOptions = {
          zoom: zoomLevel,
          center: mapCenter,
          scrollwheel: false,
          disableDefaultUI: true
        };
      map = new google.maps.Map(listMap, mapOptions);
      new google.maps.Marker({
        position: new google.maps.LatLng(lm.data('lat'), lm.data('lng')),
        map: map,
        title: lm.data('title')
      });
    }
    google.maps.event.addDomListener(window, 'load', initialize);

  }
});

module.exports = ViewfinderPage;
