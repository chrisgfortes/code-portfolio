/**
 * Content Scripts
 * @todo: make each a callable module instead of something that just run automatically.
 *  Example: MeetTheFleet
 * @todo: create a system where these are init'd "automagically" based on a handle to module mapping system
 * @todo: reservationpage (reserve.html) has a number of inline scripts, include Google Maps Marker With Label, etc.
 */
import TimeStamp from '../modules/TimeStamp';
TimeStamp.mark('content.js:init()');

// content scripts
import { Map } from './FeaturedCity/Map';
import { ListHideShow } from './FeaturedCity/ListHideShow';
import { Base } from './FeaturedCity/Base';
import { MapData } from './FeaturedCity/MapData';
import { accordionHandle } from './accordion'; // todo: move this, it's functional
import { emailHandle } from './email.js';
import { filmstrip } from './filmstrip';
import { upgradeHandle } from './upgrade';
import { interrupterTileBandHandle } from './interrupterTileBand';
import { bandsCaptionTextHandle } from './BandsCaptionText';
import { pursuitsHandle} from './pursuits';
import { meetTheFleetHandle } from './meetTheFleet';
import { currencySelectorHandle } from './currencySelector';
import { mobileCurrencySelectorHandle } from './mobileCurrencySelector';
import { carouselContainerHandle } from './carousel';
import { magazineCarouselHandle } from './magazineCarousel';
import { featuredPhotoHandle } from './featuredPhoto';
import { slideShowHandle } from './slideShow';
import { magazineIntroBandHandle } from './magazineIntroBand';
import { driveAllianceHandle } from './driveAlliance';
import { serviceBandHandle } from './services4upBand';
import { redirectBandHandle } from './redirectBand';
import { ctaBandHandle } from './ctaBand';
import { categoryBandHandle } from './categoryBand';
import { canZoomHandle } from './canZoom';
import { introLoginBandHandle } from './introLoginBand';
import { explorePageMapHandle } from './explorePageMap';
import { featurePageHandle } from './featurePage';
import { travelWayPointsHandle } from './travelWayPoints';
import { roadTripPage } from './roadtripPage';
import { nearbyLocationHandle } from './nearbyLocations';
import { locationMapHandle } from './locationMap';
import { countryPageHandle } from './countryPage';
import { statePageHandle } from './statePage';
import { cityPageHandle } from './cityPage';
import { viewfinderPageHandle } from './viewfinderPage';
import { exploreListItemBandHandle } from './exploreListItemBand';
import { formAddress } from './formAddress';

import { debug } from '../utilities/util-debug';

export default debug({
  name: 'content:init',
  isDebug: false,
  logger: {},
  init() {
    // TimeStamp.mark('content.js:init()');
    this.logger.log('init()');

    /**
     * @todo DO ALL THIS DYNAMICALLY
     * @todo: EACH OF THESE should do a check for a DOM "handle" (or handles) then bind
     *  the doc.ready handler which we can replace later
     */
    Map.init(); // 01
    ListHideShow.init(); // 01
    Base.init();  //03
    MapData.init();  //04 Featured City map need some delay. Otherwise map doesn't update.
    accordionHandle.init(); //05 // todo: note this is functional and should move to content.js
    emailHandle.init();  //06
    filmstrip.init();  //07
    upgradeHandle.init();  //08
    interrupterTileBandHandle.init();  //09
    bandsCaptionTextHandle.init();  //10
    pursuitsHandle.init();  //11
    meetTheFleetHandle.init(); // 12
    currencySelectorHandle.init(); // 13
    mobileCurrencySelectorHandle.init(); // 14
    carouselContainerHandle.init(); // 15
    magazineCarouselHandle.init(); // 16
    featuredPhotoHandle.init(); // 17
    slideShowHandle.init(); // 18
    magazineIntroBandHandle.init(); // 19
    driveAllianceHandle.init(); // 20
    serviceBandHandle.init(); // 21
    redirectBandHandle.init(); // 22
    ctaBandHandle.init(); // 23
    categoryBandHandle.init(); // 24
    canZoomHandle.init(); // 25
    introLoginBandHandle.init(); // 26
    explorePageMapHandle.init(); // 27
    featurePageHandle.init(); // 28
    travelWayPointsHandle.init(); // 29
    roadTripPage.init(); // 30
    nearbyLocationHandle.init(); // 31
    locationMapHandle.init(); // 32
    countryPageHandle.init(); // 33
    statePageHandle.init(); // 34
    cityPageHandle.init(); // 35
    viewfinderPageHandle.init(); // 36
    exploreListItemBandHandle.init(); // 37
    formAddress.init(); //38

    TimeStamp.stop('content.js:init()');
  }
})

