import { Debugger } from '../utilities/util-debug';
import { Enterprise } from '../modules/Enterprise';

export const explorePageMapHandle = {
  init() {
    Debugger.use('content:init').log('27 ExplorePageMap');
    let ExplorePageMap = require('./ExplorePageMap/index');
    let mapList = document.querySelectorAll('.list-map');
    let locations = Enterprise.global.pages.explorePageLocations;
    $(function(){
      if (mapList.length > 0 && locations) {
        ExplorePageMap = Object.create(ExplorePageMap);
        ExplorePageMap.init(locations);
      }
    })
  }
};
