const CategoryBandObject = {
  init: function (detailsPanel) {
    // Setup track items
    var track = $('.track'),
      items = track.find('.track-item'),
      itemWidth = 294 + 20, // item width plus margin
      trackWidth = itemWidth * items.length;

    // Setup track buttons
    var controls = $('.prev, .nxt');

    // Init track width based on items
    track.css("width", trackWidth);

    controls.click(function (e) {
      var currentPosition = parseInt(track.css('left')),
        actions = $(e.currentTarget).data('action');

      if (actions === "nxt" && currentPosition >= 50) {
        return;
      }
      if (actions === "prev" && -currentPosition >= (trackWidth - (itemWidth * 5))) {
        return;
      }
      else {
        track.css('left', (actions === "prev") ? currentPosition - itemWidth : currentPosition + itemWidth);
      }
    });

    items.click(function (e) {
      var selectedIndex = $(e.currentTarget).data('index');
      detailsPanel.addClass('active');
    });
  }
};

module.exports = CategoryBandObject;
