import { Debugger } from '../utilities/util-debug';

export const magazineIntroBandHandle = {
  init() {
    Debugger.use('content:init').log('19 magazineIntroBand');
    let magazineIntroBand = require('./MagazineIntroBand/index');
    let $magazineIntroBand = $('.magazine-intro-band');
    //DOCUMENT READY
    $(function () {
      if ($magazineIntroBand.length > 0) {
        let MagazineIntroBand = Object.create(magazineIntroBand);
        MagazineIntroBand.init();
      }
    });
  }
}

