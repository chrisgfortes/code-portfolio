import EmailSignUp from './../../components/Email/SignUp';
import EmailConfirmation from './../../components/Email/Confirmation';
import GenericBaobabMixinWrapper from './../../components/GenericBaobabMixinWrapper';

const EmailHandleObject = {
  init: function (EmailHandle, ConfirmationHandle) {
    if (EmailHandle) {
      EmailHandleObject.emailHandle(EmailHandle);
    }
    if (ConfirmationHandle) {
      EmailHandleObject.confirmationHandle(ConfirmationHandle);
    }
  },
  emailHandle: function(EmailHandle) {
    React.render(
    <GenericBaobabMixinWrapper
      tree = {ReservationStateTree}
      component = {EmailSignUp}/>,
      EmailHandle
    );
  },
  confirmationHandle: function(ConfirmationHandle) {
    React.render(
    <EmailConfirmation/>,
      ConfirmationHandle
    );
  }
};

module.exports = EmailHandleObject;
