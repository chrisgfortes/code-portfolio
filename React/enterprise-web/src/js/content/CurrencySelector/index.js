import { debug } from '../../utilities/util-debug';
import { Enterprise } from '../../modules/Enterprise';
const debugScripts = Enterprise.global.settings.debugContentScripts;

const CurrencySelectorModule = (function ($) {
  var $radioGroup,
    $currencyLabel,
    $currencyNode,
    endpointPrefix = enterprise.services.path,
    endpoint = endpointPrefix + "/changecurrency/",
    credentials = (endpointPrefix !== enterprise.solr.path) ? true : false;

  function init() {
    this.logger.log('external script fired');
    var cookieCurrency = getCookie();

    $radioGroup = $("input[name='currency']");
    $currencyNode = $(".currency");
    $currencyLabel = $currencyNode.find('.utility-nav-label');
    enterprise.selectedCurrency = enterprise.selectedCurrency != '' ? enterprise.selectedCurrency : $radioGroup.filter(':checked').val();
    attachListeners();

    if (cookieCurrency) {
      //Visually set the checked element
      $radioGroup .removeClass('selected')
        .filter(':checked')
        .prop('checked', false)
        .closest('label');
      $radioGroup .filter('[value='+cookieCurrency+']')
        .prop('checked', true)
        .closest('label')
        .addClass('selected');
      //Trigger change
      changeCurrency(cookieCurrency);
    }
  }

  function setCookie(currency) {
    document.cookie = 'view_currency_code=' + currency;
  }

  function getCookie() {
    var cookie = document.cookie
      .split(';')
      .filter(function(keyVal){
        keyVal = keyVal.split('=');

        return keyVal[0].replace(' ', '') === 'view_currency_code';
      });

    return cookie.length && cookie[0].split('=')[1];
  }

  function attachListeners() {
    $radioGroup.change(function (e) {
      CurrencySelectorModule.logger.log('changing');
      changeCurrency($(this).val());
      setCookie($(this).val());
    });
  }

  function changeCurrency(selectedValRaw) {
    var selectedVal = selectedValRaw ? selectedValRaw.toUpperCase() : 'USD';
    var selectedSymbol = enterprise.utilities.symbolMap[selectedVal] || "$";

    $radioGroup
      .filter(':checked')
      .closest('label')
      .addClass('selected')
      .siblings()
      .removeClass('selected');

    $.ajax({
      method: "GET",
      url: endpoint + selectedVal,
      xhrFields: {
        withCredentials: credentials
      },
      error: function (response) {
        enterprise.log('AJAX GET ERROR');
      },
      success: function (response, textStatus, request) {
        $currencyLabel.text(selectedVal + " (" + selectedSymbol + ")");
        // $currencyNode.removeClass('active');
        $currencyNode
          .filter('.active')
          .trigger('focus')
          .trigger('click');
        enterprise.selectedCurrency = selectedVal;
      }
    });
  }

  return debug({
    name: 'CurrencySelector',
    logger: {},
    isDebug: debugScripts,
    init: init
  })

})($);

module.exports = CurrencySelectorModule;
