import { Debugger } from '../utilities/util-debug';

export const categoryBandHandle = {
  init() {
    Debugger.use('content:init').log('24 categoryBand');
    let categoryBand = require('./CategoryBand/index');
    let $categoryBand = $('.category-details-panel');
    //DOCUMENT READY
    $(function () {
      if ($categoryBand.length > 0) {
        let CategoryBand = Object.create(categoryBand);
        CategoryBand.init($categoryBand);
      }
    });
  }
}

