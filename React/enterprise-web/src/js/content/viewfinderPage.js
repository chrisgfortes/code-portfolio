import { Debugger } from '../utilities/util-debug';

export const viewfinderPageHandle = {
  init() {
    Debugger.use('content:init').log('36 viewfinder');
    let ViewfinderPage = require('./ViewfinderPage/index');
    let viewFinderBand = document.querySelectorAll('.view-finder-band');
    let listMap = document.getElementById('list-map');
    //DOCUMENT READY
    $(function () {
      if (viewFinderBand && listMap) {
        ViewfinderPage.init(listMap);
      }
    });
  }
};
