import { Debugger } from '../utilities/util-debug';
import { Enterprise } from '../modules/Enterprise';

export const exploreListItemBandHandle = {
  init() {
    Debugger.use('content:init').log('37 ExploreListItemBand');
    let ExploreListItemBand = require('./ExploreListItemBand/index');
    let explore = Enterprise.global.pages.exploreListItemBand;
    $(function(){
      if (explore) {
        ExploreListItemBand.init(explore);
      }
    })
  }
};

