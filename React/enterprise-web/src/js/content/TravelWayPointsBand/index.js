import { Debugger, debug } from '../../utilities/util-debug';
import { Enterprise } from '../../modules/Enterprise';
const debugScripts = Enterprise.global.settings.debugContentScripts;

const TravelWayPointsBand = debug({
  name: 'TravelWayPointsBand',
  isDebug: debugScripts,
  logger: {},
  init(waypointData) {
    this.logger.log('external script fired');
    var markerSize = { x: 22, y: 40 };
    var MarkerLabel;
    var directionsService = new google.maps.DirectionsService();
    var directionsDisplay;
    var map;
    var waypoints = waypointData;

    var googlePath = 'https://www.google.com/maps/dir';
    var redirectPath = null;

    var wayPointList = waypoints.map(
      function(waypoint) {
        return {
          "location": new google.maps.LatLng(waypoint.latitude, waypoint.longitude),
          "stopover": false
        }
      }
    );

    var wayPointDrivingPoints = waypoints.map(
      function (wayPoint) {
        return {
          "location": wayPoint.latitude + ',' + wayPoint.longitude,
          "name": wayPoint.title
        }
      }
    );

    google.maps.Marker.prototype.setLabel = function (label) {
      this.label = new MarkerLabel({
        map: this.map,
        marker: this,
        text: label
      });
      this.label.bindTo('position', this, 'position');
    };

    MarkerLabel = function(options) {
      this.setValues(options);
      this.span = document.createElement('span');
      this.span.className = 'map-marker-label';
    };

    MarkerLabel.prototype = $.extend(new google.maps.OverlayView(), {
      onAdd: function() {
        this.getPanes().overlayImage.appendChild(this.span);
        var self = this;
        this.listeners = [
          google.maps.event.addListener(this, 'position_changed', function() { self.draw();    })];
      },
      draw: function() {
        var text = String(this.get('text'));
        var position = this.getProjection().fromLatLngToDivPixel(this.get('position'));
        this.span.innerHTML = text;
        this.span.style.left = (position.x - (markerSize.x / 2)) - (text.length * 3) + 10 + 'px';
        this.span.style.top = (position.y - markerSize.y + 40) + 'px';
      }
    });

    var valueUI = false;
    var zoomValue = false;
    if (window.matchMedia("(max-width: 600px)").matches) {
      valueUI = true;
      zoomValue = true;
    }
    function initialize() {

      Debugger.use('TravelWayPointsBand').log('initialize() map fired');

      directionsDisplay = new google.maps.DirectionsRenderer({
        suppressInfoWindows: true,
        suppressMarkers: true,
        map: map
      });
      var zoomLevel = 3,
        mapCenter = new google.maps.LatLng(41.850033, -87.6500523),
        mapOptions = {
          zoom: zoomLevel,
          disableDoubleClickZoom: zoomValue,
          center: mapCenter,
          draggable: false,
          scrollwheel: false,
          disableDefaultUI: valueUI
//        fullscreenControl: true
        };

      if (wayPointDrivingPoints && wayPointDrivingPoints.length) {
        for (var i = 0; i < wayPointDrivingPoints.length; i++) {
          googlePath += '/' + wayPointDrivingPoints[i].location;
        }
      }

      map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
      directionsDisplay.setMap(map);
      calcRoute();
      map.controls[google.maps.ControlPosition.TOP_RIGHT].push(viewLargerMap());
      document.getElementById('map-canvas').className= "active";
    }

    function viewLargerMap(){
      var label = enterprise.i18nReservation.utopian_0004 || 'View Interactive Map';
      var btn = $('<div id="view-larger">' + label + '</div>');
      btn.bind('click', function(){
        window.open(googlePath);
      });
      return btn[0];
    }

    function calcRoute() {
      var start = wayPointList[0].location,
        end = wayPointList[wayPointList.length - 1].location;

      var request = {
        origin: start,
        destination: end,
        waypoints: wayPointList,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING
      };

      directionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
          directionsDisplay.setDirections(response);
          for(var i = 0; i < wayPointList.length; i++) {
            var location = wayPointList[i].location;

            var marker = new google.maps.Marker({
              position: location,
              map: map,
              label: waypoints[i].markerLabel,
              title: waypoints[i].title,
              icon: '/etc/designs/ecom/dist/img/icons/png/map-pin-blank.png'
            });
          }
        }
      });
    }

    google.maps.event.addDomListener(window, 'load', initialize);


  }
});

module.exports = TravelWayPointsBand;
