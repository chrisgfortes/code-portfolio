/**
 * @todo: Get State Tree Manipulation out of the content scripts!!
 */
import { debug } from '../../utilities/util-debug';
import { Enterprise } from '../../modules/Enterprise';
import ReservationStateTree from '../../stateTrees/ReservationStateTree';
import ReservationCursors from '../../cursors/ReservationCursors';

const debugScripts = Enterprise.global.settings.debugContentScripts;

export default debug({
  name: 'IntroLoginBand',
  isDebug: debugScripts,
  logger: {},
  init($loginPanel) {
    this.logger.log('external script fired');
    $loginPanel.parent().hide();
    function tmpl() {
      $loginPanel.find('h3').text(enterprise.i18nReservation.eplusaccount_0025);
      $('.login-btn', $loginPanel).remove();
      let ccul='<ul>';
      ccul+='<li><a href="'+enterprise.aem.path+'/account.html#reservation">'+enterprise.i18nReservation.reservationnav_0038+'</a></li>'+
        '<li><a href="'+enterprise.aem.path+'/account.html#reward">'+enterprise.i18nReservation.eplusaccount_0033+'</a></li>'+
        '<li><a href="'+enterprise.aem.path+'/account.html#settings">'+enterprise.i18nReservation.eplusaccount_0034+'</a></li></ul>';
      $('.cta-container', $loginPanel).html(ccul);
      $loginPanel.parent().fadeIn();
    }
    // @todo: Get State Tree Manipulation out of the content scripts!!
    setTimeout(function () {
      (ReservationStateTree.select(ReservationCursors.userLoggedIn).get()) ? tmpl() : $loginPanel.parent().fadeIn();
    }, 500);
  }
});
