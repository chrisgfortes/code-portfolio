import { debug } from '../../utilities/util-debug';
import { Enterprise } from '../../modules/Enterprise';
const debugScripts = Enterprise.global.settings.debugContentScripts;

const MobileCurrencySelectorModule = (function ($) {
  var selector,
    endpointPrefix = enterprise.services.path,
    endpoint = endpointPrefix + "/changecurrency/",
    credentials = (endpointPrefix !== enterprise.solr.path) ? true : false;

  function init() {
    this.logger.log('external script fired');
    selector = $("#mobileCurrencySelector");
    attachListeners();
  }

  function attachListeners() {
    selector.change(function (e) {
      MobileCurrencySelectorModule.logger.log('changed');
      var selectedVal = $(this).val().toUpperCase();

      $.ajax({
        method: "GET",
        url: endpoint + selectedVal,
        xhrFields: {
          withCredentials: credentials
        },
        error: function (response) {
          enterprise.log('AJAX GET ERROR');
          console.log(response);
        },
        success: function (response, textStatus, request) {
          console.log(response);
        }
      });
    });
  }

  return debug({
    name: 'MobileCurrencySelector',
    logger: {},
    isDebug: debugScripts,
    init: init
  })

})($);

module.exports = MobileCurrencySelectorModule;
