import { debug } from '../../utilities/util-debug';
import { Enterprise } from '../../modules/Enterprise';
const debugScripts = Enterprise.global.settings.debugContentScripts;

const ExploreListItemBand = debug({
  name: 'ExploreListItemBand',
  isDebug: debugScripts,
  logger: {},
  init(exploreData) {
    this.logger.log('init() fired');
    let startLat = exploreData.startLat;
    let startLng = exploreData.startLng;
    let endLat = exploreData.endLat;
    let endLng = exploreData.endLng;
    let exploreElement = enterprise.pages.exploreListItemBand.element;

    google.maps.Marker.prototype.setLabel = function(label){
      this.label = new google.maps.MarkerLabel({
        map: this.map,
        marker: this,
        text: label
      });
      this.label.bindTo('position', this, 'position');
    };

    var MarkerLabel = function(options) {
      this.setValues(options);
      this.span = document.createElement('span');
      this.span.className = 'map-marker-label';
    };

    MarkerLabel.prototype = $.extend(new google.maps.OverlayView(), {
      onAdd: function() {
        this.getPanes().overlayImage.appendChild(this.span);
        var self = this;
        this.listeners = [
          google.maps.event.addListener(this, 'position_changed', function() { self.draw();    })];
      },
      draw: function() {
        var text = String(this.get('text'));
        var position = this.getProjection().fromLatLngToDivPixel(this.get('position'));
        this.span.innerHTML = text;
        this.span.style.left = (position.x - (markerSize.x / 2)) - (text.length * 3) + 10 + 'px';
        this.span.style.top = (position.y - markerSize.y + 40) + 'px';
      }
    });

    function initialize() {
      ExploreListItemBand.logger.log(`map initialize() ran; looking for map div#${exploreElement}`);
      var map,
        directionsService = new google.maps.DirectionsService(),
        start = new google.maps.LatLng(startLat, startLng),
        end = new google.maps.LatLng(endLat, endLng),
        directionsDisplay = new google.maps.DirectionsRenderer({
          suppressInfoWindows: false,
          suppressMarkers: false,
          map: map
        });

      var zoomLevel = 3,
        mapCenter = new google.maps.LatLng(41.850033, -87.6500523),
        mapOptions = {
          zoom: zoomLevel,
          center: mapCenter,
          scrollwheel: false,
          disableDefaultUI: true
        };

      var latlng = new google.maps.LatLng(-34.397, 150.644);
      var myOptions = {
        zoom: 8,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      map = new google.maps.Map(document.getElementById(exploreElement), mapOptions);
      directionsDisplay.setMap(map);
      calcRoute(start, end, directionsDisplay, directionsService, map);
    }

    function calcRoute(start, end, directionsDisplay, directionsService, map) {
      //var start = wayPointList.splice(0, 1),
      //  end = wayPointList.splice(wayPointList.length - 1, 1);

      var request = {
        origin: start,
        destination: end,
        travelMode: google.maps.TravelMode.DRIVING
      };

      directionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
          directionsDisplay.setDirections(response);
        }
      });
    }

    google.maps.event.addDomListener(window, 'load', initialize);

  }
});

module.exports = ExploreListItemBand;
