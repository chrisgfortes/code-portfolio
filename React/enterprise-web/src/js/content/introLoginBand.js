/**
 * from ecLoyaltyHeader.html
 */
import { Debugger } from '../utilities/util-debug';
import IntroLoginBand from './IntroLoginBand/index';

export const introLoginBandHandle = {
  init() {
    Debugger.use('content:init').log('26 introLoginBandHandle');
    //DOCUMENT READY (2)
    $(function () {
      let lp = $('#loyalty-login-panel');
      if (lp.length > 0) {
        IntroLoginBand.init(lp);
      }
    });
  }
}
