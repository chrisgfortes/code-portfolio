const Cookies = require('../../utilities/util-cookies');
const UpgradeObject = {
  init: function() {
    let cookieBrowser = Cookies.get('cookieBrowser');

    if (cookieBrowser !== null) {
      console.log('cookie found!.');
    } else {
      console.warn('no cookie.');
    }

    $('#goClassic').click(function () {
      window.location.href = enterprise.legacy.path;
    });

    $('#acceptBrowser').click(function (e) {
      e.preventDefault();
      let $link = $(this).attr('href');
      Cookies.setDays('cookieBrowser', true, 30);
      window.location.href = $link;
    });
  }
};

module.exports = UpgradeObject;
