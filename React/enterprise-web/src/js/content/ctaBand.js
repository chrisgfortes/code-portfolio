import { Debugger } from '../utilities/util-debug';

export const ctaBandHandle = {
  init() {
    Debugger.use('content:init').log('23 ctaBand');
    let ctaBand = require('./CtaBand/index');
    let $ctaBand = $('.business-intro-band');
    //DOCUMENT READY
    $(function () {
      if ($ctaBand.length > 0) {
        let CtaBand = Object.create(ctaBand);
        CtaBand.init();
      }
    });
  }
}
