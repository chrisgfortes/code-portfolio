import { debug } from '../../utilities/util-debug';
import { Enterprise } from '../../modules/Enterprise';
const debugScripts = Enterprise.global.settings.debugContentScripts;

const CarouselObject = debug({
  name: 'CarouselObject',
  isDebug: debugScripts,
  logger: {},
  init: function () {
    this.logger.log('external script fired init()');
    ($('.icon-globe').length > 1 && $('.breadcrumb-band ul li').length == 2) ? null : $('.moving-car, .main-road').remove();
    var count = 0;
    var carPos = 0;
    var tot = $(".carousel-container").children(':not(.main-road, .moving-car)').length;
    var mc = $('.moving-car');

    function moveCar(e) {
      if (count == 0) {
        carPos = 0;
        mc.addClass('flip');
      }
      else if (e.currentTarget.className == 'prev')
        mc.addClass('flip');
      else
        mc.removeClass('flip');
      if (carPos < 0) {
        carPos = (100 - (100 / tot));
        mc.removeClass('flip');
      }
      mc.css({left: carPos + "%"});
    }

    $(".carousel-container").children(':not(.main-road, .moving-car)').css({left: "100%"}).first().css({left: "0%"}).addClass("active");
    $(".carousel-controls .next").on("click", function (event) {
      var $this = $(".carousel-container"), $kids = $this.children(':not(.main-road, .moving-car)'), $active, $previous;
      count++;
      if (count === $kids.length) {
        count = 0;
      }
      $active = $kids.eq(count);
      if (count > 0) {
        $previous = $kids.eq(count - 1);
      } else {
        $previous = $kids.last();
      }

      $previous.addClass("active").css({left: "-100%"});
      $active.addClass("active").css({left: 0});

      //START CAR
      carPos += (100 / tot);
      moveCar(event);
      //END CAR
      setTimeout(function () {
        $kids.removeClass("active").css({left: "100%"});
        $active.css({left: 0});
      }, 400)

    });
    $(".carousel-controls .prev").on("click", function (event) {
      var $this = $(".carousel-container"), $kids = $this.children(':not(.main-road, .moving-car)'), $active, $previous;
      count--;
      if (count === -1) {
        count = ($kids.length - 1);
      }
      $active = $kids.eq(count);
      if (count === ($kids.length - 1)) {
        $previous = $kids.eq(0);
      } else {
        $previous = $kids.eq(count + 1);
      }
      $active.css({left: "-100%"});
      //START CAR
      carPos -= (100 / tot);
      moveCar(event);
      //END CAR
      setTimeout(function () {
        $previous.addClass("active").css({left: "100%"});
        $active.addClass("active").css({left: 0});
      }, 50)
      setTimeout(function () {
        $kids.removeClass("active").css({left: "100%"});
        $active.css({left: 0});
      }, 450)

    });
  }
});

module.exports = CarouselObject;
