import { debug } from '../../utilities/util-debug';
import { Enterprise } from '../../modules/Enterprise';
const debugScripts = Enterprise.global.settings.debugContentScripts;

const DriveAllianceObject = debug({
  name: 'DriveAllianceObject',
  isDebug: debugScripts,
  logger: {},
  init: function() {
    this.logger.log('fired');
    var message = $(".beta-message-holder").html();
    $("#beta-message").html(message).show();
//ECR-6902 - hide SM links on non-US
    (!$('body').hasClass('en_us')) ? $('ul.social-links.cf').remove() : null;
  }
});

module.exports = DriveAllianceObject;
