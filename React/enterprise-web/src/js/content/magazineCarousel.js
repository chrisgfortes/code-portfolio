import { Debugger } from '../utilities/util-debug';

export const magazineCarouselHandle = {
  init() {
    Debugger.use('content:init').log('16 magazineCarousel');
    let magazineCarouselContainer = require('./MagazineCarousel/index');
    let $magazineCarouselContainer = $('.slide-magazine');
    //DOCUMENT READY
    $(function () {
      if ($magazineCarouselContainer.length > 0) {
        let MagazineCarouselContainer = Object.create(magazineCarouselContainer);
        MagazineCarouselContainer.init();
      }
    });
  }
}
