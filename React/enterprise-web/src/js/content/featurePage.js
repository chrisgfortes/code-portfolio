import { Debugger } from '../utilities/util-debug';

export const featurePageHandle = {
  init() {
    Debugger.use('content:init').log('28 FeaturePage');
    let FeaturePage = require('./FeaturePage/index');
    let mapCanvas = document.querySelectorAll('.map-canvas');
    $(function(){
      if (mapCanvas.length) {
        FeaturePage.init();
      }
    })
  }
};
