const RedirectBandObject = {
  init: function() {
    $(".redirect-component").on("change", function () {
      var val = $(this).val();
      if (val.indexOf("PROMO:") === -1) {
        window.location.href = val;
      } else {
        val = val.substr(6, val.length);
        ReservationStateTree.set(["model", "coupon"], val);
      }
    });
  }
};

module.exports = RedirectBandObject;
