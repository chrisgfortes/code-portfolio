import { Debugger } from '../utilities/util-debug';

export const mobileCurrencySelectorHandle = {
  init() {
    Debugger.use('content:init').log('14 mobileCurrencySelector');
    let mobileCurrencySelector = require('./MobileCurrencySelector/index');
    let $mobileCurrencySelector = $('#mobileCurrencySelector');
    //DOCUMENT READY
    $(function () {
      if ($mobileCurrencySelector.length > 0) {
        let MobileCurrencySelector = Object.create(mobileCurrencySelector);
        MobileCurrencySelector.init();
      }
    });
  }
};
