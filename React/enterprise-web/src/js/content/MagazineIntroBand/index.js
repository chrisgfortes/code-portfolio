import { debug } from '../../utilities/util-debug';
import { Enterprise } from '../../modules/Enterprise';
const debugScripts = Enterprise.global.settings.debugContentScripts;

const MagazineIntroBandObject = debug({
  name: 'MagazineIntroBandObject',
  isDebug: debugScripts,
  logger: {},
  init: function() {
    this.logger.log('init() fired');
    $(".social-panel a").click(function (e) {
      e.preventDefault();
      var type = $(this).data('type'),
        sharePath = shareThis(type);

      if (sharePath)
        window.open(sharePath, '_blank');
    });

    var shareThis = function (type) {
      var currentPath = window.location.href,
        socialPath = "";

      switch (type) {
        case "facebook" :
          socialPath = "http://www.facebook.com/sharer.php?u=" + currentPath;
          break;
        case "twitter" :
          socialPath = "https://twitter.com/intent/tweet?text=" + currentPath;
          break;
        case "googleplus" :
          socialPath = "https://plus.google.com/share?url=" + currentPath;
          break;
        default :
          break;
      }

      return socialPath
    };
  }
});

module.exports = MagazineIntroBandObject;
