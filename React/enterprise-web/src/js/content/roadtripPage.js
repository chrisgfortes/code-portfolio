import { Debugger, debug } from '../utilities/util-debug';
import { Enterprise } from '../modules/Enterprise';
const debugScripts = Enterprise.global.settings.debugContentScripts;

export const roadTripPage = debug({
  name: 'RoadTripBand',
  isDebug: debugScripts,
  logger: {},
  init() {
    Debugger.use('content:init').log('30 RoadTripPage');
    let roadTrip = document.querySelector('body');
    let mapBand = document.querySelectorAll('.map-band');
    if (mapBand.length) {
      $(function(){
        if (roadTrip) {
          let classes = roadTrip.classList;
          for (let i = 0; i < classes.length; i++) {
            if (classes[i].indexOf('roadtrippage') > -1) {
              Debugger.use('RoadTripBand').log('remove map band attribute');
              $(mapBand).attr('style', '');
            }
          }
        } else {
          // Debugger.use('RoadTripBand').log('no road trip');
        }
      })
    } else {
      // Debugger.use('RoadTripBand').log('no map band');
    }
  }
});
