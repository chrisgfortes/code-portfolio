import { debug } from '../../utilities/util-debug';
import { Enterprise } from '../../modules/Enterprise';
const debugScripts = Enterprise.global.settings.debugContentScripts;

const AccordionObject = debug({
  name: 'AccordionObject',
  isDebug: debugScripts,
  logger: {},
  init: function () {
    this.logger.log('init()');
    const accordionHandler = function (e) {
      const activeRow = $(".accordion-row.active");
      const row = $(e.currentTarget).parent();

      if (row.hasClass('active')) {
        row.removeClass('active');
      } else {
        activeRow.removeClass('active');
        row.addClass('active');
      }
    };
    $(".accordion-row").on('click', '.accordion-title', accordionHandler);
  }
});

module.exports = AccordionObject;
