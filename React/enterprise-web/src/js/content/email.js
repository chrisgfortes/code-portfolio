import { Debugger } from '../utilities/util-debug';

export const emailHandle = {
  init() {
    Debugger.use('content:init').log('06 email');
    let emailHandle = require('./Email/index');
    let $emailHandle = document.getElementById('email-sign-up');
    let $confirmationHandle = document.getElementById('email-confirmation');
    //DOCUMENT READY
    $(function () {
      if ($emailHandle|| $confirmationHandle) {
        let EmailHandle = Object.create(emailHandle);
        EmailHandle.init($emailHandle, $confirmationHandle);
      }
    });
  }
}

