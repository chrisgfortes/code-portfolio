import { Debugger } from '../utilities/util-debug';

export const pursuitsHandle = {
  init() {
    Debugger.use('content:init').log('11 pursuits.js');
    const Pursuits = require('./Pursuits/index');
    $(document).ready(function () {
      Pursuits.init();
    });
  }
}
