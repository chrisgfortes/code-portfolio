import { Debugger } from '../utilities/util-debug';

export const driveAllianceHandle = {
  init() {
    Debugger.use('content:init').log('20 driveAlliance');
    console.log();
    let drivealliance = require('./DriveAlliance/index');
    let $drivealliance = $('.beta-message-holder');
    //DOCUMENT READY
    $(function () {
      if ($drivealliance.length > 0) {
        let Drivealliance = Object.create(drivealliance);
        Drivealliance.init();
      }
    });
  }
}

