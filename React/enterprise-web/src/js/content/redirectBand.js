import { Debugger } from '../utilities/util-debug';

export const redirectBandHandle = {
  init() {
    Debugger.use('content:init').log('22 redirectBand');
    let redirectBand = require('./RedirectBand/index');
    let $redirectBand = $('.business-intro-band');
    //DOCUMENT READY
    $(function () {
      if ($redirectBand.length > 0) {
        let RedirectBand = Object.create(redirectBand);
        RedirectBand.init($redirectBand);
      }
    });
  }
}
