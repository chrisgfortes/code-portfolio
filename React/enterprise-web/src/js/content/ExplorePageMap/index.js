import { Debugger, debug } from '../../utilities/util-debug';
import { Enterprise } from '../../modules/Enterprise';
const debugScripts = Enterprise.global.settings.debugContentScripts;

const ExplorePageMap = debug({
  name: 'ExplorePageMap',
  logger: {},
  isDebug: debugScripts,
  init(places) {
    this.logger.log('external script fired');
    $('.greenInfoBlock').on('click', function(e) {
      if (e.target!== 'a') {
        $(this).toggleClass('active');
      }
    });

    var locations = places;
    this.logger.log(locations);

    google.maps.Marker.prototype.setLabel = function(label){
      this.label = new google.maps.MarkerLabel({
        map: this.map,
        marker: this,
        text: label
      });
      this.label.bindTo('position', this, 'position');
    };

    let MarkerLabel = function(options) {
      this.setValues(options);
      this.span = document.createElement('span');
      this.span.className = 'map-marker-label';
    };

    MarkerLabel.prototype = $.extend(new google.maps.OverlayView(), {
      onAdd: function() {
        this.getPanes().overlayImage.appendChild(this.span);
        var self = this;
        this.listeners = [
          google.maps.event.addListener(this, 'position_changed', function() { self.draw();    })];
      },
      draw: function() {
        var text = String(this.get('text'));
        var position = this.getProjection().fromLatLngToDivPixel(this.get('position'));
        this.span.innerHTML = text;
        this.span.style.left = (position.x - (markerSize.x / 2)) - (text.length * 3) + 10 + 'px';
        this.span.style.top = (position.y - markerSize.y + 40) + 'px';
      }
    });

    function initialize() {
      Debugger.use('ExplorePageMap').log('initialize() map fired');
      locations.forEach(function(location, index){
        var map,
          directionsService = new google.maps.DirectionsService(),
          start = new google.maps.LatLng(location.latitudeStart, location.longitudeStart),
          end = new google.maps.LatLng(location.latitudeDestination, location.longitudeDestination),
          directionsDisplay = new google.maps.DirectionsRenderer({
            suppressInfoWindows: false,
            suppressMarkers: false,
            map: map
          });

        var zoomLevel = 3,
          mapCenter = new google.maps.LatLng(41.850033, -87.6500523),
          mapOptions = {
            zoom: zoomLevel,
            center: mapCenter,
            scrollwheel: false,
            disableDefaultUI: true
          };

        map = new google.maps.Map(document.getElementById('list-map-' + index), mapOptions);
        directionsDisplay.setMap(map);
        calcRoute(start, end, directionsDisplay, directionsService, map);
      });
    }

    function calcRoute(start, end, directionsDisplay, directionsService, map) {
      //var start = wayPointList.splice(0, 1),
      //  end = wayPointList.splice(wayPointList.length - 1, 1);

      var request = {
        origin: start,
        destination: end,
        travelMode: google.maps.TravelMode.DRIVING
      };

      directionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
          directionsDisplay.setDirections(response);
        }
      });
    }

    google.maps.event.addDomListener(window, 'load', initialize);


  }
});

module.exports = ExplorePageMap;
