import { Debugger, debug } from '../../utilities/util-debug';
import { Enterprise } from '../../modules/Enterprise';
const debugScripts = Enterprise.global.settings.debugContentScripts;

const CountryPage = debug({
  isDebug: debugScripts,
  name: 'CountryPage',
  logger: {},
  init(countryData) {
    this.logger.log('external script fired');
    let countryLat = countryData.lat;
    let countryLng = countryData.lng;
    let countryZoom = countryData.zoom;
    var map, locations;
    var local = $.ajax({
      url : window.location.href.substring(0,window.location.href.indexOf(".html")) +"/_jcr_content.mapdata.js",
      dataType : "json",
    })
      .done(function(data){
        $(".loading").removeClass('active');
        $("#map-canvas").addClass('active');
        map, locations = data;
        initialize();
      });
    if(local.readyState == 1 || local.readyState == 2){
      $('.loading').addClass('active');
    }

    function initialize() {
      Debugger.use('CountryPage').log('initialize() CountryPage map');
      $(".start-res-btn").click(function(e) {
        e.preventDefault();
        $('html, body').animate({
          scrollTop: $(".booking-widget").offset().top
        }, 1000);
      });
      var mobile = window.window.matchMedia( "(max-width: 760px)" );
      var myLatlng = new google.maps.LatLng(countryLat, countryLng),
        infoWindow = new google.maps.InfoWindow(),
        mapOptions = {
          zoom: countryZoom,
          center: myLatlng,
          draggable: !Modernizr.touch,
          scrollwheel: false,
          streetViewControl: false,
          mapTypeControl: false,
          animation: google.maps.Animation.DROP
        };
      var buildMap = function(opts) {
        map = new google.maps.Map(document.getElementById('map-canvas'), opts);

        // Removes links from focus order (ECR-9926)
        google.maps.event.addListener(map, "tilesloaded", function() {
          //console.log("removing links from focus order");
          Array.from(document.querySelectorAll('#map-canvas a')).forEach(function(item) {
            item.setAttribute('tabindex','-1');
          });
        });
        for(var i = 0; i < locations.length; i++) {
          var location = locations[i];

          var latLong = new google.maps.LatLng(location.latitude, location.longitude);

          var marker = new google.maps.Marker({
            position: latLong,
            map: map,
            title: location.locationNameTranslation,
            locationObj: {
              details: {},
              lat: location.latitude,
              key: location.peopleSoftId,
              locationName: location.locationNameTranslation || location.addressLines[0],
              locationType: "BRANCH",
              long: location.longitude,
              results: {
                airports: [],
                branches: [],
                cities: [],
                ports: [],
                trains: []
              }
            },
            icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAMAAAC67D+PAAAAM1BMVEUAAAAAZDkAZDkAZDkAZDkJm1sImVoAb0A1qHIgomYAjFJGrn0rpWwsmWcOnFwUkVoAckQwzSlYAAAABXRSTlMA+rlQjciNlyQAAABKSURBVAjXRY23EcAwDMQYzKi4/7Sm5MKocF/gAYAJkRiKx0Zrw57atnuE+2ag1afI7IsANaQIxVK5lFJ+mgRsekfjE0vVPLH/4gV8TwIX44T6egAAAABJRU5ErkJggg=='
          });
          google.maps.event.addListener(marker, 'click', (function(marker, infoWindow, content){
            return function() {
              infoWindow.setContent(content);
              infoWindow.open(map, marker);

              $(".if-start-res-btn").click(function(e) {

                e.preventDefault();
                ReservationStateTree.set(["view",  "externalBookingLocation"], marker.locationObj);

                $('html, body').animate({
                  scrollTop: $(".booking-widget").offset().top
                }, {
                  duration: 1000
                });
              });
            };
          })(marker, infoWindow, buildInfoWindow(location)));
        }
      };
      var buildInfoWindow = function(location) {
        var ifHeader = "<div style='min-width: 220px;'>" +
          "<div style='border-bottom: 1px solid black; height: 26px; margin-bottom: 10px;'>"+enterprise.i18nReservation.locations_0046+"</div>";

        var addressLine = location.addressLines[0];

        if(location.addressLines[1]) {
          addressLine += ', '+location.addressLines[1];
        }


        var ifBody = "<div>" +
          "<ul style='list-style: none; margin: 0;'>" +
          "<li style='margin: 0;'>" + addressLine + "</li>" +
          "<li style='margin: 0;'>" + location.city + ", " + location.state + " " + location.postalCode + "</li>" +
          "<li style='margin: 0;'>" + location.formattedPhone + "</li>" +
          "</ul>" +
          "</div>";

        var ifFooter = "<div>" +
          "<div style='padding-top: 20px; text-align: right; '>" +
          "<a class='btn' href='" + location.url + "' style='margin-bottom: 10px;'>"+enterprise.i18nReservation.resflowlocations_0022+"</a><br/>" +
          "<a class='btn if-start-res-btn' href='#book'>"+enterprise.i18nReservation.resflowreview_0001+"</a>" +
          "</div>" +
          "</div>" +
          "</div>";

        return ifHeader + ifBody + ifFooter;
      };
      buildMap(mapOptions);
    }

  }
});

module.exports = CountryPage;
