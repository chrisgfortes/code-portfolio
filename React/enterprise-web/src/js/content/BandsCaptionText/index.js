const BandsCaptionTextObject = {
  init: function() {
    let infoBlock = document.getElementsByClassName('gray-info-block');
    function captionBlock () {
      if (infoBlock) {
        Array.from(infoBlock).forEach(text => {
          if (text.className === 'gray-info-block text-overflow') {
          text.classList.remove('text-overflow');
        }
        if (text.offsetHeight < text.scrollHeight || text.offsetWidth < text.scrollWidth) {
          text.classList.add('text-overflow');
        }
      });
      }
    }
    captionBlock();
    $(window).on('resize', captionBlock);
  }
};

module.exports = BandsCaptionTextObject;
