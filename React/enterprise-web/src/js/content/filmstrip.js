import { Debugger } from '../utilities/util-debug';

/**
 * We use two files to separate definition from usage.
 * Actual class / module lives in ./Filmstrip/index
 * This just runs the document.ready and sets the
 *   enterprise.filmstrip boolean that was in
 *   filmstrip.html previously.
 */
export const filmstrip = {
  init() {
    Debugger.use('content:init').log('07 filmstrip');
    let filmstrip = require('./Filmstrip/index');
    let $filmstrips = $('.filmstripBand');
    //DOCUMENT READY (2)
    $(function () {
      if ($filmstrips.length > 0) {
        $filmstrips.each(function () {
          let FilmStrip = Object.create(filmstrip);
          FilmStrip.init($(this));
        });
      }
      $('.toggle-collapse').on('click', function () {
        $(this).toggleClass('collapsed');
        $(this).parent().next('.filmstrip-carousel').toggleClass('collapsed');
      });
    });
    if (!enterprise.filmstrip) {
      enterprise.filmstrip = true;
    }
    return enterprise.filmstrip;
  }
}
