import { debug } from '../../utilities/util-debug';
import { Enterprise } from '../../modules/Enterprise';
const debugScripts = Enterprise.global.settings.debugContentScripts;

const CtaBandObject = debug({
  name: 'CtaBandObject',
  isDebug: debugScripts,
  logger: {},
  init: function () {
    if ($('.ctaBand ul').children('li').length < 5) {
      this.logger.log('fired');
      $('.ctaBand ul li').width('32%');
      $(window).resize(function () {
        if ($(this).width() < 800)
          $('.ctaBand ul li').attr('style', '');
        else
          $('.ctaBand ul li').width('32%');
      });
    }
    $('.ctaBandToTop').on('click', function () {
      $(window).scrollTop(0);
    });
  }
});

module.exports = CtaBandObject;
