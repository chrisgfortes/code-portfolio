import { Debugger } from '../utilities/util-debug';

export const canZoomHandle = {
  init() {
    Debugger.use('content:init').log('25 canZoom');
    let $canZoom = $('.canZoom');
    //DOCUMENT READY
    $(function () {
      if ($canZoom.length > 0) {
        // console.log('canZoom.length', $canZoom.length, $canZoom);
        window.initZoomableImgs.init();
      }
    });
  }
}
