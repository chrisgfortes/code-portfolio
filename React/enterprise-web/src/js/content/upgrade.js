import { Debugger } from '../utilities/util-debug';

export const upgradeHandle = {
  init() {
    Debugger.use('content:init').log('08 upgrade');
    let upgrade = require('./Upgrade/index');
    let $upgrade = document.querySelector('body.browserupgradepage');
    //DOCUMENT READY (2)
    $(function () {
      if ($upgrade) {
        let Upgrade = Object.create(upgrade);
        Upgrade.init();
      }
    });
  }
}
