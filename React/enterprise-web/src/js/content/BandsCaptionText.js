import { Debugger } from '../utilities/util-debug';

export const bandsCaptionTextHandle = {
  init() {
    Debugger.use('content:init').log('10 BandsCaptionText');
    let bandsCaptionText = require('./BandsCaptionText/index');
    let $bandsCaptionText = $('.gray-info-block');
    //DOCUMENT READY
    $(function () {
      if ($bandsCaptionText.length > 0) {
        let BandsCaptionText = Object.create(bandsCaptionText);
        BandsCaptionText.init();
      }
    });
  }
}

