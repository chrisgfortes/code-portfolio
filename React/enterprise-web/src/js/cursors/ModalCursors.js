/**
 * @module ModalCursors
 * @description Cursors unique to Modals. Please also look in LEGAL CURSORS FOR MODALS. Combined with ReservationCursors.
 */
export default {
  cancelModifyModalOpen: ['view', 'modify', 'cancel', 'modal'],
  cancelReservationModal: ['view', 'cancel', 'modal'],
  cannotModifyModal: ['model', 'cannotModifyModal'],
  conflictAccountModalRoot: ['view', 'verification', 'conflictAccountModal'],
  conflictAccountModal: ['view', 'verification', 'conflictAccountModal', 'modal'],
  destinationPriceInfoModal: ['model', 'destinationPriceInfoModal'],
  editCardOrBIllingErrors: ['User', 'view', 'account', 'editModalErrors'],
  exclusionExtrasModal: ['view', 'extras', 'exclusionModal'],
  homeModal: ['view', 'home'],
  inflightModifyModal: ['view', 'inflightModify', 'modal'],
  learnPayNowModal: ['view', 'learnPayNowModal', 'modal'],
  lockedCIDModal: ['view', 'lockedCIDModal'],
  loginModal: ['view', 'loginWidget', 'modal'],
  logout: ['view', 'logout'],
  modifyModalOpen: ['view', 'modify', 'modal'],
  viewTermsModalParent: ['view', 'viewTermsModal'],
  notAvailableModal: ['view', 'carSelect', 'notAvailable', 'modal'],
  noVehicleAvailability: ['view', 'specialError', 'noVehicleAvailability', 'modal'],
  rateComparisonModal: ['view', 'carSelect', 'rateComparison', 'modal'],
  redemptionModal: ['view', 'carSelect', 'redemption', 'modal'],
  rentalDetailsModal: ['view', 'existingReservations', 'rentalDetailsModal'],
  requestModal: ['view', 'carSelect', 'requestModal'],
  // @todo - please, we need to rename this ModalModal thing... (check `CarCursors`'s `requestModal`')
  requestModalModal: ['view', 'carSelect', 'requestModal', 'modal'],
  rightPlaceModal: ['view', 'rightPlace', 'modal'],
  redirectModal: ['view', 'redirect', 'modal'],
  setEmailModal: ['User', 'createPassword', 'setEmailModal'],
  showAfterHoursModal: ['view', 'locationSelect', 'afterHours', 'modal'],
  showDatePickerModalOfType: ['view', 'locationSelect', 'showDatePickerModalOfType'],
  showExtraDetailsModal: ['view', 'extras', 'showExtraDetailsModal'],
  showExtrasProtectionModal: ['view', 'showExtrasProtectionsModal'],
  specialErrorChangePasswordModal: ['view', 'specialError', 'changePassword', 'modal'],
  showPrepayTerms: ['view', 'verification', 'showPrepayTerms'],
  showTimeoutModal: ['view', 'sessionTimeout', 'showModal'],
  taxesAndFeesModal: ['view', 'carSelect', 'taxesAndFees', 'modal'],
  vanModalControl: ['view', 'carSelect', 'vanModal', 'modal'],
  vehiclePriceModalControls: ['view', 'carSelect', 'vehiclePriceModal', 'modal'],
  receiptModal: ['view', 'receipt', 'modal']
}
