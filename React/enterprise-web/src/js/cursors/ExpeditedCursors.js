/**
 * @module ExpeditedCursors
 * @description Cursors unique to expedited states.
 * @todo We should consolidate error handling...
 */
export default {
  expedited: ['model', 'expedited'],
  expeditedCoupon: ['model', 'expeditedCoupon'],
  expediteEligibility: ['model', 'expediteEligibility'],
  externalBookingLocation: ['view', 'externalBookingLocation'],
  expeditedLicenseExpiryDate: ['model', 'expedited', 'licenseExpiryDate'],
  expeditedLicenseIssueDate: ['model', 'expedited', 'licenseIssueDate'],
  expeditedDateOfBirth: ['model', 'expedited', 'dateOfBirth']
}
