/**
 * @module UserCursors
 * @description Cursors having to do with User Data
 */
export default {


  user: ['User'],
  userProfile: ['User', 'profile'],
  userLoggedIn: ['User', 'loggedIn'],
  profile: ['User', 'profile'], // yes, redundant (@todo: update all references)

  basicProfile: ['User', 'profile', 'basicProfile'],
  basicCustomerDetails: ['User', 'profile', 'basicProfile', 'customerDetails'],
  basicProfileLoyalty: ['User', 'profile', 'basicProfile', 'loyaltyData'],
  driverInformation: ['User', 'driverInfo'],
  // Used for EC Unauth
  driverInfo: ['User', 'driverInfo'], // redundant but backwards compatible
  driverInfoIdentifier: ['User', 'driverInfo', 'individual_indentifier'],
  driverInfoLoyalty: ['User', 'driverInfo', 'loyalty_program_type'],
  driverInfoPaymentProfile: ['User', 'driverInfo', 'payment_profile', 'payment_methods'],
  licenseProfile: ['User', 'profile', 'licenseProfile'],
  paymentProfile: ['User', 'profile', 'paymentProfile'],
  profilePaymentMethods: ['User', 'profile', 'paymentProfile', 'payment_methods'],

  // Account Page
  account: ['User', 'view', 'account'],
  accountContact: ['User', 'view', 'account', 'contact'],
  accountDriver: ['User', 'view', 'account', 'driver'],
  accountPassword: ['User', 'view', 'account', 'password'],
  age: ['model', 'age'],
  ageLabel: ['model', 'ageLabel'],
  ageRange: ['model', 'ageRange'],

  associateAccount: ['view', 'verification', 'associateAccount'],

  // Existing Resevations
  currentTripsLoadingClass: ['view', 'existingReservations', 'myTrips', 'loading', 'CURRENT'],
  existingReservations: ['view', 'existingReservations'],
  myTrips: ['view', 'existingReservations', 'myTrips'],
  upcomingTrips: ['view', 'existingReservations', 'myTrips', 'UPCOMING'],
  currentTrips: ['view', 'existingReservations', 'myTrips', 'CURRENT'],
  pastTrips: ['view', 'existingReservations', 'myTrips', 'PAST'],
  pastTripsLoadingClass: ['view', 'existingReservations', 'myTrips', 'loading', 'PAST'],
  existingReservationsReservations: ['view', 'existingReservations', 'reservations'],
  existingReservationsSearch: ['view', 'existingReservations', 'search'],
  existingReservationsLoading: ['view', 'existingReservations', 'loading'],
  existingReservationsErrors: ['view', 'existingReservations', 'errors'],
  myTripsMore: ['view', 'existingReservations', 'myTrips', 'more'],
  myTripsStart: ['view', 'existingReservations', 'myTrips', 'startRecord'],
  myTripsLoadingType: ['view', 'existingReservations', 'myTrips', 'loading'],
  reservationSearchVisible: ['view', 'existingReservations', 'search', 'visibility'],
  upcomingTripsLoadingClass: ['view', 'existingReservations', 'myTrips', 'loading', 'UPCOMING'],

  // Email
  email: ['User', 'personal', 'email'],
  employeeNumber: ['User', 'employeeNumber'],

  // Enroll Page
  enroll: ['User', 'view', 'enroll'],
  enrollErrors: ['User', 'view', 'enroll', 'errors'],
  enrollProfile: ['User', 'view', 'enroll', 'profile'], // ECR-14250

  // loyaltyBook: ['model', 'loyaltyBook'],
  // loyaltyBrand: ['model', 'loyaltyBook', 'loyalty_brand'],
  // loyaltyLastName: ['model', 'loyaltyBook', 'last_name'],
  // memberShipId: ['model', 'loyaltyBook', 'membership_id'],
  loyaltyBook: ['User', 'loyaltyBook'],
  loyaltyBrand: ['User', 'loyaltyBook', 'loyalty_brand'],
  loyaltyLastName: ['User', 'loyaltyBook', 'last_name'],
  memberShipId: ['User', 'loyaltyBook', 'membership_id'],

  // Receipt
  receipt: ['view', 'receipt'],

  // Forgot Your Password
  resetPassword: ['view', 'resetPassword', 'password'],

  // Create Password
  createPassword: ['User', 'createPassword'],
  setPassword: ['User', 'createPassword', 'setPassword'],

  personal: ['User', 'personal'],

  paymentMethods: ['User', 'view', 'payment'],
  // Login
  login: ['User', 'login'],

  // Login Widget
  loginForgot: ['view', 'loginWidget', 'forgot'],
  loginWidget: ['view', 'loginWidget'],

  resetPasswordErrors: ['view', 'resetPassword', 'password', 'errors']

};

