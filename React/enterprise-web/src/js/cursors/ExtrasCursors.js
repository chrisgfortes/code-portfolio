/**
 * @module ExtrasCursors
 * @description Cursors unique to Extras.  Combined with ReservationCursors
 */
export default {
  availableUpgrades: ['view', 'extras', 'availableUpgrades'],
  blockSubmit: ['view', 'extras', 'blockSubmit'],
  equipmentExtras: ['view', 'extras', 'equipment'],
  extrasProtections: ['model', 'extrasProtections'],
  extrasView: ['view', 'extras'],
  fuelExtras: ['view', 'extras', 'fuel'],
  highlightedExtra: ['view', 'extras', 'highlightedExtra'],
  includedExtras: ['view', 'extras', 'includedExtras'],
  insuranceExtras: ['view', 'extras', 'insurance'],
  mandatoryExtras: ['view', 'extras', 'mandatoryExtras'],
  previousCarClass: ['view', 'extras', 'previousCarClass'],
  requiredExtras: ['model', 'extrasProtections', 'protectionsRequiredAtCounter'],
  updateQueue: ['view', 'extras', 'updateQueue'],
  vehicleUpgraded: ['view', 'extras', 'upgraded']
}
