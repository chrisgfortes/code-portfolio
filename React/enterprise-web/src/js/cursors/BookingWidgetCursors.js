/**
 * @module BookingWidgetCursors
 * @description Cursors unique to the BookingWidget. Combined with ReservationCursors
 */
export default {
  bookingWidget: ['view', 'bookingWidget'],
  bookingWidgetExpanded: ['view', 'bookingWidgetExpanded'],
  bookingWidgetLoading: ['view', 'bookingWidget', 'loading'],
  pin: ['model', 'pin']
}
