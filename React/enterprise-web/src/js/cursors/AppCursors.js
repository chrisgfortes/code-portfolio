/**
 * AppCursors
 * @description General application-wide Cursors. Combined with ReservationCursors
 */
export default {
  componentToRender: ['view', 'componentToRender'], // this should be renamed componentRendered since it is past tense. see comment in RservationActions
  bookDataReady: ['view', 'dateTime', 'dataReady'],
  carSelectDataReady: ['view', 'carSelect', 'dataReady'],
  countries: ['User', 'view', 'account', 'countries'],
  currentHash: ['view', 'currentHash'],
  currentView: ['view', 'currentView'],
  currentViewBreakpoint: ['view', 'currentViewBreakpoint'],
  dateFormat: ['view', 'dateFormat'],
  dateTimeDataReady: ['view', 'dateTime', 'dataReady'],
  debug: ['debug'],
  deepLink: ['model', 'deepLink'],
  deepLinkReservation: ['model', 'deepLinkReservation'],
  deepLinkView: ['view', 'deepLink'],
  extrasDataReady: ['view', 'extras', 'dataReady'],
  initialDataReady: ['view', 'initialDataReady'],
  loadingClass: ['view', 'loadingClass'],
  locationDataReady: ['view', 'locationSelect', 'dataReady'],
  queryStringData: ['model', 'queryStringData'],

  redirectPreferredLang: ['view', 'redirect', 'preferredLang'],
  redirectType: ['view', 'redirect', 'type'],
  redirectCountry: ['view', 'redirect', 'country'],
  redirectDestinationLang: ['view', 'redirect', 'destinationLang'],

  redirect: ['view', 'redirect'],
  redirectToLegacy: ['view', 'redirectToLegacy'],
  reservationSteps: ['view', 'reservationSteps'],
  reservationCarStep: ['view', 'reservationSteps', 'carClass'],
  reservationStatusFlags: ['ReservationStatusFlags'],
  rightPlaceRefer: ['view', 'rightPlace', 'refer'],
  rightPlaceType: ['view', 'rightPlace', 'type'],
  sessionTimeout: ['view', 'sessionTimeout'],
  sessionTimeoutDuration: ['view', 'sessionTimeout', 'duration'],
  showPaymentList: ['model', 'showPaymentList'],
  showResFlowLogin: ['view', 'loginWidget', 'showResFlowLogin'],
  timeFormat: ['view', 'timeFormat'],
  verificationDataReady: ['view', 'verification', 'dataReady']
}
