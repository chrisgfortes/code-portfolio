/**
 * @module LocationCursors
 * @description Cursors unique to Location Search and Location Data.  Combined with ReservationCursors
 */
export default {
  afterHourMessage: ['model', 'dropoff', 'afterHour', 'message'],
  dropoffAfterHour: ['model', 'dropoff', 'afterHour'],
  dropoffClosedDates: ['model', 'dropoff', 'date', 'closedDates'],
  dropoffClosedTime: ['model', 'dropoff', 'time', 'closed'],
  dropoffDate: ['model', 'dropoff', 'date', 'momentDate'],
  dropoffLocation: ['model', 'dropoff', 'location', 'locationId'],
  // dropoffLocationID: ['model', 'dropoff', 'location', 'locationID'],
  dropoffLocationCity: ['model', 'dropoff', 'location', 'city'],
  dropoffLocationDetails: ['model', 'dropoff', 'location', 'details'],
  dropoffLocationHours: ['model', 'dropoff', 'locationHours'],
  dropoffLocationObj: ['model', 'dropoff', 'location'],
  dropoffMapModel: ['model', 'dropoff', 'map'],
  dropoffModel: ['model', 'dropoff'],
  dropoffModelDate: ['model', 'dropoff', 'date'],
  dropoffPolicies: ['model', 'dropoff', 'policies'],
  dropoffPreviousDate: ['model', 'dropoff', 'date', 'previousDate'],
  dropoffPreviousTime: ['model', 'dropoff', 'time', 'previousTime'],
  dropoffTime: ['model', 'dropoff', 'time'],
  dropoffTimeValue: ['model', 'dropoff', 'time', 'value'],

  // dropoffDecoupled: ['view', 'decoupledComponents', 'dropoff'], // no longer used?
  // dropoffDecoupledViewDate: ['view', 'decoupledComponents', 'dropoff', 'viewDate'], // no longer used?
  dropoffView: ['view', 'dropoff'],
  dropoffViewDate: ['view', 'dropoff', 'date'],
  dropoffTempDate: ['view', 'dropoff', 'tempDate'],
  dropoffTimeInitial: ['view', 'dropoff', 'time', 'initial'],
  dropoffInvalidTime: ['view', 'locationSelect', 'dropoff', 'invalidTime'],
  dropoffMap: ['view', 'locationSelect', 'dropoff', 'map'],

  // Decoupled Components
  dropoffHours: ['view', 'confirmation', 'dropoffHours'],

  // both highlight are used to track the pin and location highlights
  highlightLocation: ['view', 'locationSelect', 'highlightLocation'],
  highlightPinLocation: ['view', 'locationSelect', 'highlightPinLocation'],

  dropoffInvalidDate: ['view', 'locationSelect', 'dropoff', 'invalidDate'],
  dropoffLocationSelect: ['view', 'locationSelect', 'dropoff'],
  dropoffTarget: ['view', 'locationSelect', 'dropoff', 'map', 'target'],
  dropoffTargetNortheast: ['view', 'locationSelect', 'dropoff', 'map', 'target', 'northeast'],
  dropoffTargetSouthwest: ['view', 'locationSelect', 'dropoff', 'map', 'target', 'southwest'],
  dropoffValidHours: ['view', 'locationSelect', 'dropoff', 'validHours'],
  locationDisplayResults: ['view', 'locationSelect', 'displayResults'],
  locationFilters: ['view', 'locationSelect', 'filters'],
  locationOpenForMyTimesFilter: ['view', 'locationSelect', 'filters', 'openForMyTimes'],
  locationSearchType: ['view', 'locationSelect', 'showType'],
  locationSelect: ['view', 'locationSelect'],

  pickupInvalidDate: ['view', 'locationSelect', 'pickup', 'invalidDate'],
  pickupInvalidTime: ['view', 'locationSelect', 'pickup', 'invalidTime'],
  pickupLocationSelect: ['view', 'locationSelect', 'pickup'],

  pickupMap: ['view', 'locationSelect', 'pickup', 'map'],
  pickupTarget: ['view', 'locationSelect', 'pickup', 'map', 'target'],
  pickupTargetNortheast: ['view', 'locationSelect', 'pickup', 'map', 'target', 'northeast'],
  pickupTargetSouthwest: ['view', 'locationSelect', 'pickup', 'map', 'target', 'southwest'],
  pickupValidHours: ['view', 'locationSelect', 'pickup', 'validHours'],

  // pickup view
  locationView: ['view', 'locationView'],

  costCenter: ['model', 'pickup', 'location', 'costCenter'],

  modelNoResults: ['model', 'pickup', 'noResults'],
  pickupAfterHour: ['model', 'pickup', 'afterHour'],
  pickupClosedDates: ['model', 'pickup', 'date', 'closedDates'],
  pickupClosedTime: ['model', 'pickup', 'time', 'closed'],
  pickupDate: ['model', 'pickup', 'date', 'momentDate'],
  // pickupDecoupled: ['view', 'decoupledComponents', 'pickup'], // no longer used?
  // pickupDecoupledViewDate: ['view', 'decoupledComponents', 'pickup', 'viewDate'], // no longer used?
  pickupLocation: ['model', 'pickup', 'location', 'locationId'],
  // pickupLocationID: ['model', 'pickup', 'location', 'locationID'],
  pickupLocationCity: ['model', 'pickup', 'location', 'city'],
  pickupLocationDetails: ['model', 'pickup', 'location', 'details'],
  pickupLocationHours: ['model', 'pickup', 'locationHours'],
  pickupLocationObj: ['model', 'pickup', 'location'],
  pickupMapModel: ['model', 'pickup', 'map'],
  pickupModel: ['model', 'pickup'],
  pickupModelDate: ['model', 'pickup', 'date'],
  pickupPolicies: ['model', 'pickup', 'policies'],
  pickupPreviousDate: ['model', 'pickup', 'date', 'previousDate'],
  pickupPreviousTime: ['model', 'pickup', 'time', 'previousTime'],
  pickupTempDate: ['view', 'pickup', 'tempDate'],
  pickupTime: ['model', 'pickup', 'time'],
  pickupTimeInitial: ['view', 'pickup', 'time', 'initial'],
  pickupTimeValue: ['model', 'pickup', 'time', 'value'],
  pickupView: ['view', 'pickup'],
  pickupViewDate: ['view', 'pickup', 'date'],

  pickupHours: ['view', 'confirmation', 'pickupHours'],

  // Map
  map: ['model', 'map'],

  // pickup location
  sameLocation: ['model', 'sameLocation'],

  //session location data
  sessionPickupTime: ['model', 'reservationSession', 'pickup_time'],
  sessionDropoffTime: ['model', 'reservationSession', 'return_time'],
  sessionPickupLocation: ['model', 'reservationSession', 'pickup_location'],
  sessionDropoffLocation: ['model', 'reservationSession', 'return_location'],
  sessionPickupLocationWithDetail: ['model', 'reservationSession', 'pickupLocationWithDetail']
}
