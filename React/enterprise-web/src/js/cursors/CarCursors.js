/**
 * @module CarCursors
 * @description Cursors Unique to Car Select. Combined with ReservationCursors
 */
export default {
  availablePayTypes: ['model', 'carSelect', 'availablePayTypes'],
  availableVehicles: ['model', 'carSelect', 'availableVehicles'],
  carFilters: ['model', 'carSelect', 'filters'],
  carPayType: ['model', 'carSelect', 'payType'],
  carRateCompare: ['view', 'carSelect', 'carRateCompare'],
  // @todo - This `carSelect` cursor was only being used by `ErrorActions.setErrorsForComponent`.
  //         Check if this is still the case and remove it if it's not
  carSelect: ['view', 'carSelect'],
  detailsCar: ['view', 'carSelect', 'detailsCar'],
  filterBandState: ['model', 'carSelect', 'filterBandState'],
  filteredVehicles: ['model', 'carSelect', 'filteredVehicles'],
  isNAPrepayEnabled: ['model', 'carSelect', 'isNAPrepayEnabled'],
  modelCarSelect: ['model', 'carSelect'],
  notAvailable: ['view', 'carSelect', 'notAvailable'],
  preSelectedCarClass: ['model', 'carSelect', 'preSelectedCarClass'],
  preSelectedMeta: ['model', 'carSelect', 'preSelectedCarMetaClass'],
  preSelectedVehicle: ['view', 'carSelect', 'preSelectedVehicle'],
  redemption: ['view', 'carSelect', 'redemption'],
  redemptionLoading: ['view', 'carSelect', 'redemption', 'loading'],
  redemptionDays: ['view', 'carSelect', 'redemption', 'days'],
  redemptionPreviousDays: ['view', 'carSelect', 'redemption', 'previousDays'],
  redemptionPayLaterPrice: ['view', 'carSelect', 'redemption', 'payLaterPrice'],
  requestModal: ['view', 'carSelect', 'requestModal'],
  requestModalOrigin: ['view', 'carSelect', 'requestModal', 'origin'],
  requestModalConfirm: ['view', 'carSelect', 'requestModal', 'confirm'],
  selectedCar: ['model', 'carSelect', 'selectedCar'],
  selectedCarCode: ['model', 'carSelect', 'selectedCarCode'],
  selectedCarFilters: ['model', 'carSelect', 'selectedFilters'],
  transmissionBandState: ['model', 'carSelect', 'transmissionBandState'],
  vanModal: ['view', 'carSelect', 'vanModal'],
  vanModalDescription: ['view', 'carSelect', 'vanModal', 'description'],
  vanModalConfirm: ['view', 'carSelect', 'vanModal', 'confirm'],
  vehiclePriceModal: ['view', 'carSelect', 'vehiclePriceModal'],
  viewCarClassDetailsStatus: ['view', 'carSelect', 'selectedCarDetails', 'status'],
  viewCarSelect: ['view', 'carSelect'],
  viewSelectedCar: ['view', 'carSelect', 'selectedCar'],
  viewSelectedCarDetails: ['view', 'carSelect', 'selectedCarDetails']
}
