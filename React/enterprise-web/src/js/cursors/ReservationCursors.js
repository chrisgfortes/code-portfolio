/**
 * @module ReservationCursors
 * @description Used to consolidate selection paths for the ReservationStateTree
 * If you get an "improper cursor mapping" error in Baobab your cursors and state tree are out of sync.
 */
import AppCursors from './AppCursors';
import BookingWidgetCursors from './BookingWidgetCursors';
import CarCursors from './CarCursors';
import CorporateCursors from './CorporateCursors';
import ErrorsCursors from './ErrorsCursors';
import ExtrasCursors from './ExtrasCursors';
import LegalCursors from './LegalCursors';
import LocationCursors from './LocationCursors';
import ModalCursors from './ModalCursors';
import ModifyCursors from './ModifyCursors';
import PricingCursors from './PricingCursors';
import SessionCursors from './SessionCursors';
import UserCursors from './UserCursors';
import ExpeditedCursors from './ExpeditedCursors';

let ReservationCursors = {
  model: ['model'], // seriously considering removing these
  view: ['view']    // seriously considering removing these
};

let cursors = [
  AppCursors,
  BookingWidgetCursors,
  CarCursors,
  CorporateCursors,
  ErrorsCursors,
  ExtrasCursors,
  LegalCursors,
  LocationCursors,
  ModalCursors,
  ModifyCursors,
  PricingCursors,
  SessionCursors,
  UserCursors,
  ExpeditedCursors
];

Object.assign(ReservationCursors, ...cursors);
module.exports = ReservationCursors;
