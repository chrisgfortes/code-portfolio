/**
 * @module PricingCursors
 * @description Cursors related to pricing, cost, charges, etc. Combined with ReservationCursors.
 */
export default {
  cancellationDetails: ['model', 'cancellationDetails'],
  viewPromo: ['view', 'promo'],
  codeApplicable: ['view', 'promo', 'applicable'],
  confirmation: ['view', 'confirmation'],
  paymentProcessor: ['model', 'paymentProcessor'],
  paymentReferenceID: ['model', 'paymentReferenceID'],
  registerCardSuccess: ['view', 'verification', 'registerCardSuccess'],
  showTotals: ['view', 'reservationSteps', 'showTotals'],
  threeDS: ['view', 'verification', 'threeDS'],
  verification: ['view', 'verification'],
  verificationEnrollValidity: ['view', 'verification', 'enrollValidity']
}
