/**
 * @module LegalCursors
 * @description Cursors unique to Legal issues, policies, terms and conditions, etc.  Combined with ReservationCursors
 */
export default {
  additional: ['view', 'keyRentalFactsModal', 'policies', 'additional'],

  // Age Policy Modal
  agePolicyModal: ['view', 'agePolicyModal', 'modal'],

  disputes: ['view', 'keyRentalFactsModal', 'disputes'],
  equipment: ['view', 'keyRentalFactsModal', 'policies', 'equipment'],
  keyFactsBrand: ['view', 'keyRentalFactsModal', 'brand'],
  keyFactsDetail: ['view', 'keyRentalFactsModal', 'detail'],
  keyFactsModal: ['view', 'keyRentalFactsModal'],
  keyFactsPolicies: ['view', 'keyRentalFactsModal', 'policies'],
  minimum_requirements: ['view', 'keyRentalFactsModal', 'policies', 'minimumRequirements'],
  protections: ['view', 'keyRentalFactsModal', 'policies', 'protections'],

  policyModal: ['view', 'verification', 'policyModal'],
  privacyPolicy: ['view', 'verification', 'privacyPolicy'],

  // Key Facts
  keyFacts: ['view', 'verification', 'keyFacts'],

  rentalTerms: ['model', 'rentalTerms'],
  rentalTermsText: ['model', 'rentalTermsText'],

  viewTermsModal: ['view', 'viewTermsModal', 'modal'],

  DNCModal: ['view', 'verification', 'DNCModal', 'modal'],
  dnr: ['view', 'specialError', 'dnr', 'modal']
}
