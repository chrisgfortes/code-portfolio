/**
 * @module ErrorCursors
 * @description Cursors unique to error states.
 * @todo We should consolidate error handling...
 */
export default {
  bookingWidgetErrors: ['view', 'bookingWidget', 'errors'],
  carSelectErrors: ['view', 'carSelect', 'errors'],
  changePassword: ['view', 'specialError', 'changePassword'],
  changePasswordErrors: ['view', 'specialError', 'changePassword', 'errors'],
  confirmationErrors: ['view', 'confirmation', 'errors'],
  createPasswordErrors: ['User', 'createPassword', 'errors'],
  employeeNumberError: ['view', 'employeeNumberField', 'errors'],
  enrollErrors: ['view', 'enroll', 'errors'],
  extrasError: ['view', 'extras', 'errors'],
  fieldErrors: ['view', 'errors', 'field'],
  formErrors: ['view', 'errors', 'form'],
  geolocationError: ['view', 'geolocationError'],
  globalErrors: ['view', 'errors', 'global'],
  invalidDate: ['view', 'invalidDate'],
  locationSelectErrors: ['view', 'locationSelect', 'errors'],
  loginWidgetErrors: ['view', 'loginWidget', 'errors'],
  payErrors: ['User', 'view', 'payment', 'errors'],
  verificationErrors: ['view', 'verification', 'errors'],
  viewSpecialError: ['view', 'specialError'],
  accountContactErrors: ['User', 'view', 'account', 'contact', 'errors'],
  accountPasswordErrors: ['User', 'view', 'account', 'password', 'errors'],
  accountDriverErrors: ['User', 'view', 'account', 'driver', 'errors']
}
