/**
 * @module ModifyCursors
 * @description Cursors unique to Modify / Cancel etc.  Combined with ReservationCursors
 */
export default {
  blockModifyDropoffLocation: ['model', 'blockModifyDropoffLocation'],
  blockModifyPickupLocation: ['model', 'blockModifyPickupLocation'],
  callbackAfterAuthenticate: ['view', 'modify', 'callbackAfterAuthenticate'],

  // will a modify do a cancel-rebook?
  cancelRebook: ['view', 'modify', 'cancel_rebook'],
  collectNewModifyPaymentCard: ['model', 'collectNewModifyPaymentCard'],
  inflightModify: ['view', 'inflightModify'],
  inflightModifyIsInFlight: ['view', 'inflightModify', 'isInflight'],

  // is a MODIFY IN PROGRESS?
  modify: ['view', 'modify', 'rebookCancel'],
  // Modify
  modifyPayment: ['User', 'view', 'account', 'modifyPayment'],
  viewModify: ['view', 'modify'],
  viewModifyCancel: ['view', 'modify', 'cancel'],
  viewCancel: ['view', 'cancel']

}
