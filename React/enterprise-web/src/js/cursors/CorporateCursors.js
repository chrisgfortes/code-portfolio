/**
 * @module CorporateCursors
 * @description Cursors unique to Corporate features.  Combined with ReservationCursors.
 */
export default {
  additionalInfo: ['model', 'additionalInfo'],
  collection: ['model', 'collection'],
  commitRequestData: ['model', 'commitRequestData'],

  // ECR-14251
  contractAdditionalInfo: ['Corporate', 'contractDetails', 'additional_information'],
  contractDetails: ['Corporate', 'contractDetails'], // watch out, this cursor might not be set even though there was a ContractDetails on the Session response
  // ECR-14251
  // contractDetailsTEMP: ['model', 'contractDetails'],
  // SEE THE REDUNDANT VALUES BELOW?????
  contractName: ['Corporate', 'contractDetails', 'contract_name'],
  contractType: ['Corporate', 'contractDetails', 'contract_type'],

  // ECR-14251 WHAT IS GOING ON HERE ?????
  // sessionContractDetails: ['model', 'reservationSession', 'contract_details'], // Need this, cause the contractDetails is not getting the Session response ContractDetails
  // sessionContractName: ['model', 'reservationSession', 'contract_details', 'contract_name'], // Same situation of `sessionContractDetails` above :(
  // sessionContractType: ['model', 'reservationSession', 'contract_details', 'contract_type'], // Same situation of `sessionContractDetails` above :(

  // hackity hack hack to make sure things do not break in the near term
  sessionContractDetails: ['Corporate', 'contractDetails'], // Need this, cause the contractDetails is not getting the Session response ContractDetails
  sessionContractName: ['Corporate', 'contractDetails', 'contract_name'], // Same situation of `sessionContractDetails` above :(
  sessionContractType: ['Corporate', 'contractDetails', 'contract_type'], // Same situation of `sessionContractDetails` above :(

  // ECR-14251 And again ... and this path only exists when we load a reservationSession that does not have a contract_details and we call the Contract Details endpoint
  // contractDetails: ['model', 'reservationSession', 'contractDetails'],

  corporate: ['view', 'corporate'],
  coupon: ['model', 'coupon'],
  couponLabel: ['model', 'couponLabel'],
  delivery: ['model', 'delivery'],
  fedexRequisitionNumber: ['model', 'fedexRequisitionNumber'],
  fedexSequence: ['model', 'fedexSequence'],
  isDoNotMarketCID: ['model', 'isDoNotMarketCID'],
  messages: ['model', 'messages'],
  prepopulatedCoupon: ['model', 'prepopulatedCoupon']
}
