import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import Validator from '../../utilities/util-validator';
import { GLOBAL } from '../../constants';

const DateSelector = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  getInitialState () {
    return {
      month: null,
      monthMasked: null,
      monthChanged: null,
      day: null,
      year: null,
      yearMasked: null,
      yearChanged: null
    };
  },
  componentWillMount () {
    let value = ReservationStateTree.get(this.props.cursor);

    // ECR-12417 - @TODO Remove/Replace this regex and (if) statement
    // All dates come back in YYYY-MM-DD format from GMA
    // ToDo :: Adding temporary new bullet Masking regex for now
    if (enterprise.settings.regularex.maskedDate.test(value) || GLOBAL.REGEX_MASK_CHAR.test(value)) {
      let m = value.substring(5, 7);
      let y = value.substring(0, 4);
      this.setState({
        year: y,
        yearMasked: y,
        yearChanged: false,
        month: m,
        monthMasked: m,
        monthChanged: false,
        day: value.substring(8, 10)
      });
    }
    this.validator = new Validator(this, this.fieldMap);
  },
  isNumberOrNumberMask (field, len) {
    let retVal = false;
    let val = this.state[field];
    let numberTest = new RegExp('\\d{' + len + '}');
    let starTest = new RegExp('\\*{' + len + '}');
    let newMaskTest = new RegExp('\\u2022{' + len + '}');
    let numberResult = numberTest.test(val);
    let starResult = starTest.test(val);
    let newMaskResult = newMaskTest.test(val);
    if (numberResult || starResult || newMaskResult) {
      retVal = true;
    }
    return retVal;
  },
  fieldMap () {
    return {
      refs: {
        year: this.year,
        month: this.month,
        day: this.day
      },
      value: {
        year: this.state.year,
        month: this.state.month,
        day: this.state.day
      },
      schema: {
        year: () => {
          return this.isNumberOrNumberMask('year', 4);
        },
        month: () => {
          return this.isNumberOrNumberMask('month', 2);
        },
        day: () => {
          return this.isNumberOrNumberMask('day', 2);
        }
      }
    };
  },
  _setDate (attribute, value) {
    let overall;

    this.setState({
      [attribute]: value
    }, ()=>{
      overall = [
        (attribute === 'year' ? value : this.state.year),
        (attribute === 'month' ? value : this.state.month),
        (attribute === 'day' ? value : this.state.day)
      ]
        .join('-')
        .replace(' ', '');

      if (this.props.cursor) {
        ReservationStateTree.select(this.props.cursor).set(overall);
      }

      if (this.props.onChange) {
        this.props.onChange(overall);
      }

      this.validator.validate(attribute, value);
    });

  },
  _onDayChange (event) {
    this._setDate('day', event.target.value);
    event.target.setAttribute('aria-invalid', 'false');
  },
  _onMonthChange (event) {
    this.setState({
      monthChanged: true
    });
    this._setDate('month', event.target.value);
    event.target.setAttribute('aria-invalid', 'false');
  },
  _onYearChange (event) {
    this.setState({
      yearChanged: true
    });
    this._setDate('year', event.target.value);
    event.target.setAttribute('aria-invalid', 'false');
  },
  _onFocus (field, event) {
    if (enterprise.utilities.isMaskedField(event.target.value)) {
      event.target.value = '';
    }
  },
  revertField (field, event) {
    if (this.state[field + 'Changed'] !== true) {
      if (this.state[field + 'Masked']) {
        event.target.value = this.state[field + 'Masked'];
      }
    }
  },
  _onBlur (field, event) {
    this.revertField(field, event);
    this._setDate('year', event.target.value.substring(0, 4));
  },
  _fillZero (field, event) {
    let value = event.target.value;

    this.revertField(field, event);

    if (value.length === 1 && !isNaN(parseInt(value, 10))) {
      event.target.value = '0' + value;

      if (field === 'day') {
        this._onDayChange(event);
      }
      if (field === 'month') {
        this._onMonthChange(event);
      }
    }
  },
  render () {
    // The following format manipulation takes into consideration the several
    // languages provided by moment.js
    // Separators vary in position and from '.', '/', '-', '. ' to chinese characters
    // Days and Months vary: 'DD' -> 'D' and 'MMM' -> 'MM' -> 'M'
    const {disabled} = this.props;
    const format = moment.localeData()._longDateFormat.L;
    const selectors = {
      M: (
        <input
          type="tel"
          key="month"
          name="month"
          maxLength="2"
          aria-required="true"
          ref={c => this.month = c}
          className={'month-selector' + (this.props.error ? ' invalid' : '')}
          aria-label={this.props.label + '-' + i18n('eplusenrollment_0025')}
          onChange={this._onMonthChange}
          onBlur={this._fillZero.bind(this, 'month')}
          onFocus={this._onFocus.bind(this, 'month')}
          placeholder={i18n('eplusenrollment_0026')}
          value={this.state.month}
          aria-invalid={this.props.error}
          disabled={!!disabled}
        />
      ),
      D: (
        <input
          type="tel"
          key="day"
          name="day"
          maxLength="2"
          aria-required="true"
          ref={c => this.day =c}
          className={'day-selector' + (this.props.error ? ' invalid' : '')}
          aria-label={this.props.label + '-' + i18n('eplusenrollment_0027')}
          onChange={this._onDayChange}
          onBlur={this._fillZero.bind(this, 'day')}
          placeholder={i18n('eplusenrollment_0028')}
          onFocus={this._onFocus.bind(this, 'day')}
          value={this.state.day}
          aria-invalid={this.props.error}
          disabled={!!disabled}
        />
      ),
      Y: (
        <input
          type="tel"
          key="year"
          name="year"
          maxLength="4"
          aria-required="true"
          ref={c => this.year =c}
          className={'year-selector' + (this.props.error ? ' invalid' : '')}
          aria-label={this.props.label + '-' + i18n('eplusenrollment_0029')}
          onChange={this._onYearChange}
          onBlur={this._onBlur.bind(this, 'year')}
          onFocus={this._onFocus.bind(this, 'year')}
          placeholder={this.props.masked ? '••••' : i18n('eplusenrollment_0030')}
          value={this.state.year}
          aria-invalid={this.props.error}
          disabled={!!disabled}
        />
      )
    };

    const interpolatedSelectors = (function () {
      let selectorsArr = Object.keys(selectors);
      let parsedFormat = format;

      //start replacing selectors for markers
      selectorsArr.forEach((selector) => {
        parsedFormat = parsedFormat
          .split(new RegExp(selector + '+', 'i'))
          .map((str, i) => {
            return (i > 0 ? '@@@' + selector + '@@@' : '') + str;
          }).join('');
      });
      //now we have something like "@@@D@@@/@@@M@@@/@@@Y@@@"
      //which splits to [undefined, "D", "/", "M", "/", "Y", undefined]

      //and then we replace D,M,Y for the respective input element
      //keeping the original separators in place
      return parsedFormat
        .split('@@@')
        .map((str, index) => {
          if (selectors[str]) {
            return selectors[str];
          } else if (str.length) {
            return <span key={index} className="separator">{str}</span>;
          }
        });
    }());

    return (
      <div className="date-selector">
        {interpolatedSelectors}
      </div>
    );
  }
});

module.exports = DateSelector;
