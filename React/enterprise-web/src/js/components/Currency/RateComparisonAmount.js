/**
    This Amount component has a slightly different behavior than the default.
    It's currently being used only on `RateComparisonModalContent`
**/
export default function RateComparisonAmount ({className, amount}) {
  return (
    <div className={className}>
      <sup>{amount.symbol}</sup>
      <span className="price">{amount.integralPart}</span>
      <sup>{amount.decimalDelimiter}{amount.decimalPart}*</sup>
    </div>
  );
}
RateComparisonAmount.displayName = 'RateComparisonAmount';