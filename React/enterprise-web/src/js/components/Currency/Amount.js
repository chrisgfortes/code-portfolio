export default function Amount ({amount, className}) {
  return (
    <div className={className}>
      <sup className="symbol">{amount.symbol}</sup>
      <span className="unit">{amount.integralPart}</span>
      <sup className="fraction">{amount.decimalDelimiter}{amount.decimalPart}</sup>
    </div>
  );
}
Amount.displayName = 'Amount';