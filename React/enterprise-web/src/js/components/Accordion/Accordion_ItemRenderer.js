'use strict';
var Accordion_ItemRenderer = React.createClass({
  getDefaultProps: function () {
    return {
      data: {}
    };
  },
  itemClicked_Handler (e) {
    var allRows = $(".accordion-row"),
      item = e.currentTarget,
      row = $(item).parent(),
      content = row.find('.accordion-content');
      

    // If selected row is already active, close it
    if (row.hasClass("active")) {
      row.removeClass("active");
      row.outerHeight(50);
    }
    // Otherwise, close all rows and open selected one
    else {
      allRows.removeClass("active");
      allRows.outerHeight(50);
      row.addClass("active");
      row.outerHeight(content.outerHeight() + 100);
    }
  },
  render () {
    return (
      <section className="accordion-row" data-index={this.props.key}>
        <div className="accordion-title cf" onClick={this.itemClicked_Handler}>
          <i className="icon icon-nav-carrot-down"></i>

          <h2>{this.props.title}</h2>
        </div>
        <div className="accordion-content" dangerouslySetInnerHTML={{ __html: this.props.content }}>
        </div>
      </section>
    );
  }
});

module.exports = Accordion_ItemRenderer;
