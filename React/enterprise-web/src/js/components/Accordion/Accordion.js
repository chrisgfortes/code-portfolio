'use strict';
var Accordion_ItemRenderer = require('./Accordion_ItemRenderer');
var Accordion = React.createClass({
  getDefaultProps: function () {
    return {
      data: []
    }
  },
  render () {
    var data = this.props.data,
      rows = data.map(function (row, currentIndex) {
        return <Accordion_ItemRenderer title={row.title}
                                       content={row.content}
                                       key={currentIndex}/>;
      });

    return (
      <section className="accordion">
        {rows}
      </section>
    );
  }
});

module.exports = Accordion;
