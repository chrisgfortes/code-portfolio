/**
 * This component is responsive and what's important is that it starts with:
 * - the wrapper with a clipping css fixed height (defined in _base.css)
 * - the text paragraph with position absolute so we can detect the total height for a transition effect
 * - if props.active is false, no unnecessary operations will happen, just displaying the text
 *
 * To expand:
 * - Recalculate the paragraph's full real height (window could have resized) and updates the wrapper to use it
 * - Change state to collapsed to display the trigger button's "show more" or "show less"
 * - CSS takes over and transitions to a full height based on _base.css styling
 * - After the transition (timing based on props.transitionDelay), set the wrapper's height to auto and the paragraph's position to relative, making it responsive again
 *
 * To collapse:
 * - Recalculate the paragraph's full real height (window could have resized) and updates the wrapper to use it, thus making a height transition possible
 * - Changes the paragraph's position to absolute to maintain it's full real height
 * - After this... (defining "after" as an async setTimeout operation to wait for React and the browser to recalculate)
 * - It will remove the wrapper's height (defaulting to what's defined in _base.scss)
 * - Change state to collapsed to display the trigger button's "show all" or "show less"
 * - CSS takes over and transitions to a collapsed height based on _base.css styling
 * */
const ReservationCursors = require('../../cursors/ReservationCursors');
const BaobabReactMixinBranch = require('baobab-react/mixins').branch;
const classNames = require('classnames');
const AccordionText = React.createClass({
  cursors: {
    modifyModalOpen: ReservationCursors.modifyModalOpen,
    cancelReservationModal: ReservationCursors.cancelReservationModal
  },
  mixins: [
    React.addons.PureRenderMixin,
    BaobabReactMixinBranch
  ],
  propTypes: {
    // condition to trigger clipping. Could be something like '...'.length < 300, or [].length >= 7
    active: React.PropTypes.bool.isRequired,
    // text to render
    text: React.PropTypes.string.isRequired,
    // optional transition delay in ms, defaults to 250
    transitionDelay: React.PropTypes.string
  },
  getInitialState () {
    return {
      wrapperHeight: false,
      collapsed: true
    };
  },
  toggleCollapse () {
    const collapsed = this.state.collapsed;

    if (collapsed) {
      // Expand

      this.setState({
        wrapperHeight: this.refs.text.getDOMNode().offsetHeight,
        collapsed: false
      });

      setTimeout(() =>
        this.setState({
          wrapperHeight: 'auto',
          textPosition: 'relative'
        })
      , this.props.transitionDelay || 250);
    } else {
      // Collapse

      this.setState({
        wrapperHeight: this.refs.text.getDOMNode().offsetHeight,
        textPosition: 'absolute'
      });

      setTimeout(() =>
        this.setState({
          wrapperHeight: false,
          collapsed: true
        })
      , 0);
    }
  },
  render () {
    let buttonClass = classNames([
      'disclosure-button',
      (this.state.modifyModalOpen || this.state.cancelReservationModal) && 'disclosure-button-confirm'
    ]);
    const classes = [
      'accordion-text-wrapper',
      this.state.collapsed && 'collapsed'
    ].filter(c => c).join(' ');

    return (
      <div
        className={this.props.active ? classes : ''}
        style={this.props.active ? { height: this.state.wrapperHeight } : {}}
      >
        {this.props.active &&
          <div className="disclosure-padding">
            <button
              className={buttonClass}
              tabIndex="0"
              onClick={this.toggleCollapse}
            >
              <span className="disclosure-more">
                <i className="icon icon-nav-carrot-down" role="presentation" />
                {i18n('resflowcorporatespecialaccountmessage_0002') || 'SHOW ALL'}
              </span>
              <span className="disclosure-less">
                <i className="icon icon-nav-carrot-up-green" role="presentation" />
                {i18n('resflowcorporatespecialaccountmessage_0003') || 'SHOW LESS'}
              </span>
            </button>
          </div>
        }
        <p
          ref="text"
          className={this.props.active && 'disclosure-target'}
          style={this.props.active ? { position: this.state.textPosition } : {}}
        >
          {this.props.text}
        </p>
      </div>
    );
  }
});

module.exports = AccordionText;
