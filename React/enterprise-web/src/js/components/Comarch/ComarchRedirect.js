export default function ComarchRedirect({page, label, showExternalIcon, profile, comarchInfo}){
  if (!comarchInfo || !page || !label) {
    return false;
  }

  const url = !page.startsWith("http") ? (enterprise.comarch.path.concat(page) || "https://enterpriseplus.enterprise.com/group/ehi/".concat(page)) : page;
  const data = {
          tokenId: _.get(comarchInfo, 'token') || null,
          tokenExp: _.get(comarchInfo, 'expirationTime') || 0,
          domain: 'legacy.enterprise.com',
          loginId: _.get(profile, 'basic_profile.loyalty_data.loyalty_number') || null
        };

  return (
    <form
      name="enterprisePlusSSORedirectForm"
      className={page + 'SSOForm' }
      target="_blank" method="post"
      action={url}
      id="formComarch"
      ref={ r => this.formComarch = r }
    >
      <input type="hidden" name="tokenId" value={data.tokenId}/>
      <input type="hidden" name="domain" value={data.domain}/>
      <input type="hidden" name="logonId" value={data.loginId}/>
      <input type="hidden" name="tokenExp" value={data.tokenExp}/>
      <a
        href={url}
        target="_blank"
        onClick={_onSubmit.bind(this, this.formComarch)}>
          {label} {showExternalIcon &&
          <i className="icon icon-nav-external-link"></i>
        }
      </a>
    </form>
  );
}

function _onSubmit(e, form) {
  e.preventDefault();
  form.submit();
}

ComarchRedirect.displayName = 'ComarchRedirect';
