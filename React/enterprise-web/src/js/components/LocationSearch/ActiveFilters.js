export default function ActiveFilters ({title, filters, filterButton}) {
  return (
    <div className="search-results__active-filters">
      <h3 className="search-results__active-filters-title">
        {title}
      </h3>
      <p>{ filters }</p>
      { filterButton }
    </div>
  );
}

ActiveFilters.displayName = 'ActiveFilters';