/* global ReservationStateTree, google, MarkerWithLabel, InfoBox */
/**
 * Maps Search Module
 * @type React Component
 * @todo Lots. Look for @todo comments.
 */
import LocationSearchController from '../../controllers/LocationSearchController';
import LocationController from '../../controllers/LocationController';
import MathUtil from '../../utilities/util-math';
import classNames from 'classnames';
import { PARTNERS, LOCATION_SEARCH } from '../../constants';

import { Debugger } from '../../utilities/util-debug';

const { VALIDITY } = LOCATION_SEARCH;

const Utility = {
  setGoogleMapOffset () {
    google.maps.Map.prototype.setCenterWithOffset = function (latlng, offsetX, offsetY) {
      let map = this;
      let ov = new google.maps.OverlayView();
      ov.onAdd = function () {
        let projection = this.getProjection();
        let aPoint = projection.fromLatLngToContainerPixel(latlng);
        aPoint.x += offsetX;
        aPoint.y += offsetY;
        map.setCenter(projection.fromContainerPixelToLatLng(aPoint));
      };
      ov.draw = function () {
      };
      ov.setMap(this);
    };
  },
  generateOffSet () {
    return {
      x: -0.13 * Math.max(document.documentElement.clientWidth, window.innerWidth || 0),
      y: 0
    };
  },
  // @todo: should we be doing this elsewhere?
  isMobile () {
    return ResponsiveUtil.isMobile('only screen and (max-width : 770px)');
  }
};

/**
 * Return a Google Api Object Declaration
 * Remember that the marker is bound to the bottom/center point of its declared position
 *   which is the "position" or Marker.position (lat/long)
 * @return {google.maps.Point|google.maps.Size|object}
 */
const googleApiObject = (key)=>{
  let retObj;
  switch (key) {
    case 'markerAnchor':
      // API Docs — Point: The offset from the marker's position to the tip of an InfoWindow
      // that has been opened with the marker as anchor.
      // retObj = new google.maps.Point(32, 28); // original
      // retObj = new google.maps.Point(-5, 29); // working version that does not cover icon
      retObj = new google.maps.Point(32, 43);
      break;
    case 'markerOverAnchor':
      // retObj = new google.maps.Point(-15, 45); // Points first argument may be implicitly negative, see below // WORKING THAT DOES NOT COVER ICON
      retObj = new google.maps.Point(41, 61);
      break;
    case 'iconSize':
      retObj = new google.maps.Size(60, 41);
      break;
    case 'iconOverSize':
      retObj = new google.maps.Size(90, 61);
      break;
    case 'iconAnchor':
      retObj = new google.maps.Point(30, 41); // icon markers negative already on the first argument? so 30 = -30
      break;
    case 'iconAnchorOver':
      retObj = new google.maps.Point(38, 61); // appears the x coordinate is negative already on icons
      break;
    case 'anchorZero':
      retObj = new google.maps.Point(0, 0);
      break;
    default:
      retObj = new google.maps.Point(0, 0);
  }
  return retObj;
};

const Map = React.createClass({
  getInitialState () {
    return {
      drag: false,
      idle: false,
      pageLoad: true,
      panSearch: true,
      initialAdjustment: true
    };
  },
  componentWillMount () {
    this.isDebug = false;
    this.logger = new Debugger(window._isDebug, this, true, 'Map.js');
    LocationSearchController.setSelectionActive(false);
    this.map = this.props.map;
  },
  componentDidMount () {
    // our dear friend Google Maps api has some race conditions
    // that mess up getBounds(), so we need to keep
    // map creation async
    setTimeout(() => this.initializeMap(), 0);
  },
  componentWillUpdate (nextProps) {
    if (!this.map.base) {
      this.logger.log('no map, reinit?');
      if (this.props.location !== nextProps.location) {
        this.logger.log('reinit!!!');
        setTimeout(() => this.initializeMap(), 0);
      } else {
        this.logger.log('no reinit...');
      }
    } else {
      this.logger.debug('willupdate');
      const pinIsHighlighted = this.props.locationSelect.highlightPinLocation;
      const pinShouldBeHighlighted = nextProps.locationSelect.highlightPinLocation;
      const isInDetailView = this.props.mapOfType.target.details;
      const willOpenDetailView = nextProps.mapOfType.target.details;

      //Handles normal cases for when Pin should be highlighted based on ResulItem hover
      if(!pinIsHighlighted && pinShouldBeHighlighted) {
        this._handleMarkerOver(pinShouldBeHighlighted - 1);
      } else if(pinIsHighlighted && !pinShouldBeHighlighted) {
        this._handleMarkerOut(pinIsHighlighted - 1);
      } else if(pinIsHighlighted !== pinShouldBeHighlighted) {
        this._handleMarkerOut(pinIsHighlighted - 1);
        this._handleMarkerOver(pinShouldBeHighlighted - 1);
      }

      //Handles highlighting when Location Details modal is open
      if(isInDetailView && willOpenDetailView) {
        this._handleMarkerOut(isInDetailView.index - 1);
        this._handleMarkerOver(willOpenDetailView.index - 1);
      } else if (isInDetailView && !willOpenDetailView) {
        this._handleMarkerOut(isInDetailView.index - 1);
      } else if (!isInDetailView && willOpenDetailView) {
        this._handleMarkerOver(willOpenDetailView.index - 1);
      }
      this.updateHandler(nextProps);
    }
  },
  componentWillUnmount () {
    this.logger.log('componentWillUnmount()');
    let mapResults = LocationSearchController.getResults();

    mapResults.off('update');

    google.maps.event.removeListener(this.map._listeners.idle);
    google.maps.event.removeListener(this.map._listeners.dragend);

    LocationSearchController.setMapTarget(this.props.type, {
      longitude: null,
      lat: null
    });

    LocationSearchController.unloadDetailsPage(this.props.type);
    LocationSearchController.clearDisplayResults();
    this.map.base = null;

    window.onscroll = null;
  },
  dockMapBasedOnScroll () {
    // this.logger.log('dockMapBasedOnScroll()');

    const $locationSearch = $('.location-search'); // @todo - seriously, we shouldn't be doing it here and we shouldn't be using jQuery for these guys
    const $locationDetails = $('.location-details');
    let details = this.props.mapOfType.target.details;

    if (!details && !Utility.isMobile()) {
      if ($locationSearch.offset().top + $locationSearch.outerHeight(true) - $(window).scrollTop() < 0) {
        $locationDetails.addClass('top');
      } else {
        $locationDetails.removeClass('top');
      }

      if ($(window).scrollTop() > $('footer').offset().top - $('#map-canvas').outerHeight(true)) {
        $locationDetails.removeClass('top');
        $locationDetails.addClass('bottom');
      } else {
        $locationDetails.removeClass('bottom');
      }
    }
  },
  updateHandler (nextProps) {
    if(nextProps.location.locationName != this.props.location.locationName) {
      this.map.initialZoom = nextProps.map.initialZoom;
      if (LocationController.isLocationTypeCountry(nextProps.location)) {
        setTimeout(() => this.initializeMap(), 0);
      }
      if (!this.props.locationSelect.selectionActive) {
        this.logger.debug('locationName update handler');
        if (this.map && this.map.base && nextProps.location.locationName) {
          LocationSearchController.getLocationCenter(nextProps.location).then((resp) => {
            let center = resp;
            if (nextProps.location.lat && nextProps.location.longitude) {
              this.map.base.setCenter(center);
            }
          });
          if (nextProps.location.locationType) {
            setTimeout(()=> {
              this.map.base.setZoom(this.map.initialZoom);
              this.searchThisArea();
            }, 100);
          }
        }
      }
    }

    if(nextProps.mapOfType.target != this.props.mapOfType.target) {
      this.logger.debug('mapTarget update handler');
      if (!this.state.pageLoad) {
        this.resetMap();
      }
    }

    if(nextProps.location.details != this.props.location.details) {
      this.logger.debug('details update handler');
      if (!this.props.locationSelect.selectionActive) {
        this.resetMap();
      }
    }
  },
  // @Todo: Remove this function and change how the update works
  startUpdateListener () {
    let mapResults = LocationSearchController.getResults();

    mapResults.on('update', () => {
      this.logger.debug('map results update handler');
      if (!this.state.pageLoad && !this.props.locationSelect.selectionActive) {
        LocationSearchController.setSelectionActive(false);
        this.loadMapMarkers(true);
      }
    });
  },
  infoBox: null,
  markers: [],
  markerIcons: [],
  /**
   * @todo leverage a generic, global, "open/closed" action or controller method
   * @returns {string url}
   */
  getMarkerIcon (errorStateFlag, suffix) {
    let retVal;
    let pref = '/etc/designs/ecom/dist/img/raster-icons/gmaps/ENT-mappin-single-number-';
    let midTxt = '@2x';
    let endExt = '.png';
    if (errorStateFlag !== null) {
      retVal = pref + suffix + midTxt + '-closed' + endExt;
    } else {
      retVal = pref + suffix + midTxt + endExt;
    }
    return retVal;
  },
  getValidityFlags (nodeList, valType, index) {
    return _.get(nodeList[index], valType + 'Validity.validityType')
  },
  getErrorState (nodeList, index) {
    this.logger.log('getErrorState');
    const nodeDrop = this.getValidityFlags(nodeList, 'dropoff', index);
    const nodePick = this.getValidityFlags(nodeList, 'pickup', index);

    let retVal = null;

    if ( !LocationSearchController.isExternalBrand(_.get(nodeList[index], 'brand')) ) {
      if (nodePick === VALIDITY.INVALID_ALL_DAY || nodePick === VALIDITY.INVALID_AT_THAT_TIME) {
        if(nodeDrop === VALIDITY.INVALID_ALL_DAY || nodeDrop === VALIDITY.INVALID_AT_THAT_TIME) {
          retVal = VALIDITY.CLOSED;
        } else {
          retVal = VALIDITY.PICKUP_INVALID
        }
      } else if (nodeDrop === VALIDITY.INVALID_ALL_DAY || nodeDrop === VALIDITY.INVALID_AT_THAT_TIME) {
        retVal = VALIDITY.DROPOFF_INVALID;
      }
    }

    // return retVal;
    return {
      dropoff: nodeDrop,
      pickup: nodePick,
      flag: retVal
    };
  },
  /**
   * Primarily used by analytics/metrics to identify the types of errors on the map.
   * @param  {object|var} index object or variable path for key
   * @return {string}       string used in a className, e.g. "CLOSED INVALID_ALL_DAY VALID_AFTER_HOURS"
   */
  loadErrorString (index, flag) {
    let val = (flag) ? index.flag : index.errorState;
    val = (val === null) ? 'OPEN' : 'CLOSED';
    return val + ' ' + index.pickup + ' ' + index.dropoff;
  },
  loadMapMarkers () {
    // @todo - Refactor this component to trigger this method only when DisplayingResults exists
    const nodeList = LocationSearchController.getResults(this.props.type, true) || [];
    this.logger.warn('loadMapMarkers() getResults() >> ', nodeList, this.props.type);

    const latLngs = [];

    this.clearMarkers();
    this.map.paintMarkers = [];

    //Ad-hoc :: 1.4 Spatial Alias
    let bounds = new google.maps.LatLngBounds();

    for (let i = 0, len = nodeList.length; i < len; i++) {
      let special = null;
      for (let z = 0; z < nodeList[i].attributes.length; z++) {
        if (nodeList[i].attributes[z] === PARTNERS.MOTORCYCLES || nodeList[i].attributes[z] === PARTNERS.EXOTICS || nodeList[i].attributes[z] === PARTNERS.TRUCKS) {
          special = nodeList[i].attributes[z].toLowerCase();
        } else if ( LocationSearchController.isExternalBrand(_.get(nodeList[i], 'brand')) ) {
          special = nodeList[i].brand.toLowerCase();
        }
      }

      // this.logger.debug('default lat lng', nodeList[i].latitude, nodeList[i].longitude);

      let latLng = new google.maps.LatLng(nodeList[i].latitude, nodeList[i].longitude);
      let brand = nodeList[i].brand.toLowerCase();
      let suffix = special ? special : (nodeList[i].locationType === 'CITY') ? brand : nodeList[i].locationType.toLowerCase();
      let finalLatLng = null;

      if (latLngs.length !== 0) {
        for (let j = 0, max = latLngs.length; j < max; j++) {
          let existingMarker = latLngs[j];
          let pos = existingMarker.getPosition();

          if (MathUtil.calculateSphericalDistance(latLng, pos) < 20) {
            let newLat = latLng.lat() + (Math.random() - 0.5) / 100;
            let newLng = latLng.lng() + (Math.random() - 0.5) / 100;
            finalLatLng = new google.maps.LatLng(newLat, newLng);
            this.logger.debug('final lat lng', newLat, newLng);
          }
        }
      }

      let offset = 50000;

      //Set exotics lower in priority
      if ($.inArray(PARTNERS.EXOTICS, nodeList[i].attributes) !== -1) {
        offset -= 500;
      }


      let errorStateFlag = this.getErrorState(nodeList, i);

      let markIcon = {
        url: this.getMarkerIcon(errorStateFlag.flag, suffix),
        size: googleApiObject('iconSize'),
        scaledSize: googleApiObject('iconSize'),
        origin: googleApiObject('anchorZero'),
        anchor: googleApiObject('iconAnchor')
      };

      let marker = new MarkerWithLabel({
        draggable: false,
        position: finalLatLng ? finalLatLng : latLng,
        map: this.map.base,
        title: nodeList[i].locationNameTranslation,
        labelContent: i + 1,
        labelAnchor: googleApiObject('markerAnchor'),
        labelClass: 'map-labels ' + this.loadErrorString(errorStateFlag, true) + ' ' + LocationSearchController._dtmTracking(nodeList[i].peopleSoftId, 'button'),
        labelInBackground: false,
        labelStyle: {zIndex: offset + 2 * i + 1},
        optimized: false,
        icon: markIcon,
        errorState: errorStateFlag.flag,
        pickup: errorStateFlag.pickup,
        dropoff: errorStateFlag.dropoff,
        id: nodeList[i].peopleSoftId
      });

      // this.logger.debug('ERROR STATE ON MARKER:', marker.errorState);
      // this.logger.debug('\tHours Data(pickup):', marker.pickupHours);
      // this.logger.debug('\tHours Data(dropoff)', marker.dropoffHours);

      this.markerIcons.push(markIcon);
      this.markers.push(marker);

      marker.setZIndex(offset - (!!errorStateFlag.flag * 1000) - 2 * i);

      latLngs.push(marker);
      bounds.extend(finalLatLng ? finalLatLng : latLng);

      let tohandle;

      google.maps.event.addListener(marker, 'mouseout', () => {
        this._handleErrorState(-1);
        tohandle = setTimeout(()=>{
          this.logger.debug('==mouseout marker', i);
          this._handleMarkerOut(i);
          LocationSearchController.setHighlightLocation(-1);
        }, 200);
      });
      google.maps.event.addListener(marker, 'mouseover', () => {
        clearTimeout(tohandle);
        this.logger.debug('==mouseover marker', i);
        this._handleMarkerOver(i);
        setTimeout(this._handleErrorState(i), 500);
        LocationSearchController.setHighlightLocation(i + 1);
      });
      google.maps.event.addListener(marker, 'click', () => this._handleMarkerClick(nodeList[i]));
      this.map.paintMarkers.push(marker);
    }

    if (this.state.initialAdjustment) {
      // ECR-10375 :: do not set fitBounds() when there are no search results, we end up with bounds of 1,180,-1,-180 which is NOT GOOD
      if (nodeList.length > 0) {
        this.map.base.fitBounds(bounds);
      }
      this.map.base.setZoom(this.map.initialZoom);
      this.setState({
        initialAdjustment: false
      });
    }

    this.logger.debug('MARKERS:', this.markers);

    this.logger.debug('map bounds: ', this.findBounds());
  },
  resetMap () {
    if (this.map.base) {
      this.setState({
        panSearch: false
      });
      setTimeout(()=> {
        google.maps.event.trigger(this.map.base, 'resize');
        this.setState({
          panSearch: true
        });
      }, 100);
    }
  },
  // @Todo: Fine Tune Map Marker Resize
  _handleMarkerOver (index) {
    // this.logger.log('_handleMarkerOver');
    if(this.markerIcons && this.markerIcons.length > 0 && this.markerIcons[index]) {
      let iconOverSize = googleApiObject('iconOverSize');
      this.markerIcons[index].scaledSize = iconOverSize;
      this.markerIcons[index].size = iconOverSize;
      this.markerIcons[index].anchor = googleApiObject('iconAnchorOver');
    }
    if(this.markers && this.markers.length > 0 && this.markerIcons[index]) {
      this.logger.debug('.....', this.markers[index].errorState, this.markers[index].pickup, this.markers[index].dropoff);
      this.markers[index].setIcon(this.markerIcons[index]);
      this.markers[index].set('labelClass', 'map-labels-over ' + this.loadErrorString(this.markers[index], false) + ' ' + LocationSearchController._dtmTracking(this.markers[index].id, 'button'));
      this.markers[index].set('labelAnchor', googleApiObject('markerOverAnchor'));
      this.markers[index].setZIndex(this.markers[index].getZIndex() + 2000);
    }
  },
  // @Todo: Fine Tune Map Marker Resize
  _handleMarkerOut (index) {
    // this.logger.log('_handleMarkerOut');
    if(this.markerIcons && this.markerIcons.length > 0 && this.markerIcons[index]) {
      let iconSize = googleApiObject('iconSize');
      this.markerIcons[index].scaledSize = iconSize;
      this.markerIcons[index].size = iconSize;
      this.markerIcons[index].anchor = googleApiObject('iconAnchor');
    }
    if(this.markers && this.markers.length > 0 && this.markerIcons[index]) {
      this.markers[index].setIcon(this.markerIcons[index]);
      this.markers[index].set('labelClass', 'map-labels ' + this.loadErrorString(this.markers[index], false) + ' ' + LocationSearchController._dtmTracking(this.markers[index].id, 'button'));
      this.markers[index].set('labelAnchor', googleApiObject('markerAnchor'));
      this.markers[index].setZIndex(this.markers[index].getZIndex() - 2000);
    }
  },
  _handleMarkerClick (details) {
    LocationSearchController.setDetailsPage(this.props.type, true);
    LocationSearchController.loadDetailsPage(details, this.props.type);
  },
  // not sure when this is fired, to be honest
  _handleSelect (details) {
    this.logger.warn('_handleSelect(details)', details);
    // @todo: ECR-14192 almost made this a model but i can't even find when used
    let location = {
      key: details.peopleSoftId,
      locationName: details.locationNameTranslation || details.defaultLocationName,
      locationType: LOCATION_SEARCH.STRICT_TYPE.BRANCH,
      lat: details.latitude,
      longitude: details.longitude
    };
    LocationSearchController.loadDetailsPage(details, this.props.type);
    LocationSearchController.setLocation(location, this.props.type);
  },
  _showTooltip (index, errorValue) {
    this.logger.log('_showToolTip');
    const tooltipContent =
      `<div class='marker-tooltip-content'>
      ${this.displayError(errorValue)}
      <div class='marker-tooltip-content-sub'>
        ${i18n('locationsearch_0017') || 'Select to adjust times'}
      </div>
    </div>`;

    let tooltipPositionX = 52;
    const dynamicOffset = errorValue  === VALIDITY.CLOSED ? 68 : 0;
    if(this.map.base.center.lng() < this.markers[index].position.lng()) {
      tooltipPositionX = - tooltipPositionX - 196 - dynamicOffset; //200 = tooltip width
    }
    if(this.infoBox) {
      this.infoBox.setOptions({
        content: tooltipContent,
        pixelOffset: new google.maps.Size(tooltipPositionX, -61),
        boxClass: `marker-tooltip-wrapper marker-${index}`
      });
    } else {
      this.infoBox = new InfoBox({
        content: tooltipContent,
        disableAutoPan: true,
        pixelOffset: new google.maps.Size(tooltipPositionX, -61),
        boxClass: `marker-tooltip-wrapper marker-${index}`,
        closeBoxURL: ''
      });
    }

    setTimeout(this.infoBox.open(this.map.base, this.markers[index]), 500);

  },
  _hideTooltip () {
    this.logger.log('_hideTooltip');
    if(this.infoBox) {
      this.infoBox.close();
      this.infoBox = null;
    }
  },
  _handleErrorState (index) {
    this.logger.log('_handleErrorState');
    if (index === -1) {
      this._hideTooltip();
    } else if(this.markers && this.markers.length > 0) {
      let errorValue = this.markers[index].errorState;
      if (errorValue) {
        this._showTooltip(index, errorValue);
      }
    }
  },
  displayError (errorValue) {
    this.logger.log('displayError');
    /*
     Error codes & translation keys:

     5. locationsearch_0002: Closed during your pick-up
     6. locationsearch_0003: Closed during your return
     7. locationsearch_0004: Closed during your pick-up & return
     21. locationsearch_0016: Hours for #{locationDate}
     22. locationsearch_0017: Select to adjust times
     */
    let retVal;
    if (errorValue === VALIDITY.CLOSED) {
      retVal = i18n('locationsearch_0004') || 'Closed during your pick-up & return';
    } else if (errorValue === VALIDITY.PICKUP_INVALID) {
      retVal = i18n('locationsearch_0002') || 'Closed during your pick-up';
    } else if (errorValue === VALIDITY.DROPOFF_INVALID) {
      retVal = i18n('locationsearch_0003') || 'Closed during your return';
    }
    return retVal;
  },
  zoomControls (controls, map, context) {
    let $controlWrapper = $('<div id="google-map-custom-controls-wrapper" class="google-map-custom-controls-wrapper" />');
    let $zoomInBtn = $('<button id="google-map-zoom-in" class="google-map-zoom-btn">+</button>');
    let $zoomOutBtn = $('<button id="google-map-zoom-out" class="google-map-zoom-btn">-</button>');

    //Assemble HTML
    $controlWrapper.append($zoomInBtn);
    $controlWrapper.append($zoomOutBtn);
    controls.appendChild($controlWrapper[0]);

    //Zoom in listener
    google.maps.event.addDomListener($zoomInBtn[0], 'click', ()=> {
      if (map.maxZoom >= map.zoom + 1) {
        let zoom = map.getZoom();
        map.setZoom(zoom + 1);
        context.setState({drag: true});
        LocationController.isLocationTypeCountry(this.props.location) ? false : this.searchThisArea();
      }
    });

    //Zoom out listener
    google.maps.event.addDomListener($zoomOutBtn[0], 'click', ()=> {
      if (map.zoom - 1 > 0) {
        let zoom = map.getZoom();
        map.setZoom(zoom - 1);
        context.setState({drag: true});
        LocationController.isLocationTypeCountry(this.props.location) ? false : this.searchThisArea();
      }
    });


    //Controls base location/placement
    map.controls[google.maps.ControlPosition.RIGHT_TOP].push($controlWrapper[0]);
  },
  clearMarkers () {
    this.markers = [];
    this.markerIcons = [];
    for (let i = 0; i < this.map.paintMarkers.length; i++) {
      this.map.paintMarkers[i].setMap(null);
    }
  },
  calculateRadius ( bounds ) {
    return (google.maps.geometry.spherical.computeDistanceBetween(bounds.mapCenter, bounds.northEast)*0.001).toFixed();
  },
  searchThisArea () {
    this.logger.warn('searchThisArea()');
    let mapBoundsModel = this.findBounds();
    //get radius in KM
    let radius = (this.map.base.zoom !== LOCATION_SEARCH.INITIAL_ZOOM && this.map.base.zoom !== LOCATION_SEARCH.INITIAL_COUNTRY_ZOOM) ? this.calculateRadius(mapBoundsModel) : LOCATION_SEARCH.INITIAL_RADIUS;
    const options = {
      radius: radius,
      type: this.props.type,
      mustPassDateTime: true
    };

    if (this.props.location.type === LOCATION_SEARCH.STRICT_TYPE.COUNTRY) {
      LocationSearchController.getCountryBranches(this.props.location.countryCode, mapBoundsModel, this.props.type);
    } else {
      LocationSearchController.doSpatialSearch(mapBoundsModel, options);
    }
    if (this.props.debug) {
      new google.maps.Rectangle({
        bounds: mapBoundsModel.bounds,
        map: this.map.base,
        fillColor: '#F1A2F5',
        strokeColor: '#FF0000'
      });
    }
  },
  findBounds () {
    return LocationSearchController.getMapBoundsModel(this.map.base);
  },
  addBoundChangeListener () {
    const location = this.props.location;

    this.map._listeners.dragend = google.maps.event.addListener(this.map.base, 'dragend', () => {
      this.setState({drag: true});
    });

    this.map._listeners.idle = google.maps.event.addListener(this.map.base, 'idle', () => {
      this.setState({idle: true});

      if (!this.props.locationSelect.selectionActive) {
        if (!this.state.drag && this.state.pageLoad) {
          this.searchThisArea();
          this.resetMap();
          this.loadMapMarkers(true);
          this.setState({pageLoad: false});
        }
        const details = this.props.mapOfType.target.details;
        const enableSearch = location.locationId && !details;
        if (this.state.drag && this.state.idle && enableSearch && this.state.panSearch) {
          setTimeout(()=> {
            LocationController.isLocationTypeCountry(location) ? false : this.searchThisArea();
            this.setState({
              drag: false,
              idle: false
            });
          }, 100);
        }
      }

    });
  },
  initializeMap () {
    this.logger.warn('initializeMap()');
    let location = this.props.location;
    // this.logger.log('attempted search using map location:', location);
    // the isLocationPopulated Promise is a promise from LocationSearchController.initiateOnLoadSearch()
    LocationSearchController.isLocationPopulated.then(() => {
      this.logger.log('0000000 location selected promise has returned!');
      LocationSearchController.getLocationCenter(location).then((resp) => {
        // LocationSearchController.setLoading(null);
        let center = resp;
        // this.logger.warn('response from getLocationCenter()', center, center.lat(), center.lng());
        const handler = document.getElementById('map-canvas');

        window.scrollTo(0, 0);
        // @todo: change this: we should never assign an event handler like this as it overrides others
        window.onscroll = () => {
          this.dockMapBasedOnScroll();
        };

        // @todo ECR-14192 model?
        this.map.base = new google.maps.Map(handler, {
          zoom: this.map.initialZoom,
          center: center,
          scrollwheel: false,
          maxZoom: 20,
          styles: [{
            featureType: 'poi',
            stylers: [{
              visibility: 'off'
            }]
          }],
          disableDefaultUI: true
        });

        //Custom Zoom controls
        let zoomControlDiv = document.createElement('div');
        this.zoomControls(zoomControlDiv, this.map.base, this);
        zoomControlDiv.index = 1;
        this.map.base.controls[google.maps.ControlPosition.TOP_LEFT].push(zoomControlDiv);

        this.startUpdateListener();
        this.addBoundChangeListener();
      })
      .catch((err) => {
        console.error('Mapping location has no center!', err);
      });
    }).catch(err => console.error('Map tried to load, but isLocationPopulated promise failed.', err));
  },
  render () {
    this.logger.count('render()');
    const map = this.props.mapOfType;
    // @todo: we can't pass this into this component?
    // const resultsList = this.props.resultsList;
    const resultsList = LocationSearchController.getResults(this.props.type, true);
    let showMobileMap = map.mobileTab === 'map';
    const results = resultsList && resultsList.length === 0;
    const details = map.target.details;
    const mapClasses = classNames({
      'active': showMobileMap || results,
      'map-wrapper': true
    });
    const mobileHeight = Utility.isMobile() ? '90.5vh' : '100vh';
    const altMobileHeight = '100vh';
    const style = {
      height: details ? '100%' : mobileHeight
    };

    let newStyle = {
      height: details ? '86.5%' : altMobileHeight
    };

    return (
      <div style={style} className={mapClasses} aria-hidden="true">
        <div style={newStyle} id="map-canvas" aria-label={i18n('locations_0047') || 'Map of locations. For details, refer to the Locations list.'}/>
      </div>
    );
  }
});

module.exports = Map;
