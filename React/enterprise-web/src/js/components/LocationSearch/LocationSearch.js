import ReservationCursors from '../../cursors/ReservationCursors';
import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';

import LocationSearchController from '../../controllers/LocationSearchController';
import LocationController from '../../controllers/LocationController';
import Header from './Header';
import Modals from './Modals';
import ResultsMobileHeader from './ResultsMobileHeader';
import SearchFilter from './SearchFilter';
import Results from './Results';
import Map from './Map';
import Details from './Details';

import { LOCATION_SEARCH } from '../../constants';

const LocationSearch = React.createClass({
  mixins: [BaobabReactMixinBranch, React.addons.PureRenderMixin],
  componentWillMount: function () {
    LocationSearchController.initiateOnLoadSearch(this.props.type);
    this.listenerCursors = LocationSearchController.initiateLocationSearchListeners(this.props.type);
  },
  componentWillUnmount() {
    LocationSearchController.releaseLocationSearchListeners(this.listenerCursors);
  },
  cursors (props) {
    return {
      locationDisplayResults: ReservationCursors.locationDisplayResults,
      locationFilters: ReservationCursors.locationFilters,
      highlightLocation: ReservationCursors.highlightLocation,
      locationSelect: ReservationCursors.locationSelect,
      isFedexReservation: ReservationCursors.isFedexReservation,
      // this has nothing to do with the logger/debug so it may no longer be used
      debug: ReservationCursors.debug,
      location: ['model', props.type, 'location'],
      mapOfType: ['view', 'locationSelect', props.type, 'map'],
      mapModel: ['model', props.type, 'map'],
      dateOfType: ['model', props.type, 'date', 'momentDate'],
      session: ReservationCursors.reservationSession,
      sameLocation: ReservationCursors.sameLocation,
      errors: ReservationCursors.locationSelectErrors,
      pickupLocationSelect: ReservationCursors.pickupLocationSelect,
      dropoffLocationSelect: ReservationCursors.dropoffLocationSelect,
      inflightModify: ReservationCursors.inflightModify,
      deepLinkReservation: ReservationCursors.deepLinkReservation
    }
  },
  onTabClick (tab) {
    LocationSearchController.mobileTabSelect(this.props.type, tab);
  },
  getResultsCount (results) {
    return results && results.length;
  },
  render: function () {
    const {locationDisplayResults, locationFilters, highlightLocation,
          locationSelect, debug, mapOfType, dateOfType, location, mapModel, session,
          // locationSelect, mapOfType, dateOfType, location, mapModel, session, // uncomment when resetting debug
          sameLocation, errors, pickupLocationSelect, dropoffLocationSelect,
          inflightModify, isFedexReservation, deeplinkReservation} = this.state;

    // let debug = true;

    const {type, deepLinkErrors, modelController} = this.props;

    const activeTab = mapOfType.mobileTab;
    const isShowingDetails = !!mapOfType.target.details && !locationSelect.preventDetails;
    const map = {
      base: null,
      paintMarkers: [],
      initialZoom: LocationController.isLocationTypeCountry(location) ? LOCATION_SEARCH.INITIAL_COUNTRY_ZOOM : LOCATION_SEARCH.INITIAL_ZOOM,
      _listeners: {}
    };
    (locationDisplayResults && locationDisplayResults.length > 0) ?
      locationDisplayResults.map((item, index) => {
        item.index = index + 1;
      }) : null;

    // let resultsList = LocationSearchController.getResults(this.props.type, true);

    return (
      <div className="location-details">
        <Header type={type}
                isFedex={isFedexReservation}/>

        {deepLinkErrors}

        <div className="search-results-wrapper">
          <ResultsMobileHeader activeTab={activeTab} onTabClick={this.onTabClick}
                               isActive={!isShowingDetails}/>

          <SearchFilter resultsCount={this.getResultsCount(locationDisplayResults)} filters={locationFilters}
                        onOpenForMyTimesChanged={LocationSearchController.updateOpenForMyTimes}
                        isActive={!isShowingDetails}/>

          { isShowingDetails ?
            <Details type={type}
                       {...{
                         mapOfType, mapModel, location, sameLocation, locationSelect, errors,
                         pickupLocationSelect, dropoffLocationSelect, inflightModify, deeplinkReservation,
                         dateOfType
                       }} />
            :
            <Results type={type} results={locationDisplayResults}
                     isActive={activeTab === 'list'}
                     highlightedLocation={highlightLocation}
                     activeFilters={LocationSearchController.getActiveFilters()}
                     clearFilters={LocationSearchController.clearFilters} />
          }

          <Map type={type}
               map={map}
               {...{debug, mapOfType, location, mapModel, locationSelect, session}}/>

        </div>

        <Modals type={type} modelController={modelController} />
      </div>
    );
  }
});

module.exports = LocationSearch;
