export default class AfterHoursDropoff extends React.Component {
  constructor (props) {
    super(props);
  }
  render (){
    return (
      <div className="location-after-hours">
        <i className="icon icon-after-hours"/>
        <span className="location-after-hours_cta">  {i18n('locationsearch_0005') || 'After-hours return'} </span>
        <span className="location-after-hours_tooltip" tabIndex="0" aria-describedby="location-after-hours_tooltip-content">
                ({i18n('locationsearch_0006') || 'What\'s This?'})
                <span id="location-after-hours_tooltip-content" className="location-after-hours_tooltip-content" role="tooltip">
                  <div>
                    {i18n('locationsearch_0007') ||
                    'You are able to return your vehicle to this location even if the location is closed during your selected time. Please view the location\'s specific after-hours policy. '}
                    <a href="#" onClick={ this.props.afterHoursModal }>{i18n('locationsearch_0027') ||
                    'After-Hours Policy'}</a>
                  </div>
                </span>
              </span>
      </div>
    );
  }
}
