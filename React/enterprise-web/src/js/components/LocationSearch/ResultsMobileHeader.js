import classNames from 'classnames';

const tabLabels = {
  list : i18n('resflowlocations_0051') || 'LIST',
  map  : i18n('resflowlocations_0052') || 'MAP'
};

let index = 0;

export default class ResultsMobileHeader extends React.Component {
  renderTab(tab) {
    const tabClasses = classNames({
      'tab': true,
      [tab]: true,
      'active': tab === this.props.activeTab
    });
    const onTabClick = this.props.onTabClick.bind(null, tab);
    // @todo - add a11y attributes to indicate these spans are tabs and handle a11y interactions
    index++;
    return (
      <span className={tabClasses} tabIndex="0"
            onClick={onTabClick} key={index} onKeyPress={a11yClick(onTabClick)}>
        { tabLabels[tab] }
      </span>
    );
  }
  render () {
    return this.props.isActive && (
      <div className="mobile-results-header">
        { ['list', 'map'].map(tab => this.renderTab(tab)) }
      </div>
    );
  }
}

ResultsMobileHeader.displayName = 'ResultsMobileHeader';