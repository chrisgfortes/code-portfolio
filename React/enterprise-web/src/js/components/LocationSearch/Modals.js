import ReservationCursors from '../../cursors/ReservationCursors';
import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import LocationDateTimeErrorModal from './LocationDateTimeErrorModal';
import CorporateModals from '../Corporate/ActionModals';
import ConfirmationModalContent from '../Modify/ConfirmationModalContent';
import NoVehicleAvailabilityModal from '../Errors/NoVehicleAvailabilityModal';
import AfterHoursModalContent from './AfterHoursModalContent';
import GlobalModal from '../Modal/GlobalModal';
import LocationSearchController from '../../controllers/LocationSearchController';
import {
  toggleInflightModifyModal,
  checkForExistingCorporateCode,
  clearEmployeeNumberError,
  setBookingWidgetUpdatedError
} from '../../controllers/BookingWidgetModelController';
import { LOCATION_SEARCH } from '../../constants';
import LocationFactory from '../../factories/LocationFactory';

const Modals = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors (props) {
    return {
      showDatePickerModalOfType: ReservationCursors.showDatePickerModalOfType,
      modifyModalOpen: ReservationCursors.modifyModalOpen,
      showAfterHoursModal: ReservationCursors.showAfterHoursModal,
      inflightModify: ReservationCursors.inflightModify,
      noVehicleAvailability: ReservationCursors.noVehicleAvailability,
      afterHoursMessage: ReservationCursors.afterHourMessage,
      mapOfType: ['view', 'locationSelect', props.type, 'map'],
      pickupLocationSelect: ReservationCursors.pickupLocationSelect,
      dropoffLocationSelect: ReservationCursors.dropoffLocationSelect,
      pickupDate: ReservationCursors.pickupDate,
      dropoffDate: ReservationCursors.dropoffDate,
      sameLocation: ReservationCursors.sameLocation,
      employeeNumber: ReservationCursors.employeeNumber,
      errors: ReservationCursors.locationSelectErrors
    }
  },
  closeDateTimeModal () {
    LocationSearchController.closeDateTimeErrorModal(this.props.type)
  },
  _handleConfirmModify: function () {
    const current = this.state.mapOfType.target.details;
    const location = LocationFactory.getSimpleLocationModel(current, {
      locationType: LOCATION_SEARCH.STRICT_TYPE.BRANCH
    });
    LocationSearchController.setLocation(location, this.props.type);
  },
  render () {
    let domHeaderFooterElement = $('header, footer');
    const {showDatePickerModalOfType, modifyModalOpen, showAfterHoursModal, inflightModify, noVehicleAvailability, afterHoursMessage,
            mapOfType, pickupLocationSelect, dropoffLocationSelect, pickupDate, dropoffDate, sameLocation, employeeNumber, errors} = this.state;

    {inflightModify.modal ?
      domHeaderFooterElement.attr('aria-hidden', 'true') :
      domHeaderFooterElement.removeAttr('aria-hidden');
    }

    return (
      <div className="modals">
        {showDatePickerModalOfType &&
        <GlobalModal active={showDatePickerModalOfType ? true : false}
                     content={<LocationDateTimeErrorModal modelController={this.props.modelController}
                                                          type={this.props.type}
                                                          pickupSelect={pickupLocationSelect}
                                                          dropoffSelect={dropoffLocationSelect}
                                                          {...{mapOfType, pickupDate,
                                                            dropoffDate, sameLocation}} />}
                     fullScreen={true}
                     header=" "
                     contentHeader={i18n('locationsearch_0024') || 'Location Closed: Please Update Your Travel Selection'}
                     disableBlur={true}
                     close={this.closeDateTimeModal}
                     classLabel="date-time-error-modal" />
        }

        {noVehicleAvailability &&
        <GlobalModal active={noVehicleAvailability}
                     header={i18n('resflowviewmodifycancel_1015')}
                     classLabel="no-vehicle-availability-modal"
                     content={<NoVehicleAvailabilityModal modelController={this.props.modelController}
                                                          setBookingWidgetUpdatedError={setBookingWidgetUpdatedError}
                                                          checkForExistingCorporateCode={checkForExistingCorporateCode}
                                                          clearEmployeeNumberError={clearEmployeeNumberError}
                                                          toggleInflightModifyModal={toggleInflightModifyModal}
                                                          {...{inflightModify, employeeNumber, errors}} />}
                     cursor={['view', 'specialError', 'noVehicleAvailability', 'modal']}/>
        }

        {inflightModify.modal &&
        <GlobalModal active={modifyModalOpen}
                     header={i18n('resflowreview_0098')}
                     content={<ConfirmationModalContent confirm={this._handleConfirmModify} />}
                     contentHeader={i18n('resflowreview_0098')}
                     contentText={i18n('resflowviewmodifycancel_2006')}
                     cursor={['view', 'inflightModify', 'modal']}/>
        }

        {showAfterHoursModal &&
        <GlobalModal active={showAfterHoursModal ? true : false}
                     content={<AfterHoursModalContent afterHoursMessage={afterHoursMessage} />}
                     fullScreen={true}
                     header={i18n('locationsearch_0027') ||
                     'After-Hours Policy'}
                     disableBlur={true}
                     close={LocationSearchController.closeAfterHoursModal}
                     classLabel="after-hours-modal"/>
        }

        <CorporateModals modelController={this.props.modelController} />
      </div>
    );
  }
});

export default Modals;
