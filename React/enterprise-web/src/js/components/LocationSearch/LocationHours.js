import classNames from 'classnames';
import Hours from './Hours';
import LocationSearchController from '../../controllers/LocationSearchController';

function getClassNames(direction, bs) {
  return direction === 'next' ?
    classNames({
      'next': true,
      'disabled': bs.disableNext
    })
    :
      classNames({
        'prev': true,
        'disabled': bs.disablePrev
      })
    ;
}

function getDisabledButtonsState(currDate, _date) {
  let oneYear = moment().add(1, 'year');
  let now = moment();
  let ret = {};
  let date = _date || currDate;

  // this prevents a JS error but doesn't fix the issue overall
  if (typeof date !== 'undefined') {
    ret.disablePrev = date.isSame(now) || date.isBefore(now);
    ret.disableNext = date.isSame(oneYear) || date.isAfter(oneYear);
  }

  return ret;
}

export default class LocationHours extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: LocationSearchController.getHoursForDate(this.props.deepLinkReservation, this.props.mapOfType,
                                              this.props.dateOfType, this.props.type)
    }
  }

  componentWillUpdate(nextProps, nextState) {
    let statesBefore = getDisabledButtonsState(this.props.date);
    let statesAfter = getDisabledButtonsState(this.props.date, nextProps.date);
    let didPrevButtonChange = statesBefore.disablePrev !== statesAfter.disablePrev;
    let didNextButtonChange = statesBefore.disableNext !== statesAfter.disableNext;

    if (didPrevButtonChange && statesAfter.disablePrev && !statesAfter.disableNext) {
      // PREV week button changed visibility and now NEXT week button is the only available.
      this.refs.nextWeekButton.getDOMNode().focus();
    } else if (didNextButtonChange && !statesAfter.disablePrev && statesAfter.disableNext) {
      // ...analogous.
      this.refs.prevWeekButton.getDOMNode().focus();
    }
  }

  onWeekChange(direction) {
    let date = (direction === 'next') ?
      this.state.date.clone().add(1, 'week')
      :
      this.state.date.clone().subtract(1, 'week')
    ;
    this.setState({ date: date });
    LocationSearchController.getWeeksData(this.props.type, date, this.props.mapOfType.target,
                                        this.props.deepLinkReservation, enterprise.locationDetail);
  }

  render() {
    let days = [];
    const {week} = this.props;
    const {date} = this.state;
    let bs = getDisabledButtonsState(date);

    if (week && week.constructor !== Array) {
      for (let i in week) {
        if (week[i].STANDARD) {
          days.push(
            <tr className="availability-wrapper" key={i}>
              <th scope="row" className="location-date">
                {enterprise.utilities.capitalize(moment(i).format('dddd'))}
              </th>
              <td>
                <Hours lineItems={LocationSearchController.getLineItems(week[i].STANDARD)}/>
              </td>
            </tr>
          );
        }
      }
    }

    return (
      <div className="location-availability">
        <div id="locationAvailabilityAriaTitle"
             className="location-availability-header">{enterprise.i18nReservation.locations_0019}</div>

        <div className="location-availability-control">
          <span className={getClassNames('prev', bs)} role="button" tabIndex="0" ref="prevWeekButton"
                onKeyPress={bs.disablePrev ? false : a11yClick(this.onWeekChange.bind(this, 'prev'))}
                onClick={bs.disablePrev ? false : this.onWeekChange.bind(this, 'prev')}>
            &larr;
          </span>
          <span className="week-label">{date.format('LL')}</span>
          <span className={getClassNames('next', bs)} role="button" tabIndex="0" ref="nextWeekButton"
                onKeyPress={bs.disableNext ? false : a11yClick(this.onWeekChange.bind(this, 'next'))}
                onClick={bs.disableNext ? false : this.onWeekChange.bind(this, 'next')}>
            &rarr;
          </span>
        </div>

        <table className="availability-datatable" aria-labelledby="locationAvailabilityAriaTitle">
          <tbody>
            {days}
          </tbody>
        </table>
      </div>
    );
  }
}

LocationHours.displayName = "LocationHours";