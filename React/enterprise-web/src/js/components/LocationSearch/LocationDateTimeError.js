import LocationSearchController from '../../controllers/LocationSearchController';
import Hours from './Hours';
import { Debugger } from '../../utilities/util-debug';

const lang = {
  header: i18n('locationsearch_0018') || 'Sorry, this location is closed for',
  button: i18n('resflowlocations_0034'),
  dates: (props, error) => {
    return i18n('locationsearch_0016').replace('#\{locationDate\}', moment(props[error.type+'Date']).format('MMM DD'))
      || 'Hours for ' + moment(props[error.type+'Date']).format('MMM DD')
  },
  invalidPickupTime: (isModal) => {
    if (isModal) {
      return i18n('resflowlocations_0092') || 'Sorry, this location is closed for your pick-up time';
    } else {
      return i18n('locationsearch_0012') || 'your pick-up time';
    }
  },
  invalidPickUpDate: (isModal) => {
    if (isModal) {
      return i18n('resflowlocations_0090') || 'Sorry, this location is closed for your pick-up date.';
    } else {
      return i18n('locationsearch_0013') || 'your pick-up date';
    }
  },
  invalidDropOffTime: (isModal) => {
    if (isModal) {
      return i18n('resflowlocations_0093') || 'Sorry, this location is closed for your  return time.';
    } else {
      return i18n('locationsearch_0014') || 'your return time';
    }
  },
  invalidDropOffDate: (isModal) => {
    if (isModal) {
      return i18n('resflowlocations_0091') || 'Sorry, this location is closed for your return date.';
    } else {
      return i18n('locationsearch_0015') || 'your return date';
    }
  }
};

const LocationDateTimeError = React.createClass({
  getInitialState () {
    return {
      errorState: [],
      fullRender: true
    }
  },
  getDefaultProps () {
    return {
      narrowType: null,
      modalError: false
    }
  },
  componentWillMount () {
    this.logger = new Debugger(window._isDebug, this, true, 'LocationDateTimeError.js');
    this.createRenderState(this.props);
  },
  componentWillReceiveProps(nextProps) {
    if (this.props.pickupSelect !== nextProps.pickupSelect || this.props.dropoffSelect !== nextProps.dropoffSelect) {
      this.logger.debug('componentWillReceiveProps()', nextProps);
      this.createRenderState(nextProps);
    }
  },
  isDebug: false,
  logger: {},
  createRenderState (propScope) {
    const errorState = [];

    let renderState = true;

    this.logger.group('createRenderState');

    if (this.props.narrowType) {
      if (LocationSearchController.hasErrorsOfType(propScope[this.props.narrowType + 'Select'])) {
        renderState = true;
      } else {
        renderState = false;
      }
    }

    if(propScope.type === 'pickup' || propScope.type === 'sameLocation' && (this.props.narrowType === 'pickup' || !this.props.narrowType)) {
      this.logger.debug(propScope.type, 'narrowed?', this.props.narrowType);
      let errorMessage;

      if (propScope.pickupSelect.invalidTime) {
        errorMessage = lang.invalidPickupTime(this.props.modalError);
      } else if (propScope.pickupSelect.invalidDate) {
        errorMessage = lang.invalidPickUpDate(this.props.modalError);
      }

      if(errorMessage) {
        errorState.push({
          type: 'pickup',
          error: propScope.pickupSelect.invalidTime ? 'invalidTime' : 'invalidDate',
          message: errorMessage,
          validHours: propScope.pickupSelect.validHours,
          date: propScope.pickupDate
        });
      }
    }

    if (propScope.type === 'dropoff' || propScope.type === 'sameLocation' && (this.props.narrowType === 'dropoff' || !this.props.narrowType)) {
      this.logger.debug(propScope.type, 'narrowed?', this.props.narrowType);

      let errorMessage;
      if (propScope.dropoffSelect.invalidTime) {
        errorMessage = lang.invalidDropOffTime(this.props.modalError);
      } else if (propScope.dropoffSelect.invalidDate) {
        errorMessage = lang.invalidDropOffDate(this.props.modalError);
      }
      if(errorMessage) {
        errorState.push({
          type: 'dropoff',
          error: propScope.dropoffSelect.invalidTime ? 'invalidTime' : 'invalidDate',
          message: errorMessage,
          validHours: propScope.dropoffSelect.validHours,
          date: propScope.dropoffDate
        });
      }
    }
    this.setState({
      errorState: errorState,
      fullRender: renderState
    });
  },
  _openDateTimePicker: function () {
    this.props.modelController.showDatePickerModalOfTypeAndView(this.props.type);
  },
  renderHeader () {
    if (!this.props.modalError) {
      return (<h2>{lang.header}</h2>);
    } else {
      return null;
    }
  },
  renderHoursHeader () {

    if(this.props.modalError && (this.state.errorState.length > 1 || this.state.errorState[0].error === 'invalidTime')) {
      return i18n('locationsearch_0025') || 'Hours';
    } else {
      return null;
    }
  },
  getHours ( valHours ) {
    let lineItems = [];
    if (valHours && valHours.STANDARD) {
      let hours = valHours.STANDARD.hours || []
      hours.map((hour, index) => {
        let operatingTime = hour ? moment(hour.open, 'H:mm').format('LT') + ' - ' + moment(hour.close, 'H:mm').format('LT') : i18n('locations_0020');
        lineItems.push(
          <span className="location-hour" key={index}>
        {operatingTime}
      </span>
        );
      });
    } else {
      lineItems.push(<span className="location-hour">{i18n('locations_0020')}</span>)
    }
    return lineItems;
  },
  render: function () {
    this.logger.log('render()');

    const button = this.props.showButton ? <a onClick={this._openDateTimePicker}
                                            className="btn modify-time"
                                              data-dtm-tracking={LocationSearchController._dtmTracking(this.props.locationId, 'button')}>{lang.button}</a> : null;

    const errorStateClass = this.state.errorState.length > 1 ? 'error-state multiple' : 'error-state';

    const errorClass = this.state.errorState
      .map(({error, type}) => `${type}-${error}`) //get all errors and turn into an array of string
      .reduce((previous, current) => `${previous} ${current}`, 'location-select-error')  //turns array into big string

    return ( (this.state.fullRender) ? (
      <div className={errorClass}>
        <div className="icon-container">
          <i className="icon icon-alert-caution" />
        </div>
        <section className="error-text">
          {this.renderHeader()}
          <ul>
            {
              this.state.errorState.map((error, i) => (
                <li key={i} className={errorStateClass}> {error.message} </li>
              ))
            }
          </ul>
          {this.renderHoursHeader()}
          <ul>
            {
              this.state.errorState.map((error, i) => (
                <li className="error-hours" key={i}>
                  {this.props.showDate &&
                    <p>
                    {lang.dates(this.props, error)}
                    </p>
                  }
                  <Hours lineItems={this.getHours(error.validHours)}/>
                </li>
              ))
            }
          </ul>
        </section>
        {button}
      </div>
    ) : null);
  }
});

module.exports = LocationDateTimeError;
