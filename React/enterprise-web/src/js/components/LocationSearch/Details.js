import classNames from 'classnames';

import LocationDateTimeError from './LocationDateTimeError';
import DetailsNavigation from './DetailsNavigation';
import AfterHoursDropoff from './AfterHoursDropoff';
import GeneralInfo from './GeneralInfo';
import AboutLocation from './AboutLocation';
import Error from '../Errors/Error';

import { PARTNERS, LOCATION_SEARCH } from '../../constants';

import ReservationFlowModelController from '../../controllers/ReservationFlowModelController';
import LocationSearchController from '../../controllers/LocationSearchController';

import LocationFactory from '../../factories/LocationFactory';

const Details = React.createClass({
  getDefaultProps () {
    return {
      brandMap: {
        ...PARTNERS.ALAMOMAPPING,
        ...PARTNERS.NATIONALMAPPING
      }
    };
  },
  getInitialState () {
    return {
      showNextArrow: true,
      showPrevArrow: true,
      tab: 'about'
    };
  },
  componentDidMount () {
    $('html, body').animate({scrollTop: $('#reservationFlow').position().top}, 'fast');
    $('.location-details').removeClass('top bottom');

    if(this.refs.heading) this.refs.heading.focus();
  },
  _handleBackToResults () {
    LocationSearchController.unloadDetailsPage(this.props.type);
  },
  _handleNextLocation () {
    this.processClick('next');
  },
  _handlePrevLocation () {
    this.processClick('prev');
  },
  processClick (direction) {
    this.setState({tab: 'about'});
    LocationSearchController.loadDetailsPage(this._getNextResult(direction), this.props.type);
  },
  _getNextResult(direction) {
    let index = this.props.mapOfType.target.details.index - 1;
    let results = this.props.mapModel.results;

    if (direction === 'next') {
      index += 1;
    } else if (direction === 'prev') {
      index -= 1;
    }

    //Safety town
    index = index < 0 ? 0 : index;
    index = index > results.length - 1 ? results.length - 1 : index;

    return results[index];
  },
  _getButtonLocationId (direction) {
    return this._getNextResult(direction).peopleSoftId;
  },
  _handleSelectBrand (brand) {
    window.location.href = this.props.brandMap[brand].link;
  },
  _handleConfirmModify () {
    const current = this.props.mapOfType.target.details;
    const location = LocationFactory.getSimpleLocationModel(current, {
      locationType: LOCATION_SEARCH.STRICT_TYPE.BRANCH
    });
    LocationSearchController.setLocation(location, this.props.type);
  },
  _launchModifyConfirmationModal () {
    ReservationFlowModelController.showInflightModifyModal(true);
  },
  _onTabClick (tab) {
    const crosDetails = this.props.location.details;
    const wayfindings = crosDetails.wayfindings && crosDetails.wayfindings.length > 0 ? crosDetails.wayfindings : [];

    if (crosDetails.location_type === 'airport' && wayfindings.length > 0) {
      this.setState({tab: tab});
    }
  },
  _onMoreInfo (value) {
    const message = (value === 'dropoff' ? this.props.afterHourMessage : this.props.pickupPolicy) || i18n('resflowconfirmation_1103');
    const heading = value === 'dropoff' ? i18n('resflowconfirmation_0025') : i18n('resflowconfirmation_0026');

    enterprise.utilities.modal.open('<h2>' + heading + '<h2><p>' + message + '</p>');
  },
  _openDateTime () {
    LocationSearchController.showDatePickerModalOfTypeAndView(
      this.props.sameLocation ? 'sameLocation' : this.props.type
    );
  },
  _handleSelectLocation () {
    const current = this.props.mapOfType.target.details;
    const location = LocationFactory.getSimpleLocationModel(current, {
      locationType: LOCATION_SEARCH.STRICT_TYPE.BRANCH
    });
    if (current.attributes.indexOf(PARTNERS.EXOTICS) !== -1 && current.bookable === false) {
      window.location.href = enterprise.exoticCarsUrl;
    } else if (this.props.inflightModify.isInflight) {
      this._launchModifyConfirmationModal(true);
    } else {
      LocationSearchController.setLocation(location, this.props.type);
    }
  },
  getDetailButtons ( brand, invalidDateTime, drivingDirectionLink ) {
    return (
      <div>
      { LocationSearchController.isExternalBrand(brand) ?
        <div role="button" tabIndex="0"
             onKeyPress={a11yClick(this._handleSelectBrand.bind(null, brand))}
             onClick={this._handleSelectBrand.bind(null, brand)}
             className="btn select"
             data-dtm-tracking={LocationSearchController._dtmTracking(this._getButtonLocationId(), 'button')}>
            {i18n('resflowcarselect_0093')}
        </div>
        : [(!invalidDateTime ?
            <div role="button" tabIndex="0"
              onKeyPress={a11yClick(this._handleSelectLocation)}
              onClick={this._handleSelectLocation}
              className="btn select"
              data-dtm-tracking={LocationSearchController._dtmTracking(this._getButtonLocationId(), 'button')}>
                {i18n('resflowlocations_0021')}
            </div>
            : null)
          ,
            <a target="_blank" href={drivingDirectionLink}
               className="btn direction"
               data-dtm-tracking={LocationSearchController._dtmTracking(this._getButtonLocationId(), 'button')}>{i18n('resflowlocations_0027')}</a>
          ]
      }
      </div>
    )
  },
  afterHoursModal (e) {
    e.preventDefault();
    LocationSearchController.loadAfterHoursModal(this.props.mapOfType.target.details.peopleSoftId);
  },
  render () {
    const {mapOfType, location, sameLocation, mapModel, type, errors, deepLinkReservation,
      pickupLocationSelect, dropoffLocationSelect, dateOfType, pickupDate, dropoffDate} = this.props;

    const target = mapOfType.target;
    const details = target.details;
    const crosDetails = location.details;

    if (!crosDetails || !details) {
      return false;
    }

    const brand = details.brand;
    const isExternalBrand= LocationSearchController.isExternalBrand(brand);
    const drivingDirectionLink =
      `https://maps.google.com/?daddr=${details.latitude},${details.longitude}&hl=${enterprise.language}`;
    const invalidDateTime = !!LocationSearchController.shouldRenderDateTimeError(type);

    const searchDetailsClasses = classNames({
      'location-details-overlay': true,
      'active': target.details,
      'is-closed': invalidDateTime && !isExternalBrand
    });

    const wayfindings = crosDetails && crosDetails.wayfindings && crosDetails.wayfindings.length > 0 ? crosDetails.wayfindings : [];
    const isAfterHoursDropoff = LocationSearchController.hasAfterHoursDropoff(details);

    return (
      <div className="search-results">
        <section className={searchDetailsClasses} role="dialog" aria-labelledby="locationDetailsAriaLabel">

            <DetailsNavigation index={details.index} results={mapModel.results}
                               tracking={LocationSearchController._dtmTracking}
                               nextLocationId={this._getButtonLocationId('next')}
                               prevLocationId={this._getButtonLocationId('prev')}
                               handleNextLocation={this._handleNextLocation}
                               handlePrevLocation={this._handlePrevLocation}
                               handleBackToResults={this._handleBackToResults} />

          {invalidDateTime && !isExternalBrand &&
          <LocationDateTimeError modelController={LocationSearchController}
                                 type={sameLocation ? 'sameLocation' : type}
                                 pickupSelect={pickupLocationSelect}
                                 dropoffSelect={dropoffLocationSelect}
                                 pickupDate={pickupDate}
                                 dropoffDate={dropoffDate}
                                 locationId={mapOfType.target.details.peopleSoftId}
                                 showButton={true}
                                 showDate={true} />
          }

          <Error errors={errors} type="GLOBAL"/>

          {isAfterHoursDropoff &&
            <AfterHoursDropoff afterHoursModal={this.afterHoursModal} />
          }

          <GeneralInfo details={details}
                       detailButtons={this.getDetailButtons(brand, invalidDateTime, drivingDirectionLink)} />

          <AboutLocation tab={this.state.tab}
                         onTabClick={this._onTabClick}
                         moreInfo={this._onMoreInfo}
                         handleSelectBrand={this._handleSelectBrand}
                         openDateTime={this._openDateTime}
                         handleSelectLocation={this._handleSelectLocation}
                         deepLinkReservation={deepLinkReservation}
                         {...{deepLinkReservation, crosDetails, isAfterHoursDropoff, type,
                           wayfindings, details, mapOfType, target, isExternalBrand, dateOfType}} />

        </section>
      </div>
    );
  }
});

module.exports = Details;
