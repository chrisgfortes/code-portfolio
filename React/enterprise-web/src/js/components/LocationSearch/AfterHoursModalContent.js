export default function AfterHoursModalContent ({afterHoursMessage}) {
  return (
    <div className="modify-confirmation">
      <p>{afterHoursMessage}</p>
    </div>
  );
}
