import classNames from 'classnames';
import LocationHours from './LocationHours';

export default class AboutLocation extends React.Component {
  constructor (props) {
    super(props);
  }

  render () {
    const {type, target, deepLinkReservation, mapOfType, afterHourMessage,
      isAfterHoursDropoff, wayfindings, details, crosDetails, tab, dateOfType,
      invalidDateTime, handleSelectLocation, openDateTime, isExternalBrand, onTabClick} = this.props;

    const locationTabClasses = classNames({
      'about-location-tab': true,
      'active': tab === 'about'
    });

    const directionTabClasses = classNames({
      'directions-from': true,
      'active': tab === 'direction',
      'enabled': details.locationType === 'AIRPORT' && wayfindings.length > 0
    });

    const aboutLocationClasses = classNames({
      'about-location': true,
      'active': tab === 'about'
    });

    const pickYouUpClasses = classNames({
      'pick-you-up': true,
      'disabled': crosDetails && !crosDetails.we_will_pick_you_up
    });

    const pickYouUpIconClasses = classNames([
      'icon',
      crosDetails.we_will_pick_you_up ? 'icon-icon-checkmark-thin-green' : 'icon-icon-unavailable-gray'
    ]);

    const wayFindingClasses = classNames({
      'way-finding': true,
      'active': tab === 'direction'
    });

    const afterHoursClass = classNames({
      'location-attribute' : true,
      'disabled': !details.afterHoursDropoff
    });

    return (
      <div>
        <div className="location-tabs cf">
          <div className={locationTabClasses} role="button" tabIndex="0"
               onKeyPress={a11yClick(onTabClick.bind(null, 'about'))}
               onClick={onTabClick.bind(null, 'about')}>
            {i18n('resflowlocations_0028')}
            <div className="tab-shadow"/>
          </div>
          <div className={directionTabClasses} role="button" tabIndex="0"
               onKeyPress={a11yClick(onTabClick.bind(null, 'direction'))}
               onClick={onTabClick.bind(null, 'direction') }>
            {i18n('resflowconfirmation_0029')}
          </div>
        </div>
        <div className={aboutLocationClasses}>
          <LocationHours week={target.weeklyHours}
                         {...{mapOfType, deepLinkReservation, type, dateOfType}} />
          {crosDetails && crosDetails.intro_paragraph &&
          <div className="location-introduction">
            {crosDetails.intro_paragraph.heading &&
            <p className="heading">{crosDetails.intro_paragraph.heading}</p>
            }
            {crosDetails.intro_paragraph.text &&
            <p className="content">{crosDetails.intro_paragraph.text}</p>
            }
          </div>
          }
          {crosDetails &&
          <div className={pickYouUpClasses}>
            <i className={pickYouUpIconClasses}/>
            <span className={crosDetails.we_will_pick_you_up_text ? 'has-tip' : ''}>
              <span className="policy-label">{i18n('locations_0006')}</span>
              { crosDetails.we_will_pick_you_up_text &&
                <span className="tooltip">
                  {crosDetails.we_will_pick_you_up_text}
                  <br />
                  { afterHourMessage &&
                    <span className="policy-toggle"
                          role="button" tabIndex="0"
                          onKeyPress={ a11yClick(this.props.moreInfo.bind(null, 'pickup')) }
                          onClick={ this.props.moreInfo.bind(null, 'pickup') }>
                      ({i18n('resflowconfirmation_0025')})
                    </span>
                  }
                </span>
              }
            </span>
          </div>
          }
          <div className={afterHoursClass}>
            {isAfterHoursDropoff ?
              <i className="icon icon-after-hours"/> :
              <i className="icon icon-icon-unavailable-gray"/>
            }
            <span className="policy-label">{i18n('locations_0011')}</span>
            {afterHourMessage &&
            <span className="policy-toggle"
                  role="button" tabIndex="0"
                  onKeyPress={a11yClick(this.props.moreInfo('dropoff'))}
                  onClick={this.props.moreInfo('dropoff')}>
                      ({i18n('resflowconfirmation_0025')})
            </span>
            }
          </div>
        </div>
        <div className={wayFindingClasses}>
          { wayfindings.filter(item => item.text) // @todo - Fix `alt` attribute below (makes no sense to use `item.text` there)
                       .map((item, index) => (
              <div key={index}>
                <img alt={item.text} src={item.path}/>
                <span className="direction">{item.text}</span>
              </div>
          ))}
        </div>
        { isExternalBrand ?
            <div role="button" tabIndex="0"
                 onClick={this.props.handleSelectBrand.bind(null, details.brand)}
                 className="btn select full">
              {i18n('resflowcarselect_0093') || 'VISIT SITE'}
            </div>
            :
            <div role="button" tabIndex="0"
                 onClick={invalidDateTime ? openDateTime : handleSelectLocation}
                 className="select-location">
              {invalidDateTime ? i18n('resflowlocations_0034') : i18n('reservationnav_0004')}
            </div>
        }
      </div>
    );
  }
}

AboutLocation.displayName = 'AboutLocation';
