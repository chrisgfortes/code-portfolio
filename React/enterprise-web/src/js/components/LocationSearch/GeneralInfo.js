export default class GeneralInfo extends React.Component {
  constructor (props) {
    super(props);
  }
  render () {
    const details = this.props.details;
    return (
      <div className="general-information">
        <div className="location-index">{details.index}</div>
        <h2 id="locationDetailsAriaLabel" className="location-name" ref="heading" tabIndex="-1">
          {details.locationNameTranslation}
        </h2>
        <div className="location-address">{details.addressLines && details.addressLines[0]}</div>
        {details && details.addressLines && details.addressLines[1] &&
        <div className="location-address">{details.addressLines[1]}</div>
        }
        <div className="location-address">{details.formattedCityStateZip}</div>
        <div className="tel">
          <a href={'tel:' + details.formattedPhone}>{details.formattedPhone}</a>
        </div>
        {this.props.detailButtons}
      </div>
    );
  }
}

GeneralInfo.displayName = 'GeneralInfo';