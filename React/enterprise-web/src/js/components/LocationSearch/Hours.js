export default function Hours ({lineItems}) {
  return (
    <span className='location-hour-item'>
      {lineItems}
    </span>
  );
}
