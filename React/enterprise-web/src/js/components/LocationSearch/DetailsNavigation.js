export default class DetailsNavigation extends React.Component {
  constructor (props) {
    super(props);
  }
  render (){
    return (
      <div className="back-to-results">
        <button type="button" className="back-to-results__button"
                onClick={this.props.handleBackToResults}
                onKeyPress={a11yClick(this.props.handleBackToResults)}>
          <i role="presentation">X</i>
          {i18n('resflowlocations_0059')}
        </button>
        <div className="details-navigation">
          {this.props.index > 1 && // TODO - Add aria-label on this icon
          <button type="button" className="prev"
                  data-dtm-tracking={this.props.tracking(this.props.prevLocationId, 'button')}
                  onKeyPress={a11yClick(this.props.handlePrevLocation)} onClick={this.props.handlePrevLocation}>
            &larr;
          </button>
          }
          {this.props.index < this.props.results.length && // TODO - Add aria-label on this icon
          <button type="button" className="next"
                  data-dtm-tracking={this.props.tracking(this.props.nextLocationId, 'button')}
                  onKeyPress={a11yClick(this.props.handleNextLocation)} onClick={this.props.handleNextLocation}>
            &rarr;
          </button>
          }
        </div>
      </div>
    );
  }
}

DetailsNavigation.displayName = 'DetailsNavigation';