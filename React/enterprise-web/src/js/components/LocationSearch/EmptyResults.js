export default function EmptyResults ({label, buttonLabel, isFiltering, clearFilters}) {
  return (
    <li className="location-result-item no-result active">
      <div className="icon icon-alert-caution"/>
      <div className="content-container">
        <p>
          {label}
        </p>
        { isFiltering &&
        <a className="btn clear-filter"
           onClick={clearFilters}
           onKeyPress={a11yClick(clearFilters)}>
          {buttonLabel}
        </a>
        }
      </div>
    </li>
  );
}

EmptyResults.displayName = 'EmptyResults';