import LocationDateTimeError from './LocationDateTimeError';
import DateTimePicker from '../BookingWidget/DateTimePicker';
import LocationSearchController from '../../controllers/LocationSearchController';
import ReservationFlowModelController from '../../controllers/ReservationFlowModelController';
import LocationDateTimeErrorModalDetails from './LocationDateTimeErrorModalDetails';
import { Debugger } from '../../utilities/util-debug';

// testing save?

const LocationDateTimeErrorModal = React.createClass({
  componentWillMount() {
    this.logger = new Debugger(window._isDebug, this, true, 'LocationDateTimeErrorModal.js');
  },
  isDebug: false,
  logger: {},
  _goBack (e) {
    e.preventDefault();
    LocationSearchController.closeDateTimeErrorModal(this.props.type);
  },
  _submit (e) {
    e.preventDefault();
    if (this.props.sameLocation) {
      if (this.props.pickupSelect.invalidTime || this.props.pickupSelect.invalidDate || this.props.dropoffSelect.invalidTime || this.props.dropoffSelect.invalidDate) {
        return false;
      }
    } else if (this.props[this.props.type + 'Select'].invalidTime || this.props[this.props.type + 'Select'].invalidDate) {
      return false;
    }
    ReservationFlowModelController.changeDateTime(this.props.type);
  },
  renderError(invalidDateTime, errorType, modelController, pickupDate, dropoffDate, pickupSelect, dropoffSelect) {
    let renderInvalidDateTime = null;

    if (invalidDateTime && this.props.mapOfType.target.details) {
      this.logger.debug('calling [LocationDateTimeError]');
      renderInvalidDateTime = (
        <LocationDateTimeError
          type={this.props.sameLocation ? 'sameLocation' : this.props.type}
          narrowType={errorType}
          modalError={true}
          locationId={this.props.mapOfType.target.details.peopleSoftId}
          showButton={false}
          {...{modelController, pickupDate, dropoffDate, pickupSelect, dropoffSelect}}
        />
      );
    }
    return renderInvalidDateTime;
  },
  render () {
    this.logger.log('render()');

    const {modelController, pickupDate, dropoffDate, type, mapOfType, pickupSelect, dropoffSelect} = this.props;

    let invalidDateTime = LocationSearchController.shouldRenderDateTimeError(type);

    let pickupRender = this.renderError(invalidDateTime, 'pickup', modelController, pickupDate, dropoffDate, pickupSelect, dropoffSelect);
    let dropoffRender = this.renderError(invalidDateTime, 'dropoff', modelController, pickupDate, dropoffDate, pickupSelect, dropoffSelect);
    let details = mapOfType.target.details;

    return (
      <div className="location-date-time-modal">
        <div className="content">
          <div className="location-location">
            <LocationDateTimeErrorModalDetails goBack={this._goBack} details={details} />
          </div>
          <div className="white-date-time-container cf booking-widget">
            <DateTimePicker modelController={ReservationFlowModelController}>
              {pickupRender}
              {dropoffRender}
            </DateTimePicker>
          </div>
          <div className="modal-actions cf">
            <button className={'btn ok ' + (invalidDateTime ? 'disabled' : '')}
               onClick={this._submit}>{i18n('reservationwidget_0014')}</button>
          </div>
        </div>
      </div>
    );
  }
});

module.exports = LocationDateTimeErrorModal;
