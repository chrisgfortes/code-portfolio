export default function LocationDateTimeErrorModalDetails ({details, goBack}) {
  return (
    <div>
      <div className="location-name">
        <strong>{details && details.locationNameTranslation}</strong> - <a href="#" className="btn-change back" onClick={goBack}>{i18n('reservationnav_0029')}</a>
      </div>
      <div className="location-address">{details && details.addressLines && details.addressLines[0]}</div>
        {details && details.addressLines && details.addressLines[1] &&
          <div className="location-address">{details && details.addressLines[1]}</div>
        }
      <div className="location-address">{details && details.formattedCityStateZip}</div>
    </div>
  )
}

LocationDateTimeErrorModalDetails.displayName = "LocationDateTimeErrorModalDetails";
