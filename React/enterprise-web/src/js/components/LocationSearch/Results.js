import classNames from 'classnames';
import LocationSearchController from '../../controllers/LocationSearchController';
import ResultsHeader from './ResultsHeader';
import ResultItem from './ResultItem';
import ActiveFilters from './ActiveFilters';
import EmptyResults from './EmptyResults';

const resultsLabels = {
  header        : {
    title       : i18n('locationsearch_0001')   || 'Choose a Location',
    count       : (c) => i18n('resflowlocations_0016', {number: c}) || `${c} LOCATIONS`
  },
  activeFilters : {
    title       : i18n('locationsearch_0023')   || 'Showing Filtered Locations',
    clearButton : i18n('locationsearch_0021')   || 'Show All Locations'
  },
  emptyResults  : {
    emptyArea   : i18n('resflowlocations_0097') || `Sorry, we couldn't find a location in this area. Try broadening your search area.`,
    emptyFilter : i18n('locationsearch_0022')   || `Sorry, we couldn't find any locations that match these filters.`
  }
};

export default class Results extends React.Component {
  componentWillUpdate(nextProps, nextState) {
    if(this.props.isShowingDetails !== nextProps.isShowingDetails &&
      nextProps.isShowingDetails) {
      LocationSearchController.cacheScrollOffset.bind(null, 'details');
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if(prevProps.isShowingDetails !== this.props.isShowingDetails &&
      !this.props.isShowingDetails) {
      LocationSearchController.cleanUpScrollOffset.bind(null, 'details');
    }
  }
  componentWillUnmount() {
    LocationSearchController.cleanUpScrollOffset.bind(null, 'details');
  }
  getHeaderContent () {
    const transition = <div className="transition"/>;
    const headerInfo = [resultsLabels.header.title, <small>{resultsLabels.header.count((this.props.results || []).length)}</small>];

    return this.props.results ? headerInfo : transition;
  }
  getClassNames (results) {
    return classNames({
      'search-results-count active': true,
      'load': !results
    });
  }
  showFilterButton (results) {
    return (results && results.length > 0) ?
          <button type="button" className="search-results__active-filters-clear-button"
            onClick={this.props.clearFilters}>
            {resultsLabels.activeFilters.clearButton}
          </button>
    : null;
  }
  render () {
    const isFiltering = this.props.activeFilters.length > 0;
    const {results, activeFilters, type, isActive, highlightedLocation, clearFilters} = this.props;

    return (
      <div className="search-results">

        <ResultsHeader content={this.getHeaderContent()} classes={this.getClassNames(results)} />

        { (LocationSearchController.getActiveFilters().length !== 0) &&
        <ActiveFilters title={resultsLabels.activeFilters.title}
                       filters={activeFilters.join(", ")}
                       filterButton={this.showFilterButton(results)}/>
        }

        {results &&
          <ul>
            {results.length > 0 ?
              results.map((item, index) => {
                return (<ResultItem item={item} type={type} key={index} active={isActive}
                                                   isHighlighted={highlightedLocation === item.index} hasMoreLocations={results.length > 99}/>)
              })
              :
              <EmptyResults label={isFiltering ? resultsLabels.emptyResults.emptyFilter : resultsLabels.emptyResults.emptyArea}
                            buttonLabel={resultsLabels.activeFilters.clearButton} 
                            isFiltering={isFiltering}
                            clearFilters={clearFilters} />
            }
          </ul>
        }
    </div>);
  }
}

Results.displayName = 'Results';
