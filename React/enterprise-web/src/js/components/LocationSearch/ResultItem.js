import LocationSearchController from '../../controllers/LocationSearchController';
import classNames from 'classnames';
import { PARTNERS, LOCATION_SEARCH } from '../../constants';

const { VALIDITY } = LOCATION_SEARCH;

const timeFormatter = (time) => moment(time, 'H:mm').format('LT');
const itemLabels = {
  details            : i18n('resflowlocations_0071') || 'Details',
  requiresDateChange : i18n('locationsearch_0010')   || 'Requires travel change',
  callToAction: {
    externalBrand    : i18n('resflowcarselect_0093') || 'VISIT SITE',
    selectLocation   : i18n('resflowlocations_0021') || 'SELECT',
    adjustDateTime   : i18n('resflowlocations_0034') || 'ADJUST'
  },
  closedBar : {
    pickup           : i18n('locationsearch_0002') || 'Closed during your pick-up',
    dropoff          : i18n('locationsearch_0003') || 'Closed during your return',
    both             : i18n('locationsearch_0004') || 'Closed during your pick-up & return'
  },
  afterHours : {
    label            : i18n('locationsearch_0005') || 'After-hours return',
    tooltip          : i18n('locationsearch_0006') || 'What\'s This?',
    tooltipContent   : i18n('locationsearch_0007') || 'You are able to return your vehicle to this location even if the location is closed during your selected time. Please view the location\'s specific after-hours policy. '
  },
  toggleAlert : {
    view             : i18n('locationsearch_0008') || 'View Hours',
    hide             : i18n('locationsearch_0009') || 'Hide Hours'
  },
  closedAlert : {
    title            : i18n('locationsearch_0011') || 'Location is closed for',
    pickupTime       : i18n('locationsearch_0012') || 'your pick-up time',
    pickupDate       : i18n('locationsearch_0013') || 'your pick-up date',
    dropoffTime      : i18n('locationsearch_0014') || 'your return time',
    dropoffDate      : i18n('locationsearch_0015') || 'your return date',
    closedDay        : i18n('locations_0020')      || 'Closed',
    openHoursTitle   : (date) => i18n('locationsearch_0016', {locationDate: date}) || `Hours for ${date}`,
    openHours        : (openingHour, closingHour) => `${timeFormatter(openingHour)} - ${timeFormatter(closingHour)}`
  }
};

const ResultItem = React.createClass({
  getInitialState () {
    return {
      hover: false,
      showingClosedAlert: false
    };
  },
  componentWillMount () {
    this.callToActionOptions = {
      externalBrand: {
        classes: 'btn ok',
        label: itemLabels.callToAction.externalBrand,
        action: this._visitExternalBrand.bind(this, this.props.item.bookingUrl)
      },
      selectLocation: {
        classes: 'btn ok',
        label: itemLabels.callToAction.selectLocation,
        action: this._selectLocation
      },
      adjustDateTime: {
        classes: 'btn ok date-time-adjust',
        label: itemLabels.callToAction.adjustDateTime,
        action: this._adjustDateTime
      }
    };
  },
  componentWillReceiveProps (nextProps) {
    const isUnhighlighting = this.props.isHighlighted !== nextProps.isHighlighted
                          && !nextProps.isHighlighted;
    if (isUnhighlighting && !ResponsiveUtil.isMobile()) {
      this.setState({
        showingClosedAlert: false
      });
    }
  },
  onMouseInteraction (isEntering) {
    if (!ResponsiveUtil.isMobile()) {
      this.setState({
        hover: isEntering,
        showingClosedAlert: isEntering
      });
      LocationSearchController.setHighlightLocation(isEntering && this.props.item.index, true);
    }
  },
  onToggleClosedAlert () {
    if (ResponsiveUtil.isMobile()) {
      const newToggleState = !this.state.showingClosedAlert;
      this.setState({
        showingClosedAlert: newToggleState
      });
    }
  },
  _onDetailClick () {
    LocationSearchController.loadDetailsPage(this.props.item, this.props.type);
  },
  _visitExternalBrand (url) {
    LocationSearchController.callDomainRedirect(url);
  },
  _selectLocation () {
    let current = this.props.item;

    if (current.attributes.indexOf(PARTNERS.EXOTICS) !== -1 && current.bookable === false) {
      window.location.href = enterprise.exoticCarsUrl;
    } else {
      LocationSearchController.selectLocationAndCheckTimeValidity(this.props.item, this.props.type);
    }
  },
  _adjustDateTime () {
    LocationSearchController.loadDetailsPage(this.props.item, this.props.type, true, true);
    LocationSearchController.cacheScrollOffset('adjust-modal');
  },
  afterHoursModal (e) {
    e.preventDefault();
    LocationSearchController.loadAfterHoursModal(this.props.item.peopleSoftId);
  },
  renderCallToAction (isLocationClosed) {
    const options = this.callToActionOptions;
    const buttonType = isLocationClosed ? 'modal' : 'button';
    let currentOptions;
    if ( LocationSearchController.isExternalBrand(this.props.item.brand) ) {
      currentOptions = options.externalBrand;
    } else if (isLocationClosed) {
      currentOptions = options.adjustDateTime;
    } else {
      currentOptions = options.selectLocation;
    }
    return (
      <div className="location-result-item__select-wrapper">
        <button type="button" className={currentOptions.classes}
                onClick={currentOptions.action} onKeyPress={a11yClick(currentOptions.action)}
                data-dtm-tracking={LocationSearchController._dtmTracking(this.props.item.peopleSoftId, buttonType)}>
          {currentOptions.label}
        </button>
        { isLocationClosed && !LocationSearchController.isExternalBrand(this.props.item.brand) &&
          <span className="location-result-item__date-change-label">{itemLabels.requiresDateChange}</span>
        }
      </div>);
  },
  renderClosedLocationBar (locationStatus) {
    let closedLabel = '';
    switch (locationStatus) {
      case VALIDITY.PICKUP_INVALID  : closedLabel = itemLabels.closedBar.pickup;  break;
      case VALIDITY.DROPOFF_INVALID : closedLabel = itemLabels.closedBar.dropoff; break;
      case VALIDITY.BOTH_CLOSED    : closedLabel = itemLabels.closedBar.both;    break;
      default: break;
    }

    let toggleHoursLabel = '';
    let toggleHoursIcon = <i className="icon icon-nav-carrot"/>;
    if (ResponsiveUtil.isMobile()) {
      toggleHoursLabel = ' - ' + itemLabels.toggleAlert[this.state.showingClosedAlert ? 'hide' : 'view'];
      toggleHoursIcon = this.state.showingClosedAlert ? <i className="icon icon-nav-carrot-up-black"/>
                                                      : <i className="icon icon-nav-carrot-down-black"/>;
    }
    return (
      <div className="location-result-item__closed-bar"
           onClick={this.onToggleClosedAlert}
           onKeyPress={a11yClick(this.onToggleClosedAlert)}>
        <span className="location-result-item__closed-label">{ [closedLabel, toggleHoursLabel, toggleHoursIcon] }</span>
      </div>
    );
  },
  renderClosedLocationAlert () {
    const labels = itemLabels.closedAlert;
    const details = LocationSearchController.getLocationDateTimeDetails(this.props.item, this.props.type);
    const isClosed = type => details[type].isClosed;
    const getRenderedDetails = type => ({
      status : this.renderClosedState(type, details[type].hours.length > 0),
      hours  : this.renderClosedHours(details[type])
    });
    const aggregateRenderedDetails = (result, elem) => {
      result.status.push(elem.status);
      result.hours.push(elem.hours);
      return result;
    };
    const initialResult = {status: [], hours: []};
    const {status, hours} = Object.keys(details)
                                  .filter(isClosed)
                                  .map(getRenderedDetails)
                                  .reduce(aggregateRenderedDetails, initialResult);

    const containerClasses = classNames({
      'location-result-item__closed-hours': true,
      'is-showing': this.state.showingClosedAlert
    });
    return (
      <div className={containerClasses}>
        <h4 className="location-result-item__closed-hours-title">{labels.title}</h4>
        <ul className="location-result-item__closed-status">
          {status}
        </ul>
        <ul className="location-result-item__open-hours">
          {hours}
        </ul>
      </div>
    );
  },
  renderClosedState (type, hasOpenHours) {
    return (<li className="error-state">
              {itemLabels.closedAlert[type + (hasOpenHours ? 'Time' : 'Date')]}
            </li>);
  },
  renderClosedHours (closedDetails) {
    const labels = itemLabels.closedAlert;
    const openDay = (hours) => (<span className="location-hour">{labels.openHours(hours.open, hours.close)}</span>);
    const closedDay = <span className="location-hour">{labels.closedDay}</span>;

    const openHours = closedDetails.hours.length > 0 ? closedDetails.hours : [null];
    return (<li className="error-hour">
              <div>{labels.openHoursTitle(closedDetails.date)}</div>
              {openHours.map( h => h ? openDay(h) : closedDay )}
            </li>);
  },
  getLocationTypeIconClasses () {
    const item = this.props.item;
    const exotics = LocationSearchController.getExoticTypeForLocation(item);

    return classNames({
      'icon location-type-icon': true,
      'icon-ico-motorcycle': exotics && exotics === PARTNERS.MOTORCYCLES,
      'icon-icon-exotics': exotics && exotics === PARTNERS.EXOTICS,
      //If airports are more important than exotics and moto, remove bit at end
      ['icon-location-' + item.locationType.toLowerCase()]: item.locationType !== 'CITY' && !LocationSearchController.isExternalBrand(this.props.item.brand),
      ['icon-brand-' + item.brand.toLowerCase()]: LocationSearchController.isExternalBrand(this.props.item.brand)
    });
  },
  renderAfterHours () {

    if(LocationSearchController.hasAfterHoursDropoff(this.props.item)) {
      return (
        <div className="location-after-hours">
          <i className="icon icon-after-hours"/>
          <span className="location-after-hours_cta">  {itemLabels.afterHours.label} </span>
          <span className="location-after-hours_tooltip" tabIndex="0" aria-describedby="location-after-hours_tooltip-content">
            ({itemLabels.afterHours.tooltip})
            <span id="location-after-hours_tooltip-content" className="location-after-hours_tooltip-content" role="tooltip">
              <div>
                {itemLabels.afterHours.tooltipContent}
                <a href="#" onClick={this.afterHoursModal}>{i18n('locationsearch_0027') ||
                'After-Hours Policy'}</a>
              </div>
            </span>
          </span>
        </div>
      )
    }
  },
  render () {
    const {active, item, type, isHighlighted, hasMoreLocations} = this.props;
    if (!active || !item) {
      return false;
    }

    const locationStatus = LocationSearchController.getLocationStatus(item, type);

    const isLocationClosed = locationStatus !== 'OPEN';

    const listItemClasses = classNames({
      'location-result-item': true,
      'is-closed': ( isLocationClosed && !LocationSearchController.isExternalBrand(item.brand) ),
      'active': active,
      'highlight': isHighlighted,
      'is-hovering': this.state.hover
    });

    const listItemIndexClass = classNames({
      'country-location-index': hasMoreLocations,
      'location-index': true
    })


    return (
      <li className={listItemClasses}
          onMouseEnter={this.onMouseInteraction.bind(this, true)}
          onMouseLeave={this.onMouseInteraction.bind(this, false)}
          onClick={Modernizr.touchevents && this.onMouseInteraction.bind(this, true)}>
        <div className={listItemIndexClass}>{item.index}</div>
        <div className="location-result-item__main-content">
          <div className="location-information">
            { item.locationNameTranslation &&
              <div className="location-name">
                <h3>{item.locationNameTranslation}</h3>
                <i className={this.getLocationTypeIconClasses()}/>
              </div>
            }
            { item.addressLines.map((line, i) => <div key={i} className="location-address">{line}</div>) }
            <div className="location-address">{item.formattedCityStateZip}</div>
          </div>
          <div className="btn-grp cf">
            <div role="button" tabIndex="0" className="green-action-text location-detail-toggle"
                 onClick={this._onDetailClick} onKeyPress={a11yClick(this._onDetailClick)}
                 data-dtm-tracking={LocationSearchController._dtmTracking(item.peopleSoftId, 'drawer')}
            >
              {itemLabels.details} <i className="icon icon-nav-carrot-green"/>
            </div>
            { this.renderCallToAction(isLocationClosed) }
          </div>
          { this.renderAfterHours() }
        </div>
        { isLocationClosed && !LocationSearchController.isExternalBrand(item.brand) && [
          this.renderClosedLocationBar(locationStatus),
          this.renderClosedLocationAlert()
        ]
        }
      </li>
    );
  }
});

module.exports = ResultItem;
