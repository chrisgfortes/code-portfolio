export default function ResultsHeader ({content, classes}) {
  return (
    <h1 className={classes}>
      {Array.isArray(content) ? content.map((item, i) => <span key={i}>{item}</span>) : content}
    </h1>
  );
}

ResultsHeader.displayName = 'ResultsHeader';