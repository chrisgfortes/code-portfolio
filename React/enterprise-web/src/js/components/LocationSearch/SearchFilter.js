import classNames from 'classnames';

const filterLabels = {
  filterMobileToggle :            i18n('resflowlocations_0053')                  || 'FILTER',
  filterTitle        :            i18n('locationsearchfilter_0001')              || 'Filter by',
  openForMyTimes     :            i18n('locationsearchfilter_0002')              || 'Open for my times'
};

function onOpenForMyTimesChanged (event) {
  const isChecked = event.target.checked;
  this.props.onOpenForMyTimesChanged(isChecked);
}
function onMobileToggle () {
  this.setState({ isMobileExpanded: !this.state.isMobileExpanded });
}

function getWrapperClasses (isMobileExpanded, isActive) {
  return classNames({
    'location-search-filter': true,
    'mobile-expanded': isMobileExpanded,
    'is-active': isActive
  });
}

export default class SearchFilter extends React.Component {
  constructor (props) {
    super(props);
    const suffixId = _.uniqueId();
    this.state = {
      isMobileExpanded: false,
      containerId: `location-search-filter-${suffixId}`,
      openForMyTimesId: `location-search-filter-open-for-my-times-${suffixId}`
    };
  }
  getMobileHeader () {
    const hasResults = typeof this.props.resultsCount === 'number';
    const mobileHeaderClasses = classNames({
      'location-search-filter__mobile-header': true,
      'load': !hasResults
    });
    const toggleIconClasses = classNames({
      'icon': true,
      'icon-nav-carrot-down': !this.state.isMobileExpanded,
      'icon-nav-carrot-up-green': this.state.isMobileExpanded
    });
    const transition = <h3 className={ mobileHeaderClasses }><div className="transition"/></h3>;
    const header = (
      <h3 className={ mobileHeaderClasses }>
        { i18n('resflowlocations_0016', {number: this.props.resultsCount}) }
        <button className="location-search-filter__mobile-toggle-button"
                onClick={ onMobileToggle.bind(this) }
                aria-controls={ this.state.containerId } aria-expanded={ this.state.isMobileExpanded }>
          { filterLabels.filterMobileToggle }
          <i className={ toggleIconClasses }/>
        </button>
      </h3>);
    return hasResults ? header : transition;
  }
  render () {
    return (
      <div className={ getWrapperClasses(this.state.isMobileExpanded, this.props.isActive) }>
        { this.getMobileHeader() }
        <div id={ this.state.containerId } className="location-search-filter__container">
          <h4 className="location-search-filter__heading">
            { filterLabels.filterTitle }
          </h4>
          <input type="checkbox" id={ this.state.openForMyTimesId } ref="openForMyTimes"
                 className="location-search-filter__open-for-my-times-input"
                 checked={ this.props.filters.openForMyTimes } onChange={ onOpenForMyTimesChanged.bind(this) }/>
          <label className="location-search-filter__open-for-my-times-label"
                 htmlFor={ this.state.openForMyTimesId }>
            { filterLabels.openForMyTimes }
          </label>
        </div>
      </div>);
  }
}

SearchFilter.defaultProps = {
  filters: {},
  resultsCount: null,
  onOpenForMyTimesChanged: (isChecked) => isChecked
};

SearchFilter.displayName = 'SearchFilter';
