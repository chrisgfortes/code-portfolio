import LocationSearch from '../BookingWidget/LocationSearch';
import LocationSearchController from '../../controllers/LocationSearchController';

function _handleSelect (selected, type) {
  LocationSearchController.clearAllResults(type);
  LocationSearchController.selectLocation(selected, type);
}

//this is currently not in use...
function _handleNearbyLocations (lat, lng, type) {
  /*LocationSearchController.clearAllResults(type);
  const center = {
    latitude: lat,
    longitude: lng
  };

  LocationSearchController.spatialSearch(center, type);
  ReservationActions.clearLocationResults(type);*/
}

export default function Header ({isFedex, type}) {
  return (
    <div className="location-search cf g g-2up">
      <div className="gi">
        <LocationSearch
            isLocationHeaderSearch
            isFedexReservation={isFedex}
            _handleSelect={_handleSelect.bind(this)}
            _handleNearbyLocations={_handleNearbyLocations}
            type={type} />
      </div>
    </div>
  );
}

