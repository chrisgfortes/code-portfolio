export default function IncludedItemRenderer ({itemName}) {
  return (
    <span className="included-item">
      { itemName }
    </span>
  );
}

IncludedItemRenderer.displayName = 'IncludedItemRenderer';

