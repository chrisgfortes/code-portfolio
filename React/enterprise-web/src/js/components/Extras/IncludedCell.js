import { PAGEFLOW } from '../../constants';

/**
 * @class IncludedCell
 * @type React.class
 * @desc Table cell to display included/mandatory label and icon
 *
 */
export default function IncludedCell ({status, notBookable, purchase_message}) {
  return (
    (notBookable) ?
      <div className="included-cell">
        <div className="sub-label">
          {purchase_message ? purchase_message
            : enterprise.i18nReservation.resflowextras_0014}
        </div>
      </div> :
      <div className="included-cell">
        <span className="label">{status === PAGEFLOW.INCLUDED ? enterprise.i18nReservation.reservationnav_0018 : status}</span>
        <i className="icon icon-forms-checkmark"></i>
      </div>
  );
}

IncludedCell.displayName = 'IncludedCell';
