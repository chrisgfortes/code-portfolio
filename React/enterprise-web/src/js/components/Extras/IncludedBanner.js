import IncludedItemRenderer from './IncludedItemRenderer';

/**
 * @class IncludedBanner
 * @type React.class
 * @desc Component to display included/mandatory items in a banner
 *
 */

export default class IncludedBanner extends React.Component {
  constructor (props) {
    super(props);
  }
  render () {
    const {mandatoryExtras, includedExtras, includedClass, mandatoryClass} = this.props;

    const getItemBanner = (list) => {
      return (list ? (
        list.map((extra, index) => {
          return (
            <IncludedItemRenderer
              key={index}
              itemName={`${(index > 0 ? ',' : '')} ${extra.name}`}
            />
          )
        })
      ) : null)
    }

    return (
      <div className="included-alert active">
        <div className={mandatoryClass}>
          <span>{i18n('resflowextras_0004')}</span>
          {getItemBanner(mandatoryExtras)}
        </div>

        <div className={includedClass}>
          <span>{i18n('resflowextras_0003')}</span>
          {getItemBanner(includedExtras)}
        </div>
      </div>
    );
  }
}

IncludedBanner.defaultProps = {
  includedExtras: null,
  mandatoryExtras: null
};

IncludedBanner.displayName = 'IncludedBanner';

