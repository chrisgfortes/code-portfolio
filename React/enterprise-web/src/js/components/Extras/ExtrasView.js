import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import ReservationCursors from '../../cursors/ReservationCursors';
import ExtrasTable from './ExtrasTable';
import IncludedBanner from './IncludedBanner';
import UpgradeBanner from './UpgradeBanner';
import Modals from './Modals';
import classNames from 'classnames';
import PrePopulatedVehicle from './PrePopulatedVehicle';
import ExtrasController from '../../controllers/ExtrasController';
import ResController from '../../controllers/ReservationFlowModelController';
import { EXTRAS } from '../../constants';

/**
 * @class ExtrasView
 * @type React.class
 * @desc Base component for extras view
 *
 */
const ExtrasView = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  getDefaultProps: function () {
    return {
      modelController: null
    };
  },
  componentDidMount: function () {
    if (this.state.blockSubmit) {
      ReservationStateTree.select(ReservationCursors.extrasView).set('blockSubmit', false);
    }
  },
  cursors: {
    includedExtras: ReservationCursors.includedExtras,
    mandatoryExtras: ReservationCursors.mandatoryExtras,
    requiredExtras: ReservationCursors.requiredExtras,
    equipment: ReservationCursors.equipmentExtras,
    insurance: ReservationCursors.insuranceExtras,
    blockSubmit: ReservationCursors.blockSubmit,
    deepLinkReservation: ReservationCursors.deepLinkReservation,
    pickupCountryCode: ReservationCursors.pickupCountryCode,
    errors: ReservationCursors.extrasError,
    upgraded: ReservationCursors.vehicleUpgraded,
    exclusionExtrasView: ReservationCursors.keyFactsPoliciesFromReservation,
    exclusionExtrasModal: ReservationCursors.exclusionExtrasModal,
    extrasView: ReservationCursors.extrasView,
    upgrades: ReservationCursors.availableUpgrades,
    chargeType: ReservationCursors.chargeType,
    selectedCarClassDetails: ReservationCursors.selectedCar,
    existingReservationConfirmationNumber: ReservationCursors.existingReservationConfirmationNumber
  },
  _onChange () {
    ResController.callGoToReservationStep('cars');
  },
  upgradeHandler (event) {
    event.preventDefault();
    ExtrasController.upgrade_Handler(event.currentTarget.getAttribute('data-code'),
      this.state.selectedCarClassDetails);
  },
  buildObjectClass(list) {
    return {
      "included-container": true,
      "visible": (list.length > 0)
    }
  },
  getClassNames(type) {
    const {
      blockSubmit,
      includedExtras,
      mandatoryExtras
    } = this.state;

    let classNamesObj = {
      btn: true,
      disabled: blockSubmit
    };

    if (type === EXTRAS.STATUS.INCLUDED){
      classNamesObj = this.buildObjectClass(includedExtras);
    } else if (type === EXTRAS.STATUS.MANDATORY){
      classNamesObj = this.buildObjectClass(mandatoryExtras);
    }

    return classNames(classNamesObj);
  },
  render () {
    const {
      includedExtras, mandatoryExtras, requiredExtras,
      insurance, equipment, blockSubmit,
      deepLinkReservation, pickupCountryCode, errors,
      exclusionExtrasModal, exclusionExtrasView, extrasView,
      upgrades, selectedCarClassDetails, chargeType, upgraded
    } = this.state;

    const { deepLinkErrors } = this.props;
    const showPrePopulatedVehicle = _.get(deepLinkReservation, 'stepStatus.ADD_ON_SELECT');

    return (
      <div className="extras-view">
        <div className="extras-header cf">
          <h1 className="alpha">{i18n('resflowextras_0001')}</h1>
          <div className="cta-container">
            <div className={blockSubmit ? 'loading' : ''}/>
            <button
              className={this.getClassNames()}
              id="extrasSubmitTop"
              onClick={() => ExtrasController.reviewButtonClicked_Handler(blockSubmit)}>
                {i18n('resflowextras_0002')}
            </button>
          </div>
        </div>

        {deepLinkErrors}

        {includedExtras.length + mandatoryExtras.length > 0 &&
          <IncludedBanner
            includedExtras={includedExtras}
            mandatoryExtras={mandatoryExtras}
            includedClass={this.getClassNames(EXTRAS.STATUS.INCLUDED)}
            mandatoryClass={this.getClassNames(EXTRAS.STATUS.MANDATORY)}
          />
        }

        {ExtrasController.hasUpgradeBanner(chargeType, selectedCarClassDetails, upgrades, upgraded) &&
          <UpgradeBanner
            upgradeHandler={this.upgradeHandler}
            bannerContent={
              ExtrasController.upgradeBannerContent(
                _.head(upgrades),
                chargeType,
                selectedCarClassDetails
              )
            }
          />
        }

        <div className="extras-content">
          {(showPrePopulatedVehicle && selectedCarClassDetails) &&
            <PrePopulatedVehicle
              carDetails={selectedCarClassDetails}
              onChange={this._onChange}
            />
          }
          {(insurance && insurance.length > 0) &&
            <ExtrasTable
              type="insurance"
              extras={insurance}
              title={i18n('resflowextras_0009')}
              {...{
                exclusionExtrasView,
                exclusionExtrasModal,
                errors,
                pickupCountryCode,
                requiredExtras,
                extrasView
              }}
            />
          }
          {(equipment && equipment.length > 0) &&
            <ExtrasTable
              type="equipment"
              extras={equipment}
              title={i18n('resflowextras_0006')}
              {...{
                exclusionExtrasView,
                exclusionExtrasModal,
                errors,
                pickupCountryCode,
                extrasView
              }}
            />
          }

          <div className="cta-container">
            <div className={blockSubmit ? 'loading' : false}/>
            <button
              className={this.getClassNames()}
              id="extrasSubmitBottom"
              onClick={() => ExtrasController.reviewButtonClicked_Handler(blockSubmit)}>
                {i18n('resflowextras_0002')}
            </button>
          </div>
        </div>

        <Modals
          reviewButtonClicked_Handler={
            () => ExtrasController.reviewButtonClicked_Handler(blockSubmit)
          }
        />
      </div>
    );
  }
});

module.exports = ExtrasView;
