import classNames from 'classnames';
import ExtrasController from '../../controllers/ExtrasController';

export default class AddRemove extends React.Component {
  render () {
    const {index, type, selected, code, currentCursor, selectedItem} = this.props;
    const select = !selectedItem.selected;

    let className = classNames({
      'add-remove': true,
      'remove': selectedItem.selected,
      'add': !selectedItem.selected
    });
    return (
      <div className={className}
           data-code={code}
           data-type={type}>
        {
          <div>
            <a role="button" tabIndex="0"
               data-action={selected ? 'remove' : 'add'}
               onClick={(e) => {ExtrasController.addRemoveExtraClicked_Handler(e, index, currentCursor, select)}}
               onKeyPress={a11yClick((e) => {ExtrasController.addRemoveExtraClicked_Handler(e, index, currentCursor, select)})}>
              <i aria-hidden="true" role="presentation" className={selected ? 'icon icon-remove' : 'icon icon-add'}></i>

              <div className="label">
                {selected ? i18n('resflowextras_0011') : i18n('resflowextras_0010')}
              </div>
            </a>
          </div>
        }
      </div>
    );
  }
}

AddRemove.defaultProps = {
  type: null,
  selected: false
};

AddRemove.displayName = 'AddRemove';
