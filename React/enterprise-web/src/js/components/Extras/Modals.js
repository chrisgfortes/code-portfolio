import ReservationCursors from '../../cursors/ReservationCursors';
import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import GlobalModal from '../Modal/GlobalModal';
import LimitedExtrasModal from '../Modal/LimitedExtrasModal';
import ExtrasReminderModal from '../Modal/ExtrasReminderModal';
import ExtraDetailsModalContent from './ExtraDetailsModalContent';
import ExtrasController from '../../controllers/ExtrasController';

const Modals = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    requiredExtras: ReservationCursors.requiredExtras,
    insurance: ReservationCursors.insuranceExtras,
    highlightedExtra: ReservationCursors.highlightedExtra,
    requestModal: ReservationCursors.requestModal,
    showExtrasProtectionModal: ReservationCursors.showExtrasProtectionModal,
    showExtraDetailsModal: ReservationCursors.showExtraDetailsModal,
    pickupCountryCode: ReservationCursors.pickupCountryCode,
    requestModalModal: ReservationCursors.requestModalModal
  },
  render () {
    const {requiredExtras, insurance, highlightedExtra, requestModal, showExtrasProtectionModal,
      showExtraDetailsModal, pickupCountryCode} = this.state;

    const modalState = (requestModal.modal
          && (requestModal.origin === 'extras'
          || requestModal.origin === 'vehicle_extras'));

    return (
      <div>
        {modalState &&
        <GlobalModal active={requestModal.modal}
                     header={i18n('resflowextras_0022')}
                     content={<LimitedExtrasModal confirm={this.props.reviewButtonClicked_Handler} />}
                     classLabel="Limited-Extras-Modal"
                     cursor={ReservationCursors.requestModalModal}/>
        }
        {showExtrasProtectionModal &&
        <GlobalModal close={ExtrasController.submitExtras}
                     header=" "
                     content={<ExtrasReminderModal goToReview={() => ExtrasController.submitExtras()}
                                                   requiredExtrasIds={requiredExtras}
                                                   pickupCountryCode={pickupCountryCode}
                                                   extras={insurance}/>}
                     classLabel="extras-reminder-modal"
                     cursor={ReservationCursors.showExtrasProtectionModal} />
        }
        {showExtraDetailsModal &&
        <GlobalModal onClosing={ExtrasController.clearHighlightedExtra}
                     header=" "
                     content={<ExtraDetailsModalContent extra={highlightedExtra}/>}
                     classLabel="extras-details-modal"
                     cursor={ReservationCursors.showExtraDetailsModal} />
        }
      </div>
    );
  }
});

export default Modals;
