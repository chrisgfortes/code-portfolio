import classNames from 'classnames';
import ExtrasRow from './ExtrasRow';
import Error from '../Errors/Error';
import CorporateModals from '../Corporate/ActionModals';
import ExtrasController from '../../controllers/ExtrasController';
import { PAGEFLOW } from '../../constants';

function onDetailsClick (index, showDetails, event) {
  event.preventDefault();
  this.setState({
    showDetails: index === showDetails ? null : index
  });
}

function getExtrasName (extras, requiredExtras) {
  return extras.filter( extra => requiredExtras.includes(extra.code) && !extra.selected)
    .map( extra => (
      <li>
        {`${extra.name} (${extra.code}) - `}
        <button className="required-extras-details"
                onClick={()=> {ExtrasController.openExtraDetailsModal(extra)}}>
          {i18n('resflowcarselect_0010')}
        </button>
      </li>
    ));
}

export default class ExtrasTable extends React.Component {
  constructor (props) {
    super(props);
    this.state = { showDetails: null }
  }
  render () {
    const {requiredExtras, pickupCountryCode, extras, title, type, errors, exclusionExtrasModal, exclusionExtrasView, extrasView} = this.props;
    const {showDetails} = this.state;
    const requiredExtrasDesc = i18n({key: 'LAC_protection_0005', countryCode: pickupCountryCode}) || 'The purchase of the protection(s) listed below will be required at the counter, unless you show valid proof of coverage:';
    const requiredExtrasName = getExtrasName( extras, requiredExtras );
    let requiredExtrasClasses = classNames({
      'cf required-extras': true,
      'no-protections': !requiredExtrasName.length
    });
    return (
      <div className="extras-container">
        <h2 className="extras-title beta">
          {title}
        </h2>
        {/*Todo: Remove below when EMA-7664 is resolved*/}
        <i className="disclaimer">
          {type === PAGEFLOW.INSURANCE && i18n('resflowextras_0021')}
        </i>

        <Error type="EXTRAS" errors={errors}/>
        {!!(type === PAGEFLOW.INSURANCE && requiredExtras.length) &&
          <div className={requiredExtrasClasses}>
            <i className="icon icon-icon-shield-white"/>
            <div className="required-extras-content">{requiredExtrasDesc}</div>
            <div className="required-extras-list"><ul>{requiredExtrasName}</ul></div>
          </div>
        }
        <table className="extras-section" role="grid">
          {extras.map((extra, index) =>
            <ExtrasRow details={index === showDetails}
              key={`${index}${extra.code}`}
              onDetailsClick={onDetailsClick.bind(this, index, showDetails)}
              {...{exclusionExtrasView, exclusionExtrasModal, requiredExtras, extra, index, type, extrasView}}/>
          )}
        </table>
        <CorporateModals />
      </div>
    );
  }
}

ExtrasTable.defaultProps = {
  extras: [],
  requiredExtras: []
};

ExtrasTable.displayName = 'ExtrasTable';
