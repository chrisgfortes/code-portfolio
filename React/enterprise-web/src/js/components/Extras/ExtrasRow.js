import classNames from 'classnames';
import Resize from '../../utilities/util-resize';
import ControlCell from './ControlCell';
import IncludedCell from './IncludedCell';
import GlobalModal from '../Modal/GlobalModal';
import ExtrasExclusionModal from '../Modal/ExtrasExclusionModal';
import ExtrasController from '../../controllers/ExtrasController';
import { PAGEFLOW } from '../../constants';

/**
 * @name Icon Dictionary
 * @type Object {{extra.code: selector}}
 * @desc Lookup table for mapping extras code to an icon
 */
const iconDictionary = {
  'COF': 'eco-offset',       // Greenhouse Gas Emissions Offset
  'CST': 'childseat',        // Child Seat
  'GPS': 'gps',              // GPS
  'TPD': 'toll',             // Toll Pass Device
  'TPW': 'toll',             // Toll Pass Device
  'SKV': 'skirack',          // Ski Rack
  'COF_WHITE': 'eco-offset-white',
  'CST_WHITE': 'childseat-white',
  'GPS_WHITE': 'gps-white',
  'TPD_WHITE': 'toll-white',
  'TPW_WHITE': 'toll-white',
  'SKV_WHITE': 'skirack-white',
  'DEFAULT': '2',
  get: function (code) {
    let retVal;
    if (typeof this[code] === 'undefined') {
      retVal = this['DEFAULT'];
    } else {
      retVal = this[code];
    }
    return retVal;
  }
};

function getIconClass(type, extra) {
  if (type !== PAGEFLOW.INSURANCE) {
    const baseIconClass = 'icon icon-addon-';
    const dynamicIconClass = (extra.selected && !extra.isIncluded) ?
                              iconDictionary.get(extra.code + '_WHITE') :
                              iconDictionary.get(extra.code);

    return baseIconClass + dynamicIconClass;
  }
}

function getClassNames (type, data) {
  return type === 'summary' ?
    classNames({
      'extras-row_summary cf': true
    })
    : type === 'row' ?
    classNames({
      'extras-row cf': true,
      'included': data.isIncluded,
      'details': data.details,
      'selected': !data.extra.notWebBookable && data.extra.selected
    })
    : type === 'exclusion' ?
    classNames({
      'extras-exclusion-name': data.extra.lrd_policy_code,
      'extras-remove-space': data.exclusionExtra
    })
    :
    null
    ;
}

/**
 * @class ExtrasRow
 * @type React.class
 * @desc Row Template for @ExtrasTable
 */
export default class ExtrasRow extends React.Component {
  componentDidMount () {
    Resize.listen();
  }
  render () {
    const {exclusionExtrasView, exclusionExtrasModal, requiredExtras,
            extra, index, type, details, onDetailsClick, extrasView} = this.props;

    const detailsText = i18n('resflowextras_0027', {rowName: extra.name}) || (extra.name + ' Details');
    const isIncluded = extra.isIncluded || extra.isMandatory;
    const exclusionExtra = (exclusionExtrasView && extra.lrd_policy_code && extra.lrd_policy_code.length > 0) ?
                              _.get(exclusionExtrasView.find((policy) => extra.lrd_policy_code === policy.code), 'policy_exclusions[0]') : null;
    const isRequiredExtra = requiredExtras ? requiredExtras.some(extraId => extraId === extra.code) : false;

    return (
      <tbody className={getClassNames('row', {isIncluded, details, extra}) + ' ' + type}>
        <tr className={getClassNames('summary')}>
          <th scope="row" className={'extras-row_name cell pad-top ' + getClassNames('exclusion', {extra, exclusionExtra})} id={extra.code + 'Header'}>
            <div className="extras-cell_icon cell" aria-hidden="true">
              {(type === 'equipment') ? <i className={getIconClass(type, extra)}></i> : <span> </span>}
              {!!isRequiredExtra &&
                <i className="icon icon-icon-shield-black"></i>
              }
            </div>
            {extra.name}
            {!!isRequiredExtra &&
             <span className="required-extra-suggestion">{i18n('LAC_protection_0006') || 'Suggested'}</span>
           }
          </th>
          { exclusionExtra &&
          <td className="extras-row_exclusion-btn cell pad-top">
            <a role="button" tabIndex="0"
               onClick={ExtrasController.openExclusionExtrasModal.bind(null, extra.lrd_policy_code)}
               aria-controls={extra.code + 'AriaDescription'}>
              {i18n('keyfacts_0046') || 'View Exclusions'}
            </a>
          </td>
          }
          <td className="extras-row_rate cell pad-top">
            {extra.formattedRateAmount}
          </td>
          <td className="extras-row_max cell pad-top">
            {extra.formattedMaxAmount}
          </td>
          <td className="extras-row_details-btn cell pad-top">
            <a role="button" tabIndex="0"
               onClick={onDetailsClick}
               onKeyPress={a11yClick(onDetailsClick)}
               aria-controls={extra.code + 'AriaDescription'}
               aria-expanded={details}
               aria-label={detailsText}>
              {i18n('resflowcarselect_0010').toUpperCase()}
            </a>
          </td>
          <td className="extras-row_action cell">
            {
              (extra.isIncluded || extra.isMandatory || extra.notWebBookable) ?
                <IncludedCell status={extra.status}
                              notBookable={extra.status === PAGEFLOW.NOT_WEB_BOOKABLE}
                              purchase_message={
                                (type === 'equipment') ?
                                  extra.equipment_purchase_message
                                  : extra.protection_purchase_message}/> :
                <ControlCell maxQuantity={extra.max_quantity}
                             selectedQuantity={extra.selected_quantity}
                             selected={extra.selected}
                             code={extra.code}
                             {...{index, type, extrasView}}/>
            }
          </td>
        </tr>
        <tr className={'extras-row_details ' + (details ? 'show' : '')}
            aria-hidden={details ? 'false' : 'true'}
            id={extra.code + 'AriaDescription'}>
          <td headers={extra.code + 'Header'} colSpan="5">
            <p>
              {extra.detailed_description || extra.description}
            </p>
          </td>
          {exclusionExtrasModal.modal &&
            <GlobalModal active={exclusionExtrasModal.modal}
                       header={i18n('keyfacts_0051') || 'Exclusions'}
                       content={<ExtrasExclusionModal/>}
                       cursor={['view', 'extras', 'exclusionModal', 'modal']}
            />}
        </tr>
      </tbody>
    );
  }
}

ExtrasRow.defaultProps = {
  extra: null
};

ExtrasRow.displayName = 'ExtrasRow';
