/**
 *
 * ControlCell
 * Component that renders either a NumericStepper or
 * AddRemove component based on @maxQuantity and @selectedQuantity
 * @author B.Podczerwinski
 *
 */

import AddRemove from './AddRemove';
import NumericStepper from '../NumericStepper/NumericStepper';

export default function ControlCell ({selected, maxQuantity, selectedQuantity, index, type, code, extrasView}) {
  return (
    <div className="control-cell">
      {(selected && maxQuantity > 1 && selectedQuantity > 0) ?
        <NumericStepper index={index}
                        type={type}
                        code={code}
                        maxQuantity={maxQuantity}
                        selectedQuantity={selectedQuantity}/>
        :
        <AddRemove index={index}
                   type={type}
                   code={code}
                   selected={selected}
                   currentCursor={type + 'Extras'}
                   selectedItem={extrasView[type][index]}
        />
      }
    </div>
  );
}

ControlCell.defaultProps = {
  index: 0,
  type: null,
  maxQuantity: null,
  selectedQuantity: null,
  selected: false,
  code: null
}

ControlCell.displayName = 'ControlCell';
