export default function ExtraDetailsModalContent ({extra}) {
  return (
    <div>
      <h2>{extra.name}</h2>
      <p>{extra.detailed_description || extra.description}</p>
    </div>
  );
}

ExtraDetailsModalContent.defaultProps = {
  extra: {}
}

ExtraDetailsModalContent.displayName = 'ExtraDetailsModalContent';
