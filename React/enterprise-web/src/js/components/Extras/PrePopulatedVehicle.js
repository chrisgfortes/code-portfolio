import Images from '../../utilities/util-images';
/**
 * @class PrePopulateVehicle
 * @type React.class
 * @desc Component to display pre populated extras
 *
 */
export default function PrePopulatedVehicle ({carDetails, onChange}) {
  return (
    <div className="extras-container pre-populated-vehicle">
      <p className="extras-title beta">
        {enterprise.i18nReservation.resflowcorporate_0076}
      </p>
      <div className="extras-section">
        <div className="extras-row cf">
          <div>
            <div className="vehicle-image-container">
              <img src={Images.getUrl(carDetails.images.SideProfile.path, 295, 'high')}
                   alt={carDetails.models}/>
            </div>
            <div className="actions-container">
              <span className="edit" role="button" tabIndex="0" onClick={onChange} onKeyPress={a11yClick(onChange)}>{enterprise.i18nReservation.resflowreview_0084}</span>
            </div>
            <div className="vehicle-information">
              <div className="vehicle-name">{carDetails.name}</div>
              <div className="vehicle-make">{carDetails.models}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

PrePopulatedVehicle.displayName = 'PrePopulatedVehicle';
