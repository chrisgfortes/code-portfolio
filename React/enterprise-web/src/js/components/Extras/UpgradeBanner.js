/**
 * @class UpgradeBanner
 * @type React.class
 * @desc Component to display available vehicle upgrade items in a banner
 *
 */
export default function UpgradeBanner ({bannerContent, upgradeHandler}) {
  return (
    <div className="upgrade-banner">
      <div className="upgrade">
        <img src={bannerContent.imgPath}/>
        <p>
          {bannerContent.upgradeText ?
            <span>
              {bannerContent.upgradeText}
              <a href="#" className="upgrade-button" data-code={bannerContent.code}
                 onClick={ upgradeHandler }>{i18n('resflowextras_0017')}</a>
            </span>
            :
            <span>{i18n('resflowextras_0018')}</span>
          }
        </p>
      </div>
    </div>
  )
}

UpgradeBanner.displayName = 'UpgradeBanner';