import EnrollmentController from '../../controllers/EnrollmentController';
import ExpeditedController from '../../controllers/ExpeditedController';

export default class DuplicateID extends React.Component{
  constructor() {
    super();
    this._onCancel = this._onCancel.bind(this);
    this._onContinue = this._onContinue.bind(this);
    this._forgot = this._forgot.bind(this);
  }
  _onCancel () {
    (this.props.enroll.modal) ? EnrollmentController.setEnrollModal(false) : ExpeditedController.setInput('modal', false);
  }
  _onContinue () {
    // -- Do state tree things
    EnrollmentController.setMemberid(true);
    (this.props.enroll.modal) ? EnrollmentController.setEnrollModal(false) : ExpeditedController.setInput('modal', false);
    if (this.props.enroll.modal) {
      EnrollmentController.callEnrollBtn();
    } else {
      //other trigger
      EnrollmentController.callReviewBtn();
    }
    // use_member_number_as_username
  }
  _forgot() {
    window.open(
      this.props.supportLinks ? this.props.supportLinks.forgot_password_url : 'https://legacy.enterprise.com/car_rental/enterprisePlusForgotPassword.do',
      '_blank'
    );
  }
  render () {
    const {enroll, expedited} = this.props;
    //duplicate account
    if (enroll.modal === 'duplicateAcc' || expedited.modal === 'duplicateAcc') {
      return (
        <div className="duplicate-modal">
          <p>{i18n('eplusenrollment_0071')}</p>

          <div className="modal-actions">
            <a onClick={this._onCancel} className="btn cancel">{i18n('eplusenrollment_0075')}</a>
            <a onClick={this._forgot} className="btn save">{i18n('eplusenrollment_0072')}</a>
          </div>
        </div>
      );
    }
    //duplicate email
    else if (enroll.modal === 'duplicateID' || expedited.modal === 'duplicateID') {
      return (
        <div className="duplicate-modal">
          <p>{i18n('eplusenrollment_0077')}</p>
          {enroll.profile.email ?
            <p className="duplicate-email">{enroll.profile.email}</p> : null}
          <p>{i18n('eplusenrollment_0078')}</p>

          <div className="modal-actions">
            <a onClick={this._onCancel} className="btn cancel">{i18n('eplusenrollment_0075')}</a>
            <a onClick={this._onContinue} className="btn save">{i18n('eplusenrollment_0080')}</a>
          </div>
        </div>
      );
    } else {
      return false;
    }
  }
}

DuplicateID.displayName = "DuplicateID";
