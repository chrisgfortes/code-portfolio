export default function CreatePasswordHeader () {
  return (
    <header>
      <h1>{i18n('loyaltyenrollment_0001')|| 'Complete Your Enterprise Plus Account'}</h1>
      <div className="required-label">
        {i18n('loyaltyenrollment_0002') || "You're a few clicks away from earning amazing benefits. Enter your information in the required fields below."}
      </div>
    </header>
  )
}

CreatePasswordHeader.displayName = "CreatePasswordHeader";