import AccountInfo from './AccountInfo';
import ContactInfo from './ContactInfo';
import LicenseInfo from './LicenseInfo';
import Preferences from './Preferences';
import Error from '../Errors/Error';
import EnrollmentController from '../../controllers/EnrollmentController';
import ScrollUtil from '../../utilities/util-scroll';

export default class EnrollForms extends React.Component{
  constructor() {
    super();
    this.state = {
      accountProgress: false,
      contactProgress: false,
      licenseProgress: false,
      preferencesProgress: false
    }
    EnrollmentController.getCountries();
    this.setEnrollProgress = this.setEnrollProgress.bind(this);
    this.setProgressAllFalse = this.setProgressAllFalse.bind(this);
  }
  componentWillUpdate(nextProps) {
    if(this.props.errors !== nextProps.errors) {
      ScrollUtil.scrollToOffset(0);
    }
  }
  setEnrollProgress(section, bool) {
    this.setState({
      [section]: bool
    });
  }
  setProgressAllFalse() {
    this.setState({
      accountProgress: false,
      contactProgress: false,
      licenseProgress: false,
      preferencesProgress: false
    });
  }
  render () {
    const {errors, enroll, account, expedited, supportLinks, verification, loginWidget} = this.props;
    const {accountProgress, contactProgress, licenseProgress, preferencesProgress} = this.state;
    return (
      <div className="enroll-forms-container">
        <Error errors={errors} type="GLOBAL"/>
        <AccountInfo setEnrollProgress={this.setEnrollProgress}
                     {...{errors,
                          enroll,
                          accountProgress}} />

        <ContactInfo setEnrollProgress={this.setEnrollProgress}
                     {...{errors,
                          enroll,
                          account,
                          contactProgress,
                          accountProgress}} />

        <LicenseInfo setEnrollProgress={this.setEnrollProgress}
                     {...{errors,
                          enroll,
                          account,
                          contactProgress,
                          accountProgress,
                          licenseProgress}} />

        <Preferences setEnrollProgress={this.setEnrollProgress}
                     setProgressAllFalse={this.setProgressAllFalse}
                     {...{expedited,
                          supportLinks,
                          enroll,
                          verification,
                          loginWidget,
                          accountProgress,
                          contactProgress,
                          licenseProgress,
                          preferencesProgress}} />
      </div>
    );
  }
}

EnrollForms.displayName = "EnrollForms";
