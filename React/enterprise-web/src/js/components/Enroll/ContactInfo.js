import Validator from '../../utilities/util-validator';
import EnrollmentController from '../../controllers/EnrollmentController';
import classNames from 'classnames';

export default class ContactInfo extends React.Component{
  constructor() {
    super();
    this.state = {
      country: null,
      streetAddress: null,
      additionalStreetAddress: null,
      city: null,
      subdivision: null,
      postal: null,
      phoneNumber: null,
      phoneNumberType: null,
      alternativePhoneNumber: null,
      alternativePhoneNumberType: null
    }
    this.validator = new Validator(this, this.fieldMap);
    this.fieldMap = this.fieldMap.bind(this);
    this._handleInputChange = this._handleInputChange.bind(this);
    this._onContinue = this._onContinue.bind(this);
    this._expand = this._expand.bind(this);
    this._onCountryChange = this._onCountryChange.bind(this);
    this._onRegionChange = this._onRegionChange.bind(this);
  }
  fieldMap() {
    let map = {
      refs: {
        country: this.country,
        streetAddress: this.streetAddress,
        additionalStreetAddress: this.additionalStreetAddress,
        city: this.city,
        phoneNumber: this.phoneNumber,
        phoneNumberType: this.phoneNumberType,
        subdivision: this.subdivision,
        postal: this.postal,
        alternativePhoneNumber: this.alternativePhoneNumber,
        alternativePhoneNumberType: this.alternativePhoneNumberType
      },
      value: {
        country: this.country.value,
        streetAddress: this.streetAddress.value,
        additionalStreetAddress: this.additionalStreetAddress.value,
        city: this.city.value,
        phoneNumber: this.phoneNumber.value,
        phoneNumberType: this.phoneNumberType.value,
        alternativePhoneNumber: this.alternativePhoneNumber.value,
        alternativePhoneNumberType: this.alternativePhoneNumberType.value
      },
      schema: {
        country: '?string',
        streetAddress: 'string',
        additionalStreetAddress: '?string',
        city: 'string',
        phoneNumber: 'phone',
        phoneNumberType: 'string',
        alternativePhoneNumber: '?phone',
        alternativePhoneNumberType: '?string'
      }
    };

    if (this.props.enroll.originCountry) {
      if (this.props.enroll.originCountry.enable_country_sub_division) {
        map.value.subdivision = this.subdivision.value;
        map.schema.subdivision = 'string';
      }

      if (this.props.enroll.originCountry.postal_code_type) {
        map.value.postal = this.postal.value;
        map.schema.postal = 'string';
      }
    }

    return map;
  }
  _handleInputChange (attribute, event) {
    this.setState(
      {
        [attribute]: event.target.value
      }
    );
    EnrollmentController.setProfileDetails(attribute, event.target.value);
    if (!this.validator.validate(attribute, event.target.value)) {
      this.props.setEnrollProgress('contactProgress', false);
    }
  }
  _onContinue (event) {
    event.preventDefault();
    if (this.validator.validateAll().valid) {
      EnrollmentController.setEnrollSectionDetails('license', this.fieldMap().value);
      this.props.setEnrollProgress('contactProgress', true);
      EnrollmentController.getIssueCountrySubdivisions(this.props.enroll.profile.licenseCountry || enterprise.countryCode);
    } else {
      this.props.setEnrollProgress('contactProgress', false);
    }
  }
  _onCountryChange(event) {
    let elementIndex = event.target.selectedIndex,
      index = event.target.children[elementIndex].getAttribute('data-index');

    EnrollmentController.setEnrollContactDetails(this.props.account.countries[index], event.target.value, null);
  }
  _onRegionChange(event) {
    EnrollmentController.setProfileDetails('subdivision', event.target.value);
    this.validator.validate('subdivision', event.target.value);
  }
  _expand () {
    if (this.props.enroll.section === 'contact') {
      EnrollmentController.setEnrollSection(null);
    } else if (this.props.contactProgress || this.props.accountProgress) {
      EnrollmentController.setEnrollSection('contact');
    }
  }
  render () {
    let originCountry = this.props.enroll.originCountry;
    let subdivisionLabel = originCountry ? EnrollmentController.getSubdivisionLabel( originCountry ) : null;
    let postalLabel = i18n('resflowreview_0068');
    let contactInfoClasses = classNames({
      'contact-info': true,
      'active': this.props.enroll.section === 'contact' || this.props.enroll.section === 'all'
    });
    let circleClasses = classNames({
      'numberCircle': true,
      'completed': this.props.contactProgress
    });
    let carrotClasses = classNames({
      'icon': true,
      'icon-nav-carrot-down': true,
      'active': this.props.enroll.section === 'contact'
    });
    let enrollFormsClasses = classNames({
      'enroll-forms': true,
      'active': this.props.enroll.section === 'contact'
    });

    return (
      <form className={enrollFormsClasses}>
        <h2 className={(this.props.accountProgress) ? '' : 'disabled'} onClick={this._expand}>
          <span className={circleClasses}>{this.props.contactProgress ?
            <i className="icon icon-forms-checkmark-green"/> : 2}</span>
          {i18n('eplusenrollment_0002')}
          <span onClick={this._expand} className={carrotClasses}/>
        </h2>

        <div className={contactInfoClasses}>
          <div className="field-container country">
            <label htmlFor="country">{i18n('resflowreview_0062')}</label>
            {this.props.account.countries && this.props.account.countries.length > 0 ?
              <select className="styled" onChange={this._onCountryChange} id="country"
                      ref={c => this.country = c}
                      defaultValue={enterprise.countryCode}>
                {this.props.account.countries.map(function (country, index) {
                  return (<option key={index}
                                 data-index={index}
                                 value={country.country_code}>{country.country_name}</option>)
                })}
              </select>
              :
              <div ref={c => this.country = c}
                   className="loading">{i18n('resflowcorporate_4037')}</div>
            }
          </div>

          <div className="field-container">
            <label htmlFor="streetAddress">{i18n('resflowreview_0063')}</label>
            <input onChange={this._handleInputChange.bind(this, 'streetAddress')} id="streetAddress"
                   ref={c => this.streetAddress = c} type="text"/>
          </div>

          <div className="field-container">
            <label htmlFor="additionalStreetAddress">
              {i18n('resflowreview_0064')}</label>
            <input onChange={this._handleInputChange.bind(this, 'additionalStreetAddress')}
                   id="additionalStreetAddress"
                   ref={c => this.additionalStreetAddress = c}
                   type="text"/>
          </div>

          <div className="field-container city">
            <label htmlFor="city">{i18n('resflowreview_0065')}</label>
            <input onChange={this._handleInputChange.bind(this, 'city')} ref={c => this.city = c} id="city"
                   type="text"/>
          </div>

          {(originCountry && originCountry.enable_country_sub_division) ?
            <div className="field-container subdivision">
              <label htmlFor="subdivision">{subdivisionLabel}</label>
              {this.props.account.subdivisions && this.props.account.subdivisions.length > 0 ?
                <select className="styled"
                        onChange={this._onRegionChange}
                        id="subdivision"
                        ref={c => this.subdivision = c}>
                  <option key="0"
                          value="">{i18n('resflowlocations_0021')}</option>
                  {this.props.account.subdivisions.map(function (region, index) {
                    return (<option key={index}
                                   value={region.country_subdivision_code}>{region.country_subdivision_name}</option>)
                  })}
                </select>
                :
                <input ref={c => this.subdivision = c} onChange={this._onRegionChange} id="subdivision" type="text"/>
              }
            </div>
            : false}

          {originCountry && originCountry.postal_code_type ? <div className="field-container postal">
            <label htmlFor="postal">{postalLabel}</label>
            <input onChange={this._handleInputChange.bind(this, 'postal')} id="postal" ref={c => this.postal = c}
                   type="text"/>
          </div> : false}

          <div className="field-container phone">
            <label htmlFor="phoneNumber">{i18n('eplusenrollment_0014')}</label>
            <select className="styled" ref={c => this.phoneNumberType = c}
                    onChange={this._handleInputChange.bind(this, 'phoneNumberType')}>
              <option value="HOME">{i18n('eplusenrollment_0017')}</option>
              <option value="WORK">{i18n('eplusenrollment_0018')}</option>
              <option value="MOBILE">{i18n('eplusenrollment_0016')}</option>
              <option value="OTHER">{i18n('eplusenrollment_0020')}</option>
            </select>
            <input
              ref={c => this.phoneNumber = c}
              onChange={this._handleInputChange.bind(this, 'phoneNumber')}
              id="phoneNumber"
              type="tel"/>
          </div>
          <div className="field-container phone">
            <select className="styled" defaultValue="MOBILE" ref={c => this.alternativePhoneNumberType = c}
                    onChange={this._handleInputChange.bind(this, 'alternativePhoneNumberType')}>
              <option value="HOME">{i18n('eplusenrollment_0017')}</option>
              <option value="WORK">{i18n('eplusenrollment_0018')}</option>
              <option value="MOBILE">{i18n('eplusenrollment_0016')}</option>
              <option value="OTHER">{i18n('eplusenrollment_0020')}</option>
            </select>
            <input
              ref={c => this.alternativePhoneNumber = c}
              onChange={this._handleInputChange.bind(this, 'alternativePhoneNumber')}
              id="alternativePhoneNumber"
              placeholder={i18n('reservationwidget_0040')}
              type="tel"/>
          </div>

          {
            this.props.errors ? false : <div className="form-actions">
              <button onClick={this._onContinue}
                      className="btn continue">{i18n('reservationwidget_0014')}</button>
            </div>
          }

        </div>
      </form>
    );
  }
}

ContactInfo.displayName = "ContactInfo";
