import Validator from '../../utilities/util-validator';
import classNames from 'classnames';
import EnrollmentController from '../../controllers/EnrollmentController';
import { PasswordErrorList, PasswordErrorListItem } from '../Password/PasswordErrorList';

export default class CreatePasswordInfo extends React.Component{
  constructor() {
    super();
    this.state = {
      password: null,
      passwordConfirm: null,
      passwordRules: {
        space: null,
        length: null,
        blacklist: null,
        oneLetter: null,
        oneNumber: null,
        email: null
      },
      confirmPasswordRules: {
        match: null
      }
    };
    this.validator = new Validator(this, this.fieldMap);
    this.fieldMap = this.fieldMap.bind(this);
    this._handleInputChange = this._handleInputChange.bind(this);
  }
  fieldMap() {
    let map = {
      refs: {
        password: this.password,
        passwordConfirm: this.passwordConfirm
      },
      value: {
        password: this.password.value,
        passwordConfirm: this.passwordConfirm.value
      },
      schema: {
        password: 'password',
        passwordConfirm: () => {
          let passwordConfirm = this.passwordConfirm.value;
          let matches = passwordConfirm.length > 0 && this.password.value === passwordConfirm;
          this.setState({confirmPasswordRules: {match: matches}});
          return matches;
        }
      }
    };
    return map;
  }
  _handleInputChange (attribute, event) {
    this.setState(
      {
        [attribute]: event.target.value
      }
    );
    EnrollmentController.setCreatePasswordDetails(attribute, event.target.value);
    if (attribute === 'password') {
      let errorObj = this.validator.validate('password', event.target.value);
      this.setState({passwordRules: errorObj.errorReasons.password});
      this.validator.validate('passwordConfirm', event.target.value);
    }
    if (attribute === 'passwordConfirm') {
      this.validator.validate('passwordConfirm', event.target.value);
    }
  }
  render() {
    let passwordConfirmClasses = classNames({
      'cf': true,
      'field-container': true,
      'active': this.state.password && this.state.password.length > 0
    });
    return (
      <div className="cf password-info">
        <div className="field-container">
          <label htmlFor="password">{i18n('loyaltysignin_0004')}</label>
          <input onChange={this._handleInputChange.bind(this, 'password')}
                 id="password" type="password"
                 ref={c => this.password = c}/>
          <PasswordErrorList errors={this.state.passwordRules}/>
        </div>
        <div className={passwordConfirmClasses}>
          <label htmlFor="passwordConfirm">{i18n('resflowreview_0048')}</label>
          <input onChange={this._handleInputChange.bind(this, 'passwordConfirm')}
                 id="passwordConfirm" type="password"
                 ref={c => this.passwordConfirm = c}/>
          <ul className="criteria error-list">
            <PasswordErrorListItem showOnError={true} text={i18n('loyaltysignin_0044') || "Passwords Don't Match"} valid={this.state.confirmPasswordRules.match} />
          </ul>
        </div>
      </div>
    );
  }
}

CreatePasswordInfo.displayName = "CreatePasswordInfo";