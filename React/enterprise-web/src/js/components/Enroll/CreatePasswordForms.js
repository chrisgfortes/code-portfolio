import ReservationCursors from '../../cursors/ReservationCursors';
import Validator from '../../utilities/util-validator';
import ScrollUtil from '../../utilities/util-scroll';
import EnrollmentController from '../../controllers/EnrollmentController';
import ConfirmAcceptTermsModal from './ConfirmAcceptTermsModal';
import CreatePasswordMemberInfo from './CreatePasswordMemberInfo';
import CreatePasswordInfo from './CreatePasswordInfo';
import CreateEmailModal from '../Modal/CreateEmailModal';
import Terms from './Terms';
import GlobalModal from '../Modal/GlobalModal';
import CreatePasswordModals from './CreatePasswordModals';
import classNames from 'classnames';

import Error from '../Errors/Error';

export default class CreatePasswordForms extends React.Component{
  constructor() {
    super();
    this.state = {
      acceptTerms: false
    };
    EnrollmentController.getCountries();
    EnrollmentController.getTerms();
    this.validator = new Validator(this, this.fieldMap);
    this.fieldMap = this.fieldMap.bind(this);
    this._handleInputChange = this._handleInputChange.bind(this);
    this.doEnroll = this.doEnroll.bind(this);
    this._toggleTerms = this._toggleTerms.bind(this);
    this._modalAccept = this._modalAccept.bind(this);
  }
  componentWillUpdate(nextProps) {
    if(this.props.errors !== nextProps.errors) {
      ScrollUtil.scrollToOffset(0);
    }
  }
  fieldMap() {
    let map = {
      value: {
        acceptTerms: this.acceptTerms.checked
      },
      schema: {
        acceptTerms: () => {
          return this.acceptTerms.checked;
        }
      },
      refs: {
        acceptTerms: this.acceptTermsLabel
      }
    }

    return map;
  }

  _handleInputChange (attribute, event) {
    this.setState(
      {
        [attribute]: event.target.checked
      }
    );
    EnrollmentController.setProfileDetails(attribute, event.target.checked);
  }
  doEnroll (event) {
    event.preventDefault();
    const memberInfoValidate = this.memberInfo.validator.validateAll().valid;
    const passwordValidate = this.passwordInfo.validator.validateAll().valid;
    if (this.props.createPassword.licenseCountry === 'GB') {
      EnrollmentController.setCreatePasswordDetails('licenseRegion', 'DVLA');
    }
    if (memberInfoValidate && passwordValidate && this.validator.validateAll().valid && !this.props.createPassword.loading) {
      EnrollmentController.callCreatePassword(true, this.props.loginWidget.terms.terms_and_conditions_version, this.fieldMap().value.acceptTerms);
    } else if (memberInfoValidate && passwordValidate) {
      EnrollmentController.setEnrollModal('confirm');
    }
  }

  _toggleTerms (event) {
    event.preventDefault();
    EnrollmentController.setEnrollModal('terms');
    this._handleInputChange('acceptTerms', {target: {checked: true}});
  }
  _modalAccept (event) {
    this._handleInputChange('acceptTerms', {target: {checked: true}});
    this.acceptTerms.checked = true;
    this.doEnroll(event);
  }
  render () {
    let submitClasses = classNames({
      'btn': true,
      'continue': true,
      'disabled': this.props.createPassword.loading
    });
    return (
      <div className="enroll-forms-container">
        <Error errors={this.props.errors} type="GLOBAL"/>
        <form className="cf enroll-forms account">
          <h2>{i18n('loyaltyenrollment_0003') || 'Confirm Member Details'}</h2>
          <CreatePasswordMemberInfo ref={c => this.memberInfo =c} />
          <h2 className="partial-enroll-header">{i18n('eplusenrollment_0013') || 'Create A Password'}</h2>
          <CreatePasswordInfo ref={c => this.passwordInfo = c}/>
          <div className="preferences active">
            <div className="field-container">
              <input onChange={this._handleInputChange.bind(this, 'acceptTerms')} id="acceptTerms"
                     ref={c => this.acceptTerms = c} name="acceptTerms" type="checkbox" checked={this.state.acceptTerms}/>
              <label ref={c => this.acceptTermsLabel = c} htmlFor="acceptTerms">
                {i18n('resflowreview_1007')}<a href="#" className="acceptTermsLink"
                                                                  onClick={this._toggleTerms}>{i18n('eplusenrollment_0062')}</a>{i18n('resflowreview_1009')}
              </label>
            </div>
            <div className="form-actions">
              <div className="button-right">
                <div className={this.props.createPassword.loading ? 'loading' : false}/>
                <button onClick={this.doEnroll} className={submitClasses}>
                  {i18n('loyaltyenrollment_0009') || 'Complete Enrollment'}
                </button>
              </div>
            </div>
          </div>
          <CreatePasswordModals modalAccept={this._modalAccept}
                                enroll={this.props.enroll}
                                isEmailModal={this.props.isEmailModal}
                                loginWidget={this.props.loginWidget} />
        </form>
      </div>
    );
  }
}

CreatePasswordForms.displayName = "CreatePasswordForms";
