import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import ReservationCursors from '../../cursors/ReservationCursors';
import CreatePasswordHeader from './CreatePasswordHeader';
import CreatePasswordForms from './CreatePasswordForms';
import CreatePasswordSuccess from './CreatePasswordSuccess';

const CreatePassword = React.createClass({
  mixins: [BaobabReactMixinBranch, React.addons.PureRenderMixin],
  cursors: {
    enroll: ReservationCursors.enroll,
    errors:  ReservationCursors.createPasswordErrors,
    verification: ReservationCursors.verification,
    isEmailModal: ReservationCursors.setEmailModal,
    createPassword: ReservationCursors.createPassword,
    loginWidget: ReservationCursors.loginWidget
  },
  render () {
    const {enroll, errors, verification, isEmailModal, createPassword, loginWidget} = this.state;
    return (
      <section className="enroll-page partial-enroll">
        {enroll.success ?
          <CreatePasswordSuccess />
          :
          <div>
            <CreatePasswordHeader />
            <CreatePasswordForms {...{errors, 
                                      enroll,
                                      verification,
                                      isEmailModal,
                                      createPassword,
                                      loginWidget}} />
          </div>
        }
      </section>
    );
  }
});

module.exports = CreatePassword;