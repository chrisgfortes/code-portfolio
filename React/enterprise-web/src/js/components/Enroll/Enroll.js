import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import ReservationCursors from '../../cursors/ReservationCursors';
import CorporateModals from '../Corporate/ActionModals';
import EnrollmentController from '../../controllers/EnrollmentController';
import EnrollHeader from './EnrollHeader';
import EnrollForms from './EnrollForms';
import EnrollSuccess from './EnrollSuccess';
import UrlUtil from '../../utilities/util-url';

const Enroll = React.createClass({
  mixins: [BaobabReactMixinBranch, React.addons.PureRenderMixin],
  componentDidMount () {
    window.onhashchange = () => {
      this.hashHandler();
    };
    this.hashHandler();
  },
  cursors: {
    enroll: ReservationCursors.enroll,
    account: ReservationCursors.account,
    errors: ReservationCursors.enrollErrors,
    verification: ReservationCursors.verification,
    loginWidget: ReservationCursors.loginWidget,
    expedited: ReservationCursors.expedited,
    supportLinks: ReservationCursors.supportLinks
  },
  hashHandler () {
    let cid = UrlUtil.getParameters().cid;
    if (cid && cid.length > 0) {
      EnrollmentController.setProfileDetails('cid', cid[0]);
    }
  },
  render () {
    const {enroll, errors, account, verification, loginWidget, expedited, supportLinks} = this.state;
    return (
      <section className="enroll-page">
        {enroll.success ?
          <EnrollSuccess enroll={enroll} />
          :
          <div>
            <EnrollHeader />
            <EnrollForms {...{errors,
                         enroll, 
                         account,
                         verification,
                         loginWidget,
                         expedited,
                         supportLinks}} />
            <CorporateModals />   
          </div>
        }
      </section>
    );
  }
});

module.exports = Enroll;
