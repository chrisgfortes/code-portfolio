import VerificationController from '../../controllers/VerificationController';

export default class Privacy extends React.Component{
  constructor() {
    super();
    VerificationController.getDisclaimer();
  }
  render() {
    return (
      <div className="enroll-privacy">
        <div className="privacy-modal-content"
             dangerouslySetInnerHTML={{__html: this.props.verification.privacyPolicy}}/>
      </div>
    );
  }
}

Privacy.displayName = "Privacy";
