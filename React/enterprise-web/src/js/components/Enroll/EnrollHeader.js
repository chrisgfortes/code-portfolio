export default function EnrollHeader() {
  return (
    <header>
      <h1>{i18n('resflowcorporate_4041')}</h1>

      <div className="required-label">
        {i18n('resflowreview_0004')}
      </div>
    </header>
  );
}

EnrollHeader.displayName = "EnrollHeader";
