import RedirectController from '../../controllers/RedirectController';
import SessionController from'../../controllers/SessionController';

export default class CreatePasswordSuccess extends React.Component{
  constructor() {
    super();
    window.location.hash = 'success';
    SessionController.callGetSession();
    this._onConfirmClick = this._onConfirmClick.bind(this);
  }
  _onConfirmClick (event) {
    event.preventDefault();
    RedirectController.homeRedirect();
  }
  render () {
    return (
      <div className="centered-section">
        <h1>{i18n('loyaltyenrollment_0010') || 'You have successfully completed your Enterprise Plus Account.'}</h1>
        <div className="modal-actions">
          <a href="account.html" className="btn cancel">{i18n('loyaltyforgotpassword_0010')}</a>
          <a href="#" onClick={this._onConfirmClick} className="btn ok">{i18n('loyaltyforgotpassword_0011')}</a>
        </div>
      </div>
    );
  }
}

CreatePasswordSuccess.displayName = "CreatePasswordSuccess";