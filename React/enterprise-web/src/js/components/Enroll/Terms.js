import {getTerms} from '../../controllers/EnrollmentController';

export default class Terms extends React.Component{
  constructor(props) {
    super(props);
    getTerms();
    this._onPrintClick = this._onPrintClick.bind(this);
    if (!this.props.terms) {
      getTerms();
    }
  }
  _onPrintClick(event){
    event.preventDefault();
    //Get the iframe
    let iframe = document.getElementById('terms-and-conditions-modal-iframe');
    //Create the iframe's HTML - not react friendly :-( but we need to get that link tag...
    let html = '<head>' + $('<div />').append($('head link[media="print"]').clone()).html() + '</head><body onload="window.print()"><div id="print-modal">' + this.props.loginWidget.terms.terms_and_conditions + '</div></body>';

    //check if the element exists already, no need to create it twice...
    if (!iframe) {
      iframe = document.createElement('iframe');
      iframe.id = "terms-and-conditions-modal-iframe";
      document.body.appendChild(iframe);
    }

    //open the iframe, inject the html and close it back up.
    iframe.contentWindow.document.open();
    iframe.contentWindow.document.write(html);
    iframe.contentWindow.document.close();

    //the print call is in the actual html as an onload attribute
  }
  render () {
    return (
      <div className="enroll-terms">
        <div className="print-link">
          <a href="#" onClick={this._onPrintClick}>
            <i className="icon icon-confirmation-print"/>
            {i18n('resflowreview_0153')}
          </a>
        </div>
        { this.props.loginWidget.terms ?
          <div className="terms-modal-content"
               dangerouslySetInnerHTML={{__html: this.props.loginWidget.terms.terms_and_conditions}}/>
          : false}
      </div>
    );
  }
}

Terms.displayName = "Terms";

