import ReactDOM from 'react-dom';
import Validator from '../../utilities/util-validator';
import DateSelector from '../DateSelector/DateSelector';
import classNames from 'classnames';
import EnrollmentController from '../../controllers/EnrollmentController';

export default class LicenseInfo extends React.Component{
  constructor() {
    super();
    this.state = {
      birthDate: null,
      licenseNumber: null,
      licenseIssue: null,
      licenseExpiry: null,
      licenseCountry: null,
      licenseRegion: null
    }
    this.validator = new Validator(this, this.fieldMap);
    this.fieldMap = this.fieldMap.bind(this);
    this._handleInputChange = this._handleInputChange.bind(this);
    this._onContinue = this._onContinue.bind(this);
    this._expand = this._expand.bind(this);
    this._onCountryChange = this._onCountryChange.bind(this);
    this._onRegionChange = this._onRegionChange.bind(this);
    this._inputCallback = this._inputCallback.bind(this);
  }
  componentWillUpdate () {
    if (!this.state.licenseCountry && this.props.account.countries.length > 0) {
      this.setState({licenseCountry: enterprise.countryCode});
    }
  }
  fieldMap () {
    let licenseIssueValid = this.state.licenseIssue && this.state.licenseIssue.length > 9;
    let licenseExpiryValid = this.state.licenseExpiry && this.state.licenseExpiry.length > 9;
    let issueCountry = this.props.enroll.issueCountry;

    let map = {
      refs: {
        birthDate: ReactDOM.findDOMNode(this.birthDate),
        licenseNumber: this.licenseNumber,
        licenseCountry: this.licenseCountry,
        licenseRegion: this.licenseRegion
      },
      value: {
        birthDate: this.state.birthDate,
        licenseNumber: this.licenseNumber.value,
        licenseCountry: this.licenseCountry.value
      },
      schema: {
        birthDate: 'date',
        licenseNumber: 'string',
        licenseCountry: '?string'
      }
    };

    if (issueCountry) {
      if (!issueCountry.license_expiry_date) {
        map.value.licenseExpiry = this.state.licenseExpiry;
        map.schema.licenseExpiry = function () {
          if (issueCountry.license_expiry_date && issueCountry.license_expiry_date === 'MANDATORY') {
            return licenseExpiryValid;
          } else if (issueCountry.license_expiry_date && issueCountry.license_expiry_date === 'OPTIONAL') {
            return licenseIssueValid || licenseExpiryValid;
          }
          return true;
        };
      }

      if (this.props.enroll.issueCountry.license_issue_date) {
        map.value.licenseIssue = this.state.licenseIssue;
        map.schema.licenseIssue = function () {
          if (issueCountry.license_issue_date && issueCountry.license_issue_date === 'MANDATORY') {
            return licenseIssueValid;
          } else if (issueCountry.license_issue_date && issueCountry.license_issue_date === 'OPTIONAL') {
            return licenseIssueValid || licenseExpiryValid;
          }
          return true;
        };
      }

      if (this.props.enroll.issueCountry.enable_country_sub_division) {
        map.value.licenseRegion = this.licenseRegion.value;
        map.schema.licenseRegion = 'string';
      }
    }

    return map;
  }
  _inputCallback (attribute, value) {
    this.setState({
      [attribute]: value
    });

    EnrollmentController.setProfileDetails(attribute, value);
    setTimeout(()=> {
      if (!this.validator.validate(attribute, value)) {
        this.props.setEnrollProgress('licenseProgress', false);
      }
    }, 0);
  }
  _handleInputChange (attribute, event) {
    this._inputCallback(attribute, event.target.value);
  }
  _onContinue (event) {
    event.preventDefault();
    if (this.validator.validateAll().valid) {
      EnrollmentController.setEnrollSectionDetails('preferences', this.fieldMap().value);
      this.props.setEnrollProgress('licenseProgress', true);
    } else {
      this.props.setEnrollProgress('licenseProgress', false);
    }
  }
  _onCountryChange (event) {
    let value = event.target.value;
    let elementIndex = event.target.selectedIndex;
    let index = event.target.children[elementIndex].getAttribute('data-index');

    this.setState({licenseCountry: value});

    EnrollmentController.setEnrollLicenseDetails(this.props.account.countries[index], value, null);
  }
  _onRegionChange (event) {
    let value = event.target.value;
    this.setState({licenseRegion: value});
    EnrollmentController.setProfileDetails('licenseRegion', value);
    this.validator.validate('licenseRegion', value);
  }
  _expand () {
    if (this.props.enroll.section === 'license') {
      EnrollmentController.setEnrollSection(null);
    } else if (this.props.accountProgress && this.props.contactProgress) {
      EnrollmentController.setEnrollSection('license');
    }
  }
  render () {
    let fields = this.props.enroll.issueCountry;
    let issuingAuthorityLabel = null;
    let licenseInfoClasses = classNames({
      'license-info': true,
      'active': this.props.enroll.section === 'license' || this.props.enroll.section === 'all'
    });
    let circleClasses = classNames({
      'numberCircle': true,
      'completed': this.props.licenseProgress
    });
    let carrotClasses = classNames({
      'icon': true,
      'icon-nav-carrot-down': true,
      'active': this.props.enroll.section === 'license'
    });
    let enrollFormsClasses = classNames({
      'enroll-forms': true,
      'active': this.props.enroll.section === 'license'
    });
    let enableLicenseIssueDate = fields && (fields.license_issue_date === 'OPTIONAL' || fields.license_issue_date === 'MANDATORY');
    let enableLicenseExpiryDate = fields && (fields.license_expiry_date === 'OPTIONAL' || fields.license_expiry_date === 'MANDATORY');

    if (fields) {
      issuingAuthorityLabel = EnrollmentController.getIssueAuthorityLabel(fields);
    }

    return (
      <form className={enrollFormsClasses}>
        <h2 className={(this.props.accountProgress && this.props.contactProgress) ? '' : 'disabled'}
            onClick={this._expand}><span
          className={circleClasses}>{this.props.licenseProgress ?
          <i className="icon icon-forms-checkmark-green"/> : '3'}</span>
          {i18n('resflowreview_0121')}
          <span onClick={this._expand} className={carrotClasses}/>
        </h2>

        <div className={licenseInfoClasses}>

          <div className="field-container issue-country">
            <label htmlFor="issue-country">{i18n('resflowreview_0070')}</label>
            {this.props.account.countries && this.props.account.countries.length > 0 ?
              <select className="styled" onChange={this._onCountryChange}
                      ref={c => this.licenseCountry = c}
                      id="issue-country"
                      defaultValue={enterprise.countryCode}>
                {this.props.account.countries.map(function (country, index) {
                  return (<option key={index}
                                  data-index={index}
                                  value={country.country_code}>{country.country_name}</option>);
                })}
              </select>
              :
              <div className="loading">{i18n('resflowcorporate_4037')}</div>
            }
          </div>

          {(fields && fields.enable_country_sub_division) ?
            <div className="field-container issue-authority">
              <label htmlFor="issue-authority">{issuingAuthorityLabel}</label>
              {this.props.account.issueCountrySubdivisions && this.props.account.issueCountrySubdivisions.length > 0 ?
                <select className="styled" onChange={this._onRegionChange}
                        ref={c => this.licenseRegion = c}
                        id="subdivision">
                  {{GB: true}[fields.country_code] ? false : <option key="0"
                                                                     value="">{i18n('resflowlocations_0021')}</option> }
                  {{GB: true}[fields.country_code] ? <option value="DVLA" key={-1}>DVLA</option> :
                    this.props.account.issueCountrySubdivisions.map(function (region, index) {
                      return (<option key={index}
                                      value={region.country_subdivision_code}>{region.country_subdivision_name}</option>);
                    })}
                </select>
                :
                <input ref={c => this.licenseRegion = c}
                       onChange={this._onRegionChange} type="text"/>
              }
            </div> : false}

          <div className="field-container birth-date">
            <label htmlFor="birth-date-year">
              {i18n('resflowreview_0076')}{' '}
              <em>({moment.localeData()._longDateFormat.L})</em>
            </label>

            <DateSelector ref={c => this.birthDate = c} onChange={this._inputCallback.bind(this, 'birthDate')}/>

          </div>
          <div className="field-container license-number">
            <label htmlFor="license-number">{i18n('resflowreview_0072')}</label>
            <input ref={c => this.licenseNumber = c}
                   onChange={this._handleInputChange.bind(this, 'licenseNumber')}
                   id="license-number" type="text"/>
          </div>

          {enableLicenseIssueDate ?
            <div className="field-container license-issue">
              <label htmlFor="issue-date-year">
                {i18n('resflowreview_0720')}{' '}
                <em>({moment.localeData()._longDateFormat.L})</em>
              </label>

              <DateSelector ref="licenseIssue" onChange={this._inputCallback.bind(this, 'licenseIssue')}/>

            </div> : false}

          {enableLicenseExpiryDate ?
            <div className="field-container expire-date">
              <label htmlFor="expire-date-year">
                {i18n('resflowreview_0037')}{' '}
                <em>({moment.localeData()._longDateFormat.L})</em>
              </label>
              <DateSelector ref="licenseExpiry" onChange={this._inputCallback.bind(this, 'licenseExpiry')}/>
            </div> : false}

          {
            this.props.errors ? false :
              <div className="form-actions">
                <div className="button-right">
                  <button onClick={this._onContinue} className="btn continue">
                    {i18n('reservationwidget_0014')}
                  </button>
                </div>
              </div>
          }
        </div>
      </form>
    );
  }
}

LicenseInfo.displayName = "LicenseInfo";
