import BookingWidgetModelController from '../../controllers/BookingWidgetModelController';
import BookingWidget from '../BookingWidget/BookingWidget';
import scrollUtil from '../../utilities/util-scroll';
import SessionController from'../../controllers/SessionController';

export default class EnrollSuccess extends React.Component{
  constructor() {
    super();
    window.location.hash = 'success';
    SessionController.callGetSession();
    scrollUtil.scrollTop('body', 'slow');
  }
  render () {
    let basic = this.props.enroll.profile.basic_profile,
      AEMData = enterprise.enroll;

    return (
      <div>
        <div className="band hero-band enroll-success full-bleed">
          <div className="hero-container" style={{backgroundImage: 'url(' + AEMData.heroImage + ')'}}>
            <div className="hero-position">
              <img className="lob-logo" src={AEMData.epluslogo} alt={AEMData.epluslogoAltText} />

              <h2>{basic && basic.loyalty_data && basic.loyalty_data.loyalty_number ? enterprise.i18nReservation.resflowconfirmation_0016.replace('#{memberNumber}', basic.loyalty_data.loyalty_number) : AEMData.heroTitle}</h2>
            </div>
          </div>
        </div>
        <div className="band enroll-success grid-band full-bleed">
          <div className="g g-3up">
            <div className="gi">
              <div className="content-container">
                <div className="cta-container">
                  <a href={AEMData.calltoactionlinkurl1}>{AEMData.calltoactionlinktext1}</a>

                  <p>{AEMData.ctadescription1}</p>
                </div>
              </div>
            </div>
            <div className="gi">
              <div className="content-container">
                <div className="cta-container">
                  <a href={AEMData.calltoactionlinkurl2}>{AEMData.calltoactionlinktext2}</a>

                  <p>{AEMData.ctadescription2}</p>
                </div>
              </div>
            </div>
            <div className="gi">
              <div className="content-container">
                <div className="cta-container">
                  <blockquote className="pullquote caption">{AEMData.heroQuote}</blockquote>
                  <div className="name">{AEMData.heroName}</div>
                  <div className="line"/>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="cf enroll-booking-widget">
          <h1>{i18n('reservationwidget_0001')}</h1>
          <BookingWidget modelController={BookingWidgetModelController}/>
        </div>
      </div>
    );
  }
}

EnrollSuccess.displayName = "EnrollSuccess";
