import Validator from '../../utilities/util-validator';
import CorporateController from '../../controllers/CorporateController';
import EnrollmentController from '../../controllers/EnrollmentController';
import classNames from 'classnames';

import { PasswordErrorList, PasswordErrorListItem } from '../Password/PasswordErrorList';

export default class AccountInfo extends React.Component{
  constructor() {
    super();
    this.state = {
      firstName: null,
      lastName: null,
      email: null,
      emailConfirm: null,
      password: null,
      passwordConfirm: null,
      passwordRules: {
        space: null,
        length: null,
        blacklist: null,
        oneLetter: null,
        oneNumber: null,
        email: null
      },
      confirmPasswordRules: {
        match: null
      }
    };
    this.validator = new Validator(this, this.fieldMap);
    this.fieldMap = this.fieldMap.bind(this);
    this._handleInputChange = this._handleInputChange.bind(this);
    this._onRemoveCode = this._onRemoveCode.bind(this);
    this._expand = this._expand.bind(this);
    this._onContinue = this._onContinue.bind(this);
  }
  fieldMap() {
    return {
      refs: {
        firstName: this.firstName,
        lastName: this.lastName,
        email: this.email,
        emailConfirm: this.emailConfirm,
        password: this.password,
        passwordConfirm: this.passwordConfirm
      },
      value: {
        firstName: this.firstName.value,
        lastName: this.lastName.value,
        email: this.email.value,
        emailConfirm: this.emailConfirm.value,
        password: this.password.value,
        passwordConfirm: this.passwordConfirm.value
      },
      schema: {
        firstName: 'string',
        lastName: 'string',
        email: 'email',
        emailConfirm: () => {
          let emailConfirm = this.emailConfirm.value;
          return emailConfirm.length > 0 && this.email.value === emailConfirm;
        },
        password: 'password',
        passwordConfirm: () => {
          let passwordConfirm = this.passwordConfirm.value;
          let matches = passwordConfirm.length > 0 && this.password.value === passwordConfirm;
          this.setState({confirmPasswordRules: {match: matches}});
          return matches;
        }
      }
    }
  }
  _handleInputChange (attribute, event) {
    this.setState(
      {
        [attribute]: event.target.value
      }
    );
    EnrollmentController.setProfileDetails(attribute, event.target.value);

    //Reverse validation for password and email
    if (attribute === 'password') {
      let errorObj = this.validator.validate('password', event.target.value);
      this.setState({passwordRules: errorObj.errorReasons.password});
      this.validator.validate('passwordConfirm', event.target.value);
    }

    if (attribute === 'email') {
      this.validator.validate('emailConfirm', event.target.value);
    }

    if (!this.validator.validate(attribute, event.target.value)) {
      this.props.setEnrollProgress('accountProgress', false);
    }
  }
  _onRemoveCode () {
    CorporateController.showModalInState('enrollRemoveCode');
  }
  _onContinue (event) {
    event.preventDefault();
    if (this.validator.validateAll().valid) {
      EnrollmentController.setEnrollSectionDetails('contact', this.fieldMap().value);
      this.props.setEnrollProgress('accountProgress', true);
    } else {
      this.props.setEnrollProgress('accountProgress', false);
    }
  }
  _expand () {
    if (!this.props.errors) {
      if (this.props.enroll.section === 'account') {
        EnrollmentController.setEnrollSection(null);
      } else {
        EnrollmentController.setEnrollSection('account');
      }
    }
  }
  render () {
    let emailConfirmClasses = classNames({
      'cf': true,
      'field-container': true,
      'collapse': false,
      'active': this.state.email ? this.state.email.length > 0 : false
    });
    let passwordConfirmClasses = classNames({
      'cf': true,
      'field-container': true,
      'collapse': false,
      'active': this.state.password ? this.state.password.length > 0 : false
    })
    let accountDetailsClasses = classNames({
      'cf': true,
      'account-details': true,
      'active': this.props.enroll.section === 'account' || this.props.enroll.section === 'all'
    });
    let circleClasses = classNames({
      'cf': true,
      'numberCircle': true,
      'completed': this.props.accountProgress
    });
    let carrotClasses = classNames({
      'cf': true,
      'icon': true,
      'icon-nav-carrot-down': true,
      'active': this.props.enroll.section === 'account'
    });
    let enrollFormsClasses = classNames({
      'cf': true,
      'enroll-forms': true,
      'active': this.props.enroll.section === 'account'
    });

    return (
      <form className={enrollFormsClasses}>
        <h2 onClick={this._expand} className={this.props.errors ? 'disabled' : ''}>
          <span className={circleClasses}>{this.props.accountProgress ?
            <i className="icon icon-forms-checkmark-green"/> : '1'}</span>
          {i18n('eplusaccount_0034')}
          <span onClick={this._expand} className={carrotClasses}/>
        </h2>
        <div className={accountDetailsClasses}>
          <div className="field-container first-name">
            <label htmlFor="first-name">{i18n('reservationwidget_0021')}</label>
            <input id="first-name"
                   onChange={this._handleInputChange.bind(this, 'firstName')}
                   type="text"
                   ref={c => this.firstName = c}/>
          </div>
          <div className="field-container last-name">
            <label htmlFor="last-name">{i18n('reservationwidget_0022')}</label>
            <input id="last-name"
                   onChange={this._handleInputChange.bind(this, 'lastName')}
                   type="text"
                   ref={c => this.lastName = c}/>
          </div>
          <div className="field-container">
            <label htmlFor="emailAddress">{i18n('reservationwidget_0030')}</label>
            <input onChange={this._handleInputChange.bind(this, 'email')}
                   id="email" type="email"
                   ref={c => this.email = c}/>
          </div>
          <div className={emailConfirmClasses}>
            <label htmlFor="emailConfirm">{i18n('eplusenrollment_0011')}</label>
            <input onChange={this._handleInputChange.bind(this, 'emailConfirm')}
                   id="emailConfirm" type="email"
                   ref={c => this.emailConfirm = c}
                   disabled={this.state.email ? !this.state.email.length > 0 : 'disabled' }/>
          </div>
          {this.props.enroll.profile.cid ? <div className="field-container">
            <label htmlFor="cid" className="cid-label">{i18n('eplusenrollment_0046')}</label>
            <span onClick={this._onRemoveCode}
                  className="remove-cid">{i18n('eplusenrollment_0047')}</span>
            <input className="disabled"
                   id="cid" type="text"
                   value={this.props.enroll.profile.cid}
                   ref={c => this.cid = c}
                   disabled="disabled"/>
          </div> : false}
          <div className="field-container">
            <label htmlFor="password">{i18n('loyaltysignin_0004')}</label>
            <input onChange={this._handleInputChange.bind(this, 'password')}
                   id="password" type="password"
                   ref={c => this.password = c}/>
            <PasswordErrorList errors={this.state.passwordRules}/>
          </div>
          <div className={passwordConfirmClasses}>
            <label htmlFor="passwordConfirm">{i18n('resflowreview_0048')}</label>
            <input onChange={this._handleInputChange.bind(this, 'passwordConfirm')}
                   id="passwordConfirm" type="password"
                   ref={c => this.passwordConfirm = c}/>
            <ul className="criteria error-list">
              <PasswordErrorListItem showOnError={true} text={i18n('loyaltysignin_0044') || "Passwords Don't Match"} valid={this.state.confirmPasswordRules.match} />
            </ul>
          </div>

          {
            this.props.errors ? false : <div className="form-actions">
              <button onClick={this._onContinue} tabIndex="0"
                   className="btn continue">{i18n('reservationwidget_0014')}</button>
            </div>
          }

        </div>
      </form>
    );
  }
}

AccountInfo.displayName = "AccountInfo";
