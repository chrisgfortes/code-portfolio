import ReservationCursors from '../../cursors/ReservationCursors';
import ConfirmAcceptTermsModal from './ConfirmAcceptTermsModal';
import Privacy from './Privacy';
import Terms from './Terms';
import DuplicateID from './DuplicateID';
import Modal from '../Modal/GlobalModal';

export default function EnrollModals ({enroll, modalAccept, verification, loginWidget, expedited, supportLinks}) {
  return (
    <div>
      {enroll.modal === 'confirm' ?
          <Modal active={enroll.modal}
                 cursor={ReservationCursors.enroll.concat('modal')}
                 header={i18n('eplusenrollment_0042')}
                 content={<ConfirmAcceptTermsModal accept={modalAccept} />}/> : false}

        {enroll.modal === 'privacy' ?
          <Modal active={enroll.modal}
                 cursor={ReservationCursors.enroll.concat('modal')}
                 header={i18n('eplusenrollment_0061')}
                 content={<Privacy verification={verification} />}/> : false}

        {enroll.modal === 'terms' ?
          <Modal active={enroll.modal}
                 cursor={ReservationCursors.enroll.concat('modal')}
                 header={i18n('eplusenrollment_0042')}
                 content={<Terms loginWidget={loginWidget}/>}/> : false}
        {enroll.modal === 'duplicateID' || enroll.modal === 'duplicateAcc' ?
          <Modal active={enroll.modal}
                 cursor={ReservationCursors.enroll.concat('modal')}
                 header={enroll.modal === 'duplicateID' ?
                    i18n('eplusenrollment_0076') : i18n('eplusenrollment_0070')}
                 content={<DuplicateID expedited={expedited}
                                       supportLinks={supportLinks}
                                       enroll={enroll} />}/> : false}
    </div>
  )
}

EnrollModals.displayName = "EnrollModals";