import { PasswordErrorListItem } from '../Password/PasswordErrorList';
import UrlUtil from '../../utilities/util-url';
import EnrollmentController from '../../controllers/EnrollmentController';
import Validator from '../../utilities/util-validator';

export default class CreatePasswordMemberInfo extends React.Component{
  constructor() {
    super();
    this.state = {
      lastName: null,
      userName: null,
      userNameEmpty: null
    };
    this.validator = new Validator(this, this.fieldMap);
    this.fieldMap = this.fieldMap.bind(this);
    this._handleInputChange = this._handleInputChange.bind(this);
  }
  componentDidMount() {
    let memberNumber = UrlUtil.getParameters().membernum;
    let lastName = UrlUtil.getParameters().last_name;
    if(memberNumber) {
      this.userName.value = memberNumber;
      EnrollmentController.setCreatePasswordDetails('userName', memberNumber[0]);
    }
    if(lastName) {
      this.lastName.value = lastName;
      EnrollmentController.setCreatePasswordDetails('lastName', lastName[0]);
    }
  }
  fieldMap() {
    let map = {
      refs: {
        lastName: this.lastName,
        userName: this.userName
      },
      value: {
        lastName: this.lastName.value,
        userName: this.userName.value
      },
      schema: {
        lastName: 'string',
        userName: 'string'
      }
    };
    if (this.userName.value.length > 0) {
      this.setState({
        userNameEmpty: false
      });
    } else {
      this.setState({
        userNameEmpty: true
      })
    }
    return map;
  }
  _handleInputChange (attribute, event) {
    this.setState(
      {
        [attribute]: event.target.value
      }
    );
    EnrollmentController.setCreatePasswordDetails(attribute, event.target.value);
  }

  render () {
    return (
      <div className="cf account-details active">
        <div className="field-container last-name">
          <label htmlFor="last-name">{i18n('reservationwidget_0022')}</label>
          <input id="last-name"
                 onChange={this._handleInputChange.bind(this, 'lastName')}
                 type="text"
                 ref={c => this.lastName = c}/>
        </div>
        <div className="field-container member-number">
          <label htmlFor="member-number">{i18n('loyaltyenrollment_0004') || 'Enterprise Plus Member Number'} 
            <span className="green" tabIndex="0" aria-describedby="enterprise-plus-member-info"> 
              <span id="enterprise-plus-member-info" className="tooltip" role="tooltip">
              {i18n('loyaltyenrollment_0007') || 'Enterprise Plus members earn free rentals through earned rewards. The more you rent, the more rewards you earn.'}
            </span>
              <span> ({i18n('locationsearch_0006') || "What's This?"})</span>
            </span>
          </label>
          <input id="member-number"
                 onChange={this._handleInputChange.bind(this, 'userName')}
                 type="text"
                 ref={c => this.userName = c}/>
          <ul className="criteria error-list">
            <PasswordErrorListItem showOnError={true} text={i18n('loyaltyenrollment_0005') || "Provide Enterprise Plus Member Number"} valid={!this.state.userNameEmpty} />
          </ul>
        </div>
      </div>
    )
  }
}

CreatePasswordMemberInfo.displayName = "CreatePasswordMemberInfo";
