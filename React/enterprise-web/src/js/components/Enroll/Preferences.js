import Validator from '../../utilities/util-validator';
import EnrollModals from './EnrollModals';
import classNames from 'classnames';
import EnrollmentController from '../../controllers/EnrollmentController';

export default class Preferences extends React.Component{
  constructor(props) {
    super(props);
    
    this.state = {
      specialOffers: false,
      acceptTerms: false,
      submitLoading: false
    };
    this.validator = new Validator(this, this.fieldMap);
    this.fieldMap = this.fieldMap.bind(this);
    this._handleInputChange = this._handleInputChange.bind(this);
    this._onContinue = this._onContinue.bind(this);
    this._expand = this._expand.bind(this);
    this._togglePrivacy = this._togglePrivacy.bind(this);
    this._toggleTerms = this._toggleTerms.bind(this);
    this._modalAccept = this._modalAccept.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if(this.props.enroll.issueCountry !== nextProps.enroll.issueCountry) {
      let marketingEmailCheck = false;
      if (nextProps.enroll.issueCountry) {
        marketingEmailCheck = nextProps.enroll.issueCountry.default_email_opt_in === false ? null : nextProps.enroll.issueCountry.default_email_opt_in;
        EnrollmentController.setProfileDetails('specialOffers', marketingEmailCheck);
      }
      this.setState({specialOffers: marketingEmailCheck})
    }
  }
  fieldMap () {
    return {
      value: {
        acceptTerms: this.acceptTerms.checked
      },
      schema: {
        acceptTerms: () => {
          return this.acceptTerms.checked;
        }
      },
      refs: {
        acceptTerms: this.acceptTermsLabel
      }
    };
  }
  _handleInputChange (attribute, event) {
    this.setState(
      {
        [attribute]: event.target.checked
      }
    );
    EnrollmentController.setProfileDetails(attribute, event.target.checked);
    if (!this.validator.validateAll().valid) {
      this.props.setEnrollProgress('preferencesProgress', false);
    }
  }
  _onContinue (event) {
    event.preventDefault();
    if (this.validator.validateAll().valid && this.state.submitLoading === false) {
      this.setState({submitLoading: true});
      EnrollmentController.mergeProfileDetails(this.fieldMap().value);
      EnrollmentController.createProfile().then((response)=> {
        this.setState({submitLoading: false});
        if (response === 'error') {
          this.props.setProgressAllFalse();
          EnrollmentController.setEnrollSection('all');
        } else {
          EnrollmentController.setSuccessProfile(response);
          EnrollmentController.setEnrollSuccess(true);
        }
      });
    } else {
      EnrollmentController.setEnrollModal('confirm');
    }
  }
  _expand () {
    if (this.props.enroll.section === 'preferences') {
      EnrollmentController.setEnrollSection(null);
    } else if (this.props.contactProgress && this.props.licenseProgress) {
      EnrollmentController.setEnrollSection('preferences');
    }
  }
  _togglePrivacy () {
    EnrollmentController.setEnrollModal('privacy');
  }
  _toggleTerms (event) {
    event.preventDefault();
    EnrollmentController.setEnrollModal('terms');
    this._handleInputChange('acceptTerms', {target: {checked: true}});
  }
  _modalAccept (event) {
    this._handleInputChange('acceptTerms', {target: {checked: true}});
    this.acceptTerms.checked = true;
    this._onContinue(event);
  }
  render () {
    const {submitLoading, specialOffers, acceptTerms} = this.state;
    const {enroll, preferencesProgress, accountProgress, contactProgress, licenseProgress,
      expedited, supportLinks, verification, loginWidget} = this.props;
    let preferencesClasses = classNames({
      'preferences': true,
      'active': enroll.section === 'preferences' || enroll.section === 'all'
    });
    let submitClasses = classNames({
      'btn': true,
      'continue': true,
      'disabled': submitLoading
    });
    let circleClasses = classNames({
      'numberCircle': true,
      'completed': preferencesProgress
    });
    let carrotClasses = classNames({
      'icon': true,
      'icon-nav-carrot-down': true,
      'active': enroll.section === 'preferences'
    });
    let enrollFormsClasses = classNames({
      'enroll-forms': true,
      'active': enroll.section === 'preferences'
    });

    return (
      <form className={enrollFormsClasses}>
        <h2
          className={(accountProgress && contactProgress && licenseProgress) ? '' : 'disabled'}
          onClick={this._expand}><span
          className={circleClasses}>{preferencesProgress ? <i
          className="icon icon-forms-checkmark-green"/> : '4'}</span>{i18n('eplusenrollment_0004')}
          <span onClick={this._expand} className={carrotClasses}/>
        </h2>

        <div className={preferencesClasses}>

          <h3>{i18n('eplusenrollment_0032')}
            ({i18n('reservationwidget_0040')})</h3>

          <div className="field-container requestPromotion">
            <input onChange={this._handleInputChange.bind(this, 'specialOffers')} id="specialOffers"
                   name="specialOffers" type="checkbox" checked={specialOffers}/>
            <label htmlFor="specialOffers">
              {i18n('resflowreview_0005')}
            </label>

            <p>{i18n('resflowreview_0006')} <a href="#" onClick={this._togglePrivacy}>{i18n('resflowreview_0008')}</a>
            </p>
          </div>
          <div className="field-container">
            <input onChange={this._handleInputChange.bind(this, 'acceptTerms')} id="acceptTerms"
                   ref={c => this.acceptTerms = c} name="acceptTerms" type="checkbox" checked={acceptTerms}/>
            <label ref={c => this.acceptTermsLabel = c} htmlFor="acceptTerms">
              {i18n('resflowreview_1007')}<a href="#" className="acceptTermsLink"
                                                                onClick={this._toggleTerms}>{i18n('eplusenrollment_0062')}</a>{i18n('resflowreview_1009')}
            </label>
          </div>
          <div className="form-actions">
            <div className="button-right">
              <div className={submitLoading ? 'loading' : false}/>
              <button onClick={this._onContinue} className={submitClasses}>
                {i18n('eplusenrollment_0033')}
              </button>
            </div>
          </div>
        </div>
        <EnrollModals modalAccept={this._modalAccept}
                      {...{enroll,
                           verification,
                           loginWidget,
                           expedited,
                           supportLinks}} />
      </form>
    );
  }
}

Preferences.displayName = "Preferences";
