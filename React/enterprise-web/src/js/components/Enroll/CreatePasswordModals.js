import ReservationCursors from '../../cursors/ReservationCursors';
import ConfirmAcceptTermsModal from './ConfirmAcceptTermsModal';
import Terms from './Terms';
import CreateEmailModal from '../Modal/CreateEmailModal';
import GlobalModal from '../Modal/GlobalModal';

export default function CreatePasswordModals ({modalAccept, enroll, isEmailModal, loginWidget}) {
  return (
    <div>
      {enroll.modal === 'confirm' ?
        <GlobalModal active={enroll.modal}
                     cursor={ReservationCursors.enroll.concat('modal')}
                     header={i18n('eplusenrollment_0042')}
                     content={<ConfirmAcceptTermsModal accept={modalAccept} />}/> : false}
      {enroll.modal === 'terms' ?
        <GlobalModal active={enroll.modal}
                     cursor={ReservationCursors.enroll.concat('modal')}
                     header={enterprise.i18nReservation.eplusenrollment_0042}
                     content={<Terms terms={loginWidget.terms} loginWidget={loginWidget} />}/> : false}
      {isEmailModal ? 
        <GlobalModal active={isEmailModal}
                     cursor={ReservationCursors.setEmailModal}
                     header=" "
                     content={<CreateEmailModal />}/> : false}
    </div>
  )
}

CreatePasswordModals.displayName ="CreatePasswordModals";