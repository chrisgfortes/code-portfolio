import {setEnrollModal} from '../../controllers/EnrollmentController';

export default function ConfirmAcceptTermsModal ({accept}) {
  return (
    <div className="confirm-accept">
      <h2>{i18n('eplusenrollment_0042')}</h2>
      <div>{i18n('eplusenrollment_0043')}</div>
      <div className="modal-actions">
        <div onClick={() => window.location = '/'} className="btn cancel">
        {i18n('eplusenrollment_0050')}
        </div>
        <div 
          onClick={(event) => {
            accept(event); 
            setEnrollModal(false);
          }} 
          className="btn save">
        {i18n('eplusenrollment_0037') + i18n('eplusenrollment_0062')}
        </div>
      </div>
    </div>
  );
}


ConfirmAcceptTermsModal.displayName = "ConfirmAcceptTermsModal";
