import DateTimePicker from '../BookingWidget/DateTimePicker';
import SubmitButton from '../BookingWidget/SubmitButton';
import BookingWidgetModelController from '../../controllers/BookingWidgetModelController';

export default class NoVehicleAvailabilityModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showDateTime: false
    };
    this._showDateTime = this._showDateTime.bind(this);
  }
  _showDateTime() {
    this.setState({showDateTime: true});
  }
  render () {
    const {modelController, inflightModify, employeeNumber, errors, setBookingWidgetUpdatedError,
          checkForExistingCorporateCode, clearEmployeeNumberError, toggleInflightModifyModal} = this.props;
    return (
      <div className="no-vehicles-error">
        {!this.state.showDateTime &&
        <div>
          <h2>{i18n('resflowlocations_0038')}</h2>
          <hr />
          <p>{i18n('resflowlocations_0039')}</p>

          <div className="btn-grp cf">
            <button onClick={BookingWidgetModelController.closeNoVehicleAvailabilityModal}
                    className="btn ok">{i18n('resflowlocations_0040')}</button>
            <h5 className="horizon-heading"><span
              className="conjunction-or">{i18n('resflowlocations_0041')}</span></h5>
            <button onClick={this._showDateTime}
                    className="btn ok">{i18n('resflowlocations_0042')}</button>
          </div>
        </div>
        }
        {this.state.showDateTime &&
        <div className="white-date-time-container cf booking-widget">
          <DateTimePicker modelController={this.props.modelController} isBookingController={this.props.isBookingController}/>
          <SubmitButton {...{modelController,
            inflightModify,
            employeeNumber,
            errors,
            setBookingWidgetUpdatedError,
            checkForExistingCorporateCode,
            clearEmployeeNumberError,
            toggleInflightModifyModal}} />
        </div>
        }
      </div>
    );
  }
}

NoVehicleAvailabilityModal.displayName = "NoVehicleAvailabilityModal";