const RedirectActions = require('../../actions/RedirectActions');

const TimedOut = React.createClass({
  mixins: [React.addons.PureRenderMixin],
  _onClick (event) {
    event.preventDefault();
    RedirectActions.goHome();
  },
  render: function () {
    return (
      <div className="error-page">
        <h1>{i18n('reservationnav_1001')}<i className="icon icon-nav-time-green"/></h1>
        <h2>{i18n('reservationnav_1002')}</h2>
        <p>{i18n('reservationnav_1003')}</p>
        <a onClick={this._onClick} href="#" className="btn btn-next">
          {i18n('resflowcorporate_0025')}
        </a>
      </div>
    );
  }
});

/*
 <div className="scene">
 <img className="car" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/43033/car.svg" alt="" />
 <img className="poof" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/43033/poof.svg" alt="" />
 <img className="sign" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/43033/sign.svg" alt="" />
 </div>
 */

module.exports = TimedOut;
