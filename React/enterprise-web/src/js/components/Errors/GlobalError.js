const GlobalError = React.createClass({
  getDefaultProps: function () {
    return {
      error: '',
      allowClose: false
    };
  },
  _onClose: function (event) {
    event.preventDefault();
    let parentComponent = $(this.getDOMNode()).parent();
    parentComponent.css('display', 'none');
  },
  componentDidMount () {
    // WCAG (ECR-9923)
    $('#globalErrorsContainer').focus();
  },
  render: function () {
    return (
      <li className="global-error" ref="container">
        <span className="error-prefix">{enterprise.i18nReservation.errormessage_0001} </span>
        {this.props.error} {enterprise.prod === 'false' ? this.props.code : false}
        {
          this.props.allowClose ?
            <button type="button" className="btn-close-error" onClick={this._onClose} aria-label={(i18n('resflowcarselect_0065') || 'Close')}>
              X</button> :
            false
        }
      </li>
    );
  }
});

module.exports = GlobalError;
