/**
 * I suspect this is no longer used?
 *
 * @type       {ReactComponent}
 */
const FieldError = React.createClass({
  getDefaultProps: function () {
    return {
      error: '',
      allowClose: false
    };
  },
  _onClose: function (value, event) {
    event.preventDefault();
    let parentComponent = $(this.getDOMNode()).parent();
    parentComponent.css('opacity', 0);
  },
  render: function () {
    return (
      <div className="field-error">
        {this.props.error}
        {
          this.props.allowClose ?
            <button type="button" className="btn-close-error" onClick={this._onClose.bind(this, 'privacy')}>X</button> :
            false
        }
      </div>
    );
  }
});

module.exports = FieldError;
