const GlobalError = require('./GlobalError');

const Error = React.createClass({
  /*
   error: {
   type: 'GLOBAL',
   msg: 'RSI Services Down',
   code: 'RSIDown123abc',
   priority: 'ERROR',
   shouldShowCallback: null,
   field: 'retrun_location'
   }
   */
  getDefaultProps: function () {
    return {
      allowClose: false
    };
  },
  render: function () {
    let errorTypeToRender = null;

    if (!this.props.errors) {
      return false;
    }

    switch (this.props.type) {
      case 'GLOBAL':
        errorTypeToRender = this.props.errors.map((error, i) => {
          return (
            <GlobalError error={error.defaultMessage}
                         code={error.code}
                         key={`${i}${error.code}`}
                         allowClose={this.props.allowClose}/>
          );
        });
        break;
      //The below can be potentially removed as it offers no difference from top REF: Burton
      case 'EXTRAS':
        errorTypeToRender = this.props.errors.map((error, i) => {
          return (
            <GlobalError error={error.defaultMessage}
                         code={error.code}
                         key={`${i}${error.code}`}
                         allowClose={this.props.allowClose}/>
          );
        });
        break;
      default:
        errorTypeToRender = null;
        break;
    }

    return (
      <div>
        {(errorTypeToRender.length > 0) && (
          <div>
            <h3 id="error-messages-heading" className="is-vishidden">
              {i18n('resflowreview_0139') || 'One or more errors were found. Here are the errors:'}
            </h3>
            <ul id="globalErrorsContainer" aria-labelledby="error-messages-heading" className="error-container" tabIndex="-1">
              {errorTypeToRender}
            </ul>
          </div>
        )}
      </div>
    );
  }
});

module.exports = Error;
