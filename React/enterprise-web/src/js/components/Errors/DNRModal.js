import {getDNRMessage} from '../../controllers/LoginController';
export default class DNRModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: false
    };
    this._confirm = this._confirm.bind(this);
    this._onChange = this._onChange.bind(this);
  }
  _confirm() {
    if (this.state.checked) {
      this.props.closeDNRModal();
    }
  }
  _onChange(event) {
    this.setState({
      checked: event.target.checked
    });
  }
  render () {
    const domain = window.location.host.split(".").pop();
    return (
      <div className="dnr">
        <h2>{enterprise.i18nReservation.dnr_0001}</h2>

        <p>{getDNRMessage(domain)}</p>
        <label htmlFor="dnrcheckbox"><input onChange={this._onChange} id="dnrcheckbox" name="dnrcheckbox"
                                            type="checkbox"
                                            checked={this.state.checked && 'checked'}/>{enterprise.i18nReservation.dnr_0003}
        </label>

        <div className="btn-grp cf">
          <button onClick={this._confirm}
                  className={"btn ok " + (this.state.checked ? '' : 'disabled')}>{enterprise.i18nReservation.dnr_0004}
          </button>
        </div>
      </div>
    );
  }
}

DNRModal.displayName = "DNRModal";
