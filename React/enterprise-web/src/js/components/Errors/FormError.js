/**
 * I suspect this is no longer used?
 *
 * @type       {ReactComponent}
 */
const FormError = React.createClass({
  getDefaultProps() {
    return {
      error: '',
      allowClose: false
    };
  },
  _onClose (value, event) {
    event.preventDefault();
    let parentComponent = $(this.getDOMNode()).parent();
    parentComponent.css('opacity', 0);
  },
  render () {
    return (
      <div className="form-error">
        {this.props.error}
        {
          this.props.allowClose ?
            <button type="button" className="btn-close-error" onClick={this._onClose.bind(this, 'privacy')}>X</button> :
            false
        }
      </div>
    );
  }
});

module.exports = FormError;
