import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';

import CorporateActions from '../../actions/CorporateActions';

import ReservationCursors from '../../cursors/ReservationCursors';

import {
  isContractTypeCorporate,
  isContractTypePromo,
  isCodeNotApplicable,
  selectCodeBannerData
} from '../../controllers/CorporateController';

const CodeBanner = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  getInitialState () {
    return {
      imageNotFound: false
    };
  },
  cursors: {
    confirmationNumber: ReservationCursors.confirmationNumber,
    contractDetails: ReservationCursors.contractDetails,
    deepLinkReservation: ReservationCursors.deepLinkReservation,
    viewTermsModal: ReservationCursors.viewTermsModal,
    codeApplicable: ReservationCursors.codeApplicable,
    resInitiateRequest: ReservationCursors.resRequest
  },
  _onRemove () {
    CorporateActions.showModalInState('removeCode');
  },
  viewTerms () {
    CorporateActions.viewTerms();
  },
  imageOnError () {
    this.setState({
      imageNotFound: true
    });
  },
  render () {
    const {
      contractDetails,
      confirmationNumber,
      deepLinkReservation,
      codeApplicable,
      resInitiateRequest
    } = this.state;

    const {
      contractData,
      promoCode,
      contractType,
      customLinks,
      contractImage,
      contractImagePath,
      showRemove
    } = selectCodeBannerData(contractDetails, deepLinkReservation, resInitiateRequest, confirmationNumber);

    // translations
    let corpPromoText = i18n('promotionemail_001', { code: promoCode });

    if (isContractTypePromo(contractType)) {
      corpPromoText = (
        <span className="code-type promo">
          <i className="icon icon-alert-success" />
          {i18n('promotionemail_0003')}
        </span>
      );

      if (isCodeNotApplicable(codeApplicable)) {
        corpPromoText = (
          <span className="code-type promo">
            <i className="icon icon-slash" />
            {i18n('promotionemail_0019')}
          </span>
        );
      }
    } else if (isContractTypeCorporate(contractType)) {
      corpPromoText = (
        <span className="code-type promo">
          <i className="icon icon-alert-success" />
          {i18n('resflowcarselect_8011')}
        </span>
      );

      if (isCodeNotApplicable(codeApplicable)) {
        corpPromoText = (
          <span className="code-type promo">
            <i className="icon icon-slash" />
            {i18n('promotionemail_0021')}
          </span>
        );
      }
    }

    return (!!contractData &&
      <div className="code-banner header-nav-item">
        {contractImage &&
          <div className={`corporate-image ${this.state.imageNotFound ? 'not-found' : ''}`}>
            <img
              src={contractImagePath}
              onError={this.imageOnError}
              alt="" />
          </div>
        }
        <div className="code-banner-details">
          <div className="banner-top">
            <span className="corporate-account-name">{contractData.contract_name}</span>
            {corpPromoText}
          </div>
          <div className="banner-bottom cf">
            {!!showRemove &&
              <div className="banner-bottom-item">
                <div onClick={this._onRemove} className="remove-button banner-bottom-item-link">
                  {i18n('resflowcorporate_4032')}
                  <i className="icon icon-remove-white-filled" />
                </div>
                {!!(contractData.terms_and_conditions || customLinks.length) &&
                  <span aria-hidden="true" className="separator">|</span>
                }
              </div>
            }
            {!!contractData.terms_and_conditions &&
              <div className="banner-bottom-item">
                <div onClick={this.viewTerms} className="view-terms banner-bottom-item-link">
                  {i18n('promotionemail_0200')}
                </div>
                {!!customLinks.length &&
                  <span aria-hidden="true" className="separator">|</span>
                }
              </div>
            }
            {customLinks.slice(0, 3).map((link, i) =>
              <div className="banner-bottom-item" key={i}>
                <a
                  className="banner-bottom-item-link"
                  href={link.url}
                  target="_blank"
                  key={i}
                >
                  {link.label}
                </a>
                {i < (customLinks.length - 1) &&
                  <span aria-hidden="true" className="separator">|</span>
                }
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
});

module.exports = CodeBanner;

