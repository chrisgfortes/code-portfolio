import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';

import CorporateActions from '../../actions/CorporateActions';

import ReservationCursors from '../../cursors/ReservationCursors';

import Validator from '../../mixins/Validator';

/*

 Additional field item explanation,
 Display_on_splash: asked to be displayed pre-rate
 Modifiable: Modify flow, not modifiable
 Sequence: Order to display the fields
 Required: Make them required to enter
 Type: Possible types: Date, Drop Down List, Exact Value, Pattern, Text

 */

const AdditionalInformation = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    corporate: ReservationCursors.corporate,
    session: ReservationCursors.reservationSession
  },
  compare (a, b) {
    //Sort list of additional fields based on sequence attribute
    if (a.sequence < b.sequence) {
      return -1;
    }
    if (a.sequence > b.sequence) {
      return 1;
    }
    return 0;
  },
  combineSavedAdditionalWithPostRate () {
    let fields = this.state.session.additionalInformationAll.length > 0 ? this.state.session.additionalInformationAll : [];
    let savedFields = this.state.session.additionalInformationSaved.length > 0 ? this.state.session.additionalInformationSaved : [];

    fields = fields.sort(this.compare);
    savedFields = savedFields.sort(this.compare);

    if (savedFields.length) {
      fields.forEach((field) => {
        let sameField = savedFields.filter((savedField) => {
          return savedField.id === field.id;
        });
        if (sameField.length) {
          field.value = sameField[0].value;
        }
      });
    }
    return fields;
  },
  render: function () {
    let sorted = this.combineSavedAdditionalWithPostRate();

    return (
      !!sorted.length &&
      <div className="section-content">
        <AdditionalInformationFields fields={sorted}/>
      </div>
    );
  }
});

const AdditionalInformationFields = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    session: ReservationCursors.reservationSession,
    deepLinkReservation: ReservationCursors.deepLinkReservation,
    additionalInfo: ReservationCursors.additionalInfo,
  },
  render () {
    const deepLinkAdditionalInfo = _.get(this.state.deepLinkReservation, 'reservation.additional_information');
    const commitRequest = ReservationStateTree.select(ReservationCursors.commitRequestData).get();
    const commitRequestAddInfo = commitRequest && commitRequest.additional_information;
    return (
      <div>
        {this.props.fields.length > 0 ?
          <div className="option-block">
            {this.props.disableHeader ? false :
              <div className="view-header">{i18n('resflowcorporate_0020').toLowerCase()}</div>}
            {this.props.fields.map((item, index) => {
              const disabled = this.state.session.confirmationNumber && !item.modifiable;
              const deepLinkPrepop = deepLinkAdditionalInfo && deepLinkAdditionalInfo.find(info => info.id === item.id);
              const commitRequestPrepop = commitRequestAddInfo && commitRequestAddInfo.find(function (info) {
                if (info === null) { return false; }
                if (item === null) { return false; }
                return info.id === item.id;
              });
              let fedexPrepop;
              if (this.state.session.fedexReservation && this.state.additionalInfo) {
                fedexPrepop = this.state.additionalInfo.find(info => info.id === item.id);
              }
              const prepopedValue = (deepLinkPrepop && deepLinkPrepop.value) || (commitRequestPrepop && commitRequestPrepop.value) || (fedexPrepop && fedexPrepop.value);

              if (!item.validate_additional_info && this.props.prerate) {
                return false;
              }

              if (item.type === 'TEXT' || item.type === 'EXACTVALUE' || item.type === 'PATTERN') {
                return (
                  <Text key={index}
                    item={item}
                    validate={this.props.validate ? this.props.validate : null}
                    prepopedValue={prepopedValue}
                    disabled={disabled}/>
                );
              }
              if (item.type === 'DATE') {
                return (
                  <Date key={index}
                    item={item}
                    validate={this.props.validate ? this.props.validate : null}
                    prepopedValue={prepopedValue}
                    disabled={disabled}/>
                );
              }
              if (item.type === 'DROPDOWN') {
                return (
                  <Dropdown key={index}
                    item={item}
                    validate={this.props.validate ? this.props.validate : null}
                    prepopedValue={prepopedValue}
                    disabled={disabled}/>
                );
              }
              return false;
            })}
          </div> : false}
      </div>
    );
  }
});

const Dropdown = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin,
    Validator
  ],
  componentDidMount () {
    const value = this.props.item.value || this.props.prepopedValue;

    if (value) {
      this.setAndValidate(this.props.item.id, value);
    }
  },
  fieldMap () {
    let item = this.props.item;

    return {
      value: {
        [item.id]: item.id
      },
      schema: {
        [item.id]: 'string'
      }
    };
  },
  cursors: {
    session: ReservationCursors.reservationSession
  },
  setAndValidate (id, value) {
    CorporateActions.setAdditionalInfo({
      id: id,
      value: value
    });
    if (this.props.validate) {
      this.props.validate();
    } else {
      this.validate(id, value);
    }
  },
  _handleChange (id, event) {
    this.setAndValidate(id, event.target.value);
  },
  render: function () {
    let item = this.props.item;
    return (
      <fieldset>
        <label htmlFor={item.id}>{item.name}</label>
        {item.required ? false
          : ' (' + enterprise.i18nReservation.reservationwidget_0040 + ')'}
        <div>
          <select className="styled" ref="input" onChange={this._handleChange.bind(this, item.id)}
                  id={item.id}
                  ref={item.id}
                  disabled={this.props.disabled}
                  defaultValue={item.value || this.props.prepopedValue || ''}>
            <option value="" disabled>{enterprise.i18nReservation.resflowlocations_0021}</option>
            {item.supported_values && item.supported_values.map((option, index)=> {
              return <option key={index} value={option.display_text}>{option.display_text}</option>;
            })}
          </select>
        </div>
        <p className="helperText">{item.helper_text}</p>
      </fieldset>
    );
  }
});

const Text = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin,
    Validator
  ],
  cursors: {
    session: ReservationCursors.reservationSession
  },
  componentDidMount () {
    const value = this.props.item.value || this.props.prepopedValue;

    if (value) {
      this.setAndValidate(this.props.item.id, value);
    }
  },
  fieldMap () {
    let item = this.props.item;

    return {
      value: {
        [item.id]: item.id
      },
      schema: {
        [item.id]: 'string'
      }
    };
  },
  setAndValidate (id, value) {
    CorporateActions.setAdditionalInfo({
      id: id,
      value: value
    });

    if (this.props.validate) {
      this.props.validate();
    } else {
      this.validate(id, value);
    }
  },
  _handleChange (id, event) {
    this.setAndValidate(id, event.target.value);
  },
  render: function () {
    let item = this.props.item;
    return (
      <fieldset>
        <label htmlFor={item.id}>{item.name}
        {item.required ? false : ' (' + (i18n('reservationwidget_0040') || 'Optional') + ')'}</label>
        <div>
          <input type={item.type}
                 id={item.id}
                 ref={item.id}
                 name="purpose"
                 disabled={this.props.disabled}
                 defaultValue={item.value || this.props.prepopedValue || ''}
                 onChange={this._handleChange.bind(this, item.id)}/>
        </div>
        {item.helper_text ? <p className="helper-text">{item.helper_text}</p> : null}
      </fieldset>
    );
  }
});

const Date = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin,
    Validator
  ],
  cursors: {
    session: ReservationCursors.reservationSession
  },
  componentDidMount () {
    const value = this.props.item.value || this.props.prepopedValue;

    if (value) {
      this.setAndValidate(this.props.item.id, value);
    }
  },
  fieldMap () {
    let item = this.props.item;

    return {
      value: {
        [item.id]: item.id
      },
      schema: {
        [item.id]: 'string'
      }
    };
  },
  setAndValidate (id, value) {
    CorporateActions.setAdditionalInfo({
      id: id,
      value: value
    });
    if (this.props.validate) {
      this.props.validate();
    } else {
      this.validate(id, value);
    }
  },
  _handleChange (id, event) {
    this.setAndValidate(id, event.target.value);
  },
  render: function () {
    let item = this.props.item;
    return (
      <fieldset>
        <label htmlFor={item.id}>{item.name}</label>
        {item.required ? false
          : ' (' + enterprise.i18nReservation.reservationwidget_0040 + ')'}
        <div>
          <input type={item.type}
                 id={item.id}
                 name="purpose"
                 placeholder={enterprise.i18nReservation.eplusenrollment_1030}
                 disabled={this.props.disabled}
                 defaultValue={item.value || this.props.prepopedValue || ''}
                 onChange={this._handleChange.bind(this, item.id)}/>
        </div>
      </fieldset>
    );
  }
});


module.exports = {
  AdditionalInformation,
  AdditionalInformationFields
};
