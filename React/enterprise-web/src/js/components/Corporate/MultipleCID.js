import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import ReservationCursors from '../../cursors/ReservationCursors';
import SessionService from '../../services/SessionService';
import ReservationActions from '../../actions/ReservationActions';

const MultipleCode = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    coupon: ReservationCursors.coupon,
    session: ReservationCursors.reservationSession
  },
  _onRestart () {
    if (enterprise.currentView === 'reserve') {
      SessionService.clearSession({removeCID: true})
                    .then(() => window.location = enterprise.aem.path + '.html');
    } else {
      // ECR-14251
      const CID = (
        _.get(this.state.session, 'profile.basic_profile.customer_details.contract_number') ||
        _.get(this.state.session, 'contract_details.contract_number')
      );
      ReservationActions.setCoupon(CID);
      this._onContinue();
    }
  },
  _onContinue () {
    this.props.modelController.submit();
  },
  render: function () {
    const session = this.state.session;
    const existingCID = (
      _.get(session, 'profile.basic_profile.customer_details.contract_name') ||
      _.get(session, 'contract_details.contract_name')
    );

    return (
      <div className="manage-cid corporate">
        <h2>{enterprise.i18nReservation.resflowcorporate_4010}</h2>

        <div className="top-disclaimer">
          {i18n('resflowcorporate_0051', { ContractName: this.state.coupon })}
        </div>
        <div onClick={this._onContinue} className="btn submit">{i18n('reservationwidget_0014')}</div>
        <div className="bottom-disclaimer">
          {existingCID ?
            i18n('resflowcorporate_0054', { account: existingCID }) :
            i18n('resflowcorporate_0089')
          }
        </div>
        <div onClick={this._onRestart}
             className="btn cancel">{i18n('resflowcorporate_0055', { account: existingCID })}</div>

      </div>
    );
  }
});

module.exports = MultipleCode;
