import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import ReservationCursors from '../../cursors/ReservationCursors';
import CorporateActions from '../../actions/CorporateActions';
import RedirectActions from '../../actions/RedirectActions';
import SessionService from '../../services/SessionService';

const RemoveCode = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    session: ReservationCursors.reservationSession
  },
  _confirmPurpose () {
    SessionService.clearSession()
                  .then(RedirectActions.goHome);
  },
  _onContinue () {
    CorporateActions.setModal(false);
  },
  render: function () {
    const corporate = this.state.session.contract_details;
    let label = i18n('resflowcorporate_4016');
    let question = i18n('resflowcorporate_4018');
    let disclaimer = i18n('resflowcorporate_4020');

    if (corporate && corporate.contract_type === 'CORPORATE') {
      label = i18n('resflowcorporate_4015');
      question = i18n('resflowcorporate_4017');
      disclaimer = i18n('resflowcorporate_4019');
    }
    return (
      <div className="travel-purpose corporate">
        <h2>{i18n('resflowcorporate_4032')} {label}</h2>
        <div>{question}</div>
        <div>{disclaimer}</div>
        <div onClick={this._confirmPurpose} className="btn">{i18n('resflowcorporate_0025')}</div>
        <div onClick={this._onContinue} className="continue">{i18n('resflowcorporate_4021')}</div>
      </div>
    );
  }
});

module.exports = RemoveCode;
