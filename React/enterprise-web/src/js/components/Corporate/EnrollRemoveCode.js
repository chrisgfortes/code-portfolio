'use strict';

var EnrollActions = require('../../actions/EnrollActions'),
  CorporateActions = require('../../actions/CorporateActions');

var BaobabReactMixinBranch = require('baobab-react/mixins').branch;

var EnrollRemoveCode = React.createClass({
  mixins: [
    React.addons.PureRenderMixin
  ],
  _confirmPurpose () {
    EnrollActions.setProfileDetails('cid', null);
    CorporateActions.setModal(false);
  },
  _onContinue () {
    CorporateActions.setModal(false);
  },
  render: function () {
    return (
      <div className="enroll-remove corporate">
        <h2>{enterprise.i18nReservation.resflowcorporate_4006}</h2>

        <div className="sub-warning">
          {enterprise.i18nReservation.resflowcorporate_4007}
        </div>
        <div onClick={this._onContinue} className="btn">{enterprise.i18nReservation.resflowcorporate_4008}</div>
        <div onClick={this._confirmPurpose}
             className="continue">{enterprise.i18nReservation.resflowcorporate_4009}</div>
      </div>
    );
  }
});

module.exports = EnrollRemoveCode;

