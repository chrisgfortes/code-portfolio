const ReservationCursors = require('../../cursors/ReservationCursors');
const CorporateActions = require('../../actions/CorporateActions');
const Validator = require('../../mixins/Validator');
const BaobabReactMixinBranch = require('baobab-react/mixins').branch;

const DeliveryCollection = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    session: ReservationCursors.reservationSession,
    model: ReservationCursors.model
  },
  getInitialState () {
    CorporateActions.setDelivery('enabled', false);
    CorporateActions.setCollection('enabled', false);
    return {
      delivery: false,
      collection: false,
      sameAddress: 'true'
    };
  },
  _onCheck (type, event) {
    let formatType = type === 'delivery' ? 'setDelivery' : 'setCollection';
    this.setState({
      [type]: event.target.checked
    });
    CorporateActions[formatType]('enabled', event.target.checked);
  },
  _onRadio (event) {
    this.setState({
      sameAddress: event.target.value
    });
    this.setDeliveryFields();
  },
  setDeliveryFields () {
    setTimeout(()=> {
      if (this.state.sameAddress === 'true') {
        CorporateActions.setCollectionSameAsDelivery();
      } else {
        CorporateActions.setCollectionSameAsDelivery(true);
      }
    }, 50);
  },
  render: function () {
    const session = this.state.session;
    const travelOnBusiness = session.travel_purpose === 'BUSINESS' || this.state.model.purpose === 'BUSINESS';
    let delivery = session.delivery_allowed;
    let collection = session.collection_allowed;
    const contractHasBenefit = _.has(session, 'contract_details.contract_has_additional_benefits') && session.contract_details.contract_has_additional_benefits;

    if (contractHasBenefit && travelOnBusiness && (delivery || collection)) {
      return (
        <div className="section-content">
          <div className="option-block delivery-collection">
            <div className="view-header">
              {delivery ? i18n('resflowcorporate_0003').toLowerCase() : false} {delivery && collection ? ' & ' : false} {collection ? i18n('resflowcorporate_0004').toLowerCase() : false}</div>

            <div className="dnc-disclaimer">{i18n('resflowcorporate_0032')}</div>
            {delivery ? <div className="delivery">
              <input id="delivery"
                     onChange={this._onCheck.bind(this, 'delivery')}
                     checked={this.state.delivery}
                     name="delivery" type="checkbox"/>
              <i className="icon icon-forms-checkmark-green"></i>
              <label className="inline-label" htmlFor="delivery">
                {i18n('resflowcorporate_0033')}
              </label>
            </div> : false }

            {this.state.delivery ? <Fields type="Delivery" sameAddress={this.state.sameAddress}/> : false}

            {collection ? <div className="collection">
              <input id="collection"
                     onChange={this._onCheck.bind(this, 'collection')}
                     name="collection" type="checkbox"
                     checked={this.state.collection}
                />
              <label className="inline-label" htmlFor="collection">
                {i18n('resflowcorporate_0035')}
              </label>
            </div> : false}

            {delivery && this.state.delivery && this.state.collection ? <div className="same-address">
              <div>
                <input type="radio"
                       name="copyAddress"
                       id="sameAddress"
                       value="true"
                       checked={this.state.sameAddress === 'true'}
                       ref="sameAddress"
                       onChange={this._onRadio}/>
                <label className="inline-label"
                       htmlFor="sameAddress">{i18n('resflowcorporate_0043')}</label>
              </div>
              <div>
                <input type="radio"
                       name="copyAddress"
                       id="differentAddress"
                       checked={this.state.sameAddress === 'false'}
                       onChange={this._onRadio}
                       value="false"/>
                <label className="inline-label"
                       htmlFor="differentAddress">{i18n('resflowcorporate_0044')}</label>
              </div>
            </div> : false}

            {this.state.collection && ((this.state.delivery && this.state.sameAddress === 'false') || (!this.state.delivery)) ?
              <Fields type="Collection"/> :
              this.state.collection && (this.state.delivery && this.state.sameAddress === 'true') ?
                <CollectionComments /> : false
            }
          </div>
        </div>);
    } else {
      return false;
    }
  }
});

const Fields = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin,
    Validator
  ],
  fieldMap () {
    return {
      value: {
        streetAddress: this.refs.streetAddress.getDOMNode().value,
        city: this.refs.city.getDOMNode().value,
        phone: this.refs.phone.getDOMNode().value
      },
      schema: {
        streetAddress: 'string',
        city: 'string',
        phone: 'phone'
      }
    };
  },
  cursors: {
    session: ReservationCursors.reservationSession,
    verification: ReservationCursors.verification,
    personal: ReservationCursors.personal,
    account: ReservationCursors.account
  },
  getInitialState () {
    return {
      comments: null
    };
  },
  _onChange (field, event) {
    if (field === 'comments') {
      this.setState({
        comments: event.target.value
      });
    }
    CorporateActions['set' + this.props.type](field, event.target.value);
    if (this.props.sameAddress && this.props.type === 'Delivery') {
      if (field !== 'comments') {
        CorporateActions['setCollection'](field, event.target.value);
      }
    }
    this.validate(field, event.target.value);
  },
  render: function () {
    return (
      <div className="delivery-collection-fields">
        <div className="field-container">
          <label htmlFor={this.props.type + 'address'}>{i18n('eplusaccount_0049')}</label>
          <input onChange={this._onChange.bind(this, 'streetAddress')} id={this.props.type + 'address'}
                 ref="streetAddress"
                 type="text"/>
        </div>

        <div className="field-container city">
          <label htmlFor={this.props.type + 'city'}>{i18n('resflowreview_0065')}</label>
          <input onChange={this._onChange.bind(this, 'city')} id={this.props.type + 'city'}
                 ref="city"
                 type="text"/>
        </div>

        <div className="field-container postal">
          <label htmlFor={this.props.type + 'postal'}>{i18n('resflowreview_0068')}
            ({i18n('reservationwidget_0040')})</label>
          <input onChange={this._onChange.bind(this, 'postal')} id={this.props.type + 'postal'}
                 type="text"/>
        </div>

        <div className="field-container phone">
          <label htmlFor={this.props.type + 'phone'}>{i18n('resflowreview_0010')}</label>
          <input onChange={this._onChange.bind(this, 'phone')}
                 id={this.props.type + 'phone'} type="text"
                 ref="phone" pattern="[0-9]*"/>
        </div>
        <div className="field-container comments">
          <label htmlFor={this.props.type + 'comments'}>Comments
            ({i18n('reservationwidget_0040')})</label>
                    <textarea onChange={this._onChange.bind(this, 'comments')}
                              id={this.props.type + 'comments'} type="text" maxLength="200"
                              ref="comments"/>

          <div className="word-count">{this.state.comments ? this.state.comments.length : 0}/200</div>
        </div>
      </div>
    );
  }
});


const CollectionComments = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    model: ReservationCursors.model
  },
  getInitialState () {
    return {
      comments: null
    };
  },
  _onChange (field, event) {
    if (field === 'comments') {
      this.setState({
        comments: event.target.value
      });
    }
    CorporateActions['setCollection'](field, event.target.value);
  },
  render: function () {
    return (
      <div className="delivery-collection-fields">
        <div className="field-container comments">
          <label htmlFor="Collectioncomments">Comments</label>
                      <textarea onChange={this._onChange.bind(this, 'comments')}
                                value={this.state.model.collection.comments}
                                id="Collectioncomments" type="text" maxLength="200"
                                ref="comments"/>

          <div className="word-count">{this.state.comments ? this.state.comments.length : 0}/200</div>
        </div>
      </div>);
  }
});

module.exports = DeliveryCollection;
