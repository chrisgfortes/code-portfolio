'use strict';

var ReservationCursors = require('../../cursors/ReservationCursors'),
  AccountService = require('../../services/AccountService'),
  BookingWidgetService = require('../../services/BookingWidgetService'),
  CorporateActions = require('../../actions/CorporateActions'),
  classNames = require('classnames');

var BaobabReactMixinBranch = require('baobab-react/mixins').branch;

var ChangeTripPurpose = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  _confirmPurpose () {
    window.location = enterprise.aem.path + '.html';
  },
  _onContinue () {
    CorporateActions.setModal(false);
  },
  render: function () {

    let confirmClasses = classNames({
      'btn': true,
      'disabled': false
    });

    return (
      <div className="travel-purpose corporate">
        <header>
          <h2>{enterprise.i18nReservation.resflowcorporate_0023}</h2>
        </header>
        <fieldset>
          <div>{enterprise.i18nReservation.resflowcorporate_0024}</div>
          <div onClick={this._confirmPurpose}
               className={confirmClasses}>{enterprise.i18nReservation.resflowcorporate_0025}</div>
          <div onClick={this._onContinue} className="continue">{enterprise.i18nReservation.resflowcorporate_0026}</div>
        </fieldset>
      </div>
    );
  }
});

module.exports = ChangeTripPurpose;

