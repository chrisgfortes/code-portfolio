const ReservationCursors = require('../../cursors/ReservationCursors');
const CorporateActions = require('../../actions/CorporateActions');
const BaobabReactMixinBranch = require('baobab-react/mixins').branch;

const TravelPurpose = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    corporate: ReservationCursors.corporate,
    session: ReservationCursors.reservationSession
  },
  componentWillMount () {
    let purpose = this.state.session.travel_purpose;
    if (purpose) {
      CorporateActions.setTravelPurpose(purpose);
    }
    /* else if (this.state.session.deeplink_billing_account) {
     CorporateActions.setTravelPurpose('BUSINESS');
     }*/
  },
  render: function () {
    const session = this.state.session;
    const contractHasBenefit = _.has(session, 'contract_details.contract_has_additional_benefits') && session.contract_details.contract_has_additional_benefits;
    const tripPurposeCondition = contractHasBenefit && session.contract_details;
    const preRatePostRate = session.travel_purpose && !session.rebookCancel && !session.inflightModification ? <PreRatePurpose /> : <PostRatePurpose />;

    //Question is only prompted if either additional info, D&C are offered
    const content = tripPurposeCondition ? preRatePostRate : false;

    return (
      content &&
      <div className="section-content">
        {content}
      </div>
    );
  }
});

const PreRatePurpose = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    session: ReservationCursors.reservationSession
  },
  _restatePurpose () {
    CorporateActions.setModal(true);
    CorporateActions.setCorporateState('changePurpose');
  },
  render: function () {
    return (
      <div className="option-block travel-purpose">
        <div className="view-header">{i18n('resflowcorporate_0017').toLowerCase()}</div>

        <div>{i18n('resflowcorporate_0021')}</div>
        <a onClick={this._restatePurpose} className="cta">{i18n('resflowcorporate_0022')}</a>
      </div>
    );
  }
});

const PostRatePurpose = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    session: ReservationCursors.reservationSession,
    model: ReservationCursors.model
  },
  componentWillMount () {
    if (this.state.session.deeplink_billing_account) {
      CorporateActions.setTravelPurpose('BUSINESS');
    }
  },
  _handleChange (event) {
    CorporateActions.setTravelPurpose(event.target.value);
    CorporateActions.setPaymentID(null);
    CorporateActions.setPaymentType(null);
    CorporateActions.setBillingAuthorization('false');
    this.refs.label.getDOMNode().classList.remove('invalid');
  },
  render: function () {
    const session = this.state.session;
    const accountName = session.contract_details ? session.contract_details.contract_name : '';
    const question = i18n('resflowcorporate_0019') ? i18n('resflowcorporate_0019').replace(/#{name}/g, accountName) : '';

    return (
      <div className="option-block travel-purpose">
        <div className="view-header">{i18n('resflowcorporate_0017').toLowerCase()}</div>
        <fieldset>
          <legend ref="label" className="travel-purpose-label">
            {question}
            <abbr title={i18n('resflowcorporate_4026')}></abbr>
          </legend>
          <div>
            <input type="radio"
                   name="purpose"
                   id="business"
                   value="BUSINESS"
                   onChange={this._handleChange}
                   checked={this.state.model.purpose === 'BUSINESS'}
              />
            <label className="inline-label"
                   htmlFor="business">{i18n('resflowcorporate_0200')}</label>
          </div>
          <div>
            <input type="radio"
                   name="purpose"
                   id="leisure"
                   value="LEISURE"
                   onChange={this._handleChange}
                   checked={this.state.model.purpose === 'LEISURE'}
              />
            <label className="inline-label"
                   htmlFor="leisure">{i18n('resflowcorporate_0201')}</label>
          </div>
        </fieldset>
      </div>
    );
  }
});

module.exports = TravelPurpose;
