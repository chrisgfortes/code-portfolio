const ReservationCursors = require('../../cursors/ReservationCursors');
const CorporateActions = require('../../actions/CorporateActions');
const Validator = require('../../mixins/Validator');
const Error = require('../Errors/Error');
const BaobabReactMixinBranch = require('baobab-react/mixins').branch;
const classNames = require('classnames');

const Pin = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin,
    Validator
  ],
  cursors: {
    coupon: ReservationCursors.coupon,
    session: ReservationCursors.reservationSession,
    errors: ['view', 'corporate', 'error']
  },
  getInitialState () {
    return {
      loading: false,
      pin: null
    };
  },
  _onInputChange (pin, event) {
    let pinValue = event.target.value;
    CorporateActions.setPin(pinValue);
    this.setState({
      pin: pinValue
    });
  },
  _onConfirm () {
    if (this.state.pin) {
      this.setState({loading: true});
      this.props.modelController.submit(() => {
        this.setState({loading: false});
      });
    }
  },
  render () {
    let confirmClasses = classNames({
      'btn': true,
      'disabled': this.state.loading || !this.state.pin
    });
    return (
      <div className="pin corporate">
        {this.state.loading ?
          <div className="transition"></div>
          : <div>
          <header>
            <h2>{i18n('resflowcorporate_0082') || 'Please Enter Your PIN'}</h2>
          </header>
          <Error errors={this.state.errors} type="GLOBAL"/>
          <div
            className="disclaimer">{i18n('resflowcorporate_0083') || "To apply your corporate account, please enter the first three characters of your company's name or PIN."}</div>
          <div className="field-container pin-field">
            <input onChange={this._onInputChange.bind(this, 'pin')}
                   id="pin" type="text"
                   maxLength="3"
                   ref="pin"
                   value={this.state.pin}/>

            <div className="hints">
              <strong>{i18n('resflowcorporate_0084') || 'Examples:'}</strong>

              <div>{i18n('resflowcorporate_0085') || '"St. Charles Lumber" = "STC"'}</div>
              <div>{i18n('resflowcorporate_0086') || '"A-1 Corporation" = "A1C"'}</div>
            </div>
          </div>
          <div className="modal-action">
            <div className={this.state.loading ? 'loading' : false}></div>
            <button className={confirmClasses}
                    onClick={this._onConfirm}>{enterprise.i18nReservation.resflowcorporate_0078}
            </button>
          </div>
        </div>

        }
      </div>
    );
  }
});

module.exports = Pin;
