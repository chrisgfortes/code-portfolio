import PaymentModelController from '../../controllers/PaymentModelController';
const ReservationCursors = require('../../cursors/ReservationCursors');
const CorporateActions = require('../../actions/CorporateActions');
const Validator = require('../../mixins/Validator');
const Settings = require('../../settings');
import {PRICING} from '../../constants/';

const BaobabReactMixinBranch = require('baobab-react/mixins').branch;

const Billing = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  getInitialState () {
    return {
      authorized: 'false',
      containsBN: false,
      containsCC: false,
      defaultProvided: 0,
      preferredType: null,
      preferredNumber: null,
      ecUnauth: false
    };
  },
  cursors: {
    corporate: ReservationCursors.corporate,
    session: ReservationCursors.reservationSession,
    model: ReservationCursors.model,
    driverInfoIdentifier: ReservationCursors.driverInfoIdentifier,
    driverInfoLoyalty: ReservationCursors.driverInfoLoyalty,
    driverInfoPaymentProfile: ReservationCursors.driverInfoPaymentProfile
  },
  // @todo: this gets replaced when Settings is refactored
  // starting here:
  strs: {
    loyalty: {}
  },
  getLoyalty () {
    return {
      emeraldClub: _.get(Settings, '_collection[0]str.loyalty.emeraldClub', '')
    };
  },
  // ending here
  determineUnauthBilling (payments) {
    let associated = ((this.state.driverInfoIdentifier) ? true : false) && (this.state.driverInfoLoyalty === this.strs.loyalty.emeraldClub);
    let payBillingType = (this.state.driverInfoPaymentProfile && this.state.driverInfoPaymentProfile.length);
    let paycodes;
    if (associated && payBillingType) {
      this.setState({
        ecUnauth: true
      });
      paycodes = this.state.driverInfoPaymentProfile;
    } else {
      paycodes = payments;
    }
    return paycodes;
  },
  determinePaymentDefault () {
    this.strs.loyalty = this.getLoyalty();

    let payments = null;
    let profile = this.state.session.profile;
    // ECR-14251 NO!!!!!!!
    if ((profile && profile.basic_profile.customer_details)) {
      if (!this.state.session.contract_details || this.state.session.contract_details.contract_number === profile.basic_profile.customer_details.contract_number) {
        payments = profile.payment_profile.payment_methods;
      }
    }
    // ec unauthenticated users
    // weird race condition with setting the ecUnauth state so we pass stuff around
    payments = this.determineUnauthBilling(payments);

    // ECR-14251 NO!!!!!!!
    let billingAccount = this.state.session.contract_details ? this.state.session.contract_details.contract_billing_account : false;
    let authorized = 'false';

    if (payments) {
      let i;
      let len = payments.length;
      for (i = 0; i < len; i++) {
        if (payments[i].preferred && !billingAccount) {
          CorporateActions.setDefaultPaymentType(payments[i].payment_type);
          if (PaymentModelController.isBusinessAccount(payments[i])) {
            authorized = 'true';
          }
          this.setState({
            defaultProvided: i
          });
          CorporateActions.setPaymentID(payments[i].payment_reference_id);
          CorporateActions.setPaymentType(payments[i].payment_type);
        }
        if (PaymentModelController.isBusinessAccount(payments[i])) {
          this.setState({
            containsBN: true
          });
        }
        if (payments[i].payment_type === PRICING.CREDIT) {
          this.setState({
            containsCC: true
          });
        }
      }
    }

    if (billingAccount) {
      authorized = 'true';
    } else if (this.state.session.deeplink_billing_account) {
      authorized = 'true';
    }

    this.setState({
      authorized: authorized
    });

    CorporateActions.setBillingAuthorization(authorized);
  },
  componentWillMount () {
    this.determinePaymentDefault();
  },
  _onChangeCreditCard (event) {
    CorporateActions.setPaymentID(event.target.value);
  },
  _onRadio (event) {
    let payments;
    if (!this.state.ecUnauth) {
      payments = this.state.session.profile && this.state.session.profile.payment_profile.payment_methods &&
      this.state.session.profile.payment_profile.payment_methods.length > 0
        ? this.state.session.profile.payment_profile.payment_methods : null;
    } else {
      payments = this.state.driverInfoPaymentProfile;
    }
    this.setState({
      authorized: event.target.value
    });

    if (event.target.value === 'false') {
      if (this.state.containsCC) {
        CorporateActions.setPaymentID(payments[this.state.defaultProvided].payment_reference_id);
        CorporateActions.setPaymentType(payments[this.state.defaultProvided].payment_type);
      } else {
        CorporateActions.setPaymentID(null);
        CorporateActions.setPaymentType(null);
      }
    } else {
      if (this.state.containsBN && PaymentModelController.isBusinessAccount(payments[this.state.defaultProvided])) {
        CorporateActions.setPaymentID(payments[this.state.defaultProvided].payment_reference_id);
        CorporateActions.setPaymentType(payments[this.state.defaultProvided].payment_type);
      } else if (this.state.containsBN && !PaymentModelController.isBusinessAccount(payments[this.state.defaultProvided])) {
        for (let i = payments.length - 1; i >= 0; i--) {
          let first = true;
          if (PaymentModelController.isBusinessAccount(payments[i])) {
            if (first) {
              first = false;
              CorporateActions.setPaymentID(payments[i].payment_reference_id);
              CorporateActions.setPaymentType(payments[i].payment_reference_id);
            }
          }
        }
      } else {
        CorporateActions.setPaymentID(null);
        CorporateActions.setPaymentType(null);
      }
    }
    CorporateActions.setBillingAuthorization(event.target.value);
  },
  render () {
    let corporate = this.state.session.contract_details;
    let accountName = corporate ? corporate.contract_name : false;
    let billingAccount = corporate ? corporate.contract_billing_account : false;

    let payments = this.state.session.profile ? this.state.session.profile.payment_profile.payment_methods : [];

    return (
      <div className="section-content">
        <div className="option-block billing">
          <div className="view-header">{enterprise.i18nReservation.resflowcorporate_4031}</div>

          <fieldset>
            <legend ref="label" className="billing-label">{enterprise.i18nReservation.resflowcorporate_4027}
              <strong> {accountName}</strong> {enterprise.i18nReservation.resflowcorporate_4028}
            </legend>
            <div>
              <input type="radio"
                     name="authorization"
                     id="authorized"
                     checked={this.state.authorized === 'true'}
                     value="true"
                     onChange={this._onRadio}/>
              <label className="inline-label"
                     htmlFor="authorized">{enterprise.i18nReservation.resflowcorporate_0200}</label>
            </div>
            {this.state.authorized === 'true' && !billingAccount ?
              <BillingNumber
                defaultProvided={this.state.defaultProvided}
                  containsBN={this.state.containsBN}
                  ecunauth={this.state.ecUnauth}/>
              : false}
            <div>
              <input type="radio"
                     name="authorization"
                     id="notAuthorized"
                     checked={this.state.authorized === 'false'}
                     onChange={this._onRadio}
                     value="false"/>
              <label className="inline-label"
                     htmlFor="notAuthorized">{enterprise.i18nReservation.resflowcorporate_0201}</label>
            </div>
          </fieldset>
          {this.state.authorized === 'false' && this.state.containsCC ?
            <div className="credit-selection">
              <div>{enterprise.i18nReservation.review_payment_options_payment_subtitle}</div>
              <label>{enterprise.i18nReservation.resflowcorporate_4030}</label>
              <select className="styled"
                      onChange={this._onChangeCreditCard} ref="provided">
                {payments.map((payment, index)=> {
                  if (payment.payment_type === PRICING.CREDIT) {
                    return (<option key={index} value={payment.payment_reference_id}>
                      {payment.alias} ({PaymentModelController.maskCC(payment.last_four)})</option>);
                  }
                  return false;
                })}
              </select>
            </div>
            : false}
        </div>
      </div>
    );
  }
});


const BillingNumber = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin,
    Validator
  ],
  cursors: {
    session: ReservationCursors.reservationSession,
    account: ReservationCursors.account,
    driverBilling: ReservationCursors.driverInfoPaymentProfile
  },
  getInitialState () {
    return {
      filledField: null,
      method: 'provided',
      hasBillingNumber: false
    };
  },
  fieldMap () {
    return {
      value: {
        filledField: this.refs.filledField.getDOMNode().value
      },
      schema: {
        filledField: 'string'
      }
    };
  },
  componentWillMount () {
    this.determineDefaults();
  },
  determineDefaults () {
    let defaultRadio = 'filled';
    // ECR-14251
    let payments = this.state.session.profile ? this.state.session.profile.payment_profile.payment_methods : (this.props.ecunauth) ? this.state.driverBilling : [];

    if (payments.length > 0 && this.props.containsBN) {
      defaultRadio = 'provided';
    }

    CorporateActions.setPaymentType(defaultRadio === 'filled' ? 'CUSTOM' : 'EXISTING');

    this.setState({
      method: defaultRadio
    });
  },
  componentDidMount () {
    if (this.state.session.deeplink_billing_account) {
      this.validateAndSetInput('filledField', this.state.session.deeplink_billing_account);
    }
  },
  _onChange (field, event) {
    this.validateAndSetInput(field, event.target.value);
  },
  validateAndSetInput (field, value) {
    if (field === 'filledField') {
      this.setState({
        [field]: value
      });
      if (this.state.method === 'filled') {
        this.validate(field, value);
        CorporateActions.setPaymentID(value);
      }
    } else {
      if (this.state.method === 'provided') {
        CorporateActions.setPaymentID(value);
      }
    }
  },
  _onRadio (event) {
    this.setState({
      method: event.target.value
    });
    if (event.target.value === 'filled') {
      CorporateActions.setPaymentType('CUSTOM');
      CorporateActions.setPaymentID(this.refs.filledField.getDOMNode().value);
    } else {
      CorporateActions.setPaymentType('EXISTING');
      CorporateActions.setPaymentID(this.refs.provided.getDOMNode().value);
    }
  },
  getDefaultInput () {
    if (this.state.session.deeplink_billing_account) {
      return '••••' + this.state.session.deeplink_billing_account.substring(this.state.session.deeplink_billing_account.length - 4);
    } else {
      return '';
    }
  },
  render: function () {
    let payments = this.state.session.profile ? this.state.session.profile.payment_profile.payment_methods : (this.props.ecunauth) ? this.state.driverBilling : [];

    return (
      <div className="billing-fields">
        <div className="billing-number-label">{enterprise.i18nReservation.resflowcorporate_0046}</div>
        {payments.length > 0 && this.props.containsBN ?
          <div className="field-container">
            <input type="radio"
                   name="billingMethod"
                   id="provided"
                   checked={this.state.method === 'provided'}
                   value="provided"
                   onChange={this._onRadio}/>
            <label className="inline-label"
                   htmlFor="provided">{enterprise.i18nReservation.resflowcorporate_0047}</label>
            <select className="styled"
                    onChange={this._onChange.bind(this, 'dropDown')}
                    ref="provided"
                    defaultValue={payments[this.props.defaultProvided].payment_reference_id}>
              {payments.map((payment, index)=> {
                if (PaymentModelController.isBusinessAccount(payment)) {
                  return (<option key={index} value={payment.payment_reference_id}>
                    {payment.alias} ({payment.billing_number})</option>);
                }
                return false;
              })}
            </select>
          </div> : false}

        <div className="field-container">
          <input type="radio"
                 name="billingMethod"
                 id="selfFilled"
                 checked={this.state.method === 'filled'}
                 value="filled"
                 onChange={this._onRadio}/>
          <label className="inline-label"
                 htmlFor="selfFilled">{enterprise.i18nReservation.resflowcorporate_0208}</label>
          <input onChange={this._onChange.bind(this, 'filledField')} id="filledField"
                 ref="filledField"
                 type="text"
                 defaultValue={this.getDefaultInput()}
            />
        </div>
      </div>
    );
  }
});

module.exports = Billing;
