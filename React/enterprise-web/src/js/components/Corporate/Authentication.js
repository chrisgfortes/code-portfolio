import Validator from '../../utilities/util-validator';
import Error from '../Errors/Error';
import classNames from 'classnames';
import { PARTNERS } from '../../constants';
import CorporateController from '../../controllers/CorporateController';
import LoginController from '../../controllers/LoginController';
import ReservationFlowModelController from '../../controllers/ReservationFlowModelController';
import SessionController from '../../controllers/SessionController';

export default class Unauthenticated extends React.Component{
  constructor() {
    super();
    this.state = {
      epUsername: null,
      epPassword: null,
      ecUsername: null,
      ecPassword: null,
      epRememberMe: false,
      ecRememberMe: false,
      brand: PARTNERS.BRAND.ENTERPRISE_PLUS
    };
    this.validator = new Validator(this, this.fieldMap);
    this.fieldMap = this.fieldMap.bind(this);
    this._login = this._login.bind(this);
    this._onForgot = this._onForgot.bind(this);
    this._onRememberMe = this._onRememberMe.bind(this);
    this._handleInputChange = this._handleInputChange.bind(this);
    this.processLogin = this.processLogin.bind(this);
    this._toggleLogin = this._toggleLogin.bind(this);
  }
  componentWillUnmount () {
    ReservationFlowModelController.clearErrorsForComponent('loginWidget');
  }
  fieldMap () {
    return {
      refs: {
        epUsername: this.epUsername,
        epPassword: this.epPassword,
        ecUsername: this.ecUsername,
        ecPassword: this.ecPassword
      },
      value: {
        epUsername: this.epUsername.value,
        epPassword: this.epPassword.value,
        ecUsername: this.ecUsername.value,
        ecPassword: this.ecPassword.value
      },
      schema: {
        epUsername: 'string',
        epPassword: 'password',
        ecUsername: 'string',
        ecPassword: 'string'
      }
    };
  }
  _onRememberMe (brand, event) {
    this.setState({
      [brand]: event.target.checked
    });
  }
  _handleInputChange (attribute, event) {
    this.setState(
      {
        [attribute]: event.target.value
      }
    );
    this.validator.validate(attribute, event.target.value);
  }
  _login (brand) {
    let usernameKey;
    let passwordKey;
    let rememberMe;
    if (brand === PARTNERS.BRAND.ENTERPRISE_PLUS) {
      usernameKey = 'epUsername';
      passwordKey = 'epPassword';
      rememberMe = this.state.epRememberMe;
    } else if (brand === PARTNERS.BRAND.EMERALD_CLUB) {
      usernameKey = 'ecUsername';
      passwordKey = 'ecPassword';
      rememberMe = this.state.ecRememberMe;
    }
    const username = this.validator.validate(usernameKey, this[usernameKey].value);
    const password = this.validator.validate(passwordKey, this[passwordKey].value);

    if (password && username && !this.state.submitLoading) {
      let params = {
        username: this[usernameKey].value,
        password: this[passwordKey].value,
        rememberMe: rememberMe,
        brand: brand,
        getSession: false
      };

      this.processLogin(params);
    }
  }
  processLogin (params) {
    this.setState({submitLoading: true});

    LoginController.login(params).then((response) => {
      this.setState({submitLoading: false});
      if (response.data !== 'error') {
        CorporateController.setCorporateState('transition');

        // @todo - ECR-14682 :: Confirm if commenting this part of code, we are covering all scenarios
        // Try to recover the action (and all its parameters) that was attempted before the
        //   login action was triggered.
        // let callback = LoginController.getCallbackAfterAuthenticate();
        // if (callback && typeof callback === 'function') {
        //   callback();
        //   LoginController.setCallbackAfterAuthenticate(null);
        //   CorporateController.setModal(false);
        // } else {
        //   // The case above should catch all callback cases, but I'm leaving this here anyway to
        //   //   ensure retrocompatibility.
        //   this.props.modelController.submit(() => { });
        // }

        this.props.modelController.submit(() => {});
      } else if (response.data === 'error' && response.errors && response.errors.messages) {
        if (response.errors.messages.some((message) => message.code === 'CROS_LOGIN_WEAK_PASSWORD_ERROR')) {
          CorporateController.setModal(false);
        }
      }
      SessionController.callGetSession(true, false, true);
    })
    .catch((err) => {
      console.log('LoginController.login::catch', err);
    })
  }
  _toggleLogin (section) {
    ReservationFlowModelController.clearErrorsForComponent('loginWidget');
    this.setState({
      brand: section
    });
  }
  _onForgot () {
    CorporateController.setModal(false);
    window.open(this.props.supportLinks ? this.props.supportLinks.forgot_password_url : 'https://legacy.enterprise.com/car_rental/enterprisePlusForgotPassword.do');
  }
  render () {
    const loginButtonClasses = classNames({
      'btn': true,
      'disabled': this.state.submitLoading
    });
    const enterpriseAuthClasses = classNames({
      'enterprise-auth': true,
      'active': this.state.brand === PARTNERS.BRAND.ENTERPRISE_PLUS
    });
    const ECAuthClasses = classNames({
      'ec-auth': true,
      'active': this.state.brand === PARTNERS.BRAND.EMERALD_CLUB
    });
    const EPHeaderClasses = classNames({
      'auth-header': true,
      'active': this.state.brand === PARTNERS.BRAND.ENTERPRISE_PLUS
    });
    const ECHeaderClasses = classNames({
      'auth-header': true,
      'active': this.state.brand === PARTNERS.BRAND.EMERALD_CLUB
    });

    return (
      <div className="authentication corporate">
        <header>
          <h5>{i18n('resflowcorporate_0009')}</h5>
        </header>

        <h2 className={EPHeaderClasses}
            onClick={this._toggleLogin.bind(this, PARTNERS.BRAND.ENTERPRISE_PLUS)}>{i18n('loyaltysignin_0001')}</h2>

        <div className={enterpriseAuthClasses}>
          <Error errors={this.props.errors} type="GLOBAL"/>

          <label htmlFor="ep-email">{i18n('loyaltysignin_0003')}</label>
          <input onChange={this._handleInputChange.bind(this, 'epUsername')}
                 type="text"
                 name="ep-email"
                 ref={c => this.epUsername = c}
                 id="epEmail"/>

          <label htmlFor="ep-password">{i18n('loyaltysignin_0004')}</label>
          <input onChange={this._handleInputChange.bind(this, 'epPassword')}
                 type="password"
                 name="ep-password"
                 ref={c => this.epPassword = c}
                 id="epPassword"/>

          <label className="ep-remember"
                 htmlFor="ep-remember">
            <input onChange={this._onRememberMe.bind(this, 'epRememberMe')}
                   checked={this.state.epRememberMe} type="checkbox" id="ep-remember"
                   name="ep-remember"/>
            {i18n('resflowcorporate_4002')}
          </label>

          <div className={this.state.submitLoading ? 'loading' : false}/>
          {this.state.submitLoading ? false :
            <div onClick={this._login.bind(this, PARTNERS.BRAND.ENTERPRISE_PLUS)}
                 className={loginButtonClasses}>{i18n('resflowreview_0080')}</div>}
                    <span className="forgot"
                          onClick={this._onForgot}>{i18n('loyaltysignin_0019')}</span>
        </div>
        <div className="divider">
          <span className="strike-through"/>
          <i>{i18n('fedexcustompath_0002')}</i>
          <span className="strike-through"/>
        </div>
        <h2 className={ECHeaderClasses}
            onClick={this._toggleLogin.bind(this, PARTNERS.BRAND.EMERALD_CLUB)}>{i18n('resflowcorporate_4005')}</h2>

        <div className={ECAuthClasses}>
          <Error errors={this.props.errors} type="GLOBAL"/>

          <label htmlFor="ec-email">{i18n('resflowcorporate_4003')}</label>
          <input onChange={this._handleInputChange.bind(this, 'ecUsername')}
                 type="text"
                 name="ec-email"
                 ref={c => this.ecUsername = c}
                 id="ecEmail"/>

          <label htmlFor="ec-password">{i18n('loyaltysignin_0004')}</label>
          <input onChange={this._handleInputChange.bind(this, 'ecPassword')}
                 type="password"
                 name="ec-password"
                 ref={c => this.ecPassword = c}
                 id="ecPassword"/>

          <div className={this.state.submitLoading ? 'loading' : false}/>
          {this.state.submitLoading ? false :
            <div onClick={this._login.bind(this, PARTNERS.BRAND.EMERALD_CLUB)}
                 className={loginButtonClasses}>{i18n('resflowreview_0080')}</div>}
        </div>
        <div className="disclaimer">{i18n('resflowcorporate_0010')}</div>
      </div>
    );
  }
}

Unauthenticated.displayName = "Unauthenticated";
