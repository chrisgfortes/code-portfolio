import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';

import AdditionalInformation from './AdditionalInformation';
import Error from '../Errors/Error';

import { MessageLogger } from '../../controllers/MessageLogController';
import CorporateController from '../../controllers/CorporateController';
import ReservationCursors from '../../cursors/ReservationCursors';
import Validator from '../../mixins/Validator';
import classNames from 'classnames';

const PreRate = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin,
    Validator
  ],
  getInitialState () {
    return {
      model: null,
      validated: false,
      loading: false
    };
  },
  componentWillMount () {
    if (this.state.coupon) {
      CorporateController.getContractDetails(this.state.coupon)
                         .then((contractDetails)=> {
                           this.setState({ model: contractDetails.additional_information });
                         })
                         .catch((error) => {
                           // @Todo : Need to properly handle error on Front-end when we set up better error handling overall (by responding to global error event)
                           let errorLogger = new MessageLogger();
                           console.error(error);
                           if ( _.get(error, 'messages.length') > 0 ) {
                             errorLogger.addMessages(error.messages);
                             console.error('PreRate.js: Contract details call failed. Messages Returned: ', error.messages);
                           } else {
                             let logObject = enterprise.settings.frontEndMessageLogCodes.preRateContractDetailsFailed;
                             errorLogger.addMessage(logObject);
                             console.error('PreRate.js error:', logObject);
                           }
                           errorLogger.track();
                         });
    }
  },
  cursors: {
    coupon: ReservationCursors.coupon,
    session: ReservationCursors.reservationSession,
    errors: ['view', 'corporate', 'error']
  },
  fieldMap () {
    let fields = this.state.model;
    let map = {
      value: {},
      schema: {},
      refs: {}
    };

    if (fields) {
      for (let i = 0, len = fields.length; i < len; i++) {
        if (fields[i].required && fields[i].validate_additional_info) {
          let fieldId = fields[i].id;
          map.value[fieldId] = document.getElementById(fieldId).value;
          map.schema[fieldId] = 'string';
          map.refs[fieldId] = document.getElementById(fieldId);
        }
      }
    }

    return map;
  },
  compareSequence (a, b) {
    if (a.sequence < b.sequence) {
      return -1;
    }
    if (a.sequence > b.sequence) {
      return 1;
    }
    return 0;
  },
  _onConfirm () {
    if (this.state.validated && !this.state.loading) {
      this.setState({loading: true});
      this.props.modelController.submit(()=> { this.setState({loading: false}); });
    }
  },
  validateAllFields () {
    let valid = this.validateAll(null, true).valid;
    this.setState({validated: valid});
  },
  render () {
    let fields = null;
    let {AdditionalInformationFields} = AdditionalInformation;
    let confirmClasses = classNames({
      'btn': true,
      'disabled': !this.state.validated || this.state.loading
    });

    if (this.state.model) {
      fields = this.state.model.sort(this.compareSequence);
    }
    return (
      <div className="pre-rate corporate">
        {this.state.model ?
          <div>
            <header>
              <h2>{i18n('resflowcorporate_0077')}</h2>
            </header>
            <Error errors={this.state.errors} type="GLOBAL"/>
            <div className="disclaimer">{i18n('resflowreview_0004')}</div>
            <AdditionalInformationFields prerate disableHeader fields={fields} validate={this.validateAllFields}/>

            <div className="modal-action">
              <div className={this.state.loading ? 'loading' : false} />
              <button className={confirmClasses}
                      onClick={this._onConfirm}>{i18n('resflowcorporate_0078')}
              </button>
            </div>
          </div>
          :
          <div className="transition" />
        }
      </div>
    );
  }
});

module.exports = PreRate;
