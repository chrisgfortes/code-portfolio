const Warning = React.createClass({
  render: function () {
    return (
      <div className="not-authorized">
        <div className="icon icon-alert-caution"></div>
        <h2>{this.props.error ? this.props.error :
          <span className="transition"></span>}</h2>
      </div>
    );
  }
});

module.exports = Warning;
