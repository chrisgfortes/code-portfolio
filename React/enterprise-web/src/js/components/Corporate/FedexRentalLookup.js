const ReservationCursors = require('../../cursors/ReservationCursors');
import ReservationActions from '../../actions/ReservationActions';
const ErrorActions = require('../../actions/ErrorActions');
const Validator = require('../../mixins/Validator');
const BaobabReactMixinBranch = require('baobab-react/mixins').branch;
const classNames = require('classnames');
const CorporateActions = require('../../actions/CorporateActions');

const PreRate = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin,
    Validator
  ],
  cursors: {
    session: ReservationCursors.reservationSession
  },
  getInitialState () {
    return {
      validated: false,
    };
  },
  fieldMap () {
    return {
      value: {
        employeeId: this.refs.employeeId.getDOMNode().value,
        requisitionNumber: this.refs.requisitionNumber.getDOMNode().value
      },
      schema: {
        employeeId: 'string',
        requisitionNumber: 'string'
      }
    };
  },
  _onConfirm () {
    if (this.state.validated) {
      ReservationActions.setEmployeeNumber(this.refs.employeeId.getDOMNode().value);
      ReservationActions.setFedexRequisitionNumber(this.refs.requisitionNumber.getDOMNode().value);

      // Try to invoke again the request that was done before the modal was triggered.
      const callback = ReservationStateTree.select(ReservationCursors.callbackAfterAuthenticate).get();
      if (callback && typeof callback === 'function') {
        callback();
      }

      CorporateActions.setModal(false);
      ErrorActions.clearErrorsForComponent('existingReservations');
    }
  },
  validateAllFields () {
    const valid = this.validateAll(null, true).valid;
    this.setState({validated: valid});
  },
  render () {
    const confirmClasses = classNames({
      'btn': true,
      'disabled': !this.state.validated
    });

    return (
      <div className="pre-rate corporate">
        <div>
          <div className="top-disclaimer">
            {i18n('fedexcustompath_0020') || 'This reservation was booked using a FedEx account. Please provide the information below to view/modify this reservation'}
          </div>

          <div className="option-block">
            <fieldset>
                <label htmlFor="employeeId">{i18n('fedexcustompath_0016') || 'FedEx Employee ID'}</label>
                <div>
                  <input type="TEXT" id="employeeId" ref="employeeId" onChange={this.validateAllFields}/>
                </div>
            </fieldset>
          </div>

          <div className="option-block">
            <fieldset>
                <label htmlFor="requisitionNumber">{i18n('fedexcustompath_0017') || 'Requisition Number'}</label>
                <div>
                  <input type="TEXT" id="requisitionNumber" ref="requisitionNumber" onChange={this.validateAllFields}/>
                </div>
            </fieldset>
          </div>

          <div className="modal-action">
            <div className={this.state.loading ? 'loading' : false}></div>
            <button className={confirmClasses} onClick={this._onConfirm}>{i18n('resflowcorporate_0078')}
            </button>
          </div>
        </div>
      </div>
    );
  }
});

module.exports = PreRate;
