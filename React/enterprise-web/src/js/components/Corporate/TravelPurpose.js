const CorporateActions = require('../../actions/CorporateActions');
const classNames = require('classnames');
const BaobabReactMixinBranch = require('baobab-react/mixins').branch;

const TravelPurpose = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  getInitialState: function () {
    return {
      purpose: null
    };
  },
  _handleChange (event) {
    this.setState(
      {
        purpose: event.target.value
      }
    );
  },
  _confirmPurpose () {
    if (this.state.purpose) {
      CorporateActions.setCorporateState('transition');
      CorporateActions.setTravelPurpose(this.state.purpose);
      this.props.modelController.submit();
    }
  },
  render () {
    let confirmClasses = classNames({
      'btn': true,
      'disabled': !this.state.purpose
    });

    return (
      <div className="travel-purpose corporate">
        <header>
          <h2>{enterprise.i18nReservation.resflowcorporate_0014}</h2>
        </header>
        <fieldset>
          <div>
            <input type="radio"
                   name="purpose"
                   id="business"
                   value="business"
                   onChange={this._handleChange}/>
            <label htmlFor="business">{enterprise.i18nReservation.resflowcorporate_0015}</label>
          </div>
          <div>
            <input type="radio"
                   name="purpose"
                   id="leisure"
                   onChange={this._handleChange}
                   value="leisure"/>
            <label htmlFor="leisure">{enterprise.i18nReservation.resflowcorporate_0016}</label>
          </div>

          <div onClick={this._confirmPurpose}
               className={confirmClasses}>{enterprise.i18nReservation.resflowcorporate_0017}</div>
        </fieldset>
      </div>
    );
  }
});

module.exports = TravelPurpose;
