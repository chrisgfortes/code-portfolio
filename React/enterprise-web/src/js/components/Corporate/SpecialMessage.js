import AccordionText from '../Accordion/AccordionText.js';

export default function SpecialMessage ({name, message}) {
  const clipText = (message && message.length > 300);

  return (
    <div className={'corporate-special-message'}>
      <h2>{i18n('resflowcorporatespecialaccountmessage_0001', { AccountName: name}) || `A message from ${name}`}</h2>
      <AccordionText active={clipText} text={message} />
    </div>
  );
}

SpecialMessage.displayName = 'SpecialMessage';
