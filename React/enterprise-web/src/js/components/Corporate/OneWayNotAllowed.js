import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import ReservationCursors from '../../cursors/ReservationCursors';
import SessionService from '../../services/SessionService';
import RedirectActions from '../../actions/RedirectActions';
import CorporateActions from '../../actions/CorporateActions';

const OneWayNotAllowed = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    session: ReservationCursors.reservationSession,
    deepLinkReservation: ReservationCursors.deepLinkReservation,
  },
  _onContinue () {
    CorporateActions.setError(null);
    SessionService.clearSession()
                  .then(RedirectActions.goHome);
  },
  render: function () {
    const contractDetails = this.state.session.contract_details || _.get(this.state.deepLinkReservation, 'reservation.contract_details');
    let contentCopy = i18n('fedexcustompath_0029') || 'Unfortunately, when using code #{accountName} you must have the same pick-up and return location.';
    contentCopy = contentCopy.replace('#{accountName}', contractDetails.contract_name);

    return (
      <div>
        <p id="dlgdesc">
          {contentCopy}
        </p>

        <div className="btn-grp cf">
          <button onClick={this._onContinue} className="btn ok">
            {i18n('resflowreview_0161') || 'Change'}
          </button>
        </div>
      </div>
    );
  }
});

module.exports = OneWayNotAllowed;
