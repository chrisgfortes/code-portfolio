import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';

import ReservationCursors from '../../cursors/ReservationCursors';

import CorporateActions from '../../actions/CorporateActions';

import FedexController from '../../controllers/FedexController';
import BookingWidgetModelController from '../../controllers/BookingWidgetModelController';

import GlobalModal from '../Modal/GlobalModal';
import Authentication from './Authentication';
import TravelPurpose from './TravelPurpose';
import PreRate from './PreRate';
import Pin from './Pin';
import MultipleCID from './MultipleCID';
import RemoveCode from './RemoveCode';
import EnrollRemoveCode from './EnrollRemoveCode';
import ChangeTripPurpose from './ChangeTripPurpose';
import Warning from './Warning';
import FedexRentalLookup from './FedexRentalLookup';
import OneWayNotAllowed from './OneWayNotAllowed';

const ActionModals = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    corporate: ReservationCursors.corporate,
    coupon: ReservationCursors.coupon,
    supportLinks: ReservationCursors.supportLinks,
    errors: ReservationCursors.loginWidgetErrors
  },
  getDefaultProps () {
    return {
      modelController: BookingWidgetModelController
    };
  },
  render () {
    let modal = false;
    const {modelController} = this.props;
    const {corporate, coupon, supportLinks, errors} = this.state;

    switch (corporate.state) {
      case 'warning':
        modal = {
          content: <Warning error={corporate.error}/>,
          title: i18n('resflowcorporate_4033')
        };
        break;
      case 'authenticate':
        modal = {
          content: <Authentication modelController={modelController}
                                   supportLinks={supportLinks}
                                   errors={errors}
                                   />,
          title: i18n('resflowcorporate_0001')
        };
        break;
      case 'changePurpose':
        modal = {
          content: <ChangeTripPurpose />,
          title: i18n('resflowcorporate_0023')
        };
        break;
      case 'manageCID':
        modal = {
          content: <MultipleCID modelController={modelController} />,
          title: i18n('resflowcorporate_4034')
        };
        break;
      case 'purpose':
        modal = {
          content: <TravelPurpose modelController={modelController} />,
          title: i18n('resflowcorporate_0013'),
          close: function () {
            CorporateActions.setModal(false);
            CorporateActions.clearAdditionalInfo();
            CorporateActions.setPin(null);
            CorporateActions.setError(null);
          }
        };
        break;
      case 'preRate':
        modal = {
          content: <PreRate modelController={modelController} />,
          title: ' ',
          close: function () {
            CorporateActions.setModal(false);
            if (!FedexController.isFedexFlow(coupon)) CorporateActions.clearAdditionalInfo();
            CorporateActions.setPin(null);
            CorporateActions.setError(null);
          }
        };
        break;
      case 'pin':
        modal = {
          content: <Pin modelController={modelController} />,
          title: ' ',
          close: function () {
            CorporateActions.setModal(false);
            CorporateActions.setPin(null);
            CorporateActions.setError(null);
          }
        };
        break;
      case 'removeCode':
        modal = {
          content: <RemoveCode />,
          title: i18n('resflowcorporate_4009')
        };
        break;
      case 'enrollRemoveCode':
        modal = {
          content: <EnrollRemoveCode />,
          title: i18n('resflowcorporate_4036')
        };
        break;
      case 'fedexRentalLookup':
        modal = {
          content: <FedexRentalLookup />,
          title: (i18n('fedexcustompath_0019') || 'FEDEX RENTAL LOOKUP'),
          close: function () {
            CorporateActions.setModal(false);
            CorporateActions.setError(null);
          }
        };
        break;
      case 'oneWayNotAllowed':
        modal = {
          title: i18n('fedexcustompath_0028') || 'Pick-up & Return location must be the same',
          hideX: true,
          disableBlur: true,
          classLabel: 'oneWayNotAllowed',
          content: <OneWayNotAllowed />,
        };
        break;
      default :
        modal = {
          content: <div className="transition"></div>,
          title: i18n('resflowcorporate_4037')
        };
    }

    return (
      <div>
        {corporate.modal ?
          <GlobalModal
            active={corporate.modal}
            cursor={ReservationCursors.corporate.concat('modal')}
            close={modal.close}
            header={modal.title}
            hideX={modal.hideX}
            disableBlur={modal.disableBlur}
            classLabel={modal.classLabel}
            content={modal.content}/>
            :
            false
        }
      </div>
    );
  }
});

module.exports = ActionModals;
