
import EnterpriseServices from '../../services/EnterpriseServices';
import EnrollmentController from '../../controllers/EnrollmentController';
import Validator from '../../mixins/Validator';
import Modal from '../../components/Modal/GlobalModal';
import { SERVICE_ENDPOINTS } from '../../constants';

const Services = {
  submit (params, callback) {
    let data = {
      first_name: params.firstName,
      last_name: params.lastName,
      email: params.email,
      mobile: params.phone,
      email_source_code: enterprise.contest.emailSourceCode,
      competition: enterprise.contest.pvCode,
      country: params.country,
      dob: params.birthDate,
      opt_in: params.promotion
    };

    return EnterpriseServices.POST(SERVICE_ENDPOINTS.CONTEST_INIT,
      {
        data: JSON.stringify(data),
        callback: function (response) {
          callback(response);
        }
      });
  },
  getCountries (callback) {
    return EnterpriseServices.GET(SERVICE_ENDPOINTS.LOCATION_COUNTRIES,
      {
        callback: (response) => {
          if (response.error) {
            console.log(response.error);
          } else {
            if (callback) {
              callback(response.countries);
            }
          }
        }
      });
  }
};

const Contest = React.createClass({
  mixins: [
    Validator
  ],
  getInitialState () {
    return {
      //Model
      firstName: null,
      lastName: null,
      email: null,
      phone: null,
      birthDate: null,
      promotion: false,
      privacy: false,
      country: null,
      //View
      emailConfirm: null,
      month: null,
      day: null,
      year: null,
      modal: false,
      disclaimer: null,
      submit: false,
      loading: false,
      countries: null,
      errors: null
    };
  },
  componentWillMount () {

    this.setState({
      disclaimer: enterprise.contest.terms
    });

    this.hashRouter();

    EnrollmentController.getCountriesBasic()
                        .then((res)=> {
                          this.setState({
                            countries: res,
                            country: enterprise.countryCode
                          })
                        });
  },
  fieldMap () {
    return {
      value: {
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        email: this.state.email,
        emailConfirm: this.state.emailConfirm,
        phone: this.state.phone,
        year: this.state.year,
        month: this.state.month,
        day: this.state.day,
        privacy: this.state.privacy
      },
      schema: {
        firstName: 'string',
        lastName: 'string',
        email: 'email',
        emailConfirm: () => {
          let confirm = this.state.emailConfirm;
          let original = this.state.email;

          return (confirm && original) && (confirm.toUpperCase() === original.toUpperCase());
        },
        phone: 'phone',
        year: 'number',
        month: 'number',
        day: 'number',
        privacy: () => {
          return this.state.privacy;
        }
      }
    };
  },
  _onInputChange (value, event) {

    let def = event.target.type === 'checkbox' ? event.target.checked : event.target.value;

    this.setState({
      [value]: def
    }, ()=> {
      if (this.state.submit) {
        this.validate(value, def);
      }
    });
  },
  _submit (event) {
    event.preventDefault();
    let validate = this.validateAll();

    this.setState({
      submit: true
    }, ()=> {
      if (validate.valid) {
        $('html, body').animate({scrollTop: 0}, 'slow');
        this.setState({loading: true});
        Services.submit(this.state, (response)=> {
          this.setState({loading: false});
          if (response.veritasExecuted && response.veritasResponse.response === '0') {
            document.getElementById('form-wrapper').style.display = 'none';
            document.getElementById('thanks').style.display = 'block';
          } else {
            this.setState({errors: response.veritasResponse.message});
          }
        });
      }
    });
  },
  _setDate (attribute, value) {
    this.setState({
      [attribute]: value
    });

    let overall = [
      (attribute === 'year' ? value : this.state.year),
      (attribute === 'month' ? value : this.state.month),
      (attribute === 'day' ? value : this.state.day)
    ].join('-').replace(' ', '');

    this.setState({
      birthDate: overall
    }, ()=> {
      if (this.state.submit) {
        this.validate(attribute, value);
      }
    });
  },
  hashRouter () {
    let hash = location.hash.replace('#', '');

    if (hash === 'terms') {
      this.setState({
        render: hash
      });
    }
  },
  render () {

    if (this.state.render === 'terms') {
      return <div dangerouslySetInnerHTML={{__html: this.state.disclaimer}}/>;
    } else {
      if (this.state.loading) {
        return <div className="transition"></div>;
      } else {
        return (
          <section>
            <h1>{enterprise.contest.contestTitle}</h1>

            {this.state.errors ? <div className="error-container">{this.state.errors}</div> : false}
            <div className="contest-disclaimer retain-native"
                 dangerouslySetInnerHTML={{__html: enterprise.contest.contestDescription}}/>
            <form>
              <div className="field-container first-name">
                <label htmlFor="first-name">{enterprise.i18nReservation.reservationwidget_0021}</label>
                <input onChange={this._onInputChange.bind(this, 'firstName')}
                       id="first-name" type="text"
                       ref="firstName"
                       value={this.state.firstName}/>
              </div>
              <div className="field-container last-name">
                <label htmlFor="last-name">{enterprise.i18nReservation.reservationwidget_0022}</label>
                <input onChange={this._onInputChange.bind(this, 'lastName')}
                       id="last-name" type="text"
                       ref="lastName"
                       value={this.state.lastName}/>
              </div>

              <div className="field-container">
                <label htmlFor="email">{enterprise.i18nReservation.reservationwidget_0030}</label>
                <input onChange={this._onInputChange.bind(this, 'email')}
                       id="email" type="text"
                       ref="email"
                       placeholder={enterprise.i18nReservation.eplusenrollment_0010}
                       value={this.state.email}/>
              </div>

              <div className="field-container">
                <label htmlFor="emailConfirm">{enterprise.i18nReservation.eplusenrollment_0011}</label>
                <input onChange={this._onInputChange.bind(this, 'emailConfirm')}
                       id="emailConfirm" type="text"
                       ref="emailConfirm"
                       value={this.state.emailConfirm}/>
              </div>

              <div className="field-container country">
                <label htmlFor="country">{enterprise.i18nReservation.resflowreview_0062}</label>
                {this.state.countries && this.state.countries.length > 0 ?
                  <select onChange={this._onInputChange.bind(this, 'country')}
                          id="country"
                          className="styled"
                          defaultValue={enterprise.countryCode}>
                    {this.state.countries.map(function (country, index) {
                      return <option key={index}
                                     data-index={index}
                                     value={country.country_code}>{country.country_name}</option>
                    })}
                  </select>
                  :
                  <div className="loading">{enterprise.i18nReservation.resflowcorporate_4037}</div>
                }
              </div>

              <div className="field-container phone">
                <label htmlFor="phone">{enterprise.i18nReservation.resflowreview_0010}</label>
                <input onChange={this._onInputChange.bind(this, 'phone')}
                       id="phone" type="text"
                       ref="phone"
                       value={this.state.phone}/>
              </div>

              <div className="field-container date-selector">
                <label htmlFor="birth-date">{enterprise.i18nReservation.eplusenrollment_0031}</label>
                <input ref="day" className="day-selector"
                       onChange={(event)=>{this._setDate('day', event.target.value.substring(0, 2));}}
                       placeholder={enterprise.i18nReservation.eplusenrollment_0028}
                       value={this.state.day}
                       type="tel" name="day" disabled={this.props.disabled}/>
                <span className="separator">/</span>
                <input ref="month" className="month-selector"
                       id="birth-date"
                       onChange={(event)=>{this._setDate('month', event.target.value.substring(0, 2));}}
                       placeholder={enterprise.i18nReservation.eplusenrollment_0026}
                       value={this.state.month}
                       type="tel" name="month" disabled={this.props.disabled}/>
                <span className="separator">/</span>
                <input ref="year" className="year-selector"
                       onChange={(event)=>{this._setDate('year', event.target.value.substring(0, 4));}}
                       placeholder={enterprise.i18nReservation.eplusenrollment_0030}
                       value={this.state.year}
                       type="tel" name="year" disabled={this.props.disabled}/>
              </div>

              <div className="privacy-container" ref="privacy">
                <input id="privacy"
                       onChange={this._onInputChange.bind(this, 'privacy')}
                       name="privacy" type="checkbox"
                       checked={this.state.privacy}/>
                <label className="checkbox-label"
                       htmlFor="privacy">
                  {enterprise.i18nReservation.resflowreview_1007}</label>
                    <span className="accented modal-toggle"
                          onClick={()=>{this.setState({modal: true});}}> {enterprise.i18nReservation.uefacontest_0004}</span>
              </div>

              <div className="field-container">
                <input id="promotion"
                       ref="promotion"
                       onChange={this._onInputChange.bind(this, 'promotion')}
                       name="promotion" type="checkbox"
                       checked={this.state.checked}/>
                <label className="checkbox-label"
                       htmlFor="promotion">
                  {enterprise.i18nReservation.resflowreview_0005}
                </label>

                <p>
                  {enterprise.i18nReservation.resflowreview_0006}
                </p>
              </div>

              <div className="action-container">
                <button className="btn" onClick={this._submit}>{enterprise.i18nReservation.uefacontest_0007}</button>
              </div>
              {this.state.modal ?
                <Modal close={()=>{this.setState({modal: false});}}
                       header={enterprise.i18nReservation.uefacontest_0007}
                       content={
                           <div dangerouslySetInnerHTML={{__html: this.state.disclaimer}}/>
                           }/> : false}
            </form>
            <div className="footer-disclaimer retain-native">
              <small dangerouslySetInnerHTML={{__html: enterprise.contest.contestFooterText}}/>
            </div>

          </section>
        );
      }
    }
  }
});

module.exports = Contest;
