import ReactDOM from 'react-dom';
import GenericBaobabMixinWrapper from './GenericBaobabMixinWrapper';
import RedirectController from '../controllers/RedirectController';
import RedirectModal from './Modal/RedirectModal';
import HashModalIngest from '../modules/HashModalIngest';

const redirectDiv = document.createElement('div');

function init() {
  redirectDiv.id = 'ip-redirect';
  document.body.appendChild(redirectDiv);
  ReactDOM.render(
    <GenericBaobabMixinWrapper
      tree={ReservationStateTree}
      component={RedirectModal}/>,
    redirectDiv
  );

  //Determines whether to show IP Redirect Modal
  RedirectController.initiate();

  //BURNT TREE / ATESA / CITER ARRIVAL LOGIC
  if (!!location.hash) {
    HashModalIngest.ingest();
  }
}

module.exports = {init: init};
