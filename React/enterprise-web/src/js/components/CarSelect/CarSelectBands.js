import ReservationCursors from '../../cursors/ReservationCursors';
import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';

import CarSelectActions from '../../actions/CarSelectActions';
import RedirectActions from '../../actions/RedirectActions';
import { clearAllFilters,
         getPreFilteredSetting,
         getMetaFilteredSetting,
         getTransmissionSetting } from '../../controllers/CarSelectController'

import PreFilteredBand from './PreFilteredBand.js';
import TransmissionBand from './TransmissionBand.js';

import { exists } from '../../utilities/util-predicates';
import classNames from 'classnames';

function clearTransmissionFilters () {
  // need to clear any filter to show all vehicles
  clearAllFilters();
  CarSelectActions.setTransmissionBandState(false);
}

function clearFilters () {
  // need to clear any filter to show all vehicles
  clearAllFilters();
  CarSelectActions.setBandState('cleared');
}

function backToLocation () {
  RedirectActions.goToReservationStep('location');
}

function getFilteredSettings (carClass) {
  return {
    default: {
      description: i18n('vehiclesdlp_8001', {vehicle: carClass}),
      label: i18n('resflowcarselect_0005'),
      text: i18n('vehiclesdlp_8002'),
      action: clearFilters
    },
    none: {
      label: i18n('vehiclesdlp_0020', {vehicle: carClass}),
      action: backToLocation
    }
  }
}

const transmissionSettings = {
  empty: {
    description: i18n('resflowcarselect_0023', {transmission: enterprise.transmission}),
    label: i18n('resflowcarselect_0024'),
    action: backToLocation
  },
  automatic: {
    description: i18n('resflowcarselect_0018'),
    label: i18n('resflowcarselect_0019'),
    action: clearTransmissionFilters
  },
  manual: {
    description: i18n('resflowcarselect_0026'),
    label: i18n('resflowcarselect_0027'),
    action: clearTransmissionFilters
  }
}

const CarSelectBands = React.createClass({
  mixins: [BaobabReactMixinBranch, React.addons.PureRenderMixin],
  cursors: {
    selectedFilters: ReservationCursors.selectedCarFilters,
    filterBandState: ReservationCursors.filterBandState,
    preSelectedCarClass: ReservationCursors.preSelectedCarClass,
    preSelectedMeta: ReservationCursors.preSelectedMeta,
    transmissionBandState: ReservationCursors.transmissionBandState
  },
  render: function () {
    const {hasPreferredCars, contractName} = this.props;
    const {selectedFilters, filterBandState, preSelectedCarClass, preSelectedMeta, transmissionBandState} = this.state;
    
    const transmissionProps = getTransmissionSetting(transmissionBandState, transmissionSettings);
    let preFilteredProps;
    if (exists(preSelectedCarClass)) {
      const settings = getFilteredSettings(preSelectedCarClass);
      preFilteredProps = getPreFilteredSetting(settings, filterBandState, selectedFilters);
      
    }
    if (exists(preSelectedMeta)) {
      const settings = getFilteredSettings(preSelectedMeta);
      preFilteredProps = getMetaFilteredSetting(settings, filterBandState, selectedFilters);
    }

    return (
      <div className="cf">
        { !!preFilteredProps &&
          <PreFilteredBand {...preFilteredProps}/>
        }
        { !!transmissionProps &&
          <TransmissionBand {...transmissionProps}/>
        }
        { hasPreferredCars &&
          <div className={classNames({'generic-band': true, 'centralized': !transmissionProps})}>
            <div className="col">
              <i className="icon icon-addon-4-white"/>
            </div>
            <div className="col icon-label">
              = {
                  i18n('resflowcorporate_0087', { AccountName: contractName }) ||
                  `Vehicles preferred for ${contractName}`
                }.
            </div>
          </div>
        }
      </div>
    );
  }
});

module.exports = CarSelectBands;
