import Images from '../../utilities/util-images';
import VehiclePriceBox from './VehiclePriceBox';
import { selectVehicle,
         onSelectingInVehiclePrice,
         getChargeConfig } from '../../controllers/CarSelectController';
import {PRICING} from '../../constants';
import { intersperse } from '../../utilities/util-array';


function isOnPrepayModify (isModify, selectedChargeType) {
  return (isModify && selectedChargeType === PRICING.PREPAY);
}

function isPaylaterOnPrepayModify (chargeType, isModify, selectedChargeType) {
  return chargeType === PRICING.PAYLATER
      && isOnPrepayModify(isModify, selectedChargeType);
}

function getVehiclePriceLabels(car, isModify, selectedChargeType){
  return {
    [PRICING.PREPAY]: (car.charges[PRICING.PREPAY] ?
      { header: i18n('MVT_0001'), explanation: i18n('MVT_0002'), action: i18n('resflowcarselect_0006') } :
      { header: i18n('MVT_0006'), explanation: i18n('MVT_0007'), action: "" }
    ),
    [PRICING.PAYLATER]: (car.charges[PRICING.PAYLATER] && !isOnPrepayModify(isModify, selectedChargeType) ?
      { header: i18n('MVT_0009'), explanation: i18n('MVT_0010'), action: i18n('resflowcarselect_0600') } :
      { header: i18n('MVT_0004'), explanation: i18n('MVT_0003'), action: "" }
    )
  }
}

export default function VehiclePriceModal({car, isModify, selectedChargeType}) {
  const labels = getVehiclePriceLabels(car, isModify, selectedChargeType)
  return (
    <div className="modify-confirmation">
      <div className="vehicle-header-modal">
        <div className="pricing-modal">{car.name}</div>
        <div className="vehicle-model-text">
          {i18n('resflowcarselect_0008', { carName: car.models })}
        </div>
        <div className="car-image">
          {car.images.ThreeQuarter &&
            <img src={Images.getUrl(car.images.ThreeQuarter.path, 240, 'high')} alt=""/>
          }
        </div>
      </div>
      <div className="vehicle-prices-box">
        { intersperse([PRICING.PREPAY, PRICING.PAYLATER].map( chargeType => (
            <VehiclePriceBox key={chargeType} {...{
              chargeType,
              chargeConfig: getChargeConfig(car.charges[chargeType]),
              selectVehicle: selectVehicle.bind(null, car, chargeType, onSelectingInVehiclePrice),
              isPaylaterOnPrepayModify: isPaylaterOnPrepayModify(chargeType, isModify, selectedChargeType),
              isPrepay: chargeType === PRICING.PREPAY,
              labels: labels[chargeType]
            }}/>
          )), <div className="or-seperator">Or</div>)
        }
      </div>
    </div>
  );
}
VehiclePriceModal.displayName = 'VehiclePriceModal';