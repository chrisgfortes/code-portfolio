import ReservationCursors from '../../cursors/ReservationCursors';
import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import DomManager from '../../modules/outerDomManager';

import { whenVehicleIsSelected,
         vanModalControls,
         limitedVehicleModalControls,
         pointsToggle,
         redemptionModalControls,
         onConfirmingInNotAvailableModal } from '../../controllers/CarSelectController';

import GlobalModal from '../Modal/GlobalModal';
import LimitedVehicleModal from '../Modal/LimitedVehicleModal';
import ConfirmationModalContent from '../Modify/ConfirmationModalContent';
import TaxesAndFeesModalContent from './TaxesAndFeesModalContent';
import VanModalContent from './VanModalContent';
import CorporateModals from '../Corporate/ActionModals';
import RedemptionModal from './RedemptionModal';
import NotAvailableModal from './NotAvailableModal';
import VehiclePriceModal from './VehiclePriceModal.js';
import RateComparisonModalContent from './RateComparisonModalContent';

const CarModals = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    inflightModifyModal: ReservationCursors.inflightModifyModal,
    carSelect: ReservationCursors.viewCarSelect,
    preSelectedVehicle: ReservationCursors.preSelectedVehicle,
    selectedCar: ReservationCursors.selectedCar,
    basicProfileLoyalty: ReservationCursors.basicProfileLoyalty,
    redemption: ReservationCursors.redemption,
    isModify: ReservationCursors.modify,
    chargeType: ReservationCursors.chargeType,
    filteredCars: ReservationCursors.filteredVehicles,
    carRateCompare: ReservationCursors.carRateCompare
  },
  render () {
    const {carSelect, inflightModifyModal, preSelectedVehicle, selectedCar, basicProfileLoyalty, redemption, isModify, chargeType, filteredCars, carRateCompare} = this.state;
    const {hasNoVehiclesForCoupon} = this.props;
    const isRequestModalVehicle = _.get(carSelect, 'requestModal.modal') &&
                                  _.get(carSelect, 'requestModal.origin') === 'vehicle';

    DomManager.trigger('toggleInflightModal', inflightModifyModal);

    return (
      <div>
        {inflightModifyModal && (
          <GlobalModal
            header={i18n('resflowreview_0098')}
            contentHeader={i18n('resflowreview_0098')}
            contentText={i18n('resflowviewmodifycancel_2007')}
            cursor={ReservationCursors.inflightModifyModal}
            content={
              <ConfirmationModalContent 
                confirm={whenVehicleIsSelected}
              />
            }
          />
        )}
        
        {_.get(carSelect, 'taxesAndFees.modal') && (
          <GlobalModal
            header={i18n('reservationnav_0019')}
            cursor={ReservationCursors.taxesAndFeesModal}
            content={
              <TaxesAndFeesModalContent
                car={filteredCars.find(c => c.code === carSelect.detailsCar)}
              />
            }
          />
        )}

        {_.get(carSelect, 'vanModal.modal') && (
          <GlobalModal
            header={i18n('resflowcarselect_0098')}
            cursor={ReservationCursors.vanModalControl}
            content={
              <VanModalContent
                description={_.get(carSelect, 'vanModal.description')}
                controlsModal={vanModalControls}
              />
            }
          />
        )}

        {isRequestModalVehicle && (
          <GlobalModal
            header={i18n('resflowcarselect_0150') || 'Limited Vehicle Selected'}
            classLabel="Limited-Vehicle-Modal"
            cursor={ReservationCursors.requestModalModal}
            content={
              <LimitedVehicleModal controlsModal={limitedVehicleModalControls}/>
            }
          />
        )}

        {/*
          Take into account that this cursor is set by `CarSelectController.whenRedemptionVehicleIsSelected`
          and that this method is called either when clicking on a _Car's Select Button_ or
          when getting into #cars step (under special conditions).
          Check `routeRules.cars` on `reservation.js` for more details.
        */}
        {_.get(carSelect, 'redemption.modal') && (
          <GlobalModal
            hideX
            header={i18n('resflowcarselect_0007')}
            cursor={ReservationCursors.redemptionModal}
            classLabel="redemption-modal-container"
            content={
              <RedemptionModal {...{
                selectDaysControl: pointsToggle.createControls(),
                modalControls: redemptionModalControls,
                selectedCar,
                basicProfileLoyalty,
                redemption
              }}/>
            }
          />
        )}

        {_.get(carSelect, 'rateComparison.modal') && (
          <GlobalModal
            header={i18n('prepay_0015') || 'RATE INFORMATION'}
            classLabel="rate-comparison-modal"
            cursor={ReservationCursors.rateComparisonModal}
            content={
              <RateComparisonModalContent car={carRateCompare}/>
            }
          />
        )}

        {_.get(carSelect, 'vehiclePriceModal.modal') && (
          <GlobalModal
            header={i18n('MVT_0005')}
            cursor={ReservationCursors.vehiclePriceModalControls}
            classLabel="vehicle-price-modal" 
            content={
              <VehiclePriceModal {...{
                car: preSelectedVehicle,
                selectedChargeType: chargeType,
                isModify
              }}/>
            }
          />
        )}

        {hasNoVehiclesForCoupon && carSelect.notAvailable.modal && (
          <GlobalModal
            header="&nbsp;"
            cursor={ReservationCursors.notAvailableModal}
            content={
                <NotAvailableModal confirm={onConfirmingInNotAvailableModal}/>
            }
          />
        )}

        <CorporateModals />
      </div>
    );
  }
});

export default CarModals;