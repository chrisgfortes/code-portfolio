import FilterOption from './FilterOption.js';
import classNames from 'classnames';

function onClickStopPropagation (event) {
  event.stopPropagation();
  event.nativeEvent.stopImmediatePropagation();
}

export default class FilterDropdown extends React.Component {
  constructor(props) {
    super(props);
    this.filterId = _.uniqueId('filter-dropdown-');
    this.filterButtonId = _.uniqueId('filter-dropdown-button-');
  }
  componentDidMount () {
    document.addEventListener('click', this.props.hideFilterDropdowns);
  }
  componentWillUnmount () {
    document.removeEventListener('click', this.props.hideFilterDropdowns);
  }
  render() {
    const {items, name, label, selectedItems, isExpanded, toggleFilterDropdown} = this.props;
    const toggleFilter = toggleFilterDropdown.bind(null, name, isExpanded);
    if (items.length === 1) {
      return false;
    }

    return (
      <div className="selector" onClick={onClickStopPropagation}>
        <div id={this.filterButtonId} className="selector-name" tabIndex="0"
             role="button" aria-expanded={isExpanded} aria-controls={this.filterId}
             onKeyPress={a11yClick(toggleFilter)} onClick={toggleFilter}>
          {label} {selectedItems.length > 0 && `(${selectedItems.length})`}
          <i className="icon icon-nav-carrot-down"/>
        </div>
        <div id={this.filterId} className={classNames({'filters': true, 'is-collapsed': !isExpanded})}
             role="group" aria-labelledby={this.filterButtonId}>
          {items.map(item =>
            <FilterOption key={item} name={name} value={item} isChecked={selectedItems.includes(item)}/>
          )}
          <div className="clear-button" tabIndex="0"
               role="button" aria-expanded={isExpanded} aria-controls={this.filterId}
               onKeyPress={a11yClick(toggleFilter)} onClick={toggleFilter}>
            {i18n('resflowcarselect_0036')}
          </div>
        </div>
      </div>
    );
  }
}
FilterDropdown.displayName = 'FilterDropdown';
