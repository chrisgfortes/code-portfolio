import { isUnavailable,
         isAtContractRate,
         isAtPromotionalRate,
         isTruck } from '../../controllers/CarSelectController';
import PricingController from '../../controllers/PricingController';
import BasicPayDifference from './BasicPayDifference';
import {PRICING} from '../../constants';

const { isCorporate,
        isPromotion } = PricingController;

function availableAtContractRate (promotionSavings, contractType, isOnCarDetails) {
  if (contractType === 'CORPORATE') {
    // CUSTOM RATE
    // @todo - checkout if `isOnCarDetails` shouldn't be used everywhere
    return {content: i18n('resflowcarselect_8010'), isOnCarDetails: isOnCarDetails};
  } else if (promotionSavings) {
    // Promotion Applied (total savings)
    // @todo - check out if `promotionSavings` doesn't need `.replace('-', '')` like on `availableAtPromotionalRate()`
    return {content: `${i18n('resflowcarselect_0094')} (${promotionSavings} ${i18n('resflowcarselect_8007')})`};
  } else if (contractType === 'PROMOTION') {   // PROMOTIONAL RATE APPLIED
    return {content: i18n('promotionemail_0017')};
  } else {
    return {};
  }
}

function availableAtPromotionalRate (promotionSavings) {
  if (promotionSavings) {
    return {content: `${i18n('resflowcarselect_0094')} (${promotionSavings.replace('-', '')} ${i18n('resflowcarselect_8007')})`};
  } else {
    return {content: i18n('resflowcarselect_0094')};
  }
}

export default function PriceDifference ({car, payType, contractType, isOnCarDetails}) {
  if (payType === PRICING.REDEMPTION || isUnavailable(car)) {
    return <noscript/>; // @todo - return null/false when we update to React 0.15 (https://github.com/facebook/react/pull/5884)
  }
  const promotionSavings = _.get(car, 'priceDifferences.PROMOTION.difference_amount_view.format');
  if (isAtContractRate(car)) {
    return <BasicPayDifference {...availableAtContractRate(promotionSavings, contractType, isOnCarDetails)}/>;
  }
  if (isAtPromotionalRate(car)) {
    return <BasicPayDifference {...availableAtPromotionalRate(promotionSavings)}/>;
  }
  if (isPromotion(payType, contractType)) {
    // NOT ELIGIBLE FOR PROMOTION
    return <div className="car-savings saved not-bold"> {i18n('resflowcarselect_0117')}</div>;
  }
  if (isCorporate(payType, contractType) && !isTruck(car)) {
    // NOT ELIGIBLE FOR CONTRACT
    return <div className="car-savings saved"> {i18n('promotionemail_0020')} </div>;
  }
  return <noscript/>; // @todo - return null/false when we update to React 0.15 (https://github.com/facebook/react/pull/5884)
}
PriceDifference.displayName = 'PriceDifference';