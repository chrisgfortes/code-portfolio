export default function VehiclePriceBox ({chargeConfig, labels, chargeType,
                                          selectVehicle, isPrepay, isPaylaterOnPrepayModify}){
  return (
    <div className="vehicle-pay-box">
      {(chargeConfig.hasCharge && isPrepay) &&
        <div className="best-price">
          <span>{i18n('MVT_0008')}</span>
        </div>
      }
      <div className="vehicle-price-label">{labels.header}</div>
      {(chargeConfig.hasCharge && !isPaylaterOnPrepayModify) &&
        <div>
          <div className="total-price">{chargeConfig.totalPrice}</div>
          <div className="sub-price">{chargeConfig.unitPrice} {chargeConfig.rateType}</div>
          <button className={`select-button ${chargeType} select-button-type-price`}
                  onClick={selectVehicle}>
            {labels.action}
          </button>
        </div>
      }
      <div className="price-explanation-message">
        {labels.explanation}
      </div>
    </div>
  );
}
VehiclePriceBox.displayName = 'VehiclePriceBox';