import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import ReservationCursors from '../../cursors/ReservationCursors';

import { getDisplayingVehiclesAmount } from '../../controllers/CarSelectController';
import PricingController from'../../controllers/PricingController';

import PayTypeSelector from './PayTypeSelector.js';
import FilterSelector from './FilterSelector.js';
import Error from '../Errors/Error';

import classNames from 'classnames';

const { getCurrencySymbol,
        canChangePayType } = PricingController;

function getFilterBarClasses (payTypes) {
  const payTypeCount = Object.keys(payTypes)
                             .filter(item => payTypes[item])
                             .length;
  return classNames({
    'filter-bar cf': true,
    'with-pay-selector': payTypeCount > 1
  });
}

const CarFilter = React.createClass({
  mixins: [
    React.addons.PureRenderMixin,
    BaobabReactMixinBranch
  ],
  getDefaultProps () {
    return {
      payTypes : {}
    };
  },
  cursors: {
    carSelect: ReservationCursors.modelCarSelect,
    errors: ReservationCursors.carSelectErrors,
    existingReservationConfirmationNumber: ReservationCursors.existingReservationConfirmationNumber,
    defaultAmount: ReservationCursors.defaultAmount
  },
  render: function () {
    const {payTypes} = this.props;
    const {carSelect: {filters, selectedFilters, filteredVehicles, availableVehicles, payType},
           existingReservationConfirmationNumber, errors, defaultAmount} = this.state;
    const isModifying = !!existingReservationConfirmationNumber;
    const displayingVehiclesAmount = getDisplayingVehiclesAmount({filteredVehicles, availableVehicles});
    const currencySymbol = getCurrencySymbol(defaultAmount);

    return (
      <div>
        <div className={getFilterBarClasses(payTypes)} role="complementary">
          <h1 className="page-heading alpha">{i18n('resflowcarselect_0001')}</h1>

          <div className="filter-header cf">
            <div className="filter-header-options">
              {canChangePayType(isModifying, payTypes) &&
                <PayTypeSelector {...{selectedPayType: payType, currencySymbol}}/>
              }
              <div className="taxes-and-fees-tooltip">
                <span className="taxes-text">{i18n('resflowcarselect_0016_pre')} </span>
                <span className="green" tabIndex="0" aria-describedby="taxes-and-fees-tooltip-content">
                  {' ' + i18n('resflowcarselect_0016_cta')}
                  <span id="taxes-and-fees-tooltip-content" className="tooltip" role="tooltip">
                    {i18n('resflowcarselect_0017')}
                  </span>
                </span>
                {i18n('resflowcarselect_0016_post')}
              </div>
            </div>
          </div>

          <FilterSelector {...{filters, selectedFilters, displayingVehiclesAmount}}/>

        </div>

        <div className="car-select-errors">
          <Error errors={errors} type="GLOBAL" />
        </div>

      </div>
    );
  }
});

export default CarFilter;