import { getPayTypeForCar, getPaymentItemPriceLabel } from '../../controllers/CarSelectController';
import DetailLineItem from './DetailLineItem';

export default function TaxesAndFeesModalContent ({ car }) {
  // This is a short term solution (see ECR-13137). The Car Components like CarDetails, RateComparisonModal and
  // TaxesAndFeesModalContent should follow the same business rules to find out the pay type being used.
  // @todo - Refactor this!
  const payType = getPayTypeForCar(car);
  const taxesAndFees = _.get(car, `.details.vehicleRates.${payType}.price_summary.fee_line_items`) || [];

  return (
    <div>
      <ul className="cf taxes-and-fees">
        { taxesAndFees.map( (item, index) => (
          <DetailLineItem key={index} description={item.description}
                          rightSide={getPaymentItemPriceLabel(item.status, item.total_amount_view.format)} />
        ))}
      </ul>
      <div className="taxes-copy taxes-clear">
        {i18n('LAC_legal_taxesandfees_0001') || "Rates, taxes, and fees do not reflect rates, taxes and fees applicable to non-included coverages, extras added later or to coverages required if the customer fails to provide acceptable proof of current liability coverages."}
      </div>
    </div>
  );
}
TaxesAndFeesModalContent.displayName = 'TaxesAndFeesModalContent';