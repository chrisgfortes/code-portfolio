export default function VanModalContent({description, controlsModal}){
  return (
    <div className="modify-confirmation">
      <div dangerouslySetInnerHTML={{__html: description}}/>
      <div className="btn-grp cf">
        <button onClick={controlsModal.close} className="btn cancel">{i18n('eplusaccount_0108')}</button>
        <button onClick={controlsModal.confirm} className="btn ok">{i18n('resflowcarselect_8100')}</button>
      </div>
    </div>
  );
}
VanModalContent.displayName = 'VanModalContent';