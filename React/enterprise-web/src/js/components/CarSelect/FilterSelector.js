import CarSelectController from '../../controllers/CarSelectController';
import FilterDropdown from './FilterDropdown.js';
import { VEHICLES } from '../../constants';
import classNames from 'classnames';

const filterLabels = {
  [VEHICLES.FILTER.CATEGORY]     : i18n('resflowcarselect_0002'),
  [VEHICLES.FILTER.PASSENGERS]   : i18n('resflowcarselect_0004'),
  [VEHICLES.FILTER.TRANSMISSION] : i18n('resflowcarselect_0003')
};

const {hasAnyFilterSelected, clearAllFilters} = CarSelectController;

export default class FilterSelector extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      mobileShowFilter: false,
      expandedDropdown: null
    };
    this.hideFilterDropdowns     = this.hideFilterDropdowns.bind(this);
    this.toggleFilterDropdown    = this.toggleFilterDropdown.bind(this);
    this.filterToggleHandleClick = this.filterToggleHandleClick.bind(this);
  }
  hideFilterDropdowns () {
    this.setState({expandedDropdown: null});
  }
  toggleFilterDropdown (name, isExpanded) {
    this.setState({expandedDropdown: isExpanded ? null : name});
  }
  filterToggleHandleClick () {
    this.setState({mobileShowFilter: !this.state.mobileShowFilter});
  }
  render () {
    const {filters, selectedFilters, displayingVehiclesAmount} = this.props;
    const {mobileShowFilter, expandedDropdown} = this.state;

    return (
      <div className="filter-selectors cf">
        <div className="mobile-filter-header">
          {
            // TODO: replace hardcoded with translation
            `${displayingVehiclesAmount} Results `
          }
          <button className="text-btn" onClick={this.filterToggleHandleClick}>
            {i18n('resflowlocations_0053')}
          </button>
        </div>

        <div className={classNames({'filters-wrapper': true, 'hide-filters': !mobileShowFilter})}>
          {VEHICLES.FILTERS.map((filter) => (
            <FilterDropdown key={filter} name={filter} label={filterLabels[filter]}
                            items={Object.keys(filters[filter])} selectedItems={selectedFilters[filter]}
                            isExpanded={expandedDropdown === filter}
                            hideFilterDropdowns={this.hideFilterDropdowns}
                            toggleFilterDropdown={this.toggleFilterDropdown}/>
          ))}
          {hasAnyFilterSelected(selectedFilters) &&
            <div className="clear-vehicle-filters" role="button" tabIndex="0"
                 onClick={clearAllFilters} onKeyPress={a11yClick(clearAllFilters)}>
              X - {i18n('resflowcarselect_0005')}
            </div>
          }
        </div>
      </div>
    );
  }
}
FilterSelector.displayName = 'FilterSelector';