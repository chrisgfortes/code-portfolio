export default function PreFilteredBand({ description, label, text, action }){
  return (
    <div className="pre-filter-band">
      <div>
        {description + ' '}
        <button onClick={action}>{label}</button>
        {' ' + text}
      </div>
    </div>
  )
}
PreFilteredBand.displayName = 'PreFilteredBand';