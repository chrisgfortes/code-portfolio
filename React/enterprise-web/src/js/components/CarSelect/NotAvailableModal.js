export default function NotAvailableModal ({confirm}){
  return (
    <div className="not-available-modal">
      <div className="header">
        {i18n('promotionemail_0019')}
      </div>
      <p>
        {i18n('resflowcarselect_147')}
      </p>

      <div className="btn-grp cf">
        <button className="btn ok" onClick={confirm}>
          {i18n('resflowcarselect_0078')}
        </button>
      </div>
    </div>
  );
}
NotAvailableModal.displayName = 'NotAvailableModal';