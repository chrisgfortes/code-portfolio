export default function PointsToggle({car, redemption, selectDaysControl}) {
  const days = redemption.days;
  const max = car.redemptionDaysMax == days;
  const min = days == 1;

  return (
    <div className="points-toggle-section">
      <div className="header">
        {i18n('eplusaccount_0090')}
      </div>
      <div className="points-toggle">
        <button 
          onClick={min ? false : selectDaysControl.minus} 
          className={"points-button" + (min ? ' disabled' : '')}>
          <span>-</span>
        </button>

        <div className="point-amount">
          <span>{i18n('resflowcarselect_0068', {number: days})}</span>
        </div>

        <button 
          onClick={max ? false : selectDaysControl.plus} 
          className={"points-button" + (max ? ' disabled' : '')}>
          <span>+</span>
        </button>
      </div>
      <div
        className="subtext">{i18n('resflowcarselect_0058', {
          points: car.redemptionPointsUsed,
          number: car.redemptionDaysCount
        })}</div>
    </div>
  );
}

PointsToggle.displayName = 'PointsToggle';