import DetailLineItem from '../DetailLineItem';
import classNames from 'classnames';

function getClasses (configs) {
  return classNames({
    'pricing-details cf': true,
    'loading': !configs.hasDetails,
    'details-error': configs.hasDetails && !configs.hasSummary
  })
}

export default function WhatsIncluded ({configs, showTaxesAndFees}) {
  return (
    <div className={getClasses(configs)}>
      <div className="loading-error">
        { // @todo - Ugh...do we have a message key for that??
          'Pricing information currently unavailable'
        }
      </div>
      <ul className="pricing-list">
        {configs.vehicleItems.map( item => (
          <DetailLineItem key={item.code}
                          description={item.header}
                          rightSide={item.total} />
        ))}
        <DetailLineItem description={<button onClick={showTaxesAndFees}>{i18n('resflowcarselect_0076')}</button>}
                        rightSide={configs.feesTotal}/>
        {configs.extraItems.map( item => (
          <DetailLineItem key={item.code}
                          description={item.header}
                          rightSide={item.total} />
        ))}
        {configs.savingItems.map( item => (
          <DetailLineItem key={item.code}
                          description={item.header}
                          rightSide={item.total} />
        ))}
      </ul>
    </div>
  );
}
WhatsIncluded.displayName = 'WhatsIncluded';