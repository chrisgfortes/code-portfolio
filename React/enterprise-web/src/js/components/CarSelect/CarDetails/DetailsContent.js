import { getPassengersCapacity,
         getCarClass } from '../../../controllers/CarSelectController';

export default function DetailsContent ({car}) {
  const features = car.features;
  const carClass = getCarClass(car);
  const passengers = getPassengersCapacity(car);
  return (
    <div className="feature-details cf">
      <table>
        <tbody>
          <tr>
            <th>{!!carClass && i18n('resflowcarselect_0002')}</th>
            <th>{!!passengers && i18n('resflowcarselect_0004')}</th>
            <th>{i18n('resflowcarselect_0080')}</th>
          </tr>
          <tr>
            <td>{carClass}</td>
            <td>{passengers}</td>
            <td>{car.luggageCapacity}</td>
          </tr>
        </tbody>
      </table>
      <p>
        { features.filter((f) => f.value)
                  .map((f) => <span key={f.code} className="feature-list">{f.value}</span>)
        }
      </p>
    </div>
  );
}
DetailsContent.displayName = 'DetailsContent';