import {PRICING} from '../../../constants';

const titleLabels = {
  [PRICING.PREPAY]   : i18n('reservationnav_0008'),
  [PRICING.PAYLATER] : i18n('resflowreview_0079')
}

export default function Footer ({chargeType, totalAmount, payAtCounter, learnMore}) {
  return (
    <div className="bottom-area">
      <div className="total-pricing cf">
        <div className="total-pricing-title">
          {titleLabels[chargeType]}
        </div>
        <div className="price-total">
          <div className="rate-uppertext" />
          <div className="rate-normal">{totalAmount}</div>
          <div className="rate-uppertext">*</div>
        </div>
      </div>
      <div className="taxes-copy">
        {i18n('LAC_legal_taxesandfees_0001') || "*Rates, taxes, and fees do not reflect rates, taxes and fees applicable to non-included coverages, extras added later or to coverages required if the customer fails to provide acceptable proof of current liability coverages."}
      </div>
      {payAtCounter}
      {learnMore}
    </div>
  );
}
Footer.displayName = 'Footer';