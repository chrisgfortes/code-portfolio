import { getChargeSummaryConfigs,
         showTaxesAndFees } from '../../../controllers/CarSelectController';

import Header from './Header';
import WhatsIncluded from './WhatsIncluded';
import DetailsContent from './DetailsContent';
import Footer from './Footer';

import {PRICING} from '../../../constants';

export default class CarDetails extends React.Component {
  constructor (props) {
    super(props);
    this.getCloseButton = this.getCloseButton.bind(this);
  }
  getCloseButton () {
    return this.header.getCloseButton();
  }
  render () {
    const { car, chargeType, viewDefault, headerId, learnMore } = this.props;
    const configs = getChargeSummaryConfigs(car, chargeType);
    
    // @todo - Check if we really need to display `payAtCounter`
    let payAtCounter = null;
    // Show at counter price
    // @todo - change this to `chargeType === PRICING.PAYLATER` (this will require some CSS adjustments)
    if ((chargeType === 'later' && car.charges.PAYLATER) || (chargeType === 'currency' && !car.charges.PREPAY)) {
      if (_.get(car, 'charges.PAYLATER.total_price_payment.code') !== _.get(car, 'charges.PAYLATER.total_price_view.code')) {
        payAtCounter = (
          <div className="price-total">
            <div className="rate-uppertext">
              {i18n('resflowcarselect_0101', {price: car.charges.PAYLATER.total_price_payment.format})}
            </div>
          </div>);
      }
    }

    let totalAmount = _.get(car, `charges.${chargeType}.total_price_view.format`) || 0;
    if (car.details) {
      let vehicleRates;
      if (chargeType === PRICING.PREPAY && car.details.vehicleRates.PREPAY) {
        vehicleRates = car.details.vehicleRates.PREPAY;
      } else {
        vehicleRates = car.details.vehicleRates.PAYLATER;
      }
      if (vehicleRates) {
        let lineItems = vehicleRates.price_summary;

        totalAmount = lineItems.estimated_total_view.format;

        // Show at counter price
        // @todo - change this to `chargeType === PRICING.PAYLATER` (this will require some CSS adjustments)
        if (chargeType === 'later' &&
            _.get(lineItems, 'estimated_total_payment.code') !== _.get(lineItems, 'estimated_total_view.code')) {
          payAtCounter = (
            <div className="price-total">
              <div className="rate-uppertext">
                {i18n('resflowcarselect_0101', {price: lineItems.estimated_total_payment.format})}
              </div>
            </div>);
        }
      }
    }

    return (
      <div className="cf full-height">
        <Header ref={r => this.header = r} {...{headerId, car, viewDefault}}/>
        <WhatsIncluded {...{configs, showTaxesAndFees: showTaxesAndFees.bind(null, car)}}/>
        <DetailsContent {...{car}}/>
        <Footer {...{chargeType, totalAmount, payAtCounter, learnMore}}/>
      </div>
    );
  }
}
CarDetails.displayName = 'CarDetails';