import { getTransmission } from '../../../controllers/CarSelectController';

export default class Header extends React.Component {
  constructor (props) {
    super(props);
    this.getCloseButton = this.getCloseButton.bind(this);
  }
  getCloseButton() {
    return this.closeButton
  }
  render () {
    const {headerId, car, viewDefault} = this.props;
    return (
      <div className="car-details cf">
        <div className="cf">
          <div className="car-header">
            <h2 id={headerId} className="beta">{car.name}</h2>
            <span className="or-similar">
              {i18n('resflowcarselect_0008', {carName: car.models})}
            </span>
            <span className="transmission">{getTransmission(car)}</span>
          </div>
          <button className="state-link"
                  ref={r => this.closeButton = r}
                  onClick={viewDefault}>
            {i18n('resflowcarselect_0065')}
            <i className="icon icon-ENT-icon-close" />
          </button>
        </div>
        <hr className="car-details-divider" />
      </div>
    );
  }
}
Header.displayName = 'Header';