import PricingController from '../../controllers/PricingController';
import PayTypeToggle from './PayTypeToggle';

import {PRICING} from '../../constants';

const { isPayTypeSelected,
        selectPayType } = PricingController;

export default function PayTypeSelector ({selectedPayType, currencySymbol}) {
  const toggleLabels = {
    [PRICING.CURRENCY]   : i18n('MVT_0012', { currency: currencySymbol }),
    [PRICING.REDEMPTION] : i18n('eplusaccount_0039')
  };
  return (
    <ul>
      {[PRICING.CURRENCY, PRICING.REDEMPTION].map(type => (
        <PayTypeToggle key={type} name={toggleLabels[type]}
                       isPressed={isPayTypeSelected(type, selectedPayType)}
                       selectPayType={selectPayType.bind(null, type)} />
      ))}
    </ul>
  );
}
PayTypeSelector.displayName = 'PayTypeSelector';