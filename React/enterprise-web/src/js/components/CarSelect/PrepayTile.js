import ReservationCursors from '../../cursors/ReservationCursors';
import GlobalModal from '../Modal/GlobalModal';
import LearnPayNowModal from '../Modal/LearnPayNowModal';

export default function PrepayTile (props){
  const {learnPayNowModal, openPayNowModal} = props;
  
  return (
    <div className = "prepaytile-container">
      <div className = "prepay-content-container">
        <div className = "content">
          <h1 className="header-content">{enterprise.reservation.introducingPayNowTitle || 'Introducing “Pay Now” - a new way to save you money'}</h1>
          <p className="prepay-content">
            {enterprise.reservation.introducingPayNowDescription || 'By paying online, as opposed to when you pick up your vehicle, we give you a special discounted rate.'}  
          </p>
        </div>
        <div className="cf blank-space"/>
        <div className="prepay-learn-more select-button"
          role="button"
          tabIndex="0"
          aria-label={i18n('resflowcarselect_0077').replace('taxes and fees', i18n('resflowcarselect_0006'))} // @todo - This... seems... VERY wrong...
          onClick={openPayNowModal}
          onKeyPress={a11yClick(openPayNowModal)}>
          {i18n('prepay_0005') || 'Learn More About Pay Now'}
        </div>
      </div>
      {learnPayNowModal &&
        <GlobalModal
          header={' '}
          content={<LearnPayNowModal />}
          cursor={ReservationCursors.learnPayNowModal}
          classLabel="learn-paynow-modal" />
      }
    </div>
  );
}
PrepayTile.displayName = 'PrepayTile';