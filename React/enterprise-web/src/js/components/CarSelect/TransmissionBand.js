export default function TransmissionBand({description, label, action}){
  return (
    <div className="pre-filter-band">
      <div>
        {description}
        <button onClick={action}>{label}</button>
      </div>
    </div>
  );
}
TransmissionBand.displayName = 'TransmissionBand';