import { getPayTypeForCar,
         getChargeConfig } from '../../../controllers/CarSelectController';

import BasePayTypeView from './BasePayTypeView';
import NAPrepayView from './NAPrepayView';

import {PRICING} from '../../../constants';

const priceLabels = {
  [PRICING.PREPAY]   : i18n('MVT_0008'),
  [PRICING.PAYLATER] : i18n('resflowcarselect_0600')
};

export default function CurrencyView ({isNAPrepayEnabled, car, viewPrices,
                                       onSelectVehicle, getIncludedLabel, openPayNowModal}) {
  if (isNAPrepayEnabled) {
    return <NAPrepayView {...{car, onSelectVehicle, openPayNowModal}}/>;
  }
  const currencyPayType = getPayTypeForCar(car);
  return (
    <BasePayTypeView {...{car, viewPrices}}
                     chargeConfig={getChargeConfig(car.charges[currencyPayType])}
                     label={priceLabels[currencyPayType]}
                     includedText={getIncludedLabel(car)}
                     isTotalFirstOrder={enterprise.pricingOrderDisplayTotalFirst}/>
  );
}
CurrencyView.displayName = 'CurrencyView';