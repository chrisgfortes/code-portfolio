export default function HiddenCharges ({label, netRate}) {
  return (
    <div className="cf">
      <div className="rate-section">
        <span>{label}</span>
      </div>
      <div className="rates cf">
        <div className="total-rate rate-info no-pricing">
          <div className="rate-normal">{netRate}</div>
          <div className="rate-subtext">{i18n('reservationnav_0008')}</div>
        </div>
      </div>
    </div>
  );
}
HiddenCharges.displayName = 'HiddenCharges';