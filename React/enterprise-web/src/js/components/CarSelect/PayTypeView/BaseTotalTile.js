export default function BaseTotalTile ({ car, chargeConfig, viewPrices, includedText }) {
  return (
    <div className="total-rate rate-info">
      <div className="block-separator">
        <div className="rate-uppertext"/>
        <div className="rate-normal">{chargeConfig.totalPrice}</div>
        <div className="rate-uppertext"/>
      </div>
      <div className="rate-subtext">
        {i18n('reservationnav_0008') || 'Total'}
        <a className="included-text" aria-label={includedText}
           href="#" role="button" onClick={viewPrices}>
          {i18n('resflowcarselect_0015') || "What's Included"}
        </a>
      </div>
    </div>
  );
}
BaseTotalTile.displayName = 'BaseTotalTile';