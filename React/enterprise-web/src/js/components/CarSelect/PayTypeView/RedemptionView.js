export default function RedemptionView ({ chargeConfig, viewPrices, label, includedText }) {
  if (!chargeConfig.hasRates) {
    return <noscript/>; // @todo - return null/false when we update to React 0.15 (https://github.com/facebook/react/pull/5884)
  }
  return (
    <div className="cf">
      <div className="rate-section">
        <span>{label}</span>
      </div>
      <div className="rates cf">
        <div className="total-rate no-pricing rate-info no-border">
          <div className="block-separator">
            <div className="rate-uppertext"/>
            <div className="rate-normal">{chargeConfig.unitPrice}</div>
          </div>
          <div className="rate-subtext" >
            {chargeConfig.rateType}
            <a className="included-text" aria-label={includedText}
               href="#" role="button" onClick={viewPrices}>
              {i18n('resflowcarselect_0015') || "What's Included"}
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
RedemptionView.displayName = 'RedemptionView';