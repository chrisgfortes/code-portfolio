import { getCurrencyChargesConfig,
         getSavingsAmount } from '../../../controllers/CarSelectController';

import NAPrepayTile from './NAPrepayTile';

import {PRICING} from '../../../constants';

const chargeTypeSettings = {
  [PRICING.PREPAY]: {
    containerClass: 'rate-container right-rate-section',
    headerLabel: i18n('resflowcarselect_0006') || 'PAY NOW',
    hasInfoButton: true,
    notAvailableLabel: i18n('MVT_0007') || 'Pay Now is not available',
    selectLabel: i18n('resflowcarselect_0006') || 'PAY NOW'
  },
  [PRICING.PAYLATER]: {
    containerClass: 'rate-container left-rate-section',
    headerLabel: i18n('resflowcarselect_0600') || 'PAY LATER',
    hasInfoButton: false,
    notAvailableLabel: i18n('MVT_0004') || 'Pay Later is not available',
    selectLabel: i18n('resflowcarselect_0600') || 'PAY LATER'
  }
};


export default function NAPrepayView ({car, onSelectVehicle, openPayNowModal}) {
  const chargeConfigs = getCurrencyChargesConfig(car);
  return (
    <div className="cf">
      <div className="pay-now-container">
        { Object.keys(chargeConfigs).map( chargeType => (
          <NAPrepayTile key={chargeType}
                        openPayNowModal={openPayNowModal}
                        onSelectVehicle={onSelectVehicle.bind(null, chargeType)}
                        chargeTypeSetting={chargeTypeSettings[chargeType]}
                        chargeConfig={chargeConfigs[chargeType]}
                        savingsAmount={getSavingsAmount(car, chargeType)}/>
        ))}
      </div>
    </div>
  );
}
NAPrepayView.displayName = 'NAPrepayView';