import { toggleLearnPayNowModal } from '../../../actions/CarSelectActions';
import PricingController from '../../../controllers/PricingController';
import { hasAnyCurrencyCharge,
         isUnavailable,
         getChargeConfig } from '../../../controllers/CarSelectController';
import CurrencyView from './CurrencyView';
import RedemptionView from './RedemptionView';
import HiddenCharges from './HiddenCharges';

const { isCurrencyPayType,
        isRedemptionPayType } = PricingController;

function getIncludedLabel (car) {
  return i18n('resflowcarselect_0157', {carClass: car.models}) ||
         `What's included in the ${car.models}`;
}

function openPayNowModal (event) {
  event.preventDefault();
  toggleLearnPayNowModal(true);
}

export default function PayTypeView ({ car, viewPrices, onSelectVehicle, isNAPrepayEnabled, payType }) {
  if (!hasAnyCurrencyCharge(car)) {
    return <HiddenCharges label={i18n('resflowcarselect_1028')} netRate={PricingController.getNetRate(true)}/>;
  }
  if (isUnavailable(car)) {
    return <noscript/>; // @todo - return null/false when we update to React 0.15 (https://github.com/facebook/react/pull/5884)
  }
  if (isCurrencyPayType(payType)) {
    return (<CurrencyView {...{isNAPrepayEnabled, car, viewPrices,
                              onSelectVehicle, getIncludedLabel, openPayNowModal}}/>);
  }
  if (isRedemptionPayType(payType)) {
    return (<RedemptionView {...{ viewPrices}}
                            chargeConfig={getChargeConfig(car.charges[payType])}
                            includedText={getIncludedLabel(car)}
                            label={i18n('resflowcarselect_0007')}/>);
  }
}
PayTypeView.displayName = 'PayTypeView';