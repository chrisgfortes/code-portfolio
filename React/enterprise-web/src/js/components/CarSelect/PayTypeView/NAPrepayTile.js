import Amount from '../../Currency/Amount';

import classNames from 'classnames';

export default function NAPrepayTile ({ chargeTypeSetting, chargeConfig, savingsAmount,
                                        onSelectVehicle, openPayNowModal }) {
  return (
    <div className={chargeTypeSetting.containerClass}>
      {/* header */}
      <div className="header-border">
        <span className={classNames({'tab-copy': true, 'has-saved-copy': savingsAmount})}>
          {!!savingsAmount && (i18n('prepay_0001', {price: savingsAmount}) || `SAVE ${savingsAmount}`)}
        </span>
        <span className="header-copy">
          { chargeTypeSetting.headerLabel }
          { chargeTypeSetting.hasInfoButton && [
            ' ',
            <button key="paynow-info" className="paynow-info" onClick={openPayNowModal}
                    aria-label={i18n('prepay_0005') || 'Learn More About Pay Now'}>
              <i className="icon icon-icon-info-green"/>
            </button>
          ]}
        </span>
      </div>
      {/* body */}
      <div className="day-rate rate-info">
        <div className="block-separator">
          <div className="rate-uppertext"/>
          { chargeConfig.totalPriceParts && 
            <Amount className="rate-normal currency" amount={chargeConfig.totalPriceParts}/>
          }
          { chargeConfig.hasRates &&
            <div className="rate-uppertext rate-per-time">{chargeConfig.unitPrice} {chargeConfig.rateType}</div>
          }
        </div>
        { !chargeConfig.hasRates && (
          <div className="rate-subtext">
            {chargeTypeSetting.notAvailableLabel}
          </div>
        )}
      </div>
      { chargeConfig.hasRates && (
        <button className="select-button" onClick={onSelectVehicle}>
          {chargeTypeSetting.selectLabel}
        </button>
      )}
    </div>
  );
}
NAPrepayTile.displayName = 'NAPrepayTile';