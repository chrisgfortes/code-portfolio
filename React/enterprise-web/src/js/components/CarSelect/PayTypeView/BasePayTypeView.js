import BaseTotalTile from './BaseTotalTile';
import BaseUnitTile from './BaseUnitTile';

export default function BasePayTypeView ({ car, chargeConfig, viewPrices, label, includedText, isTotalFirstOrder }) {
  if (!chargeConfig.hasRates) {
    return <noscript/>; // @todo - return null/false when we update to React 0.15 (https://github.com/facebook/react/pull/5884)
  }
  const totalTile = <BaseTotalTile key="total" {...{ car, chargeConfig, viewPrices }}/>;
  const unitTile  = <BaseUnitTile  key="unit"  {...{ chargeConfig, includedText }}/>;

  return (
    <div className="cf">
      <div className="rate-section">
        <span>{label}</span>
      </div>
      <div className="rates cf">
        { isTotalFirstOrder ? [totalTile, unitTile]
                            : [unitTile, totalTile]
        }
      </div>
    </div>
  );
}
BasePayTypeView.displayName = 'BasePayTypeView';