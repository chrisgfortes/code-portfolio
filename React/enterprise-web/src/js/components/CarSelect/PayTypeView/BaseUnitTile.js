export default function BaseUnitTile ({chargeConfig}) {
  return (
    <div className="day-rate rate-info">
      <div className="block-separator">
        <div className="rate-uppertext"/>
        <div className="rate-normal">{chargeConfig.unitPrice}</div>
        <div className="rate-uppertext"/>
      </div>
      <div className="rate-subtext">{chargeConfig.rateType}</div>
    </div>
  );
}
BaseUnitTile.displayName = 'BaseUnitTile';