export default function EnoughPoints({redemption, points, redirect}){
  return (
    <div className="points-band cf">
      <div className="points-left-info cf">
        <div className="enterprise-plus">
          <div className="icon icon-eplus-logo"/>
          {i18n('resflowcarselect_0046')}</div>
      </div>
      <div className="points-right-info cf">
        {redemption ?
          <div onClick={redirect.bind(null, 'cars')} className="about-points">
            {i18n('resflowreview_0084')}
          </div>
          :
          <div className="about-points has-tip">
            {i18n('resflowcarselect_0049')}
              <span className="tooltip">
                  {i18n('resflowcarselect_0050')}
              </span>
          </div>
        }
        {redemption &&
        <div className="points-total"><span
          className="stacked-text">{i18n('resflowcarselect_0115')}</span><span
          className="points">{redemption.days * redemption.points}</span></div>
        }
        <div className="points-total"><span
          className="stacked-text">{i18n('resflowcarselect_0047')}</span><span
          className="points">{points}</span></div>
      </div>
    </div>
  );
}
EnoughPoints.displayName = 'EnoughPoints';