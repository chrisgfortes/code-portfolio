import Images from '../../utilities/util-images';
import PointsToggle from './PointsToggle';

/**
 * Check `CarModals` for more details about when this modal is shown up
 */
export default function RedemptionModal({redemption, selectedCar, basicProfileLoyalty, selectDaysControl, modalControls}) {
  const price = _.get(selectedCar, 'vehicleRates.PAYLATER.price_summary.estimated_total_view');

  return (
    <div className={'redemption-modal'}>
      {redemption.loading ?
        <div className="transition"/>
        :
        <div className="redemption-body">
          <div className="redemption-header cf">
            <div className="car-info">
              <h2>{selectedCar.name}</h2>

              <p>{i18n('resflowcarselect_0008', {carName: selectedCar.models})}</p>
            </div>
            <div className="points-info">
              <p>{i18n('resflowcarselect_0051', {points: selectedCar.redemptionPointsRate})}</p>
              { _.get(selectedCar, 'images.SideProfile') &&
                <img src={Images.getUrl(selectedCar.images.SideProfile.path, 240, 'high')}
                     alt={selectedCar.models} />
              }
            </div>
          </div>
          <div className="redemption-body cf">
            <h5>{i18n('resflowcarselect_0059', {number: basicProfileLoyalty.points_to_date})}</h5>
            <h5>{i18n('resflowcarselect_0061')}</h5>
          </div>
          <div className="redemption-toggle cf">
            <PointsToggle {...{
              car: selectedCar,
              redemption,
              selectDaysControl
            }} />

            <div className="equals"><span>=</span></div>
            <div className="balance-due-section">
              <div className="header">
                {i18n('resflowcarselect_0063')}
              </div>
              <span className="balance-due">{price.format}</span>

              <div className="subtext">{i18n('resflowcarselect_0064')}</div>
            </div>
          </div>
          <div className="btn-grp full-width cf">
            <button className="btn ok redemptionModal-btn-ok"
                    onClick={modalControls.confirm}>
              {i18n('resflowcarselect_0078')}
            </button>
            <button className="green-action-text redemptionModal-btn-cancel"
                    onClick={modalControls.cancel}>
              {i18n('resflowcarselect_0605')}
            </button>
          </div>
        </div>
      }
    </div>
  );
}
RedemptionModal.displayName = 'RedemptionModal';