import CarSelectController from '../../controllers/CarSelectController';

function selectFilter (name, value, event) {
  CarSelectController.toggleFilterOption(name, value, event.target.checked);
}

export default function FilterOption ({name, value, isChecked}) {
  return (
    <label className="filter-name">
      <input type="checkbox" name={name} value={value} checked={isChecked}
             onChange={selectFilter.bind(null, name, value)} />
      {value}
    </label>
  );
}

FilterOption.displayName= 'FilterOption';