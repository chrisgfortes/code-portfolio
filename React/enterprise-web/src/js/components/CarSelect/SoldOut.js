const soldOutConfigs = {
  unavailable: () => ({
    description: i18n('resflowcarselect_0089'),
    ctaLabel: i18n('resflowcarselect_0024'),
    ctaAttributes: { href: '#location' } // @todo - use RedirectActions
  }),
  truck: truckUrl => ({
    description: i18n('resflowcarselect_0092'),
    ctaLabel: i18n('resflowcarselect_0093'),
    ctaAttributes: { target: '_blank', href: truckUrl }
  }),
  checkAvailability: (phoneNumber) => ({
    description: i18n('resflowcarselect_0088'),
    ctaLabel: phoneNumber,
    ctaAttributes: { href: `tel:${phoneNumber.replace(/ /g, '')}` }
  })
};

function getSoldOutConfig (isSoldOut, truckUrl, phoneNumber) {
  if (isSoldOut) {
    return soldOutConfigs.unavailable();
  }
  if (truckUrl) {
    return soldOutConfigs.truck(truckUrl);
  }
  return soldOutConfigs.checkAvailability(phoneNumber);
}

export default function SoldOut ({truckUrl, isSoldOut, phoneNumber}) {
  const config = getSoldOutConfig(isSoldOut, truckUrl, phoneNumber);
  return (
    <div className="sold-out-container">
      {config.description}
      <a {...config.ctaAttributes} className="change-location-button">
        {config.ctaLabel}
      </a>
    </div>
  );
}
SoldOut.displayName = 'SoldOut';