import { isTruck,
         isSoldOut,
         isRestricted,
         isAvailableAtRetail,
         isPreferred,
         hasUncertainAvailability,
         hasAnyCurrencyCharge,
         getTransmission,
         getPhoneNumber,
         getPayTypeForCar,
         onSelectVehicle,
         whenCarHasDetails,
         showRateComparison } from '../../controllers/CarSelectController';

import PricingController from '../../controllers/PricingController';

import PayTypeView from './PayTypeView';
import CarDetails from './CarDetails';
import SoldOut from './SoldOut.js';
import PriceDifference from './PriceDifference';

import Images from '../../utilities/util-images';
import classNames from 'classnames';
import { PRICING } from '../../constants';

const { isCurrencyPayType,
        isRedemptionPayType } = PricingController;

function openLearnMoreModal (e) {
  e.preventDefault();
  enterprise.utilities.modal.open(i18n('resflowcarselect_0105'));
}
// @todo - maybe this could be turned into a component, or this condition could be moved to a controller method
function showLearnMore (car, payType, cardState) {
  if ((payType === PRICING.PAYLATER || (isCurrencyPayType(payType) && !car.charges.PREPAY)) &&
      car.charges.PAYLATER && cardState === 'prices' &&
      car.charges.PAYLATER.total_price_payment.code &&
      car.charges.PAYLATER.total_price_payment.code !== car.charges.PAYLATER.total_price_view.code) {
    return (
      <div className="sub-select-text">
        {`${i18n('reservationnav_0020')} `}
        <a href="#" onClick={openLearnMoreModal}>
          {i18n('resflowcarselect_0102')}
        </a>
      </div>
    );
  }
}

function carCssClassNames (cardState, car, payType) {
  // @todo - check out if `isRestricted` is really necessary here
  return classNames({
    'car'        : true,
    'sold-out'   : isSoldOut(car),
    'restricted' : hasUncertainAvailability(payType, car),
    [cardState]  : cardState && !isRestricted(car),
    'truck'      : isTruck(car)
  });
}

export default class Car extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      cardState: false,
      transitionActive: false,
      transformActive: false,
      detailsFlipped: false,
      rateCompare: false
    };
    this.detailsHeaderId = _.uniqueId('car-details-header-');
    
    this.flipCard = this.flipCard.bind(this);
    this.viewDetails = this.viewDetails.bind(this);
    this.viewPrices = this.viewPrices.bind(this);
    this.viewDefault = this.viewDefault.bind(this);
    this.startRateComparison = this.startRateComparison.bind(this);
  }
  flipCard (card) {
    const {cardState, transformActive, transitionActive} = this.state;
    // enable transition/transform programatically
    // iPhone 6 plus iOS 8 & 9 are crashing on render
    const flipToCard = cardState === card ? false : card;

    if (!transformActive) {
      this.setState({ transformActive: true });
      this.forceUpdate();
    }
    setTimeout(() => {
      if (!transitionActive) {
        this.setState({ transitionActive: true });
      }

      this.setState({ cardState: flipToCard });

      // We have to wait until the flip transition ends to focus. Best would be to use
      //   'transitionend', but it's not supported by IE9.
      const closeButton = flipToCard ? this.carDetails.getCloseButton() : this.detailsButton;
      setTimeout(() => closeButton.focus(), 600);
    }, 0);
  }
  viewDetails (e) {
    e.preventDefault();
    this.flipCard('details');
  }
  viewPrices (e) {
    e.preventDefault();
    const {car} = this.props;

    whenCarHasDetails(car);
    
    this.flipCard('prices');
  }
  viewDefault () {
    this.flipCard(false);
  }
  startRateComparison () {
    const {car} = this.props;
    this.setState({
      'rateCompare': true
    });
    whenCarHasDetails(car).then((carWithDetails)=> {
      this.setState({'rateCompare': false});
      showRateComparison(carWithDetails);
    }).catch( () => {
      this.setState({'rateCompare': false});
    });
  }
  render () {
    const {car, payType, contractType, isSelected, locationPhones, isFedexReservation, isNAPrepayEnabled} = this.props;
    const {cardState, rateCompare, transitionActive, transformActive} = this.state;
    
    const phoneNumber = getPhoneNumber(car, locationPhones, isFedexReservation);
    // This is a short term solution (see ECR-13137). The Car Components like CarDetails, RateComparisonModal and
    // TaxesAndFeesModalContent should follow the same business rules to find out the pay type being used.
    // @todo - Refactor this!
    const currencyChargeType = getPayTypeForCar(car);
    let selectButton;
    
    //show rate comparison modal link for either paytypes
    //but not for contracts / promotion / points in session
    const canShowRateComparison = isCurrencyPayType(payType) && isAvailableAtRetail(car);
    
    const hiddenCar = canShowRateComparison && !hasAnyCurrencyCharge(car);

    if (isRedemptionPayType(payType) && car.redemptionDaysMax === 0) {
      selectButton = (
        <div className="not-enough-points-block">
          {i18n('resflowcarselect_0114')}
        </div>
      );
    } else if (!isNAPrepayEnabled || isRedemptionPayType(payType)) {
      selectButton = (
        <button className="select-button"
                onClick={onSelectVehicle.bind(null, car, isNAPrepayEnabled, payType)}
                aria-label={i18n('resflowcarselect_0158', {carClass: car.models}) || `Select ${car.models}`}>
          {i18n('resflowlocations_0021')}
        </button>
      );
    }

    const previouslySelected = car.prepopped || isSelected;

    const priceDifference = true;

    const classes = classNames({
      'car-container animated': true,
      'has-promotion': (priceDifference || isRedemptionPayType(payType)), // review this test
      'previously-selected': previouslySelected,
      'hidden-car': hiddenCar,
      'preferred': isPreferred(car) && !previouslySelected && !isSoldOut(car)
    });
    return (
      <div ref="carContainer" className={classes}>
        <div className="car-cutoff">
          <div className={carCssClassNames(cardState, car, payType)}>
            <div className={classNames({
              'default-view': true,
              'transition-active': transitionActive,
              'transform-active': transformActive,
              'hiddenByFlip': cardState
            })}>
              <span className="preferredFlag">
                {i18n('eplusaccount_0010')} <i className="icon icon-addon-4-white"/>
              </span>
              <span className="previouslySelectedFlag">
                {i18n('resflowcorporate_0088') || 'Current Selection'} <i className="icon icon-forms-checkmark"/>
              </span>
              <div className="car-details cf">
                <div className="car-header">
                  <h2 className="beta">{car.name}</h2>
                  <span>{i18n('resflowcarselect_0008', { carName: car.models })}</span>
                  <span className="transmission">{getTransmission(car)}</span>
                  <img className="trucks-icon"
                       src="/content/dam/ecom/utilitarian/enterprise-truck-logo.png"
                       alt="" />
                </div>
                {!isRestricted(car) &&
                  <button className="state-link"
                          ref={r => this.detailsButton = r}
                          aria-label={i18n('resflowcarselect_0156', {carClass: car.models}) || `${car.models} Vehicle Details`}
                          onClick={this.viewDetails}>
                    {i18n('resflowcarselect_0010')}
                  </button>
                }
              </div>
              <div className="car-image">
                {car.images.ThreeQuarter &&
                  <img src={Images.getUrl(car.images.ThreeQuarter.path, 352, 'high')}
                       alt="" />
                }
              </div>
              <PayTypeView {...{car, isNAPrepayEnabled, payType}}
                           viewPrices={this.viewPrices}
                           onSelectVehicle={onSelectVehicle.bind(null, car, isNAPrepayEnabled)}/>
              {selectButton}
              { isNAPrepayEnabled && canShowRateComparison &&
                <div className = "cf rate-compare">
                  <button className={classNames({'rate-compare-link': true, 'loading': rateCompare})}
                          aria-label={`${i18n('prepay_0002') || 'Rate Comparison'} ${i18n('resflowcarselect_0010')}`}
                          onClick={this.startRateComparison}>
                    {i18n('prepay_0002') || 'Rate Comparison'}
                  </button>
                </div>
              }
              { isRedemptionPayType(payType) && car.redemptionDaysMax > 0 &&
                <div className="car-savings saved">
                  <i className="icon icon-icon-promo-applied"/>
                  {i18n('resflowcarselect_0052', { days: car.redemptionDaysMax })}
                </div>
              }
              <PriceDifference {...{car, payType, contractType}}/>
              {(isSoldOut(car) || hasUncertainAvailability(payType, car)) &&
                <SoldOut {...{phoneNumber, truckUrl: car.truckUrl, isSoldOut: isSoldOut(car)}}/>
              }
            </div>

            <div role="dialog" aria-labelledby={this.detailsHeaderId}
                 className={classNames({
                   'detailed-view': true,
                   'transition-active': transitionActive,
                   'transform-active': transformActive,
                   'hiddenByFlip': !cardState
                 })}>
              { !isRestricted(car) &&
                <CarDetails car={car}
                            viewDefault={this.viewDefault}
                            chargeType={currencyChargeType}
                            headerId={this.detailsHeaderId}
                            ref={r => this.carDetails = r}
                            learnMore={showLearnMore(car, payType, cardState)} />
              }
              {selectButton}
              <PriceDifference {...{car, payType, contractType}}
                               isOnCarDetails/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
Car.displayName = 'Car';