import ReservationCursors from '../../cursors/ReservationCursors';
import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import CarSelectActions from '../../actions/CarSelectActions';
import RedirectActions from '../../actions/RedirectActions';
import { getAvailablePaytypes,
         partitionVehiclesByAvailability,
         sortAvailableVehicles,
         sortUnavailableVehicles,
         hasPreferredVehicles,
         isAvailableAtPromotionOrContract } from '../../controllers/CarSelectController';
import PricingController from '../../controllers/PricingController';

import Car from './Car.js';
import UnavailableVehicles from './UnavailableVehicles.js';
import CarFilter from './CarFilter.js';
import CarSelectBands from './CarSelectBands.js';
import NotEnoughPoints from './NotEnoughPoints';
import EnoughPoints from './EnoughPoints';
import CarModals from './CarModals.js';
import PrepayTile from './PrepayTile';

const { isRedemptionPayType } = PricingController;

function openPayNowModal (e) {
  e.preventDefault();
  CarSelectActions.toggleLearnPayNowModal(true);
}

function goToReservationStep(step) {
  RedirectActions.goToReservationStep(step);
}

const CarSelect = React.createClass({
  mixins: [BaobabReactMixinBranch, React.addons.PureRenderMixin],
  cursors: {
    basicProfile: ReservationCursors.basicProfile,
    contractName: ReservationCursors.sessionContractName,
    contractType: ReservationCursors.sessionContractType,
    availablePayTypes: ReservationCursors.availablePayTypes,
    selectedCar: ReservationCursors.selectedCar,
    payType: ReservationCursors.carPayType,
    deepLinkReservation: ReservationCursors.deepLinkReservation,
    isNAPrepayEnabled: ReservationCursors.isNAPrepayEnabled,
    learnPayNowModal: ReservationCursors.learnPayNowModal,
    locationPhones: ReservationCursors.locationPhones,
    isFedexReservation: ReservationCursors.isFedexReservation
  },
  render () {
    const {cars, deepLinkErrors} = this.props;
    const {availablePayTypes, payType, contractName, contractType,
           locationPhones, isFedexReservation, isNAPrepayEnabled, basicProfile,
           learnPayNowModal, selectedCar, deepLinkReservation} = this.state;

    const selectedCarCode = _.get(selectedCar, 'code');

    const hasVehicles = cars.length > 0;

    let [availableVehicles, unavailableVehicles] = partitionVehiclesByAvailability(cars);

    availableVehicles = sortAvailableVehicles(availableVehicles, payType, deepLinkReservation, selectedCar);
    unavailableVehicles = sortUnavailableVehicles(unavailableVehicles);

    const hasPreferredCars = hasPreferredVehicles(availableVehicles);

    let lastPointsIndex = -1;
    if (availableVehicles) {
      lastPointsIndex = availableVehicles.findIndex(car => car.redemptionDaysMax === 0);
    }

    // @todo - use constant value for contract type
    const hasCouponCode = contractType === 'PROMOTION';
    const hasAvailableVehiclesWithPromotion = availableVehicles.some(isAvailableAtPromotionOrContract);
    const hasNoVehiclesForCoupon = hasCouponCode && !hasAvailableVehiclesWithPromotion;

    const vehicleTiles = availableVehicles.map((car, index) => (
      <Car key={`car-${index}`}
           {...{car, payType, contractType, locationPhones, isFedexReservation, isNAPrepayEnabled}}
           isSelected={selectedCarCode === car.code}/>
    ));

    if (isNAPrepayEnabled && enterprise.reservation.hideIntroducingPayNowTile !== 'true') {
      vehicleTiles.splice(0, 0, <PrepayTile key="prepay" {...{learnPayNowModal, openPayNowModal}}/>);
    }

    let enoughPoints = null;

    if (isRedemptionPayType(payType)) {
      const points = _.get(basicProfile, 'loyalty_data.points_to_date');

      enoughPoints = <EnoughPoints key="enough-points" points={points} redirect={goToReservationStep}/>;

      if (lastPointsIndex >= 0) {
        vehicleTiles.splice(lastPointsIndex, 0, <NotEnoughPoints key="not-enough-points"/>);
      } else {
        vehicleTiles.push(<NotEnoughPoints key="not-enough-points"/>);
      }
    }

    return (
      <div className="cf">
        <CarFilter payTypes={getAvailablePaytypes(availablePayTypes, hasVehicles)}/>
        {deepLinkErrors}
        {enoughPoints}

        <CarSelectBands {...{hasPreferredCars, contractName}}/>

        <div className="cars-wrapper cf" role="main">
          {vehicleTiles}
          <UnavailableVehicles cars={unavailableVehicles}/>
        </div>

        <CarModals {...{hasNoVehiclesForCoupon}}/>
      </div>
    );
  }
});

module.exports = CarSelect;
