export default function SelectCharge ({className, onClick, title, description}) {
  return (
    <div className={className}>
      <button className="select-button" onClick={onClick}>{title}</button>
      <div className="title">{title}</div>
      <div className="description">{description}</div>
    </div>
  );
}
SelectCharge.displayName = 'SelectCharge';