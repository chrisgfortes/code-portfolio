import { getCurrencyChargesSummaryConfigs,
         selectVehicle,
         onSelectingInRateComparison } from '../../../controllers/CarSelectController';
import { exists } from '../../../utilities/util-predicates';
import { PRICING } from '../../../constants'
import classNames from 'classnames';

import RateComparisonAmount from '../../Currency/RateComparisonAmount';
import PaymentLineRow from './PaymentLineRow';
import SelectCharge from './SelectCharge';


const labels = {
  [PRICING.PAYLATER]: {
    title: i18n('resflowcarselect_0600'),
    description: i18n('MVT_0010')
  },
  [PRICING.PREPAY]: {
    title: i18n('resflowcarselect_0006'),
    description: [
      i18n('prepay_0018') || 'Your credit card will be charged at the time of booking. ',
      i18n('prepay_0019') || 'Discounted rate, pay online. Cancellation will be charged a fee(Over 72 hours is $50, within 72 hours is $100).'
    ]
  }
};

const baseCssClasses = {
  [PRICING.PAYLATER]: {
    hideCharge  : 'hide-paylater',
    column      : 'paylater-col',
    totalAmount : 'paylater-col total-amount',
    select      : 'paylater-select',
    title       : 'paylater-col paylater-title',
    feesTotal   : 'paylater-col tax-total'
  },
  [PRICING.PREPAY]: {
    hideCharge  : 'hide-prepay',
    column      : 'prepay-col',
    totalAmount : 'prepay-col total-amount',
    select      : 'prepay-select',
    title       : 'prepay-col paynow-title',
    feesTotal   : 'prepay-col tax-total'
  }
};

function getCssClasses (charges, showTaxes) {
  let cssClasses = {};
  Object.entries(baseCssClasses)
        .forEach(([charge, classes]) => {
          const hasCharge = exists(charges[charge]);
          cssClasses[charge] = Object.assign({}, classes);
          cssClasses[charge].isHidden = classNames({[classes.hideCharge]: !hasCharge});
        });
  cssClasses.hiddenCharges = classNames(Object.values(cssClasses).map( classes => classes.isHidden));
  cssClasses.showTaxes = classNames({'active': showTaxes});
  
  return cssClasses;
}

export default class RateComparisonModalContent extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      showTaxes: false
    };
    this.showTaxes = this.showTaxes.bind(this);
  }
  showTaxes (e) {
    e.preventDefault();
    this.setState({'showTaxes': !this.state.showTaxes});
  }
  render () {
    const { car } = this.props;
    const transmission = _.get(car, 'filters.TRANSMISSION.description');
    const cssClasses = getCssClasses(car.charges, this.state.showTaxes);
    const chargeConfigs = getCurrencyChargesSummaryConfigs(car);

    return (
      <div className="cf">
        <div className="car-header">
          <h2 className="beta">{car.name}</h2>
          <span className="or-similar">
            {i18n('resflowcarselect_0008', {carName: car.models})}
          </span>
          <div className="transmission">{transmission}</div>
        </div>
        <table>
          <thead className="pay-titles">
            <tr>
              <td className={classNames(['car-taxes', cssClasses.hiddenCharges])}/>
              { PRICING.CURRENCY_TYPES.map( charge => (
                <td key={charge} className={classNames([cssClasses[charge].title, cssClasses[charge].isHidden])}>
                  {labels[charge].title}
                </td>
                
              ))}
            </tr>
          </thead>
          <tbody>
            {Object.entries(chargeConfigs.vehicleItems)
                   .map(([key, line]) => <PaymentLineRow {...{key, line, cssClasses}}/>)}
            {Object.entries(chargeConfigs.savingItems)
                   .map(([key, line]) => <PaymentLineRow {...{key, line, cssClasses}}/>)}
            { car.details && (
              <tr>
                <td className={classNames(['car-taxes', cssClasses.hiddenCharges])}>
                  <a href="#" className={cssClasses.showTaxes}
                     onClick={this.showTaxes} onKeyPress={a11yClick(this.showTaxes)}>
                    {i18n('prepay_0016') || 'TAX & FEES'}
                  </a>
                  <i className={classNames(['icon icon-nav-carrot-down', cssClasses.showTaxes])}/>
                </td>
                { Object.entries(chargeConfigs.feesTotal)
                        .map( ([charge, total]) => (
                  <td key={charge}
                      className={classNames([cssClasses[charge].feesTotal, cssClasses.showTaxes, cssClasses[charge].isHidden])}>
                    {total}*
                  </td>
                ))}
              </tr>
            )}
          </tbody>
          <tbody className={classNames(['taxes', cssClasses.showTaxes])}>
            {Object.entries(chargeConfigs.feeItems)
                   .map(([key, line]) => <PaymentLineRow isFeeRow {...{key, line, cssClasses}}/>)}
          </tbody>
          <tbody>
            {Object.entries(chargeConfigs.extraItems)
                   .map(([key, line]) => <PaymentLineRow {...{key, line, cssClasses}}/>)}
          </tbody>
          <tfoot className="total-amount">
            { car.details &&
              <tr>
                <td className="total">{i18n('prepay_0017') || 'ESTIMATED TOTAL'}</td>
                { Object.entries(chargeConfigs.totalPrice)
                        .map( ([charge, amount]) => (
                  <td key={charge} className={classNames([cssClasses[charge].totalAmount, cssClasses[charge].isHidden])}>
                    <RateComparisonAmount {...{amount}}/>
                  </td>
                ))}
              </tr>
            }
          </tfoot>
        </table>
        { PRICING.CURRENCY_TYPES.map( charge => (
          <SelectCharge key={charge} className={classNames([cssClasses[charge].select, cssClasses.hiddenCharges])}
                        title={labels[charge].title} description={labels[charge].description}
                        onClick={selectVehicle.bind(null, car, charge, onSelectingInRateComparison)} />
        ))}
        <div className="taxes-copy taxes-clear">{i18n('LAC_legal_taxesandfees_0001') || "Rates, taxes, and fees do not reflect rates, taxes and fees applicable to non-included coverages, extras added later or to coverages required if the customer fails to provide acceptable proof of current liability coverages."}</div>
      </div>
    );
  }
}
RateComparisonModalContent.displayName = 'RateComparisonModalContent';