import { PRICING } from '../../../constants'
import classNames from 'classnames';

export default function PaymentLineRow ({line, isFeeRow, cssClasses}) {
  return (
    <tr>
      <td className={isFeeRow ? 'tax-desc' : 'vehicle-rate'}>{line.header}</td>
      {PRICING.CURRENCY_TYPES.map( charge => (
        <td key={charge} className={classNames([cssClasses[charge].column, cssClasses[charge].isHidden])}>
          {line[charge] && line[charge]+'*'}
        </td>
      ))}
    </tr>
  );
}
PaymentLineRow.displayName = 'PaymentLineRow';