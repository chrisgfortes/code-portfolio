//import PropTypes from 'prop-types';
const {PropTypes} = React;

export default function BasicPayDifference ({content, isOnCarDetails}) {
  if (!content) {
    return <noscript/>; // @todo - return null/false when we update to React 0.15 (https://github.com/facebook/react/pull/5884)
  }
  const iconClass = isOnCarDetails ? 'icon-icon-promo-applied-white' : 'icon-icon-promo-applied';
  return (
    <div className="car-savings saved">
      <i className={`icon ${iconClass}`}/>
      {content}
    </div>
  );
}

BasicPayDifference.defaultProps = {
  isWhiteText: false
};

BasicPayDifference.propTypes = {
  content: PropTypes.string,
  isWhiteText: PropTypes.bool
};

BasicPayDifference.displayName = 'BasicPayDifference';