export default function NotEnoughPoints (){
  return (
    <div className="points-band cf">
      <div className="not-enough-points-left-info cf">
        <div className="not-enough-points">{i18n('resflowcarselect_0112')}</div>
        <p>{i18n('resflowcarselect_0113')}</p>
      </div>
    </div>
  );
}
NotEnoughPoints.displayName = 'NotEnoughPoints';