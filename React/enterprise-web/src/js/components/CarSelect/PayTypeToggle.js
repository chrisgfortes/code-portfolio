import classNames from 'classnames';

export default function PayTypeToggle ({name, selectPayType, isPressed}) {
  return (
    <li className={classNames({'selected': isPressed})}
        onClick={selectPayType} onKeyPress={a11yClick(selectPayType)}
        aria-pressed={isPressed} tabIndex="0" role="button">
      {name}
    </li>
  );
}
PayTypeToggle.displayName = 'PayTypeToggle';