import Car from './Car.js';

const unavailableLabels = {
  [false]: (cars) => i18n('resflowcarselect_141', { number: cars.length }),
  [true] : (cars) => i18n('resflowcarselect_142', { number: cars.length })
};

export default class UnavailableVehicles extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      showUnavailable: false
    };
    this.id = _.uniqueId('unavailable-cars-');
    this.toggleUnavailable = this.toggleUnavailable.bind(this);
  }
  toggleUnavailable () {
    this.setState({
      showUnavailable: !this.state.showUnavailable
    });
  }
  render () {
    const {cars} = this.props;
    const {showUnavailable} = this.state;

    return (
      <div className="cf">
        {cars.length > 0 &&
          <button className={'unavailable-toggle ' + (showUnavailable ? 'showing' : 'hiding')}
                  aria-controls={this.id} aria-expanded={showUnavailable} onClick={this.toggleUnavailable}>
            <i className={'icon ' + (showUnavailable ? 'icon-nav-carrot-up-green' : 'icon-nav-carrot-down')} />
            {unavailableLabels[showUnavailable](cars)}
          </button>
        }
        <div id={this.id} className={'unavailable-wrapper cf ' + (showUnavailable ? 'show' : 'hide')}>
          {cars.map((car) => <Car key={car.code} car={car}/>)}
        </div>
      </div>
    );
  }
}
UnavailableVehicles.displayName = 'UnavailableVehicles';