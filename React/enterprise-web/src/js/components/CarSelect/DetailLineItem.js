export default function DetailLineItem ({ description, rightSide}) {
  return (
    <li className="cf">
      <span className="left">{description}</span>
      <span className="right">{rightSide}*</span>
    </li>
  );
}
DetailLineItem.displayName = 'DetailLineItem';