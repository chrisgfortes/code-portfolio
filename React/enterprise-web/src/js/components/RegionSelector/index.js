import classnames from 'classnames';
import LocationController from '../../controllers/LocationController';
import { EXPEDITED, LOCALES } from '../../constants';

export default class RegionSelector extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      select: {
        prev: null,
        next: (props.value || '')
      }
    };

    this._onFocus = this._onFocus.bind(this);
    this._onBlur = this._onBlur.bind(this);
    this._onChange = this._onChange.bind(this);
  }

  componentWillReceiveProps(props){
    this.setState({
      select: {
        next: (props.value || '')
      }
    });
  }

  _getSubdivisionsList(){
    const subdivisions = LocationController.getSubdivisions();
    return subdivisions || [{}];
  }

  _chooseFirstOption(value){
    const fieldValue = enterprise.utilities.isMaskedField(value);
    return (fieldValue ? value : i18n('resflowreview_0152'));
  }

  _setState(prevValue, newValue) {
    this.setState({
      select: {
        prev: prevValue,
        next: newValue
      }
    });
  }

  _onFocus(event) {
    const fieldValue = event.target.value;
    this._setState(fieldValue, '');
  }

  _onBlur(event) {
    const fieldValue = event.target.value;
    const newValue = this.state.select.prev;
    this._setState(fieldValue, newValue);
  }

  _onChange(event){
    const fieldValue = event.target.value;
    this.props.onChange(event, _.get(this, 'props.name'))
    this._setState(fieldValue, fieldValue);
  }

  render() {
    const { required, hasError, onRef, dvla } = this.props;
    const { select } = this.state;

    const selectClass = classnames({
      'styled': true,
      'invalid': hasError
    });

    const DVLACountries = [LOCALES.GB];
    const defaultLabel = this._chooseFirstOption(select.next);
    const isCountryDVLA = DVLACountries.includes(dvla);

    return (
      <div>
        <select
          ref={onRef}
          className={selectClass}
          value={select.next}
          onFocus={this._onFocus}
          onBlur={this._onBlur}
          onChange={this._onChange}
          aria-required={!!required}
          aria-invalid={hasError}
        >

          {isCountryDVLA && (
            <option
              value={EXPEDITED.LICENSE.REGIONS.DVLA}
              key={EXPEDITED.LICENSE.REGIONS.DVLA}
            >{EXPEDITED.LICENSE.REGIONS.DVLA}
            </option>
          )}

          {!isCountryDVLA && [
            <option key="0" value={defaultLabel}>
              {defaultLabel}
            </option>,

            this._getSubdivisionsList()
              .map((region, index) => {
                return (
                  <option value={region.country_subdivision_code} key={index}>
                    {region.country_subdivision_name}
                  </option>
                )
              })
          ]}
        </select>
      </div>
    )
  }
}

RegionSelector.displayName = "RegionSelector";
