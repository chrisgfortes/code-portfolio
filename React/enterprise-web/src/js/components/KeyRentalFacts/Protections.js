'use strict';

var ReservationCursors = require('../../cursors/ReservationCursors');
var BaobabReactMixinBranch = require('baobab-react/mixins').branch;


var Protections = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  render: function () {
    return (
      <div className='protections'>
        <div className="key-rental-facts-summary-section_header">
          <h3>{enterprise.i18nReservation.keyfacts_0015}</h3>
        </div>
        {this.props.policies.included.length ?
          <div>
            <div className="key-rental-facts-summary-section_subheader">
              {enterprise.i18nReservation.keyfacts_0016}
            </div>
            {this.props.policies.included.map((protection) => {
              return <div key={protection.code} className="key-rental-facts-summary-section_item-wrapper">
                <div key={protection.code} onClick={this.props.onPolicyClick.bind(null, protection.code)}
                     role="button" tabIndex="0"
                     onKeyPress={a11yClick(this.props.onPolicyClick.bind(null, protection.code))}
                     className="key-rental-facts-summary-section_item">
                  <i className="icon icon-nav-carrot-green"></i>{protection.description}
                </div>
                {protection.policy_exclusions.length ?
                  <span className="key-rental-facts-summary-section_exclusion" href="#"
                        role="button" tabIndex="0"
                        onClick={this.props.onExclusionClick.bind(null, protection.code)}
                        onKeyPress={a11yClick(this.props.onExclusionClick.bind(null, protection.code))}>
                    {enterprise.i18nReservation.keyfacts_0046}
                  </span>
                  :
                  null
                }
              </div>
            })}
          </div>
          :
          <div className="key-rental-facts-summary-section_subheader">
            {enterprise.i18nReservation.keyfacts_0017}
          </div>
        }
        {this.props.policies.optional.length ?
          <div>
            <div className="key-rental-facts-summary-section_subheader">
              {enterprise.i18nReservation.keyfacts_0018}
            </div>
            {this.props.policies.optional.map((protection) => {
              return <div key={protection.code} className="key-rental-facts-summary-section_item-wrapper">
                <div role="button" tabIndex="0"
                     onClick={this.props.onPolicyClick.bind(null, protection.code)}
                     onKeyPress={a11yClick(this.props.onPolicyClick.bind(null, protection.code))}
                     className="key-rental-facts-summary-section_item">
                  <i className="icon icon-nav-carrot-green"></i>
                  {protection.description}
                  {protection.formattedRate && <span className="policy-rate">({protection.formattedRate})</span>}
                </div>
                {protection.policy_exclusions.length ?
                  <span className="key-rental-facts-summary-section_exclusion" href="#"
                        role="button" tabIndex="0"
                        onClick={this.props.onExclusionClick.bind(null, protection.code)}
                        onKeyPress={a11yClick(this.props.onExclusionClick.bind(null, protection.code))}>
                    {enterprise.i18nReservation.keyfacts_0046}
                  </span>
                  :
                  null
                }
              </div>
            })}
          </div>
          :
          <div className="key-rental-facts-summary-section_subheader">
            {enterprise.i18nReservation.keyfacts_0020}
          </div>
        }
      </div>
    );
  }
});

module.exports = Protections;
