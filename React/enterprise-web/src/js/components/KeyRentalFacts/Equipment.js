'use strict';

var ReservationCursors = require('../../cursors/ReservationCursors');
var BaobabReactMixinBranch = require('baobab-react/mixins').branch;


var Equipment = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  render: function () {
    return (
      <div className='equipment'>
        <div className="key-rental-facts-summary-section_header">
          <h3>{enterprise.i18nReservation.keyfacts_0021}</h3>
        </div>
        {this.props.policies.included.length ?
          <div>
            <div className="key-rental-facts-summary-section_subheader">
              {enterprise.i18nReservation.keyfacts_0022}
            </div>
            {this.props.policies.included.map((policy) => {
              return <div key={policy.code} onClick={this.props.onPolicyClick.bind(null, policy.code)}
                          role="button" tabIndex="0"
                          onKeyPress={a11yClick(this.props.onPolicyClick.bind(null, policy.code))}
                          className="key-rental-facts-summary-section_item">
                {policy.description}<i className="icon icon-nav-carrot-green"></i>
              </div>
            })}
          </div>
          :
          <div className="key-rental-facts-summary-section_subheader">
            {enterprise.i18nReservation.keyfacts_0023}
          </div>
        }
        {this.props.policies.optional.length ?
          <div>
            <div className="key-rental-facts-summary-section_subheader">
              {enterprise.i18nReservation.keyfacts_0024}
            </div>
            {this.props.policies.optional.map((policy) => {
              return <div key={policy.code} onClick={this.props.onPolicyClick.bind(null, policy.code)}
                          role="button" tabIndex="0"
                          onKeyPress={a11yClick(this.props.onPolicyClick.bind(null, policy.code))}
                          className="key-rental-facts-summary-section_item">
                {policy.description}
                {policy.formattedRate && <span className="policy-rate">({policy.formattedRate})</span>}
                <i className="icon icon-nav-carrot-green"></i>
              </div>
            })}
          </div>
          :
          <div className="key-rental-facts-summary-section_subheader">
            {enterprise.i18nReservation.keyfacts_0025}
          </div>
        }
      </div>
    );
  }
});

module.exports = Equipment;
