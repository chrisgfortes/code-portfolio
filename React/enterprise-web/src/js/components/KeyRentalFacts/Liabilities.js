'use strict';

var ReservationCursors = require('../../cursors/ReservationCursors');
var BaobabReactMixinBranch = require('baobab-react/mixins').branch;


var Liabilities = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  getInitialState() {
    return {
      showLiabilities: false
    }
  },
  _showLiabilities() {
    this.setState({
      showLiabilities: !this.state.showLiabilities
    });
  },
  render: function () {
    return (
      <div className='liabilities'>
        <div className="key-rental-facts-summary-section_header">
          <h3>{enterprise.i18nReservation.keyfacts_0032}</h3>
        </div>
        <div onClick={this._showLiabilities} className="key-rental-facts-summary-section_item"
             role="button" tabIndex="0" onKeyPress={a11yClick(this._showLiabilities)}>
          {this.state.showLiabilities ?
            enterprise.i18nReservation.keyfacts_0048
            :
            enterprise.i18nReservation.keyfacts_0047}
          <i className="icon icon-nav-carrot-green"></i>
        </div>
        {this.state.showLiabilities &&
        <div>
          <div className="key-rental-facts-summary-section_subheader">
            {enterprise.i18nReservation.keyfacts_0033}
          </div>
          <ul>
            <li>{enterprise.i18nReservation.keyfacts_0034}</li>
            <li>{enterprise.i18nReservation.keyfacts_0035}</li>
            <li>{enterprise.i18nReservation.keyfacts_0036}</li>
            <li>{enterprise.i18nReservation.keyfacts_0037}</li>
            <li>{enterprise.i18nReservation.keyfacts_0038}</li>
            <li>{enterprise.i18nReservation.keyfacts_0039}</li>
            <li>{enterprise.i18nReservation.keyfacts_0040}</li>
          </ul>
          <p>{enterprise.i18nReservation.keyfacts_0041}</p>
        </div>
        }
      </div>
    );
  }
});

module.exports = Liabilities;
