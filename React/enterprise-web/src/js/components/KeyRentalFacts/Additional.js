'use strict';

var ReservationCursors = require('../../cursors/ReservationCursors');
var BaobabReactMixinBranch = require('baobab-react/mixins').branch;


var Additional = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  render: function () {
    return (
      <div className='additional'>
        <div className="key-rental-facts-summary-section_header">
          <h3>{enterprise.i18nReservation.keyfacts_0029}</h3>
        </div>
        {this.props.policies.length ?
          <div>
            <div className="key-rental-facts-summary-section_subheader">
              {enterprise.i18nReservation.keyfacts_0030}
            </div>
            {this.props.policies.map((protection) => {
              return <div onClick={this.props.onPolicyClick.bind(null, protection.code)}
                          role="button" tabIndex="0"
                          onKeyPress={a11yClick(this.props.onPolicyClick.bind(null, protection.code))}
                          className="key-rental-facts-summary-section_item">
                {protection.description}<i className="icon icon-nav-carrot-green"></i>
              </div>
            })}
          </div>
          :
          <div className="key-rental-facts-summary-section_subheader">
            {enterprise.i18nReservation.keyfacts_0031}
          </div>
        }
      </div>
    );
  }
});

module.exports = Additional;
