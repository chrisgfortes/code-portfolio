import KeyRentalFactsBlock from './KeyRentalFactsBlock';
import Policies from '../Verification/Policies';

const KeyFactsAndPolicies = React.createClass({
  mixins: [
    React.addons.PureRenderMixin
  ],
  onCarrotToggle: function () {
    //@TODO this can be done with a css class update
    let $control = $('.key-rental-facts-block_header');
    let $content = $('#rentalFacts');
    $content.slideToggle(200, () => {
      $control.attr('aria-expanded', !$content.is(':hidden'));
    });
  },
  render: function () {
    const { showKeyFacts } = this.props;
    return (
      <div className="key-rental-facts-and-policies">
        <div className="key-rental-facts-block_header"
             role="button" tabIndex="0" aria-labelledby="keyFactsHeadingAria"
             aria-expanded="true" aria-expanded="true" aria-controls="rentalFacts"
             onClick={this.onCarrotToggle} onKeyPress={a11yClick(this.onCarrotToggle)}>
          <h2 id="keyFactsHeadingAria">
            {i18n('keyfacts_0049')}
            <span className="icon icon-nav-carrot-down"></span>
          </h2>
        </div>
        <div id="rentalFacts" className="key-rental-facts-and-policies_content">
          {showKeyFacts ?
            <KeyRentalFactsBlock />
            :
            null
          }
          <Policies />
        </div>
      </div>
    );
  }
});

module.exports = KeyFactsAndPolicies;
