'use strict';

const ReservationCursors = require('../../cursors/ReservationCursors');
const GlobalModal = require('../Modal/GlobalModal');

const BaobabReactMixinBranch = require('baobab-react/mixins').branch;

const KeyRentalFactsActions = require('../../actions/KeyRentalFactsActions');

const ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;

const Protections = require('./Protections');
const Equipment = require('./Equipment');
const MinimumRequirements = require('./MinimumRequirements');
const Additional = require('./Additional');
const Liabilities = require('./Liabilities');
const Disputes = require('./Disputes');

let KeyRentalFactsModal = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  componentDidUpdate: function () {
    // The DialogFocusTrap mixin on GlobalModal wont catch the update on this child, so we have
    //   to manually call to update focus targets.
    this.props.updateFocusableLoop(true);
  },
  cursors: {
    detailView: ReservationCursors.keyFactsDetail,
    brand: ReservationCursors.keyFactsBrand,
    policies: ReservationCursors.keyFactsPolicies,
    disputes: ReservationCursors.disputes
  },
  onPolicyOrExclusionClick: function () {
    // ...
  },
  render: function () {
    return (
      <div className="key-rental-facts-modal">
        {!this.state.detailView.show ?
          <KeyRentalFactsSummaryView key="summary" disputes={this.state.disputes} {...this.state.policies} />
          :
          <KeyRentalFactsDetailView key="details" data={this.state.detailView.data}/>
        }
      </div>
    );
  }
});

let KeyRentalFactsSummaryView = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    brand: ReservationCursors.keyFactsBrand
  },
  _onPolicyClick: function (value, event) {
    event.preventDefault();
    KeyRentalFactsActions.toggleDetailView(true, value);
  },
  _onExclusionClick: function (value, event) {
    event.preventDefault();
    KeyRentalFactsActions.toggleDetailViewForExclusion(true, value);
  },
  render: function () {
    return (
      <div>
        <div className="key-rental-facts-summary">
          <h2>{i18n('keyfacts_0010')}</h2>

          <p>{i18n('keyfacts_0011')}</p>

          <p>{i18n('keyfacts_0012')}</p>

          <p>{i18n('keyfacts_0055', {licenseeName: this.state.brand})} {i18n('keyfacts_0056')}</p>

          <p>{i18n('keyfacts_0013')}</p>

          <p>{i18n('keyfacts_0014')}</p>

          <div className="key-rental-facts-summary-section">
            <Protections onExclusionClick={this._onExclusionClick} onPolicyClick={this._onPolicyClick}
                         policies={this.props.protections}/>
            <Equipment onExclusionClick={this._onExclusionClick} onPolicyClick={this._onPolicyClick}
                       policies={this.props.equipment}/>
            <MinimumRequirements onPolicyClick={this._onPolicyClick} policies={this.props.minimumRequirements}/>
            <Additional onPolicyClick={this._onPolicyClick} policies={this.props.additional}/>
            <Liabilities />
            <Disputes contactInfo={this.props.disputes} onPolicyClick={this._onPolicyClick}/>
          </div>

        </div>
        <div className="key-rental-facts-rental-footer">
          <div className="key-rental-facts-rental-footer_subheader">
            {i18n('keyfacts_0044')}
          </div>
          <a target="_blank"
             href="http://ec.europa.eu/transport/road_safety/index_en.htm">{i18n('keyfacts_0045')}</a>
        </div>
      </div>
    );
  }
});

let KeyRentalFactsDetailView = React.createClass({
  mixins: [
    React.addons.PureRenderMixin
  ],
  componentDidMount () {
    this.refs.backButton.getDOMNode().focus();
  },
  _onClick: function (event) {
    event.preventDefault();
    KeyRentalFactsActions.toggleDetailView(false, null);
  },
  render: function () {
    return (
      <div className="key-rental-facts-modal-detail-view">
        <div role="button" tabIndex="0" onKeyPress={a11yClick(this._onClick)} onClick={this._onClick}
             className="key-rental-facts-modal-detail-view_back" ref="backButton">
          <i className="icon icon-nav-carrot-left-green" />Back
        </div>
        <h1>{this.props.data.description}</h1>
        <p>{this.props.data.policy_text}</p>
      </div>
    );
  }
});

module.exports = KeyRentalFactsModal;
