'use strict';

var ReservationCursors = require('../../cursors/ReservationCursors');
var BaobabReactMixinBranch = require('baobab-react/mixins').branch;


var MinumumRequirements = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  render: function () {
    return (
      <div className='minimum-requirements'>
        <div className="key-rental-facts-summary-section_header">
          <h3>{enterprise.i18nReservation.keyfacts_0026}</h3>
        </div>
        {this.props.policies.length ?
          <div>
            <div className="key-rental-facts-summary-section_subheader">
              {enterprise.i18nReservation.keyfacts_0027}
            </div>
            {this.props.policies.map((policy) => {
              return <div key={policy.code} onClick={this.props.onPolicyClick.bind(null, policy.code)}
                          role="button" tabIndex="0"
                          onKeyPress={a11yClick(this.props.onPolicyClick.bind(null, policy.code))}
                          className="key-rental-facts-summary-section_item">
                {policy.description}<i className="icon icon-nav-carrot-green"></i>
              </div>
            })}
          </div>
          :
          <div className="key-rental-facts-summary-section_subheader">
            {enterprise.i18nReservation.keyfacts_0028}
          </div>
        }
      </div>
    );
  }
});

module.exports = MinumumRequirements;
