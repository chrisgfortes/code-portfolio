const ReservationCursors = require('../../cursors/ReservationCursors');
const BaobabReactMixinBranch = require('baobab-react/mixins').branch;

const Disputes = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    keyFacts: ReservationCursors.keyFactsPoliciesFromReservation
  },
  render: function () {
    let disputePolicy;
    this.state.keyFacts.some((facts) => {
      if (facts.code === 'DISP') {
        disputePolicy = (<div key={facts.code} className="key-rental-facts-summary-section_item-wrapper">
                <div key={facts.code} onClick={this.props.onPolicyClick.bind(null, facts.code)}
                     role="button" tabIndex="0"
                     onKeyPress={a11yClick(this.props.onPolicyClick.bind(null, facts.code))}
                     className="key-rental-facts-summary-section_item">
                  <i className="icon icon-nav-carrot-green"></i>{facts.description}
                </div>
              </div>);
        return true;
      }
    });
    return (
      <div className="disputes">
        <div className="key-rental-facts-summary-section_header">
          <h3>{i18n('keyfacts_0042')}</h3>
        </div>
        <div>
          <div className="key-rental-facts-summary-section_subheader">
            {i18n('keyfacts_0043')}
          </div>
          {this.props.contactInfo.phone ?
            <a className="contact-info-detail"
               href={'tel:' + this.props.contactInfo.phone}>{this.props.contactInfo.phone}</a>
            :
            <p>{i18n('keyfacts_0050')}</p>
          }
          {this.props.contactInfo.phone && this.props.contactInfo.email &&
          <span> {i18n('resflowlocations_0041')} </span>
          }
          {this.props.contactInfo.email &&
          <a className="contact-info-detail"
             href={'mailto:' + this.props.contactInfo.email}>{this.props.contactInfo.email}</a>
          }
          <p>{i18n('keyfacts_0041')}</p>
        </div>
        {disputePolicy}
      </div>
    );
  }
});

module.exports = Disputes;
