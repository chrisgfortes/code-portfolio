import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import GlobalModal from '../Modal/GlobalModal';
import KeyRentalFactsModal from './KeyRentalFactsModal';
import KeyRentalFactsActions from '../../actions/KeyRentalFactsActions';
import Scroll from '../../utilities/util-scroll';
import { Debugger } from '../../utilities/util-debug';

const KeyRentalFactsBlock = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  getInitialState () {
    return {
      scrollOffset: 0
    }
  },
  componentWillMount () {
    this.logger = new Debugger(window._isDebug, this, true, 'KeyRentalFactsBlock.js');
    //Getting called in AccountService when countries come back
    //KeyRentalFactsService.getKeyFactsDisputesContactInfo();
    KeyRentalFactsActions.setKeyFacts();
  },
  componentDidMount () {
    let hash = window.location.hash;
    hash = hash.replace('#', '');
    if (hash === 'details?keyfacts') {
      KeyRentalFactsActions.toggleKeyRentalFactsModal(true);
    }
  },
  isDebug: false,
  logger: {},
  cursors: {
    keyRentalFactsModal: ['view', 'keyRentalFactsModal', 'modal']
  },
  _onClick (event) {
    this.setState({
      scrollOffset: Scroll.getTopScroll()
    });
    KeyRentalFactsActions.toggleKeyRentalFactsModal(true);
    event.preventDefault();
  },
  _onClose () {
    Scroll.scrollToOffset(this.state.scrollOffset);
    this.setState({
      scrollOffset: 0
    });
    this.keyFacts.getDOMNode().focus();
  },
  getLinkRef (link) {
    this.keyFacts = link;
  },
  render: function () {
    return (
      <div className="key-rental-facts-block">
        <div className="key-rental-facts-block_body cf">
          <div className="key-rental-facts-block_body_left-panel">
            <i className="icon icon-ico-key-facts"></i>
          </div>
          <div className="key-rental-facts-block_body_right-panel">
            <h4>{i18n('keyfacts_0003')}</h4>

            <div className="key-rental-facts-block_body_right-panel_container">
              <p>{i18n('keyfacts_0004')}</p>
              <ul>
                <li><p>{i18n('keyfacts_0005')}</p></li>
                <li><p>{i18n('keyfacts_0006')}</p></li>
              </ul>
              <a role="button" ref={this.getLinkRef} onClick={this._onClick} onKeyPress={a11yClick(this._onClick)} href="#">{i18n('keyfacts_0007')}</a>
            </div>
          </div>
        </div>
        {this.state.keyRentalFactsModal &&
        <GlobalModal active={this.state.keyRentalFactsModal}
                     header={i18n('keyfacts_0003')}
                     cursor={['view', 'keyRentalFactsModal', 'modal']}
                     onClosing={this._onClose}
                     children={<KeyRentalFactsModal/>}/>
        }
      </div>
    );
  }
});

module.exports = KeyRentalFactsBlock;
