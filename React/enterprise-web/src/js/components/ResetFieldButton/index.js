/**
 * ResetFieldButton Component is required that the field have name,
 * we use this prop, for return to onChange and manipulate it.
 * This component return to onChange, that trigger a re-render of parent component.
 * e.g. <input type="text" name="myField" onChange="this._onChange(this)"/>
 */
import classNames from 'classnames';

export default class ResetFieldButton extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      hasValueOnField: true
    }

    this._setStateToField = this._setStateToField.bind(this);
    this.onClear = this.onClear.bind(this);
  }


  componentWillReceiveProps(props){
    const { children } = props;
    this._setStateToField(!!children.props.value);
  }

  _convertToSyntheticEvent(element){
    if (element.props) {
      element = element.props;
    }
    return (_.get(element, 'target') ? element : { target: element });
  }

  _setStateToField(bool){
    this.setState({
      hasValueOnField: bool
    });
  }

  onClear(event) {
    const { children } = this.props;
    const fieldValueLength = _.get(children, 'props.value.length');

    if (event) event.preventDefault();
    if (fieldValueLength) {
      const child = this.renderChildren({
        value: ''
      });
      const element = this._convertToSyntheticEvent(child);
      const name = _.get(children, 'props.name');
      children.props.onChange(element, name);
      this._setStateToField(false);
    }
  }

  renderChildren(props) {
    const { children } = this.props;
    return (
      React.cloneElement(children, Object.assign({
        key: _.get(children, 'props.id'),
        className: classNames(_.get(children, 'props.className'), 'clean-field'),
        value: _.get(children, 'props.value')
      }, props))
    )
  }

  render() {
    const { hasValueOnField } = this.state;
    const child = this.renderChildren();
    const hasValueOnChild = !!_.get(child, 'props.value.length');

    return (
      <div className="clean-enable">
        {child}

        {hasValueOnChild && hasValueOnField && (
          <button
            className="clean-button"
            onClick={this.onClear}
            aria-hidden="true"
          >
            <i className="icon icon-ENT-icon-close" />
          </button>
        )}
      </div>
    );
  }
}

ResetFieldButton.displayName = "ResetFieldButton";
