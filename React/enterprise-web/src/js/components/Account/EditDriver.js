import Validator from '../../utilities/util-validator';
import Error from '../Errors/Error';
import classNames from 'classnames';
import AccountController from "../../controllers/AccountController";
import EnrollmentController from '../../controllers/EnrollmentController';
import ResetFieldButton from "../ResetFieldButton";
import { GLOBAL, EXPEDITED, LOCALES } from '../../constants';

export default class EditDriver extends React.Component{
  constructor(props) {
    super(props);

    this.state = {
      birthDate: null,
      licenseNumber: null,
      licenseIssue: null,
      licenseExpiry: null,
      licenseCountry: null,
      licenseRegion: null,
      issueCountryData: null,
      loading: true,
      fieldErrors: []
    };

    this.validator = new Validator(this, this.fieldMap);
    this._onSave = this._onSave.bind(this);
    this._onCountryChange = this._onCountryChange.bind(this);
    this.fieldMap = this.fieldMap.bind(this);
    this._onRegionChange = this._onRegionChange.bind(this);
    this._handleInputBlur = this._handleInputBlur.bind(this);
    this._handleInputFocus = this._handleInputFocus.bind(this);
    this._handleInputChange = this._handleInputChange.bind(this);
    this.getCountryInfo = this.getCountryInfo.bind(this);
  }

  componentDidMount(){
    const { profile } = this.props;
    if (profile) {
      const license = profile.license_profile;

      this.state = Object.assign(this.state, {
        birthDate: license.birth_date,
        licenseNumber: license.license_number,
        licenseIssue: license.license_issue,
        licenseIssueModified: false,
        licenseExpiry: license.license_expiry,
        licenseExpiryModified: false,
        licenseCountry: license.country_code,
        licenseRegion: license.country_subdivision_name
      });

      AccountController
        .getIssueCountrySubdivisions(license.country_code)
        .then(() => {
          const issueCountry = this.getCountryInfo(license.country_code);
          this.setIssueCountryState(issueCountry);
        });
    }
  }

  setIssueCountryState(data){
    this.setState({
      issueCountryData: data,
      loading: false
    });
  }

  fieldMap () {
    const {licenseNumber, licenseCountry, licenseRegion, licenseIssue, licenseExpiry, issueCountryData} = this.state;
    const { account } = this.props;
    const enableSubdivision = !!_.get(account, 'subdivisions.length');
    const re = this.maskedDateExpression();
    const licenseIssueValid = re.test(licenseIssue) || GLOBAL.REGEX_MASK_CHAR.test(licenseIssue);
    const licenseExpiryValid = re.test(licenseExpiry) || GLOBAL.REGEX_MASK_CHAR.test(licenseExpiry);
    const issueCountry = issueCountryData;

    let map = {
      refs: {
        'licenseIssue': this.licenseIssue,
        'licenseExpiry': this.licenseExpiry,
        'licenseRegion': this.licenseRegion,
        'licenseNumber': this.licenseNumber,
        'issueCountry': this.issueCountry
      },
      value: {
        licenseNumber,
        licenseCountry,
        licenseRegion
      },
      schema: {
        licenseNumber: 'string',
        licenseCountry: 'string',
        licenseRegion: '?string'
      }
    };

    if (enableSubdivision) {
      map.value.licenseRegion = licenseRegion;
      map.schema.licenseRegion = enterprise.utilities.isMaskedField(this.state.licenseRegion) ? () => true : 'string';
    }

    if (issueCountry) {
      if (issueCountry.license_expiry_date) {
        map.value.licenseExpiry = licenseExpiry;
        map.schema.licenseExpiry = () => {
          if (issueCountry.license_expiry_date && issueCountry.license_expiry_date === GLOBAL.LICENSE.MANDATORY) {
            return licenseExpiryValid;
          } else if (issueCountry.license_expiry_date && issueCountry.license_expiry_date === GLOBAL.LICENSE.OPTIONAL) {
            return licenseIssueValid || licenseExpiryValid;
          }
          return true;
        };
      }
      if (issueCountry.license_issue_date) {
        map.value.licenseIssue = licenseIssue;
        map.schema.licenseIssue = () => {
          if (issueCountry.license_issue_date && issueCountry.license_issue_date === GLOBAL.LICENSE.MANDATORY) {
            return licenseIssueValid;
          } else if (issueCountry.license_issue_date && issueCountry.license_issue_date === GLOBAL.LICENSE.OPTIONAL) {
            return licenseIssueValid || licenseExpiryValid;
          }
          return true;
        };
      }

      if (enableSubdivision) {
        map.value.licenseRegion = this.licenseRegion.value;
        map.schema.licenseRegion = enterprise.utilities.isMaskedField(this.state.licenseRegion) ? () => true : '?string';
      }
    }

    return map;
  }

  getCountryInfo(countryCode){
    const { account } = this.props;
    return account.countries.find((item) => {
      return (item.country_code === countryCode);
    }) || {};
  }

  maskedDateExpression () {
    return enterprise.settings.regularex.maskedDate;
  }

  _onCountryChange (event) {
    if (this.props.profile) {
      const countrySelected = _.get(event, 'target.value') || '';
      event.target.value = countrySelected;

      // this will show the spinner when the country is changed,
      // but that may be a UX that we don't want
      // this.setState({
      //   loading: true
      // }, () => {
        EnrollmentController
          .getSubdivisions(countrySelected)
          .then(() => {
            const issueCountry = this.getCountryInfo(countrySelected);

            this.setState({
              licenseCountry: countrySelected,
              licenseRegion: null,
              licenseIssue: null,
              licenseExpiry: null,
              licenseNumber: null//,
              // loading: false
            });

            this.setIssueCountryState(issueCountry);
          });
      // });

    }
  }

  _onRegionChange(event) {
    this.setState({
      licenseRegion: event.target.value
    });
    this.validator.validate('licenseRegion', event.target.value);
  }

  _handleInputBlur (attribute) {
    const { profile } = this.props;
    const fieldBit = attribute.concat('Modified');
    if (!this.state[fieldBit]) {
      const license = _.get(profile, 'license_profile') || {};
      const map = {
        licenseIssue: license.license_issue,
        licenseNumber: license.license_number,
        licenseRegion: _.get(this, 'licenseRegion.value') || _.get(license, 'country_subdivision_name'),
        licenseExpiry: license.license_expiry
      };

      const mapResult = map[attribute];
      const fieldValue = (mapResult || '');

      this.setState({
        [attribute]: fieldValue
      });

      this[attribute].value = fieldValue;
    }
  }

  _handleInputFocus (attribute) {
    this.setState({
      [attribute]: ''
    });

    this[attribute].value = '';
  }

  _handleInputChange (attribute, event) {
    if (attribute === 'licenseRegion' && !event.target.value) {
      return false;
    } else {
      let modifiedFlag = [
        'licenseIssue',
        'licenseExpiry',
        'licenseRegion',
        'licenseNumber'
      ].includes(attribute);

      const fieldValue = event.target.value;
      const isValid = this.validator.validate(attribute, fieldValue);

      this.setState({
        [attribute]: fieldValue,
        [attribute.concat('Modified')]: modifiedFlag,
        fieldErrors: [(!isValid ? attribute : '')]
      });
    }
  }

  _onSave () {
    const hasFieldErrors = this.validator.validateAll();
    this.setState({
      fieldErrors: hasFieldErrors.errors
    });

    if (hasFieldErrors.valid && this.props.profile && !this.state.submitLoading) {
      this.setState({submitLoading: true});
      // @todo this should be replaced by leveraging a factory
      let formData = AccountController.cleanFormData(this.state);
      formData.profile = this.props.profile;
      // ECR-14250 and again
      formData.account = this.props.profile.basic_profile.loyalty_data.loyalty_number;

      AccountController
        .updateLicense(formData)
        .then((statusLoading, hasErrors) => {
          this.setState({ submitLoading: statusLoading });
          if (!hasErrors) AccountController.setAccountEditModal(false);
        })
        .catch(() => this.setState({ submitLoading: false }))
    }
  }

  render () {
    const {close, profile, account, errors} = this.props;
    const {
      licenseCountry,
      submitLoading,
      issueCountryData,
      licenseNumber,
      licenseRegion,
      licenseIssue,
      licenseExpiry,
      loading
    } = this.state;

    const saveButtonClasses = classNames({
      'btn': true,
      'save': true,
      'disabled': submitLoading
    });

    const licenseIssueDate = _.get(issueCountryData, 'license_issue_date');
    const enableLicenseIssueDate =
      EnrollmentController.isOptionalDate(licenseIssueDate) ||
      EnrollmentController.isMandatoryDate(licenseIssueDate);

    const licenseExpiryDate = _.get(issueCountryData, 'license_expiry_date');
    const enableLicenseExpiryDate =
      EnrollmentController.isOptionalDate(licenseExpiryDate) ||
      EnrollmentController.isMandatoryDate(licenseExpiryDate);

    const driverProfile = (profile ? profile.license_profile : {});
    const enableSubdivision = !!_.get(account, 'subdivisions.length');
    const subdivisionLabel = AccountController.getSubdivisionLabelBasedOnCountry(licenseCountry);
    const subdivisionValue = ((licenseCountry === LOCALES.GB) ? EXPEDITED.LICENSE.REGIONS.DVLA : licenseRegion);

    return (
      <form className="personal-entry-form driver" ref={r => this.form = r }>
        <abbr title="Required" className="required-label">
          <i>{i18n('resflowreview_0004')}</i>
        </abbr>

        <h2>{i18n('resflowreview_0121')}</h2>
        <Error errors={errors} type="GLOBAL"/>

        { loading ?
          <div className="transition"/>
          :
         (<div>
            <div className="field-container issue-country">
              <label htmlFor="country">{i18n('resflowreview_0070')}</label>
              {account.countries.length ?
                <select
                  ref={r => this.issueCountry = r}
                  className="styled"
                  onChange={this._onCountryChange}
                  id="issue-country"
                  value={licenseCountry}
                >
                  {account.countries.map(function (country, index) {
                    return (
                      <option
                        key={index}
                        data-index={index}
                        value={country.country_code}>{country.country_name}
                      </option>
                    );
                  })}
                </select> : <div className="loading" />
              }
            </div>

            {enableSubdivision && (
              <div className="field-container issue-authority">
                <label htmlFor="issue-authority">{subdivisionLabel}</label>
                <select
                  className="styled"
                  onChange={this._onRegionChange}
                  onBlur={this._handleInputBlur.bind(this, 'licenseRegion')}
                  onFocus={this._handleInputFocus.bind(this, 'licenseRegion')}
                  id="subdivision"
                  ref={r => this.licenseRegion = r}
                  value={subdivisionValue}
                >

                  {(licenseCountry !== LOCALES.GB) && (
                    <option key="0"
                      value={enterprise.utilities.isMaskedField(licenseRegion) ? licenseRegion : ''}>
                      {enterprise.utilities.isMaskedField(licenseRegion) ? licenseRegion : i18n('resflowreview_0152')}
                    </option>
                  )}

                  {(licenseCountry === LOCALES.GB) && (
                    <option
                      value={EXPEDITED.LICENSE.REGIONS.DVLA}
                      key={EXPEDITED.LICENSE.REGIONS.DVLA}
                    >{EXPEDITED.LICENSE.REGIONS.DVLA}
                    </option>
                  )}

                  {(licenseCountry !== LOCALES.GB) && (
                    account.subdivisions.map(function (region, index) {
                      return (
                        <option
                          key={index}
                          value={region.country_subdivision_code}>{region.country_subdivision_name}
                        </option>
                      );
                    })
                  )}
                </select>
              </div>
            )}

            <div className="field-container birth-date">
              <label htmlFor="birth-date">
                {i18n('resflowreview_0076')} &nbsp;
                ({i18n('eplusenrollment_0030') + '-' + i18n('eplusenrollment_0026') + '-' + i18n('eplusenrollment_0028')})
              </label>
              <input ref={r => this.birthDate = r }
                     disabled
                     id="birth-date" type="text"
                     defaultValue={driverProfile.birth_date || null}/>
            </div>
            <div className="field-container">
              <label htmlFor="licenseNumber">{i18n('resflowreview_0072')}</label>

              <ResetFieldButton>
                <input
                  ref={r => this.licenseNumber = r}
                  onBlur={this._handleInputBlur.bind(this, 'licenseNumber')}
                  onFocus={this._handleInputFocus.bind(this, 'licenseNumber')}
                  onChange={this._handleInputChange.bind(this, 'licenseNumber')}
                  id="licenseNumber"
                  type="text"
                  name="licenseNumber"
                  value={licenseNumber}
                />
              </ResetFieldButton>
            </div>

            {enableLicenseIssueDate && (
              <div className="field-container license-issue">
                <label htmlFor="license-issue">
                  {i18n('resflowreview_0720')} &nbsp;
                  ({i18n('eplusenrollment_0030') + '-' + i18n('eplusenrollment_0026') + '-' + i18n('eplusenrollment_0028')})
                </label>
                <ResetFieldButton>
                  <input
                    ref={r => this.licenseIssue = r }
                    onChange={this._handleInputChange.bind(this, 'licenseIssue')}
                    onBlur={this._handleInputBlur.bind(this, 'licenseIssue')}
                    onFocus={this._handleInputFocus.bind(this, 'licenseIssue')}
                    id="license-issue" type="text" name="licenseIssue"
                    value={licenseIssue}
                  />
                </ResetFieldButton>
              </div>
            )}

            {enableLicenseExpiryDate && (
              <div className="field-container expire-date">
                <label htmlFor="expire-date">
                  {i18n('resflowreview_0037')} &nbsp;
                  ({i18n('eplusenrollment_0030') + '-' + i18n('eplusenrollment_0026') + '-' + i18n('eplusenrollment_0028')})
                </label>

                <ResetFieldButton>
                  <input
                    ref={r => this.licenseExpiry = r}
                    onChange={this._handleInputChange.bind(this, 'licenseExpiry')}
                    onBlur={this._handleInputBlur.bind(this, 'licenseExpiry')}
                    onFocus={this._handleInputFocus.bind(this, 'licenseExpiry')}
                    id="license-expiry" type="text"
                    name="licenseExpiry"
                    value={licenseExpiry}
                    className="licenseExpiry"
                  />
                </ResetFieldButton>
              </div>
            )}

            <div className="modal-actions">
              <div className={submitLoading ? 'loading' : false}></div>
              <div onClick={this._onSave}
                   className={saveButtonClasses}>{i18n('eplusaccount_0064')}</div>
              <span onClick={close}
                    className="btn cancel">{i18n('eplusaccount_0065')}</span>
            </div>
         </div>)}
      </form>
    );
  }
}

EditDriver.displayName = "EditDriver";
