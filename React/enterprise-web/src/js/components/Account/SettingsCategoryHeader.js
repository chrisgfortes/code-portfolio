import AccountActions from '../../actions/AccountActions';

export default function SettingsCategoryHeader({title, modal}){
  return (
    <thead>
    <tr>
      <th colSpan="2">
        <h2>
          {title}
          <span 
            onClick={_editModal.bind(this, modal)}
            className="edit">
              {i18n('resflowreview_0084')}
          </span>
        </h2>
      </th>
    </tr>
    </thead>
  );
}

function _editModal (modal) {
  AccountActions.setAccountEditModal(modal);
}

SettingsCategoryHeader.displayName = "SettingsCategoryHeader";
