import Validator from '../../utilities/util-validator';
import Error from '../Errors/Error';
import classNames from 'classnames';
import ProfileController from "../../controllers/ProfileController";
import AccountController from "../../controllers/AccountController";
import { PasswordErrorList, PasswordErrorListItem } from '../Password/PasswordErrorList';

export default class EditPassword extends React.Component{
  constructor(props){
    super(props);

    this.state = {
      password: null,
      passwordConfirm: null,
      passwordRules: {
        space: null,
        length: null,
        blacklist: null,
        oneLetter: null,
        oneNumber: null,
        email: null
      },
      confirmPasswordRules: {
        match: null
      }
    };

    this.validator = new Validator(this, this.fieldMap);
    this._onSave = this._onSave.bind(this);
    this._handleInputChange = this._handleInputChange.bind(this);
  }

  componentWillUnmount(){
    AccountController.cleanEditModalErrors('accountPassword');
  }

  fieldMap () {
    return {
      value: {
        password: this.password.value,
        passwordConfirm: this.passwordConfirm.value
      },
      schema: {
        password: 'password',
        passwordConfirm: () => {
          let passwordConfirm = this.passwordConfirm.value;
          let matches = passwordConfirm.length > 0 && this.password.value === passwordConfirm;
          this.setState({confirmPasswordRules: {match: matches}});
          return matches;
        }
      }
    }
  }

  _handleInputChange (attribute, event) {
    this.setState({
      [attribute]: event.target.value
    });
    let errorObj = this.validator.validate(attribute, _.get(event, 'target.value'));
    if (typeof errorObj === 'object' && errorObj.errorReasons.password) {
      this.setState({passwordRules: errorObj.errorReasons.password});
    }
  }

  _onSave (event) {
    event.preventDefault();
    AccountController.cleanEditModalErrors('accountPassword');

    let errorObj = this.validator.validateAll();
    let valid = errorObj.valid;

    if (valid) {
      this.setState({
        submitLoading: true
      });

      ProfileController
        .updatePassword(this.fieldMap().value)
        .then((statusModal) => this.setState({ submitLoading: statusModal }))
        .catch(() => this.setState({ submitLoading: false }));

    } else if (typeof errorObj === 'object' && errorObj.errorReasons.password) {
      this.setState({passwordRules: errorObj.errorReasons.password});
    }
  }

  render () {
    const {close, errors} = this.props;
    const state = this.state;

    let saveButtonClasses = classNames({
      'btn': true,
      'save': true,
      'disabled': state.submitLoading
    });

    return (
      <form className="personal-entry-form password" ref="form">
        <abbr title="Required" className="required-label">
          <i>{i18n('resflowreview_0004')}</i>
        </abbr>

        <h2>{i18n('loyaltysignin_0004')}</h2>
        <Error errors={errors} type="GLOBAL"/>
        <div className="field-container password">
          <label htmlFor="password">{i18n('eplusaccount_0070')}</label>
          <input onChange={this._handleInputChange.bind(this, 'password')}
                 id="password" type="password"
                 ref={ r => this.password = r }
          />
          <PasswordErrorList errors={state.passwordRules} />
        </div>
        <div className="field-container password">
          <label htmlFor="passwordConfirm">{i18n('resflowreview_0048')}</label>
          <input onChange={this._handleInputChange.bind(this, 'passwordConfirm')}
                  id="passwordConfirm" type="password"
                  ref={ r => this.passwordConfirm = r } />
          <ul className="criteria error-list">
            <PasswordErrorListItem showOnError={true} text={i18n('loyaltysignin_0044') || "Passwords Don't Match"} valid={_.get(state, 'confirmPasswordRules.match')} />
          </ul>
        </div>
        <div className="modal-actions">
          <div className={state.submitLoading ? 'loading' : false}></div>
          <button
            onClick={this._onSave}
            className={saveButtonClasses}>
              {i18n('eplusaccount_0064')}
          </button>
          <button
            onClick={close}
            className="btn cancel">
              {i18n('eplusaccount_0065')}
          </button>
        </div>
      </form>
    );
  }
};

EditPassword.displayName = "EditPassword";
