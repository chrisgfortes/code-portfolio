import ExistingReservationController from '../../controllers/ExistingReservationController';
import CurrentReservationSummary from '../ExistingReservation/CurrentReservationSummary';
import UpcomingReservationSummary from '../ExistingReservation/UpcomingReservationSummary';
import PastReservationHeader from '../ExistingReservation/PastReservationHeader';
import PastReservationBody from '../ExistingReservation/PastReservationBody';
import MyTrips from '../ExistingReservation/MyTrips';
import ToggleSearch from '../ExistingReservation/ToggleSearch';
import { exists } from '../../utilities/util-predicates';
import Error from '../Errors/Error';

export default class Reservations extends React.Component{
  constructor(){
    super();
  }

  componentDidMount() {
    ExistingReservationController.getAllMyTrips();
  }

  componentWillUnmount() {
    ExistingReservationController.resetMyTrips();
  }

  _moreTripsOfType(type, event) {
    event.preventDefault();
    ExistingReservationController.getTripsOfType(type, true);
  }

  render(){
    const {
        supportLinks,
        reservations,
        reservationSearchVisible,
        modifyModalOpen,
        rentalDetailsModal,
        loading,
        myTripsHistory,
        pastTrips,
        errors,
        currentTrips,
        upcomingTrips,
        currentTripsLoadingClass,
        upcomingTripsLoadingClass,
        pastTripsLoadingClass,
        profile
    } = this.props;

    let current,
        upcoming,
        past,
        hasData = null,
        currentMore,
        upcomingMore,
        pastMore = null;

    if (currentTrips.length) {
      current = myTripsHistory.CURRENT.map(
        (reservation, index) =>
          <CurrentReservationSummary
            key={reservation.confirmation_number+index}
            reservation={reservation}
            supportLinks={supportLinks}
          />
      );
      if (myTripsHistory.more.CURRENT) {
        currentMore =
          <a
            className="btn"
            href="#"
            onClick={this._moreTripsOfType.bind(this, 'CURRENT')}>
              More Current Trips
          </a>
      }
    }
    if (upcomingTrips.length) {
      upcoming = myTripsHistory.UPCOMING.map(
        //@TODO update this to use MY TRIPS when being worked on
        (reservation, index) =>
          <UpcomingReservationSummary
            isAuthenticated={exists(profile)}
            key={reservation.confirmation_number+index}
            reservation={reservation}
            supportLinks={supportLinks}
            modifyModalOpen={modifyModalOpen}
          />
      );

      if (myTripsHistory.more.UPCOMING) {
        upcomingMore =
          <a
            className="btn"
            href="#"
            onClick={this._moreTripsOfType.bind(this, 'UPCOMING')}>
              More Upcoming Trips
          </a>
      }
    }
    if (pastTrips.length) {
      past = myTripsHistory.PAST.map(
        (reservation, index) =>
          <PastReservationBody
            key={`${reservation.confirmation_number}${index}`}
            reservation={reservation}
            supportLinks={supportLinks}
          />
      );
      past.unshift(<PastReservationHeader key="past-header" />);

      if (myTripsHistory.more.PAST) {
        pastMore =
          <a
            className="btn"
            href="#"
            onClick={this._moreTripsOfType.bind(this, 'PAST')}>
              More Past Trips
          </a>
      }
    }

    return (
      <div className={"my-reservations"}>
        <Error errors={errors} type="GLOBAL"/>

        <div className="toggle-search-reservation-container">
          <ToggleSearch {...{reservationSearchVisible, loading}} />

          <div className="my-reservations-body existing-reservation">
            {reservations && reservations.length > 0 &&
            <div className="reservation-list">
              <MyTrips
                tripsType={reservations[0].reservation_type}
                reservation={reservations[0]}
                noSearch
                {...{
                  supportLinks,
                  reservationSearchVisible,
                  loading,
                  modifyModalOpen,
                  rentalDetailsModal
                }}
              />
            </div>
            }
          </div>
        </div>
        <div className={"my-reservations-body existing-reservation"}>
          <div className="current-trips trips">
            <h2> {i18n('resflowviewmodifycancel_0003')} </h2>
            {current}
            <div className={currentTripsLoadingClass}>
              {currentMore}
              <div className="no-reservations">
                <h3>{i18n('eplusaccount_0116')}</h3>
              </div>
              <div className="error-reservations">
                <h3>Error Retrieving Current Past Trips</h3>
              </div>
            </div>
          </div>
          <div className="upcoming-trips trips">
            <h2> {i18n('resflowviewmodifycancel_0018')} </h2>
            {upcoming}
            <div className={upcomingTripsLoadingClass}>
              {upcomingMore}
              <div className="no-reservations">
                <h3>{i18n('resflowviewmodifycancel_0014')}</h3>
              </div>
              <div className="error-reservations">
                <h3>Error Retrieving Upcoming Trips</h3>
              </div>
            </div>
          </div>
          <div className="past-trips trips">
            <h2> {i18n('resflowviewmodifycancel_0011')} </h2>

            <div className="past-reservation-summary cf">
              {past}
            </div>
            <div className={pastTripsLoadingClass}>
              <div className="no-reservations">
                <h3>{i18n('resflowviewmodifycancel_0015')}</h3>

                <p>{i18n('resflowviewmodifycancel_0016').concat(' ')}
                  <a href={supportLinks.print_receipt_url} className="no-res-link"
                     target="_blank">{i18n('resflowviewmodifycancel_0082')}</a>
                </p>
              </div>
              <div className="error-reservations">
                <h3>Error Retrieving Past Trips</h3>
              </div>
              {pastMore}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Reservations.defaultProps = {
  modelController: null
}

Reservations.displayName = "Reservations";
