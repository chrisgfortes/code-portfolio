import ContactSetting from './Settings/ContactSetting';
import DriverSetting from './Settings/DriverSetting';
import Payments from './Payments';
import PasswordSetting from './Settings/PasswordSetting';
import AccountEditModal from './AccountEditModal';
import AccountController from '../../controllers/AccountController';

export default class Settings extends React.Component{
  constructor() {
    super();
    AccountController.getCountries();
  }

  render () {
    const {profile, supportLinks, paymentsErrors} = this.props;

    return (
      <div className="account-settings">
        <ContactSetting
          profile={profile}
          support={supportLinks}
        />

        <DriverSetting
          profile={profile}
        />

        <Payments
          profile={profile}
          errors={paymentsErrors}
        />

        <PasswordSetting />
        <AccountEditModal />
      </div>
    );
  }
}

Settings.displayName = 'Settings';
