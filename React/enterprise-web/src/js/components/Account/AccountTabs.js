import AccountController from '../../controllers/AccountController';
import classNames from 'classnames';
import { PARTNERS } from '../../constants';

function _activateTab (tab) {
  tab = _.get(tab, 'target.value') || tab;
  AccountController.setAccountTab(tab);
}

function getMenuClass(activeTab, menu){
  return classNames({
    [menu.name.toLowerCase()]: true,
    tab: true,
    active: activeTab === menu.name.toLowerCase() || (!activeTab && _.get(menu, 'isDefault'))
  })
}

const MENUS = [{
  name: 'reservation',
  label: i18n('reservationwidget_0004'),
  isLogged: true
},{
  name: 'reward',
  label: i18n('eplusaccount_0033'),
  isLogged: true,
  isEPExclusive: true
},{
  name: 'settings',
  isDefault: true,
  label: i18n('eplusaccount_0034'),
  isLogged: true,
  isEPExclusive: true
},{
  name: PARTNERS.BRAND.ENTERPRISE_PLUS,
  isDefault: true,
  label: i18n('loyaltysignin_0032'),
  isLogged: false
},{
  name: PARTNERS.BRAND.EMERALD_CLUB,
  label: i18n('loyaltysignin_0033'),
  isLogged: false
}];

export default function AccountTabs({loggedIn, brand, account}){
  const currentMenus = 
    MENUS
      .filter((menu) => menu.isLogged === loggedIn)
      .filter((menu) => !menu.isEPExclusive || brand !== PARTNERS.BRAND.EMERALD_CLUB)

    return (
      <div className="account-tabs-container">
        <ul className="account-tabs">
          {
            currentMenus
              .map((menu) => {
                let classNames = getMenuClass(account.tab, menu);

                return (
                  <li key={menu.name} onClick={_activateTab.bind(this, menu.name.toLowerCase())} className={classNames}>
                      {menu.label}
                  </li>
                )
            })
          }
        </ul>

        <div className="custom-select mobile-select">
          <select onChange={_activateTab} defaultValue={account.tab}>
            {
              currentMenus
                .map((menu) => (
                    <option key={menu.name} value={menu.name}>
                      {menu.label}
                    </option>
                ))
            }
          </select>
        </div>
      </div>
    );
}
