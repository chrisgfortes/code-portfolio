export default function DebitCard ({close}) {
  return (
    <div className="debit-cards">
      <h2>{i18n('prepay_0053') || 'Debit cards not accepted'}</h2>
      <p>{i18n('prepay_0054') || 'It looks like you’re trying to provide a debit card. Although some Enterprise Rent-A-Car locations may accept debit cards (policies vary by location), we do not accept debit cards online for *Pay Now* reservations.'}</p>
      <p>{i18n('prepay_0055') || 'Please add a *credit* card or choose our Pay Later option to continue.'}</p>

      <div className="modal-actions">
        <span tabIndex="0" onKeyPress={a11yClick(close)} onClick={close}
          className="btn save">{i18n('resflowcarselect_0065') || 'Close'}</span>
      </div>
    </div>
  );
}

DebitCard.displayName = "DebitCard";

