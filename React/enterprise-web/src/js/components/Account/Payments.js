import Error from '../Errors/Error';
import { getFilteredPayments, isCreditCardMethod, getCardLabel, isBusinessAccount } from '../../controllers/PaymentModelController';
import { setModifyPayment, setAccountEditModal, setPangui } from '../../controllers/ProfileController';
import { maskCC } from '../../controllers/PaymentModelController';
import { Debugger } from '../../utilities/util-debug';

export default class Payments extends React.Component{
  constructor() {
    super();
    this.state = {
      edit: true
    }
    this.logger = new Debugger(window._isDebug, this, true, this.name);
    this._preferred = this._preferred.bind(this);
    this._onEdit = this._onEdit.bind(this);
    this._updatePayment = this._updatePayment.bind(this);
    this._onRemovePayment = this._onRemovePayment.bind(this);
    this._onAdd = this._onAdd.bind(this);
    this.mapPaymentString = this.mapPaymentString.bind(this);
  }
  _onEdit () {
    this.setState({
      edit: !this.state.edit
    });
  }
  _onRemovePayment (payment) {
    setModifyPayment(payment);
    setAccountEditModal('remove');
  }
  _updatePayment (payment) {
    setModifyPayment(payment);
    setAccountEditModal('payment');
    setPangui(true);
  }
  _onAdd () {
    setModifyPayment('new');
    setPangui(true);
    setAccountEditModal('new');
  }
  _preferred (payment) {
    setModifyPayment(payment);
    setAccountEditModal('preferred');
    setPangui(true);
  }
  mapPaymentString (typeString) {
    let retString = '';
    if (typeString.toLowerCase().indexOf('credit') > -1) {
      retString = i18n('eplusaccount_0008') || 'Credit Cards';
    } else {
      retString = i18n('eplusaccount_0003') || 'Billing Numbers';
    }
    return retString;
  }
  render () {
    const {profile, errors} = this.props;
    const {edit} = this.state;
    let creditCount = 0;
    let filteredPayments = getFilteredPayments(profile);

    if (enterprise.hideCreditCard && !filteredPayments.length) {
      return null;
    }

    this.logger.log('filteredPayments', filteredPayments);

    return (
      <div className="account-payments">
        <Error errors={errors} type="GLOBAL" />
        <table className="payments">
          <thead>
          <tr>
            <th colSpan="3">
              <h2>
                {i18n('resflowreview_0033')}
              </h2>
            </th>
          </tr>
          </thead>
          {filteredPayments.length > 0 ?
            filteredPayments.map((item, index, arr)=> {
              let paymentLabel = getCardLabel(item, profile);

              let lastRowIndex = Math.max(0, index - 1); // larger value, avoid -1
              let conditionState = (index === 0 || arr[lastRowIndex].payment_type !== item.payment_type);

              if (isCreditCardMethod(item)) {
                creditCount = creditCount + 1;
              }

              return (<tbody key={index}>
                {conditionState ? <tr><td colSpan="5" className="header">{this.mapPaymentString(item.payment_type)}</td></tr> : false }
              <tr>
                <td className="buffer"/>
                <td className="mainLabel">{paymentLabel}<br />
                    {item.preferred ?
                      <span className="accented"> <i className="icon icon-checkmark-circle-green"/>
                        {i18n('eplusaccount_0010')} <span className="edit change"
                                onClick={this._preferred.bind(this, item)}>({i18n('expedited_0023') || 'Change'})</span></span>
                      : false}
                </td>
                <td>
                  <div> {isBusinessAccount(item) ? item.billing_number : maskCC(item.last_four)}</div>
                  <div className="expiration"> {item.expiration_date ? i18n('resflowreview_0037') + ': ' + item.expiration_date : ''}</div>
                </td>
                <td>
                  {edit ? <span onClick={() => this._updatePayment(item)}
                                className="edit update">{i18n('eplusaccount_0119') || 'Edit'}</span> : false}
                </td>
                <td>
                  {edit ? <span onClick={() => this._onRemovePayment(item)}
                                className="edit remove">{i18n('eplusaccount_0007') || 'Delete'}</span> : false}
                </td>
              </tr>
             </tbody>
              );
            })
            :
            <tr> {/* no payment methods */}
              <td colSpan="4">{i18n('eplusaccount_0150')}</td>
            </tr>
          }
          {edit && !enterprise.hideCreditCard ?
            <tbody>
              <tr>
                <td className="buffer"/>
                <td colSpan="4">
                  {creditCount >= enterprise.settings.maxCreditCards ? <div>{i18n('eplusaccount_2005') || 'MAXIMUM CREDIT CARDS ON FILE REACHED'}
                  <br />{i18n('eplusaccount_2006') || 'You can only add up to 4 credit cards.'}</div> :
                  <div onClick={this._onAdd}
                     className="btn add">{i18n('eplusaccount_0012')}</div>}
                </td>
              </tr>
            </tbody> : false
          }
        </table>
      </div>
    );
  }
}

Payments.propTypes = {
  profile: React.PropTypes.object.isRequired,
  errors: React.PropTypes.array
};

Payments.displayName = "Payments";
