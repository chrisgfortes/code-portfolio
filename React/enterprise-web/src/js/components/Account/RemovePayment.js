import PaymentModelController from '../../controllers/PaymentModelController';

export default class RemovePayment extends React.Component{
  constructor() {
    super();
    this.state = {
      submitLoading: false
    };
    this._confirmRemove = this._confirmRemove.bind(this);
  }
  _confirmRemove (reference, close) {
    this.setState({submitLoading: true});

    PaymentModelController
      .removePayment(reference)
      .then((responseData) => {
        this.setState({submitLoading: false});
        if (responseData.message === 'success') {
          PaymentModelController.updateProfileByResponse(responseData.response);
          close();
        }
      });
  }
  render () {
    const {close, account} = this.props;
    let currentPayment = account.modifyPayment,
        contentHeader,
        content,
        accessibilityText;

    if (currentPayment.payment_type === 'CREDIT_CARD') {
      contentHeader = (i18n('eplusaccount_2025') || 'Credit Card:') + ' ' + PaymentModelController.maskCC(currentPayment.last_four);
      content = i18n('eplusaccount_2009') || 'Would you like to delete this credit card?';
      accessibilityText = i18n('eplusaccount_2011') || 'Delete Credit Card';
    } else if (currentPayment.payment_type === 'BUSINESS_ACCOUNT_APPLICANT') {
      contentHeader = (i18n('eplusaccount_2014') || 'Billing Number:') + ' ' + currentPayment.billing_number;
      content = i18n('eplusaccount_2008') || 'Would you like to delete this billing number? You will no longer be able to select it when renting a vehicle.';
      accessibilityText = i18n('eplusaccount_2010') || 'Delete Billing Number';
    }
    return (
      <div className="confirm-remove-payment">
        { this.state.submitLoading ? <div className="loading"/> :
          <div className="payment-remove-content">
            <h2>{contentHeader}</h2>
            <p>{content}</p>
            <div className="modal-actions">
              <button
                aria-label={accessibilityText}
                onClick={this._confirmRemove.bind(null, currentPayment.payment_reference_id, close)}
                className="btn payment-delete">{i18n('eplusaccount_0007')}
              </button>
              <button
                onClick={close}
                aria-label={i18n('resflowlocations_0057')}
                className="btn payment-cancel">{i18n('resflowlocations_0057')}
              </button>
            </div>
          </div>}
      </div>
    );
  }
}

RemovePayment.displayName = "RemovePayment";
