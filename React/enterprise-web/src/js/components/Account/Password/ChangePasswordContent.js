import { PasswordErrorList } from '../../Password/PasswordErrorList';
import LoginController from '../../../controllers/LoginController';
import Error from '../../Errors/Error';

export default class ChangePasswordContent extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      passwordRules: {
        length: null,
        blacklist: null,
        oneLetter: null,
        oneNumber: null,
        passwordMatch: null
      },
      loading: false,
      success: false,
      hasError: false
    };
    this._handleInputChange = this._handleInputChange.bind(this);
    this._hasError = this._hasError.bind(this);
    this._generateErrorIcon = this._generateErrorIcon.bind(this);
    this._generateErrorClass = this._generateErrorClass.bind(this);
    this._submit = this._submit.bind(this);
  }
  _handleInputChange (attribute, event) {
    this.setState(
      {
        [attribute]: event.target.value
      }
    );
    let rules = LoginController.validationRules['changePassword'](this.changePassword, this.changePasswordConfirm);
    rules = LoginController.validationRules['changePasswordConfirm'](this.changePassword, this.changePasswordConfirm);
    this._hasError(rules);
    this.setState({passwordRules: rules});
  }
  _hasError (rules) {
    let ruleKeys = Object.keys(rules);
    this.setState({hasError: ruleKeys.some((key) => rules[key] === false)});
  }
  _generateErrorIcon (type) {
    if (this.state.passwordRules[type] === null) {
      return 'empty';
    } else if (this.state.passwordRules[type]) {
      return 'icon-icon-checkmark-thin-green';
    }
    
    return 'icon-alert-small';
  }
  _generateErrorClass (type) {
    return (this.state.passwordRules[type] || this.state.passwordRules[type] == null ? 'hide' : 'error');
  }
  _submit () {
    let data = this.props.data;

    if (this.state.hasError) return false;

    data.newPassword = this.changePassword.value;
    this.setState({loading: true});

    LoginController.login(data).then((response) => {
      this.setState({loading: false});
      if (response.data !== 'error') {
        this.setState({success: true});
      }
    });
  }
  render () {
    const {loading, success, passwordRules, submitLoading, hasError} = this.state;
    const {errors, header, close} = this.props;
    
    if (success) {
      return (
        <div className={'change-password-content ' + loading}>
          <h2>{i18n('loyaltysignin_0053')}</h2>
          <hr />
          <p>{i18n('loyaltysignin_0054')}</p>

          <div className="modal-actions">
            <div onClick={close} className="btn ok">{i18n('dnr_0004')}</div>
          </div>
        </div>
      );
    } else if (loading) {
      return (
        <div className={'change-password-content ' + loading}>
          <div className="transition"/>
        </div>
      );
    } else {
      return (
        <div className={'change-password-content ' + loading}>
          <Error 
            type="GLOBAL"
            errors={errors}
          />

          <h2>{header}</h2>
          <hr />
          <p>{i18n('loyaltysignin_0043')}</p>

          <form>
            <div className="field-container change-password">
              <label htmlFor="password">{i18n('loyaltysignin_0051')}</label>
              <input onChange={this._handleInputChange.bind(this, 'changePassword')}
                    id="changePassword"
                    type="password"
                    ref={c => this.changePassword = c}/>
              <PasswordErrorList errors={passwordRules}/>
            </div>
            <div className="field-container change-password">
              <label htmlFor="passwordConfirm">{i18n('loyaltysignin_0052')}</label>
              <input onChange={this._handleInputChange.bind(this, 'changePasswordConfirm')}
                    id="changePasswordConfirm"
                    type="password"
                    ref={c => this.changePasswordConfirm = c}/>
              <ul className="error-list">
                <li className={this._generateErrorClass('passwordMatch')}><span
                  className={'icon ' + this._generateErrorIcon('passwordMatch')}/>{i18n('loyaltysignin_0044')}
                </li>
              </ul>
            </div>
            <div className="modal-actions">
              <div className={submitLoading ? 'loading' : false}/>
              <div onClick={this._submit}
                  className={'btn ok ' + (hasError ? 'disabled' : '')}>{i18n('eplusaccount_0064')}</div>
              <span onClick={close}
                    className="btn cancel">{i18n('resflowlocations_0057')}</span>
            </div>
          </form>
        </div>
      );
    }
  }
}

ChangePasswordContent.displayName = "ChangePasswordContent";