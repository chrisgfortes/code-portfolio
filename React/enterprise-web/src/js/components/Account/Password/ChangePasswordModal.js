import GlobalModal from '../../Modal/GlobalModal';
import LoginController from '../../../controllers/LoginController';
import ChangePasswordContent from './ChangePasswordContent';
import ReservationCursors from '../../../cursors/ReservationCursors';

export default class ChangePasswordModal extends React.Component{
  constructor(props) {
    super(props);
    this._close = this._close.bind(this);
  }
  _close () {
    LoginController.setChangePasswordModal(false);
  }
  render () {
    const {changePassword, errors, header, infoText} = this.props;
    return (
      <div className="">
        {changePassword && changePassword.modal &&
          <GlobalModal 
            active={changePassword.modal}
            header={i18n('loyaltysignin_0051')}
            classLabel="change-password-modal"
            cursor={ReservationCursors.specialErrorChangePasswordModal}
            content={
              <ChangePasswordContent 
                errors={errors}
                data={changePassword.request}
                close={this._close}
                header={header}
                infoText={infoText} 
              />
            }
          />
        }
      </div>
    );
  }
}

ChangePasswordModal.displayName = "ChangePasswordModal";
