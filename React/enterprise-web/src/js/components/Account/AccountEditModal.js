import classNames from 'classnames';
import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import ReservationCursors from '../../cursors/ReservationCursors';
import EditContact from './EditContact';
import EditDriver from './EditDriver';
import EditPassword from './EditPassword';
import Privacy from '../Enroll/Privacy';
import RemovePayment from './RemovePayment';
import AddPayments from './AddPayments';
import EditCardOrBilling from './EditCardOrBilling';
import PreferredPayment from './PreferredPayment';
import DebitCard from './DebitCard';
import ProfileController from '../../controllers/ProfileController';
import DomManager from '../../modules/outerDomManager';
import AccountController from '../../controllers/AccountController';

function _handleBlur (event) {
  let modalContent = $('body').find('.modal-content');

  if (!$.contains(modalContent[0], event.target) && modalContent[0] !== event.target) {
    ProfileController.setAccountEditModal(null);
  }
}

function _handleClose(component) {
  AccountController.cleanEditModalErrors(component);
  ProfileController.setAccountEditModal(null);
}

function _getMsgRemoveModal(paymentType){
  const removeMap = [{
    'payment_type': 'BUSINESS_ACCOUNT_APPLICANT',
    'text': (i18n('eplusaccount_2013') || 'DELETE BILLING NUMBER?')
  }, {
    'payment_type': 'CREDIT_CARD',
    'text': (i18n('eplusaccount_2026') || 'DELETE CREDIT CARD?')
  }];

  const result = removeMap
    .find(function(item){
      return item.payment_type == paymentType;
    });

  return _.get(result, 'text') || '';
}

const AccountEditModal = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    account: ReservationCursors.account,
    paymentProfile: ReservationCursors.paymentProfile,
    paymentMethods: ReservationCursors.paymentMethods,
    profile: ReservationCursors.profile,
    verification: ReservationCursors.verification,
    modifyPayment: ReservationCursors.modifyPayment,
    editCardOrBIllingErrors: ReservationCursors.editCardOrBIllingErrors,
    accountContactErrors: ReservationCursors.accountContactErrors,
    accountPasswordErrors: ReservationCursors.accountPasswordErrors,
    accountDriverErrors: ReservationCursors.accountDriverErrors
  },
  render () {
    const {
      account,
      paymentProfile,
      paymentMethods,
      profile,
      verification,
      modifyPayment,
      editCardOrBIllingErrors,
      accountContactErrors,
      accountPasswordErrors,
      accountDriverErrors
    } = this.state;

    const modalClasses = classNames({
      'modal-container': true,
      'active': account.editModal
    });

    const accountMap = [{
      type: 'contact',
      component: 'accountContact',
      header: i18n('eplusaccount_0113'),
      content: (
        <EditContact
          account={account}
          close={_handleClose.bind(null, 'accountContact')}
          profile={profile}
          errors={accountContactErrors}
        />
      )
    },{
      type: 'driver',
      component: 'accountDriver',
      header: i18n('eplusaccount_0114'),
      content: (
        <EditDriver
          close={_handleClose.bind(null, 'accountDriver')}
          profile={profile}
          account={account}
          errors={accountDriverErrors}
        />
      )
    }, {
      type: 'password',
      component: 'accountPassword',
      header: i18n('eplusaccount_0115'),
      content: (
        <EditPassword
          close={_handleClose.bind(null, 'accountPassword')}
          errors={accountPasswordErrors}
        />
      )
    }, {
      type: 'privacy',
      header: i18n('eplusenrollment_0061'),
      content: (
        <Privacy
          verification={verification}
        />
      )
    }, {
      type: 'payment',
      component: 'paymentMethods',
      header: i18n('eplusaccount_0117'),
      content: (
        <EditCardOrBilling
          close={_handleClose.bind(null, 'paymentMethods')}
          triggerDelete={() => ProfileController.setAccountEditModal('remove')}
          modifyPayment={modifyPayment}
          paymentMethods={paymentMethods}
          errors={editCardOrBIllingErrors}
        />
      )
    }, {
      type: 'remove',
      header: _getMsgRemoveModal(_.get(account, 'modifyPayment.payment_type')),
      content: (
        <RemovePayment
          close={_handleClose}
          account={account}
        />
      )
    }, {
      type: 'preferred',
      component: 'paymentMethods',
      header: '',
      content: (
        <PreferredPayment
          close={_handleClose.bind(null, 'paymentMethods')}
          paymentProfile={paymentProfile}
          paymentMethods={paymentMethods}
        />
      )
    },{
      type: 'new',
      header: i18n('prepay_1012'),
      content: (
        <AddPayments
          close={_handleClose}
          profile={profile}
          paymentMethods={paymentMethods}
        />
      )
    },{
      type: 'debitCard',
      header: (i18n('prepay_0053') || 'Debit cards are not allowed '),
      content: (
        <DebitCard
          close={_handleClose}
        />
      )
    }];

    const currentModal =
      accountMap
        .find((modal) => (modal.type == account.editModal));

    DomManager.trigger('toggleInflightModal', !!currentModal);

    return currentModal ? (
      <div className={modalClasses} onClick={_handleBlur}>
        <div className="modal-content" tabIndex="-1">
          <div className="modal-header">
            {currentModal.header}
            <button onClick={_handleClose.bind(null, currentModal)} className="close-modal">☓</button>
          </div>
          <div className="modal-body cf">
            {currentModal.content}
          </div>
        </div>
      </div>
    ) : null
  }
});

module.exports = AccountEditModal;
