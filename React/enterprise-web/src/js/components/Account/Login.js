import Error from '../Errors/Error';
import ReservationFlowModelController from '../../controllers/ReservationFlowModelController';
import LoginController from '../../controllers/LoginController';
import classNames from 'classnames';
import UrlUtil from '../../utilities/util-url';
import Validator from '../../utilities/util-validator';
import ChangePasswordModal from '../Account/Password/ChangePasswordModal';
import { PARTNERS } from "../../constants";

export default class EnterpriseLogin extends React.Component{
  constructor() {
    super();
    this.state = {
      username: null,
      password: null,
      rememberMe: false,
      submitLoading: false
    }
    this.validator = new Validator(this, this.fieldMap);
    this.fieldMap = this.fieldMap.bind(this);
    this._handleInputChange = this._handleInputChange.bind(this);
    this._onRememberMe = this._onRememberMe.bind(this);
    this._login = this._login.bind(this);
    this._handleKeyUp = this._handleKeyUp.bind(this);
    this.processLogin = this.processLogin.bind(this);
  }
  componentDidMount() {
    let memberNumber = UrlUtil.getParameters().membernum;
    if(memberNumber) this.username.value = memberNumber;
  }
  componentWillUnmount () {
    ReservationFlowModelController.clearErrorsForComponent('loginWidget');
  }
  fieldMap() {
    return {
      refs: {
        username: this.username,
        password: this.password
      },
      value: {
        username: this.username.value,
        password: this.password.value
      },
      schema: {
        username: 'string',
        password: 'password'
      }
    }
  }
  _handleInputChange (attribute, event) {
    this.setState(
      {
        [attribute]: event.target.value
      }
    );
    this.validator.validate(attribute, event.target.value);
  }

  _onRememberMe (brand, event) {
    this.setState({
      [brand]: event.target.checked
    });
  }
  _login (brand, rememberMe) {
    let username = this.validator.validate('username', this.username.value),
      password = this.validator.validate('password', this.password.value);

    if (password && username && !this.state.submitLoading) {
      let params = {
        username: this.username.value,
        password: this.password.value,
        rememberMe: this.state.rememberMe,
        brand: this.props.brand
      };

      this.processLogin(params);
    }
  }
  _handleKeyUp (event) {
    if (event.keyCode === 13) {
      this._login();
    }
  }
  processLogin(params) {
    this.setState({submitLoading: true});
    LoginController.login(params).then((response) => {
      this.setState({submitLoading: false});
      if (response.data !== 'error') {
        if (this.props.brand === PARTNERS.BRAND.ENTERPRISE_PLUS) {
          LoginController.setAccountTab('settings');
        } else {
          LoginController.redirectToHome();
        }
      }
    });
  }
  render () {
    const {submitLoading, epRememberMe, rememberMe} = this.state;
    const {supportLinks, brand, errors, changePassword, changePasswordErrors} = this.props;

    let loginButtonClasses = classNames({
        'btn': true,
        'disabled': submitLoading
      }), isEP = brand === PARTNERS.BRAND.ENTERPRISE_PLUS,
      userNameLabel = isEP ?
        i18n('loyaltysignin_0003') : i18n('resflowcorporate_4003'),
      forgotURL = supportLinks ?
        supportLinks.forgot_password_url : 'https://legacy.enterprise.com/car_rental/enterprisePlusForgotPassword.do';

    return (
      <div className="enterprise-login cf">
        <Error errors={errors} type="GLOBAL"/>

        <label htmlFor="username">{userNameLabel}</label>
        <input onChange={this._handleInputChange.bind(this, 'username')}
               onKeyUp={this._handleKeyUp}
               type="text"
               name="username"
               ref={c => this.username = c}
               id="username"/>

        <label htmlFor="password">{i18n('loyaltysignin_0004')}</label>
        <input onChange={this._handleInputChange.bind(this, 'password')}
               onKeyUp={this._handleKeyUp}
               type="password"
               name="password"
               ref={c => this.password = c}
               id="password"/>

        {isEP ? <label className="remember"
                       htmlFor="remember">
          <input onChange={this._onRememberMe.bind(this, 'rememberMe')}
                 checked={epRememberMe} type="checkbox" id="remember"
                 name="remember"/>
          {i18n('loyaltysignin_0005')}
        </label> : false }


        <div className="login-actions">
          <div className={submitLoading ? 'loading' : false}/>
          {submitLoading ? false :
            <div onClick={this._login}
                 className={loginButtonClasses}>{i18n('resflowreview_0080')}</div>}
        </div>
        {isEP ? <a className="forgot" href={forgotURL}
                   target="_blank">{i18n('loyaltysignin_0006')}</a> : false}

        <ChangePasswordModal header={i18n('loyaltysignin_0050')}
                             changePassword={changePassword}
                             errors={changePasswordErrors} />
      </div>
    );
  }
}

EnterpriseLogin.displayName = "EnterpriseLogin";
