import Error from '../../components/Errors/Error';
import classNames from 'classnames';
import PaymentModelController from '../../controllers/PaymentModelController';

export default class EditCardOrBilling extends React.Component{
  constructor() {
    super();
    this.state = {
      isEditingDate: false
    }

    this.handleEditClick = this.handleEditClick.bind(this);
    this.handleMonthChange = this.handleMonthChange.bind(this);
    this.handleYearChange = this.handleYearChange.bind(this);
  }

  toggleEditingDate (e, isEditingDate) {
    e.setState({
      isEditingDate: !isEditingDate
    });
  }

  handleEditClick (isEditingDate) {
    this.toggleEditingDate(this, isEditingDate);
  }

  handleNameChange (e) {
    let value = _.get(e, 'target.value');
    PaymentModelController.setEditModalNickname(value);
  }

  handleSave (errors, close) {
    if (errors) {
      return;
    }

    PaymentModelController.setLoadingState(true);
    PaymentModelController.postPaymentUpdate(close);
  }

  handleMonthChange (modifyPayment, e) {
    let value = _.get(e, 'target.value');
    let month = parseInt(value, 10);
    let existingParts = PaymentModelController.getDateParts(modifyPayment);
    PaymentModelController.updateExpirationDate([existingParts[0], month]);
  }

  handleYearChange (modifyPayment, e) {
    let value = _.get(e, 'target.value');
    let year = parseInt(value, 10);
    let existingParts = PaymentModelController.getDateParts(modifyPayment);
    PaymentModelController.updateExpirationDate([year, existingParts[1]]);
  }

  render () {
    const {close, triggerDelete, modifyPayment, paymentMethods, errors} = this.props;
    const {isEditingDate} = this.state;

    const payment = modifyPayment;
    const isCard = payment.payment_type === _.get(enterprise, 'settings.paymentTypes.creditCard');
    let fields;

    if (isCard) {
      let expirationDate = moment(modifyPayment.expiration_date).format('MM/YY');
      let editDateSection;

      if (isEditingDate) {
        let months = moment.months().map((month, i) => {
          return (<option value={i + 1}>{month}</option>);
        });
        let currentYear = parseInt(moment().format('YYYY'), 10);
        let years = [];

        for (let i = currentYear; i < currentYear + 20; i++) {
          years.push(<option value={i}>{i}</option>);
        }

        let dateParts = PaymentModelController.getDateParts(modifyPayment);

        editDateSection = [
          <label htmlFor="selectMonth">{i18n('eplusenrollment_0025') || 'Month'}</label>,
          <select id="selectMonth" onChange={this.handleMonthChange.bind(this, modifyPayment)} value={dateParts[1]} name="month">{months}</select>,
          <label htmlFor="selectYear">{i18n('eplusenrollment_0029') || 'Year' }</label>,
          <select id="selectYear" onChange={this.handleYearChange.bind(this, modifyPayment)} value={dateParts[0]} name="year">{years}</select>
        ];
      } else {
        editDateSection = [
          <span>{expirationDate}</span>,
          <a id="changeExpirationDate" className="green" onClick={this.handleEditClick.bind(this, isEditingDate)}>{i18n('eplusaccount_2002') || 'EDIT'}</a>
        ];
      }

      fields = [
        <div key="inputTypeAndNumber" className="field-container type-and-number">
          <label htmlFor="inputTypeAndNumber">{i18n('prepay_0026') || 'Type and Number'}</label>
          <span id="inputTypeAndNumber">{payment.card_type+' '+PaymentModelController.maskCC(payment.last_four)}</span>
          <p className="explanation-text">{i18n('eplusaccount_2022') || 'Number cannot be changed.'}</p>
        </div>,
        <div key="inputExpirationDate" className="field-container expiration-date">
          <label htmlFor="changeExpirationDate">{i18n('prepay_0020') || 'Expiration Date'}</label>
          {editDateSection}
        </div>
      ];
    } else {
      fields = [
        <div key="inputBillingNumber" className="field-container type-and-number">
          <label htmlFor="inputBillingNumber">{i18n('eplusaccount_2014') || 'Billing Number'}</label>
          <input disabled type="text" id="inputBillingNumber" value={payment.billing_number}/>
          <span className="explanation-text">{i18n('eplusaccount_2012')
          || 'Billing number can only be added or edited by an account admin.'}</span>
        </div>
      ];
    }

    let buttonsOrSpinner;

    if (paymentMethods.submitting) {
      buttonsOrSpinner = <div key="loading" className="loading"></div>;
    } else {
      const saveClasses = classNames({
        btn: true,
        'btn-wide': true,
        'btn-save': true,
        disabled: errors
      });

      buttonsOrSpinner = [
        <button key="save"
                id="edit-modal-save-button"
                disabled={errors}
                className={saveClasses}
                onClick={this.handleSave.bind(null, errors, close)}>{i18n('eplusaccount_2001') || 'Change'}</button>,
        <button key="cancel"
                id="edit-modal-cancel-button"
                className="btn btn-wide btn-cancel"
                onClick={close}>{i18n('resflowlocations_0057') || 'Cancel'}</button>
      ];
    }
    let nickNameValue;
    if (!errors) {
      nickNameValue = PaymentModelController.getCardLabel(payment);
    }
    return (
      <div className="payments-wrapper edit-payments-modal">
        {/* GDCMP-1565 waiting on these strings */}
        <h2>{isCard ?
          (i18n('eplusaccount_2019') || 'Edit Credit Card') :
          (i18n('eplusaccount_2015') || 'Edit Billing Number')}</h2>
        <a id="editModalDeleteLink" className="green" onClick={triggerDelete}>DELETE</a>
        <hr className="divider" />
        <Error errors={errors} type="GLOBAL" />
        <div className="field-container">
          <label htmlFor="inputNickname">{i18n('prepay_0069') || 'Credit Card Nickname'}</label>
          <input type="text" id="inputNickname" placeholder="Nickname here" value={nickNameValue} onChange={this.handleNameChange}/>
        </div>
        {fields}
        <hr />
        <div className="modal-actions">
          {buttonsOrSpinner}
        </div>
      </div>
    );
  }
}

EditCardOrBilling.displayName = "EditCardOrBilling";
