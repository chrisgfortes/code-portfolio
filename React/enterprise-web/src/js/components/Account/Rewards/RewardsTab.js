import classNames from 'classnames';
import ComarchRedirect from '../../Comarch/ComarchRedirect';
import ManagePointsBand from './ManagePointsBand/ManagePointsBand';
import TiersBand from './TiersBand/TiersBand';
import AccountController from '../../../controllers/AccountController';

export default class RewardsTab extends React.Component{
  constructor(props){
    super(props);
    this.scrollHandler = this.scrollHandler.bind(this);
  }

  componentDidMount () {
    const rentalsToDate = $('#rentalsToDateValue');
    // If our gauges are in view trigger them,
    // otherwise add scroll listener for when they become in view
    if (this.isElementVisible(rentalsToDate)) {
      this.scrollHandler();
    } else {
      window.onscroll = () => {
        this.scrollHandler();
      };
    }
  }

  componentWillUnmount () {
    if (window.onscroll) {
      window.onscroll = null;
    }
  }

  scrollHandler () {
    const rentalsToDate = $('#rentalsToDateValue');
    const daysToDate = $('#daysToDateValue');
    const currentRentalsVal = parseInt(rentalsToDate.text(), 10);
    const currentDaysVal = parseInt(daysToDate.text(), 10);

    if (rentalsToDate.length && this.isElementVisible(rentalsToDate)) {
      if (this.rentalsToDate && currentRentalsVal === 0) {
        rentalsToDate.css('opacity', 1.0);
        rentalsToDate.css('transform', 'scale(1.5)');
        this.updateGauges('rentals', this.rentalsDashOffset);
        this.animateValue('rentalsToDateValue', 0, this.rentalsToDate, 750);
      }
      if (this.rentalDaysToDate && currentDaysVal === 0) {
        daysToDate.css('opacity', 1.0);
        daysToDate.css('transform', 'scale(1.5)');
        this.updateGauges('days', this.daysDashOffset);
        this.animateValue('daysToDateValue', 0, this.rentalDaysToDate, 750);
      }
    }
  }

  isElementVisible (elem) {
    const docViewTop = $(window).scrollTop();
    const docViewBottom = docViewTop + $(window).height();
    const elemTop = $(elem).offset().top;
    const elemBottom = elemTop + $(elem).height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
  }

  updateGauges (gauge, offset) {
    if (gauge === 'rentals') {
      $('.rentals-path').css('stroke-dashoffset', -offset);
    }
    if (gauge === 'days') {
      $('.days-path').css('stroke-dashoffset', -offset);
    }
  }

  animateValue (id, start, end, duration) {
    const range = end - start;
    const increment = end > start ? 1 : -1;
    const stepTime = Math.abs(Math.floor(duration / range));
    let obj = document.getElementById(id);
    let current = start;
    const timer = setInterval(function () {
      current += increment;
      obj.innerHTML = current;
      if (current === end) {
        $('#' + id).css('transform', 'scale(1.0)');
        clearInterval(timer);
      }
    }, stepTime);
  } 

  _onClickRedeem (event) {
    event.preventDefault();
    AccountController.goToReservationStep('book');
  }

  render () {
    const {
        magicNumber, 
        loyaltyData, 
        contentData,
        profile,
        comarchInfo
    } = this.props;
    
    const {
      points_to_date: points,
      loyalty_tier: loyaltyTier,
      rentals_to_date: rentalsToDate,
      rental_days_to_date: rentalDaysToDate,
      activity_to_next_tier: {
        loyalty_tier: nextLoyaltyTier,
        remaining_rental_count: remainingRentalCount,
        remaining_rental_days: remainingRentalDays,
        next_tier_rental_count: nextTierRentalGoal,
        next_tier_rental_days: nextTierDayGoal
      }
    } = loyaltyData;

    const rentalsToMaintain = AccountController.rewards.rentalsToMaintain(nextTierRentalGoal, rentalsToDate);
    const daysToMaintain = AccountController.rewards.daysToMaintain(nextTierDayGoal, rentalDaysToDate);

    // Calculate rentals data
    const rentalPercentageComplete = AccountController.rewards.rentalPercentageComplete(rentalsToDate, nextTierRentalGoal);
    const rentalsDashOffset = AccountController.rewards.rentalsDashOffset(magicNumber, rentalPercentageComplete);

    // Calculate days data
    const daysPercentageComplete = AccountController.rewards.daysPercentageComplete(nextTierDayGoal, rentalDaysToDate);
    const daysDashOffset = AccountController.rewards.daysDashOffset(magicNumber, daysPercentageComplete);

    const gaugeClass = classNames({
      'ent-gauge': true,
      'ent-gauge-last': true,
      'disabled': nextTierDayGoal === 0
    });
    const dividerClass = classNames({
      'divider': true,
      'disabled': nextTierDayGoal === 0
    });
    const loyaltyClass = classNames({
      'current-tier': true,
      'plus': loyaltyTier === 'Plus',
      'silver': loyaltyTier === 'Silver',
      'gold': loyaltyTier === 'Gold',
      'platinum': loyaltyTier === 'Platinum'
    });
    const nextLoyaltyClass = classNames({
      'next-tier': true,
      'plus': nextLoyaltyTier === 'Plus',
      'silver': nextLoyaltyTier === 'Silver',
      'gold': nextLoyaltyTier === 'Gold',
      'platinum': nextLoyaltyTier === 'Platinum'
    });

    this.points = points;
    this.rentalsToDate = rentalsToDate;
    this.rentalDaysToDate = rentalDaysToDate;
    this.rentalsDashOffset = rentalsDashOffset;
    this.daysDashOffset = daysDashOffset;

    let pointsText = i18n('resflowreview_0137')
      .split('#{number}')
      .map(str => (str.length ? str : <span className="points-count" key="points-count">{points}</span>));

    // TODO: Decouple Gauge into its own re-usable component

    //translation string (ts) variables
    const tsLoyaltyTier = i18n('eplusaccount_0083') ? i18n('eplusaccount_0083').split('#{level}') : ['', ''];
    const tsNextLoyaltyTier = i18n('eplusaccount_0084') ? i18n('eplusaccount_0084').replace('#{percentage}', rentalPercentageComplete).split('#{level}') : ['', ''];
    const tsRentals = i18n('eplusaccount_0074') ? i18n('eplusaccount_0074').split('#{number}') : ['', ''];
    const tsRentalDays = i18n('eplusaccount_0075') ? i18n('eplusaccount_0075').split('#{number}') : ['', ''];

    return (
      <section>
        <section className="account-rewards cf">
          <aside className="summary-panel">
            <h3> {i18n('eplusaccount_0072')} </h3>
            <ul>
              <li>
                <span className="summary-label">
                  {tsRentals[0]}
                  <span className="summary-value">{rentalsToDate}</span>
                  {tsRentals[1]}
                </span>
              </li>
              <li>
                <span className="summary-label">
                  {tsRentalDays[0]}
                  <span className="summary-value">{rentalDaysToDate}</span>
                  {tsRentalDays[1]}
                </span>
              </li>
            </ul>

            <h3> {i18n('eplusaccount_0077')} </h3>
            <ul>
              <li>
                <span className="summary-label">
                    {i18n('eplusaccount_0120')}
                  <span className="summary-value">{points}</span>
                </span>
              </li>
            </ul>

            <ComarchRedirect 
              label={i18n('eplusaccount_0080')} page="account-activity"
              showExternalIcon={true}
              profile={profile}
              comarchInfo={comarchInfo}
            />

            <a href={enterprise.aem.path + '/loyalty.html'}>{i18n('eplusaccount_0081')}</a>

            <span className="ytd">{i18n('eplusaccount_0076')}</span>
          </aside>
          <section className="rewards-panel">
            <div className="rewards-panel-inner cf">
              <p className="rewards-title">
                <span className={loyaltyClass}>
                  {tsLoyaltyTier[0]}
                  <span className="tier-name">{loyaltyTier}</span>
                  {tsLoyaltyTier[1]}
                </span>
                {loyaltyTier !== 'Platinum' &&
                  <span className={nextLoyaltyClass}>
                    {tsNextLoyaltyTier[0]}
                    <span className="">{nextLoyaltyTier}</span>
                    {tsNextLoyaltyTier[1]}
                  </span>}
              </p>
              <section className="ent-gauge">
                <div className="ent-gauge-inner">
                  <div className="ent-gauge-wrapper">
                    <h1 id="rentalsToDateValue" className={loyaltyClass}>{rentalsToDate}</h1>
                    <i className="icon icon-nav-vehicle"></i>

                    <h2>{i18n('eplusaccount_0085')}</h2>
                    <svg viewBox="0 0 518.32001 250.26666"
                         height="185"
                         width="380">
                      <g id="rental-g10" transform="matrix(1.3333333,0,0,-1.3333333,0,250.26667)">
                        <g transform="scale(0.1)" id="g12">
                          <path id="rental-path14"
                                className={loyaltyClass + ' rentals-path'}
                                style={{ fill: 'none', strokeWidth: 240, strokeLinecap: 'butt', strokeLinejoin: 'miter', strokeMiterlimit: 10, strokeDasharray: [magicNumber, magicNumber], strokeDashoffset: -rentalsDashOffset, strokeOpacity: 1}}
                                d="m 3760.24,6.25 c -4.45,367.5 -154.02,752.949 -379.37,1043.75 -335.89,433.46 -855.66,703.34 -1443.01,703.34 -286.57,0 -561.81,-66.98 -805.51,-189.78 C 866.473,1429.6 637.812,1229.91 464.852,995.758 372.348,870.531 300.117,732.531 243.785,589.172 171.359,404.852 129.203,210.02 129.203,9.58984"/>
                          <path id="path16"
                                style={{ fill: '#9fa0a0', fillOpacity: 1, fillRule: 'nonzero', stroke: 'none'}}
                                d="m 3643.28,10 c -17.27,438.551 -201.24,848.59 -518.32,1155.04 -319.06,308.37 -738.56,478.19 -1181.21,478.19 -442.67,0 -862.19,-169.82 -1181.289,-478.19 C 445.328,858.578 261.344,448.539 244.059,10 L 10.3867,10 C 30.25,508.699 239.656,974.992 600.41,1323.42 c 362.934,350.54 840.01,543.59 1343.35,543.59 503.34,0 980.38,-193.05 1343.27,-543.59 C 3647.73,975.004 3857.11,508.711 3876.97,10 l -233.69,0 z M 3887.17,5.19141 C 3868.47,508.461 3657.8,979.172 3293.98,1330.61 c -364.76,352.35 -844.28,546.4 -1350.22,546.4 -505.94,0 -985.49,-194.05 -1350.299,-546.4 C 229.586,979.164 18.8906,508.449 0.195312,5.19141 L 0,0 253.691,0 253.867,4.82031 C 269.887,442.582 452.98,852.059 769.41,1157.85 c 317.23,306.55 734.28,475.38 1174.34,475.38 440.04,0 857.07,-168.83 1174.26,-475.38 C 3434.39,852.07 3617.45,442.59 3633.47,4.82031 L 3633.64,0 l 253.71,0 -0.18,5.19141"/>
                        </g>
                      </g>
                    </svg>
                  </div>
                    <span className={nextLoyaltyClass + ' gauge-details'}>
                        { loyaltyTier === 'Platinum' ?
                          i18n('eplusaccount_0094', {number: rentalsToMaintain, level: nextLoyaltyTier}) :
                          i18n('eplusaccount_0087', {number: remainingRentalCount, level: nextLoyaltyTier})
                        }
                    </span>
                </div>
              </section>
              <section className={dividerClass}>
                {i18n('eplusaccount_0089')}
              </section>
              <section className={gaugeClass}>
                <div className="ent-gauge-inner">
                  <div className="ent-gauge-wrapper">
                    <h1 id="daysToDateValue" className={loyaltyClass}>{rentalDaysToDate}</h1>
                    <i className="icon icon-ENT-days"></i>

                    <h2>
                      {i18n('eplusaccount_0090')}
                    </h2>
                    <svg viewBox="0 0 518.32001 250.26666"
                         height="185"
                         width="380">
                      <g id="days-g10" transform="matrix(1.3333333,0,0,-1.3333333,0,250.26667)">
                        <g transform="scale(0.1)" id="g12">
                          <path id="days-path14"
                                className={loyaltyClass + ' days-path'}
                                style={{ fill: 'none', strokeWidth: 240, strokeLinecap: 'butt', strokeLinejoin: 'miter', strokeMiterlimit: 10, strokeDasharray: [magicNumber, magicNumber], strokeDashoffset: -daysDashOffset, strokeOpacity: 1}}
                                d="m 3760.24,6.25 c -4.45,367.5 -154.02,752.949 -379.37,1043.75 -335.89,433.46 -855.66,703.34 -1443.01,703.34 -286.57,0 -561.81,-66.98 -805.51,-189.78 C 866.473,1429.6 637.812,1229.91 464.852,995.758 372.348,870.531 300.117,732.531 243.785,589.172 171.359,404.852 129.203,210.02 129.203,9.58984"/>
                          <path id="path16"
                                style={{ fill: '#9fa0a0', fillOpacity: 1, fillRule: 'nonzero', stroke: 'none'}}
                                d="m 3643.28,10 c -17.27,438.551 -201.24,848.59 -518.32,1155.04 -319.06,308.37 -738.56,478.19 -1181.21,478.19 -442.67,0 -862.19,-169.82 -1181.289,-478.19 C 445.328,858.578 261.344,448.539 244.059,10 L 10.3867,10 C 30.25,508.699 239.656,974.992 600.41,1323.42 c 362.934,350.54 840.01,543.59 1343.35,543.59 503.34,0 980.38,-193.05 1343.27,-543.59 C 3647.73,975.004 3857.11,508.711 3876.97,10 l -233.69,0 z M 3887.17,5.19141 C 3868.47,508.461 3657.8,979.172 3293.98,1330.61 c -364.76,352.35 -844.28,546.4 -1350.22,546.4 -505.94,0 -985.49,-194.05 -1350.299,-546.4 C 229.586,979.164 18.8906,508.449 0.195312,5.19141 L 0,0 253.691,0 253.867,4.82031 C 269.887,442.582 452.98,852.059 769.41,1157.85 c 317.23,306.55 734.28,475.38 1174.34,475.38 440.04,0 857.07,-168.83 1174.26,-475.38 C 3434.39,852.07 3617.45,442.59 3633.47,4.82031 L 3633.64,0 l 253.71,0 -0.18,5.19141"/>
                        </g>
                      </g>
                    </svg>
                  </div>
                    <span className={nextLoyaltyClass + ' gauge-details'}>
                        <b>
                          { loyaltyTier === 'Platinum' ?
                            i18n('eplusaccount_0094', {number: daysToMaintain, level: nextLoyaltyTier}) :
                            i18n('eplusaccount_0091', {number: remainingRentalDays, level: nextLoyaltyTier})
                          }
                        </b>
                    </span>
                </div>
              </section>
            </div>
          </section>
          <div className="redeem-points-band">
            <div className="redeem-points-band-inner cf">
              <div className="redeem-inner-item eplus-logo"><img
                src="/etc/designs/ecom/dist/img/icons/png/eplus-logo.png" alt=""/></div>
              <div className="redeem-inner-item points-copy">
                {pointsText}
                <span className="points-message">{i18n('eplusaccount_0121')}</span>
              </div>
              <div className="redeem-inner-item redeem-cta">
                <button className="start btn"
                        onClick={this._onClickRedeem}
                        onKeyPress={a11yClick(this._onClickRedeem)}>
                  {i18n('eplusaccount_0039')}
                </button>
              </div>
            </div>
          </div>
        </section>

        <ManagePointsBand 
          contentData={contentData.manage_rewards_band}
          profile={profile}
          comarchInfo={comarchInfo}
        />

        <TiersBand 
          loyaltyData={loyaltyData}
          contentData={contentData.reward_tiers_band}
        />
      </section>
    );
  }
}

RewardsTab.defaultProps = {
  magicNumber: 5565, // The number of dash segments required to have a full gauge.
  loyaltyData: {},
  contentData: {}
};

RewardsTab.displayName = 'RewardsTab';
