import ManagePoints_ItemRenderer from './ManagePoints_ItemRenderer';

export default function ManagePointsBand({contentData, profile, comarchInfo}){
  const panels = 
    contentData
      .panels
      .map((ctaPanel, currentIndex) => (
        <ManagePoints_ItemRenderer 
          data={ctaPanel}
          key={currentIndex}
          profile={profile}
          comarchInfo={comarchInfo}
        />
      ));

  return (
    <section className="band manage-rewards-band full-horizontal-bleed no-padding">
      <section className="content-container">
        <img alt={contentData.alt_text} src={contentData.background_image}/>
        <section className="cta-container">
          <h1>{contentData.title}</h1>
          <section className="g g-2up">
            {panels}
          </section>
        </section>
      </section>
    </section>
  );
}

ManagePointsBand.defaultProps = {
  loyaltyData: {},
  contentData: {}
}

ManagePointsBand.displayName = 'ManagePointsBand';
