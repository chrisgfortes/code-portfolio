import ComarchRedirect from '../../../Comarch/ComarchRedirect';

export default function ManagePoints_ItemRenderer({data, profile, comarchInfo}){
  return (
    <div className="gi cf">
      <i className={"icon " + data.icon_class}></i>
      <div>
        <ComarchRedirect 
          label={data.title} 
          page={data.page}
          profile={profile}
          comarchInfo={comarchInfo}
        />
        <p>{data.content}</p>
      </div>
    </div>
  );
}

ManagePoints_ItemRenderer.defaultProps = {
  data: {}
};

ManagePoints_ItemRenderer.displayName = 'ManagePoints_ItemRenderer';