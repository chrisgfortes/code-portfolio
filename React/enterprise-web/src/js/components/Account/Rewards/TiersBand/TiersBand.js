import Tiers_ItemRenderer from './Tiers_ItemRenderer';

export default function TiersBand({contentData, loyaltyData, }){
  const panels = 
    contentData
      .tiers
      .map((tierPanel, currentIndex) => (
        <Tiers_ItemRenderer
          contentData={tierPanel}
          loyaltyData={loyaltyData}
          key={currentIndex}
        />
      ));

  return (
    <section className="band tiers-band full-horizontal-bleed">
      <h2 className="heading">{contentData.title}</h2>
      <section className="g g-4up cf">
        {panels}
      </section>
      {contentData.link_path && (
        <section className="cta-link-container">
          <a href={contentData.link_path}>{contentData.link_title.replace('>', '')}</a>
          <i className="icon icon-nav-carrot-green"></i>
        </section>
      )}
    </section>
  );
}

TiersBand.defaultProps = {
  loyaltyData: {},
  contentData: {}
}

TiersBand.displayName = 'TiersBand';