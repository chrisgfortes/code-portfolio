import classNames from  'classnames';

export default class Tiers_ItemRenderer extends React.Component{
  constructor(props){
    super(props);

    this.state = {
      isOpen: (
        props.loyaltyData.loyalty_tier && props.contentData.type
      ) ? (
        props.loyaltyData.loyalty_tier.toLowerCase() === props.contentData.type
      ) : false
    }

    this._toggleClick = this._toggleClick.bind(this);
  }

  _toggleClick (isOpen) {
    this.setState({
      isOpen: !isOpen
    });
  }

  render () {
    const {contentData, loyaltyData} = this.props;
    const {isOpen} = this.state;
    const data = contentData;
    const currentTier = loyaltyData.loyalty_tier.toLowerCase();
    const nextLoyaltyTier = loyaltyData.activity_to_next_tier.loyalty_tier.toLowerCase();
    let tierLabelText = '';
    const columnClasses = classNames({
      'gi': true,
      'open': isOpen,
      'current-tier': currentTier === data.type,
      'next-tier': nextLoyaltyTier === data.type
    });

    //SET TIER LABEL TEXT - "current/next tier"
    if (data.type === currentTier) {
      tierLabelText = i18n('eplusaccount_0105');
    }else if (data.type === nextLoyaltyTier){
      tierLabelText = i18n('eplusaccount_0106');
    }

    return (
      <section className={columnClasses} data-tier={data.type}>
        <header onClick={this._toggleClick.bind(null, isOpen)}>
          <h2>{data.title}</h2>

          <p className="tier-label tier-holder">{tierLabelText}</p>

        </header>
        <div className={'tier-banner ' + data.type}>
          <i className="icon icon-Ent-Icon-plus"></i>
        </div>
        <div className="tier-details-wrapper">
          <p className="tier-description">
            {data[data.type + 'description']}
          </p>
          <ul className="tier-features-list">
            <li>{data[data.type + 'bonusinfo']}</li>
            <li>{data[data.type + 'upgradeinfo']}</li>
          </ul>
        </div>
      </section>
    );
  }
}

Tiers_ItemRenderer.defaultProps = {
  contentData: {},
  loyaltyData: {}
}

Tiers_ItemRenderer.displayName = 'Tiers_ItemRenderer';