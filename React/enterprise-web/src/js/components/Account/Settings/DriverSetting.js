import SettingsCategoryHeader from '../SettingsCategoryHeader';

export default function DriverSetting({ profile }) {
  if (profile) {
    const { license_profile } = profile;
    const birthDate = _.get(license_profile, 'birth_date');
    const licenseExpiry = _.get(license_profile, 'license_expiry');
    const licenseIssue = _.get(license_profile, 'license_issue');
    const licenseNumber = _.get(license_profile, 'license_number');
    const licenseCountry = _.get(license_profile, 'country_name');

    return (
      <table>
        <SettingsCategoryHeader
          modal="driver"
          title={i18n('eplusenrollment_0500')}
        />
        <tbody>
          <tr>
            <td>{i18n('resflowreview_0072')}</td>
            <td>{licenseNumber}</td>
          </tr>

          <tr>
            <td>{i18n('resflowreview_0076')}</td>
            <td>{birthDate}</td>
          </tr>

          <tr>
            <td>{i18n('resflowreview_0070')}</td>
            <td>{licenseCountry}</td>
          </tr>

          {licenseIssue && (
            <tr>
              <td>{i18n('resflowreview_0720')}</td>
              <td>{licenseIssue}</td>
            </tr>
          )}

          <tr>
            <td>{i18n('resflowreview_0037')}</td>
            <td>{licenseExpiry}</td>
          </tr>
        </tbody>
      </table>
    );
  }

  return <noscript/>; // @todo - return null/false when we update to React 0.15 (https://github.com/facebook/react/pull/5884)
}

DriverSetting.displayName = "DriverSetting";
