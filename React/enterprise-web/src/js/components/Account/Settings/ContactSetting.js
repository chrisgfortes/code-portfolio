import SettingsCategoryHeader from '../SettingsCategoryHeader';

export default function ContactSetting({profile}) {
  const phoneTypes = {
    HOME: i18n('eplusenrollment_0017'),
    MOBILE: i18n('eplusenrollment_0016'),
    WORK: i18n('eplusenrollment_0018'),
    FAX: i18n('eplusenrollment_0019'),
    OTHER: i18n('eplusenrollment_0020')
  };

  const { basicProfile, contactProfile, preference, addressProfile } = profile || {};
  const subscriptionLink = _.get(profile, 'preference.email_preference.subscriber_preferences_url');
  const firstAddress = _.head(_.get(addressProfile, 'street_addresses') || '');
  const qtdPhones = _.get(contactProfile, 'phones.length');

  return (
    <table>
      <SettingsCategoryHeader
        modal="contact"
        title={i18n('eplusenrollment_0002')}
      />

      <tbody>
        <tr>
          <td>{i18n('reservationwidget_0021')}</td>
          <td>{_.get(basicProfile, 'first_name')}</td>
        </tr>

        <tr>
          <td>{i18n('reservationwidget_0022')}</td>
          <td>{_.get(basicProfile, 'last_name')}</td>
        </tr>

        <tr>
          <td>{i18n('reservationwidget_0030')}</td>
          <td>
            {_.get(contactProfile, 'email')}
            {subscriptionLink && (
              <a
                href={subscriptionLink}
                target="_blank"
                className="edit sub-link"
              >
                {i18n('emailsubscription_0001') || "Manage Subscriptions"}
              </a>
            )}
          </td>
        </tr>

        <tr>
          <td>{i18n('eplusaccount_0047')}</td>
          <td>
            {preference
              ? _.get(preference, 'email_preference.special_offers')
                ? i18n('resflowcorporate_0200')
                : i18n('resflowcorporate_0201')
              : null}
          </td>
        </tr>

        <tr>
          <td>{i18n('eplusaccount_0049')}</td>
          <td>{firstAddress}</td>
        </tr>

        <tr>
          <td>{i18n('resflowreview_0010')}</td>
          <td>
            {contactProfile ?
              _.get(contactProfile, 'phones').map((phone, index) => {
                return (
                  <div
                    key={index}
                    className={(qtdPhones > 1 ? "inline-row" : "") }
                  >
                    {phoneTypes[phone.phone_type]}: {phone.phone_number}
                  </div>
                );
              })
            : null}
          </td>
        </tr>
      </tbody>
    </table>
  );
}

ContactSetting.displayName = "ContactSetting";
