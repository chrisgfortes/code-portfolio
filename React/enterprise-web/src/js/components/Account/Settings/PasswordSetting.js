import SettingsCategoryHeader from '../SettingsCategoryHeader';

export default function PasswordSetting(){
  return (
    <table>
        <SettingsCategoryHeader 
          modal="password" 
          title={i18n('loyaltysignin_0004')}
        />
        <tbody>
        <tr>
        <td>{i18n('loyaltysignin_0004')}</td>
        <td>*********</td>
        </tr>
        </tbody>
    </table>
  );
}

PasswordSetting.displayName = "PasswordSetting";