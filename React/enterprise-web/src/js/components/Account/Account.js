import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import ReservationCursors from '../../cursors/ReservationCursors';
import AccountHeader from './AccountHeader';
import AccountTabs from './AccountTabs';
import Settings from './Settings';
import RewardsTab from './Rewards/RewardsTab';
import Reservations from './Reservations';
import Login from './Login';
import ReceiptModal from '../Receipt/ReceiptModal';
import AccountController from '../../controllers/AccountController';

function _redirectToHome (event) {
  event.preventDefault();
  AccountController.redirectToHome();
}

const Account = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  componentDidMount () {
    this.contentData = enterprise.accountDetails.accountRewardsContent;

    window.onhashchange = () => {
      AccountController.hashRouter(this.state.loggedIn);
    };

    AccountController.hashRouter(this.state.loggedIn);
  },
  cursors: {
    account: ReservationCursors.account,
    basicProfileLoyalty: ReservationCursors.basicProfileLoyalty,
    basicProfile: ReservationCursors.basicProfile,
    brand: ReservationCursors.sessionBrand,
    changePassword: ReservationCursors.changePassword,
    changePasswordErrors: ReservationCursors.changePasswordErrors,
    comarchInfo: ReservationCursors.comarchInfo,
    currentTrips: ReservationCursors.currentTrips,
    currentTripsLoadingClass: ReservationCursors.currentTripsLoadingClass,
    existingReservationsErrors: ReservationCursors.existingReservationsErrors,
    loading: ReservationCursors.existingReservationsLoading,
    loggedIn: ReservationCursors.userLoggedIn,
    loginWidgetErrors: ReservationCursors.loginWidgetErrors,
    modifyModalOpen: ReservationCursors.modifyModalOpen,
    myTripsHistory: ReservationCursors.myTrips,
    pastTrips: ReservationCursors.pastTrips,
    pastTripsLoadingClass: ReservationCursors.pastTripsLoadingClass,
    paymentsErrors: ReservationCursors.payErrors,
    profile: ReservationCursors.profile,
    receipt: ReservationCursors.receipt,
    rentalDetailsModal: ReservationCursors.rentalDetailsModal,
    reservations: ReservationCursors.existingReservationsReservations,
    reservationSearchVisible: ReservationCursors.reservationSearchVisible,
    supportLinks: ReservationCursors.supportLinks,
    upcomingTrips: ReservationCursors.upcomingTrips,
    upcomingTripsLoadingClass: ReservationCursors.upcomingTripsLoadingClass
  },
  render () {
    const {
      account,
      basicProfile,
      basicProfileLoyalty,
      brand,
      changePassword,
      changePasswordErrors,
      comarchInfo,
      currentTrips,
      currentTripsLoadingClass,
      existingReservationsErrors,
      loading,
      loggedIn,
      loginWidgetErrors,
      modifyModalOpen,
      myTripsHistory,
      pastTrips,
      pastTripsLoadingClass,
      paymentsErrors,
      profile,
      receipt,
      rentalDetailsModal,
      reservations,
      reservationSearchVisible,
      supportLinks,
      upcomingTrips,
      upcomingTripsLoadingClass
    } = this.state;
    let loyaltyData = basicProfileLoyalty;
    let tabContent = false;

    if (loggedIn) {
      switch (account.tab) {
        case 'reservation':
          tabContent =
            <Reservations
              {...{
                myTripsHistory,
                currentTrips,
                upcomingTrips,
                pastTrips,
                loading,
                currentTripsLoadingClass,
                upcomingTripsLoadingClass,
                pastTripsLoadingClass,
                modifyModalOpen,
                supportLinks,
                errors: existingReservationsErrors,
                reservations,
                reservationSearchVisible,
                rentalDetailsModal,
                profile
              }}
            />;
          break;
        case 'reward':
          tabContent =
            (
              <RewardsTab
                {...{
                  loyaltyData,
                  profile,
                  comarchInfo,
                  contentData: this.contentData
                }}
              />
            );
          break;
        case 'settings':
          tabContent =
            <Settings
                {...{
                  profile,
                  supportLinks,
                  paymentsErrors
                }}
            />;
          break;
        default :
          tabContent =
            <Settings
              {...{
                profile,
                supportLinks,
                paymentsErrors
              }}
          />;
      }
    } else {
      switch (account.tab) {
        case 'ep':
          tabContent =
            (
              <Login
                {...{
                  brand: "EP",
                  errors: loginWidgetErrors,
                  supportLinks,
                  changePassword,
                  changePasswordErrors
                }}
              />
            );
          break;
        case 'ec':
          tabContent =
            (
              <Login
                {...{
                  brand: "EC",
                  errors: loginWidgetErrors,
                  supportLinks,
                  changePassword,
                  changePasswordErrors
                }}
              />
            );
          break;
        default :
          window.location.hash = '#ep';
          tabContent =
            (
              <Login
                {...{
                  brand: "EP",
                  errors: loginWidgetErrors,
                  supportLinks,
                  changePassword,
                  changePasswordErrors
                }}
              />
            );
      }
    }

    if (loggedIn) {
      return (<section className="account-page">
        <AccountHeader
          {...{
            profile,
            basicProfile,
            basicProfileLoyalty,
            loggedIn,
            redirectToHome: _redirectToHome
          }}
        />

        <AccountTabs
          {...{
            loggedIn,
            brand: brand,
            account
          }}
        />
        {tabContent}
        <ReceiptModal
          {...{
            receipt
          }}
        />
      </section>)
    } else {
      return (
        <section className="account-page sign-in">
          <AccountHeader
            {...{
              profile,
              basicProfile,
              basicProfileLoyalty,
              loggedIn,
              redirectToHome: _redirectToHome
            }}
          />

          <AccountTabs
            {...{
              loggedIn,
              brand: brand,
              account
            }}
          />
          {tabContent}
        </section>
      )
    }
  }
});

module.exports = Account;
