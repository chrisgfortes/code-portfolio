/**
 * Module used to add Payment Types to NA Prepay via Pangui IFRAME
 * Used on AccountEditModal.js
 * @todo: Need to keep in mind this will be used on the Verification page too...
 * @todo: Add Fareoffice Support (or do we swap this for the Verification Prepay?)
 */
import PaymentModelController from '../../controllers/PaymentModelController';
import Error from '../Errors/Error';

export default class AddPayments extends React.Component{
  constructor(){
    super();
    this.state = {
      loading: false,
      targetUrl: null,
      returnUrl: null,
      key: null,
      timeoutHandle: null
    };
    this.initPrepay = this.initPrepay.bind(this);
    this.prepayCallback = this.prepayCallback.bind(this);
    this.submitForm = this.submitForm.bind(this);
    this.handleTimeout = this.handleTimeout.bind(this);
    this.cleanTimeout = this.cleanTimeout.bind(this);
    this._handleError = this._handleError.bind(this);
    this._handleLoad = this._handleLoad.bind(this);
    this.selectSavePayment = this.selectSavePayment.bind(this);
    PaymentModelController.cleanError();
  }
  componentDidMount () {
    // start out with a clean slate
    PaymentModelController.reset();
    this.initPrepay();
  }
  componentWillUnmount () {
    PaymentModelController.stopwatching();
    this.cleanTimeout(true);
  }
  initPrepay () {
    this.setState({
      loading: true,
      key: null
    });

    // reservation flow
    if (enterprise.currentView === 'reserve') {
      // @TODO solve the mystery of why PaymentServices can't require() VerificationService
      PaymentModelController.initPrepay().then((response) => {
        this.prepayCallback(response);
      });
    // profile settings page
    } else {
      PaymentModelController.initPayWatching();
      // PaymentServices.getPrepayToken() is specific to Pangui and NA Prepay
      PaymentModelController.getPrepayToken().then((response) => {
        this.prepayCallback(response);
      });
    }
  }
  prepayCallback(response) {
    if (response.card_submission_key) {
      this.setState({
        key: response.card_submission_key,
        targetUrl: response.payment_context_data.card_submission_url
      });
      this.submitForm();
    } else {
      this.props.close();
    }
  }
  submitForm () {
    // @todo Need a way to tell if PanGUI is down or not responding. Tried several things in some stashed code:
    // We don't have a lot of options. The full flow and issues herein are expanded upon in ECR-11661
    // 1. Use JS to fetch a JS file attached to the post page to see if it responds --> Issue is that the JS files have dynamic URLs
    // 2. Use JS to post to the form and see if it responds --> but without all the data it bombs
    // 3. Grab something and make it the url for a fake IMG and use the img.onload and img.onerror handlers --> only certain things work for this url
    this.pangui.submit();
    let stateHandle = window.setTimeout(this.handleTimeout, enterprise.settings.panguiTimeout);
    this.setState({
      timeoutHandle: stateHandle,
      loading: true
    });
  }
  handleTimeout () {
    // Note: similar thing is in default.js
    // let warningString = [{ defaultMessage: 'Failure to load Payment Services. Please close the modal and try again.' }];
    let warningString = [{ defaultMessage: i18n('prepay_0071') || 'Failure to load Payment Services. Please close the modal and try again.' }];
    PaymentModelController.setComponentErrors(warningString);

    this.cleanTimeout();
    this.setState({
      loading: false
    });
  }
  cleanTimeout (tf = false) {
    // if tf == true we have a 'clean' scenario, though not doing much at this point
    window.clearTimeout(this.state.timeoutHandle);
  }
  // initial load event for the iframe, turn off loading indicator
  _handleLoad () {
    this.cleanTimeout(true);
    this.setState({
      loading: false,
      timeoutHandle: null
    });
  }
  /**
   * I don't think the iframe.onerror event _ever_ fires for anything anymore
   * in today's browsers
   */
  _handleError () {
    this.cleanTimeout();
    console.error('ERROR ON IFRAME');
  }
  selectSavePayment (e) {
    PaymentModelController.setSavePaymentForLater(e.target.value);
  }
  render () {
    const {paymentMethods, profile, saveForLater} = this.props;
    const {key, targetUrl, loading} = this.state;

    let appSecToken = key;
    let panguiUrl = targetUrl;
    let originUrl = PaymentModelController.resolveUrl();

    // let returnUrl = this.state.returnUrl; // @todo: handle this externally
    let returnUrl = originUrl + enterprise.aem.path + '/userprofile/_jcr_content.post.html';
    // @todo: where do we put the CSS w/in CMS; coming soon!
    // NOTE: For local dev, this file will be blocked due to lack of SSL
    // I've been re-mapping it with Charles Web Proxy
    let cssUrl = originUrl + '/etc/designs/ecom/dist/css/static.css';
    //let cssUrl = 'https://enterprise-xqa1-aem.enterprise.com/etc/designs/ecom/dist/css/static.css';

    //IF MODIFY: REPOPULATE THE INFORMATION FROM MODIFY CURSOR, OTHERWISE START OUT FRESH
    //IF MODIFY: WE NEED TO REMOVE CARD FIRST, THEN SUBMIT NEW

    return (
      <div>
        { loading ?
          <div className="transition"/> : <h2>{i18n('prepay_1012') || 'Add Payment Method'}</h2> }

        { paymentMethods.submitting ?
          <div className="loading"/> : false}

        <form name="initiateForm"
              ref={c => this.pangui = c}
              action={panguiUrl} method="post"
              target="targetIframe">

          {/*Several translations, including ones not from eplusaccount_ set need updateing */}

          <input type="hidden" name="pymtMSDTO.labelNameHidden" value={i18n('prepay_0065') || 'Name on Card'}/>
          <input type="hidden" name="pymtMSDTO.labelCardNumberHidden" value={i18n('resflowreview_0036') || 'Credit Card Number'}/>
          {/* this "swipe card" thing is never displayed, I think it's an internal thing ... */}
          <input type="hidden" name="pymtMSDTO.labelSwipeCardHidden" value={i18n('prepay_0064') || 'Swipe Card'} />
          <input type="hidden" name="pymtMSDTO.labelExpMonthHidden" value={i18n('prepay_0037') || 'Expiration Month'}/>
          <input type="hidden" name="pymtMSDTO.labelExpYearHidden" value={i18n('prepay_0038') || 'Expiration Year'}/>
          <input type="hidden" name="pymtMSDTO.labelCardSecCodeHidden" value={i18n('prepay_0039') || 'Card Security Code'}/>

          <input type="hidden" name="pymtMSDTO.labelSubmitButtonHidden" value={i18n('prepay_0040') || 'Add Credit Card'}/>
          <input type="hidden" name="pymtMSDTO.labelWhatsThisHidden" value={i18n('prepay_0066') || 'What\'s This?'} />

          <input
            type="hidden"
            name="pymtMSDTO.cidTooltipHidden"
            value={i18n('prepay_0063') || 'The security code is located on the back of MasterCard, Visa and Discover credit or debit cards and is typically a separate group of 3 digits to the right of the signature strip. On American Express cards, the Card security code is a printed, not embossed, group of four digits on the front towards the right.'}
          />

          <input type="hidden" name="pymtMSDTO.labelBackButtonHidden" value={i18n('prepay_0067') || 'Back'}/>
          <input type="hidden" name="pymtMSDTO.cardHolderName"
                 value={PaymentModelController.resolveName(profile)}/>
          <input type="hidden" name="pymtMSDTO.requiredCIDHidden" value="true"/>
          <input type="hidden" name="pymtMSDTO.defaultInputScreen" value="mediaInput"/>
          <input type="hidden" name="pymtMSDTO.cntryIsoCdeHidden" value={PaymentModelController.resolveLang(true)}/>
          <input type="hidden" name="pymtMSDTO.langIsoCdeHidden" value={PaymentModelController.resolveLang()}/>
          <input type="hidden" name="pymtMSDTO.toggleAllowedHidden" value="false"/>
          <input type="hidden" name="pymtMSDTO.showBackButtonHidden" value="false"/>

          <input type="hidden" name="pymtMSDTO.transUserIdHidden" value="WNGHJSLight"/>
          <input type="hidden" name="pymtMSDTO.clientNameHidden" value="Enterprise"/>
          <input type="hidden" name="pymtMSDTO.srceSysCdeHidden" value="10"/>

          <input type="hidden" name="pymtMSDTO.cssSourceHidden" value={cssUrl}/>
          <input type="hidden" name="pymtMSDTO.successUrlHidden"
                 value={returnUrl}/>
          <input type="hidden" name="pymtMSDTO.failureUrlHidden"
                 value={returnUrl}/>

          <input type="hidden" name="pymtMSDTO.editLinkUrlHidden" value={returnUrl}/>
          <input type="hidden" name="pymtMSDTO.backUrlHidden" value={returnUrl}/>

          <input type="hidden" name="pymtMSDTO.showEditLinkHidden" value="no-edit"/>
          <input type="hidden" name="pymtMSDTO.validateNameHidden" value="No"/>

          <input type="hidden" name="pymtMSDTO.initialAction"
                 value={panguiUrl}/>

          <input type="hidden" name="pymtMSDTO.appSecTokHidden"
                 value={appSecToken}/>
          {/* not sure if we need to change this clientSesId or not */}
          <input type="hidden" name="clientSesId" value="1431093335371"/>
          {/* this must be "self" since we are doing this in an iframe */}
          <input type="hidden" name="targetHidden" value="self"/>
        </form>
        
        {
          (loading || paymentMethods.submitting || paymentMethods.errors) ? null : 
          (saveForLater && profile && (
            <label className="save-payment">
              <input id="savePayment" name="savePayment" type="checkbox" value="false" onChange={this.selectSavePayment} />
              <i className="icon icon-forms-checkmark-green"/>
              <span> {i18n('prepay_0035') || 'Save this card to use later.'} </span>
            </label>
          ))
        }

        <Error errors={paymentMethods.errors} type="GLOBAL" />

        {paymentMethods.errors ? null :
          <iframe ref="payFrame" className="payment-iframe" id="fare" name="targetIframe"
                  onLoad={this._handleLoad} onError={this._handleError}
                  sandbox="allow-forms allow-scripts allow-same-origin" /> }
      </div>
    );
  }
}

AddPayments.displayName = "AddPayments";
