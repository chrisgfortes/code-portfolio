export default function Header({profile, basicProfile, basicProfileLoyalty, loggedIn, redirectToHome}){
  return loggedIn ? (
    <header>
      <div className="overview">
        <h1>{i18n('eplusaccount_0026')}</h1>
        <span className="prominent">{basicProfile ? _.get(basicProfile, 'first_name') + ' ' + _.get(basicProfile, 'last_name') : null}</span>
        <span className="member-number">{basicProfileLoyalty ? i18n('eplusaccount_0205', {number: _.get(basicProfileLoyalty, 'loyalty_number')}) : null} </span>
        <span className="prominent points">
          {basicProfileLoyalty ? _.get(basicProfileLoyalty, 'points_to_date') : null}
          <small>
            {i18n('eplusaccount_0031', {
              balance: ' ',
              date: moment().format(enterprise.i18nUnits.dateformat)
            })}
          </small>
        </span>
        {profile && profile.corporate_account ?
          <p className="prominent">
            {i18n('eplusaccount_0037') + ' ' + i18n('eplusaccount_0038', {
              CorporateName: _.get(profile, 'corporate_account.account_name'),
              AccountNumber: _.get(profile, 'corporate_account.corporate_id')}
              )
            }
          </p>
          : null
        }
      </div>

      <div className="cta-container">
        <button
          className="start btn"
          onClick={redirectToHome.bind(this)}
          onKeyPress={a11yClick(redirectToHome.bind(this))}
        >
          {i18n('eplusaccount_0039')}
        </button>

        <a
          href="#"
          className="cta-text-link"
          onClick={redirectToHome.bind(this)}
          onKeyPress={a11yClick(redirectToHome.bind(this))}
        >
          {i18n('eplusaccount_0040')}
        </a>
      </div>
    </header>
  ) : (
    <header>
      <div className="overview">
        <h1>{i18n('resflowreview_0080')}</h1>
        <div className="banner">{i18n('loyaltysignin_0000')}</div>
      </div>
    </header>
  );
}
