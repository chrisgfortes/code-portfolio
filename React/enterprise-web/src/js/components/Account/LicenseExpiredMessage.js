import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import ReservationCursors from '../../cursors/ReservationCursors';
import PRICING from '../../constants/pricing';

const LicenseExpiredMessage = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    profile: ReservationCursors.profile,
    prematureExpiration: ReservationCursors.prematureExpiration
  },
  render(){
    const {profile, prematureExpiration} = this.state;
    // ECR-14250
    const paymentMethods = _.get(profile, 'payment_profile.payment_methods') || [];
    const creditCards = paymentMethods.filter(pm => pm.payment_type === PRICING.CREDIT);
    const isExpiring = creditCards.some(card => {
      const expiringDate = moment(card.expiration_date);
      const now = moment();
      return now.diff(expiringDate, 'months') === 0;
    });

    const expired = (profile ? prematureExpiration : false);

    return isExpiring || expired ?
      (<div className="alert-message-wrapper">
          <div className="icon-wrapper">
            <i className="icon icon-alert-caution-yellow"/>
          </div>
          <div className="alert-message">
            <div>
              <ul>
                <li>{expired &&
                  i18n('resflowreview_0027')
                }</li>
                <li>{isExpiring &&
                    (i18n('resflowreview_0162') || 'Your credit card will expire soon.') + ' ' +
                    (i18n('resflowreview_0163') || 'Please visit your profile to update your card.') + ' '
                  }
                  <a href={enterprise.aem.path + '/account.html#settings'}>{i18n('eplusaccount_0034')}</a>
                </li>
              </ul>
            </div>
          </div>
        </div>)
    : <div id="warning-messages"></div>;
  }
});

module.exports = LicenseExpiredMessage;
