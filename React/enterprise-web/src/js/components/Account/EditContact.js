import Validator from '../../utilities/util-validator';
import AccountController from '../../controllers/AccountController';
import EnrollmentController from '../../controllers/EnrollmentController';
import Error from '../Errors/Error';
import classNames from 'classnames';
import ResetFieldButton from "../ResetFieldButton";
import { PROFILE } from '../../constants';

export default class EditContact extends React.Component{
  constructor(props) {
    super(props);

    this.state = {
      specialOffers: null,
      phoneNumber: null,
      phoneNumberType: null,
      alternativePhoneNumber: null,
      countryData: null,
      countryResidence: null,
      fieldErrors: []
    };

    this.validator = new Validator(this, this.fieldMap);
    this._onSave = this._onSave.bind(this);
    this._setStateToField = this._setStateToField.bind(this);
    this._handleInputBlur = this._handleInputBlur.bind(this);
    this._handleInputFocus = this._handleInputFocus.bind(this);
    this._handleInputChange = this._handleInputChange.bind(this);
    this._onCountryChange = this._onCountryChange.bind(this);
    this._cleanStateToSend = this._cleanStateToSend.bind(this);

    if (props.profile) {
      const {
        addressProfile,
        contactProfile,
        preference
      } = props.profile;

      const countryCode = this.getCountryCode();
      EnrollmentController.getSubdivisions(countryCode);

      this.state = Object.assign(this.state, {
        city: {
          prevValue: null,
          newValue: (_.get(addressProfile, 'city') || null)
        },
        email: {
          prevValue: null,
          newValue: (_.get(contactProfile, 'email') || null)
        },
        address: {
          prevValue: null,
          newValue: (_.get(addressProfile, 'street_addresses[0]') || null)
        },
        addressTwo: {
          prevValue: null,
          newValue: (_.get(addressProfile, 'street_addresses[1]') || null)
        },
        phoneNumberType: {
          prevValue: null,
          newValue: (_.get(contactProfile, 'phones[0].phone_type') || PROFILE.PHONE_TYPES.HOME)
        },
        phoneNumber: {
          prevValue: null,
          newValue: (_.get(contactProfile, 'phones[0].phone_number') || null)
        },
        alternativePhoneNumberType: {
          prevValue: null,
          newValue: (_.get(contactProfile, 'phones[1].phone_type') || PROFILE.PHONE_TYPES.MOBILE)
        },
        alternativePhoneNumber: {
          prevValue: null,
          newValue: (_.get(contactProfile, 'phones[1].phone_number') || null)
        },
        countryResidence: {
          prevValue: null,
          newValue: this.getCountryCode()
        },
        postal: {
          prevValue: null,
          newValue: (_.get(addressProfile, 'postal') || null)
        },
        specialOffers: {
          prevValue: null,
          newValue: (_.get(preference, 'email_preference.special_offers') || false)
        },
        regionResidence: {
          prevValue: null,
          newValue: (_.get(addressProfile, 'country_subdivision_name') || null)
        }
      })
    }
  }

  fieldMap() {
    const {
      phoneNumber,
      phoneNumberType,
      alternativePhoneNumber,
      alternativePhoneNumberType,
      postal,
      address,
      city,
      regionResidence
    } = this.state;

    let map = {
      refs: {
        phoneNumber: this.phoneNumber,
        phoneNumberType: this.phoneNumberType,
        alternativePhoneNumber: this.alternativePhoneNumber,
        alternativePhoneNumberType: this.alternativePhoneNumberType,
        postal: this.postal,
        address: this.address,
        city: this.city,
        regionResidence: this.regionResidence
      },
      value: {
        phoneNumber: phoneNumber.newValue,
        phoneNumberType: phoneNumberType.newValue,
        alternativePhoneNumber: alternativePhoneNumber.newValue,
        alternativePhoneNumberType: alternativePhoneNumberType.newValue,
        postal: postal.newValue,
        address: address.newValue,
        city: city.newValue,
        regionResidence: regionResidence.newValue
      },
      schema: {
        phoneNumber: 'phoneMask',
        phoneNumberType: 'string',
        alternativePhoneNumber: '?phoneMask',
        alternativePhoneNumberType: '?string',
        postal: 'string',
        address: 'string',
        city: 'string',
        regionResidence: 'string'
      }
    };

    return map;
  }

  getCountryCode(){
    const { profile } = this.props;
    return _.get(profile, 'addressProfile.country_code') || enterprise.countryCode || null;
  }

  _cleanStateToSend(dataState) {
    let data = Object.assign({}, dataState);
    _.forIn(data, function (value, key) {
      let fieldValue = _.get(value, 'newValue');
      if (!fieldValue) {
        delete data[key];
      }else if (fieldValue === 'on'){
        data[key] = true;
      } else if (fieldValue === 'off'){
        data[key] = false;
      }else{
        data[key] = fieldValue;
      }
    });

    return data;
  }

  _isEmpty(event, fieldName) {
    const value = _.get(event, 'target.value') || _.get(event, 'value') || '';

    if (!value) {
      this.setState({
        fieldErrors: [fieldName]
      })
    }else{
      this.setState({
        fieldErrors: []
      });
    }
  }

  _onSave() {
    const { profile } = this.props;
    const { submitLoading } = this.state;
    const validate = this.validator.validateAll();

    this.setState({
      fieldErrors: validate.errors
    });

    if (validate.valid && !validate.errors.length && profile && !submitLoading) {
      this.setState({submitLoading: true});
      let formData = this._cleanStateToSend(this.state);
      // formData.profile = profile;
      // ECR-14250 do something better
      formData.account = _.get(profile, 'basicProfile.loyaltyData.loyalty_number');

      AccountController
        .updateContact(formData)
        .then((submitLoading, hasErrors) => {
          this.setState({ submitLoading: submitLoading });
          if (!hasErrors) AccountController.setAccountEditModal(false);
        })
        .catch(() => this.setState({ submitLoading: false }))
    }
  }

  _setStateToField(attr, prevValue, newValue){
    this.setState({
      [attr]: {
        prevValue: prevValue,
        newValue: newValue
      }
    });
  }

  _handleInputBlur (attr, event) {
    const fieldValue = event.target.value;
    const newValue = this.state[attr].prevValue;
    this._setStateToField(attr, fieldValue, newValue);
  }

  _handleInputFocus (attr, event) {
    const fieldValue = event.target.value;
    this._setStateToField(attr, fieldValue, '');
  }

  _handleInputChange (attr, event) {
    const fieldValue = event.target.value;

    if (attr == 'regionResidence') {
      const hasSubdivision = _.get(this, 'regionResidence.region');
      this._isEmpty(hasSubdivision, 'regionResidence');
    }

    this._setStateToField(attr, fieldValue, fieldValue);
    this.validator.validate(attr, event.target.value);
  }

  _togglePrivacy (event) {
    event.preventDefault();
    AccountController.setAccountEditModal('privacy');
  }

  _onCountryChange(event) {
    const { countryResidence } = this.state;
    const countrySelected = _.get(event, 'target.value') || '';

    EnrollmentController
      .getSubdivisions(countrySelected)
      .then(() => {
        this.setState({
          countryResidence: {
            prevValue: countryResidence.newValue,
            newValue: countrySelected
          },
          regionResidence: {
            prevValue: null,
            newValue: null
          }
        });

        const hasSubdivision = _.get(this, 'regionResidence.field');
        if (hasSubdivision){
          this._isEmpty(hasSubdivision, 'regionResidence');
        }
      });
  }

  renderPhoneTypes() {
    const phonesTypes = {
      [PROFILE.PHONE_TYPES.HOME]: i18n('eplusenrollment_0017'),
      [PROFILE.PHONE_TYPES.WORK]: i18n('eplusenrollment_0018'),
      [PROFILE.PHONE_TYPES.MOBILE]: i18n('eplusenrollment_0016'),
      [PROFILE.PHONE_TYPES.OTHER]: i18n('eplusenrollment_0020')
    }

    return Object.keys(phonesTypes).map((key) => {
      return (
        <option key={key} value={key}>{phonesTypes[key]}</option>
      );
    });
  }

  render () {
    const {
      profile,
      account,
      errors,
      close
    } = this.props;

    const {
      alternativePhoneNumberType,
      alternativePhoneNumber,
      countryResidence,
      phoneNumberType,
      regionResidence,
      specialOffers,
      submitLoading,
      phoneNumber,
      addressTwo,
      address,
      postal,
      email,
      city
    } = this.state;

    const {
      basicProfile
    } = (profile || {});

    const enableSubdivision = !!_.get(account, 'subdivisions.length');
    const subdivisionLabel = EnrollmentController.getSubdivisionLabel(countryResidence.newValue);

    const saveButtonClasses = classNames({
      'btn': true,
      'save': true,
      'disabled': submitLoading
    });

    return (
      <form className="personal-entry-form contact" ref="form">
        <h2>{i18n('eplusenrollment_0002')}</h2>
        <abbr title="Required" className="required-label">
          <i>{i18n('resflowreview_0004')}</i>
        </abbr>

        <hr className="divider"/>

        <Error errors={errors} type="GLOBAL"/>

        <div className="field-container first-name">
          <label htmlFor="first-name">{i18n('reservationwidget_0021')}</label>
          <input
            id="first-name"
            type="text"
            placeholder={basicProfile.first_name || null}
            disabled
          />
        </div>

        <div className="field-container last-name">
          <label htmlFor="last-name">{i18n('reservationwidget_0022')}</label>
          <input
            id="last-name"
            type="text"
            placeholder={basicProfile.last_name || null}
            disabled
          />
        </div>

        <div className="disclaimer">{i18n('eplusaccount_0061')}</div>

        <div className="field-container">
          <label htmlFor="emailAddress">{i18n('reservationwidget_0030')}</label>
          <ResetFieldButton>
            <input
              ref={r => this.email = r}
              onChange={this._handleInputChange.bind(this, 'email')}
              onFocus={this._handleInputFocus.bind(this, 'email')}
              onBlur={this._handleInputBlur.bind(this, 'email')}
              id="email" type="email" name="email"
              value={email.newValue} required
            />
          </ResetFieldButton>
        </div>

        <div className="field-container">
          <label htmlFor="specialOffers">
            <input
              onChange={this._handleInputChange.bind(this, 'specialOffers')}
              id="specialOffers"
              name="specialOffers"
              type="checkbox"
              checked={specialOffers.newValue}
            />
            {i18n('resflowreview_0005')}
          </label>

          <p>{i18n('resflowreview_0006')}
            &nbsp;
            <a className="toggle-privacy" href="#" onClick={this._togglePrivacy}>
              {i18n('resflowreview_0008')}
            </a>
          </p>
        </div>

        <hr className="divider" />

        <div className="field-container">
          <label htmlFor="address">{i18n('resflowreview_0063')}</label>
          <ResetFieldButton>
            <input
              ref={r => this.address = r}
              onChange={this._handleInputChange.bind(this, 'address')}
              onFocus={this._handleInputFocus.bind(this, 'address')}
              onBlur={this._handleInputBlur.bind(this, 'address')}
              id="address" type="text" name="address"
              value={address.newValue}
            />
          </ResetFieldButton>
        </div>

        <div className="field-container">
          <label htmlFor="addressTwo">{i18n('resflowreview_0064')}</label>
          <ResetFieldButton>
            <input
              ref={r => this.addressTwo = r}
              onChange={this._handleInputChange.bind(this, 'addressTwo')}
              onFocus={this._handleInputFocus.bind(this, 'addressTwo')}
              onBlur={this._handleInputBlur.bind(this, 'addressTwo')}
              id="addressTwo" name="addressTwo" type="text"
              value={addressTwo.newValue}
            />
          </ResetFieldButton>
        </div>

        <div className="field-container city">
          <label htmlFor="city">{i18n('resflowreview_0065')}</label>
          <ResetFieldButton>
            <input
              ref={r => this.city = r}
              onChange={this._handleInputChange.bind(this, 'city')}
              onFocus={this._handleInputFocus.bind(this, 'city')}
              onBlur={this._handleInputBlur.bind(this, 'city')}
              id="city" name="city" required type="text"
              value={city.newValue} aria-required="true"
            />
          </ResetFieldButton>
        </div>

        <div className="field-container country">
          <label htmlFor="country">{i18n('resflowreview_0062')}</label>
          {account.countries.length ?
            <select
              ref="countryResidence"
              className="styled"
              onChange={this._onCountryChange}
              id="find-country"
              value={countryResidence.newValue}
              aria-required="true"
              aria-invalid="false"
            >
              {account.countries.map(function (country, index) {
                return (
                  <option
                    key={index}
                    data-index={index}
                    value={country.country_code}>{country.country_name}
                  </option>
                );
              })}
            </select> : <div className="loading" />
          }
        </div>

        {enableSubdivision && (
          <div className="field-container region">
            <label htmlFor="region">{subdivisionLabel}</label>
            <select
              className="styled"
              onChange={this._handleInputChange.bind(this, 'regionResidence')}
              onBlur={this._handleInputBlur.bind(this, 'regionResidence')}
              onFocus={this._handleInputFocus.bind(this, 'regionResidence')}
              id="regionResidence"
              ref={r => this.regionResidence = r}
              value={regionResidence.newValue}
            >

              <option key="0"
                value={enterprise.utilities.isMaskedField(regionResidence.newValue) ? regionResidence.newValue : i18n('resflowreview_0152')}>
                {enterprise.utilities.isMaskedField(regionResidence.newValue) ? regionResidence.newValue : i18n('resflowreview_0152')}
              </option>

              {
                account.subdivisions.map(function (region, index) {
                  return (<option key={index}
                    value={region.country_subdivision_code}>{region.country_subdivision_name}</option>);
                })
              }
            </select>
          </div>
        )}

        <div className="field-container postal">
          <label htmlFor="postal">{i18n('resflowreview_0068')}</label>
          <ResetFieldButton>
            <input
              id="postal" type="text"
              name="postal" ref={r => this.postal = r}
              onChange={this._handleInputChange.bind(this, 'postal')}
              onFocus={this._handleInputFocus.bind(this, 'postal')}
              onBlur={this._handleInputBlur.bind(this, 'postal')}
              value={postal.newValue}
            />
          </ResetFieldButton>
        </div>

        <hr className="divider" />

        <div className="field-container phone">
          <label htmlFor="phoneNumber">{i18n('eplusaccount_0050')}</label>
          <select
            onChange={this._handleInputChange.bind(this, 'phoneNumberType')}
            value={phoneNumberType.newValue}
            ref={r => this.phoneNumberType = r}
            className="styled"
          >
            {this.renderPhoneTypes()}
          </select>

          <ResetFieldButton>
            <input
              ref={r => this.phoneNumber = r}
              onChange={this._handleInputChange.bind(this, 'phoneNumber')}
              onFocus={this._handleInputFocus.bind(this, 'phoneNumber')}
              onBlur={this._handleInputBlur.bind(this, 'phoneNumber')}
              id="phoneNumber" name="phoneNumber" type="string" pattern="[0-9]*"
              className="phoneNumber" value={phoneNumber.newValue}
            />
          </ResetFieldButton>
        </div>

        <div className="field-container phone">
          <select
            ref={r => this.alternativePhoneNumberType = r}
            onChange={this._handleInputChange.bind(this, 'alternativePhoneNumberType')}
            value={alternativePhoneNumberType.newValue}
            className="styled"
          >
            {this.renderPhoneTypes()}
          </select>

          <ResetFieldButton>
            <input
              ref={r => this.alternativePhoneNumber = r}
              onChange={this._handleInputChange.bind(this, 'alternativePhoneNumber')}
              onFocus={this._handleInputFocus.bind(this, 'alternativePhoneNumber')}
              onBlur={this._handleInputBlur.bind(this, 'alternativePhoneNumber')}
              id="alternativePhoneNumber"
              type="string" name="alternativePhoneNumber"
              className="alternativePhoneNumber"
              value={alternativePhoneNumber.newValue}
              pattern="[0-9]*"
            />
          </ResetFieldButton>
        </div>

        <div className="modal-actions">
          {submitLoading && (
            <div className="loading" />
          )}

          <div
            onClick={this._onSave}
            className={saveButtonClasses}>{i18n('eplusaccount_0064')}
          </div>
          <span
            onClick={close}
            className="btn cancel">{i18n('eplusaccount_0065')}
          </span>
        </div>
      </form>
    );
  }
}

EditContact.displayName = "EditContact";
