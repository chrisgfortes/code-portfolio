import PaymentModelController from '../../controllers/PaymentModelController';
import ProfileController from '../../controllers/ProfileController';
import {PRICING} from '../../constants/';

export default class PreferredPayment extends React.Component{
  constructor() {
    super();
  }

  handleSave () {
    PaymentModelController.setLoadingState(true);
    PaymentModelController.postPaymentUpdate();
  }

  handleSelect (paymentProfile, e) {
    const value = _.get(e, 'target.value');
    let selectedPayment = null;
    _.get(paymentProfile, 'payment_methods')
      .some((payment) => {
        if (payment.payment_reference_id === value) {
          // for some terrifying reason setting payment.preferred = true updates the existing state tree...
          selectedPayment = _.clone(payment);
          selectedPayment.preferred = true;
          return true;
        }
      });

    ProfileController.setModifyPayment(selectedPayment);
  }

  renderRadios (paymentMethodsProfile, paymentProfile, paymentMethods) {
    return paymentMethodsProfile.map((payment) => {
      const id = 'radio_' + payment.payment_reference_id;
      const isDisabled = paymentMethods.submitting;

      return (
        <label key={payment.payment_reference_id}
               htmlFor={'radio_' + payment.payment_reference_id}
               className="enterprise-control control-radio">
          <input id={id}
                 disabled={isDisabled}
                 value={payment.payment_reference_id}
                 name="preferredPayment"
                 type="radio"
                 className="enterprise-radio"
                 onClick={this.handleSelect.bind(this, paymentProfile)} />
          <div className="control-indicator"/>
          <span className="payment-alias">{PaymentModelController.getCardLabel(payment, paymentProfile)}</span>
          <span>{payment.payment_type === PRICING.CREDIT ? PaymentModelController.maskCC(payment.last_four) : PaymentModelController.maskCC(payment.billing_number, true)}</span>
        </label>
      );
    });
  }

  render () {
    const {paymentProfile, paymentMethods, close} = this.props;
    const radios = this.renderRadios(_.get(paymentProfile, 'payment_methods'), paymentProfile, paymentMethods);
    const buttonsOrSpinner =
      paymentMethods.submitting ?
        <div key="loading" className="loading"/> :
        [
          <button key="save"
                  id="edit-modal-save-button"
                  className="btn btn-wide btn-save"
                  onClick={this.handleSave.bind(this)}>{i18n('eplusaccount_2001') || 'Change'}</button>,
          <button key="cancel"
                  id="edit-modal-cancel-button"
                  className="btn btn-wide btn-cancel"
                  onClick={close}>{i18n('resflowlocations_0057') || 'Cancel'}</button>
        ];

    return (
      <div className="payments-wrapper preferred-payment-modal">
        <h2>{i18n('eplusaccount_2023') || 'Select Preferred Payment Method'}</h2>
        <hr className="divider"/>
        <radiogroup ref="group">
          {radios}
        </radiogroup>
        <hr />
        <div className="modal-actions">
          {buttonsOrSpinner}
        </div>
      </div>
    );
  }
}

PreferredPayment.displayName = "PreferredPayment";
