import KeyRentalFactsService from '../../services/KeyRentalFactsService';
import ReservationActions from '../../actions/ReservationActions';
import ResponsiveUtil from '../../utilities/util-responsive';
import ReservationCursors from '../../cursors/ReservationCursors';
import GlobalModal from '../Modal/GlobalModal';
import classNames from 'classnames';
import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import { GLOBAL } from '../../constants';

const PolicyLink = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  getDefaultProps () {
    return {
      type: 'pickup'
    };
  },
  componentWillMount () {
    const {rentalTerms} = this.state;
    if (!rentalTerms || !rentalTerms.code) {
      KeyRentalFactsService.getTermsAndConditions();
    }
  },
  cursors (props) {
    return {
      session: ReservationCursors.reservationSession,
      rentalTerms: ReservationCursors.rentalTerms,
      showPolicyModal: ['view', props.type, 'policies', 'modal'],
      showPolicy: ['view', props.type, 'policies', 'showPolicy']
    }
  },
  _onClick () {
    ReservationActions.showPolicyModal(this.props.type);
  },
  render () {
    const rentalTerms = this.state.rentalTerms;
    let title;
    let anyPolicies = this.state.session.reservationPolicies ? this.state.session.reservationPolicies.length > 0 : false,
      policies = anyPolicies ? this.state.session.reservationPolicies : [],
      mobile = !ResponsiveUtil.isMobile('(min-width: 768px)'),
      containerClass = classNames({
        'header-nav-item': true,
        'policy-link-container': true,
        mobile: mobile,
        'show-policy': this.state.showPolicy
      });

    if (!policies.length) return false;

    if (mobile) {
      title = i18n('reservationnav_0016') || 'POLICIES';
    } else if (rentalTerms.code) {
      if (rentalTerms.code.length > 0) {
        title = i18n('EU_commission_0001') || 'TERMS & CONDITIONS / POLICIES';
      }
    } else {
      title = i18n('reservationnav_0007') || 'View Policies';
    }

    // ECR-9956 :: Add aria-hidden to elements that are outside of the modal <div class="modal-content">.
    // TODO :: future restructure on modal's code (html hierarchy) might be able to avoid below custom jQuery selector.
    if (this.state.showPolicyModal) {
      $('#primaryHeader, #reservationFlow, footer, .policy-link, .reservation-utility-nav, .sign-in.resflow, .reservation-sub-header .header-nav-left, #reservationHeader nav').attr('aria-hidden', 'true');
    } else if ($('#reservationFlow').attr('aria-hidden')) {
      $('#primaryHeader, #reservationFlow, footer, .policy-link, .reservation-utility-nav, .sign-in.resflow, .reservation-sub-header .header-nav-left, #reservationHeader nav').removeAttr('aria-hidden', 'true');
    }

    return (
      <div className={containerClass}>
        <div role="button" tabIndex="0" onClick={this._onClick} onKeyPress={a11yClick(this._onClick)}
             className="policy-link">{title}</div>
        {this.state.showPolicyModal &&
        <GlobalModal active={this.state.showPolicyModal}
                     header={title}
                     content={<PolicyModal mobile={mobile} policies={policies} terms={rentalTerms} type={this.props.type}/>}
                     cursor={['view', this.props.type, 'policies', 'modal']}/>
        }
      </div>
    );
  }
});

const PolicyModal = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: function (props, context) {
    return {
      showPolicy: ['view', props.type, 'policies', 'showPolicy']
    };
  },
  _onClickTab (code) {
    ReservationActions.showPolicy(code, this.props.type);
  },
  componentDidMount () {
    if (!this.props.mobile) {
      let firstPolicy = _.get(this.props.policies, '0');
      if (this.props.terms && this.props.terms.code) {
        firstPolicy = this.props.terms;
      }
      ReservationActions.showPolicy(firstPolicy.code, this.props.type);
    }
  },
  render: function () {
    let mobileTermsPolicy;
    let mobileTermsTab;
    let termsTab;
    let termsPolicy;
    if (this.props.terms && this.props.terms.code) {
      mobileTermsPolicy = (<PolicyDescription
                    mobile={this.props.mobile}
                    key={this.props.terms.code}
                    show={this.state.showPolicy === this.props.terms.code}
                    onClick={this._onClickTab}
                    onKeyDown={a11yClick(this._onClickTab)}
                    policy={this.props.terms}/>);
      mobileTermsTab = (<PolicyTab
                    mobile={this.props.mobile}
                    key={this.props.terms.code}
                    selected={this.state.showPolicy === this.props.terms.code}
                    onKeyDown={a11yClick(this._onClickTab)}
                    onClick={this._onClickTab}
                    policy={this.props.terms}/>);
      termsTab = (<PolicyTab key={this.props.terms.code}
                  selected={this.state.showPolicy === this.props.terms.code}
                  onKeyDown={a11yClick(this._onClickTab)}
                  onClick={this._onClickTab}
                  policy={this.props.terms}/>);
      termsPolicy = (<PolicyDescription key={this.props.terms.code}
                    show={this.state.showPolicy === this.props.terms.code}
                    policy={this.props.terms}/>);
    }
    return (
      <div className="policy-modal">
        {this.props.mobile ?
          <div className="policy-tabs-mobile">
            {this.state.showPolicy ?
              <div className="policy-description">
                {mobileTermsPolicy}
                {this.props.policies.map((policy) => {
                  return <PolicyDescription
                    mobile={this.props.mobile}
                    key={policy.code}
                    show={this.state.showPolicy === policy.code}
                    onClick={this._onClickTab}
                    policy={policy}/>
                })}
              </div> :
              <div>
                {mobileTermsTab}
                {this.props.policies.map((policy) => {
                  if (policy.code) {
                    return <PolicyTab
                    mobile={this.props.mobile}
                    key={policy.code}
                    selected={this.state.showPolicy === policy.code}
                    onClick={this._onClickTab} policy={policy}/>
                  }
                })}  </div>
            }
          </div>
          :
          <div>
            <div className="policy-tabs cf" role="tablist">
              {termsTab}
              {this.props.policies.map((policy) => {
                if (policy.code) {
                return <PolicyTab key={policy.code} selected={this.state.showPolicy === policy.code}
                                  onClick={this._onClickTab} policy={policy}/>
                }
              })}
            </div>
            <div className="policy-description">
              {termsPolicy}
              {this.props.policies.map((policy) => {
                return <PolicyDescription key={policy.code} show={this.state.showPolicy === policy.code}
                                          policy={policy}/>
              })}
            </div>
          </div>
        }
      </div>
    );
  }
});

const PolicyTab = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  componentDidMount () {
    this.policiesNavigation();
  },
  policiesNavigation () {
    const tabs = document.querySelectorAll('[role="tab"]');
    const nextTab = function (event) {
      if (event.target.index < tabs.length - 1) {
        tabs[event.target.index + 1].focus();
      }
    };
    const previousTab = function (event) {
      if (event.target.index !== 0) {
        tabs[event.target.index - 1].focus();
      }
    };
    const addListeners = function (tabIndex) {
      tabs[tabIndex].index = tabIndex;
      tabs[tabIndex].addEventListener('keyup', function (event) {
        let key = event.keyCode || event.which;
        switch (key) {
          case 37:
          case 38:
            previousTab(event);
            break;
          case 39:
          case 40:
            nextTab(event);
            break;
          default:
        }
      });
    };
    for (let index = 0; index < tabs.length; index++) {
      addListeners(index);
    }
  },
  _onClick () {
    if (this.props.onClick) {
      this.props.onClick(this.props.policy.code);
    }
  },
  render: function () {
    const tabClasses = classNames({
      'tab': true,
      'selected': this.props.selected,
      'terms-tab': this.props.policy.code === GLOBAL.RENTAL_TERMS.CODE
    });

    return (
      <div role="button" onClick={this._onClick} onKeyPress={a11yClick(this._onClick)}
           className={tabClasses} role="tab" aria-controls={this.props.policy.code + 'Panel'}
           key={this.props.policy.code} id={this.props.policy.code + 'Tab'} onFocus={this._onClick}
           aria-selected={this.props.selected} tabIndex={this.props.selected ? '0' : '-1'}>
        {this.props.policy.description}
        {this.props.mobile ? <span className="caret">&rsaquo;</span> : false}
      </div>
    );
  }
});

const PolicyDescription = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    rentalTermsText: ReservationCursors.rentalTermsText
  },
  _onPrintClick: function (e) {
    e.preventDefault();
    window.print();
  },
  _onBack () {
    if (this.props.onClick) {
      this.props.onClick(null);
    }
  },
  _onChange: function (e) {
    e.preventDefault();
    this.props.policy.policy_text.some((lang) => {
      if (e.target.value === lang.locale_label) {
        ReservationActions.setRentalTermsDetailedText(lang.rental_terms_and_conditions_text);
        return true;
      }
    });
  },
  render: function () {
    let termsText;
    let selectLanguage;
    const show = this.props.show ? ' show' : 'hide';
    if (this.props.policy.policy_text) {
      termsText = this.props.policy.policy_text;
      if (typeof this.props.policy.policy_text !== 'string') {
        if (this.props.policy.policy_text[0].locale_label) {
        selectLanguage = (<select id="select-language" onChange={this._onChange} tabIndex="0" onKeyPress={a11yClick(this._onChange)}>
          {
          this.props.policy.policy_text.map((lang) => {
            return <option key={lang.locale_label} value={lang.locale_label}>{lang.locale_label}</option>;
          })
          }
          </select>);
        }
        termsText = <div dangerouslySetInnerHTML={{ __html: this.state.rentalTermsText.detailedText}}></div>;
      }
    }
    return (
      <div className={show + ' policy'} ref={this.props.policy.code}
           aria-labelledby={this.props.policy.code + 'Tab'} id={this.props.policy.code + 'Panel'}
           aria-hidden={this.props.show ? 'false' : 'true'}
           role="tabpanel">
        {this.props.mobile ?
          <div className="back" role="button" tabIndex="0" onKeyPress={a11yClick(this._onBack)}
               onClick={this._onBack}>&lsaquo; {i18n('resflowviewmodifycancel_2012') || 'BACK'}</div> : false
        }
        {selectLanguage}
        <h2> {this.props.policy.description} </h2>
        <p> {termsText} </p>
        {this.props.mobile ?
          false :
           <a className="print-link" href="#"
             onClick={this._onPrintClick}
             onKeyPress={a11yClick(this._onPrintClick)}>
             {i18n('resflowconfirmation_0002') || 'PRINT'}
           </a>
        }
      </div>
    );
  }
});


module.exports = PolicyLink;
