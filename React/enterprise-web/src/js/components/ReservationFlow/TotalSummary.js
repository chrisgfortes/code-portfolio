import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';

const FeeLineItem = require('./FeeLineItem');
const ReservationCursors = require('../../cursors/ReservationCursors');

const TotalSummary = React.createClass({
  mixins: [BaobabReactMixinBranch, React.addons.PureRenderMixin],
  cursors: {
    session: ReservationCursors.reservationSession
  },
  getDefaultProps: function () {
    return {
      title: enterprise.i18nReservation.reservationnav_0017
    };
  },
  toggleFeesContainer: function () {
    $('.fees-dropdown').attr('aria-hidden', !$('.fees-dropdown').toggleClass('active').hasClass('active'));
  },
  render: function () {
    let priceSummary = this.props.priceSummary;
    let feeLineItems = [];
    // let extras = [];
    let vehicleLineItems;
    let extrasPrice;
    let taxesFeesPrice;
    let totalPrice;
    // let symbol;
    let taxesAndFees;
    let carName;
    let car = this.props.car;

    if (car) carName = car.name;

    if (priceSummary) {
      // let tempPrice = 0;

      vehicleLineItems = priceSummary.payment_line_items && priceSummary.payment_line_items.map(function (item, index) {
        if (item.category !== 'VEHICLE_RATE' && item.category !== 'SAVINGS' && item.category !== 'EPLUS_REDEMPTION_SAVINGS') {
          return false;
        }

        let rateType = {
          'HOURLY': enterprise.i18nReservation.resflowcarselect_0067,
          'DAILY': enterprise.i18nReservation.resflowcarselect_0068,
          'WEEKLY': enterprise.i18nReservation.resflowcarselect_0069,
          'MONTHLY': enterprise.i18nReservation.resflowcarselect_0070,
          'RENTAL': enterprise.i18nReservation.resflowextras_9008
        };

        let rateTypeDisplay;
        if (item.rate_type === 'RENTAL') {
          rateTypeDisplay = carName + ' ' + rateType[item.rate_type].replace('#{rate}', item.rate_quantity);
        } else if (!rateType[item.rate_type]) {
          rateTypeDisplay = item.description;
        } else {
          rateTypeDisplay = carName + ' ' + rateType[item.rate_type].replace('#{number}', item.rate_quantity);
        }

        return (
          <SummaryLineItem
            key={`${index}${item.code}`}
            description={rateTypeDisplay}
            format={item.total_amount_view.format}
            status={item.status} />
        );
      });

      if (this.props.session.chargeType === 'REDEMPTION') {
        vehicleLineItems.push(
          <SummaryLineItem
            key={vehicleLineItems.length}
            description={enterprise.i18nReservation.resflowcarselect_0053.replace('#{number}', car.redemptionDaysCount)}
            format={enterprise.i18nReservation.resflowcarselect_0062.replace('#{number}', car.redemptionPointsUsed)}
            status={'REDEMPTION'}/>
        );
      }

      //vehiclePrice = priceSummary.payment_line_items[0].total_amount_view.format;
      taxesFeesPrice = priceSummary.estimated_total_taxes_and_fees_view.format;
      totalPrice = priceSummary.estimated_total_view.format;

      //If estimated extras price object is available override the "Net Rate" default
      if (priceSummary.estimated_total_extras_and_coverages_view && priceSummary.estimated_total_extras_and_coverages_view.format) {
        //set extrasPrice to value from priceSummary object
        extrasPrice = priceSummary.estimated_total_extras_and_coverages_view.format;

        //If price is $0.00 or 0 set extraPrice to null to hide this field from the view
        if (!parseFloat(extrasPrice.replace(/\D/gi, ''))) extrasPrice = null;
      }

      //If taxes and fees price is 0 show "include" instead
      if (!parseFloat(taxesFeesPrice.replace(/\D/gi, ''))) {
        taxesFeesPrice = enterprise.i18nReservation.reservationnav_0018;
      }

      // Adding up extra line items
      /*extras = priceSummary.extra_line_items;
       if(extras && extras.length > 0) {

       for (let extra of extras) {
       if(extra.total_amount_view && extra.total_amount_view.amount) {
       tempPrice = parseFloat(extra.total_amount_view.amount);
       }
       else {
       tempPrice = 0;
       }
       extrasPrice = extrasPrice + tempPrice;
       }
       extrasPrice = symbol + extrasPrice.toFixed(2).toString();
       }*/

      // Adding up fees line items
      feeLineItems = priceSummary.fee_line_items;
      if (feeLineItems) {
        taxesAndFees = feeLineItems.map(function (feeItem, currentIndex) {
          let totalAmountObj = feeItem.total_amount_view;
          return (
            <FeeLineItem key={currentIndex}
              code={feeItem.code}
              label={feeItem.description}
              hidden={true}
              price={feeItem.status === 'INCLUDED' ? enterprise.i18nReservation.reservationnav_0018 : totalAmountObj.format}/>
          );
        });
      }
    }

    let contractType = _.get(this.state.session, 'contract_details.contract_type', '').toLowerCase();
    return (
      <div className="summary-container">
        <h2 id="summaryDialogAriaTitle">{this.props.title}</h2>

        <table className="summary-table">
          <tbody>
            {vehicleLineItems}
            {extrasPrice &&
              <tr className="summary-row">
                <th scope="row" className="summary-item">{enterprise.i18nReservation.reservationnav_0011}</th>
                <td className="summary-price">{extrasPrice}</td>
              </tr>
            }
            <tr className="summary-row fees-row" tabIndex="0" role="button" aria-controls="fees-dropdown"
                 onKeyPress={a11yClick(this.toggleFeesContainer)} onClick={this.toggleFeesContainer}>
              <th scope="row" className="summary-item">
                {enterprise.i18nReservation.reservationnav_0019}
                <i className="icon icon-nav-carrot-down"></i>
              </th>
              <td className="summary-price">{taxesFeesPrice}</td>
            </tr>

            {taxesAndFees}

            <tr className="total-row">
              {contractType === 'corporate' &&
                <td colSpan="4" className="car-savings"><i className="icon icon-icon-promo-applied"></i>{i18n('resflowcarselect_8010') || 'CUSTOM RATE'}</td>
              }
              <th scope="row" className="summary-item">{enterprise.i18nReservation.reservationnav_0008}</th>
              <td className="summary-price">{totalPrice}</td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
});

const SummaryLineItem = React.createClass({
  render () {
    let rightSide;

    if (this.props.status === 'INCLUDED') {
      rightSide = enterprise.i18nReservation.reservationnav_0018;
    } else {
      rightSide = this.props.format;
    }

    return (
      <tr className="summary-row">
        <th scope="row" className="summary-item">{this.props.description}</th>
        <td className="summary-price">{rightSide}</td>
      </tr>
    );
  }
});

module.exports = TotalSummary;
