/**
 * ReservationHeader
 */
import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import classNames from 'classnames';
import ModifyController from '../../controllers/ModifyController';
import SessionService from '../../services/SessionService';
import ModifyActions from '../../actions/ModifyActions';
import RedirectActions from '../../actions/RedirectActions';
import ExpeditedController from '../../controllers/ExpeditedController';
import ReservationActions from '../../actions/ReservationActions';
import ReservationCursors from '../../cursors/ReservationCursors';
import TotalSummary from './TotalSummary';
import CodeBanner from '../Corporate/CodeBanner';
import Step from './Step';
import CancelModifyModalContent from '../Modify/CancelModifyModalContent';
import GlobalModal from '../Modal/GlobalModal';
import PolicyLink from './PolicyLink';
import PricingController from '../../controllers/PricingController';
import { LOCATION_SEARCH } from '../../constants';

/**
 * ReservationHeader
 */
const ReservationHeader = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    stepData: ReservationCursors.reservationSteps,
    session: ReservationCursors.reservationSession,
    selectedCar: ReservationCursors.selectedCar,
    currentHash: ReservationCursors.currentHash,
    locationSearchType: ReservationCursors.locationSearchType,
    sameLocation: ReservationCursors.sameLocation,
    modify: ReservationCursors.modify,
    cancelModifyModalOpen: ReservationCursors.cancelModifyModalOpen,
    viewTermsModal: ReservationCursors.viewTermsModal,
    view: ReservationCursors.view,
    blockModifyPickupLocation: ReservationCursors.blockModifyPickupLocation,
    blockModifyDropoffLocation: ReservationCursors.blockModifyDropoffLocation
  },
  buildResSteps: function () {
    let tempSteps = [];
    let stepData = this.state.stepData;
    let disabled = !!(this.state.blockModifyPickupLocation && this.state.modify);
    let dropoffDisabled = !!(this.state.blockModifyDropoffLocation && this.state.modify);
    let stepIndex = 1;

    const dateTimeFormat = `${enterprise.i18nUnits.shortdateformat}, ${enterprise.i18nUnits.timeformat}`;
    const dateTimeLabel = `${moment(stepData.pickupDate).format(dateTimeFormat)} - ${moment(stepData.dropoffDate).format(dateTimeFormat)}`;
    // TODO: Need to clean this up BP
    tempSteps.push({
      active: this.state.currentHash === 'dateTime',
      stepHash: 'dateTime',
      completed: !!stepData.pickupDate,
      stepTitle: stepIndex++ + '. ' + i18n('reservationnav_0001'),
      stepValue: dateTimeLabel
    });

    const pickupType = _.get(this.state.session, 'reservationsInitiateRequest.pickupLocation.type') || ''; // ECR-12514 - _.get() ignores the default value when null value is found
    if (this.state.sameLocation) {
      const isLocationCompleted =  pickupType !== LOCATION_SEARCH.STRICT_TYPE.CITY && pickupType !== LOCATION_SEARCH.STRICT_TYPE.COUNTRY;
      tempSteps.push({
        active: this.state.currentHash === 'location' && this.state.locationSearchType === 'pickup',
        stepHash: 'location',
        completed: isLocationCompleted,
        stepTitle: stepIndex++ + '. ' + i18n('reservationnav_0003'),
        stepValue: isLocationCompleted ? stepData.pickupLocation : i18n('reservationnav_0004'),
        disabled: disabled
      });
    } else {
      const isPickupCompleted = pickupType !== LOCATION_SEARCH.STRICT_TYPE.CITY && pickupType !== LOCATION_SEARCH.STRICT_TYPE.COUNTRY;
      tempSteps.push({
        active: (this.state.currentHash === 'location' || this.state.currentHash === 'location/pickup') && this.state.locationSearchType === 'pickup',
        stepHash: 'location/pickup',
        completed: isPickupCompleted, //stepData.pickupLocation ? true : false,
        stepTitle: stepIndex++ + '. ' + i18n('reservationnav_0005'),
        stepValue: isPickupCompleted ? stepData.pickupLocation : i18n('reservationnav_0004'),
        disabled: disabled
      });

      const returnType = _.get(this.state.session, 'reservationsInitiateRequest.returnLocation.type') || ''; // ECR-12514 - _.get() ignores the default value when null value is found
      const isReturnCompleted = returnType !== LOCATION_SEARCH.STRICT_TYPE.CITY && returnType !== LOCATION_SEARCH.STRICT_TYPE.COUNTRY;
      tempSteps.push({
        active: (this.state.currentHash === 'location' || this.state.currentHash === 'location/dropoff') && this.state.locationSearchType === 'dropoff',
        stepHash: 'location/dropoff',
        completed: isReturnCompleted, //stepData.dropoffLocation ? true : false,
        stepTitle: stepIndex++ + '. ' + i18n('reservationnav_0006'),
        stepValue: isReturnCompleted ? stepData.dropoffLocation : i18n('reservationnav_0004'),
        disabled: dropoffDisabled
      });
    }
    tempSteps.push({
      active: this.state.currentHash === 'cars',
      stepHash: 'cars',
      completed: !!stepData.carClass,
      stepTitle: stepIndex++ + '. ' + i18n('reservationnav_0009'),
      stepValue: stepData.carClass || i18n('reservationnav_0010')
    });
    tempSteps.push({
      active: this.state.currentHash === 'extras',
      stepHash: 'extras',
      completed: !!stepData.addons,
      stepTitle: stepIndex++ + '. ' + i18n('reservationnav_0011'),
      stepValue: i18n('reservationnav_0012')
    });

    return tempSteps;
  },
  _cancelModify () {
    ModifyActions.toggleCancelConfirmationModal(true);
  },
  _confirmCancel () {
    ModifyActions.toggleCancelConfirmationModal(false);
    ModifyController.abandonModify();
  },
  _openLoginModal () {
    if (!this.state.session.loggedIn) {
      ExpeditedController.setInput('modal', 'login');
    } else {
      return false;
    }
  },
  getClearReservationOptions () { // ECR-12479
    const isFedexReservation = this.state.session.fedexReservation;
    const optionsToClearBookingWidget = {removeDatetimeAndLocation: true};
    const defaultOptions = {};
    return isFedexReservation ? optionsToClearBookingWidget : defaultOptions;
  },
  getOnConfirmHomeModal () {
    const isInRegularFlow = enterprise.aem.homePath === ''; // ECR-12487
    const isDoNotMarketCID = ReservationStateTree.select(ReservationCursors.isDoNotMarketCID).get(); // ECR-10273 + ECR-11325
    return isInRegularFlow && isDoNotMarketCID ?
            () => { RedirectActions.goToReservationStep(); } :
            () => { RedirectActions.goHome(); };
  },
  _onClickHome (event) {
    event.preventDefault();

    ReservationActions.showHomeModal(() => {
      SessionService.clearSession(this.getClearReservationOptions())
                    .then(this.getOnConfirmHomeModal());
    });
  },
  render: function () {
    let totalsClassName = classNames({
      'totals-utility-nav-item': true,
      'no-hover': !this.state.stepData.showTotals,
      'show': true//this.state.stepData.showTotals
    });

    const tempSteps = this.buildResSteps();
    const steps = tempSteps.map((step, currentIndex) => <Step key={currentIndex} active={step.active}
                                                              stepHash={step.stepHash} stepTitle={step.stepTitle}
                                                              stepValue={step.stepValue} completed={step.completed}
                                                              disabled={step.disabled} columnCount={tempSteps.length}/>);
    const session = this.state.session;
    let priceSummary = null;
    let defaultTopNavAmount = session.defaultTopNavAmount;
    let splitAmountInfo = {
      symbol: enterprise.primaryCurrencySymbol
    };
    let showTopNavAmount = true;
    let selectedCar = this.state.selectedCar;

    let netRateText;

    if (
      _.get(this.state.session, 'contract_details.contract_type') === 'CORPORATE' &&
      _.get(selectedCar, `vehicleRates.${this.state.session.chargeType}.price_summary.estimated_total_view.amount`) === '0.00'
    ) {
      netRateText = PricingController.getNetRate();
    }

    if (defaultTopNavAmount) {
      splitAmountInfo = PricingController.getSplitCurrencyAmount(defaultTopNavAmount);
    }

    if (selectedCar && selectedCar.vehicleRates) {
      let chargeType = session.chargeType === 'REDEMPTION' ? 'PAYLATER' : session.chargeType;
      priceSummary = _.get(selectedCar, `vehicleRates.${chargeType}.price_summary`);
      if (priceSummary) {
        splitAmountInfo = PricingController.getSplitCurrencyAmount(priceSummary.estimated_total_view);
      } else {
        netRateText = PricingController.getNetRate();
      }
    }

    let resSubHeader = 'reservation-sub-header header-nav cf';
    let cancelModifyButton;
    if (this.state.modify) {
      cancelModifyButton = (
        <div onClick={this._cancelModify} className="header-nav-item cancel-modify-button">
          <button className="text-btn">{i18n('resflowviewmodifycancel_2001')}</button>
        </div>
      );
      resSubHeader += ' modifying';
    }

    const topNavAmount = (
      <span>
        <sup>{splitAmountInfo.symbol}</sup>
        {splitAmountInfo.integralPart}
        <sup>{splitAmountInfo.decimalDelimiter}{splitAmountInfo.decimalPart}</sup>
      </span>
    );

    return (
      <div>
        <div className={resSubHeader}>
          <div className="header-nav-left">
            <div className="logo header-nav-item" id="enterpriseHomeLogo">
              <a className="header-logo-link" href="#"
                 onClick={this._onClickHome} onKeyPress={a11yClick(this._onClickHome)}>
                <img src={enterprise.reservation.logoPath} alt={enterprise.reservation.logoAltText}/>
              </a>
            </div>
            <div className="mobile-logo header-nav-item">
              <a className="header-logo-link" href="#"
                 onClick={this._onClickHome} onKeyPress={a11yClick(this._onClickHome)}>
                <i className="icon icon-nav-mobile-e-icon-white" aria-label={i18n('enterpriselogo_0001') || 'Enterprise'}/>
              </a>
            </div>
            <CodeBanner />
          </div>
          <div className="header-nav-right">
            <div className="reservation-utility-nav-wrapper header-nav-item">
              {/*this.state.stepData.commit ? false : <PolicyLink type='pickup'/>*/}
              <PolicyLink type="pickup"/>
              <div className="reservation-utility-nav header-nav-item">
                <ol>
                  <li className={totalsClassName}>
                    <div className="res-utility-nav-label" tabIndex={showTopNavAmount ? 0 : false} aria-describedby="reservationSummaryTooltipContent">
                      { showTopNavAmount ?
                        <div className="res-utility-nav-amount">
                          <span className="total-label">{i18n('reservationnav_0008')}: </span>
                          <span className="total-price">
                            { netRateText || topNavAmount }
                          </span>
                        </div>
                        : null }
                      { showTopNavAmount ?
                        <div id="reservationSummaryTooltipContent" role="dialog"
                              className="res-utility-nav-content icon icon-utility-notch">
                          <div className="mobile-steps">
                            <ul className="mobile-reservation-steps">
                              {steps}
                            </ul>
                            { this.state.stepData.showTotals ?
                              <TotalSummary session={session} priceSummary={priceSummary} car={selectedCar}/> : null }
                          </div>
                          { this.state.stepData.showTotals ?
                            <div className="totals-content" role="dialog" aria-labelledby="summaryDialogAriaTitle">
                              <TotalSummary session={session} priceSummary={priceSummary} car={selectedCar}/>
                            </div>
                            : null }
                        </div>
                        : null }
                    </div>
                  </li>
                </ol>
              </div>
              <div className="sign-in resflow header-nav-item">
                <div id="resflow-login-container">
                  { /* Insert me later - <Login /> */ }
                </div>
              </div>
              {cancelModifyButton}
            </div>
          </div>
        </div>
        <nav>
          <ol className="reservation-steps" role="navigation">
            {steps}
          </ol>
        </nav>
        {this.state.cancelModifyModalOpen &&
        <GlobalModal active={this.state.cancelModifyModalOpen}
                     header={i18n('resflowviewmodifycancel_1019')}
                     content={
                        <CancelModifyModalContent
                          confirm={this._confirmCancel}
                          content={i18n('resflowviewmodifycancel_0035')} />
                      }
                     cursor={['view', 'modify', 'cancel', 'modal']}/>
        }
      </div>
    );
  }
});


module.exports = ReservationHeader;
