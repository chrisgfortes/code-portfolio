'use strict';
import ResponsiveUtil from '../../utilities/util-responsive';
const BaobabReactMixinBranch = require('baobab-react/mixins').branch;
const ReservationCursors = require('../../cursors/ReservationCursors');
const RedirectActions = require('../../actions/RedirectActions');
const classNames = require('classnames');

let Step = React.createClass({
  mixins: [
    BaobabReactMixinBranch
  ],
  cursors: {
    currentBreakpoint: ReservationCursors.currentViewBreakpoint
  },
  getDefaultProps: function () {
    return {
      stepHash: null,
      stepTitle: 'Step Title',
      stepValue: 'Step Value',
      completed: false,
      active: false,
      columnCount: 0
    };
  },
  cannotModify: function () {
    enterprise.utilities.modal.open(i18n('modifyflow_0001') || 'We\'re sorry, but this information cannot be modified.');
  },
  stepClicked_Handler: function () {
    let allowedStep = ['dateTime', 'location', 'location/pickup', 'location/dropoff', 'cars', 'extras'];
    RedirectActions.goToReservationStep(allowedStep.indexOf(this.props.stepHash) ? this.props.stepHash : 'dateTime');
  },
  render: function () {
    let stepClick = this.props.completed ? this.stepClicked_Handler : null;

    if (this.props.disabled) {
      stepClick = this.cannotModify;
    }
    let isMobile = ResponsiveUtil.isMobile('(max-width: 768px)');
    let className = classNames({
      'reservation-nav-item': true,
      'completed': this.props.completed,
      'active': this.props.active,
      'disabled': this.props.disabled
    });

    let columnClassName = classNames({
      'step-col-1': this.props.columnCount === 1,
      'step-col-2': this.props.columnCount === 2,
      'step-col-3': this.props.columnCount === 3,
      'step-col-4': this.props.columnCount === 4,
      'step-col-5': this.props.columnCount === 5,
      'disabled': this.props.disabled
    });

    let iconClassName = classNames({
      'icon icon-forms-checkmark': !isMobile,
      'icon icon-forms-checkmark-white': isMobile
    });

    let activeIconClassName = classNames({
      'icon icon-res-nav-notch': true,
      'active': this.props.active
    });

    const isInteractable = this.props.active || this.props.completed;
    const tabIndex = isInteractable ? 0 : -1;

    return (
      <li className={columnClassName} data-stephash={this.props.stepHash}>
        <div role="link" className={className}
             aria-disabled={!isInteractable} tabIndex={tabIndex}
             onClick={stepClick} onKeyPress={a11yClick(stepClick)}>
          <div className="step-title">
            {this.props.stepTitle}
            <i className={iconClassName} aria-label={i18n('reservationnav_0039') || 'Completed Reservation Step'}></i>
          </div>
          <div className="step-value">{this.props.stepValue}</div>
          {!this.props.disabled &&
            <i className={activeIconClassName}></i>
          }
        </div>
        {this.props.disabled ?
          <i className="edit icon icon-alert-interrogation-mark" role="button" tabIndex="0" onKeyPress={a11yClick(this.cannotModify)} onClick={this.cannotModify}></i> :
          null}
      </li>
    );
  }
});

module.exports = Step;
