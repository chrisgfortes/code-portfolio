import {branch as BaobabReactMixinBranch} from 'baobab-react/mixins';
import ReservationCursors from '../../cursors/ReservationCursors';
import BookingWidgetModelController from '../../controllers/BookingWidgetModelController';
import DateTimeWidget from '../BookingWidget/DateTimeWidget';
import EnhancedLandingPage from '../BookingWidget/EnhancedLandingPage';
import ReservationToggle from '../BookingWidget/ReservationToggle';
import CarSelect from '../CarSelect/CarSelect';
import LocationSearch from '../LocationSearch/LocationSearch';
import ExtrasView from '../Extras/ExtrasView';
import Spinnerprise from '../Spinner/Spinnerprise';
import Verification from '../Verification/Verification';
import Confirmed from '../Confirmed/Confirmed';
import TimedOut from '../Errors/TimedOut';
import GlobalModal from '../Modal/GlobalModal';
import HomeModal from '../Modal/HomeModal';
import TimeOutModal from '../Modal/TimeOutModal';
import LicenseExpiredMessage from '../Account/LicenseExpiredMessage';
import DeepLinkErrors from './DeepLinkErrors';
import ExpeditedModals from '../Verification/Expedited/ExpeditedModals';
import ChangePasswordModal from '../Account/Password/ChangePasswordModal';
import { Enterprise } from '../../modules/Enterprise';
import { Debugger } from '../../utilities/util-debug';

const ReservationFlow = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  componentWillMount(){
    this.logger = new Debugger(window._isDebug, this, true, 'ReservationFlow.js');
  },
  isDebug: false,
  logger: {},
  cursors: {
    contractDetails: ReservationCursors.contractDetails,
    locationSearchType: ReservationCursors.locationSearchType,
    filteredVehicles: ReservationCursors.filteredVehicles,
    componentToRender: ReservationCursors.componentToRender,
    loadingClass: ReservationCursors.loadingClass,
    homeModal: ReservationCursors.homeModal,
    sessionTimeout: ReservationCursors.sessionTimeout,
    viewTermsModal: ReservationCursors.viewTermsModal,
    deepLinkReservation: ReservationCursors.deepLinkReservation,
    session: ReservationCursors.reservationSession,
    changePassword: ReservationCursors.changePassword,
    changePasswordErrors: ReservationCursors.changePasswordErrors
  },
  addAriaAttribute () {
    $('header, footer, .reservation-step').attr('aria-hidden', 'true');
  },
  removeAriaAttribute (elementFocus) {
    $('header, footer, .reservation-step').removeAttr('aria-hidden');
    if (elementFocus) {
      let newElement = document.querySelector('h1');
      if (newElement) {
        newElement.setAttribute('tabIndex', '-1');
        newElement.focus();
      }
    }
  },
  getBookingWidgetComponent (deepLinkErrors) {
    const isEnhancedDeeplink = !!(_.get(this.state.deepLinkReservation, 'reservation.pickup_location'));
    return isEnhancedDeeplink ?
            <EnhancedLandingPage deepLinkErrors={deepLinkErrors} /> :
            <ReservationToggle deepLinkErrors={deepLinkErrors}
                               shouldStartExpanded={true}
                               loyaltyBrand={this.props.loyaltyBrand}
                               active={enterprise.activeReservationTab || 'book'}
                               modelController={BookingWidgetModelController} />;
  },
  render () {
    this.logger.count('render()');
    let componentToRender = null;
    let currentHash = null;
    let deepLinkErrors = <DeepLinkErrors />;
    let loadingClass = this.state.loadingClass ? this.state.loadingClass : '';
    switch (this.state.componentToRender) {
      // @todo: use constants from /constants/resflow
      case 'BookingWidget':
        componentToRender = this.getBookingWidgetComponent(deepLinkErrors);
        currentHash = 'book';
        break;
      case 'DateTimeWidget':
        componentToRender = <DateTimeWidget deepLinkErrors={deepLinkErrors} modelController={BookingWidgetModelController} isBookingController/>;
        currentHash = 'dateTime';
        break;
      case 'LocationSearch':
        componentToRender =
          <LocationSearch deepLinkErrors={deepLinkErrors} modelController={this.props.modelController} type={this.state.locationSearchType}/>;
        currentHash = 'location';
        break;
      case 'PickupLocationSearch':
        componentToRender = <LocationSearch deepLinkErrors={deepLinkErrors} modelController={this.props.modelController} type="pickup"/>;
        currentHash = 'location/pickup';
        break;
      case 'DropoffLocationSearch':
        componentToRender = <LocationSearch deepLinkErrors={deepLinkErrors} modelController={this.props.modelController} type="dropoff"/>;
        currentHash = 'location/dropoff';
        break;
      case 'CarSelect':
        componentToRender = <CarSelect deepLinkErrors={deepLinkErrors} cars={this.state.filteredVehicles}/>;
        currentHash = 'cars';
        break;
      case 'Addons':
        componentToRender = <ExtrasView deepLinkErrors={deepLinkErrors} modelController={this.props.modelController}/>;
        currentHash = 'extras';
        break;
      case 'Verification':
        componentToRender = <Verification />;
        currentHash = 'commit';
        break;
      case 'Modify':
        componentToRender = <Verification modify={true}/>;
        currentHash = 'modify';
        break;
      case 'Confirmed':
        componentToRender = <Confirmed />;
        currentHash = 'confirmed';
        break;
      case 'Modified':
        componentToRender = <Confirmed modified={true}/>;
        currentHash = 'modified';
        break;
      case 'Details':
        componentToRender = <Confirmed details={true}/>;
        currentHash = 'details';
        break;
      case 'Cancelled':
        currentHash = 'cancelled';
        componentToRender = <Confirmed cancelled={true}/>;
        break;
      case 'TimedOut':
        currentHash = 'timedout';
        componentToRender = <TimedOut />;
        break;
      default:
        currentHash = 'loading';
        componentToRender = 'DEFAULT';
        break;
    }
    if (location.hash.replace(/\?.*$/, '') != '#' + currentHash) {
      componentToRender = null;
      currentHash = 'loading';
    }
    if (this.state.componentToRender) {
      this.logger.info('this.state.componentToRender', this.state.componentToRender);
      if (!Enterprise.global.utilities.header) {
        // the following is used by Navigation.js ECR-13936
        this.logger.log('001');
        Enterprise.global.utilities.headerToRender = this.state.componentToRender;
      } else {
        this.logger.log('002');
        Enterprise.global.utilities.header(this.state.componentToRender);
      }
    } else {
      this.logger.warn('no component to render yet');
    }
    const details = this.state.session && this.state.session.contract_details;
    return (
      <div className={'reservation-flow ' + currentHash + ' ' + loadingClass}>
        <LicenseExpiredMessage/>
        <div className={'reservation-step'} id={currentHash}>
          {componentToRender}
        </div>
        {this.state.loadingClass &&
        <div className="full-screen-loading">
          <GlobalModal active={this.state.loadingClass}
                       header=""
                       content={<Spinnerprise modelController={this.props.modelController}/>}
                       onOpening={this.addAriaAttribute}
                       onClosing={this.removeAriaAttribute.bind(null, true)}
                       classLabel="spinner-class"/>
        </div>
        }
        {this.state.homeModal.modal &&
        <GlobalModal active={this.state.homeModal.modal}
                     header={enterprise.i18nReservation.resflowviewmodifycancel_2009}
                     content={
                            <HomeModal onConfirm={this.state.homeModal.onConfirmModal} />
                        }
                     contentHeader={i18n('resflowviewmodifycancel_2009')}
                     contentText={i18n('resflowviewmodifycancel_0054')}
                     onOpening={this.addAriaAttribute}
                     onClosing={this.removeAriaAttribute.bind(null, true)}
                     cursor={['view', 'home', 'modal']}/>
        }

        {this.state.sessionTimeout.showModal &&
          <TimeOutModal redirectUrl={this.state.sessionTimeout.redirectUrl}/>
        }

        {this.state.viewTermsModal && details && details.terms_and_conditions &&
        <GlobalModal active={this.state.viewTermsModal}
                     header={enterprise.i18nReservation.promotionemail_0200}
                     content={<div><p dangerouslySetInnerHTML={{__html: details.terms_and_conditions}}></p></div>}
                     cursor={['view', 'viewTermsModal', 'modal']}/>
        }

        <ExpeditedModals />
        <ChangePasswordModal header={i18n('loyaltysignin_0050')}
                             changePassword={this.state.changePassword}
                             errors={this.state.changePasswordErrors} />
      </div>
    );
  }
});

module.exports = ReservationFlow;
