'use strict';
let FeeLineItem = React.createClass({
  getDefaultProps: function () {
    return {
      key: null,
      code: null,
      label: null,
      price: null,
      hidden: false
    };
  },
  render: function () {
    return (
      <tr className="summary-row fees fees-dropdown" aria-hidden={this.props.hidden}>
        <th scope="row" className="summary-item">{this.props.label}</th>
        <td className="summary-price">{this.props.price}</td>
      </tr>
    );
  }
});

module.exports = FeeLineItem;
