import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import ReservationCursors from '../../cursors/ReservationCursors';

import ReservationFlowModelController from '../../controllers/ReservationFlowModelController';

const { clearDriverInformation } = ReservationFlowModelController;

const DeepLinkError = React.createClass({
  mixins: [BaobabReactMixinBranch, React.addons.PureRenderMixin],
  cursors: {
    deepLink: ReservationCursors.deepLinkView
  },
  getInitialState () {
    return {
      ignoredErrors: ['CROS_RES_ASSOCIATE_LAST_NAME_MISMATCH', 'CROS_RES_RENTER_NOT_FOUND']
    };
  },
  componentDidMount () {
    if (_.get(this.state, 'deepLink.errors') && this.state.deepLink.errors.length > 0) {
      if (this.state.deepLink.errors.find(error => this.state.ignoredErrors.indexOf(error.code) >= 0)) {
        clearDriverInformation();
      }
    }
  },
  render () {
    let deepLinkErrors = this.state.deepLink.errors || [];
    //filter out ignoredErrors so the list only contains what we want to show
    deepLinkErrors = deepLinkErrors.filter(item => this.state.ignoredErrors.indexOf(item.code) < 0);
    if (!deepLinkErrors || !deepLinkErrors.length) {
      return false;
    }

    return (
      <div className="alert-message-wrapper">
        <div className="icon-wrapper">
          <i className="icon icon-alert-caution-yellow"/>
        </div>
          {deepLinkErrors.map(item => (
            <div key={item.code} className="alert-message">
              {item.defaultMessage || item.message || item.code}
            </div>
          ))}
      </div>
    );
  }
});

module.exports = DeepLinkError;
