/**
 *
 * NumericStepper
 * @author B.Podczerwinski, M.Rainsford
 *
 */
import ExtrasController from '../../controllers/ExtrasController';
import classNames from 'classnames';

export default function NumericStepper ({ selectedQuantity, maxQuantity, code, index, type }) {
  let disabledClass = classNames({
    "disabled": selectedQuantity === maxQuantity
  });
  return (
    <div className="numeric-stepper" data-code={code}>
      <div className="minus" data-action="subtract"
           onClick={(e) => {ExtrasController.numericStepperClicked_Handler(e, index, type + "Extras", maxQuantity )}}
           tabIndex="0" role="button" onKeyPress={a11yClick((e) => {ExtrasController.numericStepperClicked_Handler(e, index, type + "Extras", maxQuantity )})}>
        -
      </div>
      <div className="value">{selectedQuantity}</div>
      <div className={"plus " + disabledClass} data-action="add"
           onClick={(e) => {ExtrasController.numericStepperClicked_Handler(e, index, type + "Extras", maxQuantity )}}
           tabIndex="0" role="button" onKeyPress={a11yClick((e) => {ExtrasController.numericStepperClicked_Handler(e, index, type + "Extras", maxQuantity )})}
           title={(selectedQuantity === maxQuantity) ? 'You Have Added the Maximum Amount' : ''}>

        <span>+</span>
      </div>
    </div>
  );
}

NumericStepper.defaultProps = {
  modelController: null,
  maxQuantity: 0,
  selectedQuantity: 0
};

NumericStepper.displayName = 'NumericStepper';

