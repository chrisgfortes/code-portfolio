const LocationSummary = React.createClass({
  mixins: [
    React.addons.PureRenderMixin
  ],
  render() {
    let location = this.props.location;
    return (
      <div className="location-summary_location">
        <div className="location_type bold">
          {this.props.type}
        </div>
        <div className="location_detail">
          <p>{location.name}</p>

          <p>{location.address.city}, {location.address.country_subdivision_code}</p>
          {location.phones.length ?
            <p>{location.phones[0].phone_number}</p>
            :
            null
          }
        </div>
        <div className="location_date">
          <p>{this.props.date}</p>

          <p>{this.props.time}</p>
        </div>
      </div>
    )
  }
});

module.exports = LocationSummary;
