import ReceiptRentalSummary from './ReceiptRentalSummary';
import RentalCharges from './RentalCharges';
import InformationSection from './InformationSection';
import { Debugger } from '../../utilities/util-debug';

const Receipt = React.createClass({
  mixins: [
    React.addons.PureRenderMixin
  ],
  componentWillMount () {
    this.logger = new Debugger(window._isDebug, this, true, 'Receipt.js');
  },
  isDebug: false,
  logger: {},
  printReceipt(event) {
    event.preventDefault();
    window.print();
  },
  render () {
    let receipt = this.props.receipt;
    this.logger.count('render()');
    return (
      <div>
        { this.props.loading ?
          <div className={this.props.loading ? 'loading' : ''} />
          :
          <div className="receipt" ref="receipt">
            <div className="receipt-header cf">
              <h2>{i18n('receiptsmodal_0002')} {receipt.headerDate}</h2>
              <button className="btn print-link" onClick={this.printReceipt}><i className="icon icon-receipt-print" />{i18n('resflowconfirmation_0002')}</button>
            </div>
            <div className="receipt-subheader cf">
              <div className="rental-number">
                <span className="bold">{i18n('receiptsmodal_0001')}</span> {receipt.ticket_number}
              </div>
              <div className="customer-service-number">
                <i className="icon icon-receipt-phone"/>{i18n('receiptsmodal_0003')} <span className="bold">{receipt.contactUsPhoneNumber}</span>
              </div>
            </div>
            <ReceiptRentalSummary receipt={receipt}/>
            <RentalCharges receipt={receipt}/>
            <InformationSection receipt={receipt}/>
            {receipt.vat_disclaimer_text ?
              <p className="disclaimer">
                <span className="bold">{i18n('receiptsmodal_0014')}</span>
              </p>
              :
              null
            }
          </div>
        }
      </div>
    );
  }
});

// VAT / INVOICE stuff we'll probably need to include later
/*            <p className="disclaimer">
 <span className="bold">{i18n('receiptsmodal_0014')}</span>
 <span className="bold"> {i18n('receiptsmodal_0015')}</span> {receipt.ticket_number}
 <span className="bold"> {i18n('receiptsmodal_0016')}</span> {receipt.invoice_number}
 </p>
 */

module.exports = Receipt;
