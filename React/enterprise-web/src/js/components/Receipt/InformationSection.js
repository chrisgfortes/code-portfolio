import InformationBlock from './InformationBlock';

const InformationSection = React.createClass({
  mixins: [
    React.addons.PureRenderMixin
  ],
  render () {
    let receipt = this.props.receipt;
    return (
      <div className="information-section cf">
        <InformationBlock title="Renter Information" data={receipt.renterInformation}/>
        <InformationBlock title="Vehicle Details" data={receipt.vehicleDetails}/>
        <InformationBlock title="Distance" data={receipt.distance}/>
      </div>
    );
  }
});

module.exports = InformationSection;
