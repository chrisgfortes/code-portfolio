import TotalSummary from './TotalSummary';
import LocationSummary from './LocationSummary';
import PricingController from '../../controllers/PricingController';

const ReceiptRentalSummary = React.createClass({
  mixins: [
    React.addons.PureRenderMixin
  ],
  render () {
    let receipt = this.props.receipt;
    return (
      <div className="receipt-rental-summary cf">
        <div className="receipt-rental_location-summary">
          <div className="location-summary_pickup cf">
            <LocationSummary type={enterprise.i18nReservation.reservationwidget_0008}
                             location={receipt.pickup_location}
                             date={receipt.formattedPickupDate}
                             time={receipt.formattedPickupTime}/>
          </div>
          <div className="location-summary_arrow">
            <i className="icon icon-arrow-general"/>
          </div>
          <div className="location-summary_dropoff cf">
            <LocationSummary type={enterprise.i18nReservation.reservationwidget_0011}
                             location={receipt.return_location}
                             date={receipt.formattedDropoffDate}
                             time={receipt.formattedDropoffTime}/>
          </div>
        </div>
        <div className="receipt-rental_total-summary">
          <div className="total-summary_block">
            <TotalSummary text={enterprise.i18nReservation.receiptsmodal_0004}
                          total={PricingController.getPastReceiptPrices(receipt.price_summary)}/>
          </div>
        </div>
      </div>
    );
  }
});

module.exports = ReceiptRentalSummary;
