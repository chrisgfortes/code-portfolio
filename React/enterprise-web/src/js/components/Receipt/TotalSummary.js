const TotalSummary = React.createClass({
  mixins: [
    React.addons.PureRenderMixin
  ],
  render() {
    return (
      <div className="total-block cf">
        <div className="total_text bold fl">
          {this.props.text}
        </div>
        <div className="total_number bold fr">
          {this.props.total}
        </div>
      </div>
    )
  }
});

module.exports = TotalSummary;
