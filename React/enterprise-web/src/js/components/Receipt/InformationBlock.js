
const InformationBlock = React.createClass({
  mixins: [
    React.addons.PureRenderMixin
  ],
  render () {
    let data = this.props.data;
    return (
      <div className="information-block cf">
        <h2>{this.props.title}</h2>
        {data.map((item, index) => {
          return item.value.length && item.value[0] ?
            <div className="item cf" key={index}>
              <div className="title">{item.title}</div>
              <div className="value">
                {item.value.map((value, idx) => <div key={idx}>{value}</div>)}
              </div>
            </div>
            :
            null
        })}
      </div>
    );
  }
});

module.exports = InformationBlock;
