import GlobalModal from '../Modal/GlobalModal';
import ReceiptController from '../../controllers/ReceiptController';
import Receipt from './Receipt';
import ReservationCursors from '../../cursors/ReservationCursors';

export default function ReceiptModal({receipt}) {
  return (
    <div>
      {receipt.modal &&
        <GlobalModal 
          active={receipt.modal}
          header={"RECEIPT"}
          classLabel="receipt-modal"
          close={_close}
          cursor={ReservationCursors.receiptModal}
          content={
            <Receipt 
              close={_close} 
              loading={receipt.loading} 
              receipt={receipt.details} 
            />
          }
        />
      }
    </div>
  );
}

function _close() {
  ReceiptController.toggleModal(false);
}

ReceiptModal.displayName = 'ReceiptModal';