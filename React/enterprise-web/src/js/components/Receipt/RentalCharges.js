/**
 * I debated making these separate files but I think the components below (ChargesBlock, ChargesTitle, ChargesItem)
 * could actually just be converted to be render methods of RentalCharges, unless someone else
 * disagrees. Will come back to this, time permitting.
 *
 * @todo: fix this file's multiple components in a single component issue
 */
const RentalCharges = React.createClass({
  mixins: [
    React.addons.PureRenderMixin
  ],
  render () {
    let receipt = this.props.receipt;
    return (
      <div className="rental-charges left">
        <h2>{enterprise.i18nReservation.receiptsmodal_0007}</h2>
        <ChargesBlock
          title={enterprise.i18nReservation.reservationnav_0009}
          items={receipt.paymentSection}
          bold={false}
          />
        <ChargesBlock
          items={receipt.totalSection}
          bold={true}/>

      </div>
    );
  }
});

const ChargesBlock = React.createClass({
  mixins: [
    React.addons.PureRenderMixin
  ],
  render () {
    return (
      <div className="charges-block">
        {this.props.title ? <ChargesTitle text={this.props.title}/> : null}
        {this.props.items && this.props.items.map((item, index) => <ChargesItem bold={this.props.bold} item={item} key={index}/>)}
      </div>
    );
  }
});

const ChargesTitle = React.createClass({
  mixins: [
    React.addons.PureRenderMixin
  ],
  render () {
    return (
      <div className="charges-title">
        {this.props.text}
      </div>
    );
  }
});

const ChargesItem = React.createClass({
  mixins: [
    React.addons.PureRenderMixin
  ],
  render () {
    let item = this.props.item;
    return (
      <div className="charges-items">
        <div className={(this.props.bold ? 'bold ' : '') + 'charges-items_item item-title'}>{item.title}</div>
        <div className="charges-items_item item-price">{item.price}</div>
        <div className="charges-items_item item-total">{item.total}</div>
        <div className="charges-items_item item-price mobile">{item.price}</div>
      </div>
    );
  }
});

module.exports = RentalCharges;
