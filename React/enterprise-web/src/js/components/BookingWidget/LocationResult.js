/**
 * LocationResult is used for Airports, Ports, etc (specific locations)
 * @module LocationResult
 */
import ReservationFlowModelController from '../../controllers/ReservationFlowModelController';
import LocationFactory from '../../factories/LocationFactory';
import LocationModel from '../../factories/Location';
import { Debugger } from '../../utilities/util-debug';

export default class LocationResult extends React.Component{
  constructor(props){
    super(props);
    this._selectLocation = this._selectLocation.bind(this);
    // debugging only
    this.isDebug = false;
    this.logger = new Debugger(window._isDebug, this, true, 'LocationResult');
  }
  componentWillReceiveProps (nextProps) {
    if (nextProps.selected) {
      this._selectLocation();
    }
  }
  _selectLocation () {
    ReservationFlowModelController.abortLocationsRequest();
    this.props.blockLocationsRequest(true);

    this.logger.warn('@todo: another layer of locations??? its layer inception over here!!?!???')

    // this is for the branch:E15868 type lookup!!!
    this.props._selectLocation(LocationFactory.getSelectedInputLocation(this.props.location))
  }
  render () {
    let locationName = LocationModel.getLocationName(this.props.location);
    let airportCode = this.props.location.airportCode;

    return (
      <li
        id={`location-${this.props.location.peopleSoftId}`}
        className={this.props.highlighted && 'highlighted'}
        role="option"
        data-location={this.props.location.peopleSoftId}
        data-branch={true}
        onClick={this._selectLocation}
      >
        <a>{locationName}
          <small>{airportCode}</small>
        </a>
      </li>
    );
  }
}
LocationResult.displayName = "LocationResult";
