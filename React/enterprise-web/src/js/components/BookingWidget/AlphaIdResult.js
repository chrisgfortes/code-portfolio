/**
 * @module AlphaIdResult
 * Location Input search results for the Fedex drop-down
 */
import LocationFactory from '../../factories/LocationFactory';
import { LOCATION_SEARCH } from '../../constants';

export default class AlphaIdResult extends React.Component {
  constructor(props){
    super(props);
    this._selectLocation = this._selectLocation.bind(this);
  }
  componentWillReceiveProps (nextProps) {
    if (nextProps.selected) {
      this._selectLocation();
    }
  }
  /**
   * @function _selectLocation
   * @memberOf AlphaIdResult
   * @return {void} calls process
   */
  _selectLocation () {
    this.props.blockLocationsRequest(true);
    console.log('AlphaIdResult._selectLocation()', this.props);
    this.props._selectLocation(
      // this gets two objects, have to extend it a bit
      LocationFactory.getSelectedInputLocation(
        Object.assign({}, this.props,
          {
            id: this.props.alphaId,
            locationName: this.props.alphaId,
            locationType: LOCATION_SEARCH.VALUES.CITY
          }), {
            costCenter: this.props.costCenter
          }
      )
    );
  }
  render () {
    return (
      <li
        key={this.props.key}
        id={`location-${this.props.alphaId}`}
        className={this.props.highlighted && 'highlighted'}
        role="button" tabIndex="0"
        onClick={this._selectLocation} onKeyPress={a11yClick(this._selectLocation)}
      >
        <a>{this.props.alphaId}</a>
      </li>
    );
  }
}

AlphaIdResult.displayName = "AlphaIdResult";
