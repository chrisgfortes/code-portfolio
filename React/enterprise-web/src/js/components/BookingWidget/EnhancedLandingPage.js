import ReservationCursors from '../../cursors/ReservationCursors';
import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import LandingPageMap from './LandingPageMap';
import LandingPageDetails from './LandingPageDetails';
import BookingWidgetModelController from '../../controllers/BookingWidgetModelController';
import BookingWidget from '../BookingWidget/BookingWidget';

const EnhancedLandingPage = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    deepLinkReservation: ReservationCursors.deepLinkReservation,
    pickupTarget: ReservationCursors.pickupTarget,
    mapOfType: ReservationCursors.pickupMap,
    dateOfType: ReservationCursors.pickupDate
  },
  render () {
    const {deepLinkReservation, pickupTarget, mapOfType, dateOfType} = this.state;
    return (
      <div className="book-section">
        {this.props.deepLinkErrors}
        <LandingPageMap deepLinkReservation={deepLinkReservation}/>
        <div className="landing-page-widget">
          <div className="welcome-disclaimer">{i18n('resflowcorporatelocationpage_0001')}</div>
          <BookingWidget landingPage={true} modelController={BookingWidgetModelController}/>
        </div>
        <LandingPageDetails 
          deepLinkReservation={deepLinkReservation} 
          pickupTarget={pickupTarget}
          type="pickup"
          dateOfType={dateOfType}
          mapOfType={mapOfType} />
      </div>
    );
  }
});

module.exports = EnhancedLandingPage;
