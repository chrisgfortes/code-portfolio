import GlobalModal from '../Modal/GlobalModal';
import ConfirmationModalContent from '../Modify/ConfirmationModalContent';
import BookingWidgetModelController from '../../controllers/BookingWidgetModelController';
import { GLOBAL } from '../../constants';

export default class SubmitButton extends React.Component {
  constructor(props){
    super(props);
    this._submit = this._submit.bind(this);
    this.submissionCall = this.submissionCall.bind(this);
    this._confirmationModalCallback = this._confirmationModalCallback.bind(this);
  }
  _submit () {
    let employeeNumber = this.props.employeeNumber;
    let bookingWidgetErrors = this.props.errors;

    // Move focus to global-error-message container when form is submitted w/ errors.
    if ( bookingWidgetErrors ) {
      $('#globalErrorsContainer').focus();
    }
    if (this.props.loyaltyBrand) {
      BookingWidgetModelController.setLoyaltyBrand(this.props.loyaltyBrand)
    }
    if (enterprise.b2b === 'fedex' && (typeof employeeNumber !== 'string' || employeeNumber === '')) {
      const errorCode = GLOBAL.EMPLOYEE_NUMBER_ERROR;
      const error = {
        defaultMessage: i18n('fedexcustompath_0026') || 'Field Required',
        code: errorCode
      };

      let updatedErrors;

      if (Array.isArray(bookingWidgetErrors)) {
        updatedErrors = bookingWidgetErrors;
        updatedErrors.push(error);
      } else {
        updatedErrors = [error];
      }

      this.props.setBookingWidgetUpdatedError(updatedErrors);
    } else if (this.props.checkForExistingCorporateCode()) {
      this.props.clearEmployeeNumberError();
      this.submissionCall();
    }
  }
  submissionCall () {
    if (this.props.inflightModify.isInflight) {
      this.props.toggleInflightModifyModal(true);
    } else {
      this.props.modelController.submit(() => {});
    }
  }

  _confirmationModalCallback () {
    this.props.modelController.submit(() => {});
  }

  render () {
    let domHeaderFooterElement = $('header, footer');
    if (this.props.inflightModify.modal) {
      domHeaderFooterElement.attr('aria-hidden', 'true');
    } else {
      domHeaderFooterElement.removeAttr('aria-hidden');
    }
    return (
      <div className="booking-submit">
        <a
          role="button"
          tabIndex="0"
          onKeyPress={a11yClick(this._submit)}
          onClick={this._submit}
          id="continueButton" className="btn btn-next"
        >
          {enterprise.featuredCityPage ?
            i18n('reservationwidget_0023') :
            i18n('reservationwidget_0014')
          }
        </a>

        {!!this.props.inflightModify.modal &&
          <GlobalModal
            active
            header={i18n('resflowreview_0098')}
            content={
              <ConfirmationModalContent
                confirm={this.props.modelController.submit}/>}
            contentHeader={i18n('resflowreview_0098')}
            contentText={i18n('resflowviewmodifycancel_2006')}
            cursor={['view', 'inflightModify', 'modal']}
          />
        }
      </div>
    );
  }
}

SubmitButton.displayName = "SubmitButton";
