const DateInput = require('./DateInput');
const TimeInput = require('./TimeInput');

const ReservationCursors = require('../../cursors/ReservationCursors');
import ReservationActions from '../../actions/ReservationActions';

const BaobabReactMixinBranch = require('baobab-react/mixins').branch;

const DateTimeControls = React.createClass({
  mixins: [BaobabReactMixinBranch, React.addons.PureRenderMixin],
  cursors: {
    pickupDate: ReservationCursors.pickupDate,
    dropoffDate: ReservationCursors.dropoffDate,
    pickupTempDate: ReservationCursors.pickupTempDate,
    dropoffTempDate: ReservationCursors.dropoffTempDate
  },

  getDefaultProps () {
    return {
      classes: ''
    };
  },

  _onClick (type) {
    ReservationActions.changeView(type, false);
  },

  _getDisplayDate (type) {
    if (this.state[type + 'Date']) {
      return this.state[type + 'Date'];
    } else if (this.state[type + 'TempDate']) {
      return this.state[type + 'TempDate'];
    } else {
      return null;
    }
  },

  render () {
    // @TODO: do something real with this...
    let childItems = [];
    React.Children.map(
      this.props.children,
      function(child) {
        childItems.push(child);
        return {child};
      }
    );

    return (
      <div className="date-time-selector cf">
        <div className="cf pickup label-container">
          <div className="date-time-label"> {enterprise.i18nReservation.reservationwidget_0008} </div>
          <DateInput onClick={this._onClick} type="pickupCalendar" date={this._getDisplayDate('pickup')}/>
          <TimeInput modelController={this.props.modelController} type="pickup"/>
          <div className="error-modal">{childItems[0]}</div>
        </div>
        <div className="arrow" aria-hidden="true">&rarr;</div>
        <div className="cf dropoff label-container">
          <div className="date-time-label"> {enterprise.i18nReservation.reservationwidget_0011} </div>
          <DateInput onClick={this._onClick} type="dropoffCalendar" date={this._getDisplayDate('dropoff')}/>
          <TimeInput modelController={this.props.modelController} type="dropoff"/>
          <div className="error-modal">{childItems[1]}</div>
        </div>
      </div>
    );
  }
});

module.exports = DateTimeControls;
