import ReactDOM from 'react-dom';
import ReservationActions from '../../actions/ReservationActions';

const Day = React.createClass({
  mixins: [React.addons.PureRenderMixin],

  _onClick (event) {
    event.stopPropagation();
    if (this.props.day.options.indexOf('disabled') === -1 && this.props.day.options.indexOf('closed') === -1) {
      this.props.modelController.setDate(this.props.day.day, this.props.type);
    }
  },

  _onMouseEnter () {
    if (this.props.day.options.indexOf('disabled') === -1 && this.props.day.options.indexOf('closed') === -1) {
      ReservationActions.setTempDate(this.props.day.day, this.props.type);
    }
  },

  _onMouseLeave () {
    ReservationActions.setTempDate(null, this.props.type);
  },

  componentDidMount () {
    if (!!~this.props.day.classes.indexOf('selected')) {
      ReactDOM.findDOMNode(this).focus();
    }
  },

  render () {
    const isDisabled = this.props.day.options.indexOf('disabled') !== -1;

    return (
      <button
        className="day"
        ref="dayDiv"
        aria-disabled={isDisabled.toString()}
        disabled={isDisabled}
        tabIndex={isDisabled ? '-1' : '0'}
        onKeyPress={a11yClick(this._onClick)}
        onClick={this._onClick}
      >
        <span className={`day-number ${this.props.day.classes}`}>{this.props.day.day.date()}</span>
      </button>
    );
  }
});

module.exports = Day;
