const ReservationCursors = require('../../cursors/ReservationCursors');
const TimePicker = require('./TimePicker');
const BaobabReactMixinBranch = require('baobab-react/mixins').branch;

const TimeInput = React.createClass({

  mixins: [BaobabReactMixinBranch, React.addons.PureRenderMixin],
  cursors: {
    pickupTime: ReservationCursors.pickupTime,
    pickupTimeInitial: ReservationCursors.pickupTimeInitial,
    dropoffTime: ReservationCursors.dropoffTime,
    dropoffTimeInitial: ReservationCursors.dropoffTimeInitial
  },

  componentDidMount () {
    // Solves problem of non-visible focus on hidden select
    const labelClasses = '.' + this.props.type + 'Time-label';
    $(labelClasses + ' select').on('focus blur', () => {
      $(labelClasses).toggleClass('focus');
    });
  },
  componentWillUnmount () {
    const labelClasses = '.' + this.props.type + 'Time-label';
    $(labelClasses + ' select').off('focus blur');
  },

  _onClick: function () {
    if (this.props.onClick) {
      this.props.onClick(this.props.type + 'Time');
    }
    ReservationStateTree.set(['view', this.props.type, 'time', 'initial'], false);
  },

  renderWithView: function () {
    let time = this.state[this.props.type + 'Time'].value;
    let timeObj = time ? moment(time, 'hh:mm') : '';
    let output;

    if (timeObj) {
      if (enterprise.i18nUnits.ampm === 'false') {
        output = (
          <div>
            <input type="hidden" id={this.props.type + 'TimeInput'} value={this.props.value} className="time"/>
            <span className="hour">{timeObj ? timeObj.format(enterprise.i18nUnits.hour) : ''}</span>
            <span className="min">:{timeObj ? timeObj.format(enterprise.i18nUnits.minute) : ''}</span>
            <span className="ampm"></span>
            <i className="icon icon-nav-carrot-down"></i>
          </div>
        );
      } else {
        output = (
          <div>
            <input type="hidden" id={this.props.type + 'TimeInput'} value={this.props.value} className="time"/>
            <span className="hour">{timeObj ? timeObj.format(enterprise.i18nUnits.hour) : ''}</span>
            <span className="min">:{timeObj ? timeObj.format(enterprise.i18nUnits.minute) : ''}</span>
            <span className="ampm">{timeObj ? timeObj.format(enterprise.i18nUnits.ampm) : ''}</span>
            <i className="icon icon-nav-carrot-down"></i>
          </div>
        );
      }
    } else {
      output = (
        <div className="empty-date-time-control">
          <i className="icon icon-nav-time-green default-icon"></i>
          <span>{enterprise.i18nReservation.reservationwidget_0010}</span>
          <i className="icon icon-nav-carrot-down"></i>
        </div>
      );
    }

    return output;
  },

  render: function () {
    let type = this.props.type + 'Time';
    let labelClasses = 'time-label ' + type + '-label';
    let name;

    if (this.props.type === 'pickup') {
      name = i18n('reservationwidget_0046') || 'Pick-Up Time Picker';
    } else if (this.props.type === 'dropoff') {
      name = i18n('reservationwidget_0047') || 'Return Time Picker';
    }

    return (
      <label onKeyPress={a11yClick(this._onClick)} onClick={this._onClick} className={labelClasses} htmlFor={type}>
        {this.renderWithView()}
          <TimePicker accessibilityName={name} modelController={this.props.modelController} type={this.props.type} />
      </label>
    );
  }
});

module.exports = TimeInput;
