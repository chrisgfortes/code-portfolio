const BaobabReactMixinBranch = require('baobab-react/mixins').branch;

const BookingWidget = require('./BookingWidget');
const ExistingReservation = require('../ExistingReservation/ExistingReservation');
import ReservationActions from '../../actions/ReservationActions';
const ReservationCursors = require('../../cursors/ReservationCursors');

const ReservationToggle = React.createClass({
  mixins: [BaobabReactMixinBranch, React.addons.PureRenderMixin],
  getInitialState () {
    return {
      activeTab: this.props.active
    };
  },
  componentDidMount () {
    if (enterprise.collapsed === false || this.props.shouldStartExpanded) {
      ReservationActions.setBookingWidgetExpanded(true);
    }
  },
  cursors: {
    expanded: ReservationCursors.bookingWidgetExpanded
  },
  _onClick () {
    if (this.state.activeTab === 'book') {
      this.setState({
        activeTab: 'existing'
      });
    } else {
      this.setState({
        activeTab: 'book'
      });
    }
  },
  render () {
    const lowerCaseHeader = (
      enterprise.featuredCityPage ?
        i18n('featuredcitypage_0001') :
        i18n('reservationwidget_0001')
    );
    const startReservationHeader = (
      enterprise.resWidgetStartTitle ?
        enterprise.resWidgetStartTitle :
        lowerCaseHeader
    );
    const header = (
      this.state.activeTab === 'book' ?
        startReservationHeader :
        i18n('reservationwidget_0003')
    );
    const tabTitle = (
      this.state.activeTab === 'book' ?
        i18n('reservationwidget_0003') :
        startReservationHeader
    );
    const book = (
      <div id="book" className="show">
        <BookingWidget loyaltyBrand={this.props.loyaltyBrand} modelController={this.props.modelController}/>
      </div>
    );
    const existing = (
      <div id="existing" className="show expanded">
        <ExistingReservation modelController={this.props.modelController}/>
      </div>
    );
    const headerClasses = 'left alpha reservation-toggle-header';
    const headerHTML = (
      enterprise.heading === 'H1' ?
        <h1 className={headerClasses}>{header}</h1> :
        <h2 className={headerClasses}>{header}</h2>
    );

    return (
      <div>
        {this.props.deepLinkErrors}
        <div className={'inner-container ' + (this.state.expanded ? 'expanded' : 'collapsed')}>
          <div className="cf">
            {headerHTML}
            <div className="options right">
              <span className="text">{i18n('resflowlocations_0041')} &nbsp;
                <a id="viewModifyCancelBookingWidget" className="green"
                   onClick={this._onClick} onKeyPress={a11yClick(this._onClick)} tabIndex="0">
                  <span className="active">{tabTitle}</span>
                </a>
              </span>
            </div>
          </div>
          {(this.state.activeTab === 'book') ? book : existing}
        </div>
      </div>
    );
  }
});

module.exports = ReservationToggle;
