const ReservationCursors = require('../../cursors/ReservationCursors');
const TimePickerMobile = require('./TimePickerMobile');
const BaobabReactMixinBranch = require('baobab-react/mixins').branch;

const TimePicker = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    pickupClosedTime: ReservationCursors.pickupClosedTime,
    dropoffClosedTime: ReservationCursors.dropoffClosedTime,
    pickupTimeValue: ReservationCursors.pickupTimeValue,
    dropoffTimeValue: ReservationCursors.dropoffTimeValue,

    pickupDateValue: ReservationCursors.pickupDate,
    dropoffDateValue: ReservationCursors.dropoffDate,

    pickupDetails: ReservationCursors.pickupLocationDetails,
    dropoffDetails: ReservationCursors.dropoffLocationDetails,

    dropoffAfterHour: ReservationCursors.dropoffAfterHour

  },
  getInitialState: function () {
    return {
      timePickerConfig: {
        currentItem: 26,
        startingItem: 26,
        currentBreakpoint: 10,
        breakpoints: {
          small: 4,
          medium: 8,
          large: 10,
          normal: 10
        },
        timeSection: {
          morning: 14,
          afternoon: 24,
          evening: 34,
          latenight: 44
        },
        timeArray: []
      }
    };
  },
  generateTimeArray: function () {
    let config = this.state.timePickerConfig;
    let closedTimes = this.state[this.props.type + 'ClosedTime'];
    let timeArray = [];

    for (let i = 0; i < 48; i++) {
      let hour = Math.floor(i / 2);
      let minutes = (i % 2 === 0) ? '00' : '30';

      timeArray.push({
        state: null,
        time: moment(hour + ':' + minutes, 'H:mm'),
        offset: 0
      });

      if (i === config.startingItem) {
        timeArray[i].starting = true;
      }
    }

    if (closedTimes && closedTimes.length > 0) {
      timeArray = this.setTimeArrayPanels(timeArray);
    }

    timeArray = this.setTimeArrayOffsets(timeArray);

    return timeArray;
  },
  appendLeadingZero: function (time) {
    return time.length < 4 ? '0' + time : time;
  },
  setTimeArrayPanels: function (timeArray) {
    let closedTimes = this.state[this.props.type + 'ClosedTime'];
    let openCloseTimes = [];
    let closedAllDay = false;
    let dropoffAfterHour = this.props.type === 'dropoff' && this.state.dropoffAfterHour.allowed;

    if (closedTimes.length === 1) {
      if (closedTimes[0].open === '0' && closedTimes[0].close === '0') {
        closedAllDay = true;
        closedTimes = [{
          open: '2359',
          close: '0000'
        }];
      }
    }

    for (let i = 0, len = closedTimes.length; i < len; ++i) {
      closedTimes[i].open = this.appendLeadingZero(closedTimes[i].open);
      closedTimes[i].close = this.appendLeadingZero(closedTimes[i].close);

      let openTime = moment(closedTimes[i].open, 'H:mm');
      let closeTime = closedTimes[i].close === '00:00' ? moment('23:59', 'H:mm') : moment(closedTimes[i].close, 'H:mm');
      let openTimeIndex = (openTime.minute() >= 30) ? 2 * openTime.hour() + 1 : 2 * openTime.hour();
      let closeTimeIndex = (closeTime.minute() >= 30) ? 2 * closeTime.hour() + 1 : 2 * closeTime.hour();

      for (let j = openTimeIndex, end = closeTimeIndex; j <= end; j++) {
        timeArray[j].state = 'enabled';
      }
      openCloseTimes.push(openTimeIndex, closeTimeIndex);
    }

    if (openCloseTimes.length > 0) {
      if (openCloseTimes[openCloseTimes.length - 1] < 47) {
        openCloseTimes.push(48, 0);
      }

      let leading = true;
      let leadingCount = 0;
      let closedTimeBlocks = 0;

      for (let i = 0, len = timeArray.length; i < len; i++) {
        if (dropoffAfterHour) {
          if (timeArray[i].state !== 'enabled') {
            timeArray[i].state = 'afterHour';
          }
        } else {
          if (timeArray[i].state !== 'enabled' && leading) {
            //FIX BELOW!!!
            //TODO :: Adjust for triple timeframes
            let disabledIndex = openCloseTimes[closedTimeBlocks * 2] - 1;
            let adjustedIndex = disabledIndex > 0 ? disabledIndex : 0;
            let disabledEndTime = closedAllDay ? moment('24:00', 'H:mm')
                : timeArray[adjustedIndex].time.clone();

            timeArray[i].state = 'disabledLeadingPanel';
            timeArray[i].disabledStartTime = closedAllDay ? moment('00:00', 'H:mm') : timeArray[i].time;
            timeArray[i].disabledEndTime = disabledEndTime > 0 ? disabledEndTime.add(29, 'minutes') : 0;

            leadingCount++;
            closedTimeBlocks++;

            leading = false;
          } else if (timeArray[i].state === 'enabled') {
            leading = true;
          } else {
            timeArray[i].state = 'disabled';
          }
        }
      }
    }

    return timeArray;
  },
  setTimeArrayOffsets: function (timeArray) {
    let totalOffset = 0;
    let pickupDate = this.state.pickupDateValue;
    let dropoffDate = this.state.dropoffDateValue;
    let currentTypeDateValue = this.state[this.props.type + 'DateValue'];

    for (let i = 1, len = timeArray.length; i < len; i++) {
      switch (timeArray[i].state) {
        //Add 65 offset if this is the start of the disabled block
        case 'disabledLeadingPanel' :
          timeArray[i].offset = totalOffset + 65;
          break;
        case 'disabled' :
          //Adding (4*65) offset if this is the last panel of the disabled block
          if (i + 1 < timeArray.length && timeArray[i + 1].state !== 'disabled') {
            timeArray[i].disabledEndTime = timeArray[i].time;
            totalOffset += 260;
          }
          timeArray[i].offset = timeArray[i - 1].offset;
          break;
        //Adding 65 as default
        default:
          totalOffset += 65;
          timeArray[i].offset = totalOffset;
      }

      if (this.props.type === 'dropoff' && dropoffDate && pickupDate && dropoffDate.isSame(pickupDate)) {
        if (this.state.pickupTimeValue &&
          (timeArray[i].time.diff(this.state.pickupTimeValue, 'hours', true) < 2 ||
          timeArray[i].time.isBefore(this.state.pickupTimeValue))
        ) {
          timeArray[0].notallowed = true;
          timeArray[i].notallowed = true;
        }
      }

      if (currentTypeDateValue && currentTypeDateValue.format('DDMMYY') === moment().format('DDMMYY') && timeArray[i].time.isBefore(moment())) {
        timeArray[0].notallowed = true;
        timeArray[i].notallowed = true;
      }
    }

    return timeArray;
  },
  setConfig: function (variable, value) {
    //Need React Addons for intelligent data mutation; default set state replaces instead of merges
    let newState = React.addons.update(this.state, {
      timePickerConfig: {
        [variable]: {$set: value}
      }
    });

    this.setState(newState);
  },
  render () {
    let timeArray = this.generateTimeArray();

    return (
      <TimePickerMobile
        accessibilityName={this.props.accessibilityName}
        modelController={this.props.modelController}
        type={this.props.type}
        aria={this.props.accessibilityName}
        timeArray={timeArray}/>
    );
  }
});

module.exports = TimePicker;
