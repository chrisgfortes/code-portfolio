import BookingWidgetModelController from '../../controllers/BookingWidgetModelController';
export default class LandingPageMap extends React.Component{
  constructor() {
    super()
  }
  componentDidMount () {
    BookingWidgetModelController.loadMap(this.props.deepLinkReservation.reservation);
  }
  render () {
    let request = this.props.deepLinkReservation.reservation.pickup_location;
    if (!request) {
      return false;
    }
    return (
      <div className="landing-page-map">
        <div id="map-canvas"/>
      </div>
    );
  }
}

LandingPageMap.displayName = "LandingPageMap";
