/**
 * Location Search for the Booking Widget
 * @module LocationSearch
 */
import ReservationCursors from '../../cursors/ReservationCursors';
import LocationInput from './LocationInput';
import FedexLocationInput from './FedexLocationInput';
import FedexController from '../../controllers/FedexController';
import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import ReservationFlowModelController from '../../controllers/ReservationFlowModelController';
import { Enterprise } from '../../modules/Enterprise';
import { Debugger } from '../../utilities/util-debug';
import { LOCATION_SEARCH } from '../../constants';

let enterprise = Enterprise.global;
const isFedex = (() => FedexController.isFedexFlow())();

const LocationSearch = React.createClass({
  mixins: [BaobabReactMixinBranch, React.addons.PureRenderMixin],
  getDefaultProps () {
    return {
      isFeaturedCityPage: !!enterprise.featuredCityPage,
      isFedexLandingPage: isFedex
    };
  },
  getInitialState () {
    return {
      editable: false
    };
  },
  componentWillMount() {
    this.isDebug = false;
    this.logger = new Debugger(window._isDebug, this, true, 'LocationSearch');

    this.logger.warn('componentWillMount()', this.props.modelController);
  },
  cursors: {
    sameLocation: ReservationCursors.sameLocation,
    pickupLocationDetails: ReservationCursors.pickupLocationDetails,
    geolocationError: ReservationCursors.geolocationError,
    locationView: ReservationCursors.locationView,
    externalBookingLocation: ReservationCursors.externalBookingLocation,
    pickupModel: ReservationCursors.pickupModel,
    dropoffModel: ReservationCursors.dropoffModel,
    // model: ReservationCursors.model,
    // view: ReservationCursors.view,
    pickupLocation: ReservationCursors.pickupLocationObj,
    modelNoResults: ReservationCursors.modelNoResults
  },
  _dropdownClasses (bool) {
    let classes = 'cf ';
    if (bool) {
      classes += 'is-hidden';
    }
    return classes;
  },

  /**
   * Important thing to note is that in the context of Location Search, this
   * method is overridden in LocationSearch > Header._handleSelect()
   * @param  {object} selection location object
   * @param  {string} type      pickup / dropoff etc.
   * @return {void}             just sets location values
   * @memberOf BookingWidget/LocationSearch
   */
  _handleSelect (selection, type) {
    this.logger.warn('_handleSelect(selection)', selection);
    this.logger.log('when fedex we are not passing up a model that includes costCenter');
    if (this.props.modelController) {
      this.logger.log('modelController selectLocationFromInput?', this.props.modelController);
      this.props.modelController.selectLocationFromInput(selection, type, this.state.sameLocation);
    }
    ReservationFlowModelController.clearLocationResults(type);
    this.clearErrorsForComponent('bookingWidget');
  },
  _differentLocation () {
    this.logger.warn('_differentLocation()');

    const $resWidget = $('#reservationWidget');
    const checked = this.state.sameLocation;

    if (this.props.isFeaturedCityPage) {
      this.props.featuredWidgetExpand();
    }

    ReservationFlowModelController.setSameLocation(!checked);
    this.setBookingWidgetExpanded(true);

    if ($resWidget.length) {
      $('html, body').animate({scrollTop: ($resWidget.offset().top - 40)}, 800);
    }
  },
  // @todo: these to not need to be local methods, we could often call the controller direct
  clearErrorsForComponent(component){
    ReservationFlowModelController.clearErrorsForComponent(component);
  },
  setBookingWidgetExpanded(bool){
    ReservationFlowModelController.setBookingWidgetExpanded(bool);
  },
  blockLocationsRequest (bool) {
    ReservationFlowModelController.blockLocationsRequest(bool);
  },
  setLocationView (type) {
    ReservationFlowModelController.setLocationView(type);
  },
  setCurrentView (type) {
    ReservationFlowModelController.setCurrentView(type);
  },
  render () {
    const {sameLocation, pickupLocationDetails, geolocationError, locationView, externalBookingLocation,
    pickupModel, dropoffModel, pickupLocation, modelNoResults} = this.state;
    let editable = !!(!this.props.landingPage || this.state.editable);
    let modelType;
    if (this.props.type) {
      modelType = this.props.type === LOCATION_SEARCH.PICKUP ? pickupModel : dropoffModel;
    }
    return (
      <div className={`${this.props.isLocationHeaderSearch ? '' : 'location-search'}`}>
        {(!editable && !this.props.isLocationHeaderSearch) &&
          <div className="landing-page-header">
            <h2>{pickupLocationDetails.name}</h2>

            <div className="editable" onClick={()=> this.setState({editable: true})}>
              {i18n('reservationnav_0029')}
            </div>
          </div>
        }

        {(this.props.isFedexLandingPage || this.props.isFedexReservation) ?
          <div className="cf pick-up-location">
            <FedexLocationInput
              type="pickup"
              _handleSelect={this.props._handleSelect || this._handleSelect}
              locationView={locationView}
              sameLocation={sameLocation}
              pickupLocation={pickupLocation}
              modelNoResults={modelNoResults}
              blockLocationsRequest={this.blockLocationsRequest}
              setBookingWidgetExpanded={this.setBookingWidgetExpanded} />
          </div>
          :
          <div>
            <div className={`${this.props.isLocationHeaderSearch ? '' : 'cf pick-up-location'}`}>
              <LocationInput
                modelController={this.props.modelController}
                type={this.props.type || LOCATION_SEARCH.PICKUP}
                modelType={modelType || pickupModel}
                editable={editable}
                _handleSelect={this.props._handleSelect || this._handleSelect}
                _differentLocation={this.props.isLocationHeaderSearch ? '' : this._differentLocation}
                _handleNearbyLocations={this.props.isLocationHeaderSearch ? this.props._handleNearbyLocations : ''}
                featuredWidgetExpand={this.props.isLocationHeaderSearch ? '' : this.props.featuredWidgetExpand}
                labelIdSuffix={this.props.isLocationHeaderSearch ? '' : '-pickup'}
                hideLabel={this.props.isLocationHeaderSearch ? true : false}
                locationView={locationView}
                sameLocation={sameLocation}
                externalBookingLocation={externalBookingLocation}
                canUseMyLocation= {this.props.isLocationHeaderSearch ? false : true}
                clearErrorsForComponent={this.clearErrorsForComponent}
                blockLocationsRequest={this.blockLocationsRequest}
                setBookingWidgetExpanded={this.setBookingWidgetExpanded}
                geolocationError={geolocationError} />
            </div>
            {!this.props.isLocationHeaderSearch &&
              <div className={this._dropdownClasses(sameLocation)}>
                <LocationInput
                  modelController={this.props.modelController}
                  type="dropoff"
                  modelType={dropoffModel}
                  _handleSelect={this._handleSelect}
                  labelIdSuffix="-dropoff"
                  locationView={locationView}
                  sameLocation={sameLocation}
                  externalBookingLocation={externalBookingLocation}
                  featuredWidgetExpand={this.props.featuredWidgetExpand}
                  canUseMyLocation= {true}
                  clearErrorsForComponent={this.clearErrorsForComponent}
                  blockLocationsRequest={this.blockLocationsRequest}
                  setBookingWidgetExpanded={this.setBookingWidgetExpanded}
                  geolocationError={geolocationError} />
              </div>
            }
          </div>
        }

      </div>
    );
  }
});

module.exports = LocationSearch;
