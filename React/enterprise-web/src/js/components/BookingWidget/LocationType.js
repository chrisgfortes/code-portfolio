/**
 * LocationCity is used for search results for city searching
 * @module LocationCity
 */
import LocationFactory from '../../factories/LocationFactory';
import LocationController from '../../controllers/LocationController';
import { LOCATION_SEARCH } from '../../constants';

export default class LocationType extends React.Component{
  constructor(props){
    super(props);
    this._selectLocation = this._selectLocation.bind(this);
  }
  componentWillReceiveProps (nextProps) {
    if (nextProps.selected) {
      this._selectLocation();
    }
  }
  _selectLocation () {
    this.props._selectLocation(LocationFactory.getSelectedInputLocation(this.props.city));
  }
  render () {

    return (
      <li
        id={this.props.city.type === LOCATION_SEARCH.STRICT_TYPE.CITY ? `location-${this.props.city.cityId}` : `location-${this.props.city.countryCode}`}
        className={this.props.highlighted && 'highlighted'}
        role="option"
        onClick={this._selectLocation}
      >
        <a>{LocationController.getSelectedLocationNameByType(this.props.city)}</a>
      </li>
    );
  }
}
LocationType.displayName = "LocationType";
