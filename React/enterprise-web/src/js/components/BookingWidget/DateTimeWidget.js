import DateTimePicker from './DateTimePicker';
import SubmitButton from './SubmitButton';
import Spinnerprise from '../Spinner/Spinnerprise';
import Age from './Age';
import Coupon from './Coupon';
import Error from '../Errors/Error';
import Modals from '../Corporate/ActionModals';
import ReservationCursors from '../../cursors/ReservationCursors';
import GlobalModal from '../Modal/GlobalModal';
import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import BookingWidgetModelController from '../../controllers/BookingWidgetModelController';

const DateTimeWidget = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  getDefaultProps: function () {
    return {
      modelController: null
    };
  },
  cursors: {
    errors: ReservationCursors.bookingWidgetErrors,
    loading: ReservationCursors.bookingWidgetLoading,
    modify: ReservationCursors.modify,
    deepLinkReservation: ReservationCursors.deepLinkReservation,
    prepopulatedCoupon: ReservationCursors.prepopulatedCoupon,
    profile: ReservationCursors.profile,
    sessionContractDetails: ReservationCursors.sessionContractDetails,
    coupon: ReservationCursors.coupon,
    expeditedCoupon: ReservationCursors.expeditedCoupon,
    age: ReservationCursors.age,
    ageRange: ReservationCursors.ageRange,
    inferredAge: ReservationCursors.inferredAge,
    agePolicyModal: ReservationCursors.agePolicyModal,
    inflightModify: ReservationCursors.inflightModify,
    employeeNumber: ReservationCursors.employeeNumber
  },
  facets: {
    agePolicy: 'agePolicy'
  },
  setBookingWidgetUpdatedError (error) {
    BookingWidgetModelController.setBookingWidgetUpdatedError(error);
  },
  checkForExistingCorporateCode () {
    return BookingWidgetModelController.checkForExistingCorporateCode();
  },
  clearEmployeeNumberError () {
    BookingWidgetModelController.clearEmployeeNumberError();
  },
  toggleInflightModifyModal (bool) {
    BookingWidgetModelController.toggleInflightModifyModal(bool);
  },
  getCouponDetails(deepLinkReservation, profile, coupon, contractDetails, prepopulatedCoupon) {
    return BookingWidgetModelController.getCouponDetails(deepLinkReservation, profile, coupon, contractDetails, prepopulatedCoupon);
  },
  render: function () {
    const {errors, coupon, loading, deepLinkReservation, prepopulatedCoupon, expeditedCoupon,
      profile, employeeNumber, age, ageRange, modify, inferredAge, 
      agePolicyModal, inflightModify, agePolicy, sessionContractDetails} = this.state;
    const couponDetails = this.getCouponDetails(deepLinkReservation, profile, coupon, sessionContractDetails, prepopulatedCoupon);
    return (
      <div className="booking-widget date-time-widget">
        {this.props.deepLinkErrors}
        <Error errors={errors} type="GLOBAL"/>
        <h1>{enterprise.i18nReservation.resflowviewmodifycancel_1016}</h1>
        <DateTimePicker modelController={this.props.modelController} isBookingController={this.props.isBookingController} />

        <Age 
          modify={modify}
          age={age}
          ageRange={ageRange}
          inferredAge={inferredAge}
          agePolicyModal={agePolicyModal}
          agePolicy={agePolicy} />

        <Coupon 
          modify={modify}
          expeditedCoupon={expeditedCoupon}
          coupon={coupon}
          placeholder={couponDetails && couponDetails.placeholder}
          isRemovable={couponDetails && couponDetails.isRemovable}/>

        <SubmitButton 
          modelController={this.props.modelController}
          inflightModify={inflightModify}
          employeeNumber={employeeNumber}
          errors={errors}
          setBookingWidgetUpdatedError={this.setBookingWidgetUpdatedError}
          checkForExistingCorporateCode={this.checkForExistingCorporateCode}
          clearEmployeeNumberError={this.clearEmployeeNumberError}
          toggleInflightModifyModal={this.toggleInflightModifyModal}/>
        {loading &&
        <GlobalModal active={loading}
                     header=""
                     content={<Spinnerprise />}
                     classLabel="spinner-class"/>
        }
        <Modals modelController={this.props.modelController} />
      </div>
    );
  }
});

module.exports = DateTimeWidget;
