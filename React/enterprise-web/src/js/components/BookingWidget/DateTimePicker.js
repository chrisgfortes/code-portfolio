import DateTimeControls from './DateTimeControls';
import DatePicker from './DatePicker';
import Spinnerprise from '../Spinner/Spinnerprise';
import ReservationActions from '../../actions/ReservationActions';
import ReservationCursors from '../../cursors/ReservationCursors';
import GlobalModal from '../Modal/GlobalModal';
import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import { Debugger } from '../../utilities/util-debug';

const DateTimePicker = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  getInitialState () {
    return null;
  },
  componentWillMount () {
    this.logger = new Debugger(window._isDebug, this, true, 'DateTimePicker.js');
  },
  componentDidMount () {
    //TODO: Stop going up DOM to test this and implement a bounding box solution to show/hide components
    $(document.body).on('click.datetime, keyup.datetime', this._handleClose);
  },
  componentWillUnmount () {
    $(document.body).off('click.datetime, keyup.datetime');
  },
  cursors: {
    currentView: ReservationCursors.currentView,
    pickupDate: ReservationCursors.pickupDate,
    dropoffDate: ReservationCursors.dropoffDate
  },
  isDebug: false,
  logger: {},
  _handleClose (event) {
    const $target = $(event.target);
    const closableViews = ['pickupCalendar', 'dropoffCalendar', 'pickupTime', 'dropoffTime', 'age'];
    const isCurrentViewClosable = closableViews.indexOf(this.state.currentView) !== -1;

    if (isCurrentViewClosable) {
      const isClickOutsideView = (event.type === 'click' && $target.closest('.date-time').length === 0);
      if (event.keyCode === 27 || isClickOutsideView) {
        ReservationActions.changeView(null, false);
      }
    }
  },
  render () {
    this.logger.log('render()');
    const view = this.state.currentView;

    this.logger.debug('CURRENT VIEW: ', this.state.currentView);

    return (
      <div className={`cf date-time-form ${this.state.currentView}-active`}>
        <DateTimeControls modelController={this.props.modelController}>
          {this.props.children}
        </DateTimeControls>

        <div className="date-time">
          <div className="pickup-calendar">
            {view === 'pickupCalendar' &&
              <DatePicker modelController={this.props.modelController} type="pickup" isBookingController={this.props.isBookingController} />
            }
            {this.state.pickupCalendarLoading &&
              <GlobalModal active={this.state.loading}
                     header=""
                     content={<Spinnerprise modelController={this.props.modelController}/>}
                     classLabel="spinner-class"/>
            }
          </div>

          <div className="dropoff-calendar">
            {view === 'dropoffCalendar' &&
              <DatePicker modelController={this.props.modelController} type="dropoff" isBookingController={this.props.isBookingController}/>
            }
            {this.state.dropoffCalendarLoading &&
              <GlobalModal active={this.state.loading}
                     header=""
                     content={<Spinnerprise modelController={this.props.modelController} />}
                     classLabel="spinner-class"/>
            }
          </div>

        </div>
      </div>
    );
  }
});

module.exports = DateTimePicker;
