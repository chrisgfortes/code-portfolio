import PreviousLocationResult from './PreviousLocationResult';
export default function PreviouslySelectedLocations ({type, _selectLocation, locations, highlightedItem, selectedItem}) {
  return (
    <div className="location-group" role="presentation">
      <i className="icon icon-location-recent" role="presentation"/>
      <div className="location-group-label" role="presentation">
        {i18n('reservationwidget_2005') || 'RECENT'}
      </div>
      <ul role="presentation">
        {locations && locations.map(
          (locationObj) => {
            return (
              <PreviousLocationResult
                key={locationObj.locationId}
                location={locationObj}
                type={type}
                _selectLocation={_selectLocation}
                highlightedItem={highlightedItem}
                selected={selectedItem === locationObj.locationId}
              />
            );
          }
        )}
      </ul>
    </div>
  );
}

PreviouslySelectedLocations.displayName = "PreviouslySelectedLocations";