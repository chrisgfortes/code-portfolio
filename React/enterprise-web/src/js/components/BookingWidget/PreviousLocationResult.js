export default class PreviousLocationResult extends React.Component{
  constructor(props){
    super(props);
    this._selectLocation = this._selectLocation.bind(this);
  }
  componentWillReceiveProps (nextProps) {
    if (nextProps.selected) {
      this._selectLocation();
    }
  }
  _selectLocation () {
    this.props._selectLocation({
      key: this.props.location.locationId,
      locationName: this.props.location.locationName,
      type: this.props.location.locationType,
      longitude: this.props.location.longitude,
      lat: this.props.location.lat
    });
  }

  render () {
    const location = this.props.location;
    const isHighlighted = this.props.highlightedItem === location.locationId;

    return (
      <li
        onClick={this._selectLocation}
        id={`location-${location.locationId}`}
        className={isHighlighted && 'highlighted'}
        role="option"
        >
        <a>{location.locationName}
          <small>{location.airportCode}</small>
        </a>
      </li>
    );
  }
}

PreviousLocationResult.displayName = "PreviousLocationResult";