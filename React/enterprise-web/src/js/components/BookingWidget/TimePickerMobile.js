const ReservationCursors = require('../../cursors/ReservationCursors');
import ReservationActions from '../../actions/ReservationActions';
const BaobabReactMixinBranch = require('baobab-react/mixins').branch;

const TimePickerMobile = React.createClass({
  mixins: [BaobabReactMixinBranch, React.addons.PureRenderMixin],
  cursors: {
    currentView: ReservationCursors.currentView,
    pickupTimeValue: ReservationCursors.pickupTimeValue,
    dropoffTimeValue: ReservationCursors.dropoffTimeValue
  },
  /*determineDevice () {
   let timePicker = document.getElementById(this.props.type + 'Time'),
   userAgent = navigator.userAgent || navigator.vendor || window.opera;
   //
   //if (userAgent.match(/Android/i)) {
   //    setTimeout(function () {
   //        if (document.createEvent) {
   //            let e = document.createEvent("MouseEvents");
   //            e.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
   //            timePicker.dispatchEvent(e);
   //        }
   //    }, 0);
   //}
   //else {
   //    if (document.createEvent) {
   //        let e = document.createEvent("MouseEvents");
   //        e.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
   //        timePicker.dispatchEvent(e);
   //    }
   //}

   setTimeout(function () {
   if (document.createEvent) {
   let e = document.createEvent("MouseEvents");
   e.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
   timePicker.dispatchEvent(e);
   }
   }, 0);

   },*/
  _onChange (event) {
    let target = event.target.value;
    let time = moment(target, 'LT');
    ReservationActions.changeView(null, false);
    this.props.modelController.setTime(this.props.type, 'value', time);
  },
  render () {
    let selected = this.state[this.props.type + 'TimeValue'];
    let formatted = selected ? selected.format('LT') : false;
    let selectLabel = (this.props.type === 'pickup') ? enterprise.i18nReservation.reservationwidget_5007 : enterprise.i18nReservation.reservationwidget_5006;

    return (
      <select name={this.props.accessibilityName}
              onKeyPress={(e) => {e.target.click();}} onChange={this._onChange}
              value={formatted || 'default'}
              id={this.props.type + 'Time'}
              aria-label={this.props.aria}>
        <option value="default" disabled={true}>{selectLabel}</option>
        {this.props.timeArray.map((item, index) => {
          let time = item.time.format('LT');
          let disabled = item.state === 'disabled' || item.state === 'disabledLeadingPanel';
          let status = null;

          if (item.state === 'afterHour') {
            status = ' (' + enterprise.i18nReservation.locations_0011 + ') ';
          }

          if (disabled) {
            status = ' (' + enterprise.i18nReservation.resflowlocations_0004 + ') ';
          }

          return (
            <option key={index}
                    disabled={disabled}
                    value={time}>{status ? time + status : time}</option>
          );
        })}
      </select>
    );
  }
});

module.exports = TimePickerMobile;
