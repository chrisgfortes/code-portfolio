import ReactDOM from 'react-dom';
import AlphaIdResult from './AlphaIdResult';

export default class FedexLocationDropdown extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hoverClass: ''
    };
    this._selectLocation = this._selectLocation.bind(this);
    this.setHover = this.setHover.bind(this);
  }
  componentWillReceiveProps (nextProps) {
    if (nextProps.highlightedEl) {
      this.setHover(false)();
      let thisNode = ReactDOM.findDOMNode(this);
      let selectedNode = thisNode.querySelector(`#location-${nextProps.highlightedEl}`);

      if (selectedNode) {
        thisNode.scrollTop = selectedNode.offsetTop + selectedNode.offsetHeight - thisNode.offsetHeight;
      }
    }
  }
  _selectLocation (selected) {
    this.props.onSelect(selected);
  }
  setHover (doHover) {
    return () => this.setState({ hoverClass: doHover ? 'hover' : '' });
  }

  render () {
    let locations;
    const errorClass = this.props.errorClass ? 'select-location-state' : '';
    let hasResultsToShow = this.props.results && Object.keys(this.props.results).length > 0;

    if (hasResultsToShow) {
      locations = (
        <div className="fedex-location-group">
          <div className="fedex-location-label">
            {i18n('fedexcustompath_0006') || 'SELECT AN ALPHA ID BELOW'}
          </div>
          <table> <tbody>
            { Object.keys(this.props.results).map(location => (
              <tr key={location}>
                <th> {location} </th>
                <td> <ul>
                  {this.props.results[location] && this.props.results[location].map(el => (
                    <AlphaIdResult
                      key={el.alphaId}
                      alphaId={el.alphaId}
                      longitude= {el.longitude}
                      lat= {el.latitude}
                      costCenter={el.costCenter}
                      highlighted={this.props.highlightedEl === el.alphaId}
                      selected={this.props.selected === el.alphaId}
                      _selectLocation={this._selectLocation}
                      blockLocationsRequest={this.props.blockLocationsRequest}/>
                  ))}
                </ul> </td>
              </tr>
            ))}
          </tbody> </table>
        </div>
      );
    }

    return (
      <div
        className={`auto-complete ${errorClass} ${this.state.hoverClass}`}
        onMouseMove={this.setHover(true)}
        onMouseLeave={this.setHover(false)}
      >
        {!!this.props.errorClass &&
          <div className="location-dropdown-error">
            {enterprise.i18nReservation.resflowlocations_0096}
          </div>
        }

        {locations}
        </div>
    );
  }
}

FedexLocationDropdown.displayName = "FedexLocationDropdown";

