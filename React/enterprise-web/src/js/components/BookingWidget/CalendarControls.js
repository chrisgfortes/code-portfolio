const CalendarControls = React.createClass({
  mixins: [React.addons.PureRenderMixin],
  _onNext () {
    this.props.onNext();
  },

  _onPrev () {
    this.props.onPrev();
  },

  focus () {
    if (this._showPrevious()) {
      this.refs.prevMonthBtn.getDOMNode().focus();
    } else {
      this.refs.nextMonthBtn.getDOMNode().focus();
    }
  },

  _showPrevious () {
    let currentMonth = moment().clone().startOf('month');
    let selectedMonth = this.props.date.clone().startOf('month');
    return currentMonth.isSameOrAfter(selectedMonth) ? 'invisible' : '';
  },

  _showNext () {
    let currentMonthFromNextYear = moment().clone().add(1, 'years').startOf('month');
    let selectedMonth = this.props.date.clone().startOf('month');
    return currentMonthFromNextYear.isSameOrBefore(selectedMonth) ? 'invisible' : '';
  },

  render () {
    return (
      <div className="calendar-controls">
        <button
          className={`calendar-control-arrow arrow-left ${this._showPrevious()}`}
          ref="prevMonthBtn"
          tabIndex="0"
          onKeyPress={a11yClick(this._onPrev)}
          onClick={this._onPrev}
          aria-label={enterprise.i18nReservation.reservationwidget_5002}
        >
          <i className="icon icon-Arrow-general-white-left" />
        </button>
        <span className="calendar-control-header">{this.props.date.format('MMMM') + ' ' + this.props.date.format('YYYY')}</span>
        <button
          className={`calendar-control-arrow arrow-right ${this._showNext()}`}
          ref="nextMonthBtn"
          tabIndex="0"
          onKeyPress={a11yClick(this._onNext)}
          onClick={this._onNext}
          aria-label={enterprise.i18nReservation.reservationwidget_5003}
        >
          <i className="icon icon-Arrow-general-white-right" />
        </button>
      </div>
    );
  }
});

module.exports = CalendarControls;
