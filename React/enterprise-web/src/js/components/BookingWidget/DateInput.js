const ReservationCursors = require('../../cursors/ReservationCursors');
const BaobabReactMixinBranch = require('baobab-react/mixins').branch;
const DateInput = React.createClass({

  mixins: [React.addons.PureRenderMixin, BaobabReactMixinBranch],
  cursors: {
    currentView: ReservationCursors.currentView
  },
  getDefaultProps: function () {
    return {
      classes: '',
      type: '',
      date: ''
    };
  },

  _onClick: function () {
    if (this.props.onClick) {
      this.props.onClick(this.props.type);
    }
  },

  renderWithView: function () {
    if (this.props.date) {
      return (
        <div>
          <input type="hidden" id={this.props.type} className="date"/>
          <span className="day">{this.props.date && this.props.date.format('DD')}</span>
          <span className="month">{this.props.date && this.props.date.format('MMM')}</span>
          <span className="year">{this.props.date && this.props.date.format('YYYY')}</span>
          <i className="icon icon-nav-carrot-down"></i>
        </div>
      )
    } else {
      return (
        <div className="empty-date-time-control">
          <i className="icon icon-nav-calendar-green default-icon"></i>
          <span>{enterprise.i18nReservation.reservationwidget_0009}</span>
          <i className="icon icon-nav-carrot-down"></i>
        </div>
      )
    }
  },

  render: function () {
    let labelClasses = 'date-label ' + this.props.type + '-label';
    return (
      <label id={this.props.type + 'Focusable'} role="button" tabIndex="0" onKeyPress={a11yClick(this._onClick)}
           onClick={this._onClick} className={labelClasses} htmlFor={this.props.type} aria-expanded={this.state.currentView ? true : false}>
        {this.renderWithView()}
      </label>
    );
  }
});

module.exports = DateInput;
