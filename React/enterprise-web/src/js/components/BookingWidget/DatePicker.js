const Calendar = require('./Calendar');

const ReservationCursors = require('../../cursors/ReservationCursors');

const BaobabReactMixinBranch = require('baobab-react/mixins').branch;
const DialogFocusTrap = require('../../mixins/DialogFocusTrap');

const DatePicker = React.createClass({
  mixins: [BaobabReactMixinBranch, React.addons.PureRenderMixin, DialogFocusTrap],

  focusTrapOptions: {
    shouldFocusFirst: false
  },

  cursors: {
    pickupClosedDates: ReservationCursors.pickupClosedDates,
    dropoffClosedDates: ReservationCursors.dropoffClosedDates,
    pickupDate: ReservationCursors.pickupDate,
    dropoffDate: ReservationCursors.dropoffDate,
    pickupViewDate: ReservationCursors.pickupViewDate,
    dropoffViewDate: ReservationCursors.dropoffViewDate,
    pickupTempDate: ReservationCursors.pickupTempDate,
    dropoffTempDate: ReservationCursors.dropoffTempDate
  },

  propTypes: {},

  getDefaultProps () {
    //Calendar should only be open for one year from current date
    return {
      range: [moment().clone().subtract(1, 'days'), moment().clone().add(1, 'years')]
    };
  },

  //Add events consumable by Calendar
  buildEvents () {
    const dateEvents = [];
    const startDate = this.state.pickupDate;
    const endDate = this.state.dropoffDate;


    if (startDate) {
      dateEvents.push({
        momentDate: startDate,
        eventType: 'pickup selected'
      });
    }

    if (endDate) {
      dateEvents.push({
        momentDate: endDate,
        eventType: 'dropoff selected'
      });
    }


    //Add inbetween class to show duration of trip
    if (endDate && startDate) {
      const dateClone = startDate.clone();
      while (dateClone.add(1, 'days').isBefore(endDate)) {
        dateEvents.push({
          momentDate: dateClone.clone(),
          eventType: 'selection-range'
        });
      }
    }

    dateEvents.push.apply(dateEvents, this.addClosedDateEvents());

    return dateEvents;
  },

  addClosedDateEvents () {
    let closedDates = [];

    if (this.props.type === 'pickup') {
      if (!this.state.pickupClosedDates) return closedDates;
      closedDates = this.state.pickupClosedDates.map(date => (
        {
          momentDate: moment(date, 'YYYY-MM-DD'),
          eventType: 'closed'
        }
      ));
    } else if (this.props.type === 'dropoff') {
      if (!this.state.dropoffClosedDates) return closedDates;
      closedDates = this.state.dropoffClosedDates.map(date => (
        {
          momentDate: moment(date, 'YYYY-MM-DD'),
          eventType: 'closed'
        }
      ));
    }
    return closedDates;
  },

  componentWillUnmount () {
    // Make sure focus wont be lost to limbo when dialog is unmounted.
    $('#' + this.props.type + 'CalendarFocusable').focus();
  },

  render () {
    return (
      <div role="dialog" ref={c => this.focusTrapWrapper = c} aria-labelledby={this.props.type + 'CalendarFocusable'}>
        <Calendar
          modelController={this.props.modelController}
          type={this.props.type}
          date={this.state[this.props.type + 'ViewDate']}
          range={this.props.range}
          events={this.buildEvents()}
          isBookingController={this.props.isBookingController} />
        <Calendar
          modelController={this.props.modelController}
          type={this.props.type}
          date={this.state[this.props.type + 'ViewDate'].clone().add(1, 'months')}
          range={this.props.range}
          events={this.buildEvents()}
          isBookingController={this.props.isBookingController} />
        <div className="calendar-legend">
          <div className="legend-section">
            <span className="legend-box closed"> </span> <span
            className="legend-text"> {enterprise.i18nReservation.resflowlocations_0004} </span>
          </div>
        </div>
      </div>
    );
  }
});

module.exports = DatePicker;
