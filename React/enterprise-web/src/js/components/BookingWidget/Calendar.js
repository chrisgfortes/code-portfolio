import Day from './Day';
import CalendarControls from './CalendarControls';

import DateTimeController from '../../controllers/DateTimeController';

const Calendar = React.createClass({
  propTypes: {
    forceSixRows: React.PropTypes.bool,
    showDaysOfWeek: React.PropTypes.bool
  },
  mixins: [React.addons.PureRenderMixin],

  getDefaultProps () {
    return {
      forceSixRows: false,
      events: null
    };
  },

  dateToArrayIndex: {},

  next () {
    const date = this.props.date.clone().add(1, 'months');
    if (this.props.isBookingController) {
      DateTimeController.setViewDate(date, this.props.type);
    } else {
      this.props.modelController.setViewDate(date, this.props.type);
    }
  },

  prev () {
    const date = this.props.date.clone().subtract(1, 'months');
    if (this.props.isBookingController) {
      DateTimeController.setViewDate(date, this.props.type);
    } else {
      this.props.modelController.setViewDate(date, this.props.type);
    }
  },

  generateDaysForMonth: function () {
    const days = [];
    const date = this.props.date.startOf('month');
    let diff = date.weekday();// - this.props.weekOffset;

    if (diff < 0) {
      diff += 7;
    }

    let dateIndexTracking = 0;
    let i;
    let day;

    for (i = 0; i < diff; i++) {
      day = moment([this.props.date.year(), this.props.date.month(), i - diff + 1]);
      days.push({day: day, id: 'prev-month-' + i, classes: 'prev-month disabled', options: ['disabled']});

      // For ease of reindexing into days in the case of adding events and classes
      this.dateToArrayIndex[day.format('YYYYMMDD')] = dateIndexTracking++;
    }

    const numberOfDays = date.daysInMonth();
    for (i = 1; i <= numberOfDays; i++) {
      let classes = '';
      const options = [];

      day = moment([this.props.date.year(), this.props.date.month(), i]);

      // For ease of reindexing into days in the case of adding events and classes
      this.dateToArrayIndex[day.format('YYYYMMDD')] = dateIndexTracking++;

      //Using passed in range of calendar to block out dates
      if (day.isBefore(this.props.range[0]) || day.isAfter(this.props.range[1])) {
        classes += ' disabled';
        options.push('disabled');
      }

      days.push({day: day, id: day.format('YYYYMMDD'), classes: classes, options: options});
    }

    i = 1;
    while (days.length % 7 !== 0) {
      day = moment([this.props.date.year(), this.props.date.month(), numberOfDays + i]);
      days.push({day: day, id: 'next-month-' + i, classes: 'next-month disabled', options: ['disabled']});
      i++;

      // For ease of reindexing into days in the case of adding events and classes
      this.dateToArrayIndex[day.format('YYYYMMDD')] = dateIndexTracking++;
    }

    i = 1;
    if (this.props.forceSixRows && days.length !== 42) {
      let start = moment(days[days.length - 1].date).add(1, 'days');
      while (days.length < 42) {
        days.push({
          day: moment(start),
          id: 'force-six-' + i++,
          classes: 'next-month disabled',
          options: ['disabled']
        });
        start.add(1, 'days');
      }
    }

    return this.splitWeeks(this.daysWithEvents(days));
  },

  daysWithEvents (days) {
    if (this.props.events) {
      for (let i = 0; i < this.props.events.length; i++) {
        const dayEvent = this.props.events[i];

        if (!dayEvent.momentDate.clone().startOf('month').isSame(this.props.date.clone().startOf('month'))) {
          continue;
        }

        const dayIndex = this.dateToArrayIndex[dayEvent.momentDate.format('YYYYMMDD')];

        days[dayIndex].classes += ' ' + dayEvent.eventType + ' ';

        if (!days[dayIndex].options) {
          days[dayIndex].options = [];
        }
        days[dayIndex].options.push(dayEvent.eventType);
      }
    }
    return days;
  },

  splitWeeks (days) {
    return days.reduce((out, day) => {
      const lastWeek = out[out.length - 1];
      lastWeek.push(day);

      return (lastWeek.length === 7 ? out.concat([[]]) : out);
    }, [[]]);
  },

  daysOfWeek () {
    let daysOfWeek = this.props.daysOfWeek;

    if (!daysOfWeek) {
      daysOfWeek = [];
      for (let i = 0; i < 7; i++) {
        const weekday = moment().weekday(i);

        daysOfWeek.push({
          dd: weekday.format('dd'),
          dddd: weekday.format('dddd')
        });
      }
    }
    return daysOfWeek;
  },

  render () {
    return (
      <div className="calendar">
        { /* ECR-12734 - CalendarControls should be inside calendar-grid (as a <caption>),
                         but Safari doesn't like this idea at the moment... */ }
        <CalendarControls
          date={this.props.date}
          onNext={this.next}
          onPrev={this.prev}
        />
        <table className="calendar-grid">
          <thead className="day-headers">
            <tr>
              {this.daysOfWeek().map((day, i) => {
                return <th scope="col" key={'weekday-' + i} aria-label={day.dddd}>{day.dd}</th>;
              })}
            </tr>
          </thead>
          <tbody className="days cf">
            {this.generateDaysForMonth().map((week, i) => (
              <tr key={'week-' + i}>
                {week.map(day => (
                  <td>
                    <Day
                      modelController={this.props.modelController}
                      type={this.props.type}
                      key={'day-' + day.id}
                      day={day}
                    />
                  </td>
                ))}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
});

module.exports = Calendar;
