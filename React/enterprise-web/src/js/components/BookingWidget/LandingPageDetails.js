import LocationHours from '../LocationSearch/LocationHours';
import classNames from 'classnames';
import { PARTNERS } from '../../constants';

export default function LandingPageDetails ({deepLinkReservation, pickupTarget, type, dateOfType, mapOfType}) {
  let details = deepLinkReservation.reservation.pickup_location;
  if (!details) {
    return false;
  }
  let brand = details && details.brand;
  let isExternalBrand = (brand === PARTNERS.ALAMO || brand === PARTNERS.NATIONAL);
  let drivingDirectionLink = details && details.gps && 'https://maps.google.com/?daddr=' + details.gps.latitude + ',' +
    details.gps.longitude + '&hl=' + enterprise.language;

  let allowAfterHoursDropoff = details && details.afterHoursDropoff;
  let willPickUp = details && details.we_will_pick_you_up;

  let afterHoursDropoffClasses = classNames({
    'location-attribute': true,
    'disabled': !allowAfterHoursDropoff
  });

  let pickYouUpClasses = classNames({
    'pick-you-up': true,
    'disabled': !willPickUp
  });

  return (
    <div className="landing-page-details about-location">
      <div className="details-panel">
        <h2 className="panel-header">{i18n('resflowlocations_0022')}</h2>

        <div className="general-information">

          <h2 className="location-name">{details.name}</h2>
          {details.address && details.address.street_addresses.map((line, index)=> {
            if (line) {
              return (
                <div key={index} className="location-address">{line}</div>
              );
            }
          })}
          <div>{details.address && details.address.city}, {details.address && details.address.postal}</div>
          {details.phones && details.phones.length ?
            <div className="tel">
              <a href={'tel:' + details.phones[0].phone_number}>{details.phones[0].phone_number}</a>
            </div> : false
          }

          <div className={pickYouUpClasses}>
            <i className={willPickUp ? 'icon icon-icon-checkmark-thin-green' :
        'icon icon-icon-unavailable-gray'}></i>
            <span className={details.we_will_pick_you_up_text ? 'has-tip' : ''}>
                <span className="policy-label">{i18n('locations_0006')}</span>
            </span>
          </div>

          <div className={afterHoursDropoffClasses}>
            <i className={allowAfterHoursDropoff ? 'icon icon-icon-checkmark-thin-green' :
          'icon icon-icon-unavailable-gray'}></i>
            <span className="policy-label">{i18n('locations_0011')}</span>
          </div>

          {isExternalBrand ?
            false :
            <a target="_blank"
               href={drivingDirectionLink}
               className="btn direction">
              {i18n('resflowlocations_0027')}
            </a>
          }
        </div>
        <div className="line-divider"></div>

        {details && pickupTarget.weeklyHours ?
          <LocationHours
            {...{mapOfType, deepLinkReservation, dateOfType, type}}
            week={pickupTarget.weeklyHours} />
          : false}
      </div>
    </div>
  );
}

LandingPageDetails.displayName = "LandingPageDetails";
