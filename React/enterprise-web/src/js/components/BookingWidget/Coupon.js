import BookingWidgetModelController from '../../controllers/BookingWidgetModelController';

export default class Coupon extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      cleared: false 
    };
    this._onChange = this._onChange.bind(this);
    this.clearCoupon = this.clearCoupon.bind(this);
  }
  componentDidMount () {
    if (this.props.expeditedCoupon) {
      BookingWidgetModelController.setCoupon(this.props.expeditedCoupon);
    }
  }
  _onChange (e) {
    BookingWidgetModelController.setCoupon(e.target.value.trim());
  }
  clearCoupon () {
    if (this.props.modify) {
      return false;
    }
    this.setState({ cleared: true });
    BookingWidgetModelController.setCoupon(null);
    BookingWidgetModelController.setCouponLabel(null);
  }
  render () {
    let chicklet;
    let couponClassName = 'coupon-field-wrapper';
    const {placeholder, isRemovable, modify, coupon} = this.props;
    
    if (placeholder && !this.state.cleared) {
      if (isRemovable) {
        // Editable CID
        chicklet = (
          <div className="coupon-chicklet removable" onClick={this.clearCoupon}>
            <a className="chicklet">
              {placeholder}{' '}
              {!modify &&
                <span aria-label={i18n('reservationwidget_5001')}>X</span>
              }
            </a>
          </div>
        );
        couponClassName = `${couponClassName} couponChicklet-active`;
      } else {
        // Locked CID
        chicklet = (
          <div className="coupon-chicklet" onClick={BookingWidgetModelController.setLockedCIDModal.bind(null, true)}>
            <a className="chicklet">
              {placeholder}{' '}
              {!modify &&
                <span aria-label={i18n('reservationwidget_5001')} className="icon icon-icon-info-green"/>
              }
            </a>
          </div>
        );
        couponClassName = `${couponClassName} couponChicklet-active disabled`;
      }
    }

    couponClassName = `${couponClassName} ${modify ? 'disabled-coupon' : ''}`;

    return (
      <div className={couponClassName}>
        <input
          id="coupon"
          name="coupon"
          className="coupon-input"
          disabled={modify}
          onChange={this._onChange}
          value={coupon}
          placeholder={i18n('reservationwidget_0013')} />
        {chicklet}
      </div>
    );
  }
}
Coupon.displayName = "Coupon";
