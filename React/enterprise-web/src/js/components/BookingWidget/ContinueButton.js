/**
 * Created by cmeng on 6/17/16.
 */

export default function ContinueButton ({featuredWidgetExpand}) {
  return (
    <button className="btn featured-continue-btn" onClick={featuredWidgetExpand}
      onKeyPress={a11yClick(featuredWidgetExpand)}>
      {i18n('reservationwidget_0014')}
    </button>
  );
}

ContinueButton.displayName = "ContinueButton";
