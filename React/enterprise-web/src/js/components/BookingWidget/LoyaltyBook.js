/**
 * LoyaltyBook
 * @module
 * When do we get loyaltyBrand?
 */
import BookingWidgetModelController from '../../controllers/BookingWidgetModelController';
export default class LoyaltyBook extends React.Component {
  constructor(props) {
    super(props);
    this.membershipIdChange = this.membershipIdChange.bind(this);
    this.loyaltyLastNameChange = this.loyaltyLastNameChange.bind(this);
  }
  membershipIdChange (event) {
    BookingWidgetModelController.setMembershipId(event.target.value);
  }
  loyaltyLastNameChange (event) {
    BookingWidgetModelController.loyaltyLastNameChange(event.target.value);
  }
  render () {
    return (
      <div className="loyalty-book cf">
        <label className="left">
          {this.props.loyaltyBrand === 'EC' ?
            i18n('corpECunauthenticatedpath_0004') || 'Emerald Club Member Number' :
            i18n('corpEPunauthenticatedpath_0004') || 'Enterprise Plus Member Number'
          }
          <input
            type="text"
            onChange={this.membershipIdChange}
            value={this.props.memberShipId}
          />
        </label>
        <label>
          {i18n('reservationwidget_0022') || 'Last name'}
          <input
            type="text"
            onChange={this.loyaltyLastNameChange}
            value={this.props.loyaltyLastName}
          />
        </label>
      </div>
    );
  }
}

LoyaltyBook.displayName = "LoyaltyBook"
