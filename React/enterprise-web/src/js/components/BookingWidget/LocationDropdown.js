import ReactDOM from 'react-dom';
import LocationResult from './LocationResult';
import LocationType from './LocationType';
import PreviouslySelectedLocations from './PreviouslySelectedLocations';
import { Debugger } from '../../utilities/util-debug';
import { LOCATION_SEARCH } from '../../constants';
import classNames from 'classnames';

const icons = {
  [LOCATION_SEARCH.TYPES.AIRPORTS]  : 'airport',
  [LOCATION_SEARCH.TYPES.CITIES]    : 'city',
  [LOCATION_SEARCH.TYPES.TRAINS]    : 'rail',
  [LOCATION_SEARCH.TYPES.PORTS]     : 'portofcall',
  [LOCATION_SEARCH.TYPES.BRANCHES]  : 'neighborhood',
  [LOCATION_SEARCH.TYPES.COUNTRIES] : 'country'
};

const locationGroupLabels = {
  [LOCATION_SEARCH.TYPES.AIRPORTS]  : i18n('reservationwidget_2002') || 'AIRPORTS',
  [LOCATION_SEARCH.TYPES.CITIES]    : i18n('reservationwidget_2003') || 'CITIES/AREAS',
  [LOCATION_SEARCH.TYPES.TRAINS]    : i18n('reservationwidget_2004') || 'TRAIN STATIONS',
  [LOCATION_SEARCH.TYPES.PORTS]     : i18n('reservationwidget_2008') || 'PORTS',
  [LOCATION_SEARCH.TYPES.BRANCHES]  : i18n('reservationwidget_2009') || 'BRANCHES',
  [LOCATION_SEARCH.TYPES.COUNTRIES] : i18n('leadforms_0008')         || 'Country'
};

export default class LocationDropdown extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      isHovering: false
    };
    this._selectLocation = this._selectLocation.bind(this);
    this.setHover = this.setHover.bind(this);
    this.locationComponent = this.locationComponent.bind(this);
    this.branchComponent = this.branchComponent.bind(this);
    // debugging
    this.isDebug = false;
    this.logger = new Debugger(window._isDebug, this, true, 'LocationDropdown');
  }
  componentWillReceiveProps (nextProps) {
    if (nextProps.activedescendant) {
      this.setHover(false)();
      let thisNode = ReactDOM.findDOMNode(this);
      let selectedNode = thisNode.querySelector(`#location-${nextProps.activedescendant}`);

      if (selectedNode) {
        thisNode.scrollTop = selectedNode.offsetTop + selectedNode.offsetHeight - thisNode.offsetHeight;
      }
    }
  }

  _selectLocation (props) {
    this.logger.warn('@todo: This needs to be changed to a Controller Call!!!!');
    this.logger.log('_selectLocation(props) calling this.props.onSelect()', props);
    this.props.onSelect(props);
  }
  setHover (isHovering) {
    return () => this.setState({ isHovering });
  }
  branchComponent (location) {
    return (<LocationResult key={location.peopleSoftId}
                            highlighted={this.props.activedescendant === location.peopleSoftId}
                            selected={this.props.selected === location.peopleSoftId}
                            location={location}
                            _selectLocation={this._selectLocation}
                            blockLocationsRequest={this.props.blockLocationsRequest} />);
  }
  locationComponent (location) {
    return (<LocationType key={location.cityId || location.countryCode}
                          highlighted={this.props.activedescendant === location.cityId || this.props.activedescendant === location.countryCode}
                          selected={this.props.selected === location.cityId || this.props.selected === location.countryCode}
                          city={location}
                          _selectLocation={this._selectLocation} />);
  }

  render () {
    const { hasUnselectedError, orderedLocations, searchedLocations,
            type, activedescendant, selected, showDropdown} = this.props;
    
    const hasResultsToShow = orderedLocations.some(el => el[1].length > 0);
    const hasRecentSearches = searchedLocations && searchedLocations.length > 0;

    this.logger.log('orderedLocations:', orderedLocations);
    const recentSearches = hasRecentSearches ? <PreviouslySelectedLocations
                                                  type={type}
                                                  _selectLocation={this._selectLocation}
                                                  locations={searchedLocations}
                                                  highlightedItem={activedescendant}
                                                  selectedItem={selected}
                                                /> : <div/>
    const locations = hasResultsToShow ?
            orderedLocations.map(([groupType, locations]) => locations.length > 0 && (
              <div className="location-group" role="presentation" key={groupType}>
                <i className={`icon icon-location-${icons[groupType]}`} role="presentation"/>
                <div className="location-group-label" role="presentation">
                  { locationGroupLabels[groupType] || '' }
                </div>
                <ul role="presentation">
                  {locations.map((groupType === LOCATION_SEARCH.TYPES.CITIES || groupType === LOCATION_SEARCH.TYPES.COUNTRIES) ? this.locationComponent : this.branchComponent)}
                </ul>
              </div>
            ))
            :recentSearches;

    this.logger.log('locations:', locations);

    const cssClasses = classNames({
      'auto-complete': true,
      'select-location-state': hasUnselectedError,
      'hover': this.state.isHovering,
      'empty-result': !hasRecentSearches && !hasResultsToShow
    });

    return (
      <div id="locations-list" className={cssClasses} role="listbox" aria-expanded={showDropdown}
           onMouseMove={this.setHover(true)} onMouseLeave={this.setHover(false)} onMouseDown={(event) => this.props.setScrollClick(event, null)}>
        {locations}
      </div>
    );
  }
}

LocationDropdown.displayName = "LocationDropdown";
