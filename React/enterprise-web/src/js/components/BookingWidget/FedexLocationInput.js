/**
 * FedexLocationInput
 * @module
 * Not dramatically different than its counterpart LocationInput
 * In fact I can't help wondering if they should share methods...
 * How it works:
 * User types, input fires handleKeyDown, also listens for handleChange
 * - handleChange fires a debounced this.searchLocation
 * - searchLocation checks some values and runs
 * ReservationFlowModelController.getFedexLocations()
 * - _handleSelect for when an item is clicked upon (see LocationInput for similar behavior)
 */
import ResponsiveUtil from '../../utilities/util-responsive';
import classNames from 'classnames';
import ReservationFlowModelController from '../../controllers/ReservationFlowModelController';
import FedexLocationDropdown from './FedexLocationDropdown';
import FedexAlphaIdDropdown from './FedexAlphaIdDropdown';
import debounce from '../../utilities/util-debounce';

export default class FedexLocationInput extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      processing: false,
      lastCall: '',
      highlightedEl: false,
      selected: false,
      inputValue: '',
      showDropdown: false,
      displayUnselectedError: false,
      inputHasFocus: false,
      isLocationSearchMode: false
    }
    this._inputClasses = this._inputClasses.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.searchLocation = this.searchLocation.bind(this);
    this.toggleSearchMode = this.toggleSearchMode.bind(this);
    this.clearLocation = this.clearLocation.bind(this);
    this._handleSelect = this._handleSelect.bind(this);
    this.onChickletMount = this.onChickletMount.bind(this);
  }
  _inputClasses (currentView) {
    let classes = 'location-input location-field ';

    if (currentView === this.props.type) {
      classes += 'selected';
    }

    return classes;
  }
  handleBlur () {
    setTimeout(() => {
      this.setState({
        showDropdown: false,
        displayUnselectedError: !!this.state.inputValue.length
      });
    }, 250);
  }
  handleFocus () {
    this.setState({
      showDropdown: true,
      inputHasFocus: true
    });
    this.props.blockLocationsRequest(false);
    ReservationFlowModelController.setLocationView(this.props.type);
    ReservationFlowModelController.setCurrentView(this.props.type);
    //@todo look to add this to the view model of the state tree.
    this.props.setBookingWidgetExpanded(true);

    const $resWidget = (
      ResponsiveUtil.isMobile('(max-height: 700px)') ?
        $('#pickupLocationTextBox') :
        $('#reservationWidget')
    );

    if ($resWidget.length) {
      $('html, body').animate({scrollTop: $resWidget.offset().top - 5}, 800);
    }
  }
  handleKeyDown (e) {
    if (e.key === 'Escape') {
      this.setState({showDropdown: false});
    }
    if (e.key === 'Enter') {
      e.preventDefault();
      this.setState({selected: this.state.highlightedEl});
    }
    if (e.key === 'ArrowUp' || e.key === 'ArrowDown') {
      e.preventDefault();

      const results = this.props.pickupLocation.results.fedex;
      let elements = [];

      if (this.state.isLocationSearchMode) {
        // Linearize all alpha IDs
        Object.keys(results).forEach( (location) => {
          return results[location].forEach( (elem) => {
            elements.push(elem);
          });
        });
      } else {
        elements = results;
      }

      if (elements.length > 0) {
        let locationsLen = elements.length;
        let highlightedEl;

        if (e.key === 'ArrowUp') {
          highlightedEl = elements[locationsLen - 1];

          if (this.state.highlightedEl) {
            highlightedEl = elements.filter((location, index, arr) => {
              let nextElement = index < locationsLen - 1 ? arr[index + 1] : arr[0];
              return nextElement.alphaId === this.state.highlightedEl;
            })[0];
          }
        } else {
          highlightedEl = elements[0];

          if (this.state.highlightedEl) {
            highlightedEl = elements.filter((location, index, arr) => {
              let prevElement = index > 0 ? arr[index - 1] : arr[locationsLen - 1];
              return prevElement.alphaId === this.state.highlightedEl;
            })[0];
          }
        }

        this.setState({highlightedEl: highlightedEl.alphaId});
      }
    }
  }
  handleChange (e) {
    this.setState({
      inputValue: e.target.value,
      displayUnselectedError: false
    });
    debounce(this.searchLocation.bind(this, e.target.value), 250)();
  }
  searchLocation (query) {
    this.setState({
      showDropdown: true,
      highlightedEl: false
    });

    if (query && query.length > 2) {
      if (this.state.lastCall !== query) {
        ReservationFlowModelController.getFedexLocations(
          query,
          this.props.type,
          this.state.isLocationSearchMode ? 'byCity' : 'byAlphaId'
        );
      }
    } else {
      ReservationFlowModelController.clearFedexResults(this.props.type);
    }

    this.setState({lastCall: query});
  }
  toggleSearchMode () {
    this.clearLocation();
    this.setState({
      isLocationSearchMode: !this.state.isLocationSearchMode
    });
  }
  clearLocation () {
    this.setState({
      selected: false,
      highlightedEl: false,
      displayUnselectedError: false
    });
    this.props.blockLocationsRequest(false);

    if (this.props.sameLocation) {
      ReservationFlowModelController.clearLocationsForAll();
    } else {
      ReservationFlowModelController.clearLocation(this.props.type);
    }

    ReservationFlowModelController.setClearLocationAttributes(this.props.type, 'all', 'all', 'all', null, {lat: null, longitude: null});

    this.setState({inputValue: ''});
    setTimeout(() => {
      this.inputField.focus();
    }, 100);
  }
  _handleSelect (selection) {
    this.setState({
      inputValue: '',
      showDropdown: false,
      displayUnselectedError: false
    });
    this.props._handleSelect(selection, this.props.type);
  }
  onChickletMount (thisNode) {
    if (thisNode && this.state.inputHasFocus) {
      this.setState({
        chickletLoaded: true,
        inputHasFocus: false
      });
      this.chicklet.focus();
    }
  }
  render () {
    const locations = this.props.pickupLocation.results.fedex;
    const chickletClasses = classNames({
      'location-chicklet': true
    });
    let dropdown;
    let chicklet;
    let locationName = this.props.pickupLocation.locationName;

    let locationsClassName = 'locationInput-active';
    let highlightedEl = this.state.highlightedEl;

    let noResultsCopy = i18n('fedexcustompath_0027') || 'Sorry, we couldn\'t find any locations matching \"#{search}\". Try reconfirming the Alpha ID you entered, enter a new Alpha ID, or switch to searching by location. If you are still unable to find your Alpha ID, please call 866-339-4058.';
    noResultsCopy = noResultsCopy.replace('#{search}', this.state.lastCall);

    if (this.state.showDropdown) {
      if (this.props.modelNoResults) {
        highlightedEl = false;
        // The following translation is english-only on purpose.
        dropdown = (
          <div className="auto-complete no-results">
            <ul>
              <li>
                {noResultsCopy}
              </li>
            </ul>
          </div>
        );
      } else {
        if (this.state.isLocationSearchMode) {
          dropdown = (
            <FedexLocationDropdown
              onSelect={this._handleSelect}
              highlightedEl={this.state.highlightedEl}
              selected={this.state.selected}
              results={locations}
              errorClass={this.state.displayUnselectedError}
              type={this.props.type}
              blockLocationsRequest={this.props.blockLocationsRequest} />
          );
        } else {
          dropdown = (
            <FedexAlphaIdDropdown
              onSelect={this._handleSelect}
              highlightedEl={this.state.highlightedEl}
              selected={this.state.selected}
              results={locations}
              errorClass={this.state.displayUnselectedError}
              type={this.props.type}
              blockLocationsRequest={this.props.blockLocationsRequest} />
          );
        }
      }
    }

    if (locationName) {
      chicklet = (
        <div
          className={chickletClasses}
          ref={this.onChickletMount}>
          <div className="chicklet">
            {locationName}
          </div>
          <div
            className="chicklet location-chicklet-clear"
            ref={c => this.chicklet = c}
            onClick={this.clearLocation}
            onKeyPress={a11yClick(this.clearLocation)}
            tabIndex="0"
            aria-label={locationName}
          >
            <i className="icon icon-ENT-icon-close"
               aria-label={i18n('reservationwidget_5001')}/>
          </div>
        </div>
      );
      locationsClassName = 'locationChicklet-active';
    }

    const noLocationError = (!!this.state.displayUnselectedError &&
      <div className="location-dropdown-error">
        {
          // The following translation is english-only on purpose.
          this.props.modelNoResults ? noResultsCopy : i18n('resflowlocations_0096')
        }
      </div>
    );

    // Toggle between Location/Alpha ID search
    let searchModeToggler;
    if (this.state.isLocationSearchMode) {
      searchModeToggler = (
        <label htmlFor={this.props.type + 'LocationTextBox'} className="location-input-label">
          <span href="#" role="button" onClick={this.toggleSearchMode} className="green no-wrap">
            {i18n('fedexcustompath_0001') || 'ENTER LOCATION (ALPHA ID)'}
          </span>
          <span className="sublabel">&nbsp;{i18n('fedexcustompath_0002') || 'OR'}&nbsp;&nbsp;</span>
          <span className="no-wrap">{i18n('fedexcustompath_0003') || 'LOOK UP ALPHA ID BY LOCATION'}</span>
        </label>
      );
    } else {
      searchModeToggler = (
        <label htmlFor={this.props.type + 'LocationTextBox'} className="location-input-label">
          <span className="no-wrap">{i18n('fedexcustompath_0001') || 'ENTER LOCATION (ALPHA ID)'}&nbsp;</span>
          <span className="sublabel">&nbsp;{i18n('fedexcustompath_0002') || 'OR'}&nbsp;&nbsp;</span>
          <span tabIndex="0" role="button" onClick={this.toggleSearchMode} onKeyPress={a11yClick(this.toggleSearchMode)} className="green no-wrap">
            {i18n('fedexcustompath_0003') || 'LOOK UP ALPHA ID BY LOCATION'}
          </span>
        </label>
      );
    }

    return (
      <div className={locationsClassName}>
        {(!this.props.hideLabel) &&
          searchModeToggler
        }

        <div className="location-input-wrapper">
          <input
            id={this.props.type + 'LocationTextBox'}
            className={this._inputClasses(this.props.locationView)}
            autoComplete="off"
            onFocus={this.handleFocus}
            onBlur={this.handleBlur}
            onKeyDown={this.handleKeyDown}
            onChange={this.handleChange}
            value={this.state.inputValue}
            placeholder={
              this.state.isLocationSearchMode ?
                i18n('fedexcustompath_0007') || 'Enter a city and select your Alpha ID'
                :
                i18n('fedexcustompath_0022') || 'Enter your Alpha ID'
            }
            aria-activedescendant={highlightedEl && `location-${highlightedEl}`}
            ref={c => this.inputField = c}
            />
          {chicklet}
          {noLocationError}
          {dropdown}
        </div>

      </div>
    );
  }
}

FedexLocationInput.displayName = "FedexLocationInput";
