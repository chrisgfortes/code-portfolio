import BookingWidgetModelController from '../../controllers/BookingWidgetModelController';
import { GLOBAL } from '../../constants';

export default class EmployeeNumber extends React.Component {
  constructor(props){
    super(props);
    this.onChange = this.onChange.bind(this);
  }
  onChange (e) {
    BookingWidgetModelController.setEmployeeNumber(e.target.value.trim());
  }
  render () {
    let hasEmployeeNumberErrors = false;

    if (Array.isArray(this.props.errors)) {
      hasEmployeeNumberErrors = this.props.errors.some((error) => {
        return error.code === GLOBAL.EMPLOYEE_NUMBER_ERROR;
      });
    }

    return (
      <div className="employee-number-wrapper">
        <label htmlFor="employeeNumber" className="employee-number-label">
          {i18n('fedexcustompath_0004') || 'EMPLOYEE NUMBER'}
          &nbsp;
          <span className="tooltip-v2" tabIndex="0" aria-describedby="more-info-tooltip-content">
            (<span className="green">
              <span>{i18n('feddexmoreinfo') || 'more info'}</span>
              <span id="more-info-tooltip-content" className="tooltip" role="tooltip">
                {i18n('fedexcustompath_0008') || 'Fedex employee ID is required'}
              </span>
            </span>)
          </span>
        </label>
        <input type="text"
               id="employeeNumber"
               name="employeeNumber"
               className={'employee-number-input' + (hasEmployeeNumberErrors ? ' invalid' : '')}
               disabled={this.props.modify}
               onChange={this.onChange}
               value={this.props.employeeNumber}
        />
      </div>
    );
  }
}
EmployeeNumber.displayName = "EmployeeNumber";
