/**
 * BookingWidget
 * @module
 * Used by home > ReservationToggle > BookingWidget
 * Also used by reservation > #book
 * Gets either BookingWidgetModelController as this.props.modelController (via home)
 * or ReservationFlowModelController as this.props.modelController (via reservation)
 * @todo Can we drop this.props.modelController?
 */
import DateTimePicker from './DateTimePicker';
import Error from '../Errors/Error';
import ReservationCursors from '../../cursors/ReservationCursors';
import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import Coupon from './Coupon';
import Age from './Age';
import LocationSearch from './LocationSearch';
import SubmitButton from './SubmitButton';
import ActionModals from '../Corporate/ActionModals';
import ChangePasswordModal from '../Account/Password/ChangePasswordModal';
import ContinueButton from './ContinueButton';
import LoyaltyBook from './LoyaltyBook';
import EmployeeNumber from './EmployeeNumber';
import EnterpriseServices from '../../services/EnterpriseServices';
import BookingWidgetModelController from '../../controllers/BookingWidgetModelController';
import BookingWidgetModals from './BookingWidgetModals';
import FedexController from '../../controllers/FedexController';
import { PAGEFLOW } from '../../constants';

const isFedex = () => {
  return FedexController.isFedexFlow(void 0)
};

const BookingWidget = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  getDefaultProps () {
    return {
      modelController: null //Must inject service for components to interact with stateTree
    };
  },
  getInitialState () {
    return ({
      showContinueButton: !!enterprise.featuredCityPage || enterprise.pursuits
    });
  },
  componentWillMount () {
    // to maintain proper page hierarchy, be sure we are showing the H1
    if (enterprise.currentView === PAGEFLOW.RESERVATION_FLOW) {
      enterprise.heading = 'H1';
    }
    if (isFedex()) {
      BookingWidgetModelController.setBookingWidgetExpanded(true);
    }
  },
  componentDidMount () {
    EnterpriseServices.cacheQueryString();
  },
  cursors: {
    errors: ReservationCursors.bookingWidgetErrors,
    coupon: ReservationCursors.coupon,
    expanded: ReservationCursors.bookingWidgetExpanded,
    deepLinkReservation: ReservationCursors.deepLinkReservation,
    prepopulatedCoupon: ReservationCursors.prepopulatedCoupon,
    expeditedCoupon: ReservationCursors.expeditedCoupon,
    profile: ReservationCursors.profile,
    sessionContractDetails: ReservationCursors.sessionContractDetails,
    employeeNumber: ReservationCursors.employeeNumber,
    memberShipId: ReservationCursors.memberShipId,
    loyaltyLastName: ReservationCursors.loyaltyLastName,
    age: ReservationCursors.age,
    ageRange: ReservationCursors.ageRange,
    inferredAge: ReservationCursors.inferredAge,
    agePolicyModal: ReservationCursors.agePolicyModal,
    inflightModify: ReservationCursors.inflightModify,
    changePassword: ReservationCursors.changePassword,
    changePasswordErrors: ReservationCursors.changePasswordErrors
  },
  facets: {
    agePolicy: 'agePolicy'
  },
  hideFeaturedContinueButton () {
    this.setState({ showContinueButton: false });
    BookingWidgetModelController.setBookingWidgetExpanded(true);
  },
  setBookingWidgetUpdatedError (error) {
    BookingWidgetModelController.setBookingWidgetUpdatedError(error);
  },
  checkForExistingCorporateCode () {
    return BookingWidgetModelController.checkForExistingCorporateCode();
  },
  clearEmployeeNumberError () {
    BookingWidgetModelController.clearEmployeeNumberError();
  },
  toggleInflightModifyModal (bool) {
    BookingWidgetModelController.toggleInflightModifyModal(bool);
  },
  getCouponDetails(deepLinkReservation, profile, coupon, contractDetails, prepopulatedCoupon) {
    return BookingWidgetModelController.getCouponDetails(deepLinkReservation, profile, coupon, contractDetails, prepopulatedCoupon);
  },
  render () {
    const {
      errors, coupon, expanded,
      deepLinkReservation, prepopulatedCoupon, expeditedCoupon,
      profile, employeeNumber, memberShipId,
      loyaltyLastName, age, ageRange,
      inferredAge, agePolicyModal, inflightModify,
      showContinueButton, agePolicy, sessionContractDetails,
      changePassword, changePasswordErrors
    } = this.state;

    const {
      modelController,
      landingPage,
      loyaltyBrand
    } = this.props;

    const couponDetails = this.getCouponDetails(deepLinkReservation, profile, coupon, sessionContractDetails, prepopulatedCoupon);

    return (
      <div className="booking-widget cf">
        <Error
          errors={errors}
          type="GLOBAL"
        />

        <LocationSearch
          modelController={modelController}
          landingPage={landingPage}
          featuredWidgetExpand={this.hideFeaturedContinueButton}
        />

        {isFedex() && (
          <EmployeeNumber
            employeeNumber={employeeNumber}
            errors={errors}
          />
        )}

        <DateTimePicker
          modelController={modelController}
          isBookingController
        />

        {loyaltyBrand && (
          <LoyaltyBook
            loyaltyBrand={loyaltyBrand}
            memberShipId={memberShipId}
            loyaltyLastName={loyaltyLastName}
          />
        )}

        {(!loyaltyBrand && !isFedex()) && (
          <Age
            age={age}
            ageRange={ageRange}
            inferredAge={inferredAge}
            agePolicyModal={agePolicyModal}
            agePolicy={agePolicy}
          />
        )}

        {!isFedex() && (
          <Coupon
            placeholder={couponDetails && couponDetails.placeholder}
            isRemovable={couponDetails && couponDetails.isRemovable}
            coupon={coupon}
            expeditedCoupon={expeditedCoupon}
          />
        )}

        <SubmitButton
          loyaltyBrand={loyaltyBrand}
          modelController={modelController}
          inflightModify={inflightModify}
          employeeNumber={employeeNumber}
          errors={errors}
          setBookingWidgetUpdatedError={this.setBookingWidgetUpdatedError}
          checkForExistingCorporateCode={this.checkForExistingCorporateCode}
          clearEmployeeNumberError={this.clearEmployeeNumberError}
          toggleInflightModifyModal={this.toggleInflightModifyModal} />

        {(showContinueButton && !expanded) &&
          <ContinueButton
            featuredWidgetExpand={this.hideFeaturedContinueButton}
          />
        }

        <ActionModals
          modelController={modelController}
        />

        <ChangePasswordModal
          header={i18n('loyaltysignin_0050')}
          changePassword={changePassword}
          errors={changePasswordErrors}
        />

        <BookingWidgetModals
          modelController={modelController}
          inflightModify={inflightModify}
          toggleInflightModifyModal={this.toggleInflightModifyModal}
          employeeNumber={employeeNumber}
          clearEmployeeNumberError={this.clearEmployeeNumberError}
          errors={errors}
          setBookingWidgetUpdatedError={this.setBookingWidgetUpdatedError}
          checkForExistingCorporateCode={this.checkForExistingCorporateCode}
          isBookingController
        />
      </div>
    );
  }
});

module.exports = BookingWidget;
