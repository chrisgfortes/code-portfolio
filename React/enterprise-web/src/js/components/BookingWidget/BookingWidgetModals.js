import ReservationCursors from '../../cursors/ReservationCursors';
import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import GlobalModal from '../Modal/GlobalModal';
import NoVehicleAvailabilityModal from '../Errors/NoVehicleAvailabilityModal';
import DNRModal from '../Errors/DNRModal';
import RightPlace from '../Modal/RightPlace';
import BookingWidgetModelController from '../../controllers/BookingWidgetModelController';
import Spinnerprise from '../Spinner/Spinnerprise';

const BookingWidgetModals = React.createClass({
  mixins: [
    BaobabReactMixinBranch, 
    React.addons.PureRenderMixin
  ],
  cursors: {
    noVehicleAvailability: ReservationCursors.noVehicleAvailability,
    dnr: ReservationCursors.dnr,
    rightPlace: ReservationCursors.rightPlaceModal,
    geolocationError: ReservationCursors.geolocationError,
    lockedCIDModal: ReservationCursors.lockedCIDModal,
    viewTermsModal: ReservationCursors.viewTermsModal,
    contractDetails: ReservationCursors.contractDetails,
    rightPlaceType: ReservationCursors.rightPlaceType,
    rightPlaceRefer: ReservationCursors.rightPlaceRefer,
    loading: ReservationCursors.bookingWidgetLoading
  },
  closeDNRModal () {
    BookingWidgetModelController.closeDNRModal();
  },
  closeRightPlaceModal () {
    BookingWidgetModelController.closeRightPlaceModal();
  },
  render() {
    let domHeaderFooterElement = $('header, footer');
    {this.state.loading ?
      domHeaderFooterElement.attr('aria-hidden', 'true') :
      domHeaderFooterElement.removeAttr('aria-hidden');
    }
    const details = this.state.contractDetails || _.get(this.state.deepLinkReservation, 'reservation.contract_details');
    return (
      <div className="modals">
        {this.state.noVehicleAvailability &&
          <GlobalModal
            active={this.state.noVehicleAvailability}
            header={enterprise.i18nReservation.resflowviewmodifycancel_1015}
            content={<NoVehicleAvailabilityModal 
                        modelController={this.props.modelController}
                        inflightModify={this.props.inflightModify}
                        employeeNumber={this.props.employeeNumber}
                        errors={this.props.errors}
                        setBookingWidgetUpdatedError={this.props.setBookingWidgetUpdatedError}
                        checkForExistingCorporateCode={this.props.checkForExistingCorporateCode}
                        clearEmployeeNumberError={this.props.clearEmployeeNumberError}
                        toggleInflightModifyModal={this.props.toggleInflightModifyModal}
                        isBookingController={this.props.isBookingController} />}
            cursor={['view', 'specialError', 'noVehicleAvailability', 'modal']}
            classLabel="no-vehicle-availability"
          />
        }

        {this.state.dnr &&
          <GlobalModal
            active={this.state.dnr}
            header={enterprise.i18nReservation.dnr_0001}
            content={<DNRModal closeDNRModal={this.closeDNRModal}/>}
            cursor={['view', 'specialError', 'dnr', 'modal']}
          />
        }

        {this.state.rightPlace &&
          <GlobalModal
            active={this.state.rightPlace}
            header={enterprise.i18nReservation.rightplace_0004}
            content={<RightPlace 
                      close={this.closeRightPlaceModal}
                      type={this.state.rightPlaceType}
                      refer={this.state.rightPlaceRefer} />}
            cursor={['view', 'rightPlace', 'modal']}
          />
        }

        {this.state.geolocationError.modal &&
          <GlobalModal
            active={this.state.geolocationError.modal}
            header={enterprise.i18nReservation.reservationwidget_0006}
            content={this.state.geolocationError.error}
            cursor={['view', 'geolocationError', 'modal']}
          />
        }

        {this.state.lockedCIDModal.modal &&
          <GlobalModal
            active={this.state.lockedCIDModal.modal}
            header={enterprise.i18nReservation.resflowcorporate_4015}
            content={enterprise.i18nReservation.resflowcorporate_0072}
            cursor={['view', 'lockedCIDModal', 'modal']}
          />
        }

        {this.state.viewTermsModal && details && details.terms_and_conditions &&
        <GlobalModal active={this.state.viewTermsModal}
                     header={enterprise.i18nReservation.promotionemail_0200}
                     content={<div><p dangerouslySetInnerHTML={{__html: details.terms_and_conditions}}/></div>}
                     cursor={['view', 'viewTermsModal', 'modal']}/>
        }

        {this.state.loading &&
        <GlobalModal active={this.state.loading}
                     header=""
                     content={<Spinnerprise modelController={this.props.modelController}/>}
                     classLabel="spinner-class"/>
        }
      </div>
    )
  }
});

module.exports = BookingWidgetModals;