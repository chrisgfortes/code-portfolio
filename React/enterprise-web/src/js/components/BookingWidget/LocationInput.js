/**
 * LocationInput
 * @module
 * Used in the BookingWidget for typing a search.
 * FOR SEARCH :: How it works:
 * User types, input fires handleKeyDown, also listens for handleChange
 * - handleChange fires a debounced this.searchLocation
 * - searchLocation checks some values and runs getLocations
 * - getLocations runs LocationController.getLocations() // ReservationFlowModelController.getLocations()
 * FOR SELECTION :: How it works:
 * Typing populates LocationResult + LocationCity, both of which fire off
 *   _selectLocation() and it bubbles up here
 * - user clicks to select, LocationDropdown triggers _handleSelect
 * - this.props._handleSelect from BookingWidget > LocationSearch._handleSelect()
 * - LocationSearch > this.props.modelController.setLocation()
 * ... for Booking widget that's BookingWidgetModelController, for Location Search step is's
 *   ReservationFlowModelController
 * @todo: compare to FedexLocationInput (can we do something here?)
 * @todo: Should ReservationFlowModelController.getLocations() be in Location Search instead?
 */

import classNames from 'classnames';
import LocationDropdown from './LocationDropdown';

import ReservationFlowModelController from '../../controllers/ReservationFlowModelController';
import LocationController from '../../controllers/LocationController';
import { MessageLogger } from '../../controllers/MessageLogController';

import { LocationSearchFactory } from '../../factories/Location';

import { GLOBAL } from '../../constants';

import debounce from '../../utilities/util-debounce';
import Cookie from '../../utilities/util-cookies';

import ResponsiveUtil from '../../utilities/util-responsive';
import { Debugger } from '../../utilities/util-debug';


function orderedLocations (locations) {
  return LocationSearchFactory.getLocationArrayModel(locations.results);
}

function reducedLocations (locations) {
  return orderedLocations(locations)
    .reduce((prev, cur) => prev.concat(cur[1]), [])
}

export default class LocationInput extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      processing: false,
      lastCall: '',
      activedescendant: false,
      selected: false,
      inputValue: '',
      showDropdown: false,
      searchedLocations: [],
      displayUnselectedError: false,
      featuredCityPageFlag: !!enterprise.featuredCityPage,
      inputHasFocus: false,
      dropdownCanClose: true
    };
    this._inputClasses = this._inputClasses.bind(this);
    this.setGlobalErrorMessage = this.setGlobalErrorMessage.bind(this);
    this.keepDropdownOpen = this.keepDropdownOpen.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.clearLocation = this.clearLocation.bind(this);
    this._handleSelect = this._handleSelect.bind(this);
    this.searchLocation = this.searchLocation.bind(this);
    this.getUserLocation = this.getUserLocation.bind(this);
    this.onChickletMount = this.onChickletMount.bind(this);
    this.setScrollClick = this.setScrollClick.bind(this);
    // debugging
    this.isDebug = false;
    this.logger = new Debugger(window._isDebug, this, true, 'LocationInput');
  }
  componentWillMount() {
    this.logger.groupCollapsed('componentWillMount()')

    if (enterprise.branchID) {
      LocationController.getLocations('branch:' + enterprise.branchID, 'pickup');
    }

    this.logger.log('00 this.props.modelController', this.props.modelController);
    this.logger.log('01 this.props.type', this.props.type);

    let pastLocations = Cookie.get(this.props.type + 'SearchedLocations');
    let locationArray;

    this.logger.log('02 pastLocations:', pastLocations);

    if (pastLocations) {
      locationArray = pastLocations.split('&&');
      if (locationArray.length) {
        try {
          locationArray = $.map(locationArray, function (location) {
            return JSON.parse(location);
          });
        } catch (e) {
          locationArray = null;
        }
      }
    }

    this.logger.log('02.5 locationArray:', locationArray);

    if (locationArray && locationArray.length > 2) {
      locationArray = locationArray.slice(0, 2);
    }

    this.logger.log('02.75 locationArray:', locationArray);

    this.setState({searchedLocations: locationArray});
    this.logger.groupEnd();
  }
  componentDidMount() {
    //Can't use state variable and setScrollClick function to add the event.target here
    //because, if user clicks outside the bookingwidget, we get error. And doing this won't throw error.
    document.addEventListener("click", function(event){
      GLOBAL.BOOKING_WIDGET_CLICK_ELEMENT = event.target; 
    });
  }
  componentWillReceiveProps (nextProps) {
    // this handles the condition where a page element such as a map is populating the input (city page, branch page, etc)
    // typically the modelController is only BookingWidgetModelController but we don't import it on the page due to scope issues
    if(this.props.externalBookingLocation !== nextProps.externalBookingLocation){
      this.logger.log('externalBookingLocation', this.props.externalBookingLocation, nextProps.externalBookingLocation);
      this.props.modelController.setLocationForAll(nextProps.externalBookingLocation);
    }
  }
  _inputClasses (currentView) {
    let classes = 'location-input location-field ';

    if (currentView === this.props.type) {
      classes += 'selected';
    }

    return classes;
  }
  setGlobalErrorMessage (component) {
    this.props.clearErrorsForComponent(component);
    let error = new MessageLogger();
    error.addMessage(enterprise.settings.frontEndMessageLogCodes.invalidLocation);
    ReservationFlowModelController.setErrorsForComponent(error, component);
  }
  keepDropdownOpen () {
    //if the dropdown is open check if someone is clicking in the dropdown
    if ( this.state.showDropdown && !ResponsiveUtil.isMobile()) {
      this.setState({dropdownCanClose: false});
    }
  }
  setScrollClick(event, clickElement) {
    event.stopPropagation();
    GLOBAL.BOOKING_WIDGET_CLICK_ELEMENT = clickElement;
  }
  handleBlur () {
    setTimeout(() => {
      let hasLength = !!this.state.inputValue.length;
      let hasLocation = !!this.props.modelType.location.locationName;
      let canClose = this.state.dropdownCanClose;

      // Error is only being show when
      // a) input box has text (hasLength), and
      // b) location is not being selected in dropdown (hasLocation)

      if ( !ResponsiveUtil.isMobile() && canClose && GLOBAL.BOOKING_WIDGET_CLICK_ELEMENT) {
        if ( hasLength && !hasLocation ) {
          // set error
          this.setGlobalErrorMessage('bookingWidget');
          // set state (display yellow wrapper in <LocationDropdown>)
          this.setState({
            showDropdown: false,
            displayUnselectedError: true,
            dropdownCanClose: true
          });
        } else {
          this.setState({
            showDropdown: false,
            displayUnselectedError: false,
            dropdownCanClose: true
          });
        }
      } else {
        this.inputField.focus();
        this.setState({dropdownCanClose: true});
      }

    }, 250);
  }
  handleFocus () {
    setTimeout(() => {
      this.setState({
        showDropdown: true,
        inputHasFocus: true,
        dropdownCanClose: true
      });
    }, 250);
    this.props.blockLocationsRequest(false);
    ReservationFlowModelController.setLocationView(this.props.type);
    ReservationFlowModelController.setCurrentView(this.props.type);
    //TODO: look to add this to the view model of the state tree.
    this.props.setBookingWidgetExpanded(true);

    const $resWidget = (
      ResponsiveUtil.isMobile('(max-height: 700px)') ?
        $('#pickupLocationTextBox') :
        $('#reservationWidget')
    );

    if ($resWidget.length) {
      $('html, body').animate({scrollTop: $resWidget.offset().top - 5}, 800);
    }
  }
  handleKeyDown (e) {
    if (e.key === 'Escape') {
      this.setState({showDropdown: false});
    }
    if (e.key === 'Enter') {
      e.preventDefault();
      this.setState({selected: this.state.activedescendant});
    }
    if (e.key === 'ArrowUp' || e.key === 'ArrowDown') {
      e.preventDefault();

      let locations = reducedLocations(this.props.modelType.location);
      if (!locations || locations.length === 0) {
        locations = this.state.searchedLocations;
      }
      if (locations && locations.length > 0) {
        let locationsLen = locations.length;
        let activedescendant;

        if (e.key === 'ArrowUp') {
          activedescendant = locations[locationsLen - 1];

          if (this.state.activedescendant) {
            activedescendant = locations.filter((location, index, arr) => {
              let nextElement = index < locationsLen - 1 ? arr[index + 1] : arr[0];
              if (nextElement) {
                return (nextElement.peopleSoftId || nextElement.cityId || nextElement.locationId || nextElement.countryCode) === this.state.activedescendant;
              }
            })[0];
          }
        } else {
          activedescendant = locations[0];

          if (this.state.activedescendant) {
            activedescendant = locations.filter((location, index, arr) => {
              let prevElement = index > 0 ? arr[index - 1] : arr[locationsLen - 1];

              return (prevElement.peopleSoftId || prevElement.cityId || prevElement.locationId || prevElement.countryCode) === this.state.activedescendant;
            })[0];
          }
        }
        if (activedescendant) {
          this.setState({activedescendant: activedescendant.peopleSoftId || activedescendant.cityId || activedescendant.locationId || activedescendant.countryCode});
        } else {
          // console.assert(activedescendant, 'No active descendant present.');
        }
      }
    }
  }
  handleChange (e) {
    this.setState({
      inputValue: e.target.value,
      displayUnselectedError: false
    });
    debounce(this.searchLocation.bind(this, e.target.value), 250)();
  }
  searchLocation (query) {
    this.setState({
      showDropdown: true,
      activedescendant: false
    });

    if (query && query.length > 2) {
      if (this.state.lastCall !== query) {
        LocationController.getLocations(query, this.props.type);
      }
    } else {
      LocationController.clearLocations(this.props.type);
    }


    this.setState({lastCall: query});
  }
  clearLocation () {
    if (this.state.featuredCityPageFlag) {
      this.props.featuredWidgetExpand();
    }

    this.setState({
      selected: false,
      activedescendant: false,
      displayUnselectedError: false
    });

    this.props.blockLocationsRequest(false);

    if (this.props.sameLocation) {
      ReservationFlowModelController.clearLocationsForAll();
    } else {
      ReservationFlowModelController.clearLocation(this.props.type);
    }

    ReservationFlowModelController.setClearLocationAttributes(this.props.type, 'all', 'all', 'all', null, {lat: null, longitude: null});
    ReservationFlowModelController.setSelectionActive(false);

    this.setState({inputValue: ''});
    setTimeout(() => {
      this.inputField.focus();
    }, 100);
  }
  /**
   * fired when the user selects something in the dropdown, this is commonly in BookingWidget > LocationSearch
   * @param  {object} props location and type
   * @return {void}         sets state and calls this.props._handleSelect from parent
   */
  _handleSelect (props) {
    // @todo: do we need to pass in this.props.type?
    this.props._handleSelect(props, this.props.type);
    this.logger.log('_handleSelect:', props, this.props)
    this.setState({
      showDropdown: false
    });
  }
  getUserLocation () {
    if (this.props.featuredWidgetExpand) {
      this.props.featuredWidgetExpand();
    }

    this.props.setBookingWidgetExpanded(true);
    if (!this.state.processing) {
      this.setState({processing: true});
      if ('geolocation' in navigator) {
        navigator.geolocation.getCurrentPosition(position => {
          const lat = position.coords.latitude;
          const lng = position.coords.longitude;

          ReservationFlowModelController.setMyLocation(this.props.type, lat, lng);

          if (this.props._handleNearbyLocations) {
            this.props._handleNearbyLocations(lat, lng, this.props.type);
          }

          setTimeout(() => {
            this.setState({processing: false});
          }, 1000);
        }, error => {
          this.setState({processing: false});
          if (error.code === 1) {
            this.props.geolocationError.set('error', i18n('reservationwidget_5008')).set('modal', true);
          }
        }, {timeout: 10000});
      } else {
        // Geolocation not supported
      }
    }
  }
  onChickletMount (thisNode) {
    if (thisNode && this.state.inputHasFocus) {
      this.setState({
        chickletLoaded: true,
        inputHasFocus: false
      });
      this.chicklet.focus();
    }
  }
  render () {
    const locations = this.props.modelType.location;
    const noResults = this.props.modelType.noResults;
    const chickletClasses = classNames({
      'location-chicklet': true
    });
    let dropdown;
    let chicklet;
    let editable = this.props.editable || this.props.type === 'dropoff';
    let locationName = LocationController.getSelectedLocationNameByType(locations);

    let locationsClassName = 'locationInput-active';
    let activedescendant = this.state.activedescendant;
    let labelIdText = 'location-text-box' + this.props.labelIdSuffix;

    if (this.state.showDropdown) {
      if (noResults) {
        activedescendant = false;
        dropdown = (
          <div className="auto-complete no-results">
            <ul>
              <li>
                {i18n('reservationwidget_8001', { search: this.state.lastCall })}
                <br />
                {i18n('reservationwidget_8002')}
              </li>
            </ul>
          </div>
        );
      } else {
        dropdown = (
          <LocationDropdown
            onSelect={this._handleSelect}
            activedescendant={this.state.activedescendant}
            selected={this.state.selected}
            orderedLocations={orderedLocations(locations)}
            searchedLocations={this.state.searchedLocations}
            hasUnselectedError={this.state.displayUnselectedError}
            showDropdown = {this.state.showDropdown}
            type={this.props.type}
            blockLocationsRequest={this.props.blockLocationsRequest}
            setScrollClick={this.setScrollClick}
          />
        );
      }
    }

    if (locationName) {
      chicklet = (
        <div
          className={chickletClasses}
          ref={this.onChickletMount}>
          <div className="chicklet">
            {locationName}
            {' '}
          </div>
          <div
            className="chicklet location-chicklet-clear"
            ref={c => this.chicklet = c}
            onClick={this.clearLocation}
            onKeyPress={a11yClick(this.clearLocation)}
            tabIndex="0"
            aria-label={locationName}
          >
            <i className="icon icon-ENT-icon-close"
               aria-label={i18n('reservationwidget_5001')}/>
          </div>
        </div>
      );
      locationsClassName = 'locationChicklet-active';
    }

    return (
      <div className={locationsClassName}>
        {(!this.props.hideLabel && editable) &&
        <label
          htmlFor={this.props.type + 'LocationTextBox'}
          className="location-input-label"
          id={labelIdText}
          >
          {i18n('locations_9025')}
          <span className="sublabel">
            {' '}
            {(i18n('reservationwidget_2000') || '(ZIP, City, or Airport)')}
          </span>
        </label>
        }
        {editable &&
        <div className="location-input-wrapper" onClick={this.keepDropdownOpen}>
          <input
            id={this.props.type + 'LocationTextBox'}
            className={this._inputClasses(this.props.locationView)}
            autoComplete="off"
            onFocus={this.handleFocus}
            onBlur={this.handleBlur}
            onKeyDown={this.handleKeyDown}
            onChange={this.handleChange}
            value={this.state.inputValue}
            tabIndex="0"
            role="combobox"
            aria-labelledby={labelIdText}
            aria-autocomplete="list"
            aria-owns="locations-list"
            aria-activedescendant={activedescendant && `location-${activedescendant}`}
            ref={c => this.inputField = c}
            />
          {chicklet}
          {dropdown}
        </div>
        }
        {this.props._differentLocation &&
        <label className="same-location-checkbox">
          <input
            type="checkbox"
            name="sameLocation"
            checked={!this.props.sameLocation}
            onChange={this.props._differentLocation}
            />
          {i18n('reservationwidget_0007')}
        </label>
        }
        {this.props.canUseMyLocation && editable &&
          <a className={`location-btn ${this.state.processing ? 'loading': ''}`} href="#"
             onClick={this.getUserLocation} onKeyPress={a11yClick(this.getUserLocation)} >
            {i18n('reservationwidget_0006')}
            <i className="icon icon-nav-nearme" role="presentation" />
          </a>
        }
      </div>
    );
  }
}

LocationInput.displayName = "LocationInput";
