import ReactDOM from 'react-dom';
import AlphaIdResult from './AlphaIdResult';

export default class FedexAlphaIdDropdown extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hoverClass: ''
    };
    this._selectLocation = this._selectLocation.bind(this);
    this.setHover = this.setHover.bind(this);
  }
  componentWillReceiveProps (nextProps) {
    if (nextProps.highlightedEl) {
      this.setHover(false)();
      let thisNode = ReactDOM.findDOMNode(this);
      let selectedNode = thisNode.querySelector(`#location-${nextProps.highlightedEl}`);

      if (selectedNode) {
        thisNode.scrollTop = selectedNode.offsetTop + selectedNode.offsetHeight - thisNode.offsetHeight;
      }
    }
  }
  _selectLocation (selected) {
    this.props.onSelect(selected);
  }
  setHover (doHover) {
    return () => this.setState({ hoverClass: doHover ? 'hover' : '' });
  }

  render () {
    let locations;
    const errorClass = this.props.errorClass ? 'select-location-state' : '';
    let hasResultsToShow = this.props.results && this.props.results.length > 0;

    if (hasResultsToShow) {
      locations = (
        <div className="fedex-alphaid-group">
          <ul>
            {this.props.results.map(el => (
              <AlphaIdResult
                key={el.alphaId}
                alphaId={el.alphaId}
                costCenter= {el.costCenter}
                longitude= {el.longitude}
                lat= {el.latitude}
                highlighted={this.props.highlightedEl === el.alphaId}
                selected={this.props.selected === el.alphaId}
                _selectLocation={this._selectLocation}
                blockLocationsRequest={this.props.blockLocationsRequest}/>
              ))}
          </ul>
        </div>
      );
    }

    return (
      <div
        className={`auto-complete ${errorClass} ${this.state.hoverClass}`}
        onMouseMove={this.setHover(true)}
        onMouseLeave={this.setHover(false)}
      >
        {!!this.props.errorClass &&
          <div className="location-dropdown-error">
            {enterprise.i18nReservation.resflowlocations_0096}
          </div>
        }

        {locations}
        </div>
    );
  }
}

FedexAlphaIdDropdown.displayName = "FedexAlphaIdDropdown";
