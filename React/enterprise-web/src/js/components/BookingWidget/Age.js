import BookingWidgetModelController from '../../controllers/BookingWidgetModelController';
import GlobalModal from '../Modal/GlobalModal';

export default class Age extends React.Component {
  constructor(props) {
    super(props);
    this.getAgeOptions = this.getAgeOptions.bind(this);
    this._onChange = this._onChange.bind(this);
    if (!this.props.age) {
      const selected = BookingWidgetModelController.getAges(this.props.ageRange).find(age => age.selected);
      BookingWidgetModelController.setAge(selected.value);
    }
  }
  getAgeOptions () {
    return BookingWidgetModelController.getAges(this.props.ageRange).map((age) => {
      const label = age.label.replace('#{age_to}', i18n('reservationwidget_0015'))
                             .replace('#{age_or_older}', i18n('reservationwidget_0042'));
      const value = age.value;
      return <option key={value} value={value}>{label}</option>;
    });
  }
  _onChange (e) {
    BookingWidgetModelController.setAge(e.target.value);
  }

  render () {
    const {modify, age, ageRange, inferredAge, agePolicyModal, agePolicy} = this.props;
    if (inferredAge) {
      return false;
    }

    const selected = BookingWidgetModelController.getAges(ageRange).find(age => age.selected);

    return (
      <div className="custom-select age-input">
        <label htmlFor="age">
          {i18n('reservationwidget_0012')}
          <span onClick={BookingWidgetModelController.showAgePolicyModal.bind(null, true)}
                className={'age-policy green-action-text' + (agePolicy ? '' : ' hide')}>
            {i18n('reservationwidget_0044')}
          </span>
        </label>
        <select value={age || selected.value}
                onChange={this._onChange} id="age" className={modify ? 'disabled-age' : false}
                disabled={modify}>
          { this.getAgeOptions() }
        </select>
        {agePolicyModal && agePolicy &&
          <GlobalModal active={agePolicyModal}
                       header={agePolicy[0].description}
                       content={<div><p>{agePolicy[0].policy_text}</p></div>}
                       cursor={['view', 'agePolicyModal', 'modal']}/>
        }
      </div>
    );
  }
}

Age.displayName = "Age";
