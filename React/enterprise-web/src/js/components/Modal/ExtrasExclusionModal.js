let ReservationCursors = require('../../cursors/ReservationCursors');
let BaobabReactMixinBranch = require('baobab-react/mixins').branch;
let ExtrasExclusionModal = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    exclusionExtrasView: ReservationCursors.keyFactsPoliciesFromReservation,
    exclusionExtrasModal: ReservationCursors.exclusionExtrasModal
  },
  render: function () {
    let extraCode = this.state.exclusionExtrasModal.code;
    let exclusionExtras = this.state.exclusionExtrasView
      .find(obj => obj.code === extraCode);
    //use description or text or empty in the worst case
    let policyExclusionDescription = _.get(exclusionExtras, 'policy_exclusions[0].policy_text') ||
                                     _.get(exclusionExtras, 'policy_exclusions[0].policy_description') ||
                                     '';
    return (
      <div>
        <h2 className="exclusion-extra-header">{exclusionExtras ? exclusionExtras.description : ''}</h2>
        <p className="exclusion-extras-text">{policyExclusionDescription}</p>
      </div>
    );
  }
});

module.exports = ExtrasExclusionModal;
