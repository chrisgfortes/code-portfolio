import Cookie from '../../utilities/util-cookies';

export default class RightPlace extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: false
    };
    this._checkboxChanged = this._checkboxChanged.bind(this);
    this.setCookie = this.setCookie.bind(this);
  }
  _checkboxChanged (event) {
    this.setState({
      checked: event.target.checked
    });
    this.setCookie(event);
  }
  setCookie (event) {
    if (event.target.checked) {
      Cookie.set(this.props.type, event.target.checked);
    } else {
      Cookie.remove(this.props.type);
    }
  }
  render () {
    let header,
      logo,
      body,
      locationInfo;
    let refer = this.props.refer;

    switch (this.props.type) {
      case "burnttree":
        header = i18n('rightplace_0001');
        logo = <img src="/etc/designs/ecom/dist/img/rightplace/burnttree.png"/>
        body = enterprise.i18nReservation.rightplace_0002;
        locationInfo = enterprise.i18nReservation.rightplace_0003;
        break;
      case "citer":
        header = i18n('rightplace_0005');
        logo = <img src="/etc/designs/ecom/dist/img/rightplace/citer.png"/>
        body = enterprise.i18nReservation.rightplace_0006;
        locationInfo = enterprise.i18nReservation.rightplace_0007;
        break;
      case "atesa":
        header = i18n('rightplace_0008');
        logo = <img src="/etc/designs/ecom/dist/img/rightplace/atesa.png"/>
        body = enterprise.i18nReservation.rightplace_0009;
        locationInfo = enterprise.i18nReservation.rightplace_0010;
        break;
      case "aem":
        header = refer.header
        logo = <img src={refer.logo}/>
        body = refer.body
        locationInfo = refer.locationInfo;
        break;
    }
    return (
      <div className="right-place-modal">
        <h2>{header}</h2>
        {logo}
        <p>{body}</p>

        <p>{locationInfo}</p>

        <div className="btn-grp cf">
          <label htmlFor="rememberCheckbox">
            <input type="checkbox"
                   onChange={this._checkboxChanged}
                   id="rememberCheckbox"
                   defaultChecked={this.state.checked}/>
            {enterprise.i18nReservation.countryofresidence_0034}
          </label>
          <button onClick={this.props.close} className="btn ok">{enterprise.i18nReservation.rightplace_0011}</button>
        </div>
      </div>
    );
  }
}

RightPlace.displayName = "RightPlace";
