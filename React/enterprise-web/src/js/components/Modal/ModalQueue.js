module.exports = {
  openModal: null,
  queue: [],
  enqueue(ready) {
    this.queue.push(ready);
    if (!this.openModal) {
      this.openModal = true;
      ready();
    }
  },
  dequeue() {
    var modal = this.queue.shift();
    if (this.queue.length) {
      this.queue[0]();
      this.openModal = true;
    } else {
      this.openModal = false;
    }
  }
};
