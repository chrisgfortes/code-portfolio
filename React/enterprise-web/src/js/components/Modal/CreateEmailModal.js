import Validator from'../../utilities/util-validator';
import EnrollmentController from '../../controllers/EnrollmentController';

export default class CreateEmailModal extends React.Component{
  constructor() {
    super();
    this.state = {
      email: null
    };
    this.validator = new Validator(this, this.fieldMap);
    this.fieldMap = this.fieldMap.bind(this);
    this._close = this._close.bind(this);
    this._confirm = this._confirm.bind(this);
    this._handleInputChange = this._handleInputChange.bind(this);
  }
  fieldMap() {
    return {
      value: {
        email: this.email.value
      },
      schema: {
        email: 'email'
      },
      refs: {
        email: this.email
      }
    }
  }
  _handleInputChange (attribute, event) {
    this.setState(
      {
        [attribute]: event.target.value
      }
    );
    EnrollmentController.setCreatePasswordDetails(attribute, event.target.value);
  }
  _close () {
    EnrollmentController.setEmailModal(false);
  }
  _confirm (e) {
    e.preventDefault();
    if (this.validator.validateAll().valid) {
      EnrollmentController.createLogin();
      this._close();
      EnrollmentController.setCreatePasswordDetails('loading', true);
    }
  }
  render() {
    return (
      <div>
	    <h3>{i18n('loyaltyenrollment_0011') || 'Email Address Required'}</h3>
	    <p>{i18n('loyaltyenrollment_0012') || 'Please provide your email address to complete your enrollment'}</p>
        <div className="field-container">
          <label htmlFor="emailAddress">{i18n('reservationwidget_0030')}</label>
          <input onChange={this._handleInputChange.bind(this, 'email')}
               id="email" type="email"
               ref={c => this.email = c}/>
        </div>
        <div className="btn-grp">
        <button onClick={this._close}
                className="btn cancel">{i18n('resflowlocations_0057')}</button>
        <button onClick={this._confirm}
                className="btn ok">{i18n('resflowextras_0010')}</button>
      </div>
	  </div>
	);
  }
}

CreateEmailModal.displayName = "CreateEmailModal";