import classNames from 'classnames';
import ModalQueue from './ModalQueue';
import DialogFocusTrap from '../../mixins/DialogFocusTrap';
import { Debugger } from '../../utilities/util-debug';
import Analytics from '../../modules/Analytics';

const GlobalModal = React.createClass({
  mixins: [
    React.addons.PureRenderMixin,
    DialogFocusTrap
  ],
  getDefaultProps () {

  },
  //Specifically so we can queue modals
  getInitialState () {
    return {
      ready: false
    };
  },
  componentWillMount () {
    //toggle body class to prevent scrolling behind modal
    $('html').addClass('modal-open');
    ModalQueue.enqueue(this.onQueueReady);
    this.logger = new Debugger(window._isDebug, this, true, 'GlobalModal.js');
    this.logger.warn('componentWillMount()');
  },
  componentDidMount () {
    Analytics.trigger('html', 'modal-open');
    window.addEventListener('keydown', this._escapeListener, true);
    if (this.props.onOpening && typeof this.props.onOpening === 'function') {
      this.props.onOpening();
    }
    let tabElement = $('.modal-container.active').find('div[role="tab"]')[0];
    if (tabElement) {
      tabElement.focus();
    } else if (this.closeButton) {
      this.closeButton.focus();
    }
  },
  componentDidUpdate () {
    if (this.props.shouldComponentUpdate !== false) {
      Analytics.trigger('html', 'modal-open');
    }
  },
  componentWillUnmount () {
    //toggle body class to re-enable scrolling of <body>
    $('html').removeClass('modal-open');
    window.removeEventListener('keydown', this._escapeListener, true);
    ModalQueue.dequeue();
    if (this.props.onClosing && typeof this.props.onClosing === 'function') {
      this.props.onClosing();
    }
  },
  isDebug: false,
  logger: {},
  _handleBlur (event) {
    if (!this.props.disableBlur) {
      let modalContent = $(this.modalContent);

      if (!$.contains(modalContent[0], event.target) && modalContent[0] !== event.target) {
        this._handleClose();
      }
    }
  },
  _handleClose () {
    if (this.props.close) {
      this.props.close();
    } else if (ReservationStateTree && this.props.cursor) {
      ReservationStateTree.set(this.props.cursor, false);
    }
  },
  _escapeListener (event) {
    if (this.props.disableBlur || this.props.hideX) return true;
    if (event.key === 'Escape' || event.keyCode === 27) {
      this._handleClose();
    }
  },
  onQueueReady () {
    this.setState({'ready': true});
  },
  _scrollTop () {
    /*
        Because modal on small screens gets scrolled to bottom
        when populated with content taller than screen height...
    */
    this.modalContent.scrollTop = 0;
  },
  render () {
    this.logger.count('render()');
    let header = null;
    let modalClasses = classNames([
      'modal-container',
      this.state.ready && 'active',
      this.props.fullScreen && 'full-screen',
      this.props.containerClass
    ]);

    let contentClasses = classNames([
      'modal-content',
      this.props.classLabel
    ]);

    if (this.props.header) {
      header = (
        <div className="modal-header">
          {this.props.header ? <span id="global-modal-title">{this.props.header}</span> : false}
          {!this.props.hideX &&
            <button
              tabIndex="0"
              onClick={this._handleClose}
              className="close-modal"
              ref={r => this.closeButton = r}
              aria-label={i18n('resflowcarselect_0065')}
            >
              <i className="icon icon-close-x-white">
                X
              </i>
            </button>}
        </div>
      );
    }
    return (
      <div ref={c => this.focusTrapWrapper = c} role={this.props.role || 'dialog'} aria-labelledby="global-modal-title"
           aria-describedby={this.props.contentText ? 'global-modal-content' : null} className={modalClasses} onClick={this._handleBlur}>
        <div className={contentClasses} ref={r => this.modalContent = r}>
          {header}
          <div className="modal-body cf">
            {this.props.children ?
              React.Children.map(
                this.props.children,
                child => React.cloneElement(child, {
                  modalScrollTop: this._scrollTop,
                  updateFocusableLoop: this.updateFocusableLoop
                })
              ) :
              <div>
                {!!this.props.contentHeader &&
                  <h2>{this.props.contentHeader}</h2>
                }
                {!!this.props.contentText &&
                  <p id="global-modal-content">{this.props.contentText}</p>
                }
                {this.props.content}
              </div>
            }
          </div>
        </div>
      </div>
    );
  }
});

module.exports = GlobalModal;
