import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';

import ReservationCursors from '../../cursors/ReservationCursors';

import GlobalModal from './GlobalModal';

import Cookie from '../../utilities/util-cookies';

const RedirectModalWrapper = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    active: ['view', 'redirect', 'modal']
  },
  render: function () {
    return (
      <div>
        {this.state.active &&
        <GlobalModal active={this.state.active}
                     disableBlur
                     header="&nbsp;"
                     classLabel="redirect-modal"
                     content={<RedirectModal />}
                     cursor={['view', 'redirect', 'modal']}/>
        }
      </div>
    );
  }
});

const RedirectModal = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  getInitialState () {
    return {
      checked: false
    };
  },
  componentWillMount () {
    Cookie.remove('defaultDomain');
    Cookie.remove('disableRedirect');
  },
  cursors: {
    redirectDomain: ['view', 'redirect', 'country'],
    type: ['view', 'redirect', 'type'],
    lang: ['view', 'redirect', 'destinationLang']
  },
  _confirm () {
    if (this.state.checked) {
      Cookie.setDays('defaultDomain', this.state.redirectDomain, 30);
    }

    if (this.state.type === 'ip') {
      let redirectUrl = enterprise.domainRedirect.domainToUrlMap[this.state.redirectDomain];
      if (redirectUrl) {
        window.location.href = window.location.protocol + '//' + redirectUrl;
      }
    } else {
      window.location.href = this.state.redirectDomain;
    }
  },
  _close () {
    if (this.state.checked) {
      Cookie.setDays('disableRedirect', 'true', 30);
    }

    ReservationStateTree.select(ReservationCursors.redirect).set('modal', false);
  },
  _checkboxChanged(event) {
    this.setState({
      checked: event.target.checked
    });
  },
  parseKey (key) {
    let lang = enterprise.i18nlocale;

    if (this.state.lang) {
      lang = this.state.lang;
    }

    let langMap = enterprise.i18nGlobalGatewayJson[lang];

    return langMap ? langMap[key] : null;
  },
  render: function () {
    if (!this.state.redirectDomain) {
      return false;
    }
    let header = this.parseKey('countryofresidence_0001');
    let content = this.parseKey('countryofresidence_0031');
    let yesButton = this.parseKey('countryofresidence_0032');
    let noButton = this.parseKey('countryofresidence_0033');
    let buttons = null;
    let checkbox = null;

    if (this.state.type === 'ip') {
      checkbox = (<label htmlFor="defaultDomainCheckbox">
        <input type="checkbox"
               onChange={this._checkboxChanged}
               id="defaultDomainCheckbox"
               defaultChecked={this.state.checked}/>
        {this.parseKey('countryofresidence_0034')}
      </label>);
      buttons = (<div className="btn-grp">
        <button onClick={this._confirm} className="btn ok">{yesButton}</button>
        <button onClick={this._close} className="btn cancel">{noButton}</button>
      </div>);
    } else {
      header = i18n('countryofresidence_0001');
      content = i18n('countryofresidence_0031');
      buttons = (<div className="btn-grp">
        <button onClick={this._close}
                className="btn cancel">{i18n('countryofresidence_0033')}</button>
        <button onClick={this._confirm}
                className="btn ok">{i18n('countryofresidence_0032')}</button>
      </div>);
    }
    return (
      <div>
        <h2>{header}</h2>

        <p>{content}</p>
        {checkbox}
        {buttons}
      </div>
    );
  }
});

module.exports = RedirectModalWrapper;
