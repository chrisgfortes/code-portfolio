import ReservationCursors from '../../cursors/ReservationCursors';
import CarSelectActions from '../../actions/CarSelectActions';
import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';

const LimitedExtrasModal = React.createClass({
  mixins: [React.addons.PureRenderMixin, BaobabReactMixinBranch],
  cursors: {
    requestModal: ReservationCursors.requestModalModal,
    requestModalOrigin: ReservationCursors.requestModalOrigin
  },
  _cancel () {
    this._closeModal();
  },
  _confirm () {
    CarSelectActions.setRequestModalConfirm(true);
    if (this.props.confirm) {
      this.props.confirm();
    }
    this._closeModal();
  },
  _closeModal () {
    CarSelectActions.toggleRequestModal(false);
  },
  render: function () {
    const modalState = this.state.requestModal
      && (this.state.requestModalOrigin === 'extras' || this.state.requestModalOrigin === 'vehicle_extras');

    return (
      <div className="modify-confirmation">
        <div>
          {modalState &&
          <h2>{i18n('resflowextras_0023')}</h2> }
          <p className="OnRequestVehicle">{i18n('resflowcarselect_0152')}</p>
          <ul>
            {modalState &&
            <li className="OnRequestVehicleList">{i18n('resflowextras_0024')}</li> }
            <li className="OnRequestVehicleList">{i18n('resflowcarselect_0154')}</li>
          </ul>
        </div>
        <div className="btn-grp cf OnRequest">
          <button onClick={this._cancel} className="btn cancel">{i18n('resflowcarselect_0155')}</button>
          <button onClick={this._confirm} className="btn ok">{i18n('reservationwidget_0014')}</button>
        </div>
      </div>
    );
  }
});

module.exports = LimitedExtrasModal;
