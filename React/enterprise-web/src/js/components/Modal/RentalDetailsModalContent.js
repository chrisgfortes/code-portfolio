import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import ReservationCursors from '../../cursors/ReservationCursors';

const RentalDetailsModalContent = React.createClass({
  mixins: [
    React.addons.PureRenderMixin,
    BaobabReactMixinBranch
  ],
  _closeModal () {
    ReservationStateTree.select(ReservationCursors.rentalDetailsModal).set(false);
  },
  render () {
    return (
      <div>
      <h2>{i18n('resflowviewmodifycancel_3001') || 'Please switch to our other site'}</h2>
        <p>{i18n('resflowviewmodifycancel_3002', {websiteUrl: this.props.url}) || 
          'This reservation was originally made on our website for another country'}</p>
        <p>{i18n('resflowviewmodifycancel_3003') ||
          'In order to view details or modify you need to visit that version of our website'}</p>
        <div className="btn-grp cf">
          <button onClick={this._closeModal}
            className="btn cancel">{i18n('resflowcarselect_0065') || 'Close'}</button>
          <a href={this.props.url}
            className="btn">{i18n('resflowviewmodifycancel_3004') || 'Switch Sites'}</a>
        </div>
      </div>
    );
  }
});

module.exports = RentalDetailsModalContent;