export default function LearnPayNowModal (){
  return (
    <div className="cf">
        <div className="paynow-header">
          <h1>{i18n('prepay_0006') || 'Pay Now'}</h1>
        </div>
        <div className="cf paynow-content-container">
          <div className="payment-content">
            <ul>
              {i18n('prepay_0008') || 'Payment'}
              <li>
                {i18n('prepay_0076') || 'The name on the reservation must match the name on the credit card.'}
              </li>
              <li>
                {i18n('prepay_0077') || 'A credit card in the renter’s name must be presented at time of pick-up, debit cards will not be accepted.'}
              </li>
              <li>
                {i18n('prepay_0009') || 'Your credit card will be securely charged online when you book your reservation'}
              </li>
            </ul>
          </div>
          <div className="payment-content">
            <ul>
              {i18n('prepay_0010') || 'Cancellation'}
              <li>
                {i18n('prepay_0011')}
              </li>
              <li>
                {i18n('prepay_0012')}
              </li>
            </ul>
          </div>
          <div className="payment-content">
            <ul>
              {i18n('prepay_0056') || 'No Show'}
              <li>
                {i18n('prepay_0057')}
              </li>
            </ul>
          </div>
          <div className="payment-content">
            <ul>
              {i18n('prepay_0078') || 'Early Returns'}
              <li>
                {i18n('prepay_0079') || 'Any unused rental days are not refundable.'}
              </li>
            </ul>
          </div>
          <div className="payment-content">
            <ul>
              {i18n('prepay_0013') || 'Modification'}
              <li>
                {i18n('prepay_0014') || 'You may modify your reservation online at any time up until 12 hours before your specified pick-up time.'}
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}
LearnPayNowModal.displayName = 'LearnPayNowModal';
