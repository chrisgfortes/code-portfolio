import ReservationStateTree from '../../stateTrees/ReservationStateTree';
import ReservationCursors from '../../cursors/ReservationCursors'
import GlobalModal from './GlobalModal';
import SessionController from '../../controllers/SessionController';
import SessionTimeoutController from '../../controllers/SessionTimeoutController';

const MODAL_COUNTDOWN_SECS = 60 * 5;
const UPDATE_DELAY = 1000;

let TimeOutModal = React.createClass({
  mixins: [React.addons.PureRenderMixin],

  _continue () {
    SessionController.updateSession();
    this._closeModal();
    SessionTimeoutController.init();
  },
  _closeModal () {
    ReservationStateTree.select(ReservationCursors.showTimeoutModal).set(false);
  },
  _timeOut () {
    SessionController.clearSession();
    location.hash = '#timedout';
    this._closeModal();
  },
  _tick () {
    let elapsed = new Date() - this.state.start;
    this.setState({elapsedSecs: Math.round(elapsed / UPDATE_DELAY)});
  },

  getInitialState () {
    return {
      start: Date.now(),
      elapsedSecs: 0
    };
  },
  componentDidMount () {
    this.timer = setInterval(this._tick, UPDATE_DELAY);
  },
  componentWillUnmount () {
    clearInterval(this.timer);
  },
  componentDidUpdate () {
    if (this.state.elapsedSecs > MODAL_COUNTDOWN_SECS) {
      this._timeOut();
    }
  },
  render () {
    const minDisplay = Math.floor((MODAL_COUNTDOWN_SECS - this.state.elapsedSecs) / 60);
    let secDisplay = (MODAL_COUNTDOWN_SECS - this.state.elapsedSecs) % 60;
    secDisplay = secDisplay < 10 ? ('0' + secDisplay) : secDisplay;

    return (
      <GlobalModal
        active
        hideX
        header={<h1 id="ariaHeader" className="sessionHeader">{i18n('sessiontimeout_0001')}</h1>}
        classLabel="sessionTimeout"
        shouldComponentUpdate={false}
      >
        <p id="dlgdesc"> {i18n('sessiontimeout_0002')} {minDisplay}:{secDisplay}</p>

        <div className="btn-grp cf">
          <button onClick={this._continue} className="btn ok">
            {i18n('sessiontimeout_0003')}
          </button>
        </div>
      </GlobalModal>
    );
  }
});

module.exports = TimeOutModal;
