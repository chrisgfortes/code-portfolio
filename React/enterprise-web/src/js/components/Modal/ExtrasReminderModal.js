import ExtrasController from '../../controllers/ExtrasController';
import classNames from 'classnames';

const ExtrasReminderModal = React.createClass({
  mixins: [React.addons.PureRenderMixin],
  getInitialState () {
    return {
      expandedExtraDetails: null
    };
  },
  addCoverage () {
    this.addCoverageExtras().then(() => {
      this.props.goToReview();
    });
  },
  addCoverageExtras () {
    let action = 'add';
    let currentCursor = 'insuranceExtras';
    let {extras, requiredExtrasIds} = this.props;
    return new Promise( resolve => {
      let promises;
      extras.forEach( (extra, index) => {
        if (requiredExtrasIds.includes(extra.code) && !extra.selected) {
          // @todo - Refactor Extras Services to better handle these cases (returning an array that is constantly changing is not a good idea)
          promises = ExtrasController.addRemoveExtra(action, index, currentCursor, true);
        }
      });
      Promise.all(promises).then(() => resolve());
    });
  },
  toggleDetails (extra) {
    const currentExtra = this.state.expandedExtraDetails;
    this.setState({
      expandedExtraDetails: extra !== currentExtra ? extra : null
    });
  },
  render () {
    const { extras, requiredExtrasIds, pickupCountryCode } = this.props;
    const requiredExtras = extras.filter(extra => requiredExtrasIds.includes(extra.code));

    const labels = {
      title: i18n('resflowextras_0009') || 'Protection Products',
      descriptions: {
        withRequiredExtras: i18n({key: 'LAC_protection_0005', countryCode: pickupCountryCode}) || 'The purchase of the protection(s) listed below will be required at the counter, unless you show valid proof of coverage:',
        withoutRequiredExtras: i18n('LAC_protection_0002') || 'We recommend adding a protection product. The purchase of protection product(s) may be required at the counter, unless you show valid proof of coverage.'
      }
    };

    const title = requiredExtras.length === 1 ? requiredExtras[0].name : labels.title;
    const description = requiredExtras.length > 0 ? labels.descriptions.withRequiredExtras : labels.descriptions.withoutRequiredExtras;

    return (
      <div className="extras-reminder-modal-content">
        <h2>{title}</h2>
        <p>{description}</p>
        {requiredExtras.length > 0 &&
          <table className="extras-required">
            {requiredExtras.map( extra => (
              <ExtrasReminderItem key={extra.code}
                                  detailsId={`extra-reminder-item-${extra.code}-details`}
                                  name={extra.name}
                                  price={i18n('resflowextras_0007', {price: extra.rate_amount_view.format})}
                                  details={extra.detailed_description || extra.description}
                                  toggleDetails={this.toggleDetails.bind(this, extra)}
                                  isExpanded={this.state.expandedExtraDetails === extra}/>
            ))}
          </table>
        }
        <div className="modal-actions">
          <button id="extras-reminder-modal-skip-button" className="btn btn-wide btn-cancel"
                  onClick={this.props.goToReview}>
            {i18n('LAC_protection_0004') || 'Skip Coverage'}
          </button>
          <button id="extras-reminder-modal-add-button" className="btn btn-wide btn-add"
                  onClick={this.addCoverage}>
            {i18n('LAC_protection_0003') || 'Add Coverage'}
          </button>
        </div>
      </div>
    );
  }
});


const getItemIconClasses = (isExpanded) => classNames([
  'icon',
  isExpanded ? 'icon-nav-carrot-up-green' : 'icon-nav-carrot-down'
]);

const getItemDetailsClasses = (isExpanded) => classNames({
  'extras-reminder-item__details': true,
  'is-expanded': isExpanded
});

function ExtrasReminderItem ({detailsId, name, price, details, isExpanded, toggleDetails}) {
  return (
    <tbody className="extras-reminder-item">
      <tr>
        <td>
          {`${name} - `}
          <button className="required-extras-details" onClick={toggleDetails}
                  aria-controls={detailsId} aria-expanded={isExpanded}
                  aria-label={i18n('resflowextras_0027', {rowName: name}) || `Details for ${name}`}>
            {i18n('resflowcarselect_0010')}
            <i className={getItemIconClasses(isExpanded)}/>
          </button>
        </td>
        <td>{price}</td>
      </tr>
      <tr id={detailsId} className={getItemDetailsClasses(isExpanded)}
          aria-hidden={!isExpanded}>
        <td colSpan="2">{details}</td>
      </tr>
    </tbody>
  );
}

module.exports = ExtrasReminderModal;
