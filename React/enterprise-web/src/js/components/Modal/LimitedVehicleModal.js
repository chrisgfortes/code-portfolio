export default function LimitedVehicleModal({controlsModal}) {
  return (
    <div className="modify-confirmation">
      <div>
          <h2>{i18n('resflowcarselect_0151')}</h2>
          <p className="OnRequestVehicle">{i18n('resflowcarselect_0152')}</p>
        <ul>
          <li className="OnRequestVehicleList">{i18n('resflowcarselect_0153')}</li>
          <li className="OnRequestVehicleList">{i18n('resflowcarselect_0154')}</li>
        </ul>
      </div>

      <div className="btn-grp cf OnRequest">
        <button onClick={controlsModal.close} className="btn cancel">{i18n('resflowcarselect_0155')}</button>
        <button onClick={controlsModal.confirm} className="btn ok">{i18n('reservationwidget_0014')}</button>
      </div>
    </div>
  );
}
LimitedVehicleModal.displayName = 'LimitedVehicleModal';