const BaobabReactMixinBranch = require('baobab-react/mixins').branch;
const GlobalModal = require('./GlobalModal');
import ReservationCursors from '../../cursors/ReservationCursors';

const RedirectToLegacyModal = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    active: ['view', 'redirectToLegacy', 'modal']
  },
  render: function () {
    return (
      <div className="">
        {this.state.active &&
        <GlobalModal active={this.state.active}
                     content={<RedirectToLegacyContent url={this.props.url} />}
                     cursor={['view', 'redirectToLegacy', 'modal']}/>
        }
      </div>
    );
  }
});

const RedirectToLegacyContent = React.createClass({
  mixins: [React.addons.PureRenderMixin],
  _cancel () {
    this._closeModal();
  },
  _confirm () {
    //window.location.href = this.props.url;
    this._closeModal();
  },
  _closeModal () {
    ReservationStateTree.select(ReservationCursors.redirectToLegacy).set('modal', false);
  },
  render: function () {
    return (
      <div className="home-modal">
        <h2>{enterprise.i18nReservation.legacyredirect_0001}</h2>

        <p>{enterprise.i18nReservation.legacyredirect_0002}</p>

        <div className="btn-grp cf">
          <button onClick={this._cancel}
                  className="btn cancel">{enterprise.i18nReservation.legacyredirect_0004}</button>
          <a href={this.props.url} target="_blank" onClick={this._confirm}
             className="btn ok">{enterprise.i18nReservation.legacyredirect_0003}</a>
        </div>
      </div>
    );
  }
});

module.exports = RedirectToLegacyModal;
