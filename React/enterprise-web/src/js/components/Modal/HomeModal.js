const ReservationCursors = require('../../cursors/ReservationCursors');

const HomeModal = React.createClass({
  mixins: [React.addons.PureRenderMixin],

  _cancel () {
    this._closeModal();
  },
  _confirm () {
    this.props.onConfirm();
    this._closeModal();
  },
  _closeModal () {
    ReservationStateTree.select(ReservationCursors.homeModal).set('modal', false);
  },
  render: function () {
    return (
      <div className="home-modal">
        <div className="btn-grp cf">
          <button onClick={this._cancel}
                  className="btn cancel">{i18n('resflowviewmodifycancel_2012')}</button>
          <button onClick={this._confirm}
                  className="btn ok">{i18n('resflowviewmodifycancel_2011')}</button>
        </div>
      </div>
    );
  }
});

module.exports = HomeModal;
