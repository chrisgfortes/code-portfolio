/**
 * @todo: This file is full of this.props.modify, but I am not certain that value is ever used.
 */
import ReservationCursors from '../../cursors/ReservationCursors';
import { PRICING, PAGEFLOW } from '../../constants';
import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import { Debugger } from '../../utilities/util-debug';

import PriceTableHeading from './PriceTableCategoryHeader';
import RatesTaxesFeesHeader from './RateTaxesFeesHeader';
import PriceTableLineItem from './PriceTableLineItem';
import MileageInfoTable from './MileageInfoTable';
import PersonalInformation from '../Confirmed/PersonalInformation';
import AdditionalInformation from '../Confirmed/AdditionalInformation';
import DeliveryCollection from '../Verification/DeliveryCollection';
import SwitchPayType from './SwitchPayType';
import CodeAndTripPurpose from '../Confirmed/CodeAndTripPurpose';
import SpecialMessage from '../Corporate/SpecialMessage';

import ReservationFlowModelController from '../../controllers/ReservationFlowModelController';
import PricingController from '../../controllers/PricingController';
import VerificationController from '../../controllers/VerificationController';
import PaymentModelController from '../../controllers/PaymentModelController';
import CorporateController from '../../controllers/CorporateController';

const { setPriceLineItemRowName } = ReservationFlowModelController;

const RateTaxesFees = React.createClass({
  mixins: [BaobabReactMixinBranch, React.addons.PureRenderMixin],
  componentWillMount () {
    this.logger = new Debugger(window._isDebug, this, true, 'RatesTaxesFees.js');
    this.logger.log('componentWillMount()');
  },
  isDebug: false,
  logger: {},
  cursors: {
    additionalInformationSaved: ReservationCursors.additionalInformationSaved,
    airlineInformation: ReservationCursors.airlineInformation,
    billingAccount: ReservationCursors.billingAccount,
    cancellationDetails: ReservationCursors.cancellationDetails,
    chargeType: ReservationCursors.chargeType,
    codeApplicable: ReservationCursors.codeApplicable,
    componentToRender: ReservationCursors.componentToRender,
    currentHash: ReservationCursors.currentHash,
    driverInformation: ReservationCursors.driverInformation,
    eligibility: ReservationCursors.reservationEligibility,
    paymentInformation: ReservationCursors.paymentInformation,
    prepay: ReservationCursors.prepay,
    renterAge: ReservationCursors.resRequestRenterAge,
    selectedCar: ReservationCursors.selectedCar,
    sessionContractDetails: ReservationCursors.sessionContractDetails,
    travelPurpose: ReservationCursors.travelPurpose,
    vehicleLogistics: ReservationCursors.vehicleLogistics,
    wasCancelRebook: ReservationCursors.wasCancelRebook
  },
  learnMore: function (e) {
    e.preventDefault();
    VerificationController.setDestinationPriceInfoModal(true);
  },
  cannotModify: function () {
    VerificationController.callCannotModify();
  },
  setPriceLineItemMidCol (midCol, item) {
    return ReservationFlowModelController.setPriceLineItemMidCol(midCol, item);
  },
  setPriceLineItemCost (item) {
    return ReservationFlowModelController.setPriceLineItemCost(item);
  },
  render () {
    this.logger.count('render()');
    const {title, modify, cancelled, showPersonal} = this.props;
    const { vehicleLogistics, selectedCar, sessionContractDetails, airlineInformation, eligibility,
            prepay, paymentInformation, chargeType, billingAccount, additionalInformationSaved, travelPurpose,
            componentToRender, wasCancelRebook, cancellationDetails, currentHash, codeApplicable, renterAge,
            driverInformation } = this.state;
    const corporateSpecialMessage = CorporateController.getSpecialMessage(sessionContractDetails);
    let pricingDifference = _.get(selectedCar, 'priceDifferences') || {};
    let paymentMethod = prepay ? PRICING.PREPAY : PRICING.PAYLATER;
    let mileageInfo = _.get(selectedCar, 'mileageInfo');
    let pricing = _.get(selectedCar, `vehicleRates[${paymentMethod}]`) && _.get(selectedCar, `vehicleRates[${paymentMethod}].price_summary`);
    let estimatedTotalValue = PricingController.getEstimatedTotalValue(sessionContractDetails, selectedCar, pricing, chargeType);
    let estimatedTotalDestinationValue = PricingController.getEstimatedTotalDestinationValue(pricing);
    let showDestinationAmount = !!pricing && pricing.estimated_total_view.code !== pricing.estimated_total_payment.code;

    let destinationCode = (showDestinationAmount) ? pricing && pricing.estimated_total_payment.code : null;
    let vehicleRate = pricing && pricing.vehicle_line_items ? pricing.vehicle_line_items : [];
    let extras = pricing && pricing.extra_line_items ? pricing.extra_line_items : [];
    let fees = pricing && pricing.fee_line_items ? pricing.fee_line_items : [];
    let miscellaneous = pricing && pricing.savings_line_items ? pricing.savings_line_items : [];
    let maskedNumber = paymentInformation ? '(' + PaymentModelController.maskCC(paymentInformation.card_details.number) + ')' : '';
    let totalLabel = prepay ? i18n('resflowreview_0118')
                            : i18n('resflowreview_0079');
    let showModifyCancel = (componentToRender === PAGEFLOW.CONFIRMED);
    let destinationAmountText = showDestinationAmount ?
      <span className="destination-amount-text">*</span> : false;

    let redemptionSavings = PricingController.getRedemptionSavings(chargeType, pricing);

    //Adjust the coupon code section heading based on if a contract is set & contract type.
    let contractType = sessionContractDetails && sessionContractDetails.contract_type && sessionContractDetails.contract_type.toLowerCase();
    let couponInfoHeading = PricingController.getCouponHeading(contractType, sessionContractDetails);


    let unpaidRefundAmount = PricingController.getUnpaidRefundAmount(pricingDifference, cancelled, prepay, cancellationDetails);
    let paymentMethodLabel = PricingController.getPaymentMethodLabel(prepay, componentToRender, unpaidRefundAmount, wasCancelRebook, maskedNumber, pricingDifference, modify, cancelled);

    let destinationContent = (showDestinationAmount) ? (i18n('reservationnav_0041', {currencyCode: destinationCode}) || `You will be charged in your destination's currency ${destinationCode}`) : null;

    let showSpecialMessage = VerificationController.showSpecialMessage(corporateSpecialMessage, currentHash);
    let showAirlineInfo = VerificationController.showAirlineInfo(showPersonal, airlineInformation);
    let showCodeAndTripPurpose = VerificationController.showTripPurpose(codeApplicable, showPersonal, sessionContractDetails);
    let showBillingAccount = VerificationController.showBillingAccount(modify, billingAccount, sessionContractDetails);
    let showDeliveryCollection = VerificationController.showDeliveryCollection(modify, vehicleLogistics);
    let showAdditionalInfo = VerificationController.showAdditionalInfo(showPersonal, modify, additionalInformationSaved);
    let showAdditionalInfoOnConfirm = VerificationController.showAdditionalInfoOnConfirm(showAdditionalInfo, componentToRender);
    let showRedemption = VerificationController.showRedemption(chargeType);
    let showSwitchPay = VerificationController.showSwitchPay(modify, pricingDifference, showPersonal);

    return (
      <div className="rate-taxes-fees-wrapper">
        <div className="rate-taxes-fees">
          <table>
              <RatesTaxesFeesHeader {...{title, showModifyCancel, chargeType, eligibility}} />
              { showSpecialMessage &&
                <tbody>
                  <tr>
                    <td colSpan="2" className="special-message-cell">
                      <SpecialMessage name={sessionContractDetails.contract_name}
                                      message={corporateSpecialMessage} />
                    </td>
                  </tr>
                </tbody>
              }

            { !!showPersonal &&
              <PersonalInformation {...{driverInformation, renterAge}}/>
            }

            { showAirlineInfo &&
              <tbody>
                <PriceTableHeading
                  id="section-header-AirlineInformation"
                  title={i18n('resflowreview_0057')}/>
                <tr>
                    <th className="airline-desc" id="AirlineInformation-row-header-1">{i18n('resflowreview_0060')}</th>
                    <td className="amount" headers="section-header-AirlineInformation AirlineInformation-row-header-1">{airlineInformation.description}</td>
                  </tr>
                   <tr>
                     <th className="airline-desc" id="AirlineInformation-row-header-2">{i18n('resflowreview_0061')}</th>
                     <td className="amount" headers="section-header-AirlineInformation AirlineInformation-row-header-2">{airlineInformation.flight_number}</td>
                   </tr>
              </tbody>
            }

            { showCodeAndTripPurpose &&
              <tbody>
                <PriceTableHeading
                  id="section-header-CodeAndTripPurpose"
                  title={couponInfoHeading}/>
                <CodeAndTripPurpose headers="section-header-CodeAndTripPurpose CodeAndTrip-row-header-1"
                                    id="CodeAndTripPurpose-row-header-1"
                                    {...{travelPurpose, sessionContractDetails}} />
              </tbody>
            }

            {!!showBillingAccount &&
              <tbody>
                <tr>
                  <td className="category-heading" colSpan="3">
                    {i18n('resflowcorporate_0207')}
                    <span className="question-modify" onClick={this.cannotModify}>?</span>
                  </td>
                </tr>
              </tbody>
            }

            {!!showBillingAccount &&
              <tbody>
                <tr>
                  <td>{sessionContractDetails.contract_billing_account}</td>
                </tr>
              </tbody>
            }

            {!!showDeliveryCollection &&
              <tbody>
                <DeliveryCollection {...{vehicleLogistics}} />
              </tbody>
            }

            {showAdditionalInfoOnConfirm &&
              <tbody className="additional-information">
                <PriceTableHeading
                  id="section-header-AdditionalInformation"
                  title={i18n('resflowcorporate_0020')}/>
                  {additionalInformationSaved.map((item, index) => {
                    if (item) {
                      return (<AdditionalInformation
                        name={item.name ? item.name : i18n('resflowconfirmation_1102')}
                        value={item.value}
                        id={'AdditionalInformation-row-header-' + (index + 1)}
                        headers={'section-header-AdditionalInformation AdditionalInformation-row-header-' + (index + 1)}/>);}
                    if (item.length <= 0) {
                      return (<AdditionalInformation
                        name={i18n('resflowconfirmation_1103')}/>);}
                  })}
              </tbody>
            }

            {!!(vehicleRate.length > 0) &&
              <tbody>
                <PriceTableHeading
                  id="section-header-VehicleInformation"
                  title={i18n('resflowreview_0092')}
                  showPersonal={showPersonal}
                  changeLabel={i18n('resflowreview_0093')}
                  destination="#cars"/>

              {vehicleRate.map((item, index) => {
                return (<PriceTableLineItem
                  rowName={setPriceLineItemRowName(selectedCar.name || item.description, false, false)}
                  middleCol={this.setPriceLineItemMidCol(true, item)}
                  cost={this.setPriceLineItemCost(item)}
                  id="VehicleInformation-row-header-1"
                  headers="section-header-VehicleInformation VehicleInformation-row-header-1"
                  key={index}/>);
              })}
              </tbody>
            }

            {!!redemptionSavings &&
              <tbody>
                <PriceTableHeading
                  id="section-header-RedemptionSavings"
                  title={i18n('resflowreview_151')}/>
                <tr>
                  <th className="redemption-name" id="RedemptionSavings-row-header-1">{i18n('resflowreview_152')}</th>
                  <td className="amount" headers="section-header-RedemptionSavings RedemptionSavings-row-header-1">
                    {i18n('resflowreview_0116', {'number': selectedCar.redemptionDaysCount, 'points': selectedCar.redemptionPointsRate})}
                  </td>
                </tr>
              </tbody>
            }

            {!!mileageInfo &&
              <tbody>
                <PriceTableHeading id="section-header-MileageInfo" title={i18n('resflowreview_0150')} />
                <MileageInfoTable mileageInfo={mileageInfo} id="MileageInfo-row-header-1" headers="section-header-MileageInfo MileageInfo-row-header-1"/>
              </tbody>
            }

            {!!(extras.length > 0) &&
              <tbody>
                <PriceTableHeading
                    id="section-header-Extras"
                    title={i18n('reservationnav_0011')}
                    showPersonal={showPersonal}
                    changeLabel={i18n('resflowreview_0301')}
                    destination="#extras"/>

                {extras.map((item, index) => (
                  <PriceTableLineItem
                    rowName={setPriceLineItemRowName(item.description, false, false)}
                    middleCol={this.setPriceLineItemMidCol(true, item)}
                    cost={this.setPriceLineItemCost(item)}
                    id={'Extras-row-header-' + (index + 1)}
                    headers={'section-header-Extras Extras-row-header-' + (index + 1)}
                    key={index}/>
                ))}
              </tbody>
            }

            {!!(miscellaneous.length > 0) &&
              <tbody>
                <PriceTableHeading id="section-header-Miscellaneous" title={i18n('resflowreview_0151')}/>

                {miscellaneous.map((item, index) => (
                  <PriceTableLineItem
                    rowName={setPriceLineItemRowName(item.description, false, false)}
                    middleCol={this.setPriceLineItemMidCol(false, item)}
                    cost={this.setPriceLineItemCost(item)}
                    id={'Miscellaneous-row-header-' + (index + 1)}
                    headers={'section-header-Miscellaneous Miscellaneous-row-header-' + (index + 1)}
                    key={index}/>
                ))}
             </tbody>
           }

            {(fees.length > 0) &&
              <tbody>
                <PriceTableHeading id="section-header-TaxesAndFees" title={i18n('reservationnav_0019')} surcharge/>

                {fees.map((item, index) => (
                  <PriceTableLineItem
                    rowName={setPriceLineItemRowName(item.description, false, false)}
                    middleCol={this.setPriceLineItemMidCol(false, item)}
                    cost={this.setPriceLineItemCost(item)}
                    id={'TaxesAndFees-row-header-' + (index + 1)}
                    headers={'section-header-TaxesAndFees TaxesAndFees-row-header-' + (index + 1)}
                    key={index}/>
                ))}
              </tbody>
            }
            <tfoot>
              <PriceTableHeading title={totalLabel}/>
              {contractType === 'corporate' &&
                <tr className="car-savings">
                  <td colSpan="2"><i className="icon icon-icon-promo-applied"/>{i18n('resflowcarselect_8010') || 'CUSTOM RATE'}</td>
                </tr>
              }
              <tr>
                {componentToRender === PAGEFLOW.VERIFICATION ?
                  false :
                  <td>
                    { billingAccount && billingAccount.billing_account_number ?
                      <div>{i18n('resflowcorporate_0207')}
                        <div>[{billingAccount.billing_account_number}]</div>
                      </div>
                      : paymentMethodLabel
                    }
                  </td>
                }
                <td className="amount" colSpan={componentToRender === PAGEFLOW.VERIFICATION ? '2' : '1'}>
                  <span className="pay-now-value">
                    {unpaidRefundAmount ? unpaidRefundAmount : estimatedTotalValue}
                    <span>{destinationAmountText}</span>
                  </span>
                </td>
              </tr>

              { !!showDestinationAmount &&
                <tr className="pay-now-tax-disclaimer">
                  <td colSpan="2">
                    <div>
                      {/*
                        i think this refund fix here will ultimately be modified such that we show
                        the destination refund amount -- but at present it is showing the wrong thing
                       */}
                      { !unpaidRefundAmount &&
                          <div className="destination-amount">
                            <span className="destination-amount-note">{destinationContent}</span>
                            <span
                              className="destination-amount-value">({estimatedTotalDestinationValue})</span>
                          </div>
                      }
                      <div className="destination-amount">
                        <span className="destination-amount-text">* {i18n('reservationnav_0020')}&nbsp;
                          <a href="#" role="button" tabIndex="0" onKeyPress={a11yClick(this.learnMore)} onClick={this.learnMore}>{i18n('resflowcarselect_0102')}</a>
                        </span>
                      </div>
                    </div>
                  </td>
                </tr>
              }
            </tfoot>
          </table>
          { showRedemption ?
            <div className="redemption-total-text">
              <div>{i18n('resflowcarselect_0064')}</div>

              <div>{i18n('resflowcarselect_0050')}</div>
            </div> : false
          }
          { showSwitchPay &&
            <SwitchPayType {...{selectedCar, prepay}} />
          }
        </div>
      </div>
    );
  }
});

module.exports = RateTaxesFees;
