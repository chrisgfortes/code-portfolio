import VerificationController from '../../controllers/VerificationController';

export default class PriceTableCategoryHeader extends React.Component{
  constructor() {
    super();
    this._onChange = this._onChange.bind(this);
  }
  _onChange () {
    location.hash = this.props.destination;
  }
  render () {
    const {id, title, changeLabel, showPersonal, surcharge} = this.props;
    let headerContent = null;

    if (changeLabel && !showPersonal) {
      headerContent = ( <span role="button" tabIndex="0" onClick={this._onChange}
                          onKeyPress={a11yClick(this._onChange)} className="edit">
                        {changeLabel}</span> );
    } else if (surcharge) {
      headerContent = ( <span role="button" tabIndex="0" onClick={() => VerificationController.callSetModal('taxesFees')}
                          onKeyPress={a11yClick(() => VerificationController.callSetModal('taxesFees'))} className="edit surcharge">
                        {enterprise.i18nReservation.resflowcarselect_0077}</span> );
    }

    return (
      <tr>
        <th className="category-heading" colSpan="2" scope="col" id={id}>
          {title}
          {headerContent}
        </th>
      </tr>
    );
  }
}

PriceTableCategoryHeader.displayName = "PriceTableCategoryHeader";
