import classNames from 'classnames';

export default function MileageInfoTable ({mileageInfo, id, headers}) {
  let label = i18n('resflowcarselect_0075');
  let overage = '';
  let mileageRowClassName;

  if (!mileageInfo.unlimited_mileage) {
    label = '' + mileageInfo.total_free_miles + ' ' + mileageInfo.distance_unit;
    overage = i18n('resflowreview_8003') + " " + mileageInfo.total_free_miles + ' ' + mileageInfo.excess_mileage_rate_view.format + '/' + mileageInfo.distance_unit;

  }
  mileageRowClassName = classNames({
    'mileage-row': true,
    'no-overage': (overage == '')
  });

  return (
    <tr className={mileageRowClassName}>
      <th id={id}>
        <span className="line-item">{label}</span>
        {!!overage &&
        <span className="line-rate">{overage}</span>
        }
      </th>
      <td headers={headers} className="amount">{i18n('reservationnav_0018')}</td>
    </tr>
  );
}

MileageInfoTable.displayName = "MileageInfoTable";
