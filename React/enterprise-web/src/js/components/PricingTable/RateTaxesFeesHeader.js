import ModifyController from '../../controllers/ModifyController';
import GlobalModal from '../Modal/GlobalModal';
import { PRICING } from '../../constants';

export default class RateTaxesFeesHeader extends React.Component{
  constructor() {
    super();
    this.state = {
      disclaimerModal: false
    };
    this._modifyReservation = this._modifyReservation.bind(this);
    this._cancelReservation = this._cancelReservation.bind(this);
  }
  _modifyReservation (event) {
    event.preventDefault();

    if (this.props.chargeType === PRICING.PREPAY && enterprise.hidePrepayModify) {
      this.setState({ disclaimerModal: true });
    } else {
      ModifyController.toggleConfirmationModal(true);
    }
  }
  _cancelReservation (event) {
    event.preventDefault();
    ModifyController.toggleRetrieveAndCancel(true);
    ModifyController.toggleCancelReservationModal(true);
  }
  render () {
    const {title, showModifyCancel, eligibility, chargeType} = this.props;
    const disclaimerModal = (!!this.state.disclaimerModal &&
      <GlobalModal
        active={true}
        close={() => this.setState({ disclaimerModal: false })}
        header={' '}
        content={i18n('resflowviewmodifycancel_0081')} />
    );

    let header = (
      <caption className="table-heading">
        <span className="beta">{title}</span>
        {disclaimerModal}
      </caption>
    );

    if (showModifyCancel) {
      const allowModify = (
        eligibility &&
        eligibility.modify_reservation &&
        !(enterprise.hideRedemptionModify && chargeType === PRICING.REDEMPTION)
      );
      const allowCancel = eligibility && eligibility.cancel_reservation;
      header = (
        <caption className="table-heading">
          <h2>
            <span className="beta">{title}</span>
          </h2>
          <div className="modify-reservation-options">
            <span className="beta">
              {allowModify &&
              <span className="modify-reservation">
                  <a href="#" role="button" tabIndex="0"
                     onClick={this._modifyReservation} onKeyPress={a11yClick(this._modifyReservation)}
                     id="modifyReservationConfirmationPage" className="grn-txt">
                    {i18n('resflowreview_0098')}
                  </a>
                  <span className="gry-txt"> {i18n('resflowlocations_0041')} </span>
              </span>
              }
              {allowCancel &&
              <a href="#" role="button" tabIndex="0"
                 onClick={this._cancelReservation} onKeyPress={a11yClick(this._cancelReservation)}
                 id="cancelReservationConfirmationPage" className="grn-txt cancel-reservation">
                {i18n('resflowreview_0099')}
              </a>
              }
            </span>
          </div>
        </caption>
      );
    } else {
      header = (
        <caption className="table-heading">
          <span className="beta">{title}</span>
        </caption> );
    }

    return header;
  }
}

RateTaxesFeesHeader.displayName = "RateTaxesFeesHeader";
