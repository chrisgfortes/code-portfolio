export default function PriceTableLineItem ({
  rowName,
  middleCol,
  cost,
  id,
  headers}) {
  return (
    <tr>
      <th id={id}>
        <span className="line-item">{rowName}</span>
        {!!middleCol &&
        <span className="line-rate">{middleCol}</span>
        }
      </th>
      <td headers={headers} className="amount">
        {cost}</td>
    </tr>
  );
}

PriceTableLineItem.displayName = "PriceTableLineItem";
