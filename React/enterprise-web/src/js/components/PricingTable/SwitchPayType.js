import PaymentModelController from '../../controllers/PaymentModelController';

export default function SwitchPayType ({selectedCar, prepay}) {
  let payAmount = null;
  if (prepay) {
    payAmount = selectedCar.vehicleRates.PAYLATER.price_summary.estimated_total_view.format;
  } else {
    payAmount = selectedCar.vehicleRates.PREPAY.price_summary.estimated_total_view.format;
  }

  return (
    <div className="switch-pay-type subvert">
      <span className="conjunction">{i18n('expedited_0033d')}</span>
      {
        prepay ?
          <span role="button" tabIndex="0" id="payLaterToggle"
               onKeyPress={a11yClick(() => PaymentModelController.switchPayment('paylater'))}
               onClick={() => PaymentModelController.switchPayment('paylater')}
               className="pay-toggle accented">
            {i18n('resflowreview_0035').replace('#{total}', payAmount)}
          </span>
          :
          <span role="button" tabIndex="0" id="payNowToggle"
               onKeyPress={a11yClick(() => PaymentModelController.switchPayment('prepay'))}
               onClick={() => PaymentModelController.switchPayment('prepay')}
               className="pay-toggle accented">
            {i18n('resflowreview_0030').replace('#{total}', payAmount)}
          </span>
      }
    </div>
  );
}

SwitchPayType.displayName = "SwitchPayType";
