import ReservationFlowModelController from '../../controllers/ReservationFlowModelController';
import { PRICING, PAGEFLOW } from '../../constants';

export default class ExtrasLineItem extends React.Component{
  constructor() {
    super();
    this.getRateType = this.getRateType.bind(this);
  }
  getRateType (itemRateType, itemRateFormat) {
    return ReservationFlowModelController.getRateType(itemRateType, itemRateFormat);
  }
  render() {
    const {id, item, vehicle, label, hideCost, hideRate} = this.props;
    let count = item.selected_equipment_quantity && item.selected_equipment_quantity > 1
        ? item.selected_equipment_quantity + 'x ' : '';
    let rowName = vehicle ? vehicle : item.name || item.description;
    let middleCol = '';
    if (item.rate_amount_view && !hideRate) {
      middleCol = label(item.rate_type, item.rate_quantity) +
       (this.getRateType(item.rate_type, item.rate_amount_view.format));
    }
    let cost = item.total_amount_view ? item.total_amount_view.format : '0';
    if (item.description === PRICING.DISCOUNT) {
      middleCol = '';
    }

    if (item.category === PAGEFLOW.INCLUDED || item.status === PAGEFLOW.INCLUDED) {
      cost = i18n('reservationnav_0018');
    }
    return (
        <div id={id} className="row">
            {!!rowName && 
            <span className="line-item">{count + rowName.toLowerCase()}</span>
            }
            {!!middleCol &&
            <span className="line-rate">{middleCol}</span>
            }
            {!(hideCost || false) &&
            <div className="amount">{cost}</div>}
        </div>
    );
  }
}

ExtrasLineItem.displayName = "ExtrasLineItem";
