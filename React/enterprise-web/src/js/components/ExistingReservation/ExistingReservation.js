import ReservationCursors from '../../cursors/ReservationCursors';
import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import ExistingReservationController from '../../controllers/ExistingReservationController';
import AuthenticatedExistingReservation from './AuthenticatedExistingReservation';
import UnauthenticatedExistingReservation from './UnauthenticatedExistingReservation';
import FedexController from '../../controllers/FedexController';
import ActionModals from '../Corporate/ActionModals';
import Error from '../Errors/Error';

const ExistingReservation = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    profile: ReservationCursors.profile,
    supportLinks: ReservationCursors.supportLinks,
    loading: ReservationCursors.existingReservationsLoading,
    errors: ReservationCursors.existingReservationsErrors,
    reservations: ReservationCursors.existingReservationsReservations,
    myTrips: ReservationCursors.myTrips,
    reservationSearchVisible: ReservationCursors.reservationSearchVisible,
    modifyModalOpen: ReservationCursors.modifyModalOpen,
    rentalDetailsModal: ReservationCursors.rentalDetailsModal
  },
  getDefaultProps () {
    return {
      isFedexLandingPage: FedexController.isFedexFlow(void 0)
    };
  },
  componentDidMount () {
    if (!this.state.profile) {
      ExistingReservationController.setLoadingState(false);
    } else {
      ExistingReservationController.setLoadingState(true);
    }
  },
  render () {
    const { loading, errors, myTrips, reservations, profile, supportLinks,
              reservationSearchVisible, modifyModalOpen, rentalDetailsModal} = this.state;

    return (
      <div className="existing-reservation cf">
        <Error errors={errors} type="GLOBAL"/>

        <ActionModals />

        <div className={loading ? 'loading' : ''}>
          {profile ?
            <AuthenticatedExistingReservation {...{myTrips, reservations, loading, profile, supportLinks,
                                                    reservationSearchVisible, modifyModalOpen, rentalDetailsModal}}/>
        :
            <UnauthenticatedExistingReservation {...{supportLinks, reservations, reservationSearchVisible,
                                                      loading, modifyModalOpen, rentalDetailsModal}}/>
        }
        </div>
      </div>
    );
  }
});

module.exports = ExistingReservation;
