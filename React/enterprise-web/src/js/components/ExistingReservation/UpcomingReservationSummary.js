import ExistingReservationController from '../../controllers/ExistingReservationController';
import ModifyController from '../../controllers/ModifyController';
import GlobalModal from '../Modal/GlobalModal';
import ModifyConfirmationModalContent from '../Modify/ModifyConfirmationModalContent';
import RentalDetailsModalContent from '../Modal/RentalDetailsModalContent';
import Address from './Address';
import Images from '../../utilities/util-images';
import {PROFILE} from '../../constants/';

export default class UpcomingReservationSummary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { disclaimerModal: false }
  }
  _modifyClick (event) {
    event.preventDefault();
    if (this.props.reservation.prepay_selected && enterprise.hidePrepayModify) {
      this.setState({disclaimerModal: true});
    } else {
      ModifyController.toggleConfirmationModal(true);
    }
  }
  render() {
    const { reservation, isAuthenticated, modifyModalOpen, rentalDetailsModal } = this.props;
    const pickupTime = moment(reservation.pickup_time).format(enterprise.i18nUnits.shortdateformat);
    const returnTime = moment(reservation.return_time).format(enterprise.i18nUnits.shortdateformat);
    const eligibility = reservation.reservation_eligibility;
    const rentalRedirectUrl = _.get(reservation, 'additional_data.redirect_url.web_redirect_url');
    const cancelRebook = reservation.cancel_rebook;
    // @todo - Pass this `car_class_details`/`vehicle_details` through CarDetailsFactory
    const carDetails = reservation.car_class_details || reservation.vehicle_details;
    const makeModelOrSimilar = carDetails ? carDetails.make_model_or_similar_text && i18n('resflowcarselect_0008', {carName: carDetails.make_model_or_similar_text}) : "";
    const carClass = carDetails ? carDetails.name : "";
    const phone = ExistingReservationController.callGetSupportPhoneNumberOfType(PROFILE.CONTACT_US);

    return (
      <div className="upcoming-reservation-summary cf">
        <div className="header cf">
          <div className="dates">{pickupTime + ' - ' + returnTime}</div>
          <div
            className="confirmation-number">{i18n('reservationwidget_0019')} {reservation.confirmation_number}</div>
        </div>
        <div className="body cf">
          <div className="section">
            <div className="title">{i18n('resflowreview_0092')}</div>
            <div className="description">
              <p>{carClass}</p>

              <p>{makeModelOrSimilar}</p>
            </div>
            <div className="overlap-car-image">
              <img
                src={carDetails && carDetails.images.ThreeQuarter && Images.getUrl(carDetails.images.ThreeQuarter.path, 200, 'high')}/>
            </div>
          </div>
          <div className="section">
            <div className="title">{enterprise.i18nReservation.reservationwidget_0008}</div>
            <div className="description">{reservation.pickup_location.name || ''}</div>
            <Address address={reservation.pickup_location.address} classStr="description"/>
          </div>
          <div className="section">
            <div className="title">{enterprise.i18nReservation.reservationwidget_0011}</div>
            <div className="description">{reservation.return_location.name || ''}</div>
            <Address address={reservation.return_location.address} classStr="description"/>
          </div>
        </div>
        <div className="action-group">
          {eligibility && eligibility.modify_reservation &&
          <span onClick={this._modifyClick.bind(this)} className="green-action-text">{i18n('resflowreview_0098')}</span>
          }
          {eligibility && eligibility.cancel_reservation &&
          <span onClick={ExistingReservationController.cancelClick.bind(null, reservation)}
                className="green-action-text">{i18n('resflowreview_0099')}</span>
          }
          {((isAuthenticated && !eligibility) || (eligibility && eligibility.view_full_reservation)) &&
          <span onClick={ExistingReservationController.detailsClick.bind(null, reservation)} className="green-action-text"
                data-reservation={reservation.confirmation_number}>{i18n('resflowreview_0302')}</span>
          }
          {eligibility && !eligibility.modify_reservation && !eligibility.cancel_reservation &&
            (rentalRedirectUrl ?
              <span className="green-action-text" onClick={ExistingReservationController.openRentalDetailsModal}>{i18n('resflowreview_0302')}</span> :
              <span className="gray-txt">{i18n('reservationwidget_0043')}
                <a className="green-action-text" href={"tel:"+phone.phone_number}>{phone.phone_number}</a>
              </span>
            )
          }
        </div>
        {modifyModalOpen &&
        <GlobalModal
          active
          header={cancelRebook ? i18n('prepay_1010') : i18n('resflowreview_0098')}
          content={<ModifyConfirmationModalContent confirm={() => ModifyController.modifyConfirmation()} cancelRebook={cancelRebook} />}
          cursor={['view', 'modify', 'modal']}/>
        }
        {rentalDetailsModal && rentalRedirectUrl &&
          <GlobalModal
            active
            header=" "
            content={<RentalDetailsModalContent url={rentalRedirectUrl}/>}
            cursor={['view', 'existingReservations', 'rentalDetailsModal']}/>
        }
      </div>
    );
  }
}

UpcomingReservationSummary.displayName = 'UpcomingReservationSummary';
