import MyTrips from './MyTrips';
import ToggleSearch from './ToggleSearch';
import ExistingReservationController from '../../controllers/ExistingReservationController';

// ECR-14250 PROFILE STUFF
export default class AuthenticatedExistingReservation extends React.Component {
  componentDidMount() {
    ExistingReservationController.checkForMyTrips();
  }
  render() {
    const { myTrips, reservations, supportLinks, loading, profile,
              reservationSearchVisible, rentalDetailsModal, modifyModalOpen } = this.props;

    const myTripsAvail = myTrips && myTrips.trip_summaries && myTrips.trip_summaries.length;

    {myTripsAvail &&
      myTrips.trip_summaries.sort((tripA, tripB) => {
        if (moment(tripA.pickup_time).isAfter(moment(tripB.pickup_time))) {
          return 1;
        }
        return -1;
      }) }

    return (
      <div className="authenticated-reservation cf">
        <h2>{
          i18n('resflowviewmodifycancel_0001', {
            firstName: profile.basic_profile.first_name,
            lastName: profile.basic_profile.last_name
          })
        }</h2>

        <div className={loading ? 'loading' : ''}>
          {myTripsAvail ?
            <MyTrips reservation={myTrips.trip_summaries[0]} tripsType={myTrips.tripsType}
                     {...{supportLinks, reservationSearchVisible, loading, modifyModalOpen, rentalDetailsModal, profile}}
                     type={'myTrips'} />
          : (reservations && reservations.length) ?
            <MyTrips reservation={reservations[0]} tripsType={myTrips.tripsType}
                     {...{supportLinks, reservationSearchVisible, loading, modifyModalOpen, rentalDetailsModal, profile}} />
          : (myTrips.error || (myTrips && myTrips.trip_summaries && !myTrips.trip_summaries.length)) ?
            <ToggleSearch {...{reservationSearchVisible, loading}} />
          : null
          }
        </div>

        <a href={enterprise.aem.path + '/account.html#reservation'} className="btn btn-next" id="viewAllMyReservationsBookingWidget">
          {enterprise.i18nReservation.resflowviewmodifycancel_0002}
        </a>
      </div>
    );
  }
}

AuthenticatedExistingReservation.displayName = 'AuthenticatedExistingReservation';
