export default function PastReservationHeader () {
  return (
    <div className="header cf">
      <div className='date-section'>
        {enterprise.i18nReservation.reservationwidget_0009}
      </div>
      <div className='location-section'>
        {enterprise.i18nReservation.reservationnav_0005}
      </div>
      <div className='confirmation-section'>
        {enterprise.i18nReservation.reservationwidget_0019}
      </div>
      <div className="print-section">
      </div>
    </div>
  );
}
