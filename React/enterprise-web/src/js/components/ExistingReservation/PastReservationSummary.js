import PastReservationHeader from './PastReservationHeader';
import PastReservationBody from './PastReservationBody';

export default function PastReservationSummary ({ reservation, supportLinks }) {
  return (
    <div className="past-reservation-summary cf">
      <PastReservationHeader />
      <PastReservationBody {...{ reservation, supportLinks }}/>
    </div>
  );
}

PastReservationSummary.displayName = 'PastReservationSummary';
