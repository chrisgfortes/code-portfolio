import ExistingReservationController from '../../controllers/ExistingReservationController';
import Address from './Address';
import {PROFILE} from '../../constants';

export default class PastReservationBody extends React.Component {
  constructor(props){
    super(props);
  }
  render() {
    const {reservation, supportLinks} = this.props;
    const pickupTime = moment(reservation.pickup_time).format(enterprise.i18nUnits.shortdateformat);
    const confirmationNumber = reservation.invoice_number ? reservation.invoice_number : reservation.confirmation_number;
    const cancelled = reservation.reservation_status == "CN" ? true : null;
    const phone = cancelled ? ExistingReservationController.callGetSupportPhoneNumberOfType(PROFILE.CONTACT_US) : '';

    return (
      <div className="body cf">
        <div className='date-section'>
          {pickupTime}
        </div>
        <div className='location-section'>
          <div> {reservation.pickup_location.name || ''} </div>
          {reservation.pickup_location.address &&
            <Address address={reservation.pickup_location.address} classStr=""/>
          }
        </div>
        <div className='confirmation-section'>
          {confirmationNumber}
        </div>
        <div className="print-section">
          {cancelled &&
          <div>
            <span className="gray-txt">{enterprise.i18nReservation.resflowviewmodifycancel_0057}</span>
            <br />
            {phone &&
            <span className="gray-txt">{enterprise.i18nReservation.reservationwidget_0045} <a
              className='green-action-text' href={"tel:"+phone.phone_number}>{phone.phone_number}</a></span>
            }
          </div>
          }
          {!cancelled &&
          <a target="_blank" href={supportLinks && supportLinks.print_receipt_url}
             onClick={(e)=> ExistingReservationController.viewReceipt(e, reservation)}
             className={(!reservation.confirmation_number ? 'hide' : '')+' green-action-text'}>{enterprise.i18nReservation.resflowviewmodifycancel_0027}</a>}
        </div>
      </div>
    );
  }
}

PastReservationBody.displayName = 'PastReservationBody';
