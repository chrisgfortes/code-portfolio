export default function Address ({address, classStr}) {
  return (
    <div>
      {address.street_addresses.map((address, index) => {
        return <div key={index} className="description">{address}</div>
      })}
      <div className={classStr}>
        {address.city}, {address.country_subdivision_code} {address.postal} {address.country_code}
      </div>
    </div>
  );
}

Address.displayName = 'Address';
