import CurrentReservationSummary from './CurrentReservationSummary';
import UpcomingReservationSummary from './UpcomingReservationSummary';
import PastReservationSummary from './PastReservationSummary';
import ToggleSearch from './ToggleSearch';
import ExistingReservationController from '../../controllers/ExistingReservationController';
import { exists } from '../../utilities/util-predicates';


export default function MyTrips ({type, tripsType, reservation, supportLinks, reservationSearchVisible,
                                  loading, modifyModalOpen, noSearch, rentalDetailsModal, profile}) {
  return (
    <div>
      <h2>{ExistingReservationController.getString( type, tripsType )}</h2>
      { tripsType === 'NO_TRIPS_AVAILABLE' ?
          !noSearch &&
          <ToggleSearch {...{reservationSearchVisible, loading}} />
        : tripsType === 'UPCOMING' ?
          <UpcomingReservationSummary
            isAuthenticated={exists(profile)}
            {...{reservation, modifyModalOpen, rentalDetailsModal}} />
        : tripsType === 'CURRENT' ?
          <CurrentReservationSummary
            reservation={reservation}
            supportLinks={supportLinks} />
        : tripsType === 'PAST' ?
          <PastReservationSummary
            reservation={reservation}
            supportLinks={supportLinks} />
        :
        null
      }
    </div>
  )
}
