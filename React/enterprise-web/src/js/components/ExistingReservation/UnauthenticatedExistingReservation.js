import Search from './Search';
import MyTrips from './MyTrips';

export default function UnauthenticatedExistingReservation ({supportLinks, reservations, reservationSearchVisible,
                                                              loading, modifyModalOpen, rentalDetailsModal}) {
  return (
    <div className="unauthenticated-reservation cf">
      <Search {...{reservationSearchVisible, loading}} />
        {reservations && reservations.length > 0 &&
        <div className="reservation-list">
          <MyTrips tripsType={reservations[0].reservation_type}
                   reservation={reservations[0]}
                   noSearch
                   {...{supportLinks, reservationSearchVisible, loading, modifyModalOpen, rentalDetailsModal}}/>
        </div>
        }
    </div>
  );
}

UnauthenticatedExistingReservation.displayName = 'UnauthenticatedExistingReservation';
