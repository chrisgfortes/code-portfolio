import ExistingReservationController from '../../controllers/ExistingReservationController';
import Search from './Search';

function toggleSearch() {
  this.setState({
    showSearch: !this.state.showSearch
  });
  ExistingReservationController.callSetReservationSearchVisibility(!this.state.showSearch);
}

export default class ToggleSearch extends React.Component {
  constructor (props) {
    super(props);
    this.state = { showSearch: false }
  }
  render() {
    const {showSearch} = this.state;
    const {reservationSearchVisible, loading} = this.props;
    return (
      <div className="toggle-search-reservation cf">
        <p>{enterprise.i18nReservation.resflowviewmodifycancel_0016}
          <span onClick={toggleSearch.bind(this)}
                className='green-action-text'>&nbsp;{enterprise.i18nReservation.reservationwidget_0018}</span>
        </p>

        <div className={showSearch ? '' : 'hide'}>
          <Search {...{reservationSearchVisible, loading}} />
        </div>
      </div>
    );
  }
}

ToggleSearch.displayName = ToggleSearch;
