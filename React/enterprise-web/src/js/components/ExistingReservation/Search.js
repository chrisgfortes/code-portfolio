import ReservationFlowModelController from '../../controllers/ReservationFlowModelController';
import ExistingReservationController from '../../controllers/ExistingReservationController';
import Validator from '../../utilities/util-validator';
import FedexController from '../../controllers/FedexController';

export default class Search extends React.Component {
  constructor(props) {
    super(props);
    this.validator = new Validator(this, this.fieldMap);
    this.state = {
      firstName: null,
      lastName: null,
      confirmation: null,
      requisitionNumber: null,
      employeeNumber: null
    }
  }
  fieldMap () {
    return this.props.isFedexLandingPage ?
      ({
        refs: {
          confirmation: this.confirmation,
          firstName: this.firstName,
          lastName: this.lastName,
          requisitionNumber: this.requisitionNumber,
          employeeNumber: this.employeeNumber
        },
        value: {
          confirmation: this.confirmation.value,
          firstName: this.firstName.value,
          lastName: this.lastName.value,
          requisitionNumber: this.requisitionNumber.value,
          employeeNumber: this.employeeNumber.value
        },
        schema: {
          confirmation: 'number',
          firstName: 'string',
          lastName: 'string',
          requisitionNumber: 'string',
          employeeNumber: 'number'
        }
      })
    :
    ({
      refs: {
        confirmation: this.confirmation,
        firstName: this.firstName,
        lastName: this.lastName
      },
      value: {
        confirmation: this.confirmation.value,
        firstName: this.firstName.value,
        lastName: this.lastName.value
      },
      schema: {
        confirmation: 'number',
        firstName: 'string',
        lastName: 'string'
      }
    });
  }
  _confirmationChange (event) {
    this.setState({
      confirmation: event.target.value
    });
  }
  _firstNameChange (event) {
    this.setState({
      firstName: event.target.value
    });
  }
  _lastNameChange (event) {
    this.setState({
      lastName: event.target.value
    });
  }
  _requisitionNumberChange (event) {
    this.setState({
      requisitionNumber: event.target.value
    });

    ReservationFlowModelController.setFedexRequisitionNumber(event.target.value);
  }
  _employeeNumberChange (event) {
    this.setState({
      employeeNumber: event.target.value
    });

    ReservationFlowModelController.setEmployeeNumber(event.target.value);
  }
  _findReservation () {
    const { confirmation, firstName, lastName } = this.state;

    if (this.validator.validateAll().valid) {
      ExistingReservationController
        .getExistingReservation(
          confirmation, firstName, lastName,
          () => {
            if (ExistingReservationController.getReservationHasLength()) {
              ExistingReservationController.setReservationSearchVisibility(false);
            }
          }
        );
    }
  }
  _onKeyUp (event) {
    if (event.keyCode === 13) {
      if (this.state.confirmation && this.state.lastName && this.state.firstName) {
        this._findReservation();
      }
    }
  }
  render () {
    const { loading, reservationSearchVisible, isFedexLandingPage } = this.props;
    return (
      <div className={'existing-reservation-search ' + (reservationSearchVisible ? '' : 'hidden')} onKeyUp={this._onKeyUp.bind(this)}>
        <div className={(loading ? ' loading' : '')}></div>
        <fieldset>
          <span className="beta">
            {isFedexLandingPage ?
              (i18n('fedexcustompath_0019') || 'FEDEX RENTAL LOOKUP')
              :
              enterprise.i18nReservation.reservationwidget_0018}
          </span>
          <span className="required-msg">{enterprise.i18nReservation.resflowreview_0004}</span>

          {
            isFedexLandingPage &&
            <div className="g g-2up cf">
              <div className="field-container gi">
                <label htmlFor="text">
                  {i18n('fedexcustompath_0017') || 'Requisition Number'}
                  &nbsp;
                  <span className="tooltip-v2" tabIndex="0" aria-describedby="reqNbrTooltip">
                    (<span className="green">
                      <span>{i18n('feddexmoreinfo') || 'more info'}</span>
                      <span id="reqNbrTooltip" className="tooltip" role="tooltip">
                        {i18n('fedexcustompath_0018') || '9 or 10 digits long, first 4 are alpha next 5 are alpha numeric'}
                      </span>
                    </span>)
                  </span>
                </label>

                <input id="text" type="text" ref={c => this.requisitionNumber = c}
                  value={this.state.requisitionNumber} onChange={this._requisitionNumberChange.bind(this)}
                  placeholder=""/>
              </div>

              <div className="field-container gi">
                <label htmlFor="text">
                  {i18n('fedexcustompath_0016') || 'FedEx Employee ID'}
                  &nbsp;
                  <span className="tooltip-v2" tabIndex="0" aria-describedby="employeeIdTooltip">
                    (<span className="green">
                      <span>{i18n('feddexmoreinfo') || 'more info'}</span>
                      <span id="employeeIdTooltip" className="tooltip" role="tooltip">
                        {i18n('fedexcustompath_0008') || 'FedEx Location should provide this number. (Example: ABCD102030).'}
                      </span>
                    </span>)
                  </span>
                </label>

                <input id="text" type="text" ref={c => this.employeeNumber = c}
                  value={this.state.employeeNumber} onChange={this._employeeNumberChange.bind(this)}
                  placeholder=""/>
              </div>
            </div>
          }

          <div className="g g-1up">
            <div className="field-container gi">
              <label htmlFor="text">{enterprise.i18nReservation.reservationwidget_0019}</label>
              <input id="text" type="text" ref={c => this.confirmation = c} value={this.state.confirmation}
                     onChange={this._confirmationChange.bind(this)} placeholder=""/>
            </div>
          </div>
          <div className="g g-2up">
            <div className="field-container gi">
              <label htmlFor="text">{enterprise.i18nReservation.reservationwidget_0021}</label>
              <input id="text" type="text" ref={c => this.firstName = c} value={this.state.firstName} onChange={this._firstNameChange.bind(this)}
                     placeholder=""/>
            </div>
            <div className="field-container gi">
              <label htmlFor="text">{enterprise.i18nReservation.reservationwidget_0022}</label>
              <input id="text" type="text" ref={c => this.lastName = c} value={this.state.lastName} onChange={this._lastNameChange.bind(this)}
                     placeholder=""/>
            </div>
          </div>

          <a className="btn btn-next"
             onClick={this._findReservation.bind(this)} onKeyPress={a11yClick(this._findReservation.bind(this))}
             role="button" tabIndex="0">
             {enterprise.i18nReservation.reservationwidget_0023}
          </a>
        </fieldset>
      </div>
    );
  }
}

Search.defaultProps = {
  isFedexLandingPage: FedexController.isFedexLandingPage()
}

Search.displayName = 'Search';
