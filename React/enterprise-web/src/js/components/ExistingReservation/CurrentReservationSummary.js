import ExistingReservationController from '../../controllers/ExistingReservationController';
import Address from './Address';
import {PROFILE} from '../../constants';

export default function CurrentReservationSummary ({reservation}) {
  const pickupTime = moment(reservation.pickup_time).format(enterprise.i18nUnits.shortdateformat);
  const returnTime = moment(reservation.return_time).format(enterprise.i18nUnits.shortdateformat);
  // @todo - Pass this `car_class_details`/`vehicle_details` through CarDetailsFactory
  const carDetails = reservation.car_class_details || reservation.vehicle_details;
  const carClass = carDetails ? carDetails.name : "";
  const confirmationNumber = !confirmationNumber ? reservation.confirmation_number : reservation.ticket_number;
  let contactUsTel = ExistingReservationController.callGetSupportPhoneNumberOfType(PROFILE.CONTACT_US);
  let roadSideAssistanceTel = ExistingReservationController.callGetSupportPhoneNumberOfType(PROFILE.ROADSIDE_ASSISTANCE);

  contactUsTel = contactUsTel && contactUsTel.phone_number;
  roadSideAssistanceTel = roadSideAssistanceTel && roadSideAssistanceTel.phone_number;

  return (
    <div className="current-reservation-summary cf">
      <div className="header cf">
        <div className="dates">{pickupTime + ' - ' + returnTime}</div>
        <div className="confirmation-number">{enterprise.i18nReservation.reservationwidget_0019}
          : {confirmationNumber}
        </div>
      </div>
      <div className="body cf">
        <div className="left-section">
          <dl>
            <dt>{enterprise.i18nReservation.reservationnav_0009}:</dt>
            <dd>
              <div className="description">{carClass}</div>
              <div className="description">{carDetails.make_model_or_similar_text && enterprise.i18nReservation.resflowcarselect_0008.replace("#{carName}", carDetails.make_model_or_similar_text)}</div>
              <div className="description">{carDetails.make && "Make: " + carDetails.make}</div>
              <div className="description">{carDetails.model && "Model: " + carDetails.model}</div>
              <div className="description">{carDetails.color && "Color: " + carDetails.color}</div>
              <div className="description">{carDetails.license_plate && "License Plate: " + carDetails.license_plate}</div>
            </dd>

            <dt>{enterprise.i18nReservation.reservationnav_0005}:</dt>
            <dd>
              <div>{reservation.pickup_location.name || ''}</div>
              <Address address={reservation.pickup_location.address} classStr=""/>
            </dd>

            <dt>{enterprise.i18nReservation.reservationnav_0006}:</dt>
            <dd>
              <div>{reservation.return_location.name || ''}</div>
              <Address address={reservation.return_location.address} classStr=""/>
            </dd>
          </dl>
        </div>
        <div className="right-section">
          <dl>
            <div className="cf">
              <dt>{enterprise.i18nReservation.resflowviewmodifycancel_0009}</dt>
              <dd><a href={"tel:"+contactUsTel}>{contactUsTel}</a></dd>
            </div>
            <div className="cf">
              <dt>{enterprise.i18nReservation.resflowviewmodifycancel_0010}</dt>
              <dd><a href={"tel:"+roadSideAssistanceTel}>{roadSideAssistanceTel}</a></dd>
            </div>
          </dl>
        </div>
      </div>
    </div>
  );
}

CurrentReservationSummary.displaynName = 'CurrentReservationSummary';
