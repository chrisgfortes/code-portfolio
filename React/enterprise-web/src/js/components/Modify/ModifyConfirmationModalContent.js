import ModifyController from '../../controllers/ModifyController';

export default class ModifyConfirmationModalContent extends React.Component {
  constructor (props) {
    super(props);
  }
  cancel() {
    this.closeModal();
  }
  confirm() {
    if (this.props.confirm) {
      this.props.confirm();
    }
  }
  closeModal() {
    ModifyController.toggleConfirmationModal(false);
  }
  render () {
    return (
      <div className="modify-confirmation">
        <h2>{this.props.cancelRebook ? i18n('prepay_1010') : i18n('resflowreview_0098')}</h2>

        <p>{this.props.cancelRebook ? i18n('prepay_1013') : i18n('resflowviewmodifycancel_0030')}</p>

        <div className="btn-grp cf">
          <button onClick={this.cancel.bind(this)}
                  className="btn cancel">{enterprise.i18nReservation.resflowviewmodifycancel_0031}</button>
          <button onClick={this.confirm.bind(this)}
                  className="btn ok">{enterprise.i18nReservation.resflowviewmodifycancel_0032}</button>
        </div>
      </div>
    );
  }
}

ModifyConfirmationModalContent.displayName = 'ModifyConfirmationModalContent';
