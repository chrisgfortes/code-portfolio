import BookingWidgetModelController from '../../controllers/BookingWidgetModelController';
import ReservationFlowModelController from '../../controllers/ReservationFlowModelController';

export default class ConfirmationModalContent extends React.Component {
  constructor (props) {
    super(props);
  }
  cancel() {
    this.closeModal();
    BookingWidgetModelController.toggleInflightModify(false);
    location.hash = ReservationFlowModelController.getLastCompleteStepHash();
  }
  confirm() {
    if (this.props.confirm){ this.props.confirm(); }
    this.closeModal();
    BookingWidgetModelController.toggleInflightModify(false);
  }
  closeModal() {
    BookingWidgetModelController.toggleInflightModifyModal(false);
  }
  render () {
    return (
      <div className="modify-confirmation">
        <div className="btn-grp cf">
          <button onClick={this.cancel.bind(this)}
                  className="btn cancel">{i18n('resflowviewmodifycancel_2003')}</button>
          <button onClick={this.confirm.bind(this)}
                  className="btn ok">{i18n('resflowviewmodifycancel_2002')}</button>
        </div>
      </div>
    );
  }
}

ConfirmationModalContent.displayName = 'ConfirmationModalContent';
