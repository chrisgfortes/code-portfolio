import CancellationController from '../../controllers/CancellationController';

export default class CancelModifyModalContent extends React.Component {
  constructor (props) {
    super(props);
  }
  confirm() {
    if (this.props.confirm) { this.props.confirm(); }
    this.closeModal();
  }
  closeModal() {
    CancellationController.setModifyCancelModal();
  }
  render (){
    return (
      <div className="modify-confirmation">
        <p>{this.props.content}</p>

        <div className="btn-grp cf">
          <button onClick={this.closeModal.bind(this)}
                  className="btn cancel">{enterprise.i18nReservation.resflowcorporate_0201}</button>
          <button onClick={this.confirm.bind(this)}
                  className="btn ok">{enterprise.i18nReservation.resflowcorporate_0200}</button>
        </div>
      </div>
    );
  }
}

CancelModifyModalContent.displayName = 'CancelModifyModalContent ';
