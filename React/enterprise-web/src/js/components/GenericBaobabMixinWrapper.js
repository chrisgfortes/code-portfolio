import { root as BaobabReactMixinRoot } from 'baobab-react/mixins';

const  GenericBaobabMixinWrapper = React.createClass({
  mixins: [BaobabReactMixinRoot, React.addons.PureRenderMixin],
  render: function () {
    return (
      <div>
        <this.props.component modelController={this.props.modelController}/>
      </div>
    );
  }
});

export default GenericBaobabMixinWrapper;
