const Spinnerprise = React.createClass({
  render () {
    return (
      <div className="spinner-container">
        <div className="spinner-circle" aria-valuetext={i18n('wcag_nav_0006') || 'Loading'} role="progressbar" tabIndex="0"></div>
      </div>
    );
  }
});

module.exports = Spinnerprise;
