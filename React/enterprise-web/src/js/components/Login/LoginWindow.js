import Unauthenticated from './Unauthenticated';
import Authenticated from './Authenticated';
import classNames from 'classnames';

export default function LoginWindow ({authenticated, brand, supportLinks, errors, loyaltyBrand, isFedexReservation}) {
  let navContentStyles = classNames({
    'authenticated': authenticated,
    'ec': brand === "EC",
    'utility-nav-content': true
  });
  let loginWindowContent = authenticated ? <Authenticated errors={errors} brand={brand} profile={authenticated} /> :
    <Unauthenticated
      supportLinks={supportLinks}
      errors={errors}
      loyaltyBrand={loyaltyBrand}
      isFedexReservation={isFedexReservation} />;

  return (
    <div className={navContentStyles}>
      <fieldset>
        <legend>{i18n('login_0001')}</legend>
        {loginWindowContent}
        <div className="field-container left-container">
          <h4>{i18n('loyaltysignin_0008')}</h4>
          <a href={enterprise.signInNavLinks.loyalty}><span
            className="icon icon-nav-carrot-white"/>{i18n('login_0003')}</a>
          <a href={enterprise.signInNavLinks.enroll}><span
            className="icon icon-nav-carrot-white"/>{i18n('resflowconfirmation_0012')}</a>
          <h4>{i18n('loyaltysignin_0034')}</h4>
          <a href={enterprise.signInNavLinks.activate}><span
            className="icon icon-nav-carrot-white"/>{i18n('loyaltysignin_0035')}</a>
        </div>
      </fieldset>
    </div>
  );
}

LoginWindow.displayName = "LoginWindow";
