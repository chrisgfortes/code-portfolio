import LoginController from '../../controllers/LoginController';
import Error from '../Errors/Error';
import classNames from 'classnames';

export default class Authenticated extends React.Component{
  constructor(props) {
    super(props);
    LoginController.triggerLoginWidget(this.props.profile.basic_profile);
    this._signOut = this._signOut.bind(this);
  }
  _signOut (e) {
    e.preventDefault();
    LoginController.setLogoutModal(true);
  }
  render () {
    const {profile, brand, errors} = this.props;
    let basicProfile;
    let loyaltyTier;

    // ECR-14250
    if (profile) {
      basicProfile = profile.basic_profile,
      loyaltyTier = basicProfile.loyalty_data.loyalty_tier;
    }

    let loyaltyClass = classNames({
      "tier-banner": true,
      "plus": loyaltyTier === "Plus",
      "silver": loyaltyTier === "Silver",
      "gold": loyaltyTier === "Gold",
      "platinum": loyaltyTier === "Platinum"
    });

    if (brand === "EC") {
      return (

        <div className="field-container logged-in">
          <i className="icon icon-brand-national"/>

          <h2>{i18n('loyaltysignin_0030')}</h2>

          <Error type="GLOBAL"
                 errors={errors}
                 allowClose={true}/>

          <div>
            <p className="ec-description">{i18n('loyaltysignin_0017')}</p>

            <div className="member-info">
              {profile ? basicProfile.first_name + ' ' + basicProfile.last_name + ' ' : null}
              <div>{profile ? i18n('loyaltysignin_0031').replace('#{number}', basicProfile.loyalty_data.loyalty_number) : null}</div>
            </div>
          </div>
          <a onClick={this._signOut} href="#">{i18n('reservationwidget_0028')}</a>
        </div>
      )
    } else {
      return (
        <div className="field-container logged-in">
          <h2>{i18n('eplusaccount_0026')}</h2>

          <Error type="GLOBAL"
                 errors={errors}
                 allowClose={true}/>

          <div className="loyalty-container">
            <div className="member-info">
              <img src="/etc/designs/ecom/dist/img/icons/png/eplus-logo.png" alt=""/>

              <div className="user-name">
                {profile ? basicProfile.first_name + ' ' + basicProfile.last_name : null}
              </div>
              <div className="loyalty-number">
                #{profile ? basicProfile.loyalty_data.loyalty_number : null}
              </div>
            </div>
            <div className={loyaltyClass}>
              <i className="icon icon-Ent-Icon-plus"/>

              <div className="tier-label">
                <span className="tier">{profile ? loyaltyTier : null}</span>
                <small> {i18n('eplusaccount_0105')}</small>
              </div>
            </div>
            <div className="points-container">
              {profile ? basicProfile.loyalty_data.points_to_date : null}
              {i18n('login_0006')}
              <small>{i18n('resflowcarselect_0048') && i18n('resflowcarselect_0048').replace("#{date}", moment().format('l'))}</small>
            </div>
          </div>

          <a
            href={enterprise.aem.path + '/account.html#reservation'}>{i18n('reservationwidget_0004')}</a>
          <a href={enterprise.aem.path + '/account.html#reward'}>{i18n('eplusaccount_0033')}</a>
          <a href={enterprise.aem.path + '/account.html#settings'}>{i18n('eplusaccount_0034')}</a>
          <a href={enterprise.aem.path + '/loyalty.html'}>{i18n('eplusaccount_0035')}</a>

          <div className="logout" onClick={this._signOut}>{i18n('reservationwidget_0028')}</div>

        </div>
      );
    }
  }
}

Authenticated.displayName = "Authenticated";
