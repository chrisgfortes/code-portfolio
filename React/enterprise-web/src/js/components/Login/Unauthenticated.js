import Validator from '../../utilities/util-validator';
import Error from '../Errors/Error';
import classNames from 'classnames';
import LoginController from '../../controllers/LoginController';
import scrollUtil from '../../utilities/util-scroll';
import SessionController from '../../controllers/SessionController';
import { PARTNERS } from '../../constants';

export default class Unauthenticated extends React.Component{
  constructor(props) {
    super(props);
    const isEcUnauthReservation = this.props.loyaltyBrand === PARTNERS.BRAND.EMERALD_CLUB;
    const isEcUnauthPage = enterprise.currentView === 'ecloyaltypage';
    const isFedexReservation = this.props.isFedexReservation;

    let defaultLoginType;
    if (enterprise.b2b === 'fedex' || isFedexReservation || isEcUnauthPage || isEcUnauthReservation) {
      defaultLoginType = PARTNERS.BRAND.EMERALD_CLUB;
    } else {
      defaultLoginType = PARTNERS.BRAND.ENTERPRISE_PLUS;
    }
    this.state = {
      epUsername: null,
      epPassword: null,
      epRememberMe: false,
      ecUsername: null,
      ecPassword: null,
      current: defaultLoginType,
      submitLoading: false
    };
    this.validator = new Validator(this, this.fieldMap);
    this.fieldMap = this.fieldMap.bind(this);
    this._handleInputChange = this._handleInputChange.bind(this);
    this._onRememberMe = this._onRememberMe.bind(this);
    this._login = this._login.bind(this);
    this._handleKeyUp = this._handleKeyUp.bind(this);
    this.processLogin = this.processLogin.bind(this);
    this._onPanelToggle = this._onPanelToggle.bind(this);
    this._forgot = this._forgot.bind(this);
    this._scrollTopMobile = this._scrollTopMobile.bind(this);
    this._ecToggleKeypress = this._ecToggleKeypress.bind(this);
    this._epToggleKeypress = this._epToggleKeypress.bind(this);
  }
  fieldMap () {
    return {
      refs: {
        epUsername: this.epUsername,
        epPassword: this.epPassword,
        ecUsername: this.ecUsername,
        ecPassword: this.ecPassword
      },
      value: {
        epUsername: this.epUsername.value,
        epPassword: this.epPassword.value,
        ecUsername: this.ecUsername.value,
        ecPassword: this.ecPassword.value
      },
      schema: {
        epUsername: 'string',
        epPassword: 'password',
        ecUsername: 'string',
        ecPassword: 'string'
      }
    };
  }
  _onRememberMe (brand, event) {
    this.setState({
      [brand]: event.target.checked
    });
  }
  _onPanelToggle (brand) {
    if (!this.state.submitLoading) {
      this.setState({
        current: brand
      });
    }
    this._scrollTopMobile();
  }
  _forgot () {
    LoginController.getForgotPassword(this.props.supportLinks);
  }
  _handleInputChange (attribute, event) {
    this.setState(
      {
        [attribute]: event.target.value
      }
    );
    this.validator.validate(attribute, event.target.value);
  }
  _handleKeyUp (brand, event) {
    if (event.keyCode === 13) {
      this._login(brand);
    }
  }
  _scrollTopMobile () {
    const windowWidth = window.screen.width;

    if (windowWidth < 716) {
      scrollUtil.scrollToOffset(0, 0);
    }
  }
  _login (brand) {
    if (brand === PARTNERS.BRAND.ENTERPRISE_PLUS) {
      const username = this.validator.validate('epUsername', this.epUsername.value);
      const password = this.validator.validate('epPassword', this.epPassword.value);

      if (password && username && !this.state.submitLoading) {
        const params = {
          username: this.epUsername.value,
          password: this.epPassword.value,
          rememberMe: this.state.epRememberMe,
          brand: PARTNERS.BRAND.ENTERPRISE_PLUS
        };

        this.processLogin(params);
      }
    } else if (brand === PARTNERS.BRAND.EMERALD_CLUB) {
      const username = this.validator.validate('ecUsername', this.ecUsername.value);
      const password = this.validator.validate('ecPassword', this.ecPassword.value);

      if (password && username && !this.state.submitLoading) {
        const params = {
          username: this.ecUsername.value,
          password: this.ecPassword.value,
          brand: PARTNERS.BRAND.EMERALD_CLUB
        };

        this.processLogin(params);
      }
    }
  }
  processLogin (params) {
    this.setState({submitLoading: true});
    LoginController.login(params).then(() => {
      this.setState({submitLoading: false});
      SessionController.callGetSession(true, false, true);
    });
  }
  _epToggleKeypress () {
    a11yClick(this._onPanelToggle.bind(this, PARTNERS.BRAND.ENTERPRISE_PLUS)).apply(this, arguments);
    setTimeout(() => this.epUsername.focus(), 1000);
  }
  _ecToggleKeypress () {
    a11yClick(this._onPanelToggle.bind(this, PARTNERS.BRAND.EMERALD_CLUB)).apply(this, arguments);
    setTimeout(() => this.ecUsername.focus(), 1000);
  }
  render () {
    let loginButtonClasses = classNames({
      'btn': true,
      'disabled': this.state.submitLoading
    });
    let enterpriseLoginClasses = classNames({
      'enterprise-login': true,
      'active': this.state.current === PARTNERS.BRAND.ENTERPRISE_PLUS
    });
    let emeraldLoginClasses = classNames({
      'emerald-club-login': true,
      'active': this.state.current === PARTNERS.BRAND.EMERALD_CLUB
    });
    let enterpriseToggle = classNames({
      'enterprise-plus': true,
      'disabled': this.state.submitLoading,
      'active': this.state.current === PARTNERS.BRAND.EMERALD_CLUB
    });
    let emeraldToggle = classNames({
      'emerald-club': true,
      'disabled': this.state.submitLoading,
      'active': this.state.current === PARTNERS.BRAND.ENTERPRISE_PLUS
    });

    return (
      <div className="field-container right-container">
        <Error type="GLOBAL"
               errors={this.props.errors}
               allowClose={true}/>

        <div className="mask">
          <div className={enterpriseLoginClasses}>
            <div className="login-field-container">
              <h4><i className="icon icon-eplus-logo" aria-hidden="true" role="presentation"/>{i18n('loyaltysignin_0001') || 'Sign In to Enterprise Plus'}</h4>
              <label htmlFor="utility-eplus-email">{i18n('loyaltysignin_0003') || 'Email / Member Number'}</label>
              <input onChange={this._handleInputChange.bind(this, 'epUsername')}
                     onKeyUp={this._handleKeyUp.bind(this, PARTNERS.BRAND.ENTERPRISE_PLUS)}
                     value={this.state.epUsername}
                     type="text"
                     ref={c => this.epUsername = c}
                     name="eplus-email"
                     id="utility-eplus-email"/>

              <label htmlFor="utility-eplus-password">{i18n('loyaltysignin_0004') || 'Password'}</label>
              <input onChange={this._handleInputChange.bind(this, 'epPassword')}
                     onKeyUp={this._handleKeyUp.bind(this, PARTNERS.BRAND.ENTERPRISE_PLUS)}
                     value={this.state.epPassword}
                     type="password"
                     ref={c => this.epPassword = c}
                     name="eplus-password"
                     id="utility-eplus-password"/>

              <label className="eplus-remember"
                     htmlFor="eplus-remember">
                <input onChange={this._onRememberMe.bind(this, 'epRememberMe')}
                       checked={this.state.epRememberMe} type="checkbox" id="eplus-remember"
                       name="eplus-remember"/>
                {i18n('loyaltysignin_0005') || 'Keep me signed in'}
              </label>

              <div className={this.state.submitLoading ? 'loading' : false}/>
              {this.state.submitLoading ? false :
                <button onClick={this._login.bind(this, PARTNERS.BRAND.ENTERPRISE_PLUS)} className={loginButtonClasses}>
                  {i18n('resflowreview_0080') || 'Sign In'}</button>}

              <a onClick={this._forgot} href="#"><span className="icon icon-nav-carrot-white" />
                {i18n('loyaltysignin_0019') || 'Forgot Password?'}</a>

            </div>

            <div tabIndex="0" className={emeraldToggle} onClick={this._onPanelToggle.bind(this, PARTNERS.BRAND.EMERALD_CLUB)} onKeyPress={this._ecToggleKeypress} >
              <div className="panel-toggle"><span className="icon icon-nav-carrot-white up"/>
                {i18n('loyaltysignin_0002') || 'Add Emerald Club'}
              </div>
            </div>
          </div>
          <div className={emeraldLoginClasses}>
            <div className="login-field-container">
              <h4><i className="icon icon-brand-national"/>{i18n('resflowcorporate_4005') || 'Sign In to Emerald Club'}</h4>
              <span className="emerald-club-login-message">{i18n('loyaltysignin_0013') || 'EMERALD CLUB MEMBER?'}</span>
              <label htmlFor="utility-emeraldClub-email">{i18n('resflowcorporate_4003') || 'Username or Emerald Club #'}</label>
              <input onChange={this._handleInputChange.bind(this, 'ecUsername')}
                     onKeyUp={this._handleKeyUp.bind(this, PARTNERS.BRAND.EMERALD_CLUB)}
                     value={this.state.ecUsername}
                     type="text"
                     ref={c => this.ecUsername = c}
                     name="emeraldClub-email"
                     id="utility-emeraldClub-email"/>

              <label htmlFor="utility-emeraldClub-password">{i18n('loyaltysignin_0004') || 'Password'}</label>
              <input onChange={this._handleInputChange.bind(this, 'ecPassword')}
                     onKeyUp={this._handleKeyUp.bind(this, PARTNERS.BRAND.EMERALD_CLUB)}
                     value={this.state.ecPassword}
                     type="password"
                     ref={c => this.ecPassword = c}
                     name="emeraldClub-password"
                     id="utility-emeraldClub-password"/>

              <div className={this.state.submitLoading ? 'loading' : false}/>
              {this.state.submitLoading ? false :
                <button onClick={this._login.bind(this, PARTNERS.BRAND.EMERALD_CLUB)}
                     className={loginButtonClasses}>{i18n('resflowreview_0080') || 'Sign In'}</button>}

            </div>

            <div tabIndex="0" className={enterpriseToggle} onClick={this._onPanelToggle.bind(this, PARTNERS.BRAND.ENTERPRISE_PLUS)} onKeyPress={this._epToggleKeypress}>
              <div className="panel-toggle"><span className="icon icon-nav-carrot-white down"/>
                {i18n('loyaltysignin_0001') || 'Sign In to Enterprise Plus'}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Unauthenticated.displayName = "Unauthenticated";
