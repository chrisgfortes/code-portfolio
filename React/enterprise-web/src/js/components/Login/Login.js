import LoginLabel from './LoginLabel';
import classNames from 'classnames';
import LoginWindow from './LoginWindow';
import ReservationCursors from '../../cursors/ReservationCursors';
import Modal from '../Modal/GlobalModal';
import Terms from './Terms';
import ClickOutside from '../../mixins/ClickOutside';
import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import LogoutModal from './LogoutModal';
import LoginController from '../../controllers/LoginController';

let Login = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin,
    ClickOutside
  ],
  getInitialState () {
    return {
      active: false
    };
  },
  cursors: {
    login: ReservationCursors.login,
    loginWidget: ReservationCursors.loginWidget,
    loginModal: ReservationCursors.loginModal,
    logout: ReservationCursors.logout,
    contractDetails: ReservationCursors.contractDetails,
    userLoggedIn: ReservationCursors.userLoggedIn,
    sessionBrand: ReservationCursors.sessionBrand,
    profile: ReservationCursors.profile,
    errors: ReservationCursors.loginWidgetErrors,
    supportLinks: ReservationCursors.supportLinks,
    loyaltyBrand: ReservationCursors.loyaltyBrand,
    isFedexReservation: ReservationCursors.isFedexReservation
  },
  _handleClickOutside () {
    this.setState({'active': false});
  },
  _closeMenu (e) {
    if (e.keyCode === 27) {
      this.setState({'active': false});
    }
  },
  _toggleMenu () {
    LoginController.toggleMenu();
    this.setState({'active': !this.state.active});
  },
  render () {
    let loginWidgetClasses = classNames({
      'login-widget': true,
      'active': this.state.active
    });
    let loyaltySignUpEligible = this.state.contractDetails && this.state.contractDetails.loyalty_sign_up_eligible === false ? false : true;
    let modal = false;

    switch (this.state.loginModal) {
      case 'terms':
        modal = {
          content: <Terms loginWidget={this.state.loginWidget} login={this.state.login} />,
          title: i18n('eplusenrollment_0036')
        };
        break;
      default :
        modal = {
          content: <div className="transition"/>,
          title: 'Please hold...'
        };
    }

    return (
      loyaltySignUpEligible ?
        <div>
          {this.state.logout.modal &&
          <Modal
            active={this.state.logout.modal}
            header={i18n('reservationwidget_0028')}
            content={<LogoutModal />}
            cursor={['view', 'logout', 'modal']}
            />
          }
          <section className={loginWidgetClasses} onKeyUp={this._closeMenu} role="menu">
            <LoginLabel onClick={this._toggleMenu} userLoggedIn={this.state.userLoggedIn} sessionBrand={this.state.sessionBrand} />
            {this.state.active ? <LoginWindow
                                  authenticated={this.state.profile}
                                  brand={this.state.sessionBrand}
                                  supportLinks={this.state.supportLinks}
                                  errors={this.state.errors}
                                  loyaltyBrand={this.state.loyaltyBrand}
                                  isFedexReservation={this.state.isFedexReservation} /> : ''}
            {this.state.loginModal ? <Modal cursor={ReservationCursors.loginModal}
                                            classLabel={this.state.loginModal === 'terms' ? 'terms' : ''}
                                            active={this.state.loginModal.concat('modal')}
                                            header={modal.title}
                                            content={modal.content}/> : false }
          </section>
        </div> : false
    );
  }
});

module.exports = Login;
