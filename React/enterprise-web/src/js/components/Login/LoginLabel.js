import { PARTNERS } from "../../constants/";

export default function LoginLabel({ onClick, userLoggedIn, sessionBrand}) {
  return (
    <div tabIndex="0" role="menuitem" onClick={onClick} onKeyPress={a11yClick(onClick)} className="utility-nav-label" aria-haspopup="true">
      {userLoggedIn ?
        <strong>{sessionBrand === PARTNERS.BRAND.EMERALD_CLUB ? <i className="icon icon-brand-national"/> :
          <i className="icon icon-specs-seats-green"/>}<span
          className="label">{i18n('eplusaccount_0025')}
                  </span></strong> : <span className="login-text"><strong
        id="signInJoinButton">{i18n('login_0001')}</strong></span>
      }
      <i className="icon icon-utility-notch"/>
    </div>
  );
}

LoginLabel.displayName = "LoginLabel";
