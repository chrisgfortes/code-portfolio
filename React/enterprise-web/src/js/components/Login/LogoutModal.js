import {logout, setLogoutModal} from '../../controllers/LoginController';

export default function LogoutModal () {
  return (
    <div className="logout-modal">
      <span>{i18n('reservationwidget_5028')}</span>

      <div className="modal-actions">
        <button className="btn cancel"
                onClick={logout}>{i18n('reservationwidget_0028')}</button>
        <button onClick={setLogoutModal.bind(null, false)}>{i18n('reservationnav_0036')}</button>
      </div>
    </div>
  );
}

LogoutModal.displayName = "LogoutModal";
