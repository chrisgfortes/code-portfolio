import LoginController from '../../controllers/LoginController';
import EnrollmentController from '../../controllers/EnrollmentController';
import ExpeditedController from '../../controllers/ExpeditedController';
import classNames from 'classnames';

export default class Terms extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      submitLoading: false
    };
    EnrollmentController.getTerms();
    this._onSubmit = this._onSubmit.bind(this);
    this._onClose = this._onClose.bind(this);
    this.renderTerms = this.renderTerms.bind(this);
  }
  _onSubmit() {
    let params = {
      username: this.props.login.username,
      password: this.props.login.password,
      brand: this.props.login.brand,
      rememberMe: this.props.login.rememberMe,
      terms: this.props.loginWidget.terms.terms_and_conditions_version
    };
    this.setState({submitLoading: true});
    LoginController.login(params).then(() => {
      this.setState({submitLoading: false});
      LoginController.setLoginModal(false);
    });
  }
  _onClose() {
    ExpeditedController.setInput('modal', false);
    LoginController.setLoginModal(false);
  }
  renderTerms() {
    const { loginWidget } = this.props;

    return {
      __html: (loginWidget.terms ? loginWidget.terms.terms_and_conditions : false)
    };
  }
  render() {
    let acceptClasses = classNames({
      'btn': true,
      'save': true,
      'disabled': this.state.submitLoading
    });

    const termsContent = this.renderTerms();

    return (
      <div>
        {termsContent.__html && (
          <div dangerouslySetInnerHTML={termsContent}/>
        )}

        <div className="modal-actions">
          <div className={this.state.submitLoading ? 'loading' : false}/>
          <span onClick={this._onClose} className="btn cancel">{i18n('eplusaccount_0108')}</span>

          <div onClick={this._onSubmit}
               className={acceptClasses}>{i18n('resflowcarselect_0100')}</div>
        </div>
      </div>
    )
  }
}

Terms.displayName = "Terms";
