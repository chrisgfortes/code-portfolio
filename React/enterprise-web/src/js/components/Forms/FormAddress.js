import EnrollmentController from '../../controllers/EnrollmentController';

export default class FormAddress extends React.Component{
  constructor() {
    super();

    this.state = {
      countries: null,
      country: null,
      streetAddress: null,
      additionalStreetAddress: null,
      city: null,
      subdivision: null,
      subdivisions: null,
      postal: null,
      countryConfig: null,
      prefix: null,
      street1Value: null,
      street2Value: null,
      cityValue: null,
      stateValue: null,
      zipValue: null,
      countryValue: null
    }

    EnrollmentController.getCountriesBasic()
      .then((response)=> {
        this.setResponseValues();
        this.setState({
          country: enterprise.countryCode,
          countries: response
        }, () => {
          this.setAEMIds();
        });
      });

    this._onCountryChange = this._onCountryChange.bind(this);
    this.setAEMIds = this.setAEMIds.bind(this);
    this.setResponseValues = this.setResponseValues.bind(this);
  }

  setAEMIds() {
    let prefix = document.getElementById('prefix').dataset.prefix;
    this.setState({
      prefix: prefix
    }, () => {
      this.setCountryConfig();
    });
  }

  //if a form is incorrectly set, push these values back into the form :/
  setResponseValues() {
    this.setState({
      street1Value: document.getElementById('street1-hidden').dataset.value,
      street2Value: document.getElementById('street2-hidden').dataset.value,
      cityValue: document.getElementById('city-hidden').dataset.value,
      stateValue: document.getElementById('state-hidden').dataset.value,
      zipValue: document.getElementById('zip-hidden').dataset.value,
      countryValue: document.getElementById('country-hidden').dataset.value
    });
  }

  setCountryConfig() {
    let handler = document.getElementById(this.state.prefix+'-country');
    let index = handler[handler.selectedIndex].getAttribute('data-index');
    let config = this.state.countries[index];
    this.setState({
      countryConfig: config
    }, () => {
      this.getSubdivision();
    });
  }

  getSubdivision () {
    let country = this.state.country.country_code ? this.state.country.country_code : this.state.country;
    EnrollmentController.getSubdivisions(country)
      .then((response) => {
        this.setState({
          subdivisions: response
        });
      });
  }

  _onCountryChange(event) {
    let elementIndex = event.target.selectedIndex,
      index = event.target.children[elementIndex].getAttribute('data-index');

    this.setState({
      country: this.state.countries[index]
    }, () => {
      this.setAEMIds();
    });
  }

  _handleInputChange (attribute, event) {
    this.setState(
      {
        [attribute]: event.target.value
      }
    );
  }

  render () {
    let originCountry = this.state.country || enterprise.countryCode;
    let subdivisionLabel = originCountry ? EnrollmentController.getSubdivisionLabel( originCountry ) : null;
    let postalLabel = i18n('resflowreview_0068');

    return (
      <fieldset>
        <div className="field-container country">
          <label htmlFor={this.state.prefix+"-country"}>{i18n('resflowreview_0062')}</label>
          {this.state.countries && this.state.countries.length > 0 ?
            <select className="styled" onChange={this._onCountryChange}
                    id={this.state.prefix+"-country"}
                    ref={c => this.country = c}
                    name="addressform.country"
                    defaultValue={this.state.countryValue ? this.state.countryValue : enterprise.countryCode}>
              {this.state.countries.map(function (country, index) {
                return (<option key={index}
                                data-index={index}
                                value={country.country_code}>{country.country_name}</option>)
              })}
            </select>
            :
            <div ref={c => this.country = c}
                 className="loading">{i18n('resflowcorporate_4037')}</div>
          }
        </div>

        <div className="field-container">
          <label htmlFor={this.state.prefix+"-street1"}>{i18n('resflowreview_0063')}</label>
          <input id={this.state.prefix+"-street1"}
                 ref={c => this.streetAddress = c} name="addressform.street1" type="text"
                 value={this.state.street1Value}
                 onChange={this._handleInputChange.bind(this, 'street1Value')}/>
        </div>

        <div className="field-container">
          <label htmlFor={this.state.prefix+"-street2"}>
            {i18n('resflowreview_0064')}</label>
          <input id={this.state.prefix+"-street2"}
                 name="addressform.street2"
                 ref={c => this.additionalStreetAddress = c}
                 type="text"
                 value={this.state.street2Value}
                 onChange={this._handleInputChange.bind(this, 'street2Value')}/>
        </div>

        <div className="field-container city">
          <label htmlFor={this.state.prefix+"-city"}>{i18n('resflowreview_0065')}</label>
          <input ref={c => this.city = c} id={this.state.prefix+"-city"}
                 name="addressform.city"
                 type="text"
                 value={this.state.cityValue}
                 onChange={this._handleInputChange.bind(this, 'cityValue')}/>
        </div>

        {(this.state.countryConfig && this.state.countryConfig.enable_country_sub_division) ?
          <div className="field-container subdivision">
            <label htmlFor={this.state.prefix+"-state"}>{subdivisionLabel}</label>
            {this.state.subdivisions && this.state.subdivisions.length > 0 ?
              <select className="styled"
                      id={this.state.prefix+"-state"}
                      name="addressform.state"
                      ref={c => this.subdivision = c}
                      defaultValue={this.state.stateValue}>
                <option key="0"
                        value="">{i18n('resflowlocations_0021')}</option>
                {this.state.subdivisions.map(function (region, index) {
                  return (<option key={index}
                                  value={region.country_subdivision_code}>{region.country_subdivision_name}</option>)
                })}
              </select>
              :
              <input ref={c => this.subdivision = c}
                     id={this.state.prefix+"-state"}
                     type="text"
                     name="addressform.state"
                     defaultValue={this.state.stateValue}
                     onChange={this._handleInputChange.bind(this, 'stateValue')}/>
            }
          </div>
          : false}

        {this.state.countryConfig && this.state.countryConfig.postal_code_type ? <div className="field-container postal">
          <label htmlFor={this.state.prefix+"-zip"}>{postalLabel}</label>
          <input id={this.state.prefix+"-zip"} ref={c => this.postal = c}
                 name="addressform.zip"
                 type="text"
                 value={this.state.zipValue}
                 onChange={this._handleInputChange.bind(this, 'zipValue')}/>
        </div> : false}

      </fieldset>);
  }
}

FormAddress.displayName = "FormAddress";
