const Confirmation = React.createClass({
  render () {
    return (
      <section>
        <div className="heading-wrapper">
          <div>
            <div className="icon-alert-success icon"></div>
            <h1>{enterprise.i18nReservation.promotionsdlp_0010}</h1>
          </div>
          <div className="heading">
            <div>{enterprise.i18nReservation.promotionsdlp_0202}</div>
          </div>
        </div>
      </section>);
  }
});

module.exports = Confirmation;
