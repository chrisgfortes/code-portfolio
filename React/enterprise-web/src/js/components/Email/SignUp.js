import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import Validator from '../../mixins/Validator';
import Services from '../../services/EmailService';
import EnrollmentController from '../../controllers/EnrollmentController';
// import VerificationServices from '../../services/VerificationServices';
import VerificationController from '../../controllers/VerificationController';
import GlobalModal from '../Modal/GlobalModal';
import ReservationCursors from '../../cursors/ReservationCursors';

const Email = React.createClass({
  mixins: [
    Validator,
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    account: ReservationCursors.account,
    verification: ReservationCursors.verification
  },
  getInitialState() {
    return {
      //Model
      firstName: null,
      lastName: null,
      email: null,
      emailConfirm: null,
      postal: null,
      location: null,
      country: null,
      source: enterprise.environment.getGlobal('sourceCode', enterprise.settings.defaultEmailSubscription, true),
      //View
      submit: false,
      loading: false,
      errors: null,
      privacy: null,
      modal: false,
      countryConfig: null
    };
  },
  componentDidMount () {
    EnrollmentController.getCountriesBasic()
                        .then((response)=> {
                          this.setState({
                            country: enterprise.countryCode,
                            countries: response
                          }, () => {
                            this.setCountryConfig();
                          });
                        });
  },
  componentWillMount() {
    // VerificationServices.getDisclaimer((content)=> {
    //   this.setState({
    //     privacy: content
    //   });
    // });
    VerificationController.getDisclaimer()
      .then((content) => {
        this.setState({
          privacy: content.privacy_policy || content
        });
      })
  },
  fieldMap() {
    return {
      value: {
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        email: this.state.email,
        emailConfirm: this.state.emailConfirm,
        postal: this.state.postal
      },
      schema: {
        firstName: 'string',
        lastName: 'string',
        email: 'email',
        emailConfirm: () => {
          let emailConfirm = this.refs.emailConfirm.getDOMNode().value;
          return emailConfirm.length > 0 && this.refs.email.getDOMNode().value === emailConfirm;
        },
        postal: this.state.countryConfig && this.state.countryConfig.postal_code_type ? 'string' : '?string'
      }
    };
  },
  setCountryConfig() {
    let handler = document.getElementById('country');
    let index = handler[handler.selectedIndex].getAttribute('data-index');
    let config = this.state.countries[index];
    this.setState({
      countryConfig: config
    });
  },
  _onInputChange(value, event) {
    let def = event.target.value;
    this.setCountryConfig();
    this.setState({
      [value]: def
    }, ()=> {
      if (this.state.submit) {
        this.validate(value, def);
      } else if (value === 'email' || value === 'emailConfirm') {
        this.validate('emailConfirm', this.state.emailConfirm);
        this.validate('email', this.state.email);
      }
    });
  },
  _viewPrivacy() {
    this.setState({
      modal: 'privacy'
    });
  },
  _close() {
    this.setState({
      modal: false
    });
  },
  _submit(event) {
    event.preventDefault();
    let validate = this.validateAll();
    this.setState({
      submit: true
    }, ()=> {
      if (validate.valid) {
        $('html, body').animate({scrollTop: 0}, 'slow');
        this.setState({loading: true});
        Services.submit(this.state, (response)=> {
          if (response.messages && response.messages.length && response.messages[0].priority === 'ERROR') {
            this.setState({loading: false});
            this.setState({errors: response.messages[0].message});
          } else {
            window.location.href = confirmation;
          }
        });
      }
    });
  },
  render() {
    let content = null;
    let originCountry = this.state.country || enterprise.countryCode;

    if (this.state.loading) {
      content = (<div className="transition"></div>);
    } else {
      content = (
        <section>
          <div className="heading-wrapper">
            <div className="icon-ico-email-extras icon"></div>
            <div className="heading">
              <h1>{i18n('promotionemail_0022') || 'Sign Up For Enterprise Email Specials'}</h1>
              <div>{i18n('promotionemail_0023')}</div>
            </div>
          </div>

          {this.state.errors ? <div className="error-container">{this.state.errors}</div> : false}
          <form>
            <i>{i18n('resflowreview_0004')}</i>

            <div className="field-container first-name">
              <label htmlFor="first-name">{i18n('reservationwidget_0021')}</label>
              <input onChange={this._onInputChange.bind(this, 'firstName')}
                     id="first-name" type="text"
                     ref="firstName"
                     value={this.state.firstName}/>
            </div>
            <div className="field-container last-name">
              <label htmlFor="last-name">{i18n('reservationwidget_0022')}</label>
              <input onChange={this._onInputChange.bind(this, 'lastName')}
                     id="last-name" type="text"
                     ref="lastName"
                     value={this.state.lastName}/>
            </div>

            <div className="field-container">
              <label htmlFor="email">{i18n('reservationwidget_0030')}</label>
              <input onChange={this._onInputChange.bind(this, 'email')}
                     id="email" type="text"
                     ref="email"
                     placeholder={i18n('eplusenrollment_0010')}
                     value={this.state.email}/>

              <div className="sub-text">{i18n('promotionsdlp_0009')}</div>
            </div>


            <div className="field-container">
              <label htmlFor="emailConfirm">{i18n('eplusenrollment_0011')}</label>
              <input onChange={this._onInputChange.bind(this, 'emailConfirm')}
                     id="emailConfirm" type="email"
                     ref="emailConfirm"
                     disabled={this.state.email ? !this.state.email.length > 0 : 'disabled' }/>
            </div>

            <div className="field-container country">
              <label htmlFor="country">{i18n('resflowreview_0062')}</label>
              {this.state.countries && this.state.countries.length > 0 ?
                <select onChange={this._onInputChange.bind(this, 'country')}
                        id="country"
                        className="styled"
                        defaultValue={originCountry}>
                  {this.state.countries.map(function (country, index) {
                    return (<option key={index}
                                    data-index={index}
                                    value={country.country_code}>{country.country_name}</option>);
                  })}
                </select>
                :
                <div className="loading"></div>
              }
            </div>
            {this.state.countryConfig && this.state.countryConfig.postal_code_type &&
            <div className="field-container postal">
              <label htmlFor="email">{i18n('resflowreview_0068')}</label>
              <input onChange={this._onInputChange.bind(this, 'postal')}
                     id="postal" type="text"
                     ref="postal"
                     value={this.state.postal}/>
            </div>}

            <div className="field-container location-preference">
              <label htmlFor="location-preference">{i18n('promotionemail_0024')}
                ({i18n('reservationwidget_0040')})</label>
              <input type="radio" name="location-preference" value="AIRPORT"
                     checked={this.state.location === 'AIRPORT'}
                     onChange={this._onInputChange.bind(this, 'location')}/> {i18n('promotionemail_0025')}
              <input type="radio" className="indent" name="location-preference" value="HOME"
                     checked={this.state.location === 'HOME'}
                     onChange={this._onInputChange.bind(this, 'location')}/> {i18n('promotionemail_0026')}
              <input type="radio" className="indent" name="location-preference" value="HOME_AIRPORT"
                     checked={this.state.location === 'HOME_AIRPORT'}
                     onChange={this._onInputChange.bind(this, 'location')}/> {i18n('promotionemail_0027')}
            </div>

            <div>{i18n('promotionemail_0028')} <span
              onClick={this._viewPrivacy}
              className="privacy"> {i18n('resflowreview_0008')}</span>
            </div>

            <div className="action-container">
              <button className="btn" onClick={this._submit}>{i18n('uefacontest_0007')}</button>
            </div>
          </form>

        </section>
      );
    }
    return (<div>
      {content}
      {this.state.modal ?
        <GlobalModal active={this.state.modal}
                     header={i18n('resflowreview_1008')}
                     classLabel="privacy-modal"
                     content={
                         <div className="enroll-privacy">
                            <div className="privacy-modal-content"
                              dangerouslySetInnerHTML={{__html: this.state.privacy}}/>
                          </div>
                       }
                     close={this._close}/> : false
      }
    </div>);
  }
});

module.exports = Email;
