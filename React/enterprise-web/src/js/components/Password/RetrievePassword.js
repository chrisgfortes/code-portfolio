const ReservationCursors = require('../../cursors/ReservationCursors');
const BaobabReactMixinBranch = require('baobab-react/mixins').branch;
const Validator = require('../../mixins/Validator');
const Error = require('../Errors/Error');
const ErrorActions = require('../../actions//ErrorActions');
const classNames = require('classnames');

const { PasswordErrorList, PasswordErrorListItem } = require('../Password/PasswordErrorList');
const PasswordResetService = require('../../services/PasswordResetService');

var RetrievePassword = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin,
    Validator
  ],
  cursors: {
    session: ReservationCursors.reservationSession,
    account: ReservationCursors.account,
    errors: ['view', 'resetPassword', 'password', 'errors']
  },
  fieldMap() {
    return {
      value: {
        username: this.refs.username.getDOMNode().value,
        firstname: this.refs.firstname.getDOMNode().value,
        lastname: this.refs.lastname.getDOMNode().value
      },
      schema: {
        username: 'string',
        firstname: 'string',
        lastname: 'string'
      }
    }
  },
  getInitialState () {
    return {
      firstname: null,
      lastname: null,
      username: null,
    }
  },
  _onSave (e) {
    e.preventDefault();

    let errorObj = this.validateAll();
    let valid = errorObj.valid;

    if (valid) {
      this.setState({
        submitLoading: true
      });
      ErrorActions.clearErrorsForComponent('resetPassword');

      PasswordResetService.retrievePassword(this.fieldMap().value, (response) => {
        this.setState({
          submitLoading: false
        });

        if (response === 'error') {
          console.log('error!');
        } else {
          console.log(response);
          window.location.hash = 'sent';
        }
      });
    } else if (typeof errorObj === 'object' && errorObj.errorReasons.password) {
      this.setState({passwordRules: errorObj.errorReasons.password});
    }
  },
  render: function () {
    let saveButtonClasses = classNames({
      'btn': true,
      'save': true,
      'disabled': this.state.submitLoading
    });
    return (
      <div className="centered-section">
        <h1>{this.props.text.h1}</h1>
        <h2>{this.props.text.h2}</h2>
        <p>{this.props.text.p1}</p>
        <p>{this.props.text.p2}</p>
        <form className="password" ref="form">
          <Error errors={this.state.errors} type="GLOBAL"/>
          <div className="field-container firstname">
            <label htmlFor="firstname">{enterprise.i18nReservation.reservationwidget_0021}</label>
            <input id="firstname" type="text" ref="firstname"/>
          </div>
          <div className="field-container lastname">
            <label htmlFor="lastname">{enterprise.i18nReservation.reservationwidget_0022}</label>
            <input id="lastname" type="text" ref="lastname"/>
          </div>
          <div className="field-container username">
            <label htmlFor="username">{enterprise.i18nReservation.reservationwidget_0030}</label>
            <input id="username" type="text" ref="username"/>
          </div>
          <div className="modal-actions">
            <div className={this.state.submitLoading ? 'loading' : false}></div>
            <button onClick={this._onSave}
                    className={saveButtonClasses}>Send
            </button>
          </div>
        </form>
      </div>
    );
  }
});

module.exports = RetrievePassword;
