const PasswordErrorListItem = React.createClass({
  mixins: [React.addons.PureRenderMixin],
  _generateErrorIcon () {
    if (this.props.valid === null) {
      return 'empty';
    } else if (this.props.valid) {
      return 'icon-icon-checkmark-thin-green';
    } else {
      return 'icon-alert-small';
    }
  },
  _generateErrorClass () {
    if (this.props.valid === null) {
      return this.props.showOnError ? 'hide' : 'empty';
    } else if (this.props.valid) {
      return this.props.showOnError ? 'hide' : 'ok';
    } else {
      return 'error';
    }
  },
  render: function () {
    return (
      <li className={this._generateErrorClass()}>
        <span className={'icon ' + this._generateErrorIcon()}></span>
        {this.props.text}
      </li>
    );
  }
});

const PasswordErrorList = React.createClass({
  //mixins: [React.addons.PureRenderMixin],
  render: function () {
    let errors = this.props.errors;
    return (
      <ul className="criteria error-list">
        <PasswordErrorListItem key={'1'} text={enterprise.i18nReservation.loyaltysignin_0037 || 'length'}
                               valid={errors.length}/>
        <PasswordErrorListItem key={'2'} text={enterprise.i18nReservation.loyaltysignin_0038 || 'letter'}
                               valid={errors.oneLetter}/>
        <PasswordErrorListItem key={'3'} text={enterprise.i18nReservation.loyaltysignin_0039 || 'number'}
                               valid={errors.oneNumber}/>
        <PasswordErrorListItem key={'6'} text={enterprise.i18nReservation.loyaltysignin_0040 || 'blacklist'}
                               valid={errors.blacklist}/>
      </ul>
    );
  }
});

module.exports = {
  PasswordErrorList,
  PasswordErrorListItem
};
