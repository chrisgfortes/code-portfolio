import NewPassword from './NewPassword';
import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import RetrievePassword from './RetrievePassword';
import RetrieveConfirmation from './RetrieveConfirmation';
import PasswordChangeSuccess from './PasswordChangeSuccess';
import CompleteProfile from './CompleteProfile';

import ReservationCursors from '../../cursors/ReservationCursors';
import SupportActions from '../../actions/SupportActions';

const i18nObj = {
  retrieve: {
    h1: i18n('loyaltyforgotpassword_0001'),
    h2: i18n('loyaltyforgotpassword_0002'),
    p1: i18n('loyaltyforgotpassword_0003'),
    p2: ''
  },
  expired: {
    h1: i18n('loyaltyforgotpassword_0015'),
    h2: i18n('loyaltyforgotpassword_0016'),
    p1: i18n('loyaltyforgotpassword_0003'),
    p2: ''
  }
}

ReservationStateTree.select(ReservationCursors.supportLinks).on('update', () => {
  i18nObj.expired.p2 = i18nObj.retrieve.p2 = i18n('loyaltyforgotpassword_0004', {'EPLUS': SupportActions.getSupportPhoneNumberOfType('EPLUS').phone_number});
});


const ForgotPasswordFlow = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  getInitialState () {
    return {
      hash: null
    }
  },
  componentDidMount () {
    let hash = location.hash.replace(/\?.*$/, "");

    hash = hash.replace('#', '');

    this.setState({
      hash: hash
    });
    window.onhashchange = () => {
      let hash = location.hash.replace(/\?.*$/, "");

      hash = hash.replace('#', '');

      this.setState({
        hash: hash
      });
    }
  },
  componentWillUnmount () {
    window.onhashchange = null;
  },
  cursors: {
    resetPasswordErrors: ReservationCursors.resetPasswordErrors
  },
  render () {
    let component = null;
    switch (this.state.hash) {
      case 'retrieve':
        component = <RetrievePassword text={i18nObj.retrieve} />;
        break;
      case 'new':
        component = <NewPassword errors={this.state.resetPasswordErrors} />;
        break;
      case 'sent':
        component = <RetrieveConfirmation text={i18nObj.retrieve} />;
        break;
      case 'success':
        component = <PasswordChangeSuccess />;
        break;
      case 'complete':
        component = <CompleteProfile />;
        break;
      case 'expired':
        component = <RetrievePassword text={i18nObj.expired} />;
        break;
      default:
        component = <RetrievePassword text={i18nObj.retrieve} />;
        break;
    }
    return (
      <div className="forgot-password-flow">
        {component}
      </div>
    );
  }
});

module.exports = ForgotPasswordFlow;
