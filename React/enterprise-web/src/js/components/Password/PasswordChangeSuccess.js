const RedirectActions = require('../../actions/RedirectActions');

const PasswordChangeSuccess = React.createClass({
  mixins: [
    React.addons.PureRenderMixin,
  ],
  _onConfirmClick (event) {
    event.preventDefault();
    RedirectActions.goHome();
  },
  render () {
    return (
      <div className="centered-section">
        <h1>{i18n('loyaltyforgotpassword_0008')}</h1>
        <p>{i18n('loyaltyforgotpassword_0009')}</p>
        <div className="modal-actions">
          <a href="account.html" className="btn cancel">{i18n('loyaltyforgotpassword_0010')}</a>
          <a href="#" onClick={this._onConfirmClick} className="btn ok">{i18n('loyaltyforgotpassword_0011')}</a>
        </div>
      </div>
    );
  }
});

module.exports = PasswordChangeSuccess;
