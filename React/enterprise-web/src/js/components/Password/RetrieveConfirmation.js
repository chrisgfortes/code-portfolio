const RetrieveConfirmation = React.createClass({
  mixins: [
    React.addons.PureRenderMixin,
  ],
  render () {
    return (
      <div className="centered-section">
        <h1>{this.props.text.h1}</h1>
        <h2>{this.props.text.h2}</h2>
        <p>{enterprise.i18nReservation.loyaltyforgotpassword_0005}</p>
        <p>{this.props.text.p2}</p>
      </div>
    );
  }
});

module.exports = RetrieveConfirmation;
