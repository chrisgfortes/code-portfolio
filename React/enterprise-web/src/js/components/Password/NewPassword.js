import Error from '../Errors/Error';
import classNames from 'classnames';
import Validator from '../../utilities/util-validator';
import LoginController from '../../controllers/LoginController';
import ProfileController from '../../controllers/ProfileController';
import RedirectController from '../../controllers/RedirectController';
import ReservationFlowModelController from '../../controllers/ReservationFlowModelController';

import { REQUEST } from '../../constants';

import { PasswordErrorList, PasswordErrorListItem } from '../Password/PasswordErrorList';

export default class NewPassword extends React.Component{
  constructor() {
    super();
    this.state = {
      username: null,
      password: null,
      passwordConfirm: null,
      passwordRules: {
        space: null,
        length: null,
        blacklist: null,
        oneLetter: null,
        oneNumber: null,
        email: null
      },
      confirmPasswordRules: {
        match: null
      }
    };
    this.validator = new Validator(this, this.fieldMap);
    this.fieldMap = this.fieldMap.bind(this);
    this._handleInputChange = this._handleInputChange.bind(this);
    this.getParameterByName = this.getParameterByName.bind(this);
    this._onSave = this._onSave.bind(this);
    this._onCancel = this._onCancel.bind(this);
  }

  fieldMap() {
    return {
      refs: {
        username: this.username,
        password: this.password,
        passwordConfirm: this.passwordConfirm
      },
      value: {
        username: this.username.value,
        password: this.password.value,
        passwordConfirm: this.passwordConfirm.value
      },
      schema: {
        username: 'string',
        password: 'password',
        passwordConfirm: () => {
          let passwordConfirm = this.passwordConfirm.value;
          let matches = passwordConfirm.length > 0 && this.password.value === passwordConfirm;
          this.setState({confirmPasswordRules: {match: matches}});
          return matches;
        }
      }
    }
  }

  _handleInputChange (attribute, event) {
    this.setState(
      {
        [attribute]: event.target.value
      }
    );
    let errorObj = this.validator.validate(attribute, event.target.value);
    if (typeof errorObj === 'object' && errorObj.errorReasons.password) {
      this.setState({passwordRules: errorObj.errorReasons.password});
    }
  }
  getParameterByName (name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    const regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
    const results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }
  _onSave (e) {
    e.preventDefault();

    let errorObj = this.validator.validateAll();
    let valid = errorObj.valid;

    if (valid) {
      this.setState({
        submitLoading: true
      });

      let token = this.getParameterByName('token');

      if (token) {
        let data = {
          username: this.state.username,
          password: token,
          brand: 'EP'
        };
        ReservationFlowModelController.clearErrorsForComponent('resetPassword');
        LoginController.login(data).then((response) => {
          if(response.messages && response.messages.length) {
            // ECR-14251 ErrorsController
            if (response.messages.find((error) => error.code === REQUEST.MESSAGES.CODES.EXPIRED)) {
              window.location.hash = 'expired';
            } else {
              ReservationFlowModelController.setErrorsForComponent(response.messages, 'resetPassword');
              this.setState({submitLoading: false});
            }
          }
        });
        LoginController.whenUserLogsIn().then(() => {
          ProfileController.updateProfilePassword(this.fieldMap().value).then((errorResponse) => {
            this.setState({submitLoading: false});

            if (!errorResponse.length) {
              window.location.hash = 'success';
            }
          });
        });
      }

    } else if (typeof errorObj === 'object' && errorObj.errorReasons.password) {
      this.setState({passwordRules: errorObj.errorReasons.password});
    }
  }
  _onCancel () {
    event.preventDefault();
    RedirectController.homeRedirect();
    if (typeof this.props.close === 'function') {
      this.props.close();
    }
  }
  render () {
    let saveButtonClasses = classNames({
      'btn': true,
      'save': true,
      'disabled': this.state.submitLoading
    });
    return (
        <div className="centered-section">
          <h1>{i18n('loyaltyforgotpassword_0006')}</h1>
          <form className="password" ref="form">
            <Error errors={this.props.errors} type="GLOBAL"/>
            <div className="field-container username">
              <label htmlFor="username">{i18n('loyaltyforgotpassword_0017')}</label>
              <input onChange={this._handleInputChange.bind(this, 'username')}
                     id="username" type="text"
                     ref={c => this.username =c}/>
            </div>
            <div className="field-container password">
              <label htmlFor="password">{i18n('eplusaccount_0070')}</label>
              <input onChange={this._handleInputChange.bind(this, 'password')}
                     id="password" type="password"
                     ref={c => this.password = c}/>
              <PasswordErrorList errors={this.state.passwordRules}/>
            </div>
            <div className="field-container password">
              <label htmlFor="passwordConfirm">{i18n('resflowreview_0048')}</label>
              <input onChange={this._handleInputChange.bind(this, 'passwordConfirm')}
                     id="passwordConfirm" type="password"
                     ref={c => this.passwordConfirm = c}/>
              <ul className="criteria error-list">
                <PasswordErrorListItem showOnError={true}
                                       text={i18n('loyaltysignin_0044') || "Passwords Don't Match"}
                                       valid={this.state.confirmPasswordRules.match}/>
              </ul>
            </div>
            <div className="modal-actions">
              <div className={this.state.submitLoading ? 'loading' : false} />
              <a href="#" onClick={this._onCancel}
                    className="btn grn-txt">{i18n('eplusaccount_0065')}</a>
              <button onClick={this._onSave}
                      className={saveButtonClasses}>{i18n('loyaltyforgotpassword_0007')}</button>
            </div>
          </form>
        </div>
    );
  }
}

NewPassword.displayName = "NewPassword";
