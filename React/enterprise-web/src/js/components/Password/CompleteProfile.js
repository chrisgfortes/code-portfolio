const CompleteProfile = React.createClass({
  mixins: [
    React.addons.PureRenderMixin,
  ],
  render () {
    return (
      <div className="centered-section">
        <h1>{enterprise.i18nReservation.loyaltyforgotpassword_0012}</h1>
        <h2>{enterprise.i18nReservation.loyaltyforgotpassword_0013}</h2>
        <p>{enterprise.i18nReservation.loyaltyforgotpassword_0014}</p>
        <p>{enterprise.i18nReservation.loyaltysignin_0022}</p>
        <div className="modal-actions">
          <a href="#new" className="btn ok">{enterprise.i18nReservation.loyaltyforgotpassword_0013}</a>
        </div>
      </div>
    );
  }
});

module.exports = CompleteProfile;
