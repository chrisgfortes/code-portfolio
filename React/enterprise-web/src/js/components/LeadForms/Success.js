const BaobabReactMixinBranch = require('baobab-react/mixins').branch;
const RedirectActions = require('../../actions/RedirectActions');

const Success = React.createClass({
  mixins: [
    BaobabReactMixinBranch
  ],
  _onReturnHome () {
    RedirectActions.goHome();
  },
  _onReturnBusinessRental () {
    RedirectActions.go(enterprise.forwardUrl);
  },
  render () {
    return (
      <div className="application-success">
        <h2
          className="disclaimer">
          {i18n('leadforms_0007') || 'Thank you for your application! We have received your submission.'}
        </h2>
        <div className="modal-action">
          <button onClick={this._onReturnHome}>{i18n('eplusenrollment_0204')}
          </button>
          <button onClick={this._onReturnBusinessRental}>{i18n('leadforms_xx') || 'Return to Business Rentals Page'}
          </button>
        </div>
      </div>
    );
  }
});

module.exports = Success;

