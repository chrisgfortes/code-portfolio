const EnrollmentController = require('../../controllers/EnrollmentController');
const Validator = require('../../mixins/Validator');
const Services = require('../../services/LeadFormsService');
const GlobalModal = require('../Modal/GlobalModal');
const BaobabReactMixinBranch = require('baobab-react/mixins').branch;
const ReservationCursors = require('../../cursors/ReservationCursors');
const AccountServices = require('../../services/AccountService');
const Success = require('../../components/LeadForms/Success');

const Entertainment = React.createClass({
  mixins: [
    Validator,
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    account: ReservationCursors.account,
    verification: ReservationCursors.verification
  },
  getInitialState () {
    return {
      //Contact Model
      contactName: null,
      companyName: null,
      email: null,
      phone: null,
      //Company Model
      postal: null,
      location: null,
      country: null,
      address: null,
      addressTwo: null,
      city: null,
      subdivision: null,
      //Event Model
      eventLocation: null,
      eventSubdivision: null,
      eventCenterName: null,
      eventLength: null,
      durationUnit: null,
      attendees: null,
      //View
      subdivisionList: null,
      submit: false,
      loading: false,
      errors: null,
      modal: false,
      countryConfig: null,
      currentView: enterprise.currentView
    };
  },
  fieldMap () {
    let eventEnabled = this.state.currentView === 'meetingAndGroupsRental';
    let map = {
      value: {
        contactName: this.state.contactName,
        companyName: this.state.companyName,
        email: this.state.email,
        phone: this.state.phone
      },
      schema: {
        contactName: 'string',
        companyName: 'string',
        email: 'email',
        phone: 'phone'
      }
    };

    if (eventEnabled) {
      map.value.eventLocation = this.state.eventLocation;
      map.schema.eventLocation = 'string';

      map.value.eventSubdivision = this.state.eventSubdivision;
      map.schema.eventSubdivision = 'string';

      map.value.attendees = this.state.attendees;
      map.schema.attendees = '?integer';
    }

    return map;
  },
  componentDidMount () {
    EnrollmentController.getCountriesBasic()
      .then((response)=> {
        this.setState({
          country: enterprise.countryCode,
          countries: response
        }, ()=> {
          this.setCountryConfig(()=> {
            this.getSubdivision();
          });
        })
      });

  },
  setCountryConfig (callback) {
    let handler = document.getElementById('country');
    let index = handler[handler.selectedIndex].getAttribute('data-index');
    let config = this.state.countries[index];
    this.setState({
      countryConfig: config
    }, ()=> {
      if (callback) {
        callback();
      }
    });
  },
  getSubdivision () {
    EnrollmentController.getSubdivisions(this.state.country)
                        .then((subdivisionList) => {
                            this.setState({
                              subdivisionList
                            });
                        });
  },
  _close () {
    this.setState({
      modal: false
    });
  },
  _onInputChange (value, event) {
    let def = event.target.value;
    this.setState({
      [value]: def
    }, ()=> {
      if (value === 'country') {
        this.setCountryConfig(()=> {
          this.getSubdivision();
        });
      }
      if (this.state.submit) {
        this.validate(value, def);
      }
    });
  },
  _submit(event) {
    event.preventDefault();
    let validate = this.validateAll();
    this.setState({
      submit: true
    }, ()=> {
      if (validate.valid) {
        this.setState({loading: true});
        Services.submit(this.state, (response)=> {
          this.setState({loading: false});
          if (response.messages && response.messages.length && response.messages[0].priority === 'ERROR') {
            this.setState({errors: response.messages[0].message});
          } else {
            this.setState({
              modal: true
            });
          }
        });
      }
    });
  },
  render () {
    let originCountry = this.state.country || enterprise.countryCode;
    let content = null;
    let subdivisionLabel = null;
    let subdivisionType = this.state.countryConfig && this.state.countryConfig.country_sub_division_type;

    if (subdivisionType === 'STATE') {
      subdivisionLabel = i18n('resflowreview_0066');
    } else if (subdivisionType === 'PROVINCE') {
      subdivisionLabel = i18n('resflowreview_0067');
    } else if (subdivisionType === 'COUNTY') {
      subdivisionLabel = i18n('eplusenrollment_0056');
    }

    if (enterprise.currentView) {
      console.log(enterprise.currentView);
    }

    let contact = (
      <div>
        <div className="section-label">{i18n('eplusenrollment_0002')}</div>
        <div className="field-container contact-name">
          <label htmlFor="contact-name">{i18n('waiting_for_key') || 'Contact Name'}</label>
          <input onChange={this._onInputChange.bind(this, 'contactName')}
                 id="contact-name" type="text"
                 ref="contactName"
                 value={this.state.contactName}/>
        </div>
        <div className="field-container company-name">
          <label htmlFor="company-name">{i18n('leadforms_0005') || 'Company Name'}</label>
          <input onChange={this._onInputChange.bind(this, 'companyName')}
                 id="company-name" type="text"
                 ref="companyName"
                 value={this.state.companyName}/>
        </div>

        <div className="field-container phone">
          <label htmlFor="phone">{i18n('resflowreview_0010')}</label>
          <input onChange={this._onInputChange.bind(this, 'phone')}
                 id="phoneNumber" type="tel"
                 ref="phone"
                 pattern="[0-9]*"
                 value={this.state.phone}/>
        </div>

        <div className="field-container email">
          <label htmlFor="email">{i18n('reservationwidget_0030')}</label>
          <input onChange={this._onInputChange.bind(this, 'email')}
                 id="email" type="text"
                 ref="email"
                 placeholder={i18n('eplusenrollment_0010')}
                 value={this.state.email}/>
        </div>
      </div>
    );

    let event = (
      <div>
        <div className="section-label">
          {i18n('leadforms_0009') || 'Event Details'}
        </div>
        <div className="field-container">
          <label htmlFor="eventLocation">{i18n('leadforms_0010') || 'Event Location'}</label>
          <input onChange={this._onInputChange.bind(this, 'eventLocation')} id="eventLocation"
                 ref="eventLocation" type="text"/>
        </div>

        <div className="field-container eventSubdivision">
          <label htmlFor="eventSubdivision">{i18n('resflowreview_0066') + '/' + i18n('resflowreview_0067')}</label>
          <input onChange={this._onInputChange.bind(this, 'eventSubdivision')} ref="eventSubdivision"
                 id="eventSubdivision"
                 type="text"/>
        </div>

        <div className="field-container">
          <label
            htmlFor="eventCenterName">
            {i18n('leadforms_0011') || 'Name of the Convention Center/Meeting Place'} ({i18n('reservationwidget_0040')})
          </label>
          <input onChange={this._onInputChange.bind(this, 'eventCenterName')} id="eventCenterName"
                 ref="eventCenterName" type="text"/>
        </div>

        <div className="field-container eventLength">
          <label htmlFor="eventCenterName">
            {i18n('leadforms_0012') || 'Length of the Event'} ({i18n('reservationwidget_0040')})
          </label>
          <input onChange={this._onInputChange.bind(this, 'eventLength')} id="eventLength"
                 ref="eventLength" type="text"/>
        </div>

        <div className="field-container durationUnit">
          <select className="styled" id="durationUnit" ref="durationUnit"
                  onChange={this._onInputChange.bind(this, 'durationUnit')}>
            <option key="0"
                    value="">{i18n('resflowlocations_0021')}</option>
            <option value="DAYS">{i18n('leadforms_0014') || 'Days'}</option>
            <option value="WEEKS">{i18n('leadforms_0015') || 'Weeks'}</option>
            <option value="MONTHS">{i18n('leadforms_0016') || 'Months'}</option>
          </select>
        </div>

        <div className="field-container attendees">
          <label htmlFor="attendees">
            {i18n('leadforms_0013') || 'Number of Attendees'} ({i18n('reservationwidget_0040')})
          </label>
          <input onChange={this._onInputChange.bind(this, 'attendees')}
                 id="attendees" type="tel"
                 ref="attendees"/>
        </div>
      </div>
    );

    let company = (
      <div>
        <div className="section-label">
          {i18n('leadforms_0003') || 'Company Information'} ({i18n('reservationwidget_0040')})
        </div>
        <div className="field-container country">
          <label htmlFor="country">{i18n('leadforms_0008') || 'Country'}</label>
          {this.state.countries && this.state.countries.length > 0 ?
            <select onChange={this._onInputChange.bind(this, 'country')}
                    id="country"
                    className="styled"
                    defaultValue={originCountry}>
              {this.state.countries.map(function (country, index) {
                return (<option key={index}
                                data-index={index}
                                value={country.country_code}>{country.country_name}</option>);
              })}
            </select>
            :
            <div className="loading"></div>
          }
        </div>

        <div className="field-container">
          <label htmlFor="streetAddress">{i18n('resflowreview_0063')}</label>
          <input onChange={this._onInputChange.bind(this, 'address')} id="streetAddress"
                 ref="streetAddress" type="text"/>
        </div>

        <div className="field-container">
          <input onChange={this._onInputChange.bind(this, 'addressTwo')} id="streetAddressTwo"
                 ref="streetAddressTwo" type="text"/>
        </div>

        <div className="field-container city">
          <label htmlFor="city">{i18n('resflowreview_0065')}</label>
          <input onChange={this._onInputChange.bind(this, 'city')} ref="city" id="city"
                 type="text"/>
        </div>

        {(this.state.countryConfig && this.state.countryConfig.enable_country_sub_division) ?
          <div className="field-container subdivision">
            <label htmlFor="subdivision">{subdivisionLabel}</label>
            {this.state.subdivisionList && this.state.subdivisionList.length > 0 ?
              <select className="styled"
                      onChange={this._onInputChange.bind(this, 'subdivision')}
                      id="subdivision"
                      ref="subdivision">
                <option key="0"
                        value="">{i18n('resflowlocations_0021')}</option>
                {this.state.subdivisionList.map(function (region, index) {
                  return (<option key={index}
                                  value={region.country_subdivision_code}>{region.country_subdivision_name}</option>);
                })}
              </select>
              :
              <input ref="subdivision" onChange={this._onRegionChange} id="subdivision" type="text"/>
            }
          </div>
          : false}

        {this.state.countryConfig && this.state.countryConfig.postal_code_type &&
        <div className="field-container postal">
          <label htmlFor="postal">{i18n('resflowreview_0068')}</label>
          <input onChange={this._onInputChange.bind(this, 'postal')}
                 id="postal" type="text"
                 ref="postal"
                 value={this.state.postal}/>
        </div>}
      </div>
    );

    if (this.state.loading) {
      content = (<div className="transition"></div>);
    } else {
      content = (
        <section>
          <div className="heading-wrapper">
            <div className="heading">
              <h1>{i18n('leadforms_0001') || 'For More Information'}</h1>
            </div>
            <i>{i18n('resflowreview_0004')}</i>
          </div>

          {this.state.errors && <div className="error-container">{this.state.errors}</div>}
          <form>
            {contact}
            {this.state.currentView === 'meetingAndGroupsRental' && event}
            {company}
            <div className="action-container">
              <button className="btn" onClick={this._submit}>{i18n('uefacontest_0007')}</button>
            </div>
          </form>

        </section>
      );
    }

    return (<div>
        {content}
        {this.state.modal ?
          <GlobalModal active={this.state.modal}
                       hideX
                       disableBlur
                       classLabel="application-success-modal"
                       header={i18n('leadforms_0006') || 'Entertainment and Product Rental Application Form Received'}
                       content={<Success/>}
                       close={this._close}/> : false
        }
      </div>

    );
  }
});

module.exports = Entertainment;
