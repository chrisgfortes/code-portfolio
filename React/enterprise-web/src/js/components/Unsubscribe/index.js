import AccountController from '../../controllers/AccountController';
import Error from '../Errors/Error';
import classNames from 'classNames';

export default class Unsubscribe extends React.Component {
  constructor() {
    super();

    this.state = {
      errors: []
    }

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount(){
    const personal = AccountController.getPersonalProfile();
    this.refs.email.value = personal.email;
  }

  getFormData(fields){
    const data = {};
    _.forIn(fields, (field, key) => (data[key] = field.value));
    return data;
  }

  handleSubmit(event){
    event.preventDefault();
    const data = this.getFormData(this.refs);
    // console.log(event.target.checkValidity());
  }

  render() {
    const { errors } = this.state;

    const emailClasses = classNames({
      "field-input": true,
      "invalid": errors.includes('email')
    });

    return (
      <section className="band intro-band page-unsubscribe">
        <Error errors={[]} type="GLOBAL" />

        <div className="title-header">
          <h1>{i18n('emailunsubscribe_0001') || 'Email unsubscribe'}</h1>
          <h2>{i18n('emailunsubscribe_0002') || 'Unsubscribe to stop receiving emails about promotions and new offerings.'}</h2>
        </div>

        <form className="page-unsubscribe_form" onSubmit={this.handleSubmit}>
          <label className="form-label">
            <span>{i18n('reservationwidget_0030') || 'Email'}</span>
            <input
              name="email"
              ref="email"
              className={emailClasses}
              type="email"
              required
            />
          </label>

          <div className="align-right btn-unsubscribe">
            <button className="btn" type="submit">{i18n('promotionemail_0016') || 'Unsubscribe'}</button>
          </div>
        </form>
      </section>
    )
  }
}
