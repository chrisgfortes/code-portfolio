/**
 * @todo: test and verify we can remove the code that's setting the Personal fields below
 */
import VerificationController from '../../controllers/VerificationController';
import Error from '../Errors/Error';
import GlobalModal from '../Modal/GlobalModal';
import PrivacyPolicyModal from '../Verification/PrivacyPolicyModal';
import Scroll from '../../utilities/util-scroll';

const notNullOr = (val, alt) => val === null ? alt : val;

export default class PersonalInformation extends React.Component {
  constructor (props){
    super(props);

    const profile = this.props.profile;
    const driverInfo = this.props.driverInformation;
    const expedited = this.props.expedited;

    // @todo: possibly remove with stuff in componentDidMount() below
    if (this.props.componentInUse === 'Modify') {
      VerificationController
        .setPersonalFields('email', notNullOr(_.get(driverInfo, 'email_address'), _.get(profile, 'contact_profile.email')));
      VerificationController
        .setPersonalFields('phoneNumber', notNullOr(_.get(driverInfo, 'phone.phone_number'), _.get(profile, 'contact_profile.phones[0].phone_number')));
    }

    this.state = {
      checked: VerificationController.setEmailPreference(profile, expedited),
      policyModal: false,
      scrollOffSet: 0
    };

    VerificationController.setRequestPromotions(this.state.checked);
  }
  componentDidMount () {
    let fieldGroup = [];
    // @todo change to a factory elsewhere
    // @todo OOOOOORRRRR REMOVE because I don't think this is necessary any more
    //  - in many cases these values _have already been set_
    if (this.props.componentInUse === 'Modify') {
      fieldGroup = ['firstName', 'lastName'];
    } else {
      fieldGroup = ['firstName', 'lastName', 'email', 'phoneNumber'];
    }
    // @todo change to a personal factory elsewhere, probably in session factory
    // - in many cases these values _have already been set_
    fieldGroup.forEach(key =>
      VerificationController
        .setPersonalFields(key, notNullOr(this.props.personal[key], this.refs[key].value))
    );

    // Special requirement for ECR-11466
    const deepLinkEmailAddress = _.get(this.props.deepLinkReservation, 'reservation.driver_info.email_address');
    if (this.props.isFedexReservation && deepLinkEmailAddress) {
      VerificationController.setPersonalFields('email', deepLinkEmailAddress);
    }
  }
  onChange (value, event) {
    if (value === 'phoneNumber' || value === 'email') {
      this.setState({
        [value + 'Modified']: true
      });
    }
    if (event.target.type === 'checkbox') {
      event.target.value = event.target.checked;
      this.setState({
        checked: event.target.checked
      });
    }

    if (event.target.value.length > 0) {
      event.target.classList.remove('invalid');
      event.target.setAttribute('aria-invalid', 'false');
    }

    VerificationController.setPersonalFields(value, event.target.value);
  }
  onBlur (value) {
    let fieldNameValue = value + 'MaskedValue';
    let fieldBit = value + 'Modified';
    if (this.state[fieldBit] !== true) {
      VerificationController.setPersonalFields(value, this.state[fieldNameValue]);
    } else {
      this.setState({
        fieldBit: false
      });
    }
  }
  onFocus (value, event) {
    if (enterprise.utilities.isMaskedField(event.target.value)) {
      // console.debug('This is a masked value - clear it:', value, event);
      this.setState({
        [value + 'MaskedValue']: this.refs[value].value
      });
      VerificationController.setPersonalFields(value, '');
    }
  }
  togglePrivacy () {
    this.setState({
      policyModal: !this.state.policyModal,
      scrollOffSet: Scroll.getTopScroll()
    });
  }
  onClosePolicyModal ( deferredScrollOffset ) {
    this.togglePrivacy();
    this.privacyLabel.focus();
    // sadly, if we do this too soon, the overflow
    // is still set on the parent window
    setTimeout(()=>{
      Scroll.scrollToOffset( deferredScrollOffset );
    }, 200);
  }
  render () {

    const { errors, enrollErrors, personal, confirmationNumber, loggedIn, driverInfoIdentifier, deepLinkReservation,
            driverInfoLoyalty, verification, profile, sessionContractDetails } = this.props;
    const { checked, policyModal, scrollOffset } = this.state;

    // @todo convert deeplink stuff to use the DeepLink controller/actions
    const ecUnauthAssociated = driverInfoIdentifier && (driverInfoLoyalty === 'EMERALDCLUB');
    const deepLinkDriverInfo = _.get(deepLinkReservation, 'reservation.driver_info');
    const deepLinkDriver = {
      firstName: deepLinkDriverInfo && decodeURI(deepLinkDriverInfo.first_name || ''),
      lastName: deepLinkDriverInfo && decodeURI(deepLinkDriverInfo.last_name || ''),
      phoneNumber: deepLinkDriverInfo && deepLinkDriverInfo.phone && decodeURI(deepLinkDriverInfo.phone.phone_number || ''),
      email: deepLinkDriverInfo && decodeURI(deepLinkDriverInfo.email_address || '')
    };
    const readOnlyAttr = { readOnly: ecUnauthAssociated };
    const fieldErrors = VerificationController.getFieldErrors( errors );

    return (
      <div id="personal-information" className="personal-information section-content">
        <div className="required-text" tabIndex="-1" id="focus-required-text-label">
          <span>* {i18n('resflowreview_0159') || 'Required Field'}</span>
        </div>
        <Error errors={errors} type="GLOBAL" />
        <Error errors={enrollErrors} type="GLOBAL" />

        <form className="personal-entry-form">
          <h3 className="view-header">{enterprise.i18nReservation.eplusenrollment_0002}</h3>
          <div className="field-container first-name">
            <label htmlFor="firstName">{enterprise.i18nReservation.reservationwidget_0021}*
            </label>
            <input onChange={this.onChange.bind(this, 'firstName')} id="firstName" type="text"
                   ref="firstName"
                   value={notNullOr(personal.firstName, deepLinkDriver.firstName)}
                   disabled={confirmationNumber || (profile && loggedIn) || readOnlyAttr.readOnly}
                   className={fieldErrors.firstName ? 'invalid' : ''}
                   aria-required="true" aria-invalid={fieldErrors.firstName}
                   {...readOnlyAttr}/>

          </div>
          <div className="field-container last-name">
            <label htmlFor="lastName">{enterprise.i18nReservation.reservationwidget_0022}*
            </label>
            <input onChange={this.onChange.bind(this, 'lastName')} id="lastName" type="text"
                   value={notNullOr(personal.lastName, deepLinkDriver.lastName)}
                   ref="lastName"
                   disabled={confirmationNumber || (profile && loggedIn) || readOnlyAttr.readOnly}
                   className={fieldErrors.lastName ? 'invalid' : ''}
                   aria-required="true" aria-invalid={fieldErrors.lastName}
                   {...readOnlyAttr}/>
          </div>
          <div className="field-container phone">
            <label htmlFor="phoneNumber">{enterprise.i18nReservation.resflowreview_0010}*
            </label>
            <input onChange={this.onChange.bind(this, 'phoneNumber')}
                   onFocus={this.onFocus.bind(this, 'phoneNumber')}
                   onBlur={this.onBlur.bind(this, 'phoneNumber')}
                   id="phoneNumber" type="tel"
                   ref="phoneNumber"
                   value={notNullOr(personal.phoneNumber, deepLinkDriver.phoneNumber)}
                   pattern="[0-9]*"
                   className={fieldErrors.phoneNumber ? 'invalid' : ''}
                   aria-required="true" aria-invalid={fieldErrors.phoneNumber}/>
          </div>
          <div className="field-container email">
            <label htmlFor="emailAddress">{enterprise.i18nReservation.reservationwidget_0030}*
            </label>
            <input onChange={this.onChange.bind(this, 'email')} id="emailAddress" type="email"
                   onFocus={this.onFocus.bind(this, 'email')}
                   onBlur={this.onBlur.bind(this, 'email')}
                   placeholder={i18n('eplusenrollment_0010') || 'name\@domain.com'}
                   value={notNullOr(personal.email, deepLinkDriver.email)}
                   ref="email"
                   className={fieldErrors.email ? 'invalid' : ''}
                   aria-required="true" aria-invalid={fieldErrors.email}/>
          </div>
          {VerificationController.shouldShowPromotions( profile, sessionContractDetails ) &&
            <div className="field-container requestPromotion">
              <label htmlFor="specialOffers">
                <input onChange={this.onChange.bind(this, 'requestPromotion')} id="specialOffers"
                      name="specialOffers" type="checkbox"
                      checked={checked}/>
                <i className="icon icon-forms-checkmark-green"/>
                {enterprise.i18nReservation.resflowreview_0005}
              </label>

              <p className="privacy-disclaimer">{enterprise.i18nReservation.resflowreview_0006}
                <span role="button" tabIndex="0" className="accented" ref={c => this.privacyLabel = c}
                  onClick={this.togglePrivacy.bind(this)} onKeyPress={a11yClick(this.togglePrivacy.bind(this))}>
                  {enterprise.i18nReservation.resflowreview_0008}
                </span>
              </p>
            </div>
          }
        </form>
        {policyModal &&
        <GlobalModal active={policyModal}
                     header={i18n('eplusenrollment_0061')}
                     close={this.onClosePolicyModal.bind(this, scrollOffset)}
                     children={<PrivacyPolicyModal disclaimer={verification.privacyPolicy}/>}
                     />}
      </div>
    );
  }
}

PersonalInformation.displayName = 'PersonalInformation';
