import {RESERVATION} from '../../constants/';

function cannotModify () {
  enterprise.utilities.modal.open(enterprise.i18nReservation.resflowviewmodifycancel_0081);
}

export default function LogisticsContent ( data, type ) {
  const header = type === RESERVATION.DELIVERY ? enterprise.i18nReservation.resflowcorporate_0060 : enterprise.i18nReservation.resflowcorporate_0061;
  return (
    <span>
      <tr>
        <td className="category-heading" colSpan="3">
          {header}
          <span className="question-modify" onClick={cannotModify}>?</span>
        </td>
      </tr>
      {data.address.street_addresses.length > 0 ?
        <tr>
          <td>{enterprise.i18nReservation.eplusaccount_0049}</td>
          <td>{data.address.street_addresses[0]}</td>
        </tr> : false }
      {data.address.city ?
        <tr>
          <td>{enterprise.i18nReservation.resflowreview_0065}</td>
          <td>{data.address.city}</td>
        </tr> : false }
      {data.address.postal ?
        <tr>
          <td>{enterprise.i18nReservation.resflowcorporate_0205}</td>
          <td>{data.address.postal}</td>
        </tr> : false }
      {data.address.phone_number ?
        <tr>
          <td>{enterprise.i18nReservation.resflowreview_0010}</td>
          <td>{data.address.phone_number}</td>
        </tr> : false }
      {data.comments ?
        <tr>
          <td>{type === RESERVATION.DELIVERY ? enterprise.i18nReservation.resflowcorporate_0040 : enterprise.i18nReservation.resflowcorporate_0045}</td>
          <td>{data.comments}</td>
        </tr> : false }
      </span>
  )
}

LogisticsContent.displayName = 'LogisticsContent';
