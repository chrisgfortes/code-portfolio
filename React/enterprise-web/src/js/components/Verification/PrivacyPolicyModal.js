import VerificationController from '../../controllers/VerificationController';
import DomManager from '../../modules/outerDomManager';

function onPrintClick (event) {
  event.preventDefault();
  window.print();
}

export default class PrivacyPolicyModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = { disclaimer: null }
    VerificationController.getDisclaimer().then(() =>{
      DomManager.trigger('modalAnchorClick', 'privacy-modal-content');
    });
  }
  render() {
    return (
      <div className="privacy-policy-modal">
        <div className="print-link">
          <a href="#" role="button" tabIndex="0" onKeyPress={a11yClick(onPrintClick)}
             onClick={onPrintClick}>
            {i18n('resflowreview_0153')}
          </a>
        </div>
        <div className="privacy-modal-content" dangerouslySetInnerHTML={{__html: this.props.disclaimer}}/>
      </div>
    );
  }
}

PrivacyPolicyModal.displayName = 'PrivacyPolicyModal';
