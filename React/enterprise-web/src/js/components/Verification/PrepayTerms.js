import VerificationController from '../../controllers/VerificationController';
import ReservationFlowModelController from '../../controllers/ReservationFlowModelController';
import { GLOBAL } from '../../constants';

export default class PrepayTerms extends React.Component {
  constructor( props ) {
    super(props);
    if (!this.props.policy) {
      VerificationController.getPrepayPolicy();
    }
    this.onPrintClick = this.onPrintClick.bind(this);
  }
  componentDidMount () {
    ReservationFlowModelController.onClickElement(document, `.open-rental-policies`, this.onRentalPoliciesClick);
  }
  componentWillUnmount () {
    ReservationFlowModelController.offClickElement(document, `.open-rental-policies`, this.onRentalPoliciesClick);
  }
  onRentalPoliciesClick (event) {
    event.preventDefault();
    VerificationController.callSetModal(false); // Close Prepay Terms Modal
    ReservationFlowModelController.showPolicy(GLOBAL.RENTAL_TERMS.CODE, 'pickup');
    ReservationFlowModelController.showPolicyModal('pickup');
  }
  onPrintClick (event) {
    event.preventDefault();
    if (this.props.showPrepayTerms) {
      let w = window.open();
      w.document.write(document.getElementsByClassName('privacy-modal-content')[0].innerHTML);
      w.print();
      w.close();
    } else {
      window.print();
    }
  }
  close (event) {
    event.preventDefault();
    VerificationController.togglePrepayTerms(false);
  }
  render () {
    const {showPrepayTerms, policy} = this.props;
    return (
      <div className="prepay-policy-modal">
        {showPrepayTerms &&
          <h2>{i18n('resflowreview_8005')}</h2>
        }
        <div className="cf">
          {showPrepayTerms &&
            <div className="go-back">
              <i className="icon icon-nav-carrot-left-green"/>
              <a href="#" role="button" onClick={this.close} onKeyPress={a11yClick(this.close)}>{i18n('prepay_0067') || 'Back'}</a>
            </div>
          }
          <div className="print-link">
            <a href="#" role="button"
               onClick={this.onPrintClick} onKeyPress={a11yClick(this.onPrintClick)}>
              {i18n('resflowreview_0153')}
            </a>
          </div>
        </div>
        <div className="prepay-modal-content" dangerouslySetInnerHTML={{__html: VerificationController.formatPolicyHtml(policy)}}/>
      </div>
    );
  }
}

PrepayTerms.defaultProps = {
  policy: null
};

PrepayTerms.displayName = 'PrepayTerms';
