import Amount from '../Currency/Amount';

export default function ModifyContent ({estimatedTotalValue, paidAmountCurrency, isUnpaidNotRefund, modifyPriceDifferenceCurrency}) {
  return (
    <div>
        <div className="information-block total-price">
          <h3 className="category-label">{"Updated Total"}</h3>
          <div className="amount">
            <Amount className="currency" amount={estimatedTotalValue}/>
          </div>
        </div>

      { !!paidAmountCurrency &&
        <div className="information-block paid-amount">
          <h3 className="category-label">{i18n('prepay_0046') || 'Paid Amount'}</h3>
          <Amount className="currency amount" amount={paidAmountCurrency}/>
          <span> ({i18n('prepay_0047') || 'Original Total'}) </span>
        </div>
      }

      <div className="information-block unpaid-amount">
        <h3 className="category-label">{isUnpaidNotRefund ? i18n('prepay_0048') || 'Unpaid Amount' : i18n('resflowreview_0127') || 'Refund Amount'} </h3>
        <Amount className="currency amount" amount={modifyPriceDifferenceCurrency}/>
        <span> ({isUnpaidNotRefund ? i18n('prepay_0049') || 'Due at end of Rental' : i18n('prepay_0052') || 'Will be refunded after rental is completed'}) </span>
      </div>
    </div>
  );
}

ModifyContent.displayName = ModifyContent;