import VerificationController from '../../controllers/VerificationController';

export default class ResidenceInformation extends React.Component {
  constructor( props ) {
    super( props );
    if (this.state.account.countries && this.state.account.countries.length < 1) {
      VerificationController.callGetCountries(this.state.profile)
        .then(()=> {
          //Two tier hierarchy defaults, domain CountryCode then profile CountryCode
          let selectedCountry = enterprise.countryCode;
          let country = this.state.account.countries.filter((country) => country.country_code == selectedCountry).pop();
          if (country) {
            this.setState({countryData: country});
            if (country.enable_country_sub_division) {
              VerificationController.callGetIssueSubdivisions(selectedCountry);
            }
          }
        });
    }
  }
  _onChange (value, event) {
    VerificationController.setPersonalFields(value, event.target.value);
  }
  _onCountryChange(event) {
    VerificationController.setPersonalFields('countryCode', event.target.value);
    VerificationController.setPersonalFields('subdivision', null);
    let country = this.state.account.countries.filter((country) => country.country_code == event.target.value).pop();
    if (country) {
      this.setState({countryData: country});
      if (country.enable_country_sub_division) {
        VerificationController.callGetSubdivisions(event.target.value);
      }
    }
  }
  _onRegionChange(event) {
    VerificationController.setPersonalFields('subdivision', event.target.value);
  }
  render () {
    const { account, personal } = this.props;

    const residenceLabels = VerificationController.getResidenceLabels( personal, this.state.countryData );

    return (
      <div>
        <div>
          <h2>{enterprise.i18nReservation.resflowreview_0062 + " (" + enterprise.i18nReservation.reservationwidget_0040 + ")"}</h2>

          <div className="field-container country">
            <label htmlFor="country">{enterprise.i18nReservation.resflowreview_0062}</label>
            {account.countries && account.countries.length > 0 ?
              <select className="styled"
                      onChange={this._onCountryChange} id="country"
                      defaultValue={residenceLabels.originCountry}>
                <option value={false}>{enterprise.i18nReservation.resflowreview_0152}</option>
                {account.countries.map(function (country, index) {
                  return <option key={index}
                                 value={country.country_code}>{country.country_name}</option>
                })}
              </select>
              :
              <div className="loading">Loading... please hold!</div>
            }
          </div>
        </div>

        <h2>{enterprise.i18nReservation.resflowreview_0450 + " (" + enterprise.i18nReservation.reservationwidget_0040 + ")"}</h2>

        <div className="field-container">
          <label htmlFor="address">{enterprise.i18nReservation.eplusaccount_0049}</label>
          <input onChange={this._onChange.bind(this, 'streetAddress')} id="address" type="text"
                 value={personal.streetAddress}/>
        </div>

        <div className="field-container">
          <input onChange={this._onChange.bind(this, 'additionalStreetAddress')}
                 id="additionalStreetAddress"
                 type="text"
                 placeholder={enterprise.i18nReservation.reservationwidget_0040}
                 value={personal.additionalStreetAddress}/>
        </div>


        <div className="field-container city">
          <label htmlFor="city">{enterprise.i18nReservation.resflowreview_0065}</label>
          <input onChange={this._onChange.bind(this, 'city')} id="city" type="text"
                 value={personal.city}/>
        </div>

        {enableIssuingAuthority ?
          <div className="field-container subdivision">
            <label htmlFor="subdivision">{residenceLabels.subdivisionLabel}</label>
            {account.subdivisions && account.subdivisions.length > 0 ?
              <select className="styled"
                      onChange={this._onRegionChange} id="subdivision"
                      defaultValue={personal.subdivision}>
                <option value={false}>{enterprise.i18nReservation.resflowreview_0152}</option>
                {account.subdivisions.map(function (region, index) {
                  return <option key={index}
                                 value={region.country_subdivision_code}>{region.country_subdivision_name}</option>
                })}
              </select>
              :
              <input onChange={this._onRegionChange} id="subdivision" type="text"
                     value={personal.subdivision}/>
            }
          </div> : false }

        <div className="field-container postal">
          <label htmlFor="postal">{residenceLabels.postalLabel}</label>
          <input onChange={this._onChange.bind(this, 'postal')} id="postal" type="text"
                 value={personal.postal}/>
        </div>
      </div>
    );
  }
}

ResidenceInformation.displayName = ResidenceInformation;
