import VerificationController from '../../controllers/VerificationController';

export default function ConflictAccountModal () {
  return (
    <div className="logout-modal">
      <p>{enterprise.i18nReservation.resflowviewmodifycancel_0065}</p>

      <p>{enterprise.i18nReservation.resflowviewmodifycancel_0066}</p>

      <p>{enterprise.i18nReservation.resflowviewmodifycancel_0067}</p>

      <p>{enterprise.i18nReservation.resflowviewmodifycancel_0068}</p>

      <div className="modal-actions">
        <button className="btn close-conflict-modal"
                onClick={VerificationController.closeConflictAccountModal}>{enterprise.i18nReservation.resflowcorporate_0201}</button>
        <button className="btn" onClick={VerificationController.confirmConflictAccount}>{enterprise.i18nReservation.resflowcorporate_0200}</button>
      </div>
    </div>
  );
}

ConflictAccountModal.displayName = 'ConflictAccountModal';
