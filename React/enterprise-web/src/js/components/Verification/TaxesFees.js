import VerificationController from '../../controllers/VerificationController';

export default class TaxesFees extends React.Component {
  constructor ( props ) {
    super( props );
    if (!this.props.verification.taxesFeesDisclaimer) {
      VerificationController.getTaxesFeesDisclaimer();
    }
  }
  componentWillUnmount () {
    VerificationController.cleanupScrollOffset();
  }
  render () {
    return (
      <div className="taxes-fees-modal">
        <div className="taxes-fees-modal-content">
          <div className="privacy-modal-content" dangerouslySetInnerHTML={{__html: this.props.verification.taxesFeesDisclaimer}}/>
        </div>
      </div>
    );
  }
}

TaxesFees.displayName = 'TaxesFees';
