import VerificationController from '../../controllers/VerificationController';

export default function DNCModal ( ) {
  return (
    <div className="logout-modal">
      <p>{enterprise.i18nReservation.resflowcorporate_0069}</p>

      <div className="modal-actions">
        <button className="btn close-conflict-modal"
                onClick={VerificationController.closeConflictAccountModal}>{enterprise.i18nReservation.resflowcorporate_0071}</button>
        <button className="btn" onClick={VerificationController.commitWithClearedDNC}>{enterprise.i18nReservation.resflowcorporate_0070}</button>
      </div>
    </div>
  );
}

DNCModal.displayName = 'DNCModal';
