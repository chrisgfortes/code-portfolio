export default function Header ({ confirmationNumber, prepay, modify, onSkipToDetails, onClick }) {
  return modify ? (
    <header className="verification-header cf">
      <div className="verification-title">
        <h1>{i18n('resflowviewmodifycancel_0033')}</h1>
        <h4>{i18n('resflowviewmodifycancel_0024').replace('#{confirmationNumber}', confirmationNumber)}</h4>
      </div>
      <div className="verification-header-buttons">
        <div className="btn-group">
          <button onClick={onClick} id="res-dont" className="btn cancel">
            {i18n('resflowviewmodifycancel_0034')}
          </button>
        </div>
      </div>
      <div className="verification-header-skip">
        <a href="#" role="button" className="hidden-toggleable-link accented"
           onClick={onSkipToDetails} onKeyPress={a11yClick(onSkipToDetails)}>
          {i18n('wcag_nav_0007') || 'Skip to Contact Details'}
        </a>
      </div>
    </header>
  ) : (
    <header className="verification-header cf">
      <div className="verification-title">
        <h1 className="alpha">
          {prepay ? i18n('resflowreview_149') : i18n('resflowreview_150')}
        </h1>
      </div>
      <div className="verification-header-skip">
        <a href="#" role="button" className="hidden-toggleable-link accented"
           onClick={onSkipToDetails} onKeyPress={a11yClick(onSkipToDetails)}>
          {i18n('wcag_nav_0007') || 'Skip to Contact Details'}
        </a>
      </div>
    </header>
  )
}

Header.displayName = 'Header';
