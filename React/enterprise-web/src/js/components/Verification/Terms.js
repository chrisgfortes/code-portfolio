import Validator from '../../utilities/util-validator';
import VerificationController from '../../controllers/VerificationController';

export default class Terms extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      privacyChecked: false,
      termsRequired: enterprise.acceptPrivacyRequired
    };
    this.validator = new Validator(this, this.fieldMap);
  }
  fieldMap () {
    return {
      refs: {
        acceptPrivacy: this.acceptPrivacy,
        acceptPrepay: this.acceptPrepay
      },
      value: {
        acceptPrivacy: this.props.verification.privacyChecked,
        acceptPrepay: this.props.verification.prepayChecked
      },
      schema: {
        acceptPrivacy: () => {
          if (enterprise.acceptPrivacyRequired) {
            return this.props.verification.privacyChecked
          } else {
            return true;
          }
        },
        acceptPrepay: () => {
          if (this.props.prepay) {
            return this.props.verification.prepayChecked
          } else {
            return true;
          }
        }
      }
    };
  }
  _onClick (value, event) {
    event.preventDefault();
    VerificationController.callSetModal( value, event );

  }
  _onChange (value, event) {
    VerificationController.setPrivacyChecked( event.target.checked );
    this.validator.validate('acceptPrivacy', true);
  }
  _onChangePrepay (value, event) {
    VerificationController.setPrepayChecked( event.target.checked );
    this.validator.validate('acceptPrepay', true);
  }
  render () {
    const prepay = this.props.prepay ? '' : 'hidden';

    return (
      <div className="terms-panel">
        <div className="policies-title">{i18n('resflowreview_0088')}<span>*</span></div>
        <form className="terms-form">
          <div className="field-container">
            { this.state.termsRequired ?
              <label ref="privacyCheckbox" className="privacy-checkbox" htmlFor="privacy">
                <input id="privacy"
                       onChange={this._onChange.bind(this, 'privacy')}
                       name="privacy" type="checkbox"
                       ref={c => this.acceptPrivacy = c}
                       checked={this.props.verification.privacyChecked}/>
                <i className="icon icon-forms-checkmark-green"></i>

                {i18n('resflowreview_1007')}
                <a href="#"
                   role="button"
                   onClick={this._onClick.bind(this, 'privacy')}
                   onKeyPress={a11yClick(this._onClick.bind(this, 'privacy'))}
                   >
                  { i18n('resflowreview_1008') }
                </a>
                {i18n('resflowreview_1009')}
              </label>
              : false }
          </div>

          <div className={'field-container ' + prepay}>
            <label ref="prepayCheckbox" className="prepay-checkbox" htmlFor="prepay">
              <input id="prepay"
                     onChange={this._onChangePrepay.bind(this, 'prepay')}
                     ref={c => this.acceptPrepay = c}
                     name="prepay" type="checkbox"
                     checked={this.props.verification.prepayChecked}/>
              <i className="icon icon-forms-checkmark-green"></i>

              {i18n('resflowreview_1007') + ' '}
              <a href="#" onClick={this._onClick.bind(this, 'prepay')}>
                {i18n('resflowreview_0124')}
              </a>
              {i18n('resflowreview_1009')}
            </label>
          </div>

        </form>
      </div>
    );
  }
}

Terms.displayName = 'Terms';
