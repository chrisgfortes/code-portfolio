import VerificationController from '../../actions/VerificationActions';
import GlobalModal from '../Modal/GlobalModal';
import KeyFactsModal from './KeyFactsModal';
import Scroll from '../../utilities/util-scroll';
import { GLOBAL } from '../../constants';
import ReservationCursors from '../../cursors/ReservationCursors';
import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';


const Policies = React.createClass({
  mixins: [
    React.addons.PureRenderMixin,
    BaobabReactMixinBranch
  ],
  getInitialState () {
    return {
      scrollOffset: 0
    };
  },
  cursors: {
    sessionContractDetails: ReservationCursors.sessionContractDetails,
    keyFacts: ReservationCursors.keyFacts,
    policyModal: ReservationCursors.policyModal,
    rentalTerms: ReservationCursors.rentalTerms,
    reservationPolicies: ReservationCursors.reservationPolicies
  },
  onOpenPolicy (item) {
    VerificationController.setReservationPolicyModalContent(item);
    VerificationController.setPolicyModal(true);
    this.setState({
      scrollOffset: Scroll.getTopScroll()
    });
  },
  onClose ( policiesLink ) {
    VerificationController.setPolicyModal(false);
    Scroll.scrollToOffset(this.state.scrollOffset);
    this.setState({
      scrollOffset: 0
    });
    policiesLink.focus();
  },
  getTermDesc () {
    return <li key="0">
      <span className="right-carrot">&gt;</span>
      <button type="button" onClick={this.onOpenPolicy}
              onKeyPress={a11yClick(this.onOpenPolicy)}>
        {this.state.rentalTerms.description}
      </button>
    </li>
  },
  render () {
    const { rentalTerms, sessionContractDetails, reservationPolicies, policyModal, keyFacts } = this.state;
    const policies = reservationPolicies || [];
    const anyPolicies = policies.length > 0;
    let detailedContent;
    let termsDesc;
    if (_.get(rentalTerms, 'code') === GLOBAL.RENTAL_TERMS.CODE) {
      detailedContent = <div dangerouslySetInnerHTML={{ __html: rentalTerms.policy_text[0].rental_terms_and_conditions_text}}/>;
      termsDesc = this.getTermDesc();
    }

    return (
      <div>

        {keyFacts.modal &&
        <KeyFactsModal active={keyFacts.modal}
                       header={i18n('resflowreview_8001') || 'Key Facts About Your Rental'}
                       content={keyFacts.content}
                       cursor={ReservationCursors.keyFacts.concat('modal')}
                       classLabel="scrollable"/>
        }

        {policyModal.content && policyModal.modal &&
        <GlobalModal active={policyModal.modal}
                     header={policyModal.header}
                     content={policyModal.content}
                     onClosing={this.onClose.bind(this, this.headerLabel)}
                     cursor={ReservationCursors.policyModal.concat('modal')}/>
        }
        {detailedContent && policyModal.modal && !policyModal.content &&
        <GlobalModal active={policyModal.modal}
                     header={rentalTerms.description}
                     content={detailedContent}
                     onClosing={this.onClose.bind(this, this.headerLabel)}
                     cursor={ReservationCursors.policyModal.concat('modal')}/>
        }

        {anyPolicies &&
          <div className="policies cf">
            <div className="policies_body">
              <div className="policies_header">
                <h3 className="category-label" tabIndex="-1"
                    ref={c => this.headerLabel = c}>{i18n('resflowreview_0088') || 'Rental Policies'}</h3>
              </div>
              <div className="policies_icon-section">
                <i className="icon icon-ico-policies"/>
              </div>
              <ul className="policies-container">
                {termsDesc}
                {policies.map((item, index) => (
                  <li key={termsDesc ? index + 1 : index}>
                    <span className="right-carrot">&gt;</span>
                    <button type="button" onClick={this.onOpenPolicy.bind(this, item)}
                            onKeyPress={a11yClick(this.onOpenPolicy.bind(this, item))}>
                      {item.description}
                    </button>
                  </li> )
                )}
              </ul>
            </div>
          </div>
        }
        {_.get(sessionContractDetails, 'contract_type') === 'CORPORATE' &&
          <div className="corporate-policy">{i18n('resflowcorporate_0037') || 'Please see your negotiated contract agreement for rental policies.'}</div>
        }
      </div> );
  }
});

module.exports = Policies;
