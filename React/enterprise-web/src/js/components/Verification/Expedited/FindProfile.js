import Validator from '../../../utilities/util-validator';
import ExpeditedController from '../../../controllers/ExpeditedController';
import EnrollmentController from '../../../controllers/EnrollmentController';
import ReservationFlowModelController from '../../../controllers/ReservationFlowModelController';
import Scroll from '../../../utilities/util-scroll';
import classNames from 'classnames';

export default class FindProfile extends React.Component{
  constructor() {
    super();
    this.state = {
      submitLoading: false,
      submitDisabled: true
    };
    this.validator = new Validator(this, this.fieldMap);
    this.fieldMap = this.fieldMap.bind(this);
    this._onCountryChange = this._onCountryChange.bind(this);
    this._onInputChange = this._onInputChange.bind(this);
    this._onSearch = this._onSearch.bind(this);
    this.validateForm = this.validateForm.bind(this);
  }
  fieldMap () {
    let countryIssue = this.props.expedited.countryIssue;
    let DVLA = countryIssue && countryIssue.issuing_authority_name;
    let enableSubdivision = countryIssue || DVLA ?
      countryIssue.enable_country_sub_division : false;

    // @todo this sometimes explodes on the console for some reason
    // nothing to do with the work on this ticket ECR-12417
    let map = {
      refs: {
        findLicense: this.findLicense,
        findRegion: this.findRegion
      },
      value: {
        findLicense: this.findLicense.value
      },
      schema: {
        findLicense: 'string'
      }
    };

    if (enableSubdivision && this.props.account.issueCountrySubdivisions.length) {
      map.value.findRegion = this.findRegion.value;
      map.schema.findRegion = 'string';
    }

    return map;
  }
  _onCountryChange (event) {
    let countryValue = event.target.value || {};
    let selectedCountry = this._getCountry(countryValue);

    ExpeditedController.setInput('regionIssue', null);
    ExpeditedController.setInput('countryIssue', selectedCountry);

    if (EnrollmentController.getIssueCountrySubdivisions(countryValue)) {
      this.validateForm();
    }
  }
  _onInputChange (variable, event) {
    ExpeditedController.setInput(variable, event.target.value);
    this.validateForm();
  }
  _onSearch () {
    if (this.props.personal.lastName) {
      this.setState({
        submitLoading: true,
        submitDisabled: true
      });

      ExpeditedController.expediteSearch();
    } else {
      Scroll.scrollTo('#personal-information');
      //Exception handling for when last name is missing
      let warning = i18n('expedited_0007');
      ReservationFlowModelController.setErrorsForComponent({
        messages: [{
          defaultMessage: warning,
          code: 'missingLastNameForExpedited'
        }]
      }, 'verification');
    }
  }
  validateForm () {
    this.setState({
      submitDisabled: !this.validator.validateAll(null, true).valid
    });
  }
  _orderByCountriesList(countries){
    return _.orderBy(countries, ['country_name'], ['asc']);
  }
  _getCountry(country){
    const { account } = this.props;

    return account.countries.find((item) => {
      return item.country_code == country;
    });
  }
  render () {
    const {expedited, account, errors} = this.props;
    let DVLA = expedited.countryIssue && expedited.countryIssue.issuing_authority_name;
    let enableSubdivision = expedited.countryIssue ? expedited.countryIssue.enable_country_sub_division : false;
    let countries = account.countries = this._orderByCountriesList(account.countries);
    let saveButtonClasses = classNames({
      'btn': true,
      'save': true,
      'disabled': this.state.submitDisabled
    });

    return (
      <form className="expedited-form">
        <h3 className="view-header">{i18n('resflowreview_0040')}</h3>
        {(this.state.submitLoading && !errors) ? <div className="transition"/> :
          <section>
            <div className="expedited-disclaimer">
              {i18n('reservationwidget_0040')} - {i18n('resflowreview_0041')}
            </div>
            <div className="field-container find-country">
              <label htmlFor="find-country">{i18n('resflowreview_0070')}</label>
              {countries.length > 0 ?
                <select id="find-country" className="styled"
                        onChange={this._onCountryChange}
                        value={expedited.countryIssue ? expedited.countryIssue.country_code: null}
                        defaultValue={enterprise.countryCode}>
                  {
                    countries
                      .map(function (country, index) {
                        return (
                          <option
                            key={index}
                            data-index={index}
                            value={country.country_code}>{country.country_name}
                          </option>
                        );
                      })
                  }
                </select>
                :
                <div className="loading"/>
              }
            </div>

            {enableSubdivision && _.get(account, 'issueCountrySubdivisions.length') ? (
              <div className="field-container find-region">
                <label htmlFor="find-region">{i18n('resflowreview_0071')}</label>
                {account.issueCountrySubdivisions && account.issueCountrySubdivisions.length > 0 ? (
                  <select id="find-region" className="styled"
                    ref={c => this.findRegion = c}
                    value={expedited.regionIssue ? expedited.regionIssue : ''}
                    onChange={this._onInputChange.bind(this, 'regionIssue')} id="find-region">
                    <option value="">{i18n('resflowreview_0152')}</option>
                    {DVLA === 'DVLA' ? <option value="DVLA" key={-1}>DVLA</option> :
                      account.issueCountrySubdivisions.map(function (region, index) {
                        return (<option key={index}
                          value={region.country_subdivision_code}>{region.country_subdivision_name}</option>);
                      })}
                  </select>
                ) : <div className="loading"/> }
              </div>
            ) : false}

            <div className="field-container find-license">
              <label htmlFor="find-license">{i18n('resflowreview_0072')}</label>
              <input
                onChange={this._onInputChange.bind(this, 'license')}
                id="find-license" type="text"
                ref={c => this.findLicense = c}
                value={expedited.license}
              />
            </div>

            <div className="find-action">
              <div role="button" tabIndex="0" onClick={this.state.submitDisabled ? false : this._onSearch}
                   onKeyPress={this.state.submitDisabled ? false : a11yClick(this._onSearch)}
                   className={saveButtonClasses}>{i18n('resflowreview_0157') || 'Begin'}
              </div>
              <span className="search-disclaimer">
                <i className="icon icon-included-theftprotection-2"/>
                <span className="disclaimer-content">
                    {i18n('expedited_0015a')}
                  <strong> {i18n('expedited_0015b')} </strong>
                  {i18n('expedited_0015c')}
                </span>
              </span>
            </div>
          </section>
        }
      </form>
    );
  }
}

FindProfile.displayName = "FindProfile";
