import {getDNRMessage} from '../../../controllers/LoginController';
import ExpeditedController from '../../../controllers/ExpeditedController';
import classNames from 'classnames';

export default class DNRWarn extends React.Component{
  constructor() {
    super();
    this.state = {
      checked: false
    };
    this._onConfirm = this._onConfirm.bind(this);
    this._onContinue = this._onContinue.bind(this);
  }
  _onConfirm (event) {
    this.setState({
      checked: event.target.checked
    });
  }
  _onContinue () {
    if (this.state.checked) {
      ExpeditedController.setInput('modal', false);
    }
  }
  render () {
    let disabled = classNames({
      disabled: !this.state.checked,
      btn: true
    });
    let domain = enterprise.country_code ? enterprise.country_code.toLowerCase() : 'com';
    return (
      <div className="dnr-warn">
        <div className="header-container">
          <h3>{i18n('expedited_0051')}</h3>
        </div>
        <div className="message-container">
          {getDNRMessage(domain)}
        </div>
        <label htmlFor="dnr-confirm">
          <input onChange={this._onConfirm} id="dnr-confirm"
                 name="dnr-confirm" type="checkbox"
                 checked={this.state.checked}/>
          {i18n('dnr_0003')}
        </label>

        <div className="modal-action">
          <div onClick={this._onContinue} className={disabled}>{i18n('dnr_0004')}</div>
        </div>
      </div>
    );
  }
}

DNRWarn.displayName = "DNRWarn";
