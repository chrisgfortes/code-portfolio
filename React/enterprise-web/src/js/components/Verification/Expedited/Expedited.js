const TransitionGroup = React.addons.CSSTransitionGroup;
import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import ReservationCursors from '../../../cursors/ReservationCursors';
import ExistingProfile from './ExistingProfile';
import BranchEnrolled from './BranchEnrolled';
import DriverProfile from './DriverProfile';
import EditProfile from './EditProfile';
import FindProfile from './FindProfile';
import NoMatch from './NoMatch';

const Expedited = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    expedited: ReservationCursors.expedited,
    personal: ReservationCursors.personal,
    account: ReservationCursors.account,
    profile: ReservationCursors.profile,
    errors: ReservationCursors.verificationErrors,
    enrollErrors: ReservationCursors.enrollErrors,
    sessionContractDetails: ReservationCursors.sessionContractDetails,
    userLoggedIn: ReservationCursors.userLoggedIn
  },
  render () {
    const {
      expedited,
      personal,
      account,
      profile,
      errors,
      enrollErrors,
      sessionContractDetails,
      userLoggedIn
    } = this.state;

    const expeditedComponents = {
      'noMatch': (<NoMatch {...{ sessionContractDetails, userLoggedIn, account, errors, expedited, profile }} />),
      'driver': (<DriverProfile {...{ sessionContractDetails, userLoggedIn, account, errors, expedited, profile }} />),
      'profile': (<ExistingProfile {...{ personal, errors, enrollErrors, profile }} />),
      'branch': (<BranchEnrolled {...{ profile, sessionContractDetails, userLoggedIn, account, errors, expedited }}/>),
      'edit': (<EditProfile {...{ profile, userLoggedIn, account, errors, expedited }} />),
      'search': (<FindProfile {...{ account, personal, expedited, errors }} />)
    };

    const content = (expeditedComponents[expedited.render] || expeditedComponents['search']);

    return content && (
      <div id="expedited" className="expedited section-content">
        <TransitionGroup
          transitionName="ease"
          transitionEnterTimeout={0}
          transitionLeaveTimeout={0}
        >
          {content}
        </TransitionGroup>
      </div>
    );
  }
});

export default Expedited;
