export default function EnterprisePlusBanner() {
  return (
    <div className="match-banner ep-banner">
      <i className="icon icon-eplus-logo"/>

      <div className="content-container">
        <div className="header-message">{i18n('expedited_0033a')}</div>
        <div className="content-message">
          {i18n('expedited_0033b')}
        </div>
      </div>
    </div>
  );
}

EnterprisePlusBanner.displayName = "EnterprisePlusBanner";
