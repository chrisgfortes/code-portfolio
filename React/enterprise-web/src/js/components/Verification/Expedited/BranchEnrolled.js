import ExpeditedController from '../../../controllers/ExpeditedController';
import ExpeditedRenterInformation from './ExpeditedRenterInformation';
import CreateProfile from './CreateProfile';

export default function BranchEnrolled({ sessionContractDetails, userLoggedIn, account, errors, expedited, profile }) {
  return (
    <form className="expedited-form">

      <div className="view-header">{enterprise.i18nReservation.resflowreview_0040}
        <span onClick={() => ExpeditedController.clearProfile()} className="edit">{enterprise.i18nReservation.expedited_0026}</span>
      </div>
      <div className="expedited-disclaimer">
        {i18n('expedited_0038')}
      </div>

      <CreateProfile sessionContractDetails={sessionContractDetails} />

      <ExpeditedRenterInformation
        {...{
          profile,
          userLoggedIn,
          account,
          errors,
          expedited
        }}
      />
    </form>
  );
}


module.exports = BranchEnrolled;
