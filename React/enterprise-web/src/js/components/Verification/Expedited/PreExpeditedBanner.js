import ExpeditedController from '../../../controllers/ExpeditedController';

function _enterprisePlusLogin () {
  ExpeditedController.setInput('modal', 'ep');
}

function  _emeraldClubLogin () {
  ExpeditedController.setInput('modal', 'ec');
}

export default function PreExpeditedBanner ({onlyShowEP}) {
  return (
    <div className="pre-expedited-banner">
      <i className="icon icon-eplus-logo-black" aria-hidden="true" role="presentation"/>
      {onlyShowEP && <i className="icon icon-brand-national" aria-hidden="true" role="presentation"/>}
      {onlyShowEP ?
        <div className="content-container">
          <span className="cta-message accented" role="button" tabIndex="0" id="focus-pexpedited-banner-login"
               onKeyPress={a11yClick(() => _enterprisePlusLogin())} onClick={() => _enterprisePlusLogin()}>
            {i18n('expedited_0004a')}
          </span>
          <span className="content-message">
            {' ' + i18n('expedited_0004b')}
          </span>
        </div>
        :
        <div className="content-container">
          <span className="content-message">
            {(i18n('expedited_0063') || 'Are you a loyalty member? Please sign in to your') + ' '}
          </span>
          <span className="cta-message accented" role="button" tabIndex="0"
               onKeyPress={a11yClick(() => _enterprisePlusLogin())} onClick={() => _enterprisePlusLogin()}>
            {(i18n('expedited_0064') || 'Enterprise Plus') + ' '}
          </span>
          <span className="content-message">
            {(i18n('expedited_0065') || 'or') + ' '}
          </span>
          <span className="cta-message accented" role="button" tabIndex="0"
               onKeyPress={() => a11yClick(_emeraldClubLogin())} onClick={() => _emeraldClubLogin()}>
            {(i18n('expedited_0066') || 'Emerald Club') + ' '}
          </span>
          <span className="content-message">
            {i18n('expedited_0067') || 'account to speed through the form below'}
          </span>
        </div>
      }
    </div>
  );
}

PreExpeditedBanner.displayName = "PreExpeditedBanner";
