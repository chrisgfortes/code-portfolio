import Error from '../../Errors/Error';
import Validator from '../../../utilities/util-validator';
import classNames from 'classnames';
import PaymentModelController from '../../../controllers/PaymentModelController';
import LoginController from '../../../controllers/LoginController';
import ExpeditedController from '../../../controllers/ExpeditedController';
import ReservationFlowModelController from '../../../controllers/ReservationFlowModelController';

export default class Login extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      epUsername: null,
      epPassword: null,
      epRememberMe: false,
      ecUsername: null,
      ecPassword: null,
      ecRememberMe: false,
      brand: this.props.brand ? this.props.brand.toUpperCase() : 'EP'
    }
    this.validator = new Validator(this, this.fieldMap);
    this.fieldMap = this.fieldMap.bind(this);
    this._handleInputChange = this._handleInputChange.bind(this);
    this._login = this._login.bind(this);
    this._handleKeyUp = this._handleKeyUp.bind(this);
    this.processLogin = this.processLogin.bind(this);
    this._onForgot = this._onForgot.bind(this);
    this._onRememberMe = this._onRememberMe.bind(this);
    this._toggleBrand = this._toggleBrand.bind(this);
  }
  fieldMap() {
    return {
      refs: {
        epUsername: this.epUsername,
        epPassword: this.epPassword,
        ecUsername: this.ecUsername,
        ecPassword: this.ecPassword
      },
      value: {
        epUsername: this.epUsername.value,
        epPassword: this.epPassword.value,
        ecUsername: this.ecUsername.value,
        ecPassword: this.ecPassword.value
      },
      schema: {
        epUsername: 'string',
        epPassword: 'password',
        ecUsername: 'string',
        ecPassword: 'password'
      }
    }
  }
  _handleInputChange (attribute, event) {
    this.setState(
      {
        [attribute]: event.target.value
      }
    );
  }
  _login (brand) {
    let handle = brand === 'EP' ? 'ep' : 'ec',
      username = this.validator.validate(handle + 'Username', this[handle + 'Username'].value),
      password = this.validator.validate(handle + 'Password', this[handle + 'Password'].value);

    if (password && username && !this.state.submitLoading) {

      let params = {
        username: this[handle + 'Username'].value,
        password: this[handle + 'Password'].value,
        rememberMe: this.state[handle + 'RememberMe'],
        brand: this.state.brand
      };

      this.processLogin(params);
    }
  }
  processLogin(params) {
    this.setState({submitLoading: true});

    LoginController.login(params).then((response) => {
      if (response && response.errors && response.errors.messages && response.errors.messages.length) {
        this.setState({submitLoading: false});
        // ECR-14251 ErrorsController
        if (response.errors.messages.some((message) => message.code === 'CROS_LOGIN_WEAK_PASSWORD_ERROR')) {
          ExpeditedController.setInput('modal', null);
        }
        if (response.errors.messages.some((message) => message.code === 'CROS_LOGIN_TERMS_AND_CONDITIONS_ACCEPT_VERSION_MISMATCH')) {
          ExpeditedController.setInput('modal', 'eplusTC');
        }
      } else {
        const saveForLater = LoginController.getSavePaymentForLater();
        if (saveForLater && this.props.paymentReferenceID) {
          PaymentModelController.registerRoundTrip(this.props.paymentReferenceID, () => {
            this.setState({submitLoading: false});
          });
        } else {
          this.setState({submitLoading: false});
        }
      }
    });
  }
  _onForgot () {
    window.open(this.props.supportLinks ? this.props.supportLinks.forgot_password_url : 'https://legacy.enterprise.com/car_rental/enterprisePlusForgotPassword.do');
  }
  _handleKeyUp (brand, event) {
    if (event.keyCode === 13) {
      this._login(brand);
    }
  }
  _toggleBrand (section) {
    ReservationFlowModelController.clearErrorsForComponent('loginWidget');
    this.setState({
      brand: section
    });
  }
  _onRememberMe (brand, event) {
    this.setState({
      [brand]: event.target.checked
    });
  }
  render () {

    let ep = this.state.brand === 'EP',
      ec = this.state.brand === 'EC',
      epContainer = classNames({
        'login-fields': true,
        'ep': true,
        'active': ep
      }),
      ecContainer = classNames({
        'login-fields': true,
        'ec': true,
        'active': ec
      });

    return (
      <div className="login">
        <div className="header-container">
          {ep ? (
            <div className="header-container-epLogin">
              <i className="icon icon-eplus-logo-black" />
              <h2>{i18n('loyaltysignin_0001')}</h2>
            </div>
          ) : (
            <h3 onClick={this._toggleBrand.bind(this, 'EP')}>{i18n('loyaltysignin_0001')}</h3>
          )}
        </div>

        <div className={epContainer}>
          <Error errors={this.props.errors} type="GLOBAL"/>

          <label htmlFor="epFieldUsername">
            <span>{i18n('loyaltysignin_0003')}</span>
            <input
              onChange={this._handleInputChange.bind(this, 'epUsername')}
              onKeyUp={this._handleKeyUp.bind(this, 'EP')}
              type="text"
              name="ep-email"
              id="epFieldUsername"
              ref={c => this.epUsername = c}
            />
          </label>

          <label htmlFor="epFieldPassword">
            <span>{i18n('loyaltysignin_0004')}</span>
            <input
              onChange={this._handleInputChange.bind(this, 'epPassword')}
              onKeyUp={this._handleKeyUp.bind(this, 'EP')}
              type="password"
              name="ep-password"
              id="epFieldPassword"
              ref={c => this.epPassword = c}
            />
          </label>

          <label className="ep-remember"
                 htmlFor="ep-remember">
            <input onChange={this._onRememberMe.bind(this, 'epRememberMe')}
                   checked={this.state.epRememberMe} type="checkbox" id="ep-remember"
                   name="ep-remember"/>
            <i className="icon icon-forms-checkmark-green"/>
            {i18n('resflowcorporate_4002')}
          </label>

          <div className={this.state.submitLoading ? 'loading' : false}/>
          {this.state.submitLoading ? false :
            <div className="btn"
                 onClick={this._login.bind(this, 'EP')}>{i18n('resflowreview_0080')}</div>}
          <div className="forgot"
               onClick={this._onForgot}>{i18n('loyaltysignin_0019')}</div>
        </div>
        <div className="divider">
          <span className="strike-through"/>
          <i>{i18n('fedexcustompath_0002') || 'OR'}</i>
          <span className="strike-through"/>
        </div>

        <div className="header-container">
          {ec ? (
            <div className="header-container-ecLogin">
              <i className="icon icon-brand-national" />
              <h2>{i18n('resflowcorporate_4005')}</h2>
            </div>
          ) : (
            <h3 onClick={this._toggleBrand.bind(this, 'EC')}>{i18n('resflowcorporate_4005')}</h3>
          )}
        </div>

        <div className={ecContainer}>

          <Error errors={this.props.errors} type="GLOBAL"/>

          <label className="field-eclogin" htmlFor="ecFieldUsername">
            <span className="field-eclogin-label">{i18n('resflowcorporate_4003')}</span>
            <input
              onChange={this._handleInputChange.bind(this, 'ecUsername')}
              onKeyUp={this._handleKeyUp.bind(this, 'EC')}
              type="text"
              name="ec-email"
              id="ecFieldUsername"
              ref={c => this.ecUsername = c}
            />
          </label>

          <label className="field-eclogin" htmlFor="ecFieldPassword">
            <span>{i18n('loyaltysignin_0004')}</span>
            <input
              onChange={this._handleInputChange.bind(this, 'ecPassword')}
              onKeyUp={this._handleKeyUp.bind(this, 'EC')}
              type="password"
              name="ec-password"
              id="ecFieldPassword"
              ref={c => this.ecPassword = c}
            />
          </label>

          <label className="ec-remember">
            <input
              onChange={this._onRememberMe.bind(this, 'ecRememberMe')}
              checked={this.state.epRememberMe} type="checkbox" id="ec-remember"
              name="ec-remember"
            />
            <i className="icon icon-forms-checkmark-green"/>
            {i18n('resflowcorporate_4002')}
          </label>

          <div className={this.state.submitLoading ? 'loading' : false}/>
          {this.state.submitLoading ? false :
            <div className="btn"
                 onClick={this._login.bind(this, 'EC')}>{i18n('resflowreview_0080')}</div>}
        </div>
      </div>
    );
  }
}

Login.displayName = "Login";

