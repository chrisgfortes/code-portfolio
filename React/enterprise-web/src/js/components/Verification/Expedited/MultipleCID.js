import ExpeditedController from '../../../controllers/ExpeditedController';
import ReservationFlowModelController from '../../../controllers/ReservationFlowModelController';

function _onRestart (cid) {
  // keeping the session makes this easier and also
  // the user can hit the back button for an UNDO action of sorts
  ExpeditedController.setInput('modal', false);
  ExpeditedController.setInput('restart', true);
  ReservationFlowModelController.callGoToReservationStep('book');
  ExpeditedController.setExpeditedCoupon(cid);
}

function _onContinue () {
  ExpeditedController.setInput('modal', false);
}

export default function MultipleCode ({profile, expedited}) {
  // ECR-14251
  const code = profile && profile.basic_profile.customer_details ? profile.basic_profile.customer_details.contract_name : '';
  const CID = profile && profile.basic_profile.customer_details.contract_number;
  return (
    <div className="multiple-cid">
      <div className="header-container">
        <h3>{i18n('resflowreview_0152')}</h3>
      </div>

      <div className="top-container">
        <div onClick={() => _onRestart(CID)}
             className="btn">{i18n('resflowcorporate_0025')}</div>
        <div className="top-disclaimer">
          {i18n('resflowcorporate_0054', {account: code})}
          {expedited.previousChargeType === 'PREPAY' &&
            <small>
              {i18n('expedited_0062') || 'You will not be able to pay now with this selection'}
            </small>
          }
        </div>
      </div>
      <hr />
      <div className="bottom-container">
        <div onClick={() => _onContinue()}
             className="btn">{i18n('reservationwidget_0014')}</div>
        <div className="bottom-disclaimer">
          {i18n('expedited_0048')}
        </div>
      </div>
    </div>
  );
}

MultipleCode.displayName = "MultipleCode";
