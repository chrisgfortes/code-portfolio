export default function EmeraldClubBanner() {
  return (
    <div className="match-banner ec-banner">
      <i className="icon icon-brand-national"/>

      <div className="content-container">
        <div className="header-message">{i18n('expedited_0036a')}</div>
        <div className="content-message">
          {i18n('expedited_0033b')}
        </div>
        <div className="emerald-club-disclaimer">
          {i18n('expedited_0037a')}
          <span className="accented"> {i18n('expedited_0037b')}</span>
        </div>
      </div>
    </div>
  );
}

EmeraldClubBanner.displayName = "EmeraldClubBanner";
