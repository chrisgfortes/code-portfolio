import ExpeditedController from '../../../controllers/ExpeditedController';
import CreateProfile from './CreateProfile';
import Scroll from '../../../utilities/util-scroll';
import ExpeditedRenterInformation from './ExpeditedRenterInformation';

export default class NoMatch extends React.Component{
  constructor() {
    super();
  }
  componentDidMount () {
    if (!this.props.errors) {
      const expeditedHeading = this.ariaHeading;
      if (expeditedHeading) {
        ExpeditedController.setExpeditedHeadingFocus(expeditedHeading);
      }
      Scroll.scrollTo('#expedited');
    }
  }
  _onSkip () {
    ExpeditedController.clearProfile();
  }
  render () {
    const {
      profile,
      userLoggedIn,
      account,
      errors,
      expedited,
      sessionContractDetails
    } = this.props;

    return (
      <form className="expedited-form">

        <h3 className="view-header" ref={c => this.ariaHeading = c}>{i18n('resflowreview_0040')}
          <span role="button" tabIndex="0" onKeyPress={a11yClick(this._onSkip)} onClick={this._onSkip} className="edit">
            {i18n('expedited_0026')}
          </span>
        </h3>
        <div className="expedited-disclaimer">
          {i18n('expedited_0027a')}
        </div>

        <ExpeditedRenterInformation
          {...{
            profile,
            userLoggedIn,
            account,
            errors,
            expedited
          }}
        />

        <CreateProfile sessionContractDetails={sessionContractDetails}/>
      </form>
    );
  }
}

NoMatch.displayName = "NoMatch";
