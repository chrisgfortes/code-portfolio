import EnrollmentController from '../../../controllers/EnrollmentController';
import ExpeditedController from '../../../controllers/ExpeditedController';
import ProfileController from '../../../controllers/ProfileController';
import ReservationCursors from '../../../cursors/ReservationCursors';
import DateSelector from '../../DateSelector/DateSelector';

export default class RenterInformation extends React.Component{
  constructor(props) {
    super(props);
    let countryCode = enterprise.countryCode;

    if (this.props.expedited.countryResidence) {
      countryCode = this.props.expedited.countryResidence.country_code;
    }

    this.state = {
      hasSubdivision: false,
      hasLoadedSubdivisions: false,
      isEmptyField: false,
      regionResidence: null,
      regionResidenceModified: false,
      countryResidenceModified: false
    }

    EnrollmentController.getSubdivisions(countryCode);
    this._onCountryChange = this._onCountryChange.bind(this);
    this._onInputChange = this._onInputChange.bind(this);
    this._onEdit = this._onEdit.bind(this);
    this._hasSubdivision = this._hasSubdivision.bind(this);
    this._onRegionChange = this._onRegionChange.bind(this);
    this._isFieldInvalid = this._isFieldInvalid.bind(this);

    this._handleFocus = this._handleFocus.bind(this);
    this._handleBlur = this._handleBlur.bind(this);
  }

  componentWillMount(){
    this._hasSubdivision();
  }

  componentDidMount() {
    const { profile } = this.props;
    if (profile) {
      this.state = Object.assign(this.state, {
        regionResidence: _.get(profile, 'license_profile.country_subdivision_name')
      });
    }
  }

  _hasSubdivision(){
    const { expedited, account } = this.props;
    const enableSubdivision = !!_.get(expedited, 'countryResidence.enable_country_sub_division');

    this.setState({
      hasSubdivision: enableSubdivision,
      hasLoadedSubdivisions: !!_.get(account, 'subdivisions.length')
    });
  }

  _onCountryChange (event) {
    let elementIndex = event.target.selectedIndex;
    let index = event.target.children[elementIndex].getAttribute('data-index');

    ExpeditedController.setInput('countryResidence', this.props.account.countries[index]);
    ExpeditedController.setInput('regionResidence', null);
    EnrollmentController
      .getSubdivisions(event.target.value)
      .then(() => {
        this.setState({
          regionResidence: null,
          countryResidenceModified: true
        })
        this._hasSubdivision()
      })
    ;
  }

  _isEmptyField(event) {
    this.setState({
      isEmptyField: (!event.target.value.length)
    });
  }

  _onInputChange (variable, event) {
    ExpeditedController.setInput(variable, event.target.value);
    this._isEmptyField(event);
  }

  _onRegionChange(event){
    this.setState({
      regionResidenceModified: true,
      regionResidence: event.target.value
    });

    ExpeditedController.setInput('regionResidence', event.target.value);
    this._isEmptyField(event);
  }

  _onEdit () {
    //Clears all including routing and redirects user back to full search
    ExpeditedController.clearProfile();
  }

  _clearValueField(event){
    if (enterprise.utilities.isMaskedField(event.target.value)) {
      event.target.value = '';
    }
  }

  _revertValueField(field, event){
    const { expedited } = this.props;
    event.target.value = _.get(expedited, field);
  }

  _hasError(field, checkEmptyField){
    const { isEmptyField } = this.state;
    const { expedited, errors } = this.props;
    const fieldErrors = EnrollmentController.getExpeditedEnrollFieldErrors(expedited, errors);
    if (checkEmptyField) {
      return (fieldErrors[field] || isEmptyField);
    }

    return fieldErrors[field];
  }

  _handleBlur(attribute) {
    const { countryResidenceModified } = this.state;
    const { profile } = this.props;

    if (!this.state[attribute.concat('Modified')]){
      const map = {
        regionResidence: (!countryResidenceModified ? _.get(profile, 'license_profile.country_subdivision_name') : '')
      };

      const fieldValue = (map[attribute] || '');

      this.setState({
        [attribute]: fieldValue
      });

      this[attribute].value = fieldValue;
    }
  }

  _handleFocus(attribute) {
    this.setState({
      [attribute]: ''
    });

    this[attribute].value = '';
  }

  _isFieldInvalid(field){
    return (this._hasError(field) ? 'invalid' : '');
  }

  render() {
    const {
      account,
      userLoggedIn,
      expedited
    } = this.props;

    const {
      hasSubdivision,
      regionResidence
    } = this.state;

    const enablePostal = expedited.countryResidence.postal_code_type;
    const hasProfile = ['driver', 'edit'].includes(expedited.render);
    const enableLicenseIssueDate = ProfileController.enableLicenseIssueDate(expedited);
    const enableLicenseExpiryDate = ProfileController.enableLicenseExpiryDate(expedited);
    const authorityLabel = {
      'US': i18n('resflowreview_0066'),
      'CA': i18n('resflowreview_0067')
    }

    const subdivisionLabel = (
      _.find(
        authorityLabel,
        (value, key) => (key == expedited.countryResidence.country_code)
      ) || i18n('eplusenrollment_0056')
    );

    return (
      <div className="renter-information">
        <div className="field-container issued-by">
          <div className="label">{i18n('resflowreview_0071')}</div>
          <div className="issued-by-value">
            { ExpeditedController.getLocationByExpedited(expedited) }
          </div>
        </div>
        <div className="field-container license">
          <div className="label">{i18n('resflowreview_0072')}</div>
          <div
            className="license-value">
            {hasProfile ? ExpeditedController.maskField(expedited.license, 4) : expedited.license}
            {!userLoggedIn && (
              <span
                role="button"
                tabIndex="0"
                onKeyPress={a11yClick(this._onEdit)}
                onClick={this._onEdit} className="accented edit">
                  {i18n('eplusaccount_0119')}
              </span>
            )}
          </div>
        </div>

        {enableLicenseExpiryDate && (
          <div className="field-container license-expiry">
            <div className="label">{i18n('expedited_0028')}</div>
            <div>
              <DateSelector
                label={i18n('expedited_0028')}
                cursor={ReservationCursors.expeditedLicenseExpiryDate}
                error={this._hasError('licenseExpiryError')}
              />
            </div>
          </div>
        )}

        {enableLicenseIssueDate && (
          <div className="field-container license-issue">
            <div className="label">{i18n('resflowreview_0720')}</div>
            <div>
              <DateSelector
                label={i18n('resflowreview_0720')}
                cursor={ReservationCursors.expeditedLicenseIssueDate}
                error={this._hasError('licenseIssueError')}
              />
            </div>
          </div>
        )}

        <div className="field-container date-of-birth">
          <div className="label">{i18n('eplusenrollment_0031')}</div>
          <div>
            <DateSelector
              masked={hasProfile}
              disabled={hasProfile}
              label={i18n('eplusenrollment_0031')}
              cursor={ReservationCursors.expeditedDateOfBirth}
              error={this._hasError('birthDateError')}
            />
          </div>
        </div>

        <div className="section-header">{i18n('resflowreview_0450')}</div>

        <div className="field-container country">
          <label htmlFor="find-country">{i18n('resflowreview_0062')}</label>
          {account.countries.length ?
            <select className="styled"
                    onChange={this._onCountryChange} id="find-country"
                    value={expedited.countryResidence ? expedited.countryResidence.country_code : null}
                    defaultValue={enterprise.countryCode}
                    aria-required="true"
                    aria-invalid="false">
              {account.countries.map(function (country, index) {
                return (
                  <option
                    key={index}
                    data-index={index}
                    value={country.country_code}>{country.country_name}
                  </option>
                );
              })}
            </select>
            :
            <div className="loading"/>
          }
        </div>

        <div className="field-container">
          <label htmlFor="address">
            {i18n('resflowreview_0063')}
          </label>
          <input
            id="address"
            type="text"
            value={expedited.address}
            aria-required="true"
            aria-invalid={this._hasError('addressError', true)}
            className={this._isFieldInvalid('addressError')}
            onChange={this._onInputChange.bind(this, 'address')}
          />
        </div>

        <div className="field-container">
          <label htmlFor="addressTwo">{i18n('resflowreview_0064')}</label>
          <input
            type="text"
            id="addressTwo"
            aria-required="false"
            value={expedited.addressTwo}
            onFocus={this._clearValueField}
            onBlur={this._revertValueField.bind(this, 'addressTwo')}
            onChange={this._onInputChange.bind(this, 'addressTwo')}
          />
        </div>

        {enablePostal && (
          <div className="field-container postal">
            <label htmlFor="postal">{i18n('resflowreview_0068')}</label>
            <input
              onChange={this._onInputChange.bind(this, 'postal')}
              onFocus={this._clearValueField}
              onBlur={this._revertValueField.bind(this, 'postal')}
              id="postal"
              type="text"
              value={expedited.postal}
              aria-required="true"
              aria-invalid={this._hasError('zipCodeError', true)}
              className={this._isFieldInvalid('zipCodeError')}
            />
          </div>
        )}

        <div className="field-container city">
          <label htmlFor="city">{i18n('resflowreview_0065')}</label>
          <input
            id="city"
            type="text"
            onChange={this._onInputChange.bind(this, 'city')}
            onFocus={this._clearValueField}
            onBlur={this._revertValueField.bind(this, 'city')}
            value={expedited.city}
            aria-required="true"
            aria-invalid={this._hasError('cityError', true)}
            className={this._isFieldInvalid('cityError')}/>
        </div>

        {hasSubdivision && (
          <div className="field-container region">
            <label htmlFor="region">{subdivisionLabel}</label>
              <select
                className="styled"
                onChange={this._onRegionChange}
                onFocus={this._handleFocus.bind(null, 'regionResidence')}
                onBlur={this._handleBlur.bind(null, 'regionResidence')}
                id="region"
                ref={r => this.regionResidence = r}
                value={regionResidence}
              >

                {
                  <option
                    key="0"
                    value={enterprise.utilities.isMaskedField(regionResidence) ? regionResidence : ''}
                  >
                    {enterprise.utilities.isMaskedField(regionResidence) ? regionResidence : i18n('resflowreview_0152')}
                  </option>
                }

                {
                  account.subdivisions.map(function (region, index) {
                    return (<option key={index}
                      value={region.country_subdivision_code}>{region.country_subdivision_name}</option>);
                  })
                }
              </select>
          </div>
        )}
      </div>
    );
  }
}

RenterInformation.displayName = "RenterInformation";
