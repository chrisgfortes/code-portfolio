import ExpeditedController from '../../../controllers/ExpeditedController';
import CreateProfile from './CreateProfile';
import ExpeditedRenterInformation from './ExpeditedRenterInformation';

export default function DriverProfile({ sessionContractDetails, userLoggedIn, account, errors, expedited, profile }) {
  return (
    <form className="expedited-form">
      <div className="view-header">{i18n('resflowreview_0040')}
        <span onClick={() => ExpeditedController.clearProfile()} className="edit">{i18n('expedited_0026')}</span>
      </div>
      <div className="expedited-disclaimer">
        {i18n('expedited_0033b')}
      </div>

      <ExpeditedRenterInformation
        {...{
          profile,
          userLoggedIn,
          account,
          errors,
          expedited
        }}
      />

      <CreateProfile sessionContractDetails={sessionContractDetails} />
    </form>
  );
}

DriverProfile.displayName = "DriverProfile";
