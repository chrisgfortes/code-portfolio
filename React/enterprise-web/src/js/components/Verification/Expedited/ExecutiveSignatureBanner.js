export default function ExecutiveSignaturesBanner ({profile}) {
  // ECR-14250
  let firstName = profile && profile.basic_profile.first_name;
  let lastName = profile && profile.basic_profile.last_name;
  return (
    <div className="match-banner executive-signature-banner">
      <i className="icon icon-icon-bell"/>

      <div className="content-container">
        <div className="header-message">
          {i18n('expedited_0039a').replace('#{firstName}', firstName).replace('#{lastName}', lastName)}</div>
        <div className="content-message">
          {i18n('expedited_0033b')}
        </div>
        <div className="executive-signature-disclaimer">
          {i18n('expedited_0040a')}
          <strong> {i18n('expedited_0040b')} </strong>
          {i18n('expedited_0040c')}
          <a href={'mailto:'+i18n('expedited_0040d')}
             className="accented"> {i18n('expedited_0040d')}</a>
        </div>
      </div>
    </div>
  );
}

ExecutiveSignaturesBanner.displayName = "ExecutiveSignaturesBanner";
