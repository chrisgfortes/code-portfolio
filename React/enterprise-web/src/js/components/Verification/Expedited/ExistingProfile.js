import ExpeditedController from '../../../controllers/ExpeditedController';
import VerificationController from '../../../controllers/VerificationController';
import Error from '../../Errors/Error';
import EnterprisePlusBanner from './EnterprisePlusBanner';
import EmeraldClubBanner from './EmeraldClubBanner';
import ExecutiveSignatureBanner from './ExecutiveSignatureBanner';
import { PROFILE } from '../../../constants';

export default class ExistingProfile extends React.Component{
  constructor() {
    super();
    this._onEdit = this._onEdit.bind(this);
    this._onChange = this._onChange.bind(this);
  }
  _onEdit () {
    ExpeditedController.setInput('modal', 'ep');
  }
  _onChange (value, event) {
    VerificationController.setPersonalFields(value, event.target.value);
  }
  render () {
    const {profile, personal, errors, enrollErrors} = this.props;
    let banner = null;
    // ECR-14250
    const loyalty = profile.basic_profile.loyalty_data;
    const account = loyalty.loyalty_program;
    const type = loyalty.loyalty_member_ship_type;
    const ePlus = account === PROFILE.ENTERPRISE_PLUS;
    const isNotExecutiveOrSignatureProfile = ExpeditedController.isNotExecutiveOrSignatureProfile(loyalty.loyalty_member_ship_type);

    if (ePlus) {
      switch (type) {
        case PROFILE.EXECUTIVE:
          banner = <EnterprisePlusBanner />;
          break;
        case PROFILE.SIGNATURE :
          banner = <ExecutiveSignatureBanner profile={profile} />;
          break;
        default:
          banner = <EnterprisePlusBanner />;
      }
    } else if (account === PROFILE.EMERALD_CLUB) {
      banner = <EmeraldClubBanner />;
    }

    return (
      <form id="expedited-profile" className="expedited-profile">
        {banner}
        <Error errors={errors} type="GLOBAL"/>
        <Error errors={enrollErrors} type="GLOBAL"/>

        <div className="beta borderless">{i18n('expedited_0035')}
          {ePlus && isNotExecutiveOrSignatureProfile &&
            <span onClick={this._onEdit} className="edit">
              <i className="icon icon-specs-seats-green"/>
              {i18n('expedited_0034')}
            </span>
          }
        </div>

        <div className="view-header">{i18n('resflowreview_0003')}</div>

        <div className="section-header">{i18n('eplusenrollment_0002')}</div>
        <dl>
          <div>
            <dt>{i18n('reservationwidget_0021')}</dt>
            <dd>{personal.firstName}</dd>
          </div>
          <div>
            <dt>{i18n('reservationwidget_0022')}</dt>
            <dd>{personal.lastName}</dd>
          </div>

          <div className="phone-container">
            <label htmlFor="phoneNumber">{i18n('resflowreview_0010')}
            </label>
            <input onChange={this._onChange.bind(this, 'phoneNumber')}
                   id="phoneNumber" type="tel"
                   ref="phoneNumber"
                   value={personal.phoneNumber}
                   pattern="[0-9]*"/>
          </div>
          <div className="email-container">
            <label htmlFor="emailAddress">{i18n('reservationwidget_0030')}
            </label>
            <input onChange={this._onChange.bind(this, 'email')} id="emailAddress" type="email"
                   value={personal.email}
                   ref="email"/>
          </div>

          <div>
            <dt>{i18n('resflowreview_0063')}</dt>
            <dd>{_.get(profile, 'address_profile.street_addresses[0]')}</dd>
          </div>
        </dl>

        <div className="section-header">{i18n('resflowreview_0026')}</div>
        <dl>
          <div>
            <dt>{i18n('resflowreview_0071')}</dt>
            <dd>{ExpeditedController.getCountryInformation(profile)}</dd>
          </div>
          <div>
            <dt>{i18n('resflowreview_0072')}</dt>
            <dd>{profile.license_profile.license_number}</dd>
          </div>
          {profile.license_profile.license_expiry ? <div>
            <dt>{i18n('resflowreview_0037')}</dt>
            <dd>{profile.license_profile.license_expiry}</dd>
          </div> : false}
          {profile.license_profile.license_issue ? <div>
            <dt>{i18n('resflowreview_0720')}</dt>
            <dd>{profile.license_profile.license_issue}</dd>
          </div> : false}
          {profile.license_profile.birth_date ? <div>
            <dt>{i18n('eplusenrollment_0031')}</dt>
            <dd>{profile.license_profile.birth_date}</dd>
          </div> : false}
        </dl>
      </form>
    );
  }
}

ExistingProfile.displayName = "ExistingProfile";
