const TransitionGroup = React.addons.CSSTransitionGroup;
import RenterInformation from './RenterInformation';

export default function ExpeditedRenterInformation({ profile, userLoggedIn, account, errors, expedited }) {
  return (
    <TransitionGroup
      transitionName="ease"
      transitionEnterTimeout={0}
      transitionLeaveTimeout={0}
    >
      <RenterInformation
        {...{
          profile,
          userLoggedIn,
          account,
          errors,
          expedited
        }}
      />
    </TransitionGroup>
  )
}
