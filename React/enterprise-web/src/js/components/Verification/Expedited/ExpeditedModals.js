import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import ReservationCursors from '../../../cursors/ReservationCursors';
import Modal from '../../Modal/GlobalModal';
import Incomplete from './Incomplete';
import Login from './Login';
import DNR from './DNRWarn';
import CID from './MultipleCID';
import Terms from '../../Enroll/Terms';
import TermsConditions from '../../Login/Terms';
import DuplicateID from '../../Enroll/DuplicateID';

let ExpeditedModals = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    expedited: ReservationCursors.expedited,
    enroll: ReservationCursors.enroll,
    loginWidget: ReservationCursors.loginWidget,
    supportLinks: ReservationCursors.supportLinks,
    loginWidgetErrors: ReservationCursors.loginWidgetErrors,
    paymentReferenceID: ReservationCursors.paymentReferenceID,
    login: ReservationCursors.login,
    profile: ReservationCursors.profile
  },
  render () {
    if (!this.state.expedited.modal) {
      return false;
    }

    let modal = false;

    switch (this.state.expedited.modal) {
      case 'ep':
        modal = {
          content: <Login brand={this.state.expedited.modal}
                          supportLinks={this.state.supportLinks}
                          errors={this.state.loginWidgetErrors}
                          paymentReferenceID={this.state.paymentReferenceID} />,
          title: i18n('resflowreview_0080')
        };
        break;
      case 'ec':
        modal = {
          content: <Login brand={this.state.expedited.modal}
                          supportLinks={this.state.supportLinks}
                          errors={this.state.loginWidgetErrors}
                          paymentReferenceID={this.state.paymentReferenceID} />,
          title: i18n('resflowreview_0080')
        };
        break;
      case 'login':
        modal = {
          content: <Login supportLinks={this.state.supportLinks}
                          errors={this.state.loginWidgetErrors}
                          paymentReferenceID={this.state.paymentReferenceID} />,
          title: i18n('resflowreview_0080')
        };
        break;
      case 'dnr':
        modal = {
          content: <DNR />,
          title: i18n('expedited_0051'),
          hideX: true,
          disableBlur: true
        };
        break;
      case 'incomplete':
        modal = {
          content: <Incomplete errors={this.state.expedited.errors} />,
          title: i18n('expedited_0052'),
          role: 'alertdialog'
        };
        break;
      case 'eplus':
        modal = {
          content: <Terms loginWidget={this.state.loginWidget}/>,
          title: i18n('eplusenrollment_0042')
        };
        break;
      case 'eplusTC':
        modal = {
          content: <TermsConditions loginWidget={this.state.loginWidget} login={this.state.login} />,
          title: i18n('eplusenrollment_0042')
        };
        break;
      case 'cid':
        modal = {
          content: <CID profile={this.state.profile} expedited={this.state.expedited} />,
          title: i18n('resflowcorporate_0053'),
          hideX: true,
          disableBlur: true
        };
        break;
      case 'duplicateID':
        modal = {
          content: <DuplicateID expedited={this.state.expedited}
                                supportLinks={this.state.supportLinks}
                                enroll={this.state.enroll}/>,
          title: i18n('eplusenrollment_0042')
        };
        break;
      case 'duplicateAcc':
        modal = {
          content: <DuplicateID expedited={this.state.expedited}
                                supportLinks={this.state.supportLinks}
                                enroll={this.state.enroll}/>,
          title: i18n('eplusenrollment_0042')
        };
        break;
      default :
        modal = {
          content: <div className="transition"/>,
          title: i18n('resflowcorporate_4037')
        };
    }

    return (
      <div className="expedited">
        {this.state.expedited.modal ?
          <Modal active={true}
                 hideX={modal.hideX}
                 disableBlur={modal.disableBlur}
                 cursor={ReservationCursors.expedited.concat('modal')}
                 header={modal.title}
                 role={modal.role ? modal.role : ''}
                 content={modal.content}/> : false}
      </div>
    );
  }
});

module.exports = ExpeditedModals;
