import ExpeditedController from '../../../controllers/ExpeditedController';
import VerificationController from '../../../controllers/VerificationController';
import ReservationFlowModelController from '../../../controllers/ReservationFlowModelController';

function _onBack (expedited) {
  if (expedited.render === 'search') {
    ExpeditedController.expediteSearch();
  }
  closeModal();
}

function _onContinue () {
  ExpeditedController.clearProfile(true); // Need to clear expedited.license field here

  //Perhaps refactor, spinning up a different thread
  setTimeout(()=> {
    closeModal();
    ReservationFlowModelController.callReviewBtn();
  }, 0);
}

function closeModal () {
  VerificationController.callSetModal(false);
}

export default function PreExpediteWarning ({expedited}) {
  return (
    <div className="info pre-expedite-warning">
      <div className="header-container">
        <h3>{i18n('expedited_0058')}</h3>
      </div>
      <div className="message-container">
        <div className="disclaimer">
          {i18n('expedited_0059')}
        </div>
      </div>
      <div className="modal-action">
        <div onClick={() => _onBack(expedited)} className="btn">{i18n('expedited_0060')}</div>
        <div className="continue">
          <span>{i18n('expedited_0055a')}</span>
          <span onClick={() => _onContinue()}
            className="accented"> {i18n('expedited_0055b')}</span>
        </div>
      </div>
    </div>
  );
}

PreExpediteWarning.displayName = "PreExpediteWarning";
