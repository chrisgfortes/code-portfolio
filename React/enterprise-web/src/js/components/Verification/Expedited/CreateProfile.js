import Validator from '../../../utilities/util-validator';
import ExpeditedController from '../../../controllers/ExpeditedController';

export default class CreateProfile extends React.Component{
  constructor() {
    super();
    this.state = {
      terms: false
    };
    this.validator = new Validator(this, this.fieldMap);
    this.fieldMap = this.fieldMap.bind(this);
    this._termsModal = this._termsModal.bind(this);
    this._onTerms = this._onTerms.bind(this);
    this._onInputChange = this._onInputChange.bind(this);
  }
  fieldMap () {
    let password = this.password.value;
    let passwordConfirm = this.passwordConfirm.value;

    return {
      refs: {
        password: this.password,
        passwordConfirm: this.passwordConfirm
      },
      value: {
        password: password,
        passwordConfirm: passwordConfirm
      },
      schema: {
        password: 'password',
        passwordConfirm: () => {
          return passwordConfirm.length && password === passwordConfirm;
        }
      }
    };
  }
  _onInputChange (variable, event) {
    this.validator.validateAll();
    ExpeditedController.setInput(variable, event.target.value);
  }
  _termsModal () {
    ExpeditedController.setInput('modal', 'eplus');
  }
  _onTerms (variable, event) {
    this.setState({
      terms: event.target.checked
    });
    ExpeditedController.setInput(variable, event.target.checked);
  }
  render () {
    let contract = this.props.sessionContractDetails;
    if (!enterprise.inResEnrollment || (contract && contract.loyalty_sign_up_eligible === false)) {
      return false;
    }

    return (
      <div className="enroll">

        <div className="enroll-header-container">
          <div className="enroll-header">
            {i18n('reservationwidget_0040')}
            - {i18n('expedited_0029')}</div>
          <span>{i18n('resflowreview_0046')}</span>
        </div>
        <i className="icon icon-eplus-logo-black"/>

        <div className="enroll-disclaimer">
          <div>{i18n('expedited_0030')}</div>
        </div>

        <div className="field-container password">
          <label htmlFor="password">{i18n('expedited_0031')}</label>
          <input onChange={this._onInputChange.bind(this, 'password')}
                 type="password"
                 name="password"
                 ref={c => this.password = c}
                 id="createPassword"/>
        </div>

        <div className="field-container confirmPassword">
          <label htmlFor="confirmPassword">{i18n('resflowreview_0048')}</label>
          <input onChange={this._onInputChange.bind(this, 'passwordConfirm')}
                 type="password"
                 name="passwordConfirm"
                 ref={c => this.passwordConfirm = c}
                 id="passwordConfirm"/>
        </div>

        <div className="field-container terms">
          <label htmlFor="terms">
            <input
              id="terms"
              name="terms"
              type="checkbox"
              checked={this.state.terms}
              onChange={this._onTerms.bind(this, 'terms')}
            />
            <i className="icon icon-forms-checkmark-green" />
            {i18n('eplusenrollment_0037')}
          </label>
                        <span onClick={this._termsModal}
                              className="accented"> {i18n('eplusenrollment_0062')}</span>
        </div>

      </div>
    );
  }
}

CreateProfile.displayName = "CreateProfile";
