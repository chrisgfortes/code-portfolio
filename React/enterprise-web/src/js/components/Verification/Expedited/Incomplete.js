import ExpeditedController from '../../../controllers/ExpeditedController';
import ReservationFlowModelController from '../../../controllers/ReservationFlowModelController';
import Scroll from '../../../utilities/util-scroll';

export default class Incomplete extends React.Component{
  constructor() {
    super();
    this._onBack = this._onBack.bind(this);
    this._onContinue = this._onContinue.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }
  componentWillUnmount () {
    const expeditedHeading = ReservationFlowModelController.getQueryElement('#expedited h3 span');
    if (expeditedHeading) {
      ExpeditedController.setExpeditedHeadingFocus(expeditedHeading);
    }
    Scroll.scrollTo('#expedited');
  }
  _onBack () {
    this.closeModal();
    Scroll.scrollTo('#expedited');
  }
  _onContinue () {
    ExpeditedController.clearProfile();
    //Perhaps refactor, spinning up a different thread
    setTimeout(()=> {
      this.closeModal();
      document.getElementById('reviewSubmit').click();
    }, 0);
  }
  closeModal () {
    ExpeditedController.setInput('modal', false);
  }
  render () {
    let errors = this.props.errors;

    return (
      <div className="incomplete">
        <div className="header-container">
          <h3>{i18n('expedited_0052')}</h3>
        </div>
        <div className="message-container">
          <div className="disclaimer">
            {i18n('expedited_0053a')}
          </div>
          <div className="error-listing">

            {errors && errors.length > 0 ?
              <ul>
                {errors.map(function (item, index) {
                  return <li key={index}>{item}</li>;
                })}
              </ul> : false}
          </div>
        </div>
        <div className="modal-action">
          <button onClick={this._onBack} className="btn">{i18n('expedited_0054')}</button>

          <div className="continue">
            <span>{i18n('expedited_0055a')}</span>
            <span role="button" tabIndex="0"
                 onKeyPress={a11yClick(this._onContinue)} onClick={this._onContinue}
                 className="accented">
              {i18n('expedited_0055b')}
            </span>
          </div>
        </div>
      </div>
    );
  }
}

Incomplete.displayName = "Incomplete";
