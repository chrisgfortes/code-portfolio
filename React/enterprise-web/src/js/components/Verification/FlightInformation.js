import VerificationController from '../../controllers/VerificationController';
import Validator from '../../utilities/util-validator';
import {LOCATION_SEARCH} from '../../constants/';

export default class FlightInformation extends React.Component {
  constructor(props) {
    super(props);
    this.validator = new Validator(this, this.fieldMap);
  }
  fieldMap () {
    return {
      refs: {
        airlineCode: this.airlineCode
      },
      value: {
        airlineCode: this.airlineCode.value
      },
      schema: {
        airlineCode: 'string'
      }
    }
  }
  onChange (value, event) {
    let selection = event.target.value === 'false' ? null : event.target.value;
    VerificationController.setPersonalFields(value, selection);
    this.validator.validate('airlineCode', selection);
  }
  render () {
    const { sessionPickupLocation, personal } = this.props;
    const pickupAirlines = sessionPickupLocation ? sessionPickupLocation.airline_details : [];
    const airlines = pickupAirlines.length > 1 ? pickupAirlines : [];
    const pickupMultiTerminal = sessionPickupLocation ? sessionPickupLocation.multi_terminal : false;
    const multiTerminalChoiceMessage =
      personal.airlineCode === LOCATION_SEARCH.WALK_IN ? i18n('multipleterminals_0001') || "I don't know or have airline information" :
      personal.airlineCode === LOCATION_SEARCH.OTHER ? i18n('multipleterminals_0004') || 'My airline is not listed' : null;

    return (
      <div className="flight-information section-content">
        <h3 className="view-header">{i18n('resflowreview_0057').toLowerCase()}</h3>
        {!pickupMultiTerminal && <div className="flight-disclaimer">
          {i18n('expedited_0016')}
        </div>}
        {pickupMultiTerminal ?
          <div className="multi-terminal">
            <span className="icon icon-location-airport-green"></span>

            <div className="message-container">
              <div className="message-description">{i18n('resflowreview_0059')}</div>
            </div>
          </div> : false
        }

        {personal.airlineCode !== LOCATION_SEARCH.WALK_IN && personal.airlineCode !== 'OTHER' ?
          <div>
            <div className="field-container airline-name">
              <label htmlFor="airlineCode">{i18n('resflowreview_0060')}</label>
              {airlines.length > 0 ?
                <select className="styled"
                        ref={c => this.airlineCode = c}
                        onChange={this.onChange.bind(this, 'airlineCode')}
                        defaultValue={personal.airlineCode}
                        id="airlineCode">
                  <option value={false}>{i18n('resflowreview_0130')}</option>
                  {airlines.map(function (airline, index) {
                    return (<option key={index}
                                    value={airline.code}>{airline.description}</option>);
                  })}
                </select>
                :
                <input onChange={this.onChange.bind(this, 'airlineCode')} id="airlineCode" type="text"
                       placeholder={i18n('resflowreview_0060')}/>
              }
            </div>
            <div className="field-container flight-number">
              <label htmlFor="flightNumber">{i18n('resflowreview_0061')} ({i18n('reservationwidget_0040')})</label>
              <input onChange={this.onChange.bind(this, 'flightNumber')}
                      value={personal.flightNumber}
                      id="flightNumber" type="text"/>
            </div>
            <div className="no-flight-action">
              <div role="button" tabIndex="0"
                   onClick={VerificationController.onNoFlight}
                   onKeyPress={a11yClick(VerificationController.onNoFlight)}
                   className="text-btn">
                {i18n('multipleterminals_0001') || "I don't know or have airline information" }
              </div>
              <div role="button" tabIndex="0"
                   onClick={VerificationController.onNotListed}
                   onKeyPress={a11yClick(VerificationController.onNotListed)}
                   className="text-btn">
                {i18n('multipleterminals_0004') || 'My airline is not listed' }
              </div>
            </div>
          </div> :
          <div className="change-flight-action">
            {i18n('multipleterminals_0002') || 'You selected'} "
            {multiTerminalChoiceMessage}"
            <div role="button" tabIndex="0"
                 onClick={VerificationController.onAddFlight}
                 onKeyPress={a11yClick(VerificationController.onAddFlight)}
                 className="edit">{i18n('resflowreview_0084')}</div>
          </div>}
      </div>
    );
  }
}

FlightInformation.displayName = 'FlightInformation';
