import ExtrasLineItem from '../PricingTable/ExtrasLineItem';

export default function InformationBlock ({summary, vehicleRate, mileageInfo, selectedCar}) {
  return (
    <div className="information-block vehicle">
      <img alt={summary.vehicleMake} src={summary.image}/>
      <h3 className="category-label">{i18n('reservationnav_0009')}</h3>
      <div className="info-block-details">
        {(vehicleRate.length > 0) &&
        vehicleRate.map((item, index) => {
          return (<ExtrasLineItem
            hideRate={true}
            item={item}
            mileageInfo={mileageInfo}
            vehicle={selectedCar.name}
            id="VehicleInformation-row-header-1"
            headers="section-header-VehicleInformation VehicleInformation-row-header-1"
            key={index}/>);
        })}
        {!!mileageInfo &&
        <div id="VehicleInformation-row-header-mileage" className="row">
          <span
            className="line-item">{mileageInfo.unlimited_mileage ? i18n('resflowcarselect_0075') : i18n('resflowreview_0150')}</span>
          <div className="amount">
            {mileageInfo.unlimited_mileage ? i18n('reservationnav_0018') : mileageInfo.total_free_miles + ' ' + mileageInfo.distance_unit}
          </div>
          {!mileageInfo.unlimited_mileage &&
          <span className="line-rate">
            {i18n('resflowreview_8003') + ' ' + mileageInfo.total_free_miles + ' ' + _.get(mileageInfo, 'excess_mileage_rate_view.format', '') + '/' + mileageInfo.distance_unit}
          </span>
          }
        </div>
        }
      </div>
    </div>
  );
}

InformationBlock.displayName = InformationBlock;
