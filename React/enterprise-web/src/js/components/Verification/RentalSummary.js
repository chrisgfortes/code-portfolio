import VerificationController from '../../controllers/VerificationController';
import GlobalModal from '../Modal/GlobalModal';
import ExtrasLineItem from '../PricingTable/ExtrasLineItem';
import classNames from 'classnames';
import Edit from './Edit';
import {PRICING} from '../../constants/';

export default class RentalSummary extends React.Component {
  constructor ( props ) {
    super( props );
    this.state = {
      collapsed: true
    };
  }
  collapse () {
    this.setState({collapsed: !this.state.collapsed});
  }
  render () {
    const { sessionContractDetails, selectedCar, prepay, modify, codeApplicable, ageLabel, profile,
            blockModifyPickupLocation, blockModifyDropoffLocation, lockedCIDModal,
            sessionPickupTime, sessionDropoffTime, sessionPickupLocation, sessionDropoffLocation} = this.props;

    const { collapsed } = this.state;

    const couponInfoHeading  = VerificationController.getCouponInfoHeading( sessionContractDetails, codeApplicable );
    const paymentMethod = prepay ? PRICING.PREPAY : PRICING.PAYLATER;
    const pricing = selectedCar.vehicleRates[paymentMethod] && selectedCar.vehicleRates[paymentMethod].price_summary;
    const extras = pricing && pricing.extra_line_items ? pricing.extra_line_items : [];
    const summary = VerificationController.getCarSummary( selectedCar, ageLabel, sessionPickupTime, sessionDropoffTime,
      sessionPickupLocation, sessionDropoffLocation, sessionContractDetails );

    let collapseClasses = classNames({
      'rental-details': true,
      'collapsed': collapsed
    });

    return (
      <div className="rental-summary section-content">
        <h2 className="view-header" onClick={this.collapse.bind(this)}>{i18n('resflowreview_0302')}
          <span role="button" tabIndex="0" onClick={this.collapse.bind(this)}
                              onKeyPress={a11yClick(this.collapse.bind(this))} className="edit">
                            {i18n('expedited_0023')}</span>
          <span className="icon icon-nav-carrot-down"></span>
        </h2>

        <div className={collapseClasses}>
          <div className="information-block resume">
            <h3 className="category-label">
              {summary.vehicleType}
            </h3>
            <span><br/></span>
            <h3 className="category-label">
              {summary.location.pickup || _.get(sessionPickupLocation, '.address.street_addresses[0]') || ''}
            </h3>
            <span><br/></span>

            <div className="info-block-details">
              {summary.time.pickup ? summary.time.pickup.format('ddd, MMM DD, YYYY') : ''}
              <span> @ </span>
              {summary.time.pickup ? summary.time.pickup.format('LT') : ''}
            </div>
            <div className="info-block-details">
              {summary.time.dropoff ? summary.time.dropoff.format('ddd, MMM DD, YYYY') : ''}
              <span> @ </span>
              {summary.time.dropoff ? summary.time.dropoff.format('LT') : ''}
            </div>
            <span><br/></span>
            <div className="info-block-details">
              {extras.length ?
                // Uses map to get all the names of the extras and then join them with a , as separator
                _.join(_.map(extras, 'name'), ', ') : ''
              }
            </div>
            {summary.code &&
              <div className="info-block-details">{summary.code} {couponInfoHeading.codeApplicableMsg}</div>
            }
          </div>

          <div className="information-block vehicle">
            <h3 className="category-label">
              {i18n('reservationnav_0009')}
            </h3>
            <Edit destination="#cars" editHeading={i18n('resflowreview_0129') || 'Modify Vehicle'} editLabel={i18n('resflowreview_0084')}/>

            <div className="info-block-details">
              {summary.vehicleType}
            </div>
          </div>
          <div className="information-block pickup">
            <h3 className="category-label">
              {(summary.location.pickup !== summary.location.dropoff) ? i18n('reservationwidget_0008') : i18n('resflowreview_0083')}
            </h3>
            {blockModifyPickupLocation ?
              <span className="edit" role="button" tabIndex="0"
                    onKeyPress={a11yClick(VerificationController.setCannotModifyModal.bind(null, true))}
                    onClick={VerificationController.setCannotModifyModal.bind(null, true)}>?</span> :
              <Edit destination="#location/pickup" editHeading={i18n('resflowreview_0136') || 'Modify Location'} editLabel={i18n('resflowreview_0084')}
                  locationType="pickup"/>}

            <div className="info-block-details">
              {summary.location.pickup || _.get(sessionPickupLocation, 'address.street_addresses[0]') || ''}
            </div>
          </div>
          {
            (summary.location.pickup !== summary.location.dropoff) ?
              <div className="information-block dropoff">
                <h3 className="category-label">{i18n('reservationwidget_0011')}</h3>

                {blockModifyDropoffLocation ?
                  <span className="edit" role="button" tabIndex="0"
                        onKeyPress={a11yClick(VerificationController.setCannotModifyModal.bind(null, true))}
                        onClick={VerificationController.setCannotModifyModal.bind(null, true)}>?</span> :
                  <Edit destination="#location/dropoff" editHeading={i18n('resflowreview_0136') || 'Modify Location'} editLabel={i18n('resflowreview_0084')}
                      locationType="dropoff"/>
                }

                <div className="info-block-details">
                  {summary.location.dropoff || _.get(sessionDropoffLocation, 'address.street_addresses[0]') || ''}
                </div>
              </div>
              : null
          }
          <div className="information-block time">
            <h3 className="category-label">{i18n('resflowreview_0085')}</h3>
            <Edit destination="#dateTime" editHeading={i18n('resflowreview_0138') || 'Modify Dates and Times'} editLabel={i18n('resflowreview_0084')}/>

            <div className="pickup-time row">
              <span className="line-item">{i18n('reservationwidget_0008').toLowerCase()}:</span>
              <span className="amount">
                {summary.time.pickup ? summary.time.pickup.format('ddd, MMM DD, YYYY') : ''}
                <span> @ </span>
                {summary.time.pickup ? summary.time.pickup.format('LT') : ''}
              </span>
            </div>
            <div className="dropoff-time row">
              <span className="line-item">{i18n('reservationwidget_0011').toLowerCase()}:</span>
              <span className="amount">
                {summary.time.dropoff ? summary.time.dropoff.format('ddd, MMM DD, YYYY') : ''}
                <span> @ </span>
                {summary.time.dropoff ? summary.time.dropoff.format('LT') : ''}
              </span>
            </div>
          </div>

          <div className="information-block extras">
            <h3 className="category-label">{i18n('reservationnav_0011')}</h3>
            <Edit destination="#extras" editHeading={i18n('resflowreview_0133') || 'Modify Extras'} editLabel={i18n('resflowreview_0084')}/>
            <div className="info-block-details">
              {extras.length ? extras.map((item, index) => {
                return (<ExtrasLineItem
                  label={this.determineRateLabel}
                  item={item}
                  hideRate={true}
                  hideCost={true}
                  id={'extras-row-header-' + (index + 1)}
                  key={index}/>);
              }) : false}
            </div>

          </div>

          {!profile && <div className="information-block age">
            <h3 className="category-label">{i18n('reservationwidget_0012')}</h3>
            {modify ? <span className="edit" onClick={VerificationController.callCannotModify}>?</span> :
              <Edit destination="#book" editHeading={i18n('resflowreview_0134') || 'Modify Renter Age'} editLabel={i18n('resflowreview_0084')}/>}

            <div className="info-block-details">{summary.age}</div>
          </div>}
          <div className="information-block coupon">
            <h3 className="category-label">{couponInfoHeading.couponInfoHeading}</h3>
            {modify || (summary.contract && !summary.contract.allow_account_removal_indicator) ?
              <span onClick={VerificationController.setLockedCIDModal.bind(null, true)}
              aria-label={i18n('reservationwidget_5001')} className="edit icon icon-icon-info-green"></span> :
              <Edit destination="#book" editHeading={i18n('resflowreview_0135') || 'Modify Promotion Code or Account Number'} editLabel={i18n('resflowreview_0084')}/>}
            {summary.code ? <div className="info-block-details">{summary.code} {couponInfoHeading.codeApplicableMsg}</div> : false}
          </div>
        </div>
        {lockedCIDModal.modal &&
        <GlobalModal active={lockedCIDModal.modal}
                     header={i18n('resflowcorporate_4015')}
                     content={i18n('resflowcorporate_0072')}
                     cursor={['view', 'lockedCIDModal', 'modal']} />
        }
      </div>
    );
  }
}

RentalSummary.displayName = 'RentalSummary';
