import VerificationController from '../../controllers/VerificationController';

function onClose () {
  VerificationController.callSetModal(false);
}

export default function PrepayModal () {
  return (
    <div className="prepay-error-modal">
      {enterprise.i18nReservation.prepay_1011}
      <div onClick={onClose}
           onKeyPress={a11yClick(onClose)}
           role="button" tabIndex="0"
           className="btn submit">{enterprise.i18nReservation.resflowcarselect_0078}</div>
    </div>
  );
}

PrepayModal.displayName = 'PrepayModal';
