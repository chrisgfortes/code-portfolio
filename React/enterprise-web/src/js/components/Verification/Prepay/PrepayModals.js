import ReservationCursors from '../../../cursors/ReservationCursors';
import PrepayPaymentModal from './PrepayPaymentModal';
import ThreeDSModal from './ThreeDSModal';
import AddPayments from '../../Account/AddPayments';
import DebitCard from '../../Account/DebitCard';
import EditCardOrBilling from '../../Account/EditCardOrBilling';
import RemovePayment from '../../Account/RemovePayment';
import GlobalModal from '../../Modal/GlobalModal';
import VerificationController from '../../../controllers/VerificationController';
import PaymentModelController from '../../../controllers/PaymentModelController';
import Analytics from '../../../modules/Analytics';

let modal = false;

export default function PrepayModals ({showModal, account, profile, paymentMethods, threeDS, profilePaymentMethods, sessionBrand, modifyPayment}) {
  if (showModal) {
    switch (showModal) {
      case 'removeNA':
        modal = {
          content: <RemovePayment close={() => VerificationController.setPrepayModal(false)}
                                account={account} />,
          title: i18n('eplusaccount_2026') || 'DELETE CREDIT CARD?'
        };
        break;
      case 'editNA':
        modal = {
          content: <EditCardOrBilling
                    close={()=>VerificationController.setPrepayModal(false)}
                    triggerDelete={() => VerificationController.setPrepayModal('removeNA')}
                    modifyPayment={modifyPayment}
                    paymentMethods={paymentMethods} />,
          title: i18n('eplusaccount_0117')
        };
        break;
      case 'add':
        modal = {
          content: <PrepayPaymentModal />,
          title: i18n('prepay_1001')
        };
        break;
      // NA Prepay
      case 'addNA':
        modal = {
          content: <AddPayments saveForLater={PaymentModelController.canSavePaymentMethods(profilePaymentMethods, sessionBrand)}
                        profile={profile}
                        paymentMethods={paymentMethods}
                        close={() => {
                          VerificationController.setPrepayModal(null);
                          Analytics.trigger('html', 'analytics-naprepay-modal-close');
                        }} />,
          onOpening: () => Analytics.trigger('html', 'analytics-prepay-modal-open'),
          title: i18n('prepay_1001')
        };
        break;
      case 'threeDS':
        modal = {
          content: <ThreeDSModal threeDS={threeDS} />,
          title: i18n('prepay_1002')
        };
        break;
      case 'debitCard':
        modal = {
          content: <DebitCard close={() => VerificationController.setPrepayModal(false)} />,
          title: i18n('prepay_0053') || 'Debit cards are not allowed '
        };
        break;
      default :
        modal = {
          content: <div className="transition"/>,
          title: i18n('resflowcorporate_4037')
        };
    }
  }
  return (
    <div>
      {showModal ?
      <GlobalModal active={showModal}
           cursor={ReservationCursors.verification.concat('cardModal')}
           header={modal.title}
           content={modal.content}
           onOpening={modal.onOpening} /> : false}
    </div>
  )
}

PrepayModals.displayName = "PrepayModals";
