import VerificationController from '../../../controllers/VerificationController';
import GlobalModal from '../../Modal/GlobalModal';

export default class PrepayChecklistAccordian extends React.Component{
  constructor() {
    super();
    this.state = {
      realHeight: 0,
      isMounted: false
    };
    this._togglePrepayItem = this._togglePrepayItem.bind(this);
    this._showPolicy = this._showPolicy.bind(this);
    this._onClickPolicy = this._onClickPolicy.bind(this);
  }
  componentDidMount () {
    setTimeout(() => {
      let listEl = this.list;

      if (listEl) {
        this.setState({
          realHeight: listEl.getBoundingClientRect().height,
          isMounted: true
        });
      }
    }, 0);
  }
  _togglePrepayItem () {
    this.props.toggleVisibility();
  }
  _showPolicy (event) {
    event.preventDefault();
    VerificationController.setBasicPrepayModal('prepay');
  }
  _onClickPolicy (event) {
    event.preventDefault();
    VerificationController.setOverallPolicy(true);
  }
  render () {
    const {reservationPolicies, list, isVisible, showRequirements, showPolicy, header,
      verification, term} = this.props
    const policies = reservationPolicies || [];

    const policyModalContent = policies.map(policy =>
        <div className="policy-content">
          <h2> {policy.description} </h2>
          <p> {policy.policy_text} </p>
        </div>
    );

    const itemsList = list && list.map(obj => <li>{obj.listitem}</li> );

    const className = 'checklist-section' +
      (isVisible ? ' shown' : '');
    const listClassName = 'prepay-unordered-list' +
      (this.state.isMounted ? ' mounted' : '');
    let height = '0';

    if (this.state.isMounted) {
      height = (this.props.isVisible ? '999px' : 0);
    }

    let requirements = false;
    if (showRequirements) {
      requirements = (
        <li>
          {(i18n('prepaychecklist_0002') || 'Renter meets') + ' '}
          <a href="#" role="button" tabIndex="0"
             onKeyPress={a11yClick(this._onClickPolicy)}
             onClick={this._onClickPolicy}>
            {i18n('prepaychecklist_0003') || 'license, documents, payment and age requirements'}
          </a>
          {' ' + (i18n('prepaychecklist_0004') || 'to rent a vehicle.')}
        </li>
      );
    }

    let policy = false;
    if (showPolicy) {
      policy = (
        <li>
          {i18n('resflowreview_8007') + ' '}
          <a href="#" role="button" tabIndex="0" onKeyPress={a11yClick(this._showPolicy)} onClick={this._showPolicy}>
            {i18n('resflowreview_8005')}
          </a>
          {' ' + i18n('resflowreview_8006')}
        </li>
      );
    }

    if (list && header) {
      return (
        <div className={className} ref={term}>

          <i className="icon icon-nav-carrot-down checklist-expand" role="button" tabIndex="0"
             onKeyPress={a11yClick(this._togglePrepayItem.bind(this, term))}
             onClick={this._togglePrepayItem.bind(this, term)}/>

          <h3 role="button" tabIndex="0" onClick={this._togglePrepayItem.bind(this, term)}
              onKeyPress={a11yClick(this._togglePrepayItem.bind(this, term))}>
            <i className="icon icon-forms-checkmark-green"/>
            {header}
          </h3>

          <ul className={listClassName} ref={c => this.list = c} style={{ maxHeight: height }}>
            {requirements}
            {itemsList}
            {policy}
            {verification.overallPolicy &&
            <GlobalModal
              active={verification.overallPolicy}
              header={i18n('reservationnav_0016')}
              content={policyModalContent}
              classLabel="policy-modal"
              cursor={['view', 'verification', 'overallPolicy']}/>
            }
          </ul>
        </div>
      );
    }

    return false;
  }
}

PrepayChecklistAccordian.displayName = "PrepayChecklistAccordian";
