import ThreeDSForm from './ThreeDSForm';

export default function ThreeDSModal ({threeDS}) {
  return (
    <div>
      {
        threeDS.url ?
          <ThreeDSForm threeDS={threeDS} /> :
          <div className="transition"/>
      }
    </div>
  );
}

ThreeDSModal.displayName = "ThreeDSModal";
