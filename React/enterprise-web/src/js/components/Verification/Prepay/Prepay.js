import ReservationCursors from '../../../cursors/ReservationCursors';
import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import PrepayPolicies from './PrepayPolicies';
import AddOrChange from './AddOrChange';
import PaymentModelController from '../../../controllers/PaymentModelController';
import VerificationController from '../../../controllers/VerificationController';
import PrepayModals from './PrepayModals';

const Prepay = React.createClass({
  mixins: [BaobabReactMixinBranch, React.addons.PureRenderMixin],
  componentWillMount () {
    if (this.state.prepayCardRegistrationSuccessful && !this.state.verification.registerCardSuccess) {
      PaymentModelController.setRegisterCardSuccess(true);
    }
  },
  componentWillUnmount () {
    let cardSuccess = PaymentModelController.selectRegisterCardSuccess();
    cardSuccess.off('update');
    VerificationController.setVerificationErrors(false);
  },
  cursors: {
    verification: ReservationCursors.verification,
    sessionBrand: ReservationCursors.sessionBrand,
    prepayCardRegistrationSuccessful: ReservationCursors.prepayCardRegistrationSuccessful,
    collectNewModifyPaymentCard: ReservationCursors.collectNewModifyPaymentCard,
    profilePaymentMethods: ReservationCursors.profilePaymentMethods,
    paymentMethods: ReservationCursors.paymentMethods,
    profile: ReservationCursors.profile,
    account: ReservationCursors.account,
    threeDS: ReservationCursors.threeDS,
    isNAPrepayEnabled: ReservationCursors.isNAPrepayEnabled,
    paymentProcessor: ReservationCursors.paymentProcessor,
    showPaymentList: ReservationCursors.showPaymentList,
    paymentReferenceID: ReservationCursors.paymentReferenceID,
    userLoggedIn: ReservationCursors.userLoggedIn,
    expedited: ReservationCursors.expedited,
    reservationPolicies: ReservationCursors.reservationPolicies,
    modifyPayment: ReservationCursors.modifyPayment
  },
  render () {
    const {verification, profile, account, paymentMethods, profilePaymentMethods, modifyPayment,
      collectNewModifyPaymentCard, prepayCardRegistrationSuccessful, threeDS, isNAPrepayEnabled,
      paymentReferenceID, paymentProcessor, showPaymentList, userLoggedIn, expedited, reservationPolicies, sessionBrand} = this.state;
    let showModal = verification.cardModal;

    const isPaymentMethodSelected = this.props.modify && !collectNewModifyPaymentCard;
    return (
      <div>
        {!isPaymentMethodSelected &&
            <div id="prepay-container" className="prepay section-content">
              <div className="view-header">{i18n('resflowreview_0033').toLowerCase()}</div>

              <AddOrChange {...{profile,
                prepayCardRegistrationSuccessful,
                verification,
                isNAPrepayEnabled,
                paymentProcessor,
                profilePaymentMethods,
                showPaymentList,
                paymentReferenceID,
                userLoggedIn,
                expedited,
                sessionBrand}} />

              <PrepayPolicies reservationPolicies={reservationPolicies}
                              verification={verification} />

              <PrepayModals showModal={showModal}
                            account={account}
                            profile={profile}
                            paymentMethods={paymentMethods}
                            threeDS={threeDS}
                            profilePaymentMethods={profilePaymentMethods}
                            sessionBrand={sessionBrand}
                            modifyPayment={modifyPayment} />
            </div>
          }
      </div>
    );
  }
});

module.exports = Prepay;
