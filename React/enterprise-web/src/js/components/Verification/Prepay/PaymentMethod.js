import PaymentModelController from '../../../controllers/PaymentModelController';
import ExpeditedController from '../../../controllers/ExpeditedController';
import VerificationController from '../../../controllers/VerificationController';
import ProfileController from '../../../controllers/ProfileController';

function editPayment (userLoggedIn, expedited, payment) {
  if (!userLoggedIn && expedited) {
    ExpeditedController.setInput('modal', 'ep');
  } else {
    VerificationController.setPrepayModal('editNA');
    ProfileController.setModifyPayment(payment);
  }
}

export default function PaymentMethod ({selected, selectPayment, payment, userLoggedIn, expedited}) {
  const expiredDate =  moment(payment.expiration_date, "YYYY-DD");
  const isExpired = expiredDate.isBefore(moment());

  return (
    <div className="payment-method cf">
      <div className="select-payment">
        <label className="enterprise-control control-radio">
          <input
            type="radio"
            className="enterprise-radio"
            onChange={() => selectPayment(payment.payment_reference_id)}
            value={payment.payment_reference_id}
            checked={payment.payment_reference_id === selected}
          />
          <div className="control-indicator"/>
        </label>
      </div>
      <div className="payment-info">
        <ul>
          <li>{PaymentModelController.getCardLabel(payment)}</li>
          <li>{PaymentModelController.maskCC(payment.last_four)}</li>
          {isExpired ?
            <li><i className="icon icon-alert-caution-yellow"/>{i18n('eplusaccount_2004') || 'Expired'}: {payment.expiration_date}</li> :
            <li>{i18n('eplusaccount_2003') || 'Expires'}: {payment.expiration_date}</li>
          }
        </ul>
      </div>
      <div className="edit-payment">
        <a onClick={() => editPayment(userLoggedIn, expedited, payment)}>{i18n('expedited_0009')}</a>
      </div>
    </div>
  )
}

PaymentMethod.displayName = "PaymentMethod";
