import PaymentModelController from '../../../controllers/PaymentModelController';
import ReservationCursors from '../../../cursors/ReservationCursors';
import CardModifyConfirmationModal from './CardModifyConfirmationModal';
import Modal from '../../Modal/GlobalModal';
import { PRICING } from '../../../constants';

export default class CardRegisteredSuccess extends React.Component{
  constructor(props) {
    super(props);
    if (this.props.paymentProcessor !== PRICING.PAYMENT_PROCESSOR_NA) {
      PaymentModelController.registerCardStatus(true);
    }
    this._onClick = this._onClick.bind(this);
  }
  _onClick () {
    PaymentModelController.setCardModifyConfirmationModal(true);
  }
  render() {
    const {verification} = this.props;
    let showModal = verification.cardModifyConfirmationModal;
    let modal = {};
    let cardType;
    let cardCC;
    let cardExpires;

    if (showModal) {
      modal.title = i18n('prepay_1008');
      modal.content = <CardModifyConfirmationModal add={this.props.add}/>;
    }
    if (verification.authReviewCardDetails) {
      let cardDetails = PaymentModelController.getReviewCardDetails(verification.authReviewCardDetails);
      cardType = cardDetails.cardType;
      cardCC = cardDetails.cardCC;
      cardExpires = cardDetails.cardExpires;
    }
    
    return (
      <div>
        <div className="change-payment">
          {verification.authReviewCardDetails ?
            <div className="preferred-payment cf">
              <p>
                <span className="check-mark">{'\u2713'}</span>
                {i18n('resflowreview_0155')}
              </p>
              <div className="preferred-payment-box">
                <div className="preferred-payment-info">
                  <ul>
                    <li><strong>{cardType}</strong> {cardCC}</li>
                    <li>{i18n('eplusaccount_2003') || 'Expires'}: {cardExpires}</li>
                  </ul>
                </div>
                <div className="change-payment-type">
                  <a onKeyPress={this._onClick} onClick={this._onClick}>{i18n('resflowcorporate_4032')}</a>
                </div>
              </div>
            </div> : 
            <div>
              <span className="check-mark">{'\u2713'}</span>
              {i18n('resflowreview_0155')}
              <span className="accented change"
                    onClick={this._onClick}>{i18n('resflowcorporate_4032')}</span>
            </div>
          }
        </div>
        {showModal &&
        <Modal active={showModal}
               cursor={ReservationCursors.verification.concat('cardModifyConfirmationModal')}
               header={modal.title}
               content={modal.content}/>
        }
      </div>
    )
  }
}

CardRegisteredSuccess.displayName = "CardRegisteredSuccess";
