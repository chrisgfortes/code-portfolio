import PaymentModelController from '../../../controllers/PaymentModelController';

export default class PrepayPaymentModal extends React.Component{
  constructor() {
    super();
    this.state = {
      loading: false,
      redirectUrl: null
    }
  }
  componentDidMount () {
    this.loadPrepayForm();
  }
  loadPrepayForm () {
    this.setState({loading: true});
    PaymentModelController.clearPrepayError();
    PaymentModelController.initPrepay().then((response)=> {
      this.setState({
        redirectUrl: PaymentModelController.getRedirectUrl(response),
        loading: false
      });
    });
  }
  render () {
    return (
      <div>
        {
          this.state.loading ?
            <div className="transition"/> :
            <iframe scrolling="no" id="fare" src={this.state.redirectUrl}/>
        }
      </div>
    );
  }
}

PrepayPaymentModal.displayName = "PrepayPaymentModal";
