import {setCardModifyConfirmationModal} from '../../../controllers/PaymentModelController';

export default class CardModifyConfirmationModal extends React.Component{
  constructor() {
    super();
    this._confirm = this._confirm.bind(this);
  }
  _confirm () {
    setCardModifyConfirmationModal(false);
    this.props.add();
  }
  render() {
    return (
      <div className="modify-card-confirmation">
        <p>{i18n('prepay_1003')}</p>

        <div className="btn-grp cf">
          <button onClick={() => setCardModifyConfirmationModal(false)}
                  className="btn cancel">{i18n('prepay_1006')}</button>
          <button onClick={this._confirm}
                  className="btn ok">{i18n('prepay_0007')}</button>
        </div>
      </div>
    )
  }
}

CardModifyConfirmationModal.displayName = "CardModifyConfirmationModal";
