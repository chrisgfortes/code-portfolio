import PrepayChecklistAccordian from './PrepayChecklistAccordian';

export default class PrepayPolicies extends React.Component{
  constructor() {
    super();
    this.state = {
      visibility: {
        prepayEligibility: false,
        payNowPolicy: false,
        refund: false
      }
    }
  }
  _toggleVisibility (target) {
    return (newVisibility) => {
      let visibility = Object.assign({}, this.state.visibility);

      if (typeof newVisibility !== 'undefined') {
        visibility[target] = newVisibility;
      } else {
        visibility[target] = !visibility[target];
      }

      Object.keys(visibility).forEach(key => {
        if (key !== target) {
          visibility[key] = false;
        }
      });

      this.setState({visibility: visibility});
    };
  }
  render () {
    const {reservationPolicies, verification} = this.props;
    let prepayEligibilityList = enterprise.reservation.prepayEligibilityList &&
      enterprise.reservation.prepayEligibilityList.map(obj => <li>{obj.listitem}</li>);

    let payNowPolicyList = enterprise.reservation.payNowPolicyList &&
      enterprise.reservation.payNowPolicyList.map(obj => <li>{obj.listitem}</li>);

    let refundList = enterprise.reservation.refundList &&
      enterprise.reservation.refundList.map(obj => <li>{obj.listitem}</li>);

    let checklistActive = prepayEligibilityList.length || payNowPolicyList.length || refundList.length;

    if (checklistActive) {
      return (
        <div className={'prepay-checklist'}>
          <h2>{i18n('prepaychecklist_0001')}</h2>
          <PrepayChecklistAccordian
            reservationPolicies={reservationPolicies}
            verification={verification}
            isVisible={this.state.visibility.prepayEligibility}
            toggleVisibility={this._toggleVisibility('prepayEligibility')}
            header={enterprise.reservation.prepayEligibilityHeader}
            list={enterprise.reservation.prepayEligibilityList}
            term="prepayList"
            showRequirements={true}
            showPolicy={true}/>
          <PrepayChecklistAccordian
            reservationPolicies={reservationPolicies}
            verification={verification}
            isVisible={this.state.visibility.payNowPolicy}
            toggleVisibility={this._toggleVisibility('payNowPolicy')}
            header={enterprise.reservation.payNowPolicyHeader}
            list={enterprise.reservation.payNowPolicyList}
            term="payNowList"/>
          <PrepayChecklistAccordian
            reservationPolicies={reservationPolicies}
            verification={verification}
            isVisible={this.state.visibility.refund}
            toggleVisibility={this._toggleVisibility('refund')}
            header={enterprise.reservation.refundHeader}
            list={enterprise.reservation.refundList}
            term="refundList"/>
        </div>
      );
    }

    return false;
  }
}

PrepayPolicies.displayName = "PrepayPolicies";
