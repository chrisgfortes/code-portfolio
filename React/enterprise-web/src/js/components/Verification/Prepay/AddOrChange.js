import PaymentModelController from '../../../controllers/PaymentModelController';
import VerificationController from '../../../controllers/VerificationController';
import SelectPayment from './SelectPayment';
import CardRegisteredSuccess from './CardRegisteredSuccess';
import { PRICING } from '../../../constants';

export default class AddOrChange extends React.Component{
  constructor() {
    super();
    this._onAdd = this._onAdd.bind(this);
    // this.paymentMethodsAvailable = this.paymentMethodsAvailable.bind(this);
    this.addPaymentMarkup = this.addPaymentMarkup.bind(this);
  }
  componentDidMount () {
    PaymentModelController.ThreeDSListener();
  }
  _onAdd () {
    // this is a weak test that is based on countryCode
    // should only rely on this if we are already INSIDE
    // a Prepay Flow
    if (this.props.paymentProcessor === PRICING.PAYMENT_PROCESSOR_NA) {
      PaymentModelController.setPaymentReferenceID(null);
      VerificationController.setPrepayModal('addNA');
    } else {
      VerificationController.setPrepayModal('add');
    }
  }
  addPaymentMarkup () {
    return (
      <div className="cf">
        <div className="prepay-label">{i18n('prepay_1011')}
        </div>
        <button tabIndex="0" className="btn change save" onKeyPress={this._onAdd} onClick={this._onAdd}>
          {i18n('prepay_1012')}
        </button>
        {!this.props.isNAPrepayEnabled && !enterprise.hide3DSLogo && //hiding the card icons for NA
        <div className="credit-card-icons">
          <i className="icon icon-icon-verifiedvisa" aria-label={i18n('resflowreview_0131') || 'Visa'}/>
          <i className="icon icon-icon-mastersecure" aria-label={i18n('resflowreview_0132') || 'MasterCard'}/>
        </div>
        }
        <div className="secure-disclaimer">
          <i className="icon icon-included-theftprotection-2"/>
          <span className="disclaimer-content">
            {i18n('expedited_0015a')}
            <strong> {i18n('expedited_0015b')} </strong>
            {i18n('expedited_0015c')}
          </span>
        </div>
      </div>
    );
  }
  render () {
    const {prepayCardRegistrationSuccessful, verification, paymentProcessor, profilePaymentMethods,
      showPaymentList, paymentReferenceID, userLoggedIn, expedited, isNAPrepayEnabled,
      profile, sessionBrand} = this.props;
    let ret = '';

    if (verification.addCardSuccess || prepayCardRegistrationSuccessful) {
      ret = (<CardRegisteredSuccess add={this._onAdd}
                                   verification={verification}
                                   paymentProcessor={paymentProcessor} />);
    } else if(PaymentModelController.paymentMethodsAvailable(sessionBrand, isNAPrepayEnabled, profile)) {
      ret = (<SelectPayment add={this._onAdd}
                           profilePaymentMethods={profilePaymentMethods}
                           showPaymentList={showPaymentList}
                           paymentReferenceID={paymentReferenceID}
                           userLoggedIn={userLoggedIn}
                           expedited={expedited} />);
    } else {
      ret = this.addPaymentMarkup();
    }

    return ret;
  }
}

AddOrChange.displayName = "AddOrChange";
