import PaymentMethod from './PaymentMethod';
import PaymentModelController from '../../../controllers/PaymentModelController';

export default class SelectPayment extends React.Component{
  constructor() {
    super();
    this.getPreferred = this.getPreferred.bind(this);
    this.selectPayment = this.selectPayment.bind(this);
    this.showPaymentList = this.showPaymentList.bind(this);
    this.showPreferred = this.showPreferred.bind(this);
  }
  componentDidMount() {
    if(!this.props.paymentReferenceID) {
      const preferred = this.getPreferred(this.props.profilePaymentMethods);
      this.selectPayment(preferred.payment_reference_id);
    }
  }
  getPreferred(paymentMethods) {
    return paymentMethods.find(payment => payment.preferred);
  }
  selectPayment(paymentId) {
    PaymentModelController.setPaymentReferenceID(paymentId);
  }
  showPaymentList(paymentMethods) {
    const {hasReachedCreditCardsLimit, filterCreditCards} = PaymentModelController;

    return(
        <div>
          <div className="payment-methods cf">
            <p>{i18n('prepay_0025') || 'Select Your Payment Method'}</p>
            {paymentMethods.map((payment) => (
                <PaymentMethod
                  selected={this.props.paymentReferenceID}
                  selectPayment={this.selectPayment}
                  payment={payment}
                  userLoggedIn={this.props.userLoggedIn}
                  expedited={this.props.expedited}
                />
              ))}
          </div>
          {hasReachedCreditCardsLimit(filterCreditCards(paymentMethods)) &&
            <div>{i18n('eplusaccount_2005') || 'MAXIMUM CREDIT CARDS ON FILE REACHED'}</div>}
          <div className="add-credit-card">
            <a onKeyPress={this.props.add} onClick={this.props.add}>
              <i className="icon icon-add"/>
              {i18n('eplusaccount_0012') || 'Add Credit Card'}
            </a>
          </div>
        </div>
      );
  }
  showPreferred(paymentMethods) {
    const preferred = this.getPreferred(paymentMethods);
    return (
      <div className="preferred-payment cf">
        <p>{i18n('prepay_0024') || 'Verify Your Payment Method'}</p>
        <div className="preferred-payment-box">
          <div className="preferred-payment-info">
            <ul>
              <li>{PaymentModelController.getCardLabel(preferred)}</li>
              <li>{PaymentModelController.maskCC(preferred.last_four)}</li>
              <li>{i18n('eplusaccount_2003') || 'Expires'}: {preferred.expiration_date}</li>
            </ul>
          </div>
          <div className="change-payment-type">
            <a onKeyPress={() => PaymentModelController.setShowPaymentList(true)}
              onClick={() => PaymentModelController.setShowPaymentList(true)}>{i18n('expedited_0023')}</a>
          </div>
        </div>
      </div>
    );
  }
  render () {
    const {profilePaymentMethods, showPaymentList} = this.props;
    if(showPaymentList === true) {
      return this.showPaymentList(profilePaymentMethods);
    } else {
      return this.showPreferred(profilePaymentMethods);
    }
  }
}

SelectPayment.displayName = "SelectPayment";
