export default class ThreeDSForm extends React.Component{
  constructor() {
    super();
    this.loadCall = this.loadCall.bind(this);
    this.state = {
      redirectUrl: ''
    }
  }
  componentDidMount () {
    this.threeDS.submit();
  }
  loadCall (e) {
    console.log("3ds iframe (ThreeDSForm.js) loaded", e.target);
  }
  render () {
    const {threeDS} = this.props;
    let redirect = window.location.origin + '/' + enterprise.language + '/payment3ds/_jcr_content.POST.html';

    return (
      <div>
        <form name="threeDSForm" ref={c => this.threeDS = c}
              action={threeDS.url} method="post" target="threeDS">
          <input type="hidden" name="PaReq" value={threeDS.paReq}/>
          <input type="hidden" name="TermUrl"
                 value={redirect}/>
        </form>
      {/*Where is the this.state.redirectUrl coming from?*/}
        <iframe name="threeDS" id="threeDS" src={this.state.redirectUrl} onLoad={this.loadCall} sandbox="allow-forms allow-scripts allow-same-origin"/>
      </div>
    );
  }
}

ThreeDSForm.displayName = "ThreeDSForm";
