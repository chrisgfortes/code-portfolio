import PreExpediteWarning from './Expedited/PreExpediteWarning';
import Modal from '../Modal/GlobalModal';
import PrivacyPolicy from './PrivacyPolicy';
import TaxesFees from './TaxesFees';
import PrepayTerms from './PrepayTerms';
import PrepayModal from './PrepayModal';
import ReservationCursors from '../../cursors/ReservationCursors';

export default class ReviewModal extends React.Component {
  onClose () {
    // Set focus point back to page after modal is closed
    if ( $('#globalErrorsContainer').length > 0 ) {
      $('#globalErrorsContainer').focus();
    } else {
      $('#prepay-container div.save').focus();
    }
  }
  render () {
    const { verification, expedited } = this.props;
    let modal = false;

    switch (verification.modal) {
      case 'taxesFees':
        modal = {
          content: <TaxesFees {...{ verification }} />,
          title: enterprise.i18nReservation.resflowcarselect_0077
        };
        break;
      case 'privacy':
        modal = {
          content: <PrivacyPolicy disclaimer={verification.privacyPolicy}/>,
          title: enterprise.i18nReservation.eplusenrollment_0061
        };
        break;
      case 'missingPayment':
        modal = {
          content: <PrepayModal />,
          title: enterprise.i18nReservation.resflowreview_0154
        };
        break;
      case 'prepay':
        modal = {
          content: (
            <PrepayTerms
              policy={verification.prepayPolicy}
              showPrepayTerms={verification.showPrepayTerms}
            />
          ),
          title: enterprise.i18nReservation.resflowreview_8005
        };
        break;
      case 'preExpediteWarning':
        modal = {
          content: <PreExpediteWarning expedited={expedited} />,
          title: enterprise.i18nReservation.expedited_0057,
          hideX: true,
          disableBlur: true
        };
        break;
      default :
        modal = {
          content: <div className="transition"></div>,
          title: enterprise.i18nReservation.resflowcorporate_4037
        };
    }

    return (
      <div>
        {verification.modal ?
          <Modal active={true}
                 cursor={ReservationCursors.verification.concat('modal')}
                 hideX={modal.hideX}
                 disableBlur={modal.disableBlur}
                 header={modal.title}
                 onClosing={this.onClose}
                 content={modal.content}/> : false}
      </div>
    );
  }
}

ReviewModal.displayName = 'ReviewModal';
