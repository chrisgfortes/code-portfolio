import ModalQueue from '../Modal/ModalQueue';
import classNames from 'classnames';
import ReservationFlowModelController from '../../controllers/ReservationFlowModelController'

export default class KeyFactsModal extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      ready: false
    };
    ModalQueue.enqueue(this.onQueueReady);
  }
  //need a better way to do this
  _handleClose () {
    ReservationFlowModelController.closeKeyFactsModal( this.props.cursor, false );
  }
  componentWillUnmount() {
    ModalQueue.dequeue();
  }
  onQueueReady() {
    this.setState({'ready': true});
  }
  createMarkup() {
    return {
      __html: this.props.content
    };
  }
  render () {
    let modalClasses = classNames({
      'modal-container': true,
      'active': this.state.ready//this.props.active //&& this.state.ready
    }), contentClasses = classNames({
      'modal-content': true,
      [this.props.classLabel]: this.props.classLabel
    });
    return (
      <div className={modalClasses} onClick={this._handleClose}>
        <div className={contentClasses} onClick={e => e.stopPropagation()}>
          {this.props.header &&
            <div className="modal-header">{this.props.header ? this.props.header : false}
              <button onClick={this._handleClose} className="close-modal"
                      aria-label={enterprise.i18nReservation.resflowcarselect_0065}>☓
              </button>
            </div>
          }

          <div className="modal-body cf" dangerouslySetInnerHTML={this.createMarkup()}/>
        </div>
      </div>
    );
  }
}

KeyFactsModal.displayName = 'KeyFactsModal';
