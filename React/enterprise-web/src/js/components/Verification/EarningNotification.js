// ECR-14250 profile and loyalty
export default function EarningNotification ({ profile, showECContent, showPageFlowCancelled }) {
  const firstName = profile.basic_profile.first_name;
  const loyaltyNumber = profile.basic_profile.loyalty_data.loyalty_number;
  return (
    <div className="notification">
      <div className="content">
        <div>
          { showECContent ? i18n('resflowreview_0022', {name: firstName, number: loyaltyNumber})
            : i18n('resflowreview_0401', {name: firstName, memberNumber: loyaltyNumber})
          }
        </div>
        { showECContent ?
          <div>
            <p>{enterprise.i18nReservation.resflowreview_0023}</p>
            <p>{enterprise.i18nReservation.resflowreview_0024}</p>
            <p>{enterprise.i18nReservation.resflowreview_0025}</p>
          </div>
          :
          <div>
            <p>{showPageFlowCancelled ? enterprise.i18nReservation.eplusaccount_0206 : null}</p>
            <p>{enterprise.i18nReservation.resflowreview_0410}</p>
            <p>{enterprise.i18nReservation.resflowreview_0440}</p>
          </div>
        }
      </div>
    </div>
  );
}

EarningNotification.displayName = 'EarningNotification';
