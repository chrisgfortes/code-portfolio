const VerificationController = require('../../controllers//VerificationController');
const Images = require('../../utilities/util-images');

export default function AvailableUpgrades ( nextUpgrade, verification, selectedCar ) {
  return (
    <div>
      { nextUpgrade && nextUpgrade.code !== selectedCar.code ?
        <div className="upgrade-block">
          <div className="icon icon-res-nav-notch"></div>
          <div className="upgrade-category">
            <img className="upgrade-image"
                 src={Images.getUrl(nextUpgrade.images.ThreeQuarter.path, 120, 'high')}
                 alt=""/>
            <div className="upgrade-info">
              {VerificationController.getUpgradeMessage( nextUpgrade )}
            </div>
            <div role="button" tabIndex="0" className="upgrade-button"
                 data-code={nextUpgrade.code}
                 onClick={VerificationController.upgradeVehicle.bind(null, nextUpgrade.code)}
                 onKeyPress={a11yClick(VerificationController.upgradeVehicle.bind(null, nextUpgrade.code))}>
              {enterprise.i18nReservation.resflowreview_0082}
            </div>
          </div>

        </div>
        : verification.upgraded ?
        <div className="upgrade-block">
          <div className="upgrade-category upgraded">
            <div className="upgrade-info">
              <div>
                {enterprise.i18nReservation.resflowextras_0018}
              </div>
            </div>
          </div>
        </div>
    : null }
    </div>
  );
}

AvailableUpgrades.displayName = 'AvailableUpgrades';
