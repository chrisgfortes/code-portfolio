import VerificationController from '../../controllers/VerificationController';

export default class Edit extends React.Component {
  constructor(props) {
    super( props );
  }
  onEdit () {
    if (this.props.locationType) {
      VerificationController.setLocationSearchType( this.props.locationType );
    }
    VerificationController.toggleInflightModify( true );
    location.hash = this.props.destination;
  }
  render() {
    return (
      <span className="edit" role="button" aria-label={this.props.editHeading} tabIndex="0" onClick={this.onEdit.bind(this)}
            onKeyPress={a11yClick(this.onEdit.bind(this))}>{this.props.editLabel}</span>
    )
  }
}

Edit.displayName = 'Edit';
