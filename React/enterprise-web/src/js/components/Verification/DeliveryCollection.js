import Content from './LogisticsContent';
import {RESERVATION} from '../../constants/'

export default function DeliveryCollection ( vehicleLogistics ) {
  return (
    <div className="section-content">
      {vehicleLogistics && vehicleLogistics.delivery_info ?
        <Content type={RESERVATION.DELIVERY} data={vehicleLogistics.delivery_info}/> : false}
      {vehicleLogistics && vehicleLogistics.collection_info ?
        <Content type={RESERVATION.COLLECTION} data={vehicleLogistics.collection_info}/> : false}
    </div>
  )
}

DeliveryCollection.displayName = 'DeliveryCollection';
