/* global ReservationStateTree, _ */
import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import Validator from '../../mixins/Validator';
import ReservationCursors from '../../cursors/ReservationCursors';

import VerificationController from '../../controllers/VerificationController';
import PricingController from '../../controllers/PricingController';

import GlobalModal from '../Modal/GlobalModal';
import SwitchPayType from '../PricingTable/SwitchPayType';
import Terms from './Terms';
import Amount from '../Currency/Amount';

import Scroll from '../../utilities/util-scroll';
import classNames from 'classnames';
import { PRICING, LOCALES, PROFILE, GLOBAL } from '../../constants';

const Submit = React.createClass({
  mixins: [
    React.addons.PureRenderMixin,
    BaobabReactMixinBranch,
    Validator
  ],
  getInitialState () {
    return {
      submitLoading: false
    };
  },
  cursors: {
    additionalInformationAll: ReservationCursors.additionalInformationAll,
    additionalInformationPostRate:ReservationCursors.additionalInformationPostRate,
    billingAuthorized: ReservationCursors.billingAuthorized,
    collection: ReservationCursors.collection,
    collectionAllowed: ReservationCursors.collectionAllowed,
    corporate: ReservationCursors.corporate,
    collectNewModifyPaymentCard: ReservationCursors.collectNewModifyPaymentCard,
    delivery: ReservationCursors.delivery,
    deliveryAllowed: ReservationCursors.deliveryAllowed,
    destinationPriceInfoModal: ReservationCursors.destinationPriceInfoModal,
    enroll: ReservationCursors.enroll,
    paymentProcessor: ReservationCursors.paymentProcessor,
    paymentReferenceID: ReservationCursors.paymentReferenceID
  },
  //@TODO this seems like too much is happening in here
  fieldMap () {
    const parentDOM = $(this).parent();
    const { verification, prepay, selectedCar, chargeType, personal, sessionContractDetails, modify, purpose, sessionPickupLocation } = this.props;
    const { deliveryAllowed, collectionAllowed, additionalInformationPostRate, delivery, collection, additionalInformationAll } = this.state;

    const _chargeType = VerificationController.getRedemptionChargeType( chargeType );
    const requestExtras = VerificationController.getRequestExtras( selectedCar, _chargeType );
    const showRequestExtras = VerificationController.showRequestExtras( selectedCar, requestExtras );
    const tripPurposeCondition = VerificationController.getTripPurposeCondition(modify, sessionContractDetails, deliveryAllowed, collectionAllowed, additionalInformationPostRate);

    let map = {
      value: {
        acceptPrivacy: verification.privacyChecked,
        acceptPrepay: verification.prepayChecked
      },
      schema: {
        acceptPrivacy: () => {
          if (enterprise.acceptPrivacyRequired) {
            return verification.privacyChecked;
          } else {
            return true;
          }
        },
        acceptPrepay: () => {
          if (prepay) {
            return verification.prepayChecked;
          } else {
            return true;
          }
        }
      },
      refs: {
        acceptPrivacy: parentDOM.find('.privacy-checkbox')[0],
        acceptPrepay: parentDOM.find('.prepay-checkbox')[0]
      }
    };

    if (showRequestExtras) {
      map.value.phoneNumber = personal.phoneNumber;
      map.schema.phoneNumber = 'phoneMask';
      map.refs.phoneNumber = document.getElementById('phoneNumber');
    }

    if (sessionContractDetails && tripPurposeCondition) {
      enterprise.log('sessionContractDetails: ', sessionContractDetails, 'tripPurposeCondition: ', tripPurposeCondition);
      map.value.purpose = purpose;
      map.schema.purpose = function () {
        return !!purpose;
      };
      map.refs.purpose = parentDOM.find('.travel-purpose-label')[0];
    }

    if (deliveryAllowed && delivery.enabled) {
      map.value.deliveryAddress = delivery.streetAddress;
      map.schema.deliveryAddress = 'string';
      map.refs.deliveryAddress = document.getElementById('Deliveryaddress');

      map.value.deliveryCity = delivery.city;
      map.schema.deliveryCity = 'string';
      map.refs.deliveryCity = document.getElementById('Deliverycity');

      map.value.deliveryPhone = delivery.phone;
      map.schema.deliveryPhone = 'phone';
      map.refs.deliveryPhone = document.getElementById('Deliveryphone');
    }

    if (collectionAllowed && collection.enabled) {
      map.value.collectionAddress = collection.streetAddress;
      map.schema.collectionAddress = 'string';
      map.refs.collectionAddress = document.getElementById('Collectionaddress');

      map.value.collectionCity = collection.city;
      map.schema.collectionCity = 'string';
      map.refs.collectionCity = document.getElementById('Collectioncity');

      map.value.collectionPhone = collection.phone;
      map.schema.collectionPhone = 'phone';
      map.refs.collectionPhone = document.getElementById('Collectionphone');
    }

    if (!!sessionPickupLocation.multi_terminal && personal.airlineCode !== false) {
      map.value.airline = personal.airlineCode;
      map.schema.airline = 'string';
      map.refs.airline = document.getElementById('airlineCode');
    }

    if (!modify) {
      for (let i = 0, len = additionalInformationAll.length; i < len; i++) {
        if (additionalInformationAll[i].required) {
          let fieldId = additionalInformationAll[i].id;
          map.value[fieldId] = document.getElementById(fieldId).value;
          map.schema[fieldId] = 'string';
          map.refs[fieldId] = document.getElementById(fieldId);
        }
      }
    }
    return map;
  },
  expeditedValidation() {
    const { expedited, loggedIn } = this.props;
    const issue = expedited.countryIssue;
    const residence = expedited.countryResidence;

    let countryResidenceLabel = i18n('resflowreview_0062');
    let regionResidenceLabel = i18n('eplusenrollment_0056');
    let addressLabel = i18n('resflowreview_0063');
    let cityLabel = i18n('resflowreview_0065');
    let postalLabel = i18n('resflowreview_0068');
    let dateOfBirthLabel = i18n('eplusenrollment_0031');
    let licenseIssueLabel = i18n('resflowreview_0720');
    let licenseExpiryLabel = i18n('resflowreview_0037');
    let passwordConfirm = i18n('loyaltysignin_0044');
    let terms = i18n('eplusenrollment_0036');

    let re = VerificationController.maskedDateExpression();
    let licenseIssueValid = re.test(expedited.licenseIssueDate) || GLOBAL.REGEX_MASK_CHAR.test(expedited.licenseIssueDate);
    let licenseExpiryValid = re.test(expedited.licenseExpiryDate) || GLOBAL.REGEX_MASK_CHAR.test(expedited.licenseExpiryDate);

    if (expedited.countryResidence.country_code === LOCALES.US) {
      regionResidenceLabel = i18n('resflowreview_0066');
    } else if (expedited.countryResidence.country_code === LOCALES.CA) {
      regionResidenceLabel = i18n('resflowreview_0067');
    }
    let additional = {
      value: {
        [dateOfBirthLabel]: expedited.dateOfBirth,
        [licenseIssueLabel]: expedited.licenseIssueDate,
        [licenseExpiryLabel]: expedited.licenseExpiryDate,
        [countryResidenceLabel]: expedited.countryResidence.country_name,
        [addressLabel]: expedited.address,
        [cityLabel]: expedited.city,
        [postalLabel]: expedited.postal,
        [regionResidenceLabel]: expedited.regionResidence,
        [passwordConfirm]: expedited.passwordConfirm,
        [terms]: expedited.terms
      },
      schema: {
        [dateOfBirthLabel]: 'dateMask',
        [licenseIssueLabel] () {
          if (issue && issue.license_issue_date && !loggedIn) {
            if (issue.license_issue_date && issue.license_issue_date === PROFILE.LICENSE_ISSUE_MANDATORY) {
              return licenseIssueValid;
            } else if (issue.license_issue_date && issue.license_issue_date === PROFILE.LICENSE_ISSUE_OPTIONAL) {
              return licenseIssueValid || licenseExpiryValid;
            } else {
              return true;
            }
          } else {
            return true;
          }
        },
        [licenseExpiryLabel] () {
          if (issue && issue.license_expiry_date && !loggedIn) {
            if (issue.license_expiry_date && issue.license_expiry_date === PROFILE.LICENSE_ISSUE_MANDATORY) {
              return licenseExpiryValid;
            } else if (issue.license_expiry_date && issue.license_expiry_date === PROFILE.LICENSE_ISSUE_OPTIONAL) {
              return licenseIssueValid || licenseExpiryValid;
            } else {
              return true;
            }
          } else {
            return true;
          }
        },
        [countryResidenceLabel]: 'string',
        [addressLabel]: 'string',
        [cityLabel]: 'string',
        [postalLabel]: (residence && residence.postal_code_type && !loggedIn) ? 'string' : '?string',
        [regionResidenceLabel]: residence && residence.enable_country_sub_division && !loggedIn ? 'string' : '?string',
        [passwordConfirm]: () => {
          if (expedited.password) {
            return (!!_.get(expedited, 'passwordConfirm.length') && (expedited.passwordConfirm === expedited.password))
          }
          return true;
        },
        [terms]: function () {
          return (expedited.password && expedited.passwordConfirm || expedited.password) ? expedited.terms : true;
        }
      }
    };
    return this.validateAll(additional).errors;
  },
  isMissingPayment() {
    return VerificationController.isMissingPayment( this.props.verification, this.props.prepay, this.props.modify,
                                              this.state.collectNewModifyPaymentCard, this.state.paymentProcessor, this.state.paymentReferenceID );
  },
  _onSubmit() {
    const { expedited, personal, modify } = this.props;
    const expediteSearchFilled = VerificationController.isExpediteSearchFilled( expedited, personal );
    const enforceExpeditedValidation = VerificationController.enforceExpeditedValidation( modify, expedited, expediteSearchFilled );
    const preExpediteWarning = expediteSearchFilled && !modify && (!expedited.render || expedited.render === 'search');

    if (preExpediteWarning) {
      VerificationController.callSetModal('preExpediteWarning');
    } else if (enforceExpeditedValidation) {
      let errors = this.expeditedValidation();
      if (errors && errors.length > 0) {
        VerificationController.callSetInput('errors', errors);
        VerificationController.callSetInput('modal', 'incomplete');
      } else {
        this.validatePage();
      }
    } else {
      this.validatePage();
    }
  },
  validatePage() {
    if (this.validateAll().valid && this.state.submitLoading === false && !this.isMissingPayment()) {
      this.setState({submitLoading: true});
      this.processEnrollment();
    } else {
      if (this.isMissingPayment()) {
        VerificationController.callSetModal('missingPayment');
        Scroll.scrollTo('#prepay-container');
      }
      const warningMessages = VerificationController.getWarningMessages( this.props.personal, this.props.sessionPickupLocation,
                                                                          this.props.prepay, this.props.verification );
      VerificationController.setErrorActions( warningMessages,  'verification');
      Scroll.scrollTo('#personal-information');
    }
  },
  processEnrollment() {
    const expedited = this.props.expedited;
    const enrollCriteria = expedited.password && expedited.passwordConfirm && expedited.terms;
    if (!this.props.loggedIn && _.isEmpty(this.state.enroll.profile) && enrollCriteria) {
      VerificationController.getEnrollResponse().then(response => {

        if (response && response.basic_profile) {
          this.commitReservation();
        } else {
          this.setState({ submitLoading: false });
          Scroll.scrollTo('#personal-information');
        }
      })
      .catch((res) => {
        console.error('processEnrollment() catch:', res)
      })
    } else {
      this.commitReservation();
    }
  },
  commitReservation() {
    VerificationController.callSubmitSave(this.props.modify)
      .then(() => {
        // a few divs are removed when the state changes
        // so we have to force the scroll position
        // Scroll.scrollTo('.submit-container', 200);
        this.setState({submitLoading: false});
        VerificationController.callSetInput('modal', false);
        // @todo: change to outerDomManager
        if (document.getElementById('personal-information')) {
          Scroll.scrollTo('#personal-information');
        } else {
          Scroll.scrollTo('#expedited-profile');
        }
      })
  },
  render() {
    let submitClasses = classNames({
      'btn': true,
      'submit': true,
      'disabled': this.state.submitLoading
    });
    const { sessionContractDetails, prepay, selectedCar, chargeType, modify,
            purpose, paymentInformation, verification, showPersonal } = this.props;

    // const { additionalInformationAll, billingAuthorized, collection, collectionAllowed, collectNewModifyPaymentCard, corporate,
    //   delivery, deliveryAllowed, enroll, isFedexReservation, paymentProcessor, destinationPriceInfoModal } = this.state;
    const { billingAuthorized, destinationPriceInfoModal } = this.state;

    const corporateSession = sessionContractDetails ? sessionContractDetails : false;

    const paymentMethod = prepay ? PRICING.PREPAY : PRICING.PAYLATER;
    const pricing = selectedCar.vehicleRates[paymentMethod] && selectedCar.vehicleRates[paymentMethod].price_summary;
    const estimatedTotalDestinationValue = pricing ? pricing.estimated_total_payment.format : PricingController.getNetRate(true);
    const estimatedTotalDestinationPriceCode = pricing ? pricing.estimated_total_payment.code : PricingController.getNetRate(true);
    const getForeignFeesDestinationAmountText = VerificationController.getForeignFeesDestinationAmountText(prepay, pricing);
    const foreignFees = getForeignFeesDestinationAmountText.foreignFees;
    const showDestinationAmount = getForeignFeesDestinationAmountText.showDestinationAmount;
    const cost = VerificationController.getCost(chargeType, selectedCar);
    const costFormat = _.get(cost, 'format') || '';
    const hasDifferentPaymentAndView = _.get(pricing, 'estimated_total_payment.code') !== _.get(pricing, 'estimated_total_view.code');
    const amount = <Amount className="amount" amount={PricingController.getSafeSplitCurrencyAmount(cost)}/>;
    const bookOrReserve = prepay ? i18n('resflowreview_0002') : i18n('resflowreview_0001');
    const submitLabel = modify ? i18n('resflowviewmodifycancel_0080') : bookOrReserve;
    const subLabel = VerificationController.getSubLabel(corporateSession, sessionContractDetails, hasDifferentPaymentAndView, costFormat, billingAuthorized, purpose);
    const corporateSummary = VerificationController.getCorporateSummary(corporateSession, sessionContractDetails, billingAuthorized, purpose, costFormat);
    const summaryText = corporateSession ? corporateSummary || subLabel : VerificationController.getSummaryText(prepay, modify, costFormat, hasDifferentPaymentAndView);
    const netRate = VerificationController.getNetRate(sessionContractDetails, selectedCar, chargeType);
    const pricingDifferences = selectedCar.priceDifferences;

    const unpaidRefundAmount = _.get(pricingDifferences, 'UNPAID_REFUND_AMOUNT.difference_amount_view') || false;
    const modifyPriceDifferenceCurrency = !!unpaidRefundAmount && PricingController.getSplitCurrencyAmount(unpaidRefundAmount);
    const modifyPriceDifferenceZero = !unpaidRefundAmount || unpaidRefundAmount.amount === PRICING.VALUE_ZERO;
    const unpaidRefundAmountPaymentFormat = _.get(pricingDifferences, 'UNPAID_REFUND_AMOUNT.difference_amount_payment.format') || false;
    const paidAmountCurrency = VerificationController.getPaidAmountCurrency(paymentInformation);
    const isUnpaidNotRefund = VerificationController.getIsUnpaidNotRefund(modifyPriceDifferenceCurrency);
    const cardNumber = '(' + _.get(paymentInformation, 'card_details.number', '') + ')';
    const showSwitchPay = VerificationController.showSwitchPay(modify, pricingDifferences, showPersonal);

    return (
      <div className="complete-reservation">
        <div className="submit-container section-content">
          <h3 className="view-header">{i18n('resflowreview_0158') || 'Complete Your Booking'}</h3>
          <div className="reserve-summary">
            {(modify && !modifyPriceDifferenceZero) &&
              <div className="information-block total-price">
                <h3 className="category-label">{"Updated Total"}</h3>
                <div className="amount">
                  {amount}
                </div>
                { hasDifferentPaymentAndView &&
                  <span> {estimatedTotalDestinationPriceCode} {estimatedTotalDestinationValue} </span>
                }
              </div>
            }
            {(modify && !!paidAmountCurrency && !modifyPriceDifferenceZero) &&
              <div className="information-block paid-amount">
                <h3 className="category-label">{i18n('prepay_0046') || 'Paid Amount'}</h3>
                <Amount className="currency amount" amount={paidAmountCurrency}/>
                {/*{ ECR-12632 = other currency will not be available until 2.4 earliest - see ECR-12947 }*/}
                {/*<span> {estimatedTotalDestinationPriceCode} {paidAmountFormat} </span>*/}
                <span> ({i18n('prepay_0047') || 'Original Total'}) </span>
              </div>
            }
            {(modify && !modifyPriceDifferenceZero) &&
              <div className="information-block unpaid-amount">
                <h3 className="category-label">
                  { isUnpaidNotRefund ? i18n('prepay_0048') || 'Unpaid Amount'
                                      : i18n('resflowreview_0127') || 'Refund Amount'
                  }
                </h3>
                <Amount className="currency amount" amount={modifyPriceDifferenceCurrency}/>
                { hasDifferentPaymentAndView &&
                  <span> {estimatedTotalDestinationPriceCode} {unpaidRefundAmountPaymentFormat} </span>
                }
                <span>
                    { isUnpaidNotRefund ? i18n('prepay_0070', {cardNumber}) || `Your credit card ${cardNumber} will be charged the following amount after rental is completed:`
                                        : i18n('prepay_0052') || 'Will be refunded after rental is completed'}
                  </span>
              </div>
            }
            {/* output the summary modified price if it's not modified or if there's modified costs */}
            {(!modify || modifyPriceDifferenceZero) &&
              summaryText
            }
            {/* check for fees and different regions */}
            {!!(showDestinationAmount || foreignFees) &&
            <div className="pay-now-tax-disclaimer">
              {!!showDestinationAmount &&
                <span>
                  <span className="destination-amount">
                    {showDestinationAmount}
                  </span>
                  <a href="#" role="button" tabIndex="0"
                     onKeyPress={a11yClick(VerificationController.learnMore)}
                     onClick={VerificationController.learnMore}
                     className="destination-price-link">{i18n('resflowcarselect_0102')}</a>
                </span>
              }
              {!!foreignFees &&
                <span>
                  {foreignFees}
                </span>
              }
            </div>
            }
            { showSwitchPay &&
              <SwitchPayType {...{selectedCar, prepay}} />
            }
            {netRate && <div> {i18n('resflowcorporate_0090')}</div>}
            {corporateSession.third_party_email_notify &&
              <div className="reserve-email-notify">
                <i className="icon icon-ico-email-extras"/>
                {i18n('resflowcorporate_0079', {ContractName: corporateSession.contract_name})}
              </div>
            }
          </div>
          <div className={this.state.submitLoading ? 'loading' : false}/>
          {(this.props.prepay || enterprise.acceptPrivacyRequired) &&
            <Terms {...{verification, prepay}} />
          }
        </div>
        <div className="submit-button-wrapper">
          <button onClick={this._onSubmit} className={submitClasses} id="reviewSubmit">
            {submitLabel}
          </button>
        </div>
        { !!destinationPriceInfoModal &&
          <GlobalModal active={destinationPriceInfoModal}
                       header={i18n('resflowcarselect_0102')}
                       content={prepay ? i18n('reservationnav_0044') : i18n('resflowcarselect_0105')}
                       cursor={ReservationCursors.model.concat('destinationPriceInfoModal')}
                       onClosing={VerificationController.destinationScrollReset} />
        }
      </div>
    );
  }
});

module.exports = Submit;
