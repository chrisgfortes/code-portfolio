import {PRICING, PAGEFLOW} from '../../constants';
import PricingController from '../../controllers/PricingController';
import VerificationController from '../../controllers/VerificationController';
import PriceTableHeading from '../PricingTable/PriceTableCategoryHeader';
import PriceTableLineItem from '../PricingTable/PriceTableLineItem';
import PersonalInformation from '../Confirmed/PersonalInformation';
import AdditionalInformation from '../Confirmed/AdditionalInformation';
import CodeAndTripPurpose from '../Confirmed/CodeAndTripPurpose';
import SwitchPayType from '../PricingTable/SwitchPayType';
import AvailableUpgrades from './AvailableUpgrades';
import InformationBlock from './InformationBlock';
import ModifyContent from './ModifyContent';
import ExtrasLineItem from '../PricingTable/ExtrasLineItem';

export default class PriceDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: false,
      ageLabel: null
    };
    this.collapse = this.collapse.bind(this);
  }
  collapse () {
    this.setState({collapsed: !this.state.collapsed});
  }
  render () {
    const { modify, codeApplicable, pickupCountryCode, selectedCar, verification, billingAccount, travelPurpose,
            chargeType, prepay, paymentInformation, sessionContractDetails, showPersonal, upgrades, title,
            sessionPickupTime, sessionDropoffTime, sessionPickupLocation, sessionDropoffLocation,
            existingReservationConfirmationNumber, airlineInformation, additionalInformationSaved,
            componentToRender, renterAge, driverInformation } = this.props;

    const paymentMethod = prepay ? 'PREPAY' : 'PAYLATER';
    const pricing = selectedCar.vehicleRates[paymentMethod] && selectedCar.vehicleRates[paymentMethod].price_summary;
    const getForeignFeesDestinationAmountText =  VerificationController.getForeignFeesDestinationAmountText( prepay, pricing );
    const foreignFees = getForeignFeesDestinationAmountText.foreignFees;
    const showDestinationAmount = getForeignFeesDestinationAmountText.showDestinationAmount;
    const vehicleRate = pricing && pricing.vehicle_line_items ? pricing.vehicle_line_items : [];
    const extras = pricing && pricing.extra_line_items ? pricing.extra_line_items : [];
    const savings = pricing && pricing.savings_line_items ? pricing.savings_line_items : [];
    const fees = VerificationController.getFees( savings, pricing );
    const labels = VerificationController.getLabels( paymentInformation, componentToRender, prepay );
    const couponInfoHeading = VerificationController.getCouponInfoHeading( sessionContractDetails );
    const estimatedTotalValue = VerificationController.getEstimatedTotalValue(pricing, sessionContractDetails, selectedCar, chargeType);
    const unpaidRefundAmount = _.get(selectedCar.priceDifferences, 'UNPAID_REFUND_AMOUNT.difference_amount_view') || false;
    const modifyPriceDifferenceZero = !unpaidRefundAmount || unpaidRefundAmount.amount === '0.00';
    const modifyPriceDifferenceCurrency = !!unpaidRefundAmount && PricingController.getSplitCurrencyAmount(unpaidRefundAmount);
    const redemptionSavings = PricingController.getRedemptionSavings( chargeType, pricing );
    const summary = VerificationController.getCarSummary( selectedCar, this.state.ageLabel, sessionPickupTime, sessionDropoffTime,
      sessionPickupLocation, sessionDropoffLocation, sessionContractDetails );

    const showRedemption = VerificationController.showRedemption( chargeType );
    const showContractDetails = VerificationController.showContractDetails( showPersonal, sessionContractDetails );
    const showAvailableUpgrades = VerificationController.showAvailableUpgrades( selectedCar, existingReservationConfirmationNumber, upgrades );
    const showAdditionalInfo = VerificationController.showAdditionalInfo( showPersonal, modify, additionalInformationSaved );
    const showAdditionalInfoOnConfirm = VerificationController.showAdditionalInfoOnConfirm( showAdditionalInfo, componentToRender );
    const showModifyContent = VerificationController.showModifyContent( modify, modifyPriceDifferenceZero );
    const showAirlineInfo = VerificationController.showAirlineInfo( showPersonal, airlineInformation );
    const showSwitchPay = VerificationController.showSwitchPay(modify, selectedCar.priceDifferences, showPersonal);

    return (
      <div id="price-details" className="section-content rate-taxes-fees" ref="price-details" aria-expanded={this.state.collapsed}>
        <h2 className="view-header" onClick={this.collapse}>
          {title}
          <span className="icon icon-nav-carrot-down" />
          <span role="button" tabIndex="0" aria-controls="price-details" className="edit"
                onClick={this.collapse} onKeyPress={a11yClick(this.collapse)}>
            {i18n('resflowcarselect_0010')}
          </span>
        </h2>
        <div className="information-block vehicle resume">
          <img alt={summary.vehicleMake} src={summary.image}/>
          <h3 id="section-header-TaxesAndFees" className="category-label">{labels.totalLabel}</h3>
          <div className="amount">
            <div className="currency">
              <span className="symbol">{estimatedTotalValue.symbol}</span>
              <span className="unit">{estimatedTotalValue.integralPart}</span>
              <span className="fraction">{estimatedTotalValue.decimalDelimiter}{estimatedTotalValue.decimalPart}</span>
            </div>
          </div>
        </div>

        <InformationBlock mileageInfo={selectedCar.mileageInfo} {...{summary, vehicleRate, selectedCar}}/>

          {(extras.length > 0) &&
          <div className="information-block extras">
            <h3 className="category-label">{i18n('reservationnav_0011')}</h3>

            <div className="info-block-details">
              {extras.length ? extras.map((item, index) => {
                return (<ExtrasLineItem
                  label={VerificationController.determineRateLabel}
                  item={item}
                  hideRate={true}
                  id={'extras-row-header-' + (index + 1)}
                  key={index}/>);
              }) : false}
            </div>

          </div>}

          {(fees.length > 0) &&
            <div className="information-block taxes-and-rates">
              <h3 id="section-header-TaxesAndFees" className="category-label">{i18n('reservationnav_0019')}</h3>
              <span role="button" tabIndex="0" onClick={VerificationController.onSurcharge}
                                  onKeyPress={a11yClick(VerificationController.onSurcharge)} className="edit surcharge">
                                {i18n('resflowcarselect_0102')}</span>
            {fees.length ?
              <table>
              <tbody>
              {fees.map((item, index) => {
                return (<PriceTableLineItem
                rowName={VerificationController.setPriceLineItemRowName(item.description, true, true, pickupCountryCode)}
                middleCol={VerificationController.setPriceLineItemMidCol(false, item)}
                cost={VerificationController.setPriceLineItemCost(item)}
                id={'taxesandfees-row-header-' + (index + 1)}
                headers={'section-header-taxesandfees taxesandfees-row-header-' + (index + 1)}
                key={index}/>);
              })}
            </tbody></table>
             : false}
            </div>
          }

        {showModifyContent &&
          <ModifyContent {...{estimatedTotalValue, modifyPriceDifferenceCurrency}}
            isunpaidNotRefund={VerificationController.getIsUnpaidNotRefund(modifyPriceDifferenceCurrency)}
            paidAmountCurrency={VerificationController.getPaidAmountCurrency(paymentInformation)}/>
        }

        {(!modify || modifyPriceDifferenceZero) &&
          <div className="information-block pricing">
          {couponInfoHeading.contractType === PRICING.CORPORATE &&
            <div className="car-savings">
              <i className="icon icon-icon-promo-applied left" />
              <span className="category-label">{i18n('resflowcarselect_8010') || 'CUSTOM RATE'}</span>
            </div>
          }
            <h3 id="section-header-TaxesAndFees" className="category-label">{labels.totalLabel}</h3>
            <div className="amount">
            {componentToRender === PAGEFLOW.VERIFICATION || componentToRender === PAGEFLOW.MODIFY ?
              <div className="currency">
                <span className="symbol">{estimatedTotalValue.symbol}</span>
                <span className="unit">{estimatedTotalValue.integralPart}</span>
                <span className="fraction">{estimatedTotalValue.decimalDelimiter}{estimatedTotalValue.decimalPart}</span>
              </div>
              :
              <div>
                {
                  billingAccount && billingAccount.billing_account_number ?
                    <div>{i18n('resflowcorporate_0207')}
                      <div>[{billingAccount.billing_account_number}]</div>
                    </div>
                  : labels.paymentMethodLabel
                }
              </div>
            }
            </div>

            { showSwitchPay &&
              <SwitchPayType {...{selectedCar, prepay}}/>
            }
            {!!(showDestinationAmount || foreignFees) &&
              <div className="pay-now-tax-disclaimer">
              {!!showDestinationAmount &&
                <span>
                  <span className="destination-amount">
                    {showDestinationAmount}
                  </span>
                  <a href="#" role="button" tabIndex="0"
                     onKeyPress={a11yClick(VerificationController.learnMore)}
                     onClick={VerificationController.learnMore}
                     className="destination-price-link">{i18n('resflowcarselect_0102')}</a>
                </span>
              }
              {!!foreignFees &&
                <span>
                  {foreignFees}
                </span>
              }
              </div>
            }
          </div>}
          { showAvailableUpgrades &&
            <AvailableUpgrades hideImage 
                               nextUpgrade={upgrades[0]}
                               {...{ verification, selectedCar }}/>
          }
          <table>
            {!!showPersonal &&
              <PersonalInformation {...{driverInformation, renterAge}}/>
            }

            {showAirlineInfo &&
            <tbody>
              <PriceTableHeading
                id="section-header-AirlineInformation"
                title={i18n('resflowreview_0057')}/>
              <tr>
                  <th className="airline-desc" id="AirlineInformation-row-header-1">{i18n('resflowreview_0060')}</th>
                  <td className="amount" headers="section-header-AirlineInformation AirlineInformation-row-header-1">{airlineInformation.description}</td>
                </tr>
                 <tr>
                   <th className="airline-desc" id="AirlineInformation-row-header-2">{i18n('resflowreview_0061')}</th>
                   <td className="amount" headers="section-header-AirlineInformation AirlineInformation-row-header-2">{airlineInformation.flight_number}</td>
                 </tr>
            </tbody>}

            {showContractDetails &&
            <tbody>
              <PriceTableHeading
                id="section-header-CodeAndTripPurpose"
                title={couponInfoHeading.couponInfoHeading}/>
            {codeApplicable && showPersonal && sessionContractDetails ?
              <CodeAndTripPurpose headers="section-header-CodeAndTripPurpose CodeAndTrip-row-header-1" id="CodeAndTripPurpose-row-header-1"
                                  travelPurpose={travelPurpose}
                                  sessionContractDetails={sessionContractDetails}  />
              : false}
            </tbody>}

            {showAdditionalInfoOnConfirm &&
            <tbody className="additional-information">
              <PriceTableHeading
                id="section-header-AdditionalInformation"
                title={i18n('resflowcorporate_0020')}/>
                {additionalInformationSaved.map((item, index) => {
                  if (item) {
                    return (<AdditionalInformation
                      name={item.name ? item.name : i18n('resflowconfirmation_1102')}
                      value={item.value}
                      id={'AdditionalInformation-row-header-' + (index + 1)}
                      headers={'section-header-AdditionalInformation AdditionalInformation-row-header-' + (index + 1)}/>);}
                  if (item.length <= 0) {
                    return (<AdditionalInformation
                      name={i18n('resflowconfirmation_1103')}/>);}
                })}
            </tbody>}

            { redemptionSavings &&
              <tbody>
                <PriceTableHeading
                  id="section-header-RedemptionSavings"
                  title={i18n('resflowreview_151')}/>
                <tr>
                  <th className="redemption-name" id="RedemptionSavings-row-header-1">{i18n('resflowreview_152')}</th>
                  <td className="amount" headers="section-header-RedemptionSavings RedemptionSavings-row-header-1">
                    {i18n('resflowreview_0116', { number: selectedCar.redemptionDaysCount, points: selectedCar.redemptionPointsRate})}
                  </td>
                </tr>
              </tbody>
            }

          </table>

          { showRedemption &&
            <div className="redemption-total-text">
              <div>{i18n('resflowcarselect_0064')}</div>

              <div>{i18n('resflowcarselect_0050')}</div>
            </div>
          }
      </div>
    );
  }
}

PriceDetails.displayName = 'PriceDetails';
