import ReservationCursors from '../../cursors/ReservationCursors';
import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';
import GlobalModal from '../Modal/GlobalModal';
import RedemptionModal from '../CarSelect/RedemptionModal';
import ReviewModals from './ReviewModal';
import ConflictAccountModal from './ConflictAccountModal';
import CorporateModals from '../Corporate/ActionModals';
import CarSelectController from '../../controllers/CarSelectController';
import CarSelectActions from '../../actions/CarSelectActions';

const pointsToggleSelectDaysControl = {
  time: 500,
  timeout: null,
  setDays: function () {
    const resultDays = (
      CarSelectController
        .pointsToggle
        .selectRedemptionVehicle(
          pointsToggleSelectDaysControl.timeout,
          pointsToggleSelectDaysControl.time
        )
    );

    pointsToggleSelectDaysControl
      .timeout = resultDays;
  },
  plus: function () {
    CarSelectActions.addDay();
    pointsToggleSelectDaysControl.setDays();
  },
  minus: function () {
    CarSelectActions.subtractDay();
    pointsToggleSelectDaysControl.setDays();
  }
};

const Modals = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    expedited: ReservationCursors.expedited,
    redemptionModal: ReservationCursors.redemptionModal,
    cannotModifyModal: ReservationCursors.cannotModifyModal,
    destinationPriceInfoModal: ReservationCursors.destinationPriceInfoModal,
    carSelect: ReservationCursors.viewCarSelect,
    conflictAccountModal: ReservationCursors.conflictAccountModal,
    verification: ReservationCursors.verification,
    selectedCar: ReservationCursors.selectedCar,
    basicProfileLoyalty: ReservationCursors.basicProfileLoyalty,
    redemption: ReservationCursors.redemption
  },
  render () {
    const { expedited, carSelect, conflictAccountModal, verification, cannotModifyModal, destinationPriceInfoModal, redemption, selectedCar, basicProfileLoyalty } = this.state;
    return (
      <div>
        <CorporateModals />

        <ReviewModals {...{verification, expedited}} />

        {carSelect.redemption.modal &&
          <GlobalModal
            active={carSelect.redemption.modal}
            hideX={true}
            header={enterprise.i18nReservation.resflowcarselect_0007}
            content={<RedemptionModal {...{
                                      car: carSelect.selectedCarDetails,
                                      selectDaysControl: pointsToggleSelectDaysControl,
                                      close: CarSelectActions.toggleRedemptionModal.bind(null, false),
                                      selectedCar,
                                      basicProfileLoyalty,
                                      redemption}} />}
            classLabel="redemption-modal-container"
            cursor={ReservationCursors.redemptionModal}
          />
        }

        {conflictAccountModal &&
          <GlobalModal
            active={conflictAccountModal}
            header="Conflict Exists"
            content={<ConflictAccountModal  />}
            cursor={ReservationCursors.conflictAccountModal}/>

        }
        {!!cannotModifyModal &&
          <GlobalModal active={cannotModifyModal}
                       header="Enterprise"
                       content={i18n('resflowviewmodifycancel_0081')}
                       cursor={ReservationCursors.cannotModifyModal}/>
        }
        {!!destinationPriceInfoModal &&
          <GlobalModal active={destinationPriceInfoModal}
                       header="Enterprise"
                       content={i18n('resflowcarselect_0105')}
                       cursor={ReservationCursors.destinationPriceInfoModal}/>
        }
      </div>
    )
  }
});

export default Modals;
