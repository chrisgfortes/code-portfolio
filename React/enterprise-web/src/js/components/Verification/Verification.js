/**
 * @todo: clean this up, starting with cursors...
 */
import VerificationController from '../../controllers/VerificationController';
import CorporateController from '../../controllers/CorporateController';
import { PROFILE } from '../../constants/';

import Header from './Header';
import EarningNotification from './EarningNotification';
import EnoughPoints from '../CarSelect/EnoughPoints';

import PriceDetails from './PriceDetails';
import RentalSummary from './RentalSummary';
import PersonalInformation from './PersonalInformation';
import Associate from './Associate';

import FlightInformation from './FlightInformation';
import PreExpeditedBanner from './Expedited/PreExpeditedBanner';
import Expedited from './Expedited/Expedited';
import Prepay from './Prepay/Prepay';
import Submit from './Submit';
import Modals from './Modals';

import ReservationCursors from '../../cursors/ReservationCursors';
import { branch as BaobabReactMixinBranch } from 'baobab-react/mixins';

//Corporate Flow
import TripPurpose from '../Corporate/TripPurpose';
import {AdditionalInformation} from '../Corporate/AdditionalInformation';
import DeliveryCollection from '../Corporate/DeliveryCollection';
import Billing from '../Corporate/Billing';

//Redemption
import RedirectActions from "../../actions/RedirectActions";

//Key Rental Facts
import KeyRentalFactsAndPolicies from '../KeyRentalFacts/KeyRentalFactsAndPolicies';
import SpecialMessage from '../Corporate/SpecialMessage';

const Verification = React.createClass({
  mixins: [
    React.addons.PureRenderMixin,
    BaobabReactMixinBranch
  ],
  getInitialState () {
    // to avoid screen jumping when the session is updated during
    // the submit, we keep the necessary components rendered by switching
    // the state below
    return {
      didRenderExpeditedBanner: false,
      didRenderExpedited: false
    };
  },
  componentWillMount () {
    VerificationController.getDefaultCountry(this.state.account, this.state.profile);
  },
  cursors: {
    // someone please stop the madness
    // @todo: clean up these cursors
    additionalInformationSaved: ReservationCursors.additionalInformationSaved,
    airlineInformation: ReservationCursors.airlineInformation,
    associateAccountStatus: ReservationCursors.associateAccount,
    account: ReservationCursors.account,
    ageLabel: ReservationCursors.ageLabel,
    billingAccount: ReservationCursors.billingAccount,
    blockModifyPickupLocation: ReservationCursors.blockModifyPickupLocation,
    blockModifyDropoffLocation: ReservationCursors.blockModifyDropoffLocation,
    confirmationNumber: ReservationCursors.confirmationNumber,
    componentToRender: ReservationCursors.componentToRender,
    chargeType: ReservationCursors.chargeType,
    codeApplicable: ReservationCursors.codeApplicable,
    componentInUse: ReservationCursors.componentToRender,
    conflictAccountModal: ReservationCursors.conflictAccountModal,
    deepLinkReservation: ReservationCursors.deepLinkReservation,
    driverInformation: ReservationCursors.driverInformation,
    driverInfoIdentifier: ReservationCursors.driverInfoIdentifier,
    driverInfoLoyalty: ReservationCursors.driverInfoLoyalty,
    errors: ReservationCursors.verificationErrors,
    enrollErrors: ReservationCursors.enrollErrors,
    expedited: ReservationCursors.expedited,
    expediteEligible: ReservationCursors.expediteEligibility,
    existingReservationConfirmationNumber: ReservationCursors.existingReservationConfirmationNumber,
    keyRentalFacts: ReservationCursors.keyFactsPoliciesFromReservation,
    lockedCIDModal: ReservationCursors.lockedCIDModal,
    loggedIn: ReservationCursors.userLoggedIn,
    paymentInformation: ReservationCursors.paymentInformation,
    pickupCountryCode: ReservationCursors.pickupCountryCode,
    personal: ReservationCursors.personal,
    prepay: ReservationCursors.prepay,
    profile: ReservationCursors.profile,
    purpose: ReservationCursors.purpose,
    redemption: ReservationCursors.redemption,
    renterAge: ReservationCursors.resRequestRenterAge,
    selectedCar: ReservationCursors.selectedCar,
    sessionBrand: ReservationCursors.sessionBrand,
    sessionDropoffTime: ReservationCursors.sessionDropoffTime,
    sessionExpedited: ReservationCursors.sessionExpedited,
    sessionPickupTime: ReservationCursors.sessionPickupTime,
    sessionDropoffLocation: ReservationCursors.sessionDropoffLocation,
    sessionPickupLocation: ReservationCursors.sessionPickupLocation,
    sessionContractDetails: ReservationCursors.sessionContractDetails,
    travelPurpose: ReservationCursors.travelPurpose,
    userAssociatedWithRes: ReservationCursors.userAssociatedWithRes,
    userLoggedIn: ReservationCursors.userLoggedIn,
    upgrades: ReservationCursors.availableUpgrades,
    vehicleLogistics: ReservationCursors.vehicleLogistics,
    verification: ReservationCursors.verification
  },
  getResPointsDays ( redemption, profile, selectedCar ) {
    // ECR-14250
    const points = (profile && profile.basic_profile.loyalty_data) ? profile.basic_profile.loyalty_data.points_to_date : null;
    const redemptionDaysPoints = {
      days: redemption.days,
      points: selectedCar.redemptionPointsRate
    };
    return {points, redemptionDaysPoints};
  },
  render () {
    //could possible break these back up into price details and submit but  alot of these are actually shared
    const { prepay, profile, sessionContractDetails, userLoggedIn, expedited, userAssociatedWithRes, driverInformation,
            redemption, chargeType, driverInfoIdentifier, driverInfoLoyalty, personal, sessionExpedited, verification,
            keyRentalFacts, expediteEligible, selectedCar, confirmationNumber, sessionBrand, codeApplicable, purpose,
            pickupCountryCode, paymentInformation, upgrades, sessionPickupTime, travelPurpose, billingAccount, errors,
            sessionDropoffTime, sessionPickupLocation, sessionDropoffLocation, existingReservationConfirmationNumber,
            airlineInformation, additionalInformationSaved, associateAccountStatus, componentToRender, componentInUse,
            vehicleLogistics, enrollErrors, loggedIn, deepLinkReservation, lockedCIDModal, blockModifyPickupLocation,
            blockModifyDropoffLocation, ageLabel, renterAge } = this.state;
    const { modify } = this.props;

    const corporateSpecialMessage =  CorporateController.getSpecialMessage(sessionContractDetails);
    let showBilling = CorporateController.showBillingInfo(purpose, sessionContractDetails);

    let personalCheck = expedited.render !== PROFILE.PROFILE && expedited.countryIssue;
    let resEnrollment = enterprise.inResEnrollment;

    // ECR-11272 if the driver info is returned and it includes a individual_indentifier (+ confirm it is a loyalty club / EC)
    const ecUnauthAssociated = VerificationController.ecUnauthAssociated( driverInfoIdentifier, driverInfoLoyalty );
    const showExpeditedBanner = VerificationController.showExpeditedBanner( resEnrollment, userLoggedIn, sessionExpedited, modify, ecUnauthAssociated );
    const onlyShowEP = VerificationController.onlyShowEP( sessionContractDetails );
    const showExpedited = VerificationController.showExpedited( userLoggedIn, expedited, modify, ecUnauthAssociated, expediteEligible );
    const showFlightInfo = VerificationController.showFlightInfo( sessionPickupLocation );
    const showAssociateAccountStatus = VerificationController.showAssociateAccountStatus( modify, userLoggedIn, profile, userAssociatedWithRes, associateAccountStatus );
    const showKeyRentalFacts = VerificationController.showKeyRentalFacts( keyRentalFacts );
    const showRedemption = VerificationController.showRedemption( chargeType );
    const showECContent = VerificationController.showECContent( sessionBrand );
    const showPageFlowCancelled = VerificationController.showPageFlowCancelled( sessionBrand );

    return (
      <section className="verification-page">

        <Header modify={modify} {...{confirmationNumber, prepay}}
                onClick={VerificationController.getRes.bind(null, confirmationNumber, _.get(driverInformation, 'first_name'), _.get(driverInformation, 'last_name'))}
                onSkipToDetails={VerificationController.skipToDetails} />

        { profile && userLoggedIn &&
          <EarningNotification {...{profile, showECContent, showPageFlowCancelled}} />
        }

        { showRedemption &&
          <EnoughPoints points={this.getResPointsDays( redemption, profile, selectedCar ).points}
                        redemption={this.getResPointsDays( redemption, profile, selectedCar ).redemptionDaysPoints}
                        redirect={RedirectActions.goToReservationStep}/>
        }
        <div className="aside">
          <PriceDetails
              showPersonal={false}
              title={i18n('resflowreview_0160') || 'Price'}
              {...{ modify, codeApplicable, pickupCountryCode, selectedCar, sessionDropoffLocation,
                    chargeType, prepay, paymentInformation, sessionContractDetails, verification,
                    sessionPickupTime, sessionDropoffTime, sessionPickupLocation, upgrades,
                    billingAccount, existingReservationConfirmationNumber, airlineInformation,
                    additionalInformationSaved, componentToRender, travelPurpose, renterAge,
                    driverInformation }}/>

          <RentalSummary {...{ modify, ageLabel, codeApplicable, lockedCIDModal, blockModifyPickupLocation,
                               blockModifyDropoffLocation, selectedCar, sessionContractDetails, prepay,
                               profile, sessionPickupTime, sessionDropoffTime, sessionPickupLocation,
                               sessionDropoffLocation }} />

          <KeyRentalFactsAndPolicies showKeyFacts={showKeyRentalFacts} />
        </div>

        <div className="review-section">
          { !!corporateSpecialMessage &&
            <div className="personal-information section-content">
              <SpecialMessage name={sessionContractDetails.contract_name}
                              message={corporateSpecialMessage} />
            </div>
          }

          { showAssociateAccountStatus &&
            <Associate {...{associateAccountStatus}} />
          }

          { showExpeditedBanner &&
            <PreExpeditedBanner onlyShowEP={onlyShowEP}/>
          }
          { !!personalCheck &&
            <PersonalInformation {...{ errors, enrollErrors, personal, loggedIn, expedited, verification,
                                       confirmationNumber, componentInUse, deepLinkReservation, profile,
                                       driverInformation, driverInfoIdentifier, driverInfoLoyalty,
                                       sessionContractDetails }} />
          }

          { prepay &&
            <Prepay modify={modify} />
          }

          { !modify && [
            <TripPurpose key="trip"/>,
            showBilling && <Billing key="billing"/>,
            <DeliveryCollection key="dnc" {...{vehicleLogistics}} />
          ]}

          <AdditionalInformation />

          { showExpedited &&
            <Expedited />
          }
          { showFlightInfo &&
            <FlightInformation {...{ sessionPickupLocation, personal }} />
          }

          <Submit {...{ chargeType, sessionContractDetails, expedited, modify, prepay,
                        purpose, paymentInformation, personal, selectedCar, verification,
                        sessionPickupLocation, loggedIn }} />

          <Modals />
        </div>
      </section>
    );
  }
});

module.exports = Verification;
