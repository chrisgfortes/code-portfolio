//is this file even used?

import VerificationController from '../../actions/VerificationActions';
import Validator from '../../utilities/util-validator';

export default class CreateAccount extends React.Component {
  constructor() {
    super();
    this.state = {
      password: null,
      confirmPassword: null,
      terms: false
    };
    this.validator = new Validator(this, this.fieldMap);
  }
  fieldMap() {
    return {
      refs: {
        password: this.password,
        confirmPassword: this.confirmPassword
      },
      value: {
        password: this.password.value,
        confirmPassword: this.confirmPassword.value
      },
      schema: {
        password: 'password',
        confirmPassword: () => {
          let passwordConfirm = this.confirmPassword.value;
          return passwordConfirm.length > 0 && this.refs.password.value === passwordConfirm;
        }
      }
    }
  }
  _handleInputChange (attribute, event) {
    this.setState(
      {[attribute]: event.target.value}
    );
    VerificationController.setPersonalFields(attribute, event.target.value);
    this.validate(attribute, event.target.value);
  }
  _onChange (value, event) {
    this.setState({
      terms: event.target.checked
    });
    VerificationController.setPersonalFields(value, event.target.checked);
  }
  render () {
    return (
      <div>
        <h2>Create An Account</h2>

        <div className="field-container password">
          <label htmlFor="password">Create Password</label>
          <input onChange={this._handleInputChange.bind(this, 'password')}
                 type="password"
                 name="password"
                 ref="password"
                 id="createPassword"/>
        </div>

        <div className="field-container confirmPassword">
          <label htmlFor="confirmPassword">Confirm Password</label>
          <input onChange={this._handleInputChange.bind(this, 'confirmPassword')}
                 type="password"
                 name="confirmPassword"
                 ref="confirmPassword"
                 id="confirmPassword"/>
        </div>

        <div className="field-container terms">
          <label htmlFor="terms">
            <input onChange={this._onChange.bind(this, 'terms')} id="terms"
                   name="terms" type="checkbox" checked={this.state.terms}/>
            I have read the Enterprise Plus Terms &amp; Conditions
          </label>
        </div>

      </div>
    );
  }
}

CreateAccount.displayName = 'CreateAccount';
