import VerificationController from '../../controllers/VerificationController';

export default function Associate () {
  return (
    <div className="associate-account-banner">
      <div className="earn-points-text">
        {enterprise.i18nReservation.resflowviewmodifycancel_0063}
      </div>

      <div onClick={VerificationController.callLinkAccount}
           onKeyPress={a11yClick(VerificationController.callLinkAccount)}
           className="btn link-account-button">{enterprise.i18nReservation.resflowviewmodifycancel_0064}</div>
    </div>
  )
}

Associate.displayName = 'Associate';
