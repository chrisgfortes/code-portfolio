import VerificationController from '../../controllers/VerificationController';

export default class PrivacyPolicy extends React.Component {
  constructor ( props ) {
    super( props );
    VerificationController.getDisclaimer();
  }
  onPrintClick (event) {
    event.preventDefault();
    window.print();
  }
  render () {
    return (
      <div className="privacy-policy-modal">
        <div className="print-link">
          <a href="#" role="button" tabIndex="0"
             onKeyPress={a11yClick(this.onPrintClick)}
             onClick={this.onPrintClick}>
            {enterprise.i18nReservation.resflowreview_0153}
          </a>
        </div>
        <div className="privacy-modal-content" dangerouslySetInnerHTML={{__html: this.props.disclaimer}}/>
      </div>
    );
  }
}

PrivacyPolicy.defaultProps = {
  disclaimer: null
};

PrivacyPolicy.displayName = 'PrivacyPolicy';
