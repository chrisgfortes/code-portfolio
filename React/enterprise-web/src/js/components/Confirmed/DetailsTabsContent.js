import DetailsMap from './DetailsMap';
import ConfirmedController from '../../controllers/ConfirmedController';
import classNames from 'classnames';

export default class DetailsTabsContent extends React.Component {
  constructor(props) {
    super(props);
    const locationType = this.props.locationType;
    ConfirmedController.getLocationHoursByDay('pickup', moment(locationType.pickup.date, enterprise.i18nUnits.dateformat, true));
    ConfirmedController.getLocationHoursByDay('dropoff', moment(locationType.dropoff.date, enterprise.i18nUnits.dateformat, true));
  }
  _onMoreInfo (afterHourMessage, event) {
    event.preventDefault();
    if (!afterHourMessage) {
      afterHourMessage = '';
    }
    enterprise.utilities.modal.open('<h2>' + enterprise.i18nReservation.resflowlocations_0012 + '</h2><p>' + afterHourMessage + '</p>');
  }
  render () {
    const { activeItem, sessionPickupLocation, sessionDropoffLocation, afterHourMessage, pickupHours, dropoffHours } = this.props;
    const location = (activeItem.label !== 'RETURN') ? sessionPickupLocation : sessionDropoffLocation;
    const type = (activeItem.label !== 'RETURN') ? 'pickup' : 'dropoff';
    const wayfindingData = location.wayfindings || [];
    const locationHours = (activeItem.label !== 'RETURN') ? pickupHours : dropoffHours;
    let phone = false;
    let willPickYouUp = null;

    let hoursLabel = (!afterHourMessage) ? <span
      className="hours-label">{type === 'pickup' ? enterprise.i18nReservation.resflowconfirmation_0024 : enterprise.i18nReservation.resflowconfirmation_0028}</span> :
      <span
        className="hours-label">{type === 'pickup' ? enterprise.i18nReservation.resflowconfirmation_0024 : enterprise.i18nReservation.resflowconfirmation_0028}
        <a href="#"
           onClick={this._onMoreInfo.bind(this, afterHourMessage)}>{enterprise.i18nReservation.resflowconfirmation_0025}</a></span>;
    let wayfindingClass = classNames({
      'wayfinding': true,
      'active': wayfindingData.length > 0
    });

    let googlePath = 'https://www.google.com/maps/dir/current+location/';
    googlePath += activeItem.content.address.street_addresses[0] + ',';
    googlePath += activeItem.content.address.city + ',';
    googlePath += activeItem.content.address.city + ',';
    googlePath += activeItem.content.address.country_subdivision_code + ',';
    googlePath += activeItem.content.address.postal;

    if (activeItem.content.phones) {
      for (let i = 0, len = activeItem.content.phones.length; i < len; i++) {
        if (activeItem.content.phones[i].phone_type === 'OFFICE') {
          phone = activeItem.content.phones[i].phone_number;
        }
      }
    }

    if (location.we_will_pick_you_up) {
      willPickYouUp =
        <a href={'tel:' + phone} className="pick-you-up">{enterprise.i18nReservation.resflowconfirmation_0022}</a>;
    }
    return (
      <div id="tabContent" className="details" role="tabpanel"
           aria-label={enterprise.utilities.capitalize(this.props.activeItem.label)}>
        <div className="date">{activeItem.content.date}</div>
        <div className="time">{activeItem.content.time}</div>
        {willPickYouUp}
        <DetailsMap gps={this.props.activeItem.content.gps} className="location-map" title="location map" alt="directions" />

        <div className={wayfindingClass}>
          <h4>{enterprise.i18nReservation.resflowconfirmation_0029}</h4>
          { wayfindingData.filter( (item) => item.text.length )
                          .map( (item, index) => (
            <div key={index}>
              <img alt={item.text} src={item.path}/>
              <span className="direction">{item.text}</span>
            </div>
          ))}
        </div>

        <div className="location-details">
          <ul>
            <li>
              <h3>{activeItem.content.name}</h3>
            </li>
            <li>
              {activeItem.content.address.street_addresses[0]}
            </li>

            {activeItem.content.address.street_addresses.length > 1 ?
              <li>{activeItem.content.address.street_addresses[1]}</li> : false}

            <li>
              {activeItem.content.address.city}, {activeItem.content.address.country_subdivision_code + ' ' + activeItem.content.address.postal}
            </li>
            <li>
              {phone}
            </li>
            <li>
              <a href={googlePath}
                 target="_blank">{enterprise.i18nReservation.resflowconfirmation_0023}</a>
            </li>
            <li className="padded-item">
              {hoursLabel}
              {locationHours ? locationHours.map((h, i) => <span key={i} className="hours-value">{h.open} - {h.close}</span>) : false}
            </li>
          </ul>
        </div>
      </div>);
  }
}

DetailsTabsContent.displayName = 'DetailsTabsContent';
