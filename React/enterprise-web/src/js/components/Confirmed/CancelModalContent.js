import CancellationController from '../../controllers/CancellationController';
import ModifyController from '../../controllers/ModifyController';
import ConfirmedController from '../../controllers/ConfirmedController';

export default class CancelModalContent extends React.Component {
  constructor(props) {
    super(props);
    this._cancelReservation = this._cancelReservation.bind(this);
    this._keepReservation = this._keepReservation.bind(this);
    this._openPrepayTCModal = this._openPrepayTCModal.bind(this);
    this._closeModal = this._closeModal.bind(this);
  }
  _cancelReservation (event) {
    event.preventDefault();
    this._closeModal();
    ConfirmedController.setLoadingClassName();
    CancellationController.cancelReservation(this.props.confirmationNumber);
  }
  _keepReservation (event) {
    event.preventDefault();
    this._closeModal();
  }
  _openPrepayTCModal (event) {
    event.preventDefault();
    ModifyController.togglePrepayTerms(true);
  }
  _closeModal () {
    ModifyController.toggleCancelReservationModal(false);
  }
  render () {
    const { sessionContractDetails, prepay, cancellationDetails, samePriceRegion } = this.props;
    const showThirdPartyEmail = ConfirmedController.showThirdPartyEmail( sessionContractDetails );
    const cancelDetails = CancellationController.getCancelDetails( cancellationDetails, samePriceRegion );
    const refundAmount = prepay && CancellationController.hasRefund(cancelDetails) ? CancellationController.getRefundAmount(cancelDetails) : false;
    const hasCancelFee = prepay && CancellationController.hasRefund(cancelDetails) ? CancellationController.hasCancelFee(cancelDetails) : false;

    return (
      <div className="cancel-confirmation cf">
        <h2>{i18n('resflowreview_0099')}</h2>

        {prepay ?
          <div className="price_container cf">
            <p>
              {cancelDetails.cancellation_message}
              {cancelDetails.cancellation_message && <br/>}
              <a href="#" className="green" role="button"
                 onClick={this._openPrepayTCModal} onKeyPress={a11yClick(this._openPrepayTCModal)}
              >
                { i18n('prepay_0022') || 'Prepayment Policy Terms & Conditions'}
              </a>
            </p>

            <table className="cancelDetailsTable">
              <tbody>
              <tr>
                <td>{i18n('resflowviewmodifycancel_2008')}</td>
                <td>{cancelDetails.total.format}</td>
              </tr>

              {hasCancelFee &&
              <tr>
                <td>{i18n('resflowviewmodifycancel_0052')}</td>
                <td>-{CancellationController.getCancelFee(cancelDetails)}</td>
              </tr>
              }
              </tbody>

              <tfoot>
              <tr>
                <td> {i18n('resflowviewmodifycancel_0048')} </td>
                <td> {refundAmount} </td>
              </tr>
              </tfoot>
            </table>
          </div>
          :
          <p>{i18n('resflowviewmodifycancel_0054')}</p>
        }
        {showThirdPartyEmail &&
          <div className="reserve-email-notify">
            <i className="icon icon-ico-email-extras"></i>{i18n('resflowcorporate_0081', {ContractName: sessionContractDetails.contract_name})}
          </div>}
        <div className="right cf">
          <button onClick={this._keepReservation}
                  className="btn cancel">{i18n('resflowviewmodifycancel_0055')}</button>
          <button onClick={this._cancelReservation} id="cancelReservationModalButton"
                  className="btn ok">{i18n('resflowviewmodifycancel_0056')}</button>
        </div>
      </div>
    );
  }
}

CancelModalContent.defaultProps = {
  samePriceRegion: true
}

CancelModalContent.displayName = 'CancelModalContent';
