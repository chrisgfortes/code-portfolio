export default class DetailsMap extends React.Component {
  componentWillReceiveProps(nextProps) {
    let mapOptions = {
      zoom: this.props.initialZoom,
      center: this.centerLatLng(nextProps.gps),
      disableDefaultUI: true,
      draggable: false
    };
    // Map
    let map = new google.maps.Map(React.findDOMNode(this), mapOptions);
    let marker = new google.maps.Marker({
      position: this.centerLatLng(nextProps.gps),
      label: 'K',
      map: map
    });
    this.setState({map: map});
  }
  centerLatLng (gps) {
    return new google.maps.LatLng(gps.latitude, gps.longitude);
  }
  render () {
    return (
      <div className="miniMap" />
    );
  }
}

DetailsMap.defaultProps = {
  initialZoom: 12
};

DetailsMap.displayName = 'DetailsMap';
