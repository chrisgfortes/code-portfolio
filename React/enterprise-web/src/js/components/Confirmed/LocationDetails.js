import DetailsTabs from './DetailsTabs';
import DetailsTabsContent from './DetailsTabsContent';
import ConfirmedController from '../../controllers/ConfirmedController';

export default class LocationDetails extends React.Component{
  constructor (props) {
    super(props);
    const locationType = ConfirmedController.getLocationDetails();
    const tabs = [
      {id: 0, label: enterprise.i18nReservation.reservationwidget_0008, content: locationType.pickup},
      {id: 1, label: enterprise.i18nReservation.reservationwidget_0011, content: locationType.dropoff}
    ];
    this.state = {
      tabs: tabs,
      activeTab: tabs[0],
      locationType: locationType
    };
    this.handleTabClick = this.handleTabClick.bind(this);
    this.handleArrowPressed = this.handleArrowPressed.bind(this)
  }
  handleTabClick (item) {
    this.setState({activeTab: item});
  }
  handleArrowPressed (tabIdToFocus) {
    this.setState({activeTab: this.state.tabs[tabIdToFocus]});
  }
  render () {
    const { sessionPickupLocation, sessionDropoffLocation, afterHourMessage, pickupHours, dropoffHours } = this.props;
    return (
      <div className="tab-container location-tabs">
        <DetailsTabs items={this.state.tabs}
              activeItem={this.state.activeTab}
              onTabClick={this.handleTabClick}
              onArrowPressed={this.handleArrowPressed}
              />
        <DetailsTabsContent activeItem={this.state.activeTab}
                     locationType={this.state.locationType}
                     {...{ sessionPickupLocation, sessionDropoffLocation, afterHourMessage, pickupHours, dropoffHours }} />
      </div>
    );
  }
}

LocationDetails.displayName = 'LocationDetails';
