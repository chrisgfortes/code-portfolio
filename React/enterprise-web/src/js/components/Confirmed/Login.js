import Error from '../Errors/Error';
import Validator from '../../utilities/util-validator';
import LoginController from '../../controllers/LoginController';
import ReservationFlowModelController from '../../controllers/ReservationFlowModelController';
import classNames from 'classnames';

export default class Login extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      epUsername: null,
      epPassword: null,
      epRememberMe: false,
      ecUsername: null,
      ecPassword: null,
      ecRememberMe: false,
      brand: this.props.brand ? this.props.brand.toUpperCase() : 'EP'
    }
    this.validator = new Validator(this, this.fieldMap);
    this.fieldMap = this.fieldMap.bind(this);
    this._handleInputChange = this._handleInputChange.bind(this);
    this._onRememberMe = this._onRememberMe.bind(this);
    this._login = this._login.bind(this);
    this._handleKeyUp = this._handleKeyUp.bind(this);
    this.processLogin = this.processLogin.bind(this);
    this._onForgot = this._onForgot.bind(this);
    this._toggleBrand = this._toggleBrand.bind(this);
    this._onConfirmExit = this._onConfirmExit.bind(this);
  }
  fieldMap() {
    return {
      refs: {
        epUsername: this.epUsername,
        epPassword: this.epPassword,
        ecUsername: this.ecUsername,
        ecPassword: this.ecPassword
      },
      value: {
        epUsername: this.epUsername.value,
        epPassword: this.epPassword.value,
        ecUsername: this.ecUsername.value,
        ecPassword: this.ecPassword.value
      },
      schema: {
        epUsername: 'string',
        epPassword: 'password',
        ecUsername: 'string',
        ecPassword: 'password'
      }
    };
  }
  _handleInputChange(attribute, event) {
    this.setState(
      {
        [attribute]: event.target.value
      }
    );
    this.validator.validate(attribute, event.target.value);
  }
  _login(brand) {
    let handle = brand === 'EP' ? 'ep' : 'ec',
      username = this.validator.validate(handle + 'Username', this[handle + 'Username'].value),
      password = this.validator.validate(handle + 'Password', this[handle + 'Password'].value);

    if (password && username && !this.state.submitLoading) {
      let params = {
        username: this[handle + 'Username'].value,
        password: this[handle + 'Password'].value,
        rememberMe: this.state[handle + 'RememberMe'],
        brand: this.state.brand,
        retrieve: true
      };
      this.processLogin(params);
    }
  }
  processLogin(params) {
    this.setState({submitLoading: true});

    LoginController.login(params).then((response) => {
      this.setState({submitLoading: false});
      if (response.data.profile) {
        ReservationFlowModelController.setDeeplinkModal(null);
      }
    });
  }
  _onForgot() {
    window.open(this.props.supportLinks ? this.props.supportLinks.forgot_password_url : 'https://legacy.enterprise.com/car_rental/enterprisePlusForgotPassword.do');
  }
  _handleKeyUp(brand, event) {
    if (event.keyCode === 13) {
      this._login(brand);
    }
  }
  _toggleBrand(section) {
    ReservationFlowModelController.clearErrorsForComponent('loginWidget');
    this.setState({
      brand: section
    });
  }
  _onRememberMe(brand, event) {
    this.setState({
      [brand]: event.target.checked
    });
  }
  _onConfirmExit() {
    window.location = '/';
  }
  render() {
    let ep = this.state.brand === 'EP',
      ec = this.state.brand === 'EC',
      epContainer = classNames({
        'login-fields': true,
        'ep': true,
        'active': ep
      }),
      ecContainer = classNames({
        'login-fields': true,
        'ec': true,
        'active': ec
      });

    return (
      <div className="login">
        <div className="header-container">
          {ep ?
            <div>
              <i className="icon icon-eplus-logo-black"/>

              <h2>{i18n('loyaltysignin_0001')}</h2>
            </div>
            :
            <h3 onClick={this._toggleBrand.bind(this, 'EP')}>{i18n('loyaltysignin_0001')}</h3>}

        </div>

        <div className={epContainer}>
          <Error errors={this.props.errors} type="GLOBAL"/>

          <label htmlFor="ep-login">{i18n('loyaltysignin_0003')}</label>
          <input onChange={this._handleInputChange.bind(this, 'epUsername')}
                 onKeyUp={this._handleKeyUp.bind(this, 'EP')}
                 type="text"
                 name="ep-email"
                 ref={c => this.epUsername = c}
                 id="epLogin"/>

          <label htmlFor="ep-password">{i18n('loyaltysignin_0004')}</label>
          <input onChange={this._handleInputChange.bind(this, 'epPassword')}
                 onKeyUp={this._handleKeyUp.bind(this, 'EP')}
                 type="password"
                 name="ep-password"
                 ref={c => this.epPassword = c}
                 id="epPassword"/>

          <label className="ep-remember"
                 htmlFor="ep-remember">
            <input onChange={this._onRememberMe.bind(this, 'epRememberMe')}
                   checked={this.state.epRememberMe} type="checkbox" id="ep-remember"
                   name="ep-remember"/>
            {i18n('resflowcorporate_4002')}
          </label>

          <div className={this.state.submitLoading ? 'loading' : false}/>
          {this.state.submitLoading ? false :
            <div className="btn"
                 onClick={this._login.bind(this, 'EP')}>{i18n('resflowreview_0080')}</div>}
          <div className="forgot"
               onClick={this._onForgot}>{i18n('loyaltysignin_0019')}</div>
        </div>
        <div className="divider">
          <span className="strike-through"/>
          <i>{i18n('fedexcustompath_0002') || 'OR'}</i>
          <span className="strike-through"/>
        </div>
        <div className="header-container">


          {ec ?
            <div>
              <i className="icon icon-brand-national"/>

              <h2>{i18n('resflowcorporate_4005')}</h2>
            </div> :
            <h3 onClick={this._toggleBrand.bind(this, 'EC')}>{i18n('resflowcorporate_4005')}</h3> }

        </div>

        <div className={ecContainer}>

          <Error errors={this.props.errors} type="GLOBAL"/>

          <label htmlFor="ec-login">{i18n('resflowcorporate_4003')}</label>
          <input onChange={this._handleInputChange.bind(this, 'ecUsername')}
                 onKeyUp={this._handleKeyUp.bind(this, 'EC')}
                 type="text"
                 name="ec-email"
                 ref={c => this.ecUsername = c}
                 id="ecLogin"/>

          <label htmlFor="ec-password">{i18n('loyaltysignin_0004')}</label>
          <input onChange={this._handleInputChange.bind(this, 'ecPassword')}
                 onKeyUp={this._handleKeyUp.bind(this, 'EC')}
                 type="password"
                 name="ec-password"
                 ref={c => this.ecPassword = c}
                 id="ecPassword"/>

          <label className="ec-remember"
                 htmlFor="ec-remember">
            <input onChange={this._onRememberMe.bind(this, 'ecRememberMe')}
                   checked={this.state.epRememberMe} type="checkbox" id="ec-remember"
                   name="ec-remember"/>
            {i18n('resflowcorporate_4002')}
          </label>

          <div className={this.state.submitLoading ? 'loading' : false}/>
          {this.state.submitLoading ? false :
            <div className="btn"
                 onClick={this._login.bind(this, 'EC')}>{i18n('resflowreview_0080')}</div>}
        </div>
        <button onClick={this._onConfirmExit}
                className="btn confirm-exit">{i18n('reservationnav_0036')}</button>
      </div>
    );
  }
}

Login.displayName = "Login";

