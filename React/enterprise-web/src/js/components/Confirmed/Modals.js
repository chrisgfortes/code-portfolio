import {branch as BaobabReactMixinBranch} from 'baobab-react/mixins';
import ReservationCursors from '../../cursors/ReservationCursors';
import VerificationController from '../../controllers/VerificationController';
import Login from './Login';
import ReviewModals from '../Verification/ReviewModal';
import MismatchAcct from './MismatchAcct';
import GlobalModal from '../Modal/GlobalModal';

const Modals = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    deepLink: ReservationCursors.deepLink,
    deepLinkView: ReservationCursors.deepLinkView,
    cannotModifyModal: ReservationCursors.cannotModifyModal,
    destinationPriceInfoModal: ReservationCursors.destinationPriceInfoModal,
    verification: ReservationCursors.verification,
    supportLinks: ReservationCursors.supportLinks,
    loginWidgetErrors: ReservationCursors.loginWidgetErrors,
    loggedIn: ReservationCursors.userLoggedIn,
    messages: ReservationCursors.messages
  },
  render () {
    const { deepLink, deepLinkView, verification, cannotModifyModal, destinationPriceInfoModal,
            supportLinks, loginWidgetErrors, loggedIn, messages } = this.state;
    const authStatus = VerificationController.requireAuthentication( deepLink, loggedIn );
    const wrongAuth = VerificationController.determineRetrieveAuth( loggedIn, messages );


    return (
      <div>
        <ReviewModals {...{verification}} />

        {(authStatus.requireAuth && !wrongAuth) || deepLinkView.modal === 'login' ?
          <GlobalModal
            active
            hideX
            containerClass="mask"
            disableBlur
            header={i18n('resflowreview_0080')}
            content={<Login brand="EP"
                            supportLinks={supportLinks}
                            loginWidgetErrors={loginWidgetErrors} />}/> : false
        }
        {wrongAuth && !deepLinkView.modal ?
          <GlobalModal
            active
            hideX
            containerClass="mask"
            disableBlur
            header={"Wrong Account"}
            content={<MismatchAcct />}/> : false
        }
        {!!destinationPriceInfoModal &&
        <GlobalModal active={destinationPriceInfoModal}
                     header="Enterprise"
                     content={i18n('resflowcarselect_0105')}
                     cursor={ReservationCursors.model.concat('destinationPriceInfoModal')}/>
        }
        {!!cannotModifyModal &&
        <GlobalModal active={cannotModifyModal}
                     header="Enterprise"
                     content={i18n('resflowviewmodifycancel_0081')}
                     cursor={ReservationCursors.model.concat('cannotModifyModal')}/>
        }
      </div>
    );
  }
});

module.exports = Modals;
