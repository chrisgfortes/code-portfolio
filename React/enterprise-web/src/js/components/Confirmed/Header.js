import ReservationCursors from '../../cursors/ReservationCursors';
import CancellationController from '../../controllers/CancellationController';
import ReservationFlowModelController from '../../controllers/ReservationFlowModelController';
import ModifyController from '../../controllers/ModifyController';
import PricingController from '../../controllers/PricingController';
import Modal from '../Modal/GlobalModal';
import Error from '../Errors/Error';
import CancelModal from './CancelModal';
import GlobalModal from '../Modal/GlobalModal';
import ModifyConfirmationModalContent from '../Modify/ModifyConfirmationModalContent';

export default class Header extends React.Component {
  constructor (props) {
    super(props);
    this.state = { disclaimerModal: false }
    if (this.props.prepay) {
      if (this.props.cancellationDetails) {
        // @todo: we really, really, really, should not be doing this, and we should not be doing it here
        CancellationController.augmentCancelDetailsWithTotal(this.props.cancellationDetails);
      }
    }
  }
  componentDidMount () {
    if (!this.props.print) {
      CancellationController.getReservationEligibility();
    }
  }
  render () {
    const { print, prepay, cancellationDetails, cancelReservationModal, errors, confirmationNumber,
      cancelRebook, modifyModalOpen, header, verification, closeCancelModal, sessionContractDetails } = this.props;

    const { disclaimerModal } = this.state;
    const modify = ReservationFlowModelController.getIsModifiedFlag() ? true : false;
    const headerStrLineOne = i18n('reservationwidget_0019') + ': ';

    return (
      <header className="confirmed cf">
        <img className="car-image" src={header.image} alt=""/>

        <div className="header-info cf">
          <h1>
            <i className="icon icon-alert-success"/>
            {modify ?
              <span><span className="black-text">{i18n('resflowconfirmation_0001')}</span>&nbsp;<span
                className="green-text">{i18n('resflowviewmodifycancel_0044')}</span></span>
              :
              <span><span className="black-text">{i18n('resflowconfirmation_0001')}</span>&nbsp;<span
                className="green-text">{i18n('resflowconfirmation_0033')}</span></span>
            }
          </h1>

          <div className="sub-header">
            <h4><span className="conf-string">{headerStrLineOne}</span><span
              className="conf-number">{header.confirmationNumber}</span></h4>
          </div>
          <div className="btn-grp cf">
            <Error errors={errors} type="GLOBAL"/>
          </div>
        </div>

        {!print && modifyModalOpen &&
        <GlobalModal
          active
          header={cancelRebook ? i18n('prepay_1010') : i18n('resflowreview_0098')}
          content={<ModifyConfirmationModalContent confirm={ModifyController.modifyConfirmation} cancelRebook={cancelRebook} />}
          cursor={['view', 'modify', 'modal']}/>
        }

        {!print && cancelReservationModal &&
        <GlobalModal active
                     header=" "
                     close = {closeCancelModal}
                     content={
                      <CancelModal
                       showPrepayTerms={verification.showPrepayTerms}
                       isSamePriceRegion={PricingController.isSamePricingRegion(cancellationDetails)}
                        {...{verification, cancellationDetails, prepay, confirmationNumber, sessionContractDetails}}/>
                     }
                     cursor={ReservationCursors.cancelReservationModal}/>
        }
        {!print && disclaimerModal ?
          <Modal active
                 close={()=>this.setState({disclaimerModal: false})}
                 header={' '}
                 content={i18n('resflowviewmodifycancel_0081')}/> : false}
      </header>
    );
  }
}

Header.displayName = 'Header';
