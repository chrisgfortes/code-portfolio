export default function FauxTableRow ({classes, label, rate, value, id, headers}) {

  return (
    <tr className={"faux-tr cf" + (classes ? (' ' + classes) : '')}>
        <th id={id} className={"faux-td row-label" + (!label ? ' empty' : '')}>{label}</th>
        <th id={id} className={"faux-td row-rate" + (!rate ? ' empty' : '')}>{rate}</th>
        <td headers={headers} className={"faux-td row-value row-amount" + (!value ? ' empty' : '')}>{value}</td>
    </tr>
  );
}

FauxTableRow.displayName = 'FauxTableRow';



