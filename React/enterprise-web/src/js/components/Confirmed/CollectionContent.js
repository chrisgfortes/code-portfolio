import BranchInformation from './BranchInformation';

export default function CollectionInformation ({ imgSrc, googlePath, data, location, header, time }) {
  return (
    <div>
    { data ?
        <div>
          <div className="date">{time.format('LL')}</div>
          <div className="time">{time.format('LT')}</div>
          <h2>{header}</h2>
          <dl>
            {data.address.street_addresses.length > 0 ? <dt>{enterprise.i18nReservation.eplusaccount_0049}</dt> : false}
            {data.address.street_addresses.length > 0 ? <dd>{data.address.street_addresses[0]}</dd> : false}
            {data.address.city ? <dt>{enterprise.i18nReservation.resflowreview_0065}</dt> : false}
            {data.address.city ? <dd>{data.address.city}</dd> : false}
            {data.address.postal ? <dt>{enterprise.i18nReservation.resflowcorporate_0205}</dt> : false}
            {data.address.postal ? <dd>{data.address.postal}</dd> : false}
            {data.phone.phone_number ? <dt>{enterprise.i18nReservation.resflowreview_0010}</dt> : false}
            {data.phone.phone_number ? <dd>{data.phone.phone_number}</dd> : false}
            {data.comments ? <dt>{enterprise.i18nReservation.resflowcorporate_0040}</dt> : false}
            {data.comments ? <dd>{data.comments}</dd> : false}
          </dl>

          <BranchInformation hideMap={!!data} {...{ location, imgSrc, googlePath }} />
        </div>
      :
        <div>
          <div className="date">{time.format('LL')}</div>
          <div className="time">{time.format('LT')}</div>

          <BranchInformation {...{ location, imgSrc, googlePath }} />
        </div>
      }
    </div>
  );
}

CollectionInformation.displayName = 'CollectionInformation';
