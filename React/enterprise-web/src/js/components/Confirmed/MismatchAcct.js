const ReservationCursors = require('../../cursors/ReservationCursors');
const BaobabReactMixinBranch = require('baobab-react/mixins').branch;
const DeepLinkActions = require('../../actions/DeepLinkActions');

const MismatchAcct = React.createClass({
  mixins: [
    React.addons.PureRenderMixin,
    BaobabReactMixinBranch
  ],
  cursors: {
    session: ReservationCursors.reservationSession,
    errors: ['view', 'loginWidget', 'errors']
  },
  _onExit() {

  },
  _onChangeLogin() {
    DeepLinkActions.setModal('login');
  },
  render() {
    return (
      <div className="mismatch-auth">
        <div>The reservation you are trying to access is associated with another account.</div>
        <div>Would you like to login into a different account?</div>
        <div clasName="action-container">
          <button className="btn" onClick={this._onExit}>No, Exit</button>
          <button className="btn" onClick={this._onChangeLogin}>Yes, Change Log In</button>
        </div>
      </div>
    );
  }
});

module.exports = MismatchAcct;

