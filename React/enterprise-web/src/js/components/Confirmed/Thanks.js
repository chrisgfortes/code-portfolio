import ConfirmedController from '../../controllers/ConfirmedController';
import {PAGEFLOW} from '../../constants/';

function onPrintClick () {
  window.print();
}

export default function Thanks ({sessionContractDetails, viewCarClassDetailsStatus, equipmentExtras, insuranceExtras,
                                  componentToRender, sessionPickupLocationWithDetail}) {

  const thanks = ConfirmedController.getConfirmationThanks();
  const requestInfo = ConfirmedController.getRequestInfo( viewCarClassDetailsStatus, equipmentExtras,
                                                              insuranceExtras, componentToRender, sessionPickupLocationWithDetail, thanks );
  const onRequestVehicle = requestInfo.onRequestVehicle;
  const onRequestExtra = requestInfo.onRequestExtra;
  const vehicleOnRequestHeader = requestInfo.vehicleOnRequestHeader;

  return (
    <div className="thank-you">
      <button className="inverse" onClick={onPrintClick}>
        <i className="icon icon-confirmation-print"/>{enterprise.i18nReservation.resflowconfirmation_0002}
      </button>
        {!!((onRequestVehicle || onRequestExtra.length > 0) && componentToRender !== PAGEFLOW.CANCELLED) &&
          <div className="reserve-email-notify">
            <i className="icon icon-alert-caution"></i>{i18n('resflowconfirmation_0034') || 'Please note, you must speak to an Enterprise employee before picking up your vehicle.'}
          </div>
        }
        <h2>{thanks.header}</h2>
        {vehicleOnRequestHeader}
        {thanks.headerBody}
        {!!thanks.secondaryHeader &&
        <div className="info-block">
        <h2>{thanks.secondaryHeader}</h2>
         <p>{thanks.body}</p>
        {thanks.phone ? (<a href={'tel:' + thanks.phone}>{thanks.locationInfo}</a>) : false}
      </div>}
      {ConfirmedController.showThirdPartyEmail( sessionContractDetails ) &&
      <div className="reserve-email-notify">
        <i className="icon icon-ico-email-extras"></i>
          {(componentToRender !== PAGEFLOW.CANCELLED) && (<span>{i18n('resflowcorporate_0080', {ContractName: sessionContractDetails.contract_name})}</span>)}
          {(componentToRender === PAGEFLOW.CANCELLED) && (<span>{i18n('resflowcorporate_0081', {ContractName: sessionContractDetails.contract_name})}</span>)}
      </div>}
    </div>
  );
}

Thanks.displayName = 'Thanks';
