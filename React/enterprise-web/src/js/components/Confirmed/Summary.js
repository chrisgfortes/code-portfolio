export default function Summary ({summary}) {
  return (
    <div className="summary cf">
      <div className="pickup cf">
        <div className="summary-location">
          <div id="summary-pickup-title">{enterprise.i18nReservation.reservationwidget_0008}</div>
          <div className="summary-locationInfo" aria-labelledby="summary-pickup-title">{summary.location.pickup}</div>
        </div>
        <div className="summary-date-and-time">
          <div className="summary-date">
            <div className="date-label" id="summary-pickup-dateTitle">{enterprise.i18nReservation.reservationwidget_0009}</div>
            <div className="summary-dateInfo" aria-labelledby="summary-pickup-dateTitle">{summary.date.pickup}</div>
          </div>
          <div className="summary-time">
            <div className="date-label" id="summary-pickup-timeTitle">{enterprise.i18nReservation.reservationwidget_0010}</div>
            <div className="summary-timeInfo" aria-labelledby="summary-pickup-timeTitle">{summary.time.pickup}</div>
          </div>
        </div>
      </div>
      <div className="dropoff cf">
        <div className="summary-location">
          <div id="summary-dropoff-title">{enterprise.i18nReservation.reservationwidget_0011}</div>
          <div className="summary-locationInfo" aria-labelledby="summary-dropoff-title">{summary.location.dropoff}</div>
        </div>
        <div className="summary-date-and-time">
          <div className="summary-date">
            <div className="date-label" id="summary-dropoff-dateTitle">{enterprise.i18nReservation.reservationwidget_0009}</div>
            <div className="summary-dateInfo" aria-labelledby="summary-dropoff-dateTitle">{summary.date.dropoff}</div>
          </div>
          <div className="summary-time">
            <div className="date-label" id="summary-dropoff-timeTitle">{enterprise.i18nReservation.reservationwidget_0010}</div>
            <div className="summary-timeInfo" aria-labelledby="summary-dropoff-timeTitle">{summary.time.dropoff}</div>
          </div>
        </div>
      </div>
      <div className="circle">
        →
      </div>
    </div>
  );
}

Summary.displayName = 'Summary';


