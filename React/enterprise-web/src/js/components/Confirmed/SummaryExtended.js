import ConfirmedController from '../../controllers/ConfirmedController';
import LocationBlock from './LocationBlock';

function getWayfindingData(type, sessionPickupLocation, sessionDropoffLocation) {
  return type === 'pickup' ? sessionPickupLocation.wayfindings : sessionDropoffLocation.wayfindings;
}

export default class SummaryExtended extends React.Component {
  constructor(props) {
    super(props);
  }
  render () {
    const { vehicleLogistics, sessionPickupLocation, sessionDropoffLocation, pickupHours, dropoffHours } = this.props;
    if (!sessionPickupLocation || !sessionDropoffLocation) {
      return false;
    }
    const summary = ConfirmedController.getSummary();
    const sameLocation = sessionPickupLocation.id === sessionDropoffLocation.id &&
                         !vehicleLogistics;
    const pickupInfo = vehicleLogistics ? vehicleLogistics.delivery_info : null;
    const dropoffInfo = vehicleLogistics ? vehicleLogistics.collection_info : null;
    const pickupLabel = pickupInfo ? i18n('resflowcorporate_0003') : i18n('reservationwidget_0008');
    const dropoffLabel = dropoffInfo ? i18n('resflowcorporate_0004') : i18n('reservationwidget_0011');

    return (
      <div className={`summary-extended ${sameLocation ? 'same-location' : ''}`}>
        <div className="summary-location-blocks cf">
            <LocationBlock type="pickup"
                           {...{summary, vehicleLogistics}}
                           label={pickupLabel}
                           locationHours={pickupHours}
                           deliveryInfo={pickupInfo}
                           sessionType={sessionPickupLocation}
                           wayfindingData={getWayfindingData('pickup', sessionPickupLocation, sessionDropoffLocation)} />
          <div className="circle">
            →
          </div>
            <LocationBlock type="dropoff"
                           {...{summary}}
                           label={dropoffLabel}
                           locationHours={dropoffHours}
                           deliveryInfo={dropoffInfo}
                           sessionType={sessionDropoffLocation}
                           wayfindingData={getWayfindingData('dropoff', sessionPickupLocation, sessionDropoffLocation)} />
        </div>
        {!vehicleLogistics && sameLocation && sessionPickupLocation.wayfindings.length &&
        <div className="summary-wayfinding">
          <div className="summary-wayfinding-title">{i18n('resflowconfirmation_0029')}</div>
          <ul>
            { sessionPickupLocation.wayfindings.filter( w => w.text.length )
                                               .map( (w, i) => <li key={i}>{w.text}</li> )
            }
          </ul>
        </div>
        }
      </div>
    );
  }
}

SummaryExtended.displayName = 'SummaryExtended';
