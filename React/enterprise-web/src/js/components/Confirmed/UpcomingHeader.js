import ReservationCursors from '../../cursors/ReservationCursors';
import CancellationController from '../../controllers/CancellationController';
import ModifyController from '../../controllers/ModifyController';
import ConfirmedController from '../../controllers/ConfirmedController';
import PricingController from '../../controllers/PricingController';
import Error from '../Errors/Error';
import CancelModal from './CancelModal';
import GlobalModal from '../Modal/GlobalModal';
import ModifyConfirmationModalContent from '../Modify/ModifyConfirmationModalContent';

export default class UpcomingHeader extends React.Component {
  constructor (props) {
    super(props);
    this._viewDifferentReservationClick = this._viewDifferentReservationClick.bind(this);
    if (this.props.prepay) {
      if (this.props.cancellationDetails) {
        // @todo: we really, really, really, should not be doing this, and we should not be doing it here
        CancellationController.augmentCancelDetailsWithTotal(this.props.cancellationDetails);
      }
    }
  }
  _viewDifferentReservationClick (event) {
    event.preventDefault();
    ConfirmedController.viewDifferentReservation(this.props.profile);
  }
  _modifyReservation (event) {
    event.preventDefault();
    //TODO: Logic to test is reservation is modify-able
    ModifyController.toggleConfirmationModal(true);
  }
  _cancelReservation (event) {
    event.preventDefault();
    ModifyController.toggleCancelReservationModal(true);
  }
  render () {
    const { prepay, cancellationDetails, cancelReservationModal, errors, confirmationNumber,
      cancelRebook, modifyModalOpen, eligibility, header, dateStr, allowModify, verification, closeCancelModal } = this.props;

    return (
      <header className="confirmed modify cf">
        <a onClick={this._viewDifferentReservationClick} href="#" className="grn-txt"><i
          className="icon icon-nav-carrot-left-green"></i> {i18n('eplusenrollment_0204')}</a>
        <img className="car-image" src={header.image} alt=""/>

        <h1>{dateStr}</h1>

        <h4><span className="conf-string">{i18n('reservationwidget_0019')}: </span><span
          className="conf-number">{header.confirmationNumber}</span></h4>

        <div className="btn-grp">
          <Error errors={errors} type="GLOBAL"/>
          {eligibility && eligibility.cancel_reservation &&
          <a href="#" onClick={this._cancelReservation} id="cancelReservationDetailsPage"
             className="btn cancel">{i18n('resflowreview_0099')}</a>
          }
          {allowModify ?
            <a href="#modify" onClick={this._modifyReservation} id="modifyReservationDetailsPage"
               className="btn ok">{i18n('resflowreview_0098')}</a>
            :
            <span className="modify-unavailable-container"><span className="modify-unavailable">{i18n('prepay_0043') || 'MODIFY UNAVAILABLE'}</span>
              <span className="tooltip-v2" tabIndex="0" aria-describedby="modify-unavailable-content">
              <i className="icon icon-icon-info-green"></i>
              <span id="modify-unavailable-content" className="tooltip" role="tooltip">{i18n('prepay_0044') || 'Modification is unavailable for your rental. Please cancel and re-book if you need to make changes (this may affect your rates).'}
              </span></span>
            </span>
          }
        </div>
        {modifyModalOpen &&
          <GlobalModal
            active
            header={cancelRebook ? i18n('prepay_1010') : i18n('resflowreview_0098')}
            content={<ModifyConfirmationModalContent
              confirm={ModifyController.modifyConfirmation}
              cancelRebook={cancelRebook} />}
            cursor={ReservationCursors.modifyModalOpen}/>
        }
        {cancelReservationModal &&
          <GlobalModal
            active
            header=" "
            close = {closeCancelModal}
            content={
              <CancelModal
                showPrepayTerms={verification.showPrepayTerms}
                isSamePriceRegion={PricingController.isSamePricingRegion(cancellationDetails)}
                {...{verification, cancellationDetails, prepay, confirmationNumber}}/>}
            cursor={ReservationCursors.cancelReservationModal}/>
        }
      </header>
    );
  }
}

UpcomingHeader.displayName = 'UpcomingHeader';

