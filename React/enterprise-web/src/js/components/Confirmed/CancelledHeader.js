export default function CancelledHeader ({header}) {
  return (
    <header className="confirmed cf">
      <img className="car-image" src={header.image} alt=""/>

      <div className="header-info cf">
        <h1>
          <i className="icon icon-alert-success"/>
          <span><span className="black-text">{i18n('resflowconfirmation_0001')}</span>&nbsp;<span
            className="green-text">{i18n('resflowviewmodifycancel_0057')}</span></span>
        </h1>

        <div className="sub-header">
          {/*<h4>{i18n('resflowviewmodifycancel_0024').replace("#{confirmationNumber}", header.confirmationNumber)}</h4>*/}
          <h4><span className="conf-string">{i18n('reservationwidget_0019')}: </span><span
            className="conf-number">{header.confirmationNumber}</span></h4>
        </div>
      </div>
    </header>
  );
}

CancelledHeader.displayName = 'CancelledHeader';

