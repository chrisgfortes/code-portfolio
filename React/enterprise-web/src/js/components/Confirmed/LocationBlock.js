export default function LocationBlock ({type, summary, label, locationHours, vehicleLogistics,
                                        deliveryInfo, sessionType, wayfindingData}) {

  const hours = [
    !!locationHours && locationHours.open,
    !!locationHours && locationHours.close
  ].filter(x => x);

  return (
    <div className={`summary-block ${type}`}>
      <div className="summary-block-title">
        {label}
      </div>

      <div className="summary-location cf">
        <div className="summary-date-and-time">
          <div className="summary-date">
            <div className="summary-dateInfo">{summary.date[type]}</div>
          </div>
          <div className="summary-time">
            <div className="summary-timeInfo">{summary.time[type]}</div>
          </div>
        </div>
        { (!vehicleLogistics || !deliveryInfo) &&
          <div className="summary-locationInfo">{summary.location[type]}</div>
        }
      </div>

      { (!vehicleLogistics || !deliveryInfo) &&
        <div className="summary-address">
          {!!hours.length &&
            <div className="summary-hours-open">
              <div className="summary-hours-open-title">
                {type === 'pickup' ? i18n('resflowconfirmation_0024') : i18n('resflowconfirmation_0028')}
              </div>
              {hours.join(' - ')}
            </div>
          }
          <div>{sessionType.address.street_addresses.filter(x=>x).join(', ')}</div>
          <div>{sessionType.address.city}, {sessionType.address.country_subdivision_code} {sessionType.address.postal}</div>
          <div>{(sessionType.phones || []).map(phone => phone.phone_number).join(', ')}</div>
        </div>
      }

      { !!vehicleLogistics && !!deliveryInfo &&
        <div className="summary-address-dc cf">
          <div className="dc-label">{i18n('eplusaccount_0049')}</div>
          <div className="dc-value">{deliveryInfo.address.street_addresses.join(', ')}</div>

          <div className="dc-label">{i18n('resflowreview_0065')}</div>
          <div className="dc-value">
            {[
              deliveryInfo.address.city,
              deliveryInfo.address.country_subdivision_code,
              deliveryInfo.address.country_code
            ].filter(x=>x).join(', ')}
          </div>

          <div className="dc-label">{i18n('resflowreview_0068')}</div>
          <div className="dc-value">{deliveryInfo.address.postal}</div>

          { !!_.get(deliveryInfo.phone, 'phone_number') &&
            <div className="dc-label">{i18n('resflowreview_0010')}</div>
          }
          { !!_.get(deliveryInfo.phone, 'phone_number') &&
            <div className="dc-value">{_.get(deliveryInfo.phone, 'phone_number')}</div>
          }

          { !!deliveryInfo.comments &&
            <div className="dc-label">
              { type === 'pickup' ? i18n('resflowcorporate_0040')
                                  : i18n('resflowcorporate_0045')
              }
            </div>
          }
          { !!deliveryInfo.comments &&
            <div className="dc-value">{deliveryInfo.comments}</div>
          }
        </div>
      }



      { (!vehicleLogistics || !deliveryInfo) && wayfindingData.length > 0 &&
        <div className="summary-wayfinding">
          <div className="summary-wayfinding-title">{i18n('resflowconfirmation_0029')}</div>
          <ul>
            { wayfindingData.filter( w => w.text.length )
                            .map( (w, i) => <li key={i}>{w.text}</li> )
            }
          </ul>
        </div>
      }
    </div>
  );
}

LocationBlock.displayName = 'LocationBlock';
