import {getDNRMessage} from '../../controllers/LoginController';

export default function DNR ({domain}) {
  return (
    <div>
      <i className="icon icon-alert-caution-yellow"></i>
      <div className="content-container">
        <div className="header-message">
          {getDNRMessage(domain)}
        </div>
      </div>
    </div>
  );
}

DNR.displayName = 'DNR';

