import {branch as BaobabReactMixinBranch} from 'baobab-react/mixins';
import ReservationCursors from '../../cursors/ReservationCursors';
import Header from './Header';
import SummaryExtended from './SummaryExtended';
import RateTaxesFees from '../PricingTable/RateTaxesFees';
import PersonalInformation from './PersonalInformation';
import ConfirmedController from '../../controllers/ConfirmedController';

const ConfirmedPrint = React.createClass({
  mixins: [
    React.addons.PureRenderMixin,
    BaobabReactMixinBranch
  ],
  cursors: {
    additionalInformationSaved: ReservationCursors.additionalInformationSaved,
    airlineInformation: ReservationCursors.airlineInformation,
    cancellationDetails: ReservationCursors.cancellationDetails,
    cancelReservationModal: ReservationCursors.cancelReservationModal,
    cancelRebook: ReservationCursors.cancelRebook,
    chargeType: ReservationCursors.chargeType,
    confirmationNumber: ReservationCursors.confirmationNumber,
    currentHash: ReservationCursors.currentHash,
    contractDetails: ReservationCursors.contractDetails,
    codeApplicable: ReservationCursors.codeApplicable,
    driverInformation: ReservationCursors.driverInformation,
    eligibility: ReservationCursors.reservationEligibility,
    errors: ReservationCursors.confirmationErrors,
    modifyModalOpen: ReservationCursors.modifyModalOpen,
    prepay: ReservationCursors.prepay,
    profile: ReservationCursors.profile,
    renterAge: ReservationCursors.resRequestRenterAge,
    selectedCar: ReservationCursors.selectedCar,
    sessionContractDetails: ReservationCursors.sessionContractDetails,
    sessionPickupLocation: ReservationCursors.sessionPickupLocation,
    sessionDropoffLocation: ReservationCursors.sessionDropoffLocation,
    sessionPickupTime: ReservationCursors.sessionPickupTime,
    vehicleLogistics: ReservationCursors.vehicleLogistics,
    verification: ReservationCursors.verification,
    pickupHours: ReservationCursors.pickupHours,
    dropoffHours: ReservationCursors.dropoffHours
  },
  render () {
    const { eligibility, chargeType, prepay, cancellationDetails, cancelReservationModal, errors,
            confirmationNumber, sessionContractDetails, contractDetails, cancelRebook, modifyModalOpen,
            profile, verification, sessionDropoffLocation, sessionPickupLocation, vehicleLogistics,
            pickupHours, dropoffHours, airlineInformation, selectedCar, additionalInformationSaved,
            driverInformation, renterAge, sessionPickupTime } = this.state;

    if (this.state.currentHash !== 'confirmed' || !selectedCar) {
      return false;
    }
    let isContractPromotion = (_.get(contractDetails, 'contract_type') || '').toLowerCase() === 'promotion';
    let isContractCorporate = (_.get(contractDetails, 'contract_type') || '').toLowerCase() === 'corporate';
    let couponInfoHeading = i18n('reservationwidget_0039');
    let codeApplicableMsg = '';

    //Adjust the coupon code section heading based on if a contract is set & contract type.
    if (isContractPromotion) {
      couponInfoHeading = i18n('resflowcorporate_4016');
      codeApplicableMsg = '(' +
        (this.state.codeApplicable ?
            i18n('promotionemail_0003') :
            i18n('promotionemail_0019')
        ) +
        ')';
    } else if (isContractCorporate) {
      couponInfoHeading = i18n('resflowcorporate_4015');
      codeApplicableMsg = '(' +
        (this.state.codeApplicable ?
            i18n('resflowcarselect_8011') :
            i18n('promotionemail_0021')
        ) +
        ')';
    }

    //header vars
    const header = ConfirmedController.getConfirmationHeader();
    const dateStr = ConfirmedController.getDetailDate(sessionPickupTime);
    const allowModify = ConfirmedController.allowModify( eligibility, chargeType );

    return (!!selectedCar &&
      <div className="confirmed-print cf">
        <Header print {...{header, prepay, cancellationDetails, cancelReservationModal, errors, confirmationNumber, sessionContractDetails,
          cancelRebook, modifyModalOpen, eligibility, chargeType, profile, dateStr, allowModify, verification}}
                closeCancelModal={this.closeCancelModal} />

        <SummaryExtended {...{ vehicleLogistics, sessionPickupLocation, sessionDropoffLocation, pickupHours, dropoffHours }} />

        <RateTaxesFees title={i18n('resflowreview_0302')} />
        <aside>

          <div className="checklist">
            <h2>{i18n('resflowconfirmation_0030')}</h2>
            <ul>
              {enterprise.reservation.checklist.map((obj, index) =>
                <li key={index}>{obj.listitem}</li>
              )}
            </ul>
          </div>

          <table className="confirmed-personal">
            <PersonalInformation {...{driverInformation, renterAge}}/>
          </table>

          {!!_.get(airlineInformation) &&
            <div className="airlineinformation cf">
              <h2>{i18n('resflowreview_0057')}</h2>
              <div className="aside-label">{i18n('resflowreview_0060')}:</div>
              <div className="aside-value">{airlineInformation.description}</div>
              <div className="aside-label">{i18n('resflowreview_0061')}:</div>
              <div className="aside-value">{airlineInformation.flight_number}</div>
            </div>
          }

          {!!_.get(contractDetails) &&
            <div className="account-details cf">
              <h2>{couponInfoHeading}</h2>
              <div className="aside-label">{_.get(contractDetails, 'contract_name') || _.get(contractDetails, 'contract_number')} {codeApplicableMsg}</div>
            </div>
          }

          { additionalInformationSaved && additionalInformationSaved.length > 0 &&
            <div className="additionalinformation cf">
              <h2>{i18n('resflowcorporate_0020')}</h2>
              {additionalInformationSaved.map(item =>
                [
                  <div className="aside-label">{item.name ? item.name : i18n('resflowconfirmation_1102')}</div>,
                  <div className="aside-value">{item.value}</div>
                ]
              )}
            </div>
          }
        </aside>
      </div>
    );
  }
});

module.exports = ConfirmedPrint;
