export default function ClosedPath({onClick}) {
  return (
    <div className="closed-path">
      <i className="icon icon-nav-time-white"></i>

      <div role="button" tabIndex="0" onKeyPress={a11yClick(onClick)} onClick={onClick} className="content-container">
        <div className="header-message">{enterprise.i18nReservation.resflowreview_0040}</div>
        <div className="content-message">
          {enterprise.i18nReservation.resflowreview_0041}
        </div>
      </div>
    </div>
  );
}

ClosedPath.displayName = 'ClosedPath';

