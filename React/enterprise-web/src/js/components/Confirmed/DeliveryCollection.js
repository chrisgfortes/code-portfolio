import {RESERVATION} from '../../constants/';
import CollectionContent from './CollectionContent';
import classNames from 'classnames';

export default class DeliveryCollection extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      tab: RESERVATION.DELIVERY
    };
  }
  _onToggle (tab) {
    this.setState({
      tab
    });
  }
  render () {
    const { sessionPickupLocation, vehicleLogistics, sessionPickupTime, sessionDropoffTime, sessionDropoffLocation } = this.props;

    const deliveryTab = classNames({
      'delivery-tab': true,
      'active': this.state.tab === RESERVATION.DELIVERY
    });
    const collectionTab = classNames({
      'collection-tab': true,
      'active': this.state.tab === RESERVATION.COLLECTION
    });

    const deliveryLabel = vehicleLogistics && vehicleLogistics.delivery_info ? 'Delivery' : RESERVATION.PICK_UP;
    const collectionLabel = vehicleLogistics && vehicleLogistics.collection_info ? 'Collection' : RESERVATION.RETURN;
    const isPickup = this.state.tab === RESERVATION.DELIVERY;
    const time = isPickup ? moment(sessionPickupTime) : moment(sessionDropoffTime);
    const location = isPickup ? sessionPickupLocation : sessionDropoffLocation;
    const header = isPickup ? enterprise.i18nReservation.resflowcorporate_0060 : enterprise.i18nReservation.resflowcorporate_0061;

    const imgSrc = "https://maps.google.com/maps/api/staticmap?center=" +
      sessionPickupLocation.gps.latitude + "," +
      sessionPickupLocation.gps.longitude +
      "&zoom=12&size=400x225&scale=2&&markers=color:0x169a5a%7Clabel:K%7C" +
      sessionPickupLocation.gps.latitude + "," + sessionPickupLocation.gps.longitude;

    const googlePath = "https://www.google.com/maps/dir/current+location/" +
      location.address.street_addresses[0] + "," +
      location.address.city + "," +
      location.address.city + "," +
      location.address.country_subdivision_code + "," +
      location.address.postal;

    return (
      <div className="delivery-collection-container">
        <div className="dc-tab-container">
          <div onClick={this._onToggle.bind(this, RESERVATION.DELIVERY)}
               onKeyPress={a11yClick(this._onToggle.bind(this, RESERVATION.DELIVERY))}
               role="button"
               className={deliveryTab}>{deliveryLabel}</div>
          <div onClick={this._onToggle.bind(this, RESERVATION.COLLECTION)}
               onKeyPress={a11yClick(this._onToggle.bind(this, RESERVATION.COLLECTION))}
               role="button"
               className={collectionTab}>{collectionLabel}</div>
        </div>
        <div className="content-container">
          { isPickup ?
          <CollectionContent type="pickup" data={vehicleLogistics && vehicleLogistics.delivery_info} {...{ imgSrc, googlePath, location, header, time }} />
          :
          <CollectionContent type="return" data={vehicleLogistics && vehicleLogistics.collection_info} {...{ imgSrc, googlePath, location, header, time }} />
          }
        </div>
      </div>
    );
  }
}

DeliveryCollection.displayName = 'DeliveryCollection';
