import { SERVICE_ENDPOINTS } from '../../constants';
import ReservationFlowModelController from '../../controllers/ReservationFlowModelController';

export default function StartAnotherReservation({isFedexReservation}) {
  return (
    <div className="start-another-res">
      <h2>{i18n('resflowcorporate_0091') || 'Start Another Reservation'}</h2>
      <div className="button-group">
        { isFedexReservation ?
            <button onClick={ReservationFlowModelController.newReservationFromSession}>
              {i18n('fedexcustompath_0023') || 'REUSE ALL INFORMATION'}
            </button>
          :
            <button onClick={ReservationFlowModelController.reuseRenterInfo}>
              {i18n('corpunauthenticatedpath_0003') || 'REUSE RENTER INFORMATION'}
            </button>
        }

        <button onClick={ReservationFlowModelController.reuseTripInfo}>
          { isFedexReservation ?
              // We use different keys here because this behaviour might change in the future
              (i18n('fedexcustompath_0024') || 'REUSE TRIP INFORMATION')
            :
              (i18n('corpunauthenticatedpath_0004') || 'REUSE TRIP INFORMATION')
          }
        </button>

        <button className="new-info" onClick={ReservationFlowModelController.enterNewInfo}>
          { isFedexReservation ?
              (i18n('fedexcustompath_0025') || 'ENTER NEW INFORMATION')
            :
              (i18n('corpunauthenticatedpath_0005') || 'ENTER ALL NEW INFORMATION')
          }
        </button>
      </div>
    </div>
  );
}

StartAnotherReservation.displayName = 'StartAnotherReservation';
