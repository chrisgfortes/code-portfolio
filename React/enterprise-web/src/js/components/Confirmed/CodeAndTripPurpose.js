export default function CodeAndTripPurpose ({headers, travelPurpose, id, sessionContractDetails}) {
  return (
    <tr className="contract-details">
      <th className="contract-name" id={id}>{sessionContractDetails ? sessionContractDetails.contract_name : false}</th>
      {travelPurpose &&
        <td headers={headers}>{enterprise.i18nReservation.resflowcorporate_0013 + ' : ' + travelPurpose }</td>
      }
    </tr>
  );
}

CodeAndTripPurpose.displayName = 'CodeAndTripPurpose';

