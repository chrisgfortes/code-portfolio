export default function SignatureExecutive () {
  return (
    <div>
      <i className="icon icon-icon-bell"></i>
      <div className="content-container">
        <div className="content-message">
          {enterprise.i18nReservation.expedited_0040a}
          <strong> {enterprise.i18nReservation.expedited_0040b} </strong>
          {enterprise.i18nReservation.expedited_0040c}
          <a className="accented"
             href={"mailto:"+enterprise.i18nReservation.expedited_0040d}> {enterprise.i18nReservation.expedited_0040d}</a>.
        </div>
      </div>
    </div>
  );
}

SignatureExecutive.displayName = 'SignatureExecutive';


