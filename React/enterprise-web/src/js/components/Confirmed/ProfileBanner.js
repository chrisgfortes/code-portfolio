import classNames from 'classnames';
import Enrolled from './Enrolled';
import DNR from './DNR';
import SignatureExecutive from './SignatureExecutive';

function getClassNames ( signature, doNotRent ) {
  return classNames({
    'profile-banner': true,
    dnr: doNotRent,
    'signature-executive': signature
  });
}

export default function ProfileBanner ({profile, enrollProfile, signature, doNotRent, domain}) {
  return (
      <div className={getClassNames( signature, doNotRent )}>
        { enrollProfile ?
          <Enrolled profile={profile}/>
        : doNotRent ?
          <DNR domain={domain} />
        : 
          <SignatureExecutive />
      }
    </div>
  );
}

ProfileBanner.displayName = 'ProfileBanner';

