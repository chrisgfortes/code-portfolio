import {branch as BaobabReactMixinBranch} from 'baobab-react/mixins';
import ReservationCursors from '../../cursors/ReservationCursors';

import ConfirmedController from '../../controllers/ConfirmedController';

import FauxTableRow from './FauxTableRow';
import PriceTableHeading from '../PricingTable/PriceTableCategoryHeader';

const PersonalInformation = React.createClass({
  mixins: [BaobabReactMixinBranch, React.addons.PureRenderMixin],
  cursors: {
    ageLabel: ReservationCursors.ageLabel
  },
  render () {
    const { driverInformation, renterAge } = this.props;
    const ageRange = this.state.ageLabel;
    const driver = ConfirmedController.getPersonalInformation(driverInformation, renterAge);
    const addressContent = !!(driver.address && driver.address.street_addresses &&
      driver.address.street_addresses.length) && (
      <span>
        { driver.address.street_addresses.map((address, index) => (
          <span key={index} className="description">{address}</span>
        ))}
      <span> {driver.address.city}, {driver.address.country_subdivision_code} {driver.address.postal} {driver.address.country_code}</span></span>
    );

    return (
      <tbody>
        <PriceTableHeading
          id="section-header-PersonalInformation"
          title={enterprise.i18nReservation.resflowreview_0003} />
        { addressContent ?
          <FauxTableRow
            class="personal-information-row"
            label={enterprise.i18nReservation.resflowreview_0102}
            value={addressContent}/>

          : null }
        <FauxTableRow
          classes="personal-information-row"
          id="PersonalInformation-row-header-1"
          headers="section-header-PersonalInformation PersonalInformation-row-header-1"
          label={enterprise.i18nReservation.resflowreview_0101}
          value={driver.name}/>
        <FauxTableRow
          classes="personal-information-row"
          id="PersonalInformation-row-header-2"
          headers="section-header-PersonalInformation PersonalInformation-row-header-2"
          label={enterprise.i18nReservation.resflowreview_0103}
          value={driver.email}/>
        <FauxTableRow
          classes="personal-information-row"
          id="PersonalInformation-row-header-3"
          headers="section-header-PersonalInformation PersonalInformation-row-header-3"
          label={enterprise.i18nReservation.resflowreview_0104}
          value={driver.phone}/>
        <FauxTableRow
          classes="personal-information-row"
          id="PersonalInformation-row-header-4"
          headers="section-header-PersonalInformation PersonalInformation-row-header-4"
          label={enterprise.i18nReservation.resflowreview_0105}
          value={ageRange}/>
      </tbody>
    );
  }
});

module.exports = PersonalInformation;
