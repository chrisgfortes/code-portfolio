export default function AdditionalInformation ({id, name, headers, value}) {
  return (
    <tr>
      <th className="Additional-name" id={id}>{name}</th>
      <td className="Additional-value" headers={headers}>{value}</td>
    </tr>
  );
}

AdditionalInformation.displayName = 'AdditionalInformation';

