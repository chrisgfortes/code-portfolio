import {branch as BaobabReactMixinBranch} from 'baobab-react/mixins';
import ReservationCursors from '../../cursors/ReservationCursors';

import {PRICING} from '../../constants/';

import ModifyController from '../../controllers/ModifyController';
import ProfileController from '../../controllers/ProfileController';

import Header from './Header';
import UpcomingHeader from './UpcomingHeader';
import CancelledHeader from './CancelledHeader';
import Summary from './Summary';
import Thanks from './Thanks';
import Checklist from './Checklist';
import RateTaxesFees from '../PricingTable/RateTaxesFees';
import LocationDetails from './LocationDetails';
import DeliveryCollection from './DeliveryCollection';
import ProfileBanner from './ProfileBanner';
import StartAnotherReservation from './StartAnotherReservation';
import ConfirmedController from '../../controllers/ConfirmedController';
import KeyRentalFactsAndPolicies from '../KeyRentalFacts/KeyRentalFactsAndPolicies';
import Modals from './Modals';

const Confirmed = React.createClass({
  mixins: [
    BaobabReactMixinBranch,
    React.addons.PureRenderMixin
  ],
  cursors: {
    afterHourMessage: ReservationCursors.afterHourMessage,
    cancelRebook: ReservationCursors.cancelRebook,
    cancellationDetails: ReservationCursors.cancellationDetails,
    cancelReservationModal: ReservationCursors.cancelReservationModal,
    chargeType: ReservationCursors.chargeType,
    codeApplicable: ReservationCursors.codeApplicable,
    componentToRender: ReservationCursors.componentToRender,
    confirmationNumber: ReservationCursors.confirmationNumber,
    driverInformation: ReservationCursors.driverInformation,
    dropoffHours: ReservationCursors.dropoffHours,
    deepLinkView: ReservationCursors.deepLinkView,
    errors: ReservationCursors.confirmationErrors,
    enroll: ReservationCursors.enroll,
    eligibility: ReservationCursors.reservationEligibility,
    equipmentExtras: ReservationCursors.equipmentExtras,
    expedited: ReservationCursors.expedited,
    isFedexReservation: ReservationCursors.isFedexReservation,
    insuranceExtras: ReservationCursors.insuranceExtras,
    keyRentalFacts: ReservationCursors.keyFactsPoliciesFromReservation,
    loginWidgetErrors: ReservationCursors.loginWidgetErrors,
    loggedIn: ReservationCursors.userLoggedIn,
    modifyModalOpen: ReservationCursors.modifyModalOpen,
    profile: ReservationCursors.profile,
    prepay: ReservationCursors.prepay,
    pickupHours: ReservationCursors.pickupHours,
    resRequest: ReservationCursors.resRequest,
    sessionPickupLocation: ReservationCursors.sessionPickupLocation,
    sessionDropoffLocation: ReservationCursors.sessionDropoffLocation,
    sessionPickupTime: ReservationCursors.sessionPickupTime,
    sessionDropoffTime: ReservationCursors.sessionDropoffTime,
    sessionPickupLocationWithDetail: ReservationCursors.sessionPickupLocationWithDetail,
    sessionContractDetails: ReservationCursors.sessionContractDetails,
    sessionExpedited: ReservationCursors.sessionExpedited,
    supportLinks: ReservationCursors.supportLinks,
    vehicleLogistics: ReservationCursors.vehicleLogistics,
    verification: ReservationCursors.verification,
    viewCarClassDetailsStatus: ReservationCursors.viewCarClassDetailsStatus
  },
  componentDidMount () {
    ConfirmedController.setAddCardSuccess(false);
    ConfirmedController.setRegisterCardSuccess(false);
  },
  closeCancelModal () {
    ModifyController.togglePrepayTerms(false);
    ModifyController.toggleCancelReservationModal(false);
  },
  render(){
    const { vehicleLogistics, sessionContractDetails, keyRentalFacts, isFedexReservation, sessionExpedited, expedited, profile,
            prepay, cancellationDetails, cancelReservationModal, errors, enroll, confirmationNumber,
            cancelRebook, viewCarClassDetailsStatus, modifyModalOpen, eligibility, chargeType, verification,
            loggedIn, supportLinks, driverInformation, equipmentExtras, insuranceExtras, componentToRender,
            sessionPickupLocationWithDetail, sessionPickupLocation, sessionDropoffLocation, afterHourMessage,
            pickupHours, dropoffHours, resRequest, codeApplicable, sessionDropoffTime, sessionPickupTime } = this.state;
    const { details, cancelled } = this.props;

    const promoCode = sessionContractDetails ? sessionContractDetails.contract_name : null;
    const contract_type = (sessionContractDetails && sessionContractDetails.contract_type || '').toLowerCase();
    const isContractPromotion = contract_type === PRICING.PROMOTION_PROMOTION;
    const isContractCorporate = contract_type === PRICING.CORPORATE;
    const loyalty_brand = _.get(resRequest, 'loyalty_brand');
    const collectionOrDelivery = ( vehicleLogistics && ( vehicleLogistics.collection_info || vehicleLogistics.delivery_info ));
    const header = ConfirmedController.getConfirmationHeader();
    const dateStr = ConfirmedController.getDetailDate(sessionPickupTime);
    const allowModify = ConfirmedController.allowModify( eligibility, chargeType );
    const doNotRent = ProfileController.isDoNotRentProfile(profile);
    const signature = ProfileController.isSignatureProfile(profile);
    const showBanner = ConfirmedController.showBanner(doNotRent, signature, expedited, enroll);
    const domain = enterprise.country_code ? enterprise.country_code.toLowerCase() : 'com';
    const couponInfoHeading = isContractPromotion ? i18n('resflowcorporate_4016') :
                            isContractCorporate ? i18n('resflowcorporate_4015') :
                            i18n('reservationwidget_0039');
    const codeApplicableMsg = isContractPromotion ? '(' + (codeApplicable ? i18n('promotionemail_0003') : i18n('promotionemail_0019')) + ')' :
                              isContractCorporate ? '(' + (codeApplicable ? i18n('resflowcarselect_8011') : i18n('promotionemail_0021')) + ')' :
                              '';

    return (
      <section className="confirmed-page">
        { showBanner &&
          <ProfileBanner {...{profile, signature, domain}}
                         enrollProfile={!_.isEmpty(enroll.profile)}
                         doNotRent={doNotRent || expedited.dnr} />
        }

        { details ?
          <UpcomingHeader {...{header, prepay, cancellationDetails, cancelReservationModal, errors, confirmationNumber,
            cancelRebook, modifyModalOpen, eligibility, chargeType, profile, dateStr, allowModify, verification}}
            closeCancelModal={this.closeCancelModal}/>
        : cancelled ?
          <CancelledHeader header={header} />
        :
          <Header {...{header, prepay, cancellationDetails, cancelReservationModal, errors, confirmationNumber, sessionContractDetails,
            cancelRebook, modifyModalOpen, eligibility, chargeType, profile, dateStr, allowModify, verification}}
            closeCancelModal={this.closeCancelModal}/>
        }

        <Summary summary={ConfirmedController.getSummary()} />

        <Thanks {...{ viewCarClassDetailsStatus, equipmentExtras, insuranceExtras, componentToRender, sessionPickupLocationWithDetail }} />

        <div className="person-pricing hidden-mobile">
          <RateTaxesFees showPersonal title={i18n('resflowreview_0302')} cancelled={this.props.cancelled} />
        </div>
        {(loyalty_brand || isFedexReservation) &&
          <StartAnotherReservation loyalty_brand={loyalty_brand} {...{isFedexReservation}} />}

        {!this.props.cancelled && (enterprise.reservation.checklist || []).length > 0 &&
          <Checklist {...{sessionExpedited, loggedIn, driverInformation, supportLinks, confirmationNumber}} />
        }
          <div className="person-pricing visible-mobile">
            <RateTaxesFees showPersonal title={i18n('resflowreview_0302')} cancelled={this.props.cancelled} />
          </div>
        {promoCode &&
          <div className="information-block coupon">
            <span className="category-label">{couponInfoHeading}</span>
            {promoCode ? <div className="info-block-details">{promoCode} {codeApplicableMsg}</div> : false}
          </div>
        }

        {(keyRentalFacts && keyRentalFacts.length) ?
          <KeyRentalFactsAndPolicies showKeyFacts/> :
          <KeyRentalFactsAndPolicies showKeyFacts={false}/>
        }

        {collectionOrDelivery ?
            <DeliveryCollection {...{ sessionPickupLocation, vehicleLogistics, sessionPickupTime, sessionDropoffTime, sessionDropoffLocation }} /> :
            <LocationDetails detailType={'pickup'} {...{ sessionPickupLocation, sessionDropoffLocation, afterHourMessage, pickupHours, dropoffHours }} />
        }

        <Modals />

      </section>
    );
  }
});

module.exports = Confirmed;
