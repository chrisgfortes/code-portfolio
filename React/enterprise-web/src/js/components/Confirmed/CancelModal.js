import PrepayTerms from '../Verification/PrepayTerms';
import CancelModalContent from './CancelModalContent';

export default function CancelModal ({showPrepayTerms, verification, sessionContractDetails, cancellationDetails,
                                      prepay, isSamePriceRegion, confirmationNumber}) {
  return (
    <div className="cancel-confirmation cf">
      {showPrepayTerms ?
        <PrepayTerms
          policy={verification.prepayPolicy}
          showPrepayTerms={verification.showPrepayTerms}
        /> :
        <CancelModalContent
          contractDetails={sessionContractDetails}
          cancellationDetails={cancellationDetails}
          prepay={prepay}
          samePriceRegion={isSamePriceRegion}
          confirmationNumber={confirmationNumber}/>}
    </div>
  );
}

module.exports = CancelModal;

