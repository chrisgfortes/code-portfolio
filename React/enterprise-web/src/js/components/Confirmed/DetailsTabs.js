export default class DetailsTabs extends React.Component {
  constructor(props) {
    super(props);
  }
  onClick (item) {
    this.props.onTabClick(item);
  }
  onKeyDown (item, event) {
    let code = event.key;
    let tabIdToFocus = item.id ? 0 : 1;
    if (code === 'ArrowDown' || code === 'ArrowUp' || code === 'ArrowLeft' || code === 'ArrowRight') {
      event.preventDefault();
      this.refs['tab' + tabIdToFocus].focus();
      this.props.onArrowPressed(tabIdToFocus);
    }
  }
  render () {
    let items = this.props.items.map(function (item, index) {
      let isActiveTab = this.props.activeItem.label === item.label;
      let classes = isActiveTab ? 'tab tab-1-2 active' : 'tab tab-1-2';
      let tabIndex = isActiveTab ? '0' : '-1';
      return (
        <div key={index} className={classes} ref={'tab' + index}
             role="tab" tabIndex={tabIndex} aria-selected={isActiveTab ? 'true' : 'false'}
             aria-controls="tabContent" onKeyDown={this.onKeyDown.bind(this, item)}
             onClick={this.onClick.bind(this, item)}>
          {item.label}
        </div>);
    }.bind(this));
    return <div className="tabs" role="tablist">{items}</div>;
  }
}

DetailsTabs.displayName = 'DetailsTabs';
