// ECR-14250 we have a cursor or method for this I think
export default function Enrolled ({profile}) {
  const loyaltyNumber = _.get(profile, 'basic_profile.loyalty_data.loyalty_number');
  return (
    <div className="eplus-enroll">
      <i className="icon icon-eplus-logo" />
      <div className="content-container">
        <div className="header-message">{i18n('expedited_0041a')}</div>
        <div className="content-message">
          {i18n('expedited_0041b')} <strong>{loyaltyNumber}</strong>
          {i18n('expedited_0041d')}
        </div>
      </div>
    </div>
  );
}

Enrolled.displayName = 'Enrolled';


