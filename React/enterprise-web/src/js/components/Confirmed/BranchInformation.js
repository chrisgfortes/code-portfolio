export default function BranchInformation ({ location, hideMap,imgSrc, googlePath }) {
  return (
    <div>
      {hideMap ? false : <img className="location-map" src={imgSrc} title="location map" alt="location map"/>}
      <h2>Branch Information</h2>
      <div className="branch-information">
        <h3>{location.name}</h3>
        <div>{location.address.street_addresses[0]}</div>
        <div>{location.address.city}, {location.address.country_subdivision_code + ' ' + location.address.postal}</div>
        <div>{location.phones ? location.phones[0].phone_number : false}</div>
        <a className="location-direction" href={googlePath}
           target="_blank">{enterprise.i18nReservation.resflowconfirmation_0023}</a>
      </div>
    </div>
  );
}

BranchInformation.displayName = 'BranchInformation';
