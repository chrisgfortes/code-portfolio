import ClosedPath from './ClosedPath';
import ConfirmedController from '../../controllers/ConfirmedController';

export default class Checklist extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checklistOpen: false
    };
    this._onCarrotToggle = this._onCarrotToggle.bind(this);
  }
  _onCarrotToggle() {
    this.setState({
      checklistOpen: !this.state.checklistOpen
    });
  }
  render () {
    const {sessionExpedited, loggedIn, driverInformation, supportLinks, confirmationNumber} = this.props;
    return (
      <div className="checklist-container">
        <div className={"checklist" + (this.state.checklistOpen ? ' open' : '')}>
          <h2 onClick={this._onCarrotToggle}>
            {enterprise.i18nReservation.resflowconfirmation_0030}
            <i className="icon icon-nav-carrot-white"></i>
          </h2>

          <div className="checklist-content">
            <ol>
              { enterprise.reservation.checklist.map(function (obj, index) {
                  return <li key={index}><i className="icon icon-forms-checkmark-green"></i>{obj.listitem}</li>
                })
              }
            </ol>
          </div>
        </div>
        {(!sessionExpedited && !loggedIn) ? <ClosedPath onClick={() => { window.open(ConfirmedController.generateURL( driverInformation, supportLinks, confirmationNumber )) }} /> : false}
      </div>
    );
  }
}

Checklist.displayName = 'Checklist';

