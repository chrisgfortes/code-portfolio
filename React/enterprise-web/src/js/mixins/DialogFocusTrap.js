const DialogFocusTrap = {
  $firstFocusable: null,
  $lastFocusable: null,

  updateFocusableLoop (reinitializeFocus) {
    this._removeFocusableLoop();
    this._addFocusableLoop();

    if (reinitializeFocus) {
      this.$firstFocusable.focus();
    }
  },

  _addFocusableLoop () {
    if (!this.focusTrapWrapper) {
      return console.error('DialogFocusTrap ERROR: please add a focusTrapWrapper ref in your component.');
    }
    const $focusTrapWrapper = $(this.focusTrapWrapper);
    const $focusablesList = $focusTrapWrapper.find(':focusable');

    this.$firstFocusable = $focusablesList.first().on('keydown.focusloop', this._focusableLoopHandler);
    this.$lastFocusable = $focusablesList.last().on('keydown.focusloop', this._focusableLoopHandler);

    return true;
  },

  _removeFocusableLoop () {
    if (this.$firstFocusable) {
      this.$firstFocusable.off('.focusloop');
    }
    if (this.$lastFocusable) {
      this.$lastFocusable.off('.focusloop');
    }
  },

  _focusableLoopHandler (event) {
    if (event.key === 'Tab') {
      if (event.shiftKey) {
        if (event.target === this.$firstFocusable.get(0)) {
          event.preventDefault();
          this.$lastFocusable.focus();
        }
      } else if (event.target === this.$lastFocusable.get(0)) {
        event.preventDefault();
        this.$firstFocusable.focus();
      }
    }
  },

  componentDidMount () {
    const shouldFocusFirst = !this.focusTrapOptions || !(this.focusTrapOptions.shouldFocusFirst === false);

    if (this._addFocusableLoop() && shouldFocusFirst) {
      this.$firstFocusable.focus();
    }
  },

  componentWillUnmount () {
    this._removeFocusableLoop();
  },

  componentDidUpdate () {
    this.updateFocusableLoop();
  }

};

module.exports = DialogFocusTrap;
