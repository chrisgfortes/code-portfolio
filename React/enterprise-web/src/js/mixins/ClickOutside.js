import EcomError from '../classes/EcomError';
import ReactDOM from 'react-dom';

const clickOutsideMixin = {

  // This is a slightly modified version of the mixin from
  // https://github.com/Pomax/react-onclickoutside

  // Use a parallel array because we can't use
  // objects as keys, they get toString-coerced

  registeredComponents: [],
  handlers: [],
  IGNORE_CLASS: 'ignore-react-onclickoutside',
  componentDidMount: function () {
    if (!this._handleClickOutside) {
      throw new EcomError('Component lacks a handleClickOutside(event) function for processing outside click events.', 'ClickOutside');
    }

    let fn = this.__outsideClickHandler = (function (localNode, eventHandler) {
      return function (evt) {

        let source = evt.target;
        let found = false;

        // If source=local then this event came from "somewhere"
        // inside and should be ignored. We could handle this with
        // a layered approach, too, but that requires going back to
        // thinking in terms of Dom node nesting, running counter
        // to React's "you shouldn't care about the DOM" philosophy.
        while (source.parentNode) {
          found = (source === localNode);
          if (found) return;
          source = source.parentNode;
        }
        eventHandler(evt);
      }
    }(ReactDOM.findDOMNode(this), this._handleClickOutside));

    let pos = this.registeredComponents.length;
    this.registeredComponents.push(this);
    this.handlers[pos] = fn;

    // If there is a truthy disableOnClickOutside property for this
    // component, don't immediately start listening for outside events.
    if (!this.props.disableOnClickOutside) {
      this.enableOnClickOutside();
    }
  },

  componentWillUnmount: function () {
    this.disableOnClickOutside();
    this.__outsideClickHandler = false;
    var pos = this.registeredComponents.indexOf(this);
    if (pos > -1) {
      if (this.handlers[pos]) {
        // clean up so we don't leak memory
        this.handlers.splice(pos, 1);
        this.registeredComponents.splice(pos, 1);
      }
    }
  },

  /**
   * Can be called to explicitly enable event listening
   * for clicks and touches outside of this element.
   */
  enableOnClickOutside: function () {
    var fn = this.__outsideClickHandler;
    document.addEventListener("mousedown", fn);
    document.addEventListener("touchstart", fn);
  },

  /**
   * Can be called to explicitly disable event listening
   * for clicks and touches outside of this element.
   */
  disableOnClickOutside: function (fn) {
    var fn = this.__outsideClickHandler;
    document.removeEventListener("mousedown", fn);
    document.removeEventListener("touchstart", fn);
  }


};

module.exports = clickOutsideMixin;
