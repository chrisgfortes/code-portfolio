'use strict';

//Validator Documentation/Steps to use
//1. Include mixin
//2. Include fieldMap() in component, this must include a schema and value
//   a. schema: ? means optional, follow by type
//   b. schema can be a function, returning a boolean of your own validation
//3. include REF the same name as the field name
//4. use this.validate(field + fieldValue) for single component validation
//   or this.validateAll() for whole form validation
//5. items not in schema will not be validated and passed as true
//6. define fieldMap specific refs IF your event handler is not in the same component as your form fields

import { GLOBAL } from '../constants';

const Validations = {
  password (actual) {
    let rules = {
      //space: null,
      length: null,
      blacklist: null,
      oneLetter: null,
      oneNumber: null,
      //email: null,
    };
    if (actual) {
      let blacklist = ['password', 'passwort', 'contraseña', 'mot de passe', 'motdepasse', 'senha', 'wachtwoord', 'palavra-passe'];
      let lowerCaseValue = actual.toLowerCase();
      rules.blacklist = !blacklist.some((str) => lowerCaseValue.indexOf(str) > -1);
      //rules.space = !/\s/g.test(actual);
      rules.length = actual.length > 7;
      rules.oneNumber = /.*\d+.*/.test(actual);
      rules.oneLetter = /.*[a-zA-Z]+.*/.test(actual);
      //rules.email = !/(.+)@(.+){2,}\.(.+){2,}/.test(actual);
      return {
        valid: !Object.keys(rules).some((rule) => rules[rule] === false),
        errors: rules
      };
    } else {
      for (let rule in rules) {
        if (rules.hasOwnProperty(rule)) {
          rules[rule] = null;
        }
      }
      return {
        valid: false,
        errors: rules
      }
    }
  }
};

const Validator = {
  hooks: {
    validateAll: [],
    validate: []
  },
  //Validation for the whole form
  validateAll (section, disableError) {
    let value = this.fieldMap().value;
    let schema = this.fieldMap().schema;
    let valid = true;

    if (section) {
      value = section.value;
      schema = section.schema;
    }

    let errors = [];
    let errorReasons = {};

    for (let i in value) {
      if (value.hasOwnProperty(i)) {

        let format = schema[i] ? schema[i] : null,
          fieldValid = this.compare(format, value[i]),
          fieldValidBool = true;

        if (fieldValid && typeof fieldValid === 'object') {
          if (!fieldValid.valid) {
            errors.push(i);
            errorReasons[i] = fieldValid.errors;
            valid = false;
          } else {
            errorReasons[i] = fieldValid.errors;
            valid = true;
          }
          fieldValidBool = fieldValid.valid;
        } else {
          if (!fieldValid) {
            errors.push(i);
            valid = false;
          }
          fieldValidBool = fieldValid;
        }

        if (!disableError) {
          this.handleErrorState(i, fieldValidBool);
        }
      }
    }

    if (this.hooks.validateAll.length) {
      this.hooks.validateAll.forEach((func) => {
        func(valid, errors);
      });
    }

    return {
      valid,
      errors,
      errorReasons
    };
  },
  //Validation for a single field
  validate (field, fieldValue) {
    let schema = this.fieldMap().schema;
    let format = schema[field] ? schema[field] : null;
    let fieldValid = this.compare(format, fieldValue);
    let valid = null;
    let errorObj;

    //if(field == 'password') debugger;

    if (typeof fieldValid === 'object') {
      let errorReasons = {};

      valid = fieldValid ? fieldValid.valid : null;
      errorReasons[field] = fieldValid ? fieldValid.errors : null;

      errorObj = {
        valid: fieldValid ? fieldValid.valid : false,
        errorReasons: errorReasons
      };
    } else {
      valid = fieldValid;
    }

    this.handleErrorState(field, valid);

    if (this.hooks.validate.length) {
      this.hooks.validate.forEach((func) => {
        func(valid, field);
      });
    }

    if (errorObj) {
      return errorObj;
    } else {
      return valid;
    }

  },
  handleErrorState (field, validity) {

    //Uses fieldMap refs if exists, or else default to native refs
    let fieldDOM = this.fieldMap().refs && this.fieldMap().refs[field] ?
      this.fieldMap().refs[field] : this.refs[field] && this.refs[field].getDOMNode();

    if (fieldDOM) {
      if (validity) {
        $(fieldDOM).removeClass('invalid');
      } else {
        $(fieldDOM).addClass('invalid');
      }
    }

  },
  compare (expected, actual) {
    if (expected) {
      //If function, return the result of that function
      if (typeof expected === 'function') {
        return expected();
      }

      let expectedType = expected.charAt(0) === '?' ?
        expected.substring(1, expected.length) : expected;

      //If content and any length check type
      if (actual && actual.length > 0) {

        //Arrange below by most frequently used for performance
        if (expectedType === 'number') {
          return !isNaN(actual);
        }

        // if (expectedType === 'numberMask') {
        //   return !/[^\d*]/.test(actual);
        // }

        if (expectedType === 'integer') {
          return /^[0-9]*$/.test(actual);
        }

        if (expectedType === 'phone') {
          let formattedPhone = actual.replace(/[- )(]/g, '');
          //const acceptedLength = 10;

          return !isNaN(formattedPhone);
        }

        if (expectedType === 'phoneMask') {
          let clean = actual.replace(/[- )(.]/g, '');
          let re = new RegExp('[^\\d'+GLOBAL.V1_MASK_CHAR+GLOBAL.V2_MASK_CHAR+']', 'g');
          let result = !re.test(clean);
          return result;
        }

        if (expectedType === 'email') {
          return /(.+)@(.+){2,}\.(.+){2,}/.test(actual);
        }

        if (expectedType === 'dateMask') {
          return enterprise.settings.regularex.maskedDate.test(actual) || GLOBAL.REGEX_MASK_CHAR.test(actual);
        }

        if (expectedType === 'date') {
          //helper var
          var dateTest = new Date(actual);

          //is just numbers and dashes (-)?
          if (isNaN(actual.replace(/[-/+]/g, ''))) return false;
          //matches the 0000-00-00 pattern?
          if (!actual.match(/\d{4}-[0-1]\d-[0-3]\d/)) return false;
          //is a valid date?
          if (dateTest == 'Invalid Date') return false;

          //if you made it this far the date must be valid...
          return true;
        }

        if (expectedType === 'password') {
          return Validations.password(actual);
        }

        return typeof actual === expectedType;

      } else {
        //Defaulting to true if its optional
        if (expected === 'password') {
          return Validations.password(actual);
        }
        return expected.charAt(0) === '?';
      }

    } else {
      //Returning true if not in schema
      return true;
    }
  }
};


module.exports = Validator;
