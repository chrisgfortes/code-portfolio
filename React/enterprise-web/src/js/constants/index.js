/**
 * CONSTANTS INDEX
 *  Please avoid magic strings.
 *
 * INSTRUCTIONS:
 *  Add a line per set of constants
 *
 * Export like:
 *  export {default as ITEM} from './file';
 *
 * Import like:
 *  import {ITEM} from './constants';
 *  Note you since this is an index, we don't need to specify which file
 *
 * Use like:
 *  ${ITEM.THING} or as necessary
 */
export PRICING from './pricing';
export LOCALES from './locales';
export PAGEFLOW from './pageflow';
export PROFILE from './profile';
export PARTNERS from './partners';
export FILEPATHS from './filepaths';
export RESERVATION from './reservation';
export EXPEDITED from './expedited';
export REQUEST from './request';
export EXTRAS from './extras';
// @todo: rename LOCATION_SEARCH to LOCATIONS as LOCATION_SEARCH is too "section of site" specific
// Also, it's confusing in Autocomplete with other constants
export LOCATION_SEARCH from './locations';
export VEHICLES from './vehicles';

// export {default as OPERATORS} from './operators';
export SERVICE_ENDPOINTS from './endpoints';
export GLOBAL from './global';
