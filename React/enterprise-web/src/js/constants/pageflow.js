export default {
  /*
  * pull more from ReservationFlow.js
  *
  * BookingWidget
  * DateTimeWidget
  * PickupLocationSearch
  * DropoffLocationSearch
  * CarSelect
  * Addons
  * Verification
  * Modify
  * Details
  * TimedOut
  * loading
  *
  * */
  CANCELLED: 'Cancelled',
  CONFIRMED: 'Confirmed',
  CONFIRMED_CONFIRMED: 'confirmed',
  VERIFICATION: 'Verification',
  MODIFY: 'Modify',
  MODIFIED: 'Modified',
  LOCATION_SEARCH: 'LocationSearch',
  HASHBOOK: '#book',

  COMMIT: '#commit',
  EXTRAS: '#extras',
  CARS: '#cars',

  HASHPICKUP: 'location/pickup',

  // "currentView" used in AEM components etc.
  LANDING_PAGE: 'landingPage',
  CAR_RENTAL: 'carRental',
  RESERVATION_FLOW: 'reserve',
  ACCOUNT: 'account',
  HOME: 'home',
  EXPEDITED: 'expedited',
  BROWSER_PAGE: 'browser-notice',
  COMPONENT_LOCATIONSEARCH: 'locationSelect',
  COMPONENT_BOOKING: 'bookingWidget',

  INCLUDED: 'INCLUDED',
  INSURANCE: 'insurance',
  NOT_WEB_BOOKABLE: 'NOT_WEB_BOOKABLE'
}
