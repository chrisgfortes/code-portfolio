/**
 * External partner constants
 * */

export default {
  ENTERPRISE: 'ENTERPRISE', //is this elsewhere?
  ALAMO: 'ALAMO',
  NATIONAL: 'NATIONAL',

  ALAMOMAPPING: {
    'ALAMO' : {
      label: 'Alamo',
      link: 'https://www.alamo.com/en_US/car-rental/locations.html'
    }
  },
  NATIONALMAPPING: {
    'NATIONAL' : {
      label: 'National',
      link: 'https://www.nationalcar.com/en_US/car-rental/locations.html'
    }
  },

  BRAND: {
    ENTERPRISE_PLUS: 'EP',
    ENTERPRISE_PLUS_LONG: 'ENERPRISEPLUS',
    EMERALD_CLUB: 'EC',
    EMERALD_CLUB_LONG: 'EMERALDCLUB'
  },

  B2BS: {
    FEDEX: 'fedex',
    USAA: 'USAA MEMBER PROGRAM'
  },

  /**
   * Reasonably sure having this here is a terrible idea as they change.
   * @type {Object}
   */
  CONTRACTS: {
    FEDEX: 'DB77100',
    USAA: 'ALNCWBA'
  },

  CONTRACT_SPECIAL_SEQUENCE: {
    USAA: {
      'membernumber': 1
    }
  },

  ENTERPRISE_LABEL: 'Enterprise Rent-A-Car', // they don't want to make this a key...

  EXOTICS: 'EXOTICS',
  TRUCKS: 'TRUCKS', //need a new file for these?
  MOTORCYCLES: 'MOTORCYCLES'
}
