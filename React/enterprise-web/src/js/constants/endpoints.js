export default {
  SESSION_CURRENT: '/current-session',
  SESSION_UPDATE: '/current-session',
  SESSION_CLEAR: '/current-session/clear-reservation',
  SESSION_ANALYTICS: '/current-session/analytics',
  SESSION_LANGUAGE: '/current-session/browser-language',

  LOCATION_SPATIAL: (query) => `/search/location/dotcom/spatial/${query}`,
  LOCATION_SPATIAL_BOX: '/search/location/dotcom/spatial/bbox/v2',
  LOCATION_SELECT: '/search/location/dotcom/select',
  LOCATION_VALIDITY: (query) => `/search/location/dotcom/validity/location/${query}`,
  LOCATION_HOURS_POLICY: '/location/',
  LOCATION_DETAILS: '/location/',

  LOCATION_SEARCH_FEDEX: (endPointName) => `/search/location/dotcom/${endPointName}/`,
  LOCATION_SEARCH: '/search/location/dotcom/text/',
  LOCATION_SEARCH_BRANCHES: (query) => `/search/location/enterprise/web/country/${query}`,
  LOCATION_COUNTRIES: '/location/countries',
  LOCATION_STATESPROV_PREFIX: '/location/countries/',
  LOCATION_STATESPROV_SUFFIX: '/statesorprovinces',

  LOCATION_AGE_SEARCH: '/search/location/dotcom/renterage/',
  LOCATION_AGE_SEARCH_BYCOUNTRY: '/search/location/dotcom/renterage/country/',
  LOCATION_HOURS: '/search/location/dotcom/hours/',

  NEW_RES_FROM_SESSION: '/current-session/newReservationFromSession',
  LOGIN: (brand) => `/enterprise/profile/login/${brand}`,
  LOGOUT: '/enterprise/profile/logout',

  PASSWORD_RESET: '/profile/resetPasswordStart',
  PASSWORD_UPDATE: '/enterprise/profile/password',

  PROFILE_CREATE: '/enterprise/profile/createProfile/',
  PROFILE_ACTIVATE: (individualId) => `/enterprise/profile/createLogin/${individualId}`,
  PROFILE_UPDATE: '/enterprise/profile/modify',
  PROFILE_MYTRIPS: '/enterprise/mytrips/',
  PROFILE_TRIPS_RECEIPTS: '/trips/invoice',
  PROFILE_MEMBER_SEARCH: '/enterprise/profile/searchRenterByMembership',

  CONTENT_TERMS_EPLUS: '/content/terms/loyalty',
  CONTENT_PRIVACY: '/content/privacy',
  CONTENT_TAXES: '/content/taxes',
  CONTENT_TERMS_PREPAY: '/enterprise/content/terms/prepay',

  CONTRACT_DETAILS: (contract) => `/contract/details/${contract}`,

  OPTIONAL_EXTRAS: '/subscription/optInExtras',

  PAYMENT_ADD: '/enterprise/addpayment',
  PAYMENT_UPDATE: '/updatepayment/',
  PAYMENT_DELETE: '/enterprise/removePayment/',
  PAYMENT_CARDSUBMIT_KEY: '/profile/cardsubmissionkey',

  RESERVATION_INIT: '/enterprise/reservations/initiate',
  RESERVATION_CANCEL: '/enterprise/reservations/cancel',

  RESERVATION_DETAILS_PREFIX: '/reservations/',
  RESERVATION_DETAILS_SUFFIX: '/details',

  RESERVATION_RETRIEVE: '/enterprise/reservations/retrieve',
  RESERVATION_RETRIEVE_CANCEL: '/reservations/retrieveAndCancel',
  RESERVATION_MODIFY_INIT: '/reservations/modify/init',
  RESERVATION_MODIFY_ABANDON: '/reservations/modify/abandon',
  RESERVATION_AFTERCOMMIT_DETAILS: '/reservations/afterCommitDetails',

  RESERVATION_VEHICLE_AVAILABILITY: '/enterprise/reservations/vehicles/availability',
  RESERVATION_VEHICLE_DETAILS: '/enterprise/reservations/carClassDetails',
  RESERVATION_VEHICLE_UPGRADES: '/enterprise/reservations/vehicleUpgradeRates',
  RESERVATION_VEHICLE_UPGRADE_SELECT: '/enterprise/reservations/selectUpgrade',
  RESERVATION_VEHICLE_CLEAR: '/reservations/vehicles/clearfromcurrentsession',
  RESERVATION_VEHICLE_REDEMPTION: '/reservations/vehicles/redemption/submit',
  RESERVATION_VEHICLE_SELECTCARCLASS: '/enterprise/reservations/selectCarClass',

  RESERVATION_EXTRAS_SUBMIT: '/reservations/extras/submit',
  RESERVATION_EXTRAS_UPDATE: '/enterprise/reservations/extras/update',

  RESERVATION_EXPEDITED: '/enterprise/profile/expedite',

  RESERVATION_ASSOCIATE: '/enterprise/reservations/associateUser',
  RESERVATION_COMMIT: '/enterprise/reservations/commit',
  RESERVATION_COMMIT_SAVE: '/reservations/commit/save',
  RESERVATION_SHOWFIELDS: '/reservations/showreviewfields',
  RESERVATION_FACTS: '/reservations/facts', // this may no longer be used

  PREPAY_FLOP: '/reservations/prepay/flop',
  PREPAY_INIT: '/enterprise/reservations/prepay/cardSubmissionKey',
  PREPAY_REGISTRATION: '/reservations/prepay/registrationsuccessful/',
  PREPAY_3DS: '/enterprise/reservations/prepay/3ds',

  TERMS_AND_CONDITIONS: '/enterprise/reservations/rentalTermsAndConditions',

  // these may no longer be used? See LeadFormsService.js
  LEADFORMS_ENTERTAINMENT: '/message/entertainmentAndProduct',
  LEADFORMS_MEETING: '/message/conventionAndMeeting',

  CONTEST_INIT: '/contest/init'
}
