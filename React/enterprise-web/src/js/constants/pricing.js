/**
 * Pricing Related Constants
 */

export default {
  PRICING_NET_RATE: 'Net Rate',
  // not pleased with this because for different currencies it may be different
  // we do not take into account the 'format' value we frequently get back
  VALUE_ZERO: '0.00',

  PURPOSE_BUSINESS: 'BUSINESS',
  PURPOSE_LEISURE: 'LEISURE',

  REDEMPTION: 'REDEMPTION',
  PAYLATER: 'PAYLATER',
  PREPAY: 'PREPAY',
  CURRENCY: 'CURRENCY',
  CURRENCY_TYPES: ['PAYLATER', 'PREPAY'], // @todo - this should reference the constants above instead of duplicating the strings :/

  BUSINESS_ACCOUNT_APPLICANT: 'BUSINESS_ACCOUNT_APPLICANT',
  CREDIT: 'CREDIT_CARD',

  EPLUS_REDEMPTION_SAVINGS: 'EPLUS_REDEMPTION_SAVINGS',

  CONTRACT_CORPORATE: 'CORPORATE',
  CORPORATE: 'corporate',

  EXTRAS_ON_REQUEST: 'ON_REQUEST',

  BUSINESS: 'BUSINESS',
  PROMOTION: 'PROMOTION',
  PROMOTION_PROMOTION: 'promotion',

  DISCOUNT: 'DISCOUNT',

  // rate types // @todo update through code base
  HOURLY: 'HOURLY',
  DAILY: 'DAILY',
  WEEKLY: 'WEEKLY',
  MONTHLY: 'MONTHLY',
  DAILY_PROMO: 'DAILY_PROMO',
  RENTAL: 'RENTAL',

  PAYMENT_PROCESSOR_NA: 'PANGUI',
  PAYMENT_PROCESSOR_NA_KEY: 'pangui_response'
}

