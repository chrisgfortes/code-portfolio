export default {
  CANADA_PAY_CODE: 'CAD',
  USA_PAY_CODE: 'USD',

  US: 'US', // what you think a country code would never change?
  CA: 'CA', // what you think a country code would never change?
  GB: 'GB',
  /**
   * ECR-12022 :: Pan GUI only offers a few languages and country codes, from PanGUI Light Spec.doc
   * ESP/spa
   * CAN/fra
   * BRA/por
   * USA/eng
   */
  PANGUI_LANG_MAP: {
    'en': { // our code // country doesn't matter
      countryCode: 'USA', // their code
      lang: 'eng' // their code
    },
    'es': {
      countryCode: 'ESP',
      lang: 'spa'
    },
    'fr': {
      countryCode: 'CAN',
      lang: 'fra'
    }
  },

  // USE ONLY FOR REGION TESTS
  // Actual values come from AEM
  DOMAIN_CANADA: 'co-ca',
  DOMAIN_USA: 'ecom'
}
