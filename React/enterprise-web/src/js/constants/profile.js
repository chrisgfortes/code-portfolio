/**
 * Profile and Account Related Constants
 * */

export default {
  LICENSE_ISSUE_MANDATORY: 'MANDATORY',
  LICENSE_ISSUE_OPTIONAL: 'OPTIONAL',
  SOURCE_CODE_EP: 'EPLUSECOM',
  SOURCE_CODE_RNT: 'RNTCONF1',
  SOURCE_CODE_HOME: 'RNTHOME1',

  NON_LOYALTY: 'Non-Loyalty',
  BRANCH_ENROLLED: 'Branch-Enrolled',
  ENTERPRISE_PLUS: 'Enterprise Plus', // similar in partners.js
  EMERALD_CLUB: 'Emerald Club', // similar in partners.js
  EXECUTIVE: 'Executive',
  SIGNATURE: 'Signature',

  PROFILE: 'profile',
  EDIT: 'edit',

  CONTACT_US: 'CONTACT_US',
  ROADSIDE_ASSISTANCE: 'ROADSIDE_ASSISTANCE',

  // character used for masking with GBO v2
  // https://unicode-table.com/en/2022/
  // Unicode Char: U+2022
  // Raw:          •
  // HTML code:    &#8226;
  // Entity:       &bull;
  MASK_CHAR: '•',

  PHONE_TYPES: {
    HOME: 'HOME',
    WORK: 'WORK',
    MOBILE: 'MOBILE',
    OTHER: 'OTHER'
  }
}
