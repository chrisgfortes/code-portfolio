/**
 * for global setting values that don't have another home
 */
export default {
  MAX_CREDIT_CARDS: 4,

  REGEX_MASKED_DATE: /^(\d{4}|\*{4})-(\d{2}|\*{2})-(\d{2}|\*{2})$/,

  //ToDo :: Adding temporary bullet mask regex
  // character used for masking with GBO v2
  // https://unicode-table.com/en/2022/
  // Unicode Char: U+2022
  // Raw:          •
  // HTML code:    &#8226;
  // Entity:       &bull;
  REGEX_MASK_CHAR: /^(\d{4}|\u2022{4})-(\d{2}|\u2022{2})-(\d{2}|\u2022{2})$/,

  //Masking Characters for both GBO V1 & V2
  V1_MASK_CHAR: '*',
  V2_MASK_CHAR: '\u2022',

  RENTAL_TERMS: {
    CODE: 'RTC'
  },

  // Added this to act like a global variable in booking widget to differentiate
  // clicks in dropdown locations list and outside the booking widget
  BOOKING_WIDGET_CLICK_ELEMENT: '',

  HREF: 'HREF',

  // Used by Redirect Controller
  HTTP_AKAMAI_EDGE: 'Akamai-Edgescape',
  HTTP_DEVICES: 'Akamai-Device-Characteristics',

  RESERVATION_STATUS_OP: 'OP', // i don't even know what these are
  RESERVATION_STATUS_CN: 'CN',

  EMPLOYEE_NUMBER_ERROR: 'employee_number_required',

  LICENSE: {
    OPTIONAL: 'OPTIONAL',
    MANDATORY: 'MANDATORY'
  },

  DEEPLINK_URL: 'deeplink.html'
}
