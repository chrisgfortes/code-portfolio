/**
 * AEM and other File Paths
 **/

export default {
  clientlibs: '/etc/designs/ecom/clientlibs/',
  dist: '/etc/designs/ecom/dist/css/',
  distRoot: '/etc/designs/ecom/dist/',
  fedexCorpFlow: '/corporate-accounts/fedex.html'
}
