export default {
  MESSAGES: {
    CODES: {
      DUPLICATE_ID: 'RNTR64054',
      DUPLICATE_ACC: 'RNTR64050',
      EXPIRED: 'RNTR01402'
    }
  }
}
