let EXTRAS = {
  STATUS: {
    INCLUDED: 'INCLUDED',
    MANDATORY: 'MANDATORY'
  }
}

export default EXTRAS;
