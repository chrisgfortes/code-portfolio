
export default {
  INITIAL_ZOOM: 12,
  INITIAL_COUNTRY_ZOOM: 6,
  INITIAL_RADIUS: 24,
  MAX_RESULTS: 40,
  PICKUP: 'pickup',
  DROPOFF: 'dropoff',
  BOTH: 'both',
  // as opposed to lowercase 'city' we use for properties in the state tree
  VALUES: {
    CITY: 'CITY'
  },
  PHONE_CONTACT_TYPE: 'OFFICE',
  // search result groups
  // can we normalize with below?
  TYPES: {
    AIRPORTS: 'airports',
    TRAINS: 'trains',
    PORTS: 'ports',
    BRANCHES: 'branches',
    CITIES: 'cities',
    COUNTRIES: 'countries',
    EXACT_MATCH_COUNTRIES: 'exactMatchCountries',
    PARTIAL_MATCH_COUNTRIES: 'partialMatchCountries'
  },
  // location types
  // can we normalize with the above
  STRICT_TYPE: {
    AIRPORT: 'AIRPORT',
    BRANCH: 'BRANCH',
    CITY: 'CITY',
    COUNTRY: 'COUNTRY',
    MYLOCATION: 'MY_LOCATION'
  },
  POLICIES: {
    AFTERHOURS: 'AFHR'
  },
  WALK_IN: 'WALK IN',
  OTHER: 'OTHER',
  VALIDITY: {
    OPEN: 'OPEN',
    PICKUP_INVALID: 'PICKUP_INVALID',
    DROPOFF_INVALID: 'DROPOFF_INVALID',
    CLOSED: 'PICKUP_DROPOFF_INVALID',
    BOTH_CLOSED: 'BOTH_CLOSED',
    INVALID_AT_THAT_TIME: 'INVALID_AT_THAT_TIME',
    INVALID_ALL_DAY: 'INVALID_ALL_DAY',
    VALID_STANDARD_HOURS: 'VALID_STANDARD_HOURS',
    VALID_AFTER_HOURS: 'VALID_AFTER_HOURS'
  }
}
