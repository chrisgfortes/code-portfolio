export default {
  DELIVERY: 'delivery',
  COLLECTION: 'collection',
  PICK_UP: 'Pick-up',
  RETURN: 'Return',
  SPECIAL_ERROR_MAP: {
    noVehicleAvailable: ['noVehicleAvailability'],
    CROS_RES_VEHICLE_NOT_AVAILABLE: ['noVehicleAvailability'], // Vehicle not available.
    CROS_RES_NOT_ALLOWED_CUSTOMER: ['noVehicleAvailability'], // Customer is not allowed.
    CROS_NO_VEHICLES_AVAILABLE: ['noVehicleAvailability'],
    PRICING_16007: ['noVehicleAvailability'], //no vehicles available
    PRICING_16014: ['noVehicleAvailability'], //zero inventory
    PRICING_16432: ['noVehicleAvailability'], //no rates available,
    PRICING_16401: ['noVehicleAvailability'], //no valid products,
    CROS_LOGIN_ACCOUNT_ERROR: ['dnr'],
    CROS_LOGIN_WEAK_PASSWORD_ERROR: ['changePassword']
  }
}
