/**
 * ReservationStateTree v2.0 ?
 * @todo @gbov2 determine if this is viable
 */
// import Baobab from 'baobab';

// testing an idea out ...
// import { Location } from '../classes/Location';
// import { CarSelect } from '../classes/Cars';
import { UserModelFactory } from '../factories/User';

import { ContractDetails } from '../classes/Corporate';

let contractDetails = new ContractDetails(null); // this is empty by default today, but maybe one day it won't be

export const treeObject = {
  // Cars: {
  //   carSelect: new CarSelect()
  // },
  Corporate: {
    ...contractDetails
  },
  // DeepLink: {},
  // LegalTerms: {},
  // Locations: {
  //   search: {
  //     results: {}
  //   },
  //   data: new Location()
  // },
  // Pricing: {},
  // Rental: {},
  ReservationStatusFlags: {},
  // Services: {},
  // Session: {},
  User: {
    ...UserModelFactory.model, // expanded at top level User...
    view: UserModelFactory.view, // nested under User.view
    loggedIn: false
  }
};

// let StateTree = new Baobab(treeObject);
// window.StateTree = StateTree;

// @todo: @GBOv2 clone export goes away once state tree is replaced
// export {
//   treeObject,
//   StateTree as default
// };
