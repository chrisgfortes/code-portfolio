/**
 * This should use classes to define the structure so we have those structures available elsewhere.
 * @module ModelState
 * @todo Break this up — I think we want to use "model" for something specific as opposed to everything.
 */

import { Location, ProtoLocationDate } from '../classes/Location';
import { CarSelect } from '../classes/Cars';
import { LogisticsInfo } from '../classes/Corporate';
import { ExpeditedModelState } from '../classes/User/ExpeditedModelState';

let expeditedState = new ExpeditedModelState();

export default {
  reservationSession: {}, // session

  // Location Search
  pickup: new Location(),
  dropoff: new Location({
    date: new ProtoLocationDate(2)
  }),
  sameLocation: true, // move

  carSelect: new CarSelect(),

  // Legal
  rentalTerms: {
    policy_text: null,
    code: '',
    description: ''
  },
  rentalTermsText: {
    detailedText: null
  },

  // Contracts || Corporate
  coupon: null,
  couponLabel: null,
  prepopulatedCoupon: {
    contract_number: null,
    contract_name: null,
    allow_account_removal_indicator: null
  },
  purpose: null,
  additionalInfo: [],
  pin: null,
  billingAuthorized: null,
  contractDetails: null, // ECR-14251 Corporate
  isDoNotMarketCID: false,

  commitRequestData: [],

  // Fedex
  fedexRequisitionNumber: null,
  fedexSequence: {
    alphaCode: null,
    costCenter: null,
    employeeId: null,
    requisitionNo: null
  },

  ageRange: null,
  ageLabel: null,
  age: null,

  // Pricing Data
  paymentReferenceID: null,
  savePaymentForLater: false,
  showPaymentList: false,
  paymentID: null,
  paymentType: null,
  paymentProcessor: null,
  defaultPaymentType: null,

  // Cancel / Modify
  blockModifyPickupLocation: null,
  blockModifyDropoffLocation: null,
  collectNewModifyPaymentCard: null,
  modifyFlow: false,
  cannotModifyModal: false,
  cancellationDetails: null,

  // Delivery and Collection
  delivery: new LogisticsInfo(),
  collection: new LogisticsInfo(),

  // Expedited
  ...expeditedState,

  // Application Level
  messages: null,
  queryStringData: {},

  // Deep Linking
  deepLink: null,

  // Extras
  extrasProtections: {
    protectionsRequiredAtCounter: [],
    protectionsRequiredAtCounterText: null
  },

  // ??
  destinationPriceInfoModal: false
}
