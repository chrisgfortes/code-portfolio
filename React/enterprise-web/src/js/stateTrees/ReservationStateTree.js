/**
 * Our state tree (poorly) sets some defaults using moment.js that may, or may not have
 * it's locales set yet. I've added the DateTimeController.init() method here to be sure
 * that the locales are fired + updated prior to this running, but we need a
 * better solution.
 * @Todo : GBOv2 : temp patch for state tree work ECR-13861
 */
import Baobab from 'baobab';
import { Debugger } from '../utilities/util-debug';

// @todo for @GBOv2 temp only to import clean tree data // experimental
import { treeObject } from './StateTree';

// TEMPORARY HACKING UP
import ViewState from './ViewState';
import ModelState from './ModelState';
import Facets from './Facets';

let ResStateTree = {
  debug: false,
  model: ModelState,

  ...treeObject, // experimental (see above)

  view: ViewState
};

let ReservationStateTree = new Baobab(
  ResStateTree,
  {
    facets: Facets,
    asynchronous: false,
    maxHistory: 0,
    shiftReferences: true
  }
);

Debugger.use('enterprise.log').log('Boot Startup: 02: ReservationStateTree init()');

window.ReservationStateTree = ReservationStateTree;

module.exports = ReservationStateTree;
