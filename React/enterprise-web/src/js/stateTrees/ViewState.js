/**
 * This should use classes to define the structure so we have those structures available elsewhere.
 * @module ViewState
 * View portion of the State Tree
 * @todo  Refactor heavily or even remove
 */

import { CarSelectViewState } from '../classes/Cars';
import { LocationViewState, ProtoLocationViewDate } from '../classes/Location';
import { LocationSelectFactory } from '../factories/Location'
// import { UserModelFactory } from '../factories/User';
// import moment from '../build-libs/moment-with-locales.custom';

export default {
  // vehicle select
  carSelect: new CarSelectViewState(),
  // location search
  pickup: new LocationViewState(),
  dropoff: new LocationViewState({
    date: new ProtoLocationViewDate(2)
  }),
  recentLocationSearches: [],
  locationSelect: LocationSelectFactory.toState(),
  locationView: 'pickup',
  // this "looks" like it's location search related ... I dunno I can't find a reference to it
  // decoupledComponents: {
  //   pickup: {
  //     date: moment(),
  //     viewDate: moment()
  //   },
  //   dropoff: {
  //     date: moment(),
  //     viewDate: moment()
  //   }
  // },

  // ECR-14250
  // ...UserModelFactory.view,

  currentView: null,
  currentViewBreakpoint: 'default',
  dateFormat: 'MM/DD/YYYY',
  timeFormat: 12,
  bookingWidgetExpanded: false,
  externalBookingLocation: null,
  //modify: false,
  inflightModify: {
    modal: false,
    isInflight: false
  },
  invalidDate: false,
  currentHash: null,
  loadingClass: '',
  componentToRender: null,
  initialDataReady: null,
  bookingWidget: {
    loading: false,
    errors: null
  },
  employeeNumberField: { // ECR-14250
    errors: null
  },
  existingReservations: { // ECR-14250
    loading: false,
    reservations: [],
    search: {
      visibility: true
    },
    myTrips: {
      startRecord: {
        UPCOMING: 0,
        CURRENT: 0,
        PAST: 0
      },
      more: {
        UPCOMING: false,
        CURRENT: false,
        PAST: false
      },
      loading: {
        UPCOMING: false,
        CURRENT: false,
        PAST: false
      },
      UPCOMING: [],
      CURRENT: [],
      PAST: []
    },
    errors: null,
    rentalDetailsModal: false
  },
  receipt: { // ECR-14250
    modal: false,
    errors: null,
    loading: true,
    details: null
  },
  dateTime: {
    dataReady: null
  },
  dateTimeError: {
    modal: false
  },
  learnPayNowModal: {
    modal: false
  },
  extras: {
    dataReady: null,
    equipment: null,
    fuel: null,
    insurance: null,
    mandatoryExtras: [],
    includedExtras: [],
    upgraded: false,
    previousCarClass: false,
    availableUpgrades: [],
    updateQueue: [],
    blockSubmit: false,
    errors: null,
    exclusionModal: {
      modal: false,
      code: null
    },
    showExtraDetailsModal: false,
    highlightedExtra: null
  },
  reservationSteps: {
    pickupLocation: null,
    pickupDate: null,
    dropoffLocation: null,
    dropoffDate: null,
    carClass: null,
    addons: null,
    showTotals: false,
    commit: false,
    errors: null
  },
  loginWidget: { // ECR-14250
    errors: null,
    active: false,
    modal: false,
    forgot: {
      errors: null
    },
    terms: null,
    showResFlowLogin: true
  },
  verification: {
    authReviewCardDetails: null, // ECR-14250
    threeDS: {  // ECR-14250
      url: null,
      paReq: null,
      token: null
    },
    addCardSuccess: false,  // ECR-14250
    registerCardSuccess: false, // ECR-14250
    cardModal: null, // ECR-14250
    fields: null,
    errors: null,
    dataReady: null,
    showOptionalForm: false,
    modal: false,
    showPrepayTerms: false,
    issueCountry: null,
    privacyChecked: false,
    privacyPolicy: null,
    prepayPolicy: null,
    taxesFeesDisclaimer: null,
    keyFacts: {
      modal: false,
      content: null
    },
    policyModal: {
      modal: false,
      content: null,
      header: null
    },
    prepayChecked: false,
    enrollValidity: { // ECR-14250
      personal: false,
      license: false,
      address: false,
      terms: false,
      password: false,
      privacy: false
    },
    overallPolicy: false,
    cardModifyConfirmationModal: false,
    associateAccount: null,
    conflictAccountModal: {
      modal: false
    },
    DNCModal: {
      modal: false
    }
  },
  confirmation: {
    pickupHours: null,
    dropoffHours: null,
    errors: null
  },

  corporate: {
    state: null,
    modal: false,
    error: null
  },
  cancel: {
    retrieveAndCancel: false,
    modal: false
  },
  home: {
    modal: false,
    url: ''
  },
  sessionTimeout: {
    showModal: false,
    duration: 0
  },
  modify: {
    modify: false,
    rebookCancel: false,
    modal: false,
    cancel_rebook: null,
    cancel: {
      modal: false
    },
    callbackAfterAuthenticate: null
  },
  specialError: {
    noVehicleAvailability: {
      modal: false
    },
    dnr: {
      modal: false
    },
    changePassword: {
      modal: false,
      request: null
    }
  },
  geolocationError: {
    modal: false,
    error: false
  },
  redirect: {
    modal: false,
    type: null,
    country: null,
    preferredLang: null
  },
  redirectToLegacy: {
    modal: false
  },
  rightPlace: {
    modal: false,
    type: '',
    refer: {
      key: null,
      body: null,
      header: null,
      locationInfo: null,
      logo: null
    }
  },
  logout: { // ECR-14250
    modal: false
  },
  agePolicyModal: {
    modal: false
  },
  viewTermsModal: {
    modal: false
  },
  promo: {
    applicable: true
  },
  deepLink: {
    modal: null,
    errors: null
  },
  keyRentalFactsModal: {
    modal: false,
    detail: {
      show: false,
      data: null
    },
    policies: {
      protections: {
        included: [],
        optional: []
      },
      equipment: {
        included: [],
        optional: []
      },
      additional: [],
      minimumRequirements: []
    },
    disputes: {
      phone: null,
      email: null
    }
  },
  lockedCIDModal: {
    modal: false
  },
  resetPassword: { // ECR-14250
    password: {
      errors: null
    }
  },
  showExtrasProtectionsModal: false
}
