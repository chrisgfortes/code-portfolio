/**
 * @module Facets
 * Facets for the state tree.
 * Note that Baobab v2 facets become "monkeys" for some reason.
 * Maybe we shouldn't use these? I like the idea though.
 */

export default {
  agePolicy: {
    cursors: {
      policies: ['model', 'pickup', 'policies']
    },
    get: function(data) {
      // console.log('facet getter', data);
      if (!data.policies) return null;
      return data.policies.filter(function(policy) {
        return policy.code == 'AGE' || policy.code == 'REQ1';
      });
    }
  }
}
