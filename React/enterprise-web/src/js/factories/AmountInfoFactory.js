import EcomFactory from './EcomFactory';
import AmountInfo from '../classes/Pricing/AmountInfo';
import { exists } from '../utilities/util-predicates';

export default new EcomFactory({
  name: 'AmountInfoFactory',
  debug: false,
  
  toState(raw) {
    let rawModel = _.pickBy({
      code: raw.code,
      symbol: raw.symbol,
      amount: raw.amount,
      format: raw.format,
      formattedAmount: raw.formatted_amount,
      decimalDelimiter: raw.decimal_separator
    }, exists);
    return new AmountInfo(rawModel);
  }
})