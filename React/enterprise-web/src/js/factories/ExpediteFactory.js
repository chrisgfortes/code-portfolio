import EcomFactory from './EcomFactory';
import { SERVICE_ENDPOINTS } from '../constants';

/**
 * @todo :: combine with User/ExpeditedFactory
 * @module ExpeditedFactory
 * Transform session related data to and from service and state formats
 * @type {EcomFactory}
 * @param {Object} describing this factory
 * Take apart or put together data bits for the expedited features
 */
export default new EcomFactory({
  name: 'ExpeditedFactory',
  debug: false,

  currentSessionToStateFormat(response, data) {
    // const reservationSession = this.get(response, 'reservationSession');
    const cros = this.get(response, 'cros');
    const additionalData = this.get(cros, 'additional_data');

    let expediteEligible;
    if (this.exists(additionalData)) {
      expediteEligible = this.get(additionalData, 'expedite_eligible');
    }
    this.setStateData({
      expediteEligible
    })
  },

  // format data
  toState(response, data) {
    let { service } = data;

    if (this.exists(service)) {

      switch (service) {
        case SERVICE_ENDPOINTS.SESSION_CURRENT:
          // this.logger.log(response, data);
          this.currentSessionToStateFormat(response, data);
          break;
        default:
          console.warn(`no matching SERVICE_ENDPOINT in ${this.name}`);
      }

    } else {
      EcomFactory.serviceFail(this.name);
    }
    return this.stateData;
  }
});
