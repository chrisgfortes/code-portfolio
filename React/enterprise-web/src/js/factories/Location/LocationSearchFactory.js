/**
 * @module LocationSearchFactory
 */
import { LOCATION_SEARCH } from '../../constants';

import EcomFactory from '../EcomFactory';

let {
  AIRPORTS,
  CITIES,
  TRAINS,
  PORTS,
  BRANCHES,
  COUNTRIES,
  EXACT_MATCH_COUNTRIES,
  PARTIAL_MATCH_COUNTRIES
} = LOCATION_SEARCH.TYPES;

/**
 * @type {EcomFactory}
 * @param {Object} params
 */
export default new EcomFactory({
  name: 'LocationSearchFactory',
  debug: false,

  /**
   * The results we get back from Location Search (SOLR) are broken out by type.
   * @param res
   * @returns {boolean}
   */
  hasNoLocationResults(res) {
    let vals = Object.values(res);
    return vals.every((x) => x.length === 0 || (x.constructor === Object && Object.values(x).every((y) => y.length === 0)));
  },

  /**
   * Used by LocationInput to control the order things appear in the dropdown in
   * @function getLocationArrayModel
   * @memberOf LocationSearchFactory
   * @param  {object} res LocationSearchModel, rouhgly
   * @return {Array}     multi-dimentional array of items
   * @todo : reconcile with below
   */
  getLocationArrayModel (res) {
    return [
      [COUNTRIES, this.get(res, EXACT_MATCH_COUNTRIES)],
      [AIRPORTS, this.get(res, AIRPORTS)],
      [TRAINS, this.get(res, TRAINS)],
      [PORTS, this.get(res, PORTS)],
      [BRANCHES, this.get(res, BRANCHES)],
      [CITIES, Array.concat(this.get(res, CITIES),
                            this.get(res, PARTIAL_MATCH_COUNTRIES))
      ]
    ]
  },

  /**
   * @param  {object} res service response cities/countries results array
   * @param {string} type type to add
   * @return {object} updated cities/countries results array
   */
  addLocationType(res, type) {
    return (res || []).map(location => ({ ...location, ...{'type': type, 'locationType': type}}))
  },

  /**
   * @param  {object} res  SOLR service response object
   * NOTE we don't want to override location types ANYWHERE ELSE
   * NOTE we ONLY NEED TO override location types for SOLR responses.
   * GMA on INITIATE expects one of these: CITY, BRANCH, MY_LOCATION, AIRPORT, COUNTRY;
   * @param  {string} type pickup, dropoff, etc.
   * @return {object}      baobab update object
   * @todo  potentially convert to use a model
   * @todo  reconcile with getLocationSearchModel above
   */
  getLocationSearchModel(res, type) {
    let branchResponse = this.addLocationType(this.get(res, 'branches'), LOCATION_SEARCH.STRICT_TYPE.BRANCH);
    let citiesResponse = this.addLocationType(this.get(res, 'cities'), LOCATION_SEARCH.STRICT_TYPE.CITY);
    let exactResponse = this.addLocationType(this.get(res, 'countries.exactMatches'), LOCATION_SEARCH.STRICT_TYPE.COUNTRY);
    let partialResponse = this.addLocationType(this.get(res, 'countries.partialMatches'), LOCATION_SEARCH.STRICT_TYPE.COUNTRY);
    let portsResponse = this.addLocationType(this.get(res, 'portsOfCall'), LOCATION_SEARCH.STRICT_TYPE.BRANCH); // treat ports like branches ...
    let railResponse = this.addLocationType(this.get(res, 'railStations'), LOCATION_SEARCH.STRICT_TYPE.BRANCH);
    let airResponse = this.addLocationType(this.get(res, 'airports'), LOCATION_SEARCH.STRICT_TYPE.BRANCH);
    return {
      [type] : {
        location: {
          results: {
            $set: {
              [AIRPORTS]: airResponse,
              [CITIES]: citiesResponse,
              [TRAINS]: railResponse,
              [PORTS]: portsResponse,
              [BRANCHES]: branchResponse,
              [EXACT_MATCH_COUNTRIES]: exactResponse,
              [PARTIAL_MATCH_COUNTRIES]: partialResponse
            }
          }
        }
      }
    };
  },

  /**
   * Location Query against SOLR for whatever someone types into the LocationInput
   * This is really used for Location Results Model on typed input searches, not map searches
   * @param {object} response
   * @param {object} data
   * @property data.type = 'pickup'|'dropoff'
   */
  toState(response, data) {
    this.resetState();
    this.logger.log('getLocationSearchResult', response, data);
    let typeFor = data.type;
    let resultSet = this.getLocationSearchModel(response, typeFor);
    let hasResultSet = !this.hasNoLocationResults(response);
    let firstResult;
    if (hasResultSet) {
      let path = this.get(resultSet[typeFor], 'location.results.$set.branches[0]');
      if (path) {
        firstResult = {
          countryCode: this.get(path, 'countryCode'),
          latitude: this.get(path, 'latitude'),
          longitude: this.get(path, 'longitude'),
          name: this.get(path, 'locationNameTranslation') + ', ' + this.get(path, 'city') + ', ' + this.get(path, 'state') + ', ' + this.get(path, 'countryCode')
        };
      } else {
        firstResult = void 0;
      }
    }
    this.setStateData({
      hasResults: hasResultSet,
      results: resultSet,
      firstResult
    });
    return this.stateData;
  }
})
