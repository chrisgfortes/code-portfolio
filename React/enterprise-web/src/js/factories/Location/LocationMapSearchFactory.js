/**
 * @module LocationSearchMapFactory
 */

import EcomFactory from '../EcomFactory';

import { LOCATION_SEARCH } from '../../constants';
import { Enterprise } from '../../modules/Enterprise';
let enterprise = Enterprise.global;

/**
 * @type {EcomFactory}
 * @param {Object} params
 */
export default new EcomFactory({
  name: 'LocationMapSearchFactory',
  debug: false,

  /**
   * Location Searches like Spatial Search
   * @todo  ECR-14192 - spatial search could be passed through this but isn't
   */
  // toState(response, data) {
  //   this.resetState();

  //   this.setStateData({
  //   });

  //   return this.stateData;
  // },

  /**
   * @function toServer
   * @memberOf LocationMapSearchFactory
   * @param  {object} rawData data to format for sending to server
   * @return {object}
   * rawData = {
   *   pickupDate,
   *   pickupTime
   *   dropoffDate,
   *   dropoffTime,
   *   radius
   * } from LocationSearchController.doSpatialSearch()
   */
  toServer(rawData) {
    this.resetState();

    let pickupDate = this.get(rawData, 'pickupDate');
    pickupDate = pickupDate && pickupDate.format('YYYY-MM-DD');
    let pickupTime = this.get(rawData, 'pickupTime')
    pickupTime = pickupTime && pickupTime.format('HH:mm');
    let dropoffDate = this.get(rawData, 'dropoffDate');
    dropoffDate = dropoffDate && dropoffDate.format('YYYY-MM-DD');
    let dropoffTime = this.get(rawData, 'dropoffTime');
    dropoffTime = dropoffTime && dropoffTime.format('HH:mm');

    let radius = this.get(rawData, 'radius');
    let openSundays = this.get(rawData, 'openSundays');
    let locationTypes = this.get(rawData, 'locationTypes');

    this.setStateData({
      pickupDate,
      pickupTime,
      dropoffDate,
      dropoffTime,
      radius,
      openSundays,
      locationTypes,
      locale: enterprise.locale || '',
      rows: LOCATION_SEARCH.MAX_RESULTS
    })

    return this.stateData;
  }
})
