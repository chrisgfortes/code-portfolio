import EcomFactory from '../EcomFactory';
import { LOCATION_SEARCH } from '../../constants';

/**
 * @module LocationFactory
 * Transform Location related data to and from service and state formats
 * @type {EcomFactory}
 * @param {Object} describing this factory
 * LocationFactory leverages the Location and related classes to better control location objects
 * @todo ECR-14192 shouldn't we have a Location Details Model to re-use for various things that fetch details?
 *  - LocationController.getLocationDetails gets a details object
 *  - LocationSearchController.selectQuery gets a details object
 */
export default new EcomFactory({
  name: 'LocationDetailsFactory',
  debug: false,

  /**
   * @memberOf LocationDetailsFactory - Used by Get Location Details (LocationController)
   * @param  {object} response response data from hours for location
   * @param  {object} input     addional data
   * @return {object}          results
   */
  toState(response, input) {
    this.resetState();

    let hasPolicies = false;
    let location = this.get(response, 'location');
    let policies = this.get(location, 'policies');
    let type = this.get(input, 'type'); // pickup, dropoff etc.

    // @todo: does this need to be a class?
    let dropoffAfterHour = {
      allowed: location.after_hours_return,
      phone: null,
      message: null
    };

    if (location.phones && location.phones.length > 0) {
      for (let i = 0, len = location.phones.length; i < len; i++) {
        if (location.phones[i].phone_type === LOCATION_SEARCH.PHONE_CONTACT_TYPE) {
          dropoffAfterHour.phone = location.phones[i].phone_number;
        }
      }
    }

    if (policies && policies.length > 0) {
      hasPolicies = true;
      for (let i = 0, len = policies.length; i < len; i++) {
        if (policies[i].code === LOCATION_SEARCH.POLICIES.AFTERHOURS) {
          dropoffAfterHour.message = policies[i].policy_text;
        }
      }
    }

    this.setStateData({
      hasPolicies,
      location,
      policies,
      type,
      dropoffAfterHour
    })

    return this.stateData;
  }

  // toServer() {
  //
  // }

});

