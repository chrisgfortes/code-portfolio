/**
 * @module LocationModel
 * @type {EcomFactory}
 * Generic Helpers for Location Models
 * REMEMBER THERE IS A GENERIC LOCATIONFACTORY IN FACTORIES ROOT
 */

import EcomFactory from '../EcomFactory';
export LocationSearchFactory from './LocationSearchFactory';
export LocationMapSearchFactory from './LocationMapSearchFactory';
export LocationHoursFactory from './LocationHoursFactory';
export LocationDetailsFactory from './LocationDetailsFactory';
export LocationSelectFactory from './LocationSelectFactory';
export LocationCoordinatesModelFactory from './LocationCoordinatesModelFactory';
export MapFactory from './MapFactory';

export default new EcomFactory({
  name: 'LocationModel',
  debug: true,

  /**
   * Normalize fetching of the location ID which has so many
   * names your head might spin
   * @param  {object} propLocation property with a location object
   * @return {[type]}              [description]
   */
  getLocationId(propLocation) {
    let loc = propLocation;
    return this.get(loc, 'key')
        || this.get(loc, 'id')
        || this.get(loc, 'locationId')
        || this.get(loc, 'peopleSoftId')
        || this.get(loc, 'cityId');
  },

  /**
   * Normalize fetching names from Location objects
   * @todo  should this be a class?
   * Used by LocationResults and others
   */
  getLocationName(propLocation) {
    let loc = propLocation;
    return this.get(loc, 'locationNameTranslation')
        || this.get(loc, 'defaultLocationName')
        || this.get(loc, 'locationName')
        || this.get(loc, 'longName') // cities usually have a "longName"
        || this.get(loc, 'countryNameTranslation')
        || this.get(loc, 'defaultCountryName')
        || this.get(loc, 'details.name') // location details object
        || null;
  },

  /**
   * Usually used on location input search results etc.
   * We arbitrarily seem to use "locationType" and "type" interchangably ... (this should stop)
   * @param  {object} propLocation location object that was selected or is being used
   * @return {string}              type of location
   */
  getLocationType(l) {
    return this.get(l, 'locationType') || this.get(l, 'type'); // check locationType first as it's been more reliable (see below)
  }

})
