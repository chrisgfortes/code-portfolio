
import EcomFactory from '../EcomFactory';
import {
  LocationResults,
  LocationCleanModel
} from '../../classes/Location';

export default new EcomFactory({
  name: 'LocationCleanFactory',
  debug: false,

  /**
   * Return a clean state tree model
   * @param  {string} directionStr direction string
   * @return {Object}
   */
  toState(directionStr) {
    return new LocationCleanModel(directionStr, new LocationResults())
  }
});
