/**
 * Sets the location coordinates model used by Google Maps via Baobab.
 * Typically stored in data.model[pickup|dropoff].location
 *
 * @param      {number}  lat        The lat
 * @param      {number}  longitude  The longitude
 * @return     {Object}  object with coordinate properties
 */
export default function LocationCoordinatesModelFactory(lat, longitude) {
  return {
    lat: {
      $set: lat
    },
    longitude: {
      $set: longitude
    }
  }
}
