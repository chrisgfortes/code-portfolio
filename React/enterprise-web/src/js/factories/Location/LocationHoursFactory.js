import EcomFactory from '../EcomFactory';
import { LOCATION_SEARCH } from '../../constants';

/**
 * @module LocationFactory
 * Transform Location related data to and from service and state formats
 * @type {EcomFactory}
 * @param {Object} describing this factory
 * LocationFactory leverages the Location and related classes to better control location objects
 */
export default new EcomFactory({
  name: 'LocationHoursFactory',
  debug: false,

  /**
   * Used by BookingWidgetModelController to format hours data for state
   * @memberOf LocationHoursFactory
   * @param  {object} response response data from hours for location
   * @param  {object} input     addional data
   * @return {object}          results
   */
  toState(response, input) {
    this.resetState();

    let data = this.get(response, 'data');
    let {
      type
    } = data;

    let date;
    let closedDates = [];

    if (data) {
      for (date in data) {
        if (data[date].STANDARD && data[date].STANDARD.closed) {
          if (type === LOCATION_SEARCH.DROPOFF && (data[date].DROP && !data[date].DROP.closed)) continue;
          closedDates.push(date);
        }
      }
    }
    this.setStateData({
      closedDates: closedDates,
      data
    })

    return this.stateData;
  },

  /**
   * Used by BookingWidgetModelController to format data for Hours for location endpoint
   * @memberOf LocationHoursFactory
   * @param  {object} data data object to parse and collect data from
   * @return {object}      state data in object format to sent to server
   */
  toServer(data) {
    this.resetState();

    let {
      reservationsInitiateRequest,
      date,
      type
    } = data;

    let locationType = {pickup: LOCATION_SEARCH.PICKUP, dropoff: 'return'}[type];

    if (
      reservationsInitiateRequest &&
      reservationsInitiateRequest[`${locationType}Location`] &&
      reservationsInitiateRequest[`${locationType}Location`].dateTime
    ) {
      date = moment(reservationsInitiateRequest[`${locationType}Location`].dateTime);
    }

    this.setStateData({
      from: date.clone().startOf('month').format('YYYY-MM-DD'),
      to: date.clone().startOf('month').add(2, 'month').startOf('month').format('YYYY-MM-DD')
    });
    return this.stateData;
  }

});

