
/**
 * @module MapFactory
 * Used by LocationSearchController and Map.js to fetch map models.
 * Not a typical factory but it felt important to collect the data structure
 *   in a more obvious place.
 */

/**
 * @function getMapBoundsModel
 * @param  {GoogleMap} gMap Google Map Instance from the Google Maps API
 * @return {object}      object containing bounds and various other bits broken out
 */
export function getMapBoundsModel(gMap) {
  // https://developers.google.com/maps/documentation/javascript/reference#Map
  // https://developers.google.com/maps/documentation/javascript/reference#LatLngBounds
  let bounds = gMap.getBounds();
  let northEast = bounds.getNorthEast();
  let southWest = bounds.getSouthWest();
  // https://developers.google.com/maps/documentation/javascript/reference#Map
  let mapCenter = gMap.getCenter();
  // https://developers.google.com/maps/documentation/javascript/reference#LatLng
  let center = {
    longitude : mapCenter.lng(),
    latitude : mapCenter.lat()
  }

  return {
    bounds,
    mapCenter,
    center,
    northEast,
    southWest
  }
}

export default {
  getMapBoundsModel
}
