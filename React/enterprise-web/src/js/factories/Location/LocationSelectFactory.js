/**
 * @module LocationSelectFactory
 * For ReservationStateTree view locationSelect
 * Combine LocationSelectView and LocationSelectViewMap for View State
 */

import EcomFactory from '../EcomFactory';
import {
  LocationSelectView,
  LocationSelectViewMap
} from '../../classes/Location';
import { LOCATION_SEARCH } from '../../constants';

/**
 * @function selectTemplate
 * @param  {LocationSelectViewMap} obj LocationSelectViewMap instance
 * @return {object}
 * This could have been another model but that seemed extreme
 */
function selectTemplate(obj) {
  return {
    map: obj,
    invalidTime: false,
    invalidDate: false,
    validHours: null
  }
}

/**
 * @module LocationSelectFactory
 * @type {EcomFactory}
 */
export default new EcomFactory({
  name: 'LocationSelectFactory',
  debug: false,

  toState() {
    this.resetState();

    let locationSelect = new LocationSelectView();
    let locationDrop = new LocationSelectViewMap();
    let locationPickup = new LocationSelectViewMap();

    let pickup = selectTemplate(locationPickup);
    let dropoff = selectTemplate(locationDrop);

    locationSelect[LOCATION_SEARCH.PICKUP] = pickup;
    locationSelect[LOCATION_SEARCH.DROPOFF] = dropoff;

    let returnState = Object.assign({}, locationSelect);

    this.setStateData(returnState);

    this.logger.log('stateData:', this.stateData);

    return this.stateData;
  }
});
