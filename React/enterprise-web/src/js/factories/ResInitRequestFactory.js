import EcomFactory from './EcomFactory';

import DateTimeField from '../classes/DateTimeField';
import LocationFactory from './LocationFactory';
import LocationModel from './Location';

import { LOCATION_SEARCH } from '../constants';
import { Enterprise } from '../modules/Enterprise';
let enterprise = Enterprise.global;

/**
 * ECR-13850 - use this and ReservationStatusFactory for reservation flow profile flags
 * @module ResInitRequestFactory
 * @type {EcomFactory}
 * @param {Object} describing this factory
 */
export default new EcomFactory({
  name: 'ResInitRequestFactory',
  debug: false,

  // toState(response, derivedData) {
  toState(response) {
    this.resetState();
    let reservationsInitiateRequest = this.get(response, 'reservationSession.reservationsInitiateRequest');
    let additionalInformation = this.get(reservationsInitiateRequest, 'additional_information');
    let bookingWidgetSelectedCarClass = this.get(reservationsInitiateRequest, 'bookingWidgetSelectedCarClass');
    let contractNumber = this.get(reservationsInitiateRequest, 'contract_number');
    let dropoffDateTime = this.get(reservationsInitiateRequest, 'returnLocation.dateTime');
    let dropoffLocation = this.get(reservationsInitiateRequest, 'returnLocation');

    // @todo the next three fields are LoyaltyBook related
    // Used in UserFactory and LoyaltyBook for things like EC Unauth
    let lastName = this.get(reservationsInitiateRequest, 'last_name');
    let loyaltyBrand = this.get(reservationsInitiateRequest, 'loyalty_brand');
    let membershipId = this.get(reservationsInitiateRequest, 'membership_id');

    let pickupDateTime = this.get(reservationsInitiateRequest, 'pickupLocation.dateTime');
    let pickupLocation = this.get(reservationsInitiateRequest, 'pickupLocation');
    let redemptionDayCount = this.get(reservationsInitiateRequest, 'redemption_day_count');
    let renterAge = this.get(reservationsInitiateRequest, 'renter_age');
    let sameLocation = this.get(reservationsInitiateRequest, 'sameLocation');
    let showRedemptionRateToClient = this.get(reservationsInitiateRequest, 'show_redemption_rate_to_client');
    dropoffDateTime = this.exists(dropoffDateTime) ? moment(dropoffDateTime) : void 0;
    pickupDateTime = this.exists(pickupDateTime) ? moment(pickupDateTime) : void 0;

    // @todo USE A MODEL that matches reservationsInitiateRequest
    // THIS SHOULD MATCH WHAT SHOULD BE USED FOR toServer ...
    this.setStateData({
      additionalInformation,
      bookingWidgetSelectedCarClass,
      contractNumber,
      dropoffDateTime, // note this is a moment object
      dropoffLocation,
      lastName,
      loyaltyBrand,
      membershipId,
      pickupDateTime, // note this is a moment object
      pickupLocation,
      renterAge,
      redemptionDayCount,
      sameLocation,
      showRedemptionRateToClient,

      // @todo: @gbov2: TEMP DO NOT RETURN WHOLE CHUNKS OF DATA NEED TO GET OUT OF THIS HABIT
      reservationsInitiateRequest
    });
    this.logger.log(this.stateData);
    return this.stateData;
  },

  /**
   * @function toServer
   * @memberOf ResInitRequestFactory
   * @param  {object} submitData - date obtained from ResInitiateActions.getInitData()
   * @return {object} data - data to submit to init endpoint
   * @todo : convert to some type of real class/model!!
   */
  toServer(submitData) {
    this.resetState();
    let {
      renterAge
    } = submitData;
    const {
      additionalInformation,
      // contractDetails,
      // costCenterValue,
      coupon,
      // employeeNumber,
      loyaltyBook,
      dropoffDate,
      dropoffLocationObj,
      dropoffTime,
      pickupDate,
      pickupLocationObj,
      pickupTime,
      pin,
      preSelectedCarClass,
      // queryStringData,
      sameLocation,
      // session,
      purpose//,
      // fedexSequence
    } = submitData;

    // @todo: use DateTimeField
    // const formattedPickupTime = (pickupDate && pickupTime) ? pickupDate.format('YYYY-MM-DD') + pickupTime.format('THH:mm') : null;
    // const formattedDropoffTime = (dropoffDate && dropoffTime) ? dropoffDate.format('YYYY-MM-DD') + dropoffTime.format('THH:mm') : null;
    const formattedPickupTime = new DateTimeField(pickupDate, pickupTime).get();
    const formattedDropoffTime = new DateTimeField(dropoffDate, dropoffTime).get();

    // ECR-11320
    // 02 adjust age if not set
    if (!renterAge) {
      renterAge = 25;
    }

    if (sameLocation && LocationModel.getLocationType(pickupLocationObj) === LOCATION_SEARCH.STRICT_TYPE.MYLOCATION) {
      dropoffLocationObj.locationType = LOCATION_SEARCH.STRICT_TYPE.MYLOCATION;
    }

    let data = {
      pickupLocation: LocationFactory.getServiceInitLocationModel(pickupLocationObj, formattedPickupTime),
      returnLocation: LocationFactory.getServiceInitLocationModel(dropoffLocationObj, formattedDropoffTime),
      contract_number: coupon,
      renter_age: renterAge,
      country_of_residence_code: enterprise.countryCode,
      enable_north_american_prepay_rates: !enterprise.hidePrepay,
      sameLocation: sameLocation,
      view_currency_code: (enterprise.selectedCurrency ? enterprise.selectedCurrency.toUpperCase() : 'USD') //ECR-11805
    };

    // 07 tweak purpose
    if (purpose) {
      data.travel_purpose = purpose.toUpperCase();
    }

    // 08 set pin
    if (pin) {
      data.auth_pin = pin;
    }

    // 10 set preselectedCarClass
    if (preSelectedCarClass) {
      data.bookingWidgetSelectedCarClass = preSelectedCarClass;
    }

    // THIS IS USED FOR EC UNAUTHENTICATED RESERVATION FLOW ECR-10792
    if (
      loyaltyBook.last_name &&
      loyaltyBook.membership_id &&
      loyaltyBook.loyalty_brand
    ) {
      let {
        last_name,
        membership_id,
        loyalty_brand
      } = loyaltyBook;
      Object.assign(data, {last_name}, {membership_id}, {loyalty_brand});
      // data.loyaltyBook = loyaltyBook;
    }
    if (additionalInformation) {
      data.additional_information = additionalInformation;
    }
    this.logger.log('stateData: ', this.stateData);
    this.setStateData(data)
    return this.stateData;
  }

});

