import EcomFactory from './EcomFactory';
import { combine } from '../utilities/util-object';
import { SERVICE_ENDPOINTS } from '../constants';
import {
  AdditionalInformationFactory,
  AnalyticsFactory,
  CarSelectFactory,
  CorporateFactory,
  DeepLinkFactory,
  ExpediteFactory,
  LocationFactory,
  ReservationStatusFactory,
  ResInitRequestFactory,
  PricingFactory,
  MessagesFactory,
  UserFactory
} from '../factories/';

function getModel(object, path) {
  return _.get(object, path, {});
}

/**
 * @module SessionFactory
 * Transform session related data to and from service and state formats
 * @type {EcomFactory}
 * @param {Object} describing this factory
 * Take apart or put together data bits for the session service
 */
const SessionFactory = new EcomFactory({
  name: 'SessionFactory',
  debug: false,

  /**
   * @todo use with currentSessionToStateFormat below
   * @todo do something BETTER with a global session model/class
   */
  getSessionTemplate(response, data, fields = {}) {
    console.warn('getSessionTemplate()', response, data, fields);
    return {
      reservationStatusFlags: ReservationStatusFactory.toState(response, data),
      resInitRequestData: getModel(fields, 'resInitRequestData'),
      additionalInfo: getModel(fields, 'additionalInfo'),
      corporateData: getModel(fields, 'corporateData'),
      deepLinkData: getModel(fields, 'deepLinkData'),
      expediteData: getModel(fields, 'expediteData'),
      location: getModel(fields, 'location'),
      carData: getModel(fields, 'carData'),
      pricingData: getModel(fields, 'pricingData'),
      messagesData: getModel(fields, 'messagesData'),
      userData: getModel(fields, 'userData'),
      analyticsFields: getModel(fields, 'analyticsFields'),
      session: getModel(fields, 'session')
    };
  },

  /**
   * Extract data from current-session Session Service
   * @memberOf SessionFactory
   * @method currentSessionToStateFormat
   * @param {object} response
   * @param {object} data
   */
  currentSessionToStateFormat(response, data) {
    this.resetState();
    this.logger.groupCollapsed('currentSessionToStateFormat()');

    this.collect(data); // this *will* make a copy!
    this.logger.log('currentSessionToStateFormat', data, this.collected);
    /**
     * @todo: @GBOv2: For all these toState() calls we need to refine how the second argument works.
     * data collected from previous factories is nice but we (a) don't always need all of it
     * though (b) picking and choosing is looking/feeling/acting a bit cumbersome.
     * Question: These factories will be re-used so ... is that a consideration?
     */

    /**************** 010 Reservation Status Flags, tests, etc. ********************************************************/
    let reservationStatusFlags = ReservationStatusFactory.toState(response, this.collected);
    let { // pull these apart for other Factories, used below, (optional)
      reservationStatus,
      hasLocationDetailsFromPage,
      hasReservationInitiate,
      chargeType,
      hasCarClassSelected,
      hasCommitRequest,
      prepopulatedCid,
      globalCid,
      hasGlobalOrPrepopulatedCid
  } = reservationStatusFlags;
    // save results for others (optional)
    this.collect({reservationStatusFlags});

    /**************** 020 Reservation Initiate Request Data ************************************************************/
    let resInitRequestData = ResInitRequestFactory.toState(response, this.collected);
    // save results for others (optional)
    this.collect({resInitRequestData});

    /**************** 030 Additional Information Fields ****************************************************************/
    let additionalInfo = AdditionalInformationFactory.toState(response, this.collected);
    let {
      additionalInfoIds
    } = additionalInfo;
    // save results for others (optional)
    this.collect({additionalInfo});

    /**************** 035 Driver and User Data *************************************************************************/
    // ECR-14250
    let userData = UserFactory.toState(response, this.collected);
    this.collect({userData});
    this.logger.log('userdata:', userData);

    /**************** 040 Corporate / B2B / Contracts Data *************************************************************/
    // For Corporate we only need some of the collected data, so we just grab what we need and combine it with the incoming data
    let businessInput = combine({}, data, { prepopulatedCid, globalCid, hasGlobalOrPrepopulatedCid, additionalInfoIds, reservationStatus, hasCarClassSelected, hasCommitRequest });
    let corporateData = CorporateFactory.toState(response, businessInput);
    // save results for others (optional)
    this.collect({corporateData});
    this.logger.log('corporateData:', corporateData);

    /**************** 050 DEEP LINKING DATA (THIS NEEDS SERIOUS HELP ***************************************************/
    // deep linking code currently just copies from old SessionService/Controller code. What it does is crazy.
    // this was the old "deepLinkDataMerge" - has no business being inside a Factory!!
    let deepLinkData = DeepLinkFactory.toState(response, this.collected);
    this.logger.log('deepLinkData:', deepLinkData);

    /**************** 060 Expedited Data *******************************************************************************/
    let expediteData = ExpediteFactory.toState(response, this.collected);

    let location;
    let carData;
    let pricingData;
    let messagesData;

    // setTree is set by getCurrentSession call. defaults to true
    // if (data.setTree) {
    /**************** 070 Location Data ******************************************************************************/
    location = LocationFactory.toState(response, combine({}, data, { hasReservationInitiate }, { resInitRequestData }, { hasLocationDetailsFromPage }));

    /**************** 080 Car Select Data ****************************************************************************/
    carData = CarSelectFactory.toState(response, combine({}, data, { hasCarClassSelected }, { chargeType }));
    this.collect({carData});

    /**************** 090 Pricing Payment Related Data ***************************************************************/
    pricingData = PricingFactory.toState(response, this.collected);

      /**************** 100 Messages Related Data **********************************************************************/
    messagesData = MessagesFactory.toState(response, this.collected);
    // } // end setTree flag

    /**************** 110 Analytics Data *******************************************************************************/
    let analyticsFields = AnalyticsFactory.toState(response, data);

    let session = this.get(response, 'reservationSession');

    // @todo: combine with the getStateModel above
    this.setStateData({
      reservationStatusFlags,
      resInitRequestData,
      additionalInfo,
      corporateData,
      deepLinkData,
      expediteData,
      location, // setLocationForAll(), setLocation()
        // locationFields.initiatePickupLocationDateTime setDate setTime via moment()
        // also set BookingWidget Expanded ReservationActions.setBookingWidgetExpanded(true)
        // locationFields.initiateReturnLocationDateTime setDate setTime via moment()
      carData, // set car class details
      pricingData,
      messagesData,
      userData, // ECR-14250
      analyticsFields,
      session
    });

    // @todo: @gbov2: format for State Tree
    this.logger.groupEnd();
  },

  /**
   * Extract data unique to Update Session
   * @memberOf SessionFactory
   * @param {object} response
   * @param {object} data
   * @todo: pull out data correctly...
   */
  updateCurrentSessionToState(response, data) {
    // hack of a workaround. need to do this right...
    // however, sort of nice we can parse once and do something else with the data
    // when it's returned, right?
    this.logger.log('updateCurrentSessionToState data: ', data.service, response);
    Object.assign(data, {
      service: SERVICE_ENDPOINTS.SESSION_CURRENT
    })
    this.logger.log('updateCurrentSessionToState data: ', data.service, response);
    this.currentSessionToStateFormat(response, data);
  },

  // format data
  toState(response, data) {
    let { service } = data;

    // this.logger.log(service);

    if (this.exists(service)) {

      switch (service) {
        case SERVICE_ENDPOINTS.SESSION_CURRENT:
          this.logger.log('Calling SessionFactory for', SERVICE_ENDPOINTS.SESSION_CURRENT);
          this.currentSessionToStateFormat(response, data);
          break;
        case SERVICE_ENDPOINTS.SESSION_UPDATE + '/update':
          this.logger.log('Calling SessionFactory (update) for', SERVICE_ENDPOINTS.SESSION_UPDATE);
          this.updateCurrentSessionToState(response, data);
          break;
        default:
          console.warn(`no matching SERVICE_ENDPOINT in ${this.name}`);
      }

    } else {
      EcomFactory.serviceFail(this.name);
    }
    return this.stateData;
  }
});

export default SessionFactory;
