import EcomFactory from './EcomFactory';
import { ContractDetailsFactory } from './Corporate';
import SessionController from '../controllers/SessionController';
import { Debugger } from '../utilities/util-debug';
import { Enterprise } from '../modules/Enterprise';
let enterprise = Enterprise.global;

/**
 * @todo MOVE TO ./Corporate folder!!!!
 * @module CorporateFactory
 * @type {EcomFactory}
 * @param {Object}
 */
export default new EcomFactory({
  name: 'CorporateFactory',
  debug: false,

  toState(response, data) {
    this.resetState();

    this.logger.log('toState()', data);

    let { reservationStatus, additionalInfoIds, currentStep,
      hasCarClassSelected, hasCommitRequest,
      prepopulatedCid, globalCid, hasGlobalOrPrepopulatedCid // simply passed forwards
    } = data;

    const cros = this.get(response, 'cros');
    // const contractDetails = this.get(cros, 'contract_details'); // ??
    let rawContract = this.get(cros, 'contract_details') || this.get(response, 'reservationSession.contract_details');
    this.logger.log('01 rawContract:', rawContract);

    const contractDetails = ContractDetailsFactory.toState(rawContract);

    this.logger.log('02 contractDetails:', contractDetails);

    // not sure why we use one over the other (see above vs. here)
    const contractName = this.get(contractDetails, 'contract_name');
    const isFedexReservation = this.get(response, 'reservationSession.fedexReservation') || !!enterprise.b2b; // shouldn't we check `enterprise.b2b` to see if it's fedex?
    // we don't combine prepopulated or Global CIDs until we prompt the user with the MultipleCID modal if the scenario comes up.
    // See CorporateActions for more information on the conditions there (until it's moved to a controller, anyway)
    // ECR-14251 this same pattern is in CorporateActions and MultipleCID
    const contractNumber = ( // why are we not retrieving it form contractDetails?
      this.get(response, 'reservationSession.reservationsInitiateRequest.contract_number') || // this should be from the ResInitRequest Factory
      this.get(response, 'reservationSession.profile.basic_profile.customer_details.contract_number') // also set in UserFactory
    );

    this.logger.log(reservationStatus, contractName, currentStep, hasCarClassSelected);

    // do we need to refresh?
    // @Todo: @GBOV2: can we move this to SessionController?
    if (SessionController.sessionNeedsRefresh(reservationStatus, contractName, currentStep, hasCarClassSelected)) {
      this.logger.log('sessionNeedsRefresh passed');
      const options = {
        additionalInfoToKeep: isFedexReservation ? [] : additionalInfoIds,
        removeDatetimeAndLocation: isFedexReservation
      };
      SessionController.clearAndGetNewSession(options);
      Debugger.use('enterprise.log').warn('Based on CorporateFactory, we need to refresh session. Did we?');
      return {};
    }

    if (hasCommitRequest) {
      let commitData = this.get(response, 'reservationSession.commitRequest');
      this.setStateData({
        commitData
      })
    }

    // @todo: @gbov2: format for State Tree
    this.setStateData({
      contractName,
      isFedexReservation,
      contractDetails,
      contractNumber,
      prepopulatedCid,
      globalCid,
      hasGlobalOrPrepopulatedCid
    });

    return this.stateData;

  }
})

