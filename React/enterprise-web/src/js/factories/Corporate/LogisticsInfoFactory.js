import EcomFactory from '../EcomFactory';
//import { LogisticsInfo } from '../../classes/Corporate';
import { exists } from '../../utilities/util-predicates';
import { validateMaskedInfo } from '../../utilities/util-validate';

/**
 * @module LogisticsInfoFactory
 * Creates server data from a Logistics Info data model
 * @type {EcomFactory}
 * @param {Object} describing this factory
 */
export default new EcomFactory({
  name: 'LogisticsInfoFactory',
  debug: false,

  toServer(model, countryCode) {
    const phone = validateMaskedInfo(model.phone);
    return _.pickBy({
      address: _.pickBy({ // @todo - make a recursive pickBy...
        street_addresses: [model.streetAddress].filter(validateMaskedInfo),
        city: validateMaskedInfo(model.city),
        postal: validateMaskedInfo(model.postal),
        country_code: validateMaskedInfo(countryCode)
      }, exists),
      phone: phone ? {
        phone_type: 'HOME',
        phone_number: phone
      } : null,
      comments: model.comments
    }, exists);
  }
});