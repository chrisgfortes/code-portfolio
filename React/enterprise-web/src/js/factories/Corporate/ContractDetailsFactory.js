import EcomFactory from '../EcomFactory';
import { ContractDetails } from '../../classes/Corporate';
import { exists } from '../../utilities/util-predicates';
// import { validateMaskedInfo } from '../../utilities/util-validate';

/**
 * @module ContractDetailsFactory
 * Creates data model from Contract Details
 * @type {EcomFactory}
 * @param {Object} describing this factory
 */
export default new EcomFactory({
  name: 'ContractDetailsFactory',
  debug: false,

  // leave raw as potentially empty. we need missing/undefined contractDetails for our code to work!
  toState(raw) {
    let rawModel;
    let result;
    if (raw) {
      rawModel = _.pickBy(raw, exists); // @todo - update model and factory to use better field names
      result = new ContractDetails(rawModel);
      this.logger.log('toState()', result);
    }
    return result;
  }
});
