/**
 * @module LocationFactory
 * LocationFactory leverages the Location and related classes to better control location objects
 * Transform Location related data to and from service and state formats
 * DO NOT forget we have extra Location Factories in factories/Location
 * @type {EcomFactory}
 * @param {Object} describing this factory
 */
import EcomFactory from './EcomFactory';
import { SERVICE_ENDPOINTS } from '../constants';
import {
  SimpleLocation,
  SelectedInputLocation,
  SelectedInputLocationExtended,
  ServiceInitLocationModel,
  SelectedLocationUpdateModel,
  LocationResults
} from '../classes/Location/';
import LocationModel from './Location';
import { Enterprise } from '../modules/Enterprise';
const enterprise = Enterprise.global;

/**
 * @module LocationFactory
 * @type {EcomFactory}
 */
const LocationFactory = new EcomFactory({
  name: 'LocationFactory',
  debug: false,

  // demo
  models: [{
    name: 'SelectedInputLocation',
    model: SelectedInputLocation
  }],

  /**
   * Return a clean results object
   * Use LocationResults object to return complex type location results model
   * @return {object} for a given direction
   */
  getLocationDirectionResults(strDirection) {
    return {
      [strDirection] : {
        location: {
          results: {
            $set: new LocationResults()
          }
        }
      }
    }
  },

  /**
   * Invoke ServiceInitLocationModel
   * @param  {object} locationObj   location object
   * @param  {Date} formattedTime date time
   * @return {ServiceInitLocationModel}
   */
  getServiceInitLocationModel(locationObj, formattedTime) {
    let locationType = LocationModel.getLocationType(locationObj);
    return new ServiceInitLocationModel(locationObj, locationType, formattedTime);
  },

  /**
   * @function getSelectedLocationUpdateModel
   * @memberOf LocationFactory
   * @param  {object} locationObj location object
   * @return {SelectedLocationUpdateModel} location class for update model
   * @todo : find usage of this method and change to just send an object, have the work done here
   */
  getSelectedLocationUpdateModel(locationObj) {
    return new SelectedLocationUpdateModel(locationObj);
  },

  /**
   * Return a customized Location Object
   * @todo : this should use a class/model similar to other location objects
   * @todo : should it extend an object like getSimpleLocationModel?
   * @param  {object} propLocation generic location object
   * @param {object|boolean} extendedProps extra properties to add
   * @return {object}              SelectedInpuLocation
   */
  getSelectedInputLocation(propLocation, extendedProps = false) {
    // this.logger.warn('.... getSelectedInputLocation()', propLocation);
    let l = propLocation;
    let locType = LocationModel.getLocationType(l);
    let keyId = LocationModel.getLocationId(l);
    let locationName = LocationModel.getLocationName(l);
    let baseSelected = {
      id: keyId,
      type: locType, // careful! sometimes this is set as pickup/dropoff ON THE OBJECT ITSELF
      locationName,
      longitude: l.longitude,
      lat: l.lat || l.latitude,
      airportCode: l.airportCode,
      countryCode: l.countryCode
    };
    // this is a bit of a hack for FEDEX for the moment...
    let costCenter =  this.get(extendedProps, 'costCenter');
    this.logger.log('getSelectedInputLocation??', propLocation, extendedProps, costCenter);
    if (!extendedProps && !costCenter) {
      return new SelectedInputLocation(baseSelected);
    } else {
      return new SelectedInputLocationExtended(baseSelected, extendedProps);
    }
  },

  /**
   * Obtain Location Data from enterprise.locationDetail
   * This is used when enterprise.locationDetail is defined.
   */
  getLocationFromPageGlobal() {
    let eld = enterprise.locationDetail;
    let location = {
      key: LocationModel.getLocationId(eld),
      locationName: LocationModel.getLocationName(eld),
      locationType: LocationModel.getLocationType(eld),
      lat: eld.lat,
      //The below long is intentional to avoid minifier from blowing up
      longitude: eld['long'],
      countryCode: eld.countryCode
    };
    return new SimpleLocation(location);
  },

  /**
   * @function getSimpleLocationModel
   * @memberOf LocationFactory
   * @todo we should change this to take both a template object and a source object
   * e.g. we pass in location, and { name: locationName, key: locationType } since if we set those before this gets
   *   them then it's not solving the problem.
   * Currently, the notes in SimpleLocationObject say it should use getLocationName etc before it's sent here, which seems
   *   backwards to me.
   * @param  {object} obj location object
   * @param {object} extendedProps override object properties to add to the object
   * NOTE: adding the extendedProps param was a half-baked attempt to address the todo above
   * @return {SimpleLocation}     object cast to SimpleLocation
   */
  getSimpleLocationModel(obj, extendedProps) {
    this.logger.log('getSimpleLocationModel()', obj, extendedProps);
    if (extendedProps) {
      Object.assign(obj, extendedProps);
    }
    let locationName = LocationModel.getLocationName(obj);
    let locationType = LocationModel.getLocationType(obj);
    let key = LocationModel.getLocationId(obj);
    Object.assign(obj, { locationName, locationType, key });
    return new SimpleLocation(obj);
  },

  /**
   * Why do we have pickup_location and pickupLocationWithDetail ?
   */
  getPickupLocationFromReponse(initiatePickupLocation, pickupLocationWithDetailID) {
    let pickupLocation;
    if (this.exists(initiatePickupLocation)) {
      pickupLocation = new SimpleLocation({
        key: pickupLocationWithDetailID || initiatePickupLocation.id,
        locationName: initiatePickupLocation.name,
        locationType: initiatePickupLocation.type,
        lat: initiatePickupLocation.latitude,
        longitude: initiatePickupLocation.longitude,
        countryCode: initiatePickupLocation.countryCode
      });
    }
    return pickupLocation;
  },

  /**
   * What do we have dropoff_location and returnLocationWithDetail ?
   */
  getDropoffLocationFromResponse(initiateReturnLocation, returnLocationWithDetailID) {
    let dropoffLocation;
    if (this.exists(initiateReturnLocation)) {
      dropoffLocation = new SimpleLocation({
        key: returnLocationWithDetailID || initiateReturnLocation.id,
        locationName: initiateReturnLocation.name,
        locationType: initiateReturnLocation.type,
        lat: initiateReturnLocation.latitude,
        longitude: initiateReturnLocation.longitude,
        countryCode: initiateReturnLocation.countryCode
      });
    }
    return dropoffLocation;
  },

  /**
   * Compare to LocationFactory.getLocationSearchResult()
   * @param {object} response
   * @param {object} data
   */
  getFedexLocationsSearchResult(response, data) {
    this.resetState();

    this.logger.log('getFedexLocationsSearchResult()', response, data);

    let typeFor = data.type;
    let fedexType = data.locationType;
    let hasResults = (fedexType === 'byCity' ? Object.keys(response).length : response.length);

    // this could be moved
    let update = {};
    update[typeFor] = {
      location: {
        results: {
          $set: {
            fedex: response
          }
        }
      }
    };

    this.setStateData({
      hasResults,
      results: update
    })
  },

  /**
   * Format data from session to state
   * @param response
   * @param data
   */
  currentSessionToStateFormat(response, data) {
    this.resetState();

    this.logger.log('currentSessionToStateFormat() incoming data:', data);

    const pickupLocationWithDetail = this.get(response, 'pickupLocationWithDetail');
    const returnLocationWithDetail = this.get(response, 'returnLocationWithDetail');
    const cros = this.get(response, 'cros');

    // VERY TORN about where to put pulling all ReservationInitiate Data. :|
    const reservationInitiateRequest = this.get(data, 'resInitRequestData'); // note this came from ResInitRequestFactory
    const initiatePickupLocation = this.get(reservationInitiateRequest, 'pickupLocation'); // note this came from other factory
    const initiateReturnLocation = this.get(reservationInitiateRequest, 'dropoffLocation') || this.get(reservationInitiateRequest, 'returnLocation'); // from other factory

    const pickupRawResponse = this.get(cros, 'pickup_location');
    // const dropoffRawResponse = this.get(cros, 'return_location');

    // this is weird. this is used as part of a test on the cros response for a key facts thing
    // it's in the test, but it's not in the data that is saved. i don't know why -- see (15)
    let pickupBrand;
    if (this.exists(pickupRawResponse)) {
      pickupBrand = this.get(pickupRawResponse, 'brand');
    }
    this.setStateData({
      pickupBrand
    });

    if (data.hasLocationDetailsFromPage) {
      let locationForAll = this.getLocationFromPageGlobal();
      this.setStateData({
        locationForAll
      })
    }

    // these are to make sure the right ID is being used
    // 00
    let pickupLocationWithDetailID = pickupLocationWithDetail && pickupLocationWithDetail.id;
    let returnLocationWithDetailID = returnLocationWithDetail && returnLocationWithDetail.id;

    // 01
    if (data.hasReservationInitiate && !data.hasLocationDetailsFromPage) {
      // this.logger.log('04');
      let pickupLocation = this.getPickupLocationFromReponse(initiatePickupLocation, pickupLocationWithDetailID);
      let dropoffLocation = this.getDropoffLocationFromResponse(initiateReturnLocation, returnLocationWithDetailID);
      this.setStateData({
        pickupLocation,
        dropoffLocation
      })
    }

    // 02
    if (data.hasReservationInitiate && this.exists(initiatePickupLocation)) {
      let initiatePickupLocationDateTime = this.get(reservationInitiateRequest, 'pickupDateTime'); // from other factory
      this.setStateData({ initiatePickupLocationDateTime });
    }
    if (data.hasReservationInitiate && this.exists(initiateReturnLocation)) {
      let initiateDropoffLocationDateTime = this.get(reservationInitiateRequest, 'dropoffDateTime'); // from other factory
      this.setStateData({ initiateDropoffLocationDateTime });
    }

    this.logger.log('state data', this.stateData);
  },

  toState(response, data) {
    let { service } = data;

    if (this.exists(service)) {

      switch (service) {
        case SERVICE_ENDPOINTS.SESSION_CURRENT:
          // this.logger.log(response, data);
          this.currentSessionToStateFormat(response, data);
          break;
        case SERVICE_ENDPOINTS.LOCATION_SEARCH_FEDEX:
          this.getFedexLocationsSearchResult(response, data);
          break;
        default:
          console.warn(`no matching SERVICE_ENDPOINT in ${this.name}`);
      }

    } else {
      EcomFactory.serviceFail(this.name);
    }
    return this.stateData;
  }

});

export default LocationFactory;
