import EcomFactory from './EcomFactory';

/**
 * This is a little bit different than other EcomFactory items due to differences
 * in how Analytics are treated.
 * @type {EcomFactory}
 */
export default new EcomFactory({
  name: 'AnalyticsFactory',
  debug: false,
  toState(response) {
    return this.get(response, 'analytics');
  }
});
