import EcomFactory from './EcomFactory';

/**
 * messages should be { messages: ... } or [], either way, formatErrors will
 * receive an array of error messages
 * This all came from the old ErrorMiddleware, so it's contrived and not really
 *   following our newer model conventions.
 * @todo: deprecate?
 * @param      {array|object}  messages  The messages
 * @return     {Array}   The messages format.
 */
export function getMessagesFormat (messages) {
  let mess = _.has(messages, 'messages') ? messages.messages : messages;
  let errors = [];
  if (mess && mess.length) {
    errors = mess.map((message) => {
      if (!message.defaultMessage) {
        message.defaultMessage = message.message || message.code;
      }
      return message;
    });
    // @todo: none of the below seems to be used/set/returned anywhere? Am I missing something?
    // const supportPhoneNumbers = ReservationStateTree.select(ReservationCursors.supportLinks).get();
    // let callUsPhoneNumber = '';
    // let supportString = i18n('calluserror_0001');
    // if (supportPhoneNumbers) {
    //   callUsPhoneNumber = supportPhoneNumbers.support_phone_numbers.filter((number) => number.phone_type === 'CONTACT_US');
    //   callUsPhoneNumber = callUsPhoneNumber.length ? callUsPhoneNumber[0].phone_number : '';
    //   supportString = supportString + ' ' + callUsPhoneNumber;
    // }
    // @todo: (see above)
  }
  return errors;
}

/**
 * @module MessagesFactory
 * Transform messages related data to and from service and state formats
 * @type {EcomFactory}
 * @param {Object} describing this factory
 */

export default new EcomFactory({
  name: 'MessagesFactory',
  debug: false,

  // format data
  toState(response, data) {
    this.resetState();

    this.logger.log('incoming data', data);

    const cros = this.get(response, 'cros');

    const messages = this.get(cros, 'messages');

    this.setStateData({
      messages
    })

    return this.stateData;
  }
})
