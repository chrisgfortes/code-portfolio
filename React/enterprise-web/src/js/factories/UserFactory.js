import EcomFactory from './EcomFactory';

import { ProfileFactory } from './User';

// ECR-14250

/**
 * @module UserFactory
 * Default UserFactory for Session derived data models
 * Transform user related data to and from service and state formats
 * @type {EcomFactory}
 * @param {Object} describing the factory
 */
export default new EcomFactory({
  name: 'UserFactory',
  debug: false,

  // @todo ECR-14250 Use Models!
  // @todo ECR-14250 Create Actual Profile!

  // format data
  toState(response, data) {
    this.resetState();

    const { resInitRequestData } = data;

    this.logger.log('resInitRequestData', resInitRequestData);

    const cros = this.get(response, 'cros');

    const driverData = this.get(response, 'reservationSession.driverInformation');
    // remove at some point
    const driverSourceData = this.get(cros, 'driver_info');
    // ECR-14250 reconcile two sources of driver info ... which to use, which to use ... ?
    // @todo pull payment profile from here

    // remove at some point
    const profileDataSource = this.get(response, 'reservationSession.profile');

    const basicProfile = this.get(profileDataSource, 'basic_profile');
    const customerContractNumber = this.get(basicProfile, 'customer_details.contract_number'); // also set in CorporateFactory

    const comarchInfo = this.get(response, 'reservationSession.comarchInfo');

    // Loyalty data is used when EC Unauth is used other situations
    // @todo this should be LoyaltyBook-related
    const loyaltyLastName = this.get(resInitRequestData, 'lastName');// || this.get(basicProfile, 'last_name');
    const loyaltyID = this.get(resInitRequestData, 'membershipId');// || this.get(basicProfile, 'individual_id'); // per GBO use the ID outside of loyalty_data
    const loyaltyBrand = this.get(resInitRequestData, 'loyaltyBrand');// || this.get(response, 'reservationSession.brand');

    const loyaltyProgram = this.get(basicProfile, 'loyalty_data.loyalty_program');
    // const paymentProfile = this.get(profileDataSource, 'payment_profile.payment_methods');
    const paymentProfile = this.get(profileDataSource, 'payment_profile'); // ECR-14250 ? I dunno
    // ECR-14250 there is a paymentProfile in the driver info sometimes too

    // let loyaltyData = this.get(basicProfile, 'loyalty_data'); // Loyalty Model
    let contactProfile = this.get(profileDataSource, 'contact_profile');
    let licenseProfile = this.get(profileDataSource, 'license_profile');
    let addressProfile = this.get(profileDataSource, 'address_profile');
    let citizenshipProfiles = this.get(profileDataSource, 'citizenship_profiles');
    let preference = this.get(profileDataSource, 'preference');

    this.logger.log('basicProfile:', basicProfile);

    if (basicProfile) {
      // refer back to UserStateModelFactory
      const profile = ProfileFactory.toState({basicProfile, addressProfile, contactProfile, licenseProfile,
            citizenshipProfiles, paymentProfile, preference});//, loyaltyData});
            // citizenshipProfiles, paymentProfile, preference, loyaltyData});
      this.setStateData({
        profile
      });
      this.logger.log('profile:', profile);
    }

    /**
     * @Todo: @GBOv2: There's a ton of stuff to do here in terms of breaking up the user Profile data and making
     * these returned values be more concise.
     */

    this.setStateData({
      driverData, // DriverInfo Model               currently used
      driverSourceData, // DriverInfo Models        curently used
      comarchInfo,
      // basicProfile, // Profile Model (basic_profile)
      customerContractNumber,
      profileDataSource, // Profile Model (profile root includes basic_profile)
      // paymentProfile, // Payment Model
      // loyaltySourceData: this.get(basicProfile, 'loyalty_data'), // Loyalty Model
      loyalty: { // Loyalty Model                   currently used
        loyaltyLastName,
        loyaltyID,
        loyaltyBrand,
        loyaltyProgram
      }
    })
    return this.stateData;
  }
});
