import EcomFactory from './EcomFactory';

export default new EcomFactory({
  name: 'SimpleExtraFactory',
  debug: false,

  toState(type, extra) {
    return {
      type: type,
      code: extra.code,
      name: extra.name,
      quantity: extra.selected_quantity
    };
  }
});
