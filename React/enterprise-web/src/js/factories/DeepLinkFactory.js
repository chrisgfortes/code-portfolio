import EcomFactory from './EcomFactory';
import { SERVICE_ENDPOINTS } from '../constants';

export default new EcomFactory({
  name: 'DeepLinkFactory',
  debug: false,

  /**
   * This code came right out of SessionService (Controller)
   * It's horrid.
   * @todo: @GBOv2 deep link
   * @param response
   * @param derivedFields
   */
  currentSessionToStateFormat(response, derivedFields) {
    this.resetState();

    this.logger.warn('*********************** This code for DeepLinkFactory / deepLinkDataMerge must be rewritten !!!!!!!!!!!!!!!!!!!!!!!!!!!!');

    const session = this.get(response, 'reservationSession');
    const cros = this.get(response, 'cros');
    const deepLinkSource = this.get(session, 'deep_link_reservation');
    const deepLinkReservation = this.get(deepLinkSource, 'reservation');
    const deepLinkInfo = this.get(session, 'deepLinkInfo'); // maybe no longer used?
    const deepLinkBillingAccount = this.get(session, 'deeplink_billing_account');
    const deepLinkDriverInfo = this.get(deepLinkReservation, 'driver_info');
    let deepLinkSameLocation;
    this.logger.log('driverInfo', deepLinkDriverInfo, deepLinkSource, deepLinkReservation);

    let deepLinkResMessages;
    if (this.exists(deepLinkSource)) {
      deepLinkResMessages = this.get(deepLinkSource, 'reservation.messages');
    }

    // this code NEEDS TO BE REWRITTEN!!!!!!
    // this is taken directly from the old SessionService and is lousy code
    // @todo: @GBOv2: old code that must be migrated to something reasonable
    if (deepLinkSource) {
      // EXTRAS
      if (cros && cros.car_class_details) {
        const deeplink_car_class_details = deepLinkSource.reservation.car_class_details;
        const deep_link_extras = deeplink_car_class_details && deeplink_car_class_details.extras;
        const cros_extras = cros.car_class_details.extras;
        const each = (keySource, callback) => Object.keys(keySource).forEach(callback);

        if (!session.manuallySelectedExtras && deep_link_extras) {
          each(deep_link_extras, (chargeType) => {
            each(deep_link_extras[chargeType], (extraType) => {
              deep_link_extras[chargeType][extraType].forEach((deepLinkExtra) => {
                [cros_extras/*, ewt_extras, session_extras*/].forEach((extra_group) => {
                  if (extra_group[chargeType] && extra_group[chargeType][extraType]) {
                    extra_group[chargeType][extraType]
                      .filter(filteredExtra => filteredExtra.code === deepLinkExtra.code)
                      .forEach(filteredExtra => {
                        filteredExtra.selected_quantity = deepLinkExtra.selected_quantity;
                      });
                  }
                });
              });
            });
          });
        }
      }

      // workaround for null info on reservationInitiateRequest with deepLinkReservation
      // for the reservation Steps, which use reservationSession instead of ewt
      const initRequest = session.reservationsInitiateRequest;
      // Little hack to fix problem when we don't receive the location name in the initRequest obj
      if (cros.res_session_id && (!initRequest.pickupLocation.name || !initRequest.returnLocation.name) &&
        deepLinkSource.stepStatus.DATETIME_LOCATION_SELECT !== 'NONE'
      ) {
        initRequest.sameLocation = (cros.pickup_location.id === cros.return_location.id);
        initRequest.pickupLocation.name = cros.pickup_location.name;
        initRequest.returnLocation.name = cros.return_location.name;
      }
      if (!cros.res_session_id) {
        const deeplinkInitRequest = deepLinkSource.reservation.reservationsInitiateRequest;
        session.reservationsInitiateRequest = _.mergeWith({}, deeplinkInitRequest, initRequest, (obj, src) => src !== null ? src : obj);
      }
      // Merge additional information
      let additionalInfoBase;
      const contractDetails = session.deep_link_reservation.reservation.contract_details;
      const contractDetailsAdditionalInfo = contractDetails && contractDetails.additional_information;

      if (session.additionalInformationAll.length) {
        additionalInfoBase = session.additionalInformationAll;
      } else if (contractDetailsAdditionalInfo && contractDetailsAdditionalInfo.length) {
        additionalInfoBase = contractDetailsAdditionalInfo;
      }
      if (deepLinkSource.reservation.additional_information.length && additionalInfoBase) {
        deepLinkSource.reservation.additional_information.forEach((info) => {
          additionalInfoBase
            .filter(ewtInfo => ewtInfo.id === info.id)
            .forEach(ewtInfo => ewtInfo.value = info.value);
        });
        session.additionalInformationAll = additionalInfoBase;
        if (cros.res_session_id) {
          cros.additional_information = additionalInfoBase;
        }
      }

      // Merge billing number
      if (!session.billing_account && deepLinkSource.reservation.billing_account) {
        session.deeplink_billing_account = deepLinkSource.reservation.billing_account.billing_account_number;
      }
      deepLinkSameLocation = initRequest.sameLocation;
      // return response;
    }

    this.setStateData({
      deepLinkSource,
      deepLinkReservation, // pretty redundant ... hmmm....
      deepLinkInfo,
      deepLinkDriverInfo,
      deepLinkResMessages,
      deepLinkBillingAccount,
      deepLinkSameLocation
    });

    this.logger.log(this.stateData);
  },

  toState(response, derivedData) {
    let { service } = derivedData;

    if (this.exists(service)) {

      switch (service) {
        case SERVICE_ENDPOINTS.SESSION_CURRENT:
          // this.logger.log(response, derivedData);
          this.currentSessionToStateFormat(response, derivedData);
          break;
        default:
          console.warn(`no matching SERVICE_ENDPOINT in ${this.name}`);
      }

    } else {
      EcomFactory.serviceFail(this.name);
    }
    return this.stateData;

  }
})
