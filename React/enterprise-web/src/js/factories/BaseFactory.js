import { Debugger } from '../utilities/util-debug';
import { exists, not, and, or } from '../utilities/util-predicates';
import { get } from '../utilities/util-object';

/**
 * BaseFactory is our baseline factory object.
 * @param name
 * @param tf
 * @constructor
 */
function BaseFactory(name, tf) {
  this.name = name;
  this.isDebug = tf;
  this.logger = new Debugger(window._isDebug, this, true, this.name);
}

// having these on the prototype may be overkill
// does each instance really need these?
BaseFactory.prototype = {
  exists: (val) => exists(val),
  not: (pred) => not(pred),
  and: (first, second) => and(first, second),
  or: (first, second) => or(first, second),
  get: (source, path) => get(source, path)
};

export default BaseFactory;
