import EcomFactory from './EcomFactory';
import { SERVICE_ENDPOINTS } from '../constants';

/**
 * @module PricingFactory
 * Transform Pricing related data to and from service and state formats
 * @type {EcomFactory}
 * @param {Object} describing this factory
 * Take apart or put together data bits for the Pricing service
 */

export default new EcomFactory({
  name: 'PricingFactory',
  debug: false,

  /**
   * @method getAppliedCancellationFees
   * @param {array} cancelFeeDetails
   * @returns {object}
   */
  getAppliedCancellationFees(cancelFeeDetails) {
    let appliedDetails;
    if (cancelFeeDetails) {
      for (let i = 0; i < cancelFeeDetails.length; i++) {
        if (this.get(cancelFeeDetails[i], 'fee_apply')) {
          appliedDetails = cancelFeeDetails[i];
        }
      }
    }
    this.logger.debug('getAppliedCancellationFees()', cancelFeeDetails, appliedDetails);
    return appliedDetails;
  },

  currentSessionToStateFormat(response, data) {
    this.logger.log('incoming data', data);

    const paymentProcessor = this.get(data, 'reservationStatusFlags.paymentProcessor');
    const collectNewCardInModify = this.get(data, 'reservationStatusFlags.collectNewCardInModify');

    const viewCurrencyCode = this.get(response, 'reservationSession.view_currency_code');
    const chargeTypeData = this.get(data, 'reservationStatusFlags.chargeType');
    // used to set CarSelectActions.setAvailablePayTypes() off current session
    // this is the way it's been done, though it seems like a default not a comprehensive setting
    const availablePayTypes = {
      [chargeTypeData]: true
    };

    const cancellationDetailsSource = this.get(response, 'cros.cancellation_details.cancel_fee_details');
    this.logger.log('cancellationDetailsSource', hasCancellationDetails);
    const hasCancellationDetails = !!(cancellationDetailsSource && cancellationDetailsSource.length > 0);
    this.logger.log(hasCancellationDetails);
    const appliedCancellationFees = this.getAppliedCancellationFees(cancellationDetailsSource);
    this.logger.log(appliedCancellationFees);

    const carRedemptionDays = this.get(response, 'reservationSession.selectedCarClassDetails.redemption_day_count');

    this.setStateData({
      paymentProcessor,
      carRedemptionDays,
      collectNewCardInModify,
      availablePayTypes,
      hasCancellationDetails,
      appliedCancellationFees,
      viewCurrencyCode
    })
  },

  // format data
  toState(response, data) {
    let { service } = data;

    if (this.exists(service)) {

      switch (service) {
        case SERVICE_ENDPOINTS.SESSION_CURRENT:
          // this.logger.log(response, data);
          this.currentSessionToStateFormat(response, data);
          break;
        default:
          console.warn(`no matching SERVICE_ENDPOINT in ${this.name}`);
      }

    } else {
      EcomFactory.serviceFail(this.name);
    }
    return this.stateData;
  }
})
