import { Debugger } from '../utilities/util-debug';
import { exists } from '../utilities/util-predicates';
import { get } from '../utilities/util-object';

/**
 * This is not your typical "Model Factory" style we use.
 * Please don't copy the patterns...
 * @class ServiceFactory
 * @static
 * NOTE THERE IS A DEBUGGER INSTANCE AT THE BOTTOM
 * @todo: @GBOv2 is this a worthwhile and constructive factory? (if we use classes for format, see below)
 * @todo: remove reference to enterprise.log and use MessageLogger or something
 * @todo: Use a class signature for determining validity of objects, etc.
 * No predefined classes are used here, which is probably unusual for a factory...
 * new Message({...}) // format from GMA/GBO
 * new promiseObject({...}) // format from EnterpriseServices
 */
class ServiceFactory {
  /**
   * Run a series of tests on the response object.
   * @param {Object} promiseObject
   * @returns {boolean}
   */
  static checkServiceResponse(promiseObject) {
    let result = ServiceFactory.checkStatus(promiseObject);
    // ServiceFactory.logger.log('checkServiceResponse()', result);
    if (result) {
      result = ServiceFactory.isValidFormat(promiseObject);
    }
    // ServiceFactory.logger.log('is still valid format?', result);
    return result;
  }

  /**
   * Check for messages coming back in the response.
   * @param promiseObject
   * Aborted requests are treated OK since we do that sometimes on purpose.
   */
  static checkNoMessages(promiseObject) {
    // ServiceFactory.logger.log('checkNoMessages()');
    let result = false;
    // if ((exists(promiseObject.responseJSON) &&
    //     (!exists(promiseObject.responseJSON.messages) || exists(promiseObject.responseJSON.messages) && promiseObject.responseJSON.messages.length === 0)
    //   ) || promiseObject.statusText === 'abort') {
    //   result = true;
    // }
    if (promiseObject) {
      result = (!ServiceFactory.checkMessages(promiseObject.responseJSON) || !ServiceFactory.checkMessages(promiseObject)) ? true : false;
    }
    // ServiceFactory.logger.log('checkNoMessages()', result);
    return result;
  }

  /**
   * @function checkMessages
   * @param  {object} response response object from ajax call
   * @return {boolean}          truthy value based on test
   */
  static checkMessages (response) {
    // ServiceFactory.logger.warn('checkMessages()', response);

    // @todo - let's try to get back to our `util-object.get` when we have the chance
    //         just keep in mind some responses are pure strings ¯\_(ツ)_/¯
    return _.get(response, 'messages.length') > 0;
  }

  /**
   * Check http headers status.
   * @todo @GBOv2 the status should be CONSTANTS
   * @param {object} promiseObject
   * @returns {boolean}
   * Aborted requests are treated OK since we do that sometimes on purpose.
   */
  static checkStatus(promiseObject) {
    // ServiceFactory.logger.log('checkStatus()');
    if (promiseObject.status === '200' || promiseObject.status === 200 || (promiseObject.status === 0 && promiseObject.statusText === 'abort')) {
      return true;
    } else {
      Debugger.use('enterprise.log').log('HTTP Status headers returned status NOT 200 OK.');
      return false;
    }
  }

  /**
   * Add strings you can safely not throw EcomError for here
   * @param  {string} err error string to look for
   * @return {boolean}     return true/false
   */
  static statusWhitelist(err) {
    // return ['success'].includes(err) ? true : false;
    return ['abort', 'success'].includes(err) ? true : false;
  }

  /**
   * This is a pretty bogus (bad/fake) example of tested data...
   * @param {object} promiseObject from jQuery XHR etc.
   * @returns {boolean}
   */
  static isValidFormat(promiseObject) {
    // ServiceFactory.logger.log('isValidFormat()');
    let result = false;
    if (!exists(promiseObject.responseJSON)) {
      result = false;
    }
    try {
      JSON.parse(promiseObject.responseText);
      return true;
    } catch (e) {
      console.error('isValidFormat(promiseObject) failed: ', e);
      result = false;
    }
    return result;
  }

  /**
   * Do something good with Messages
   * @todo: @GBOv2: Use MessageLogger
   * @todo: @GBOv2: Use ErrorMiddleware and GlobalError to display on the page
   * @param {Object} promiseObject
   * @todo: reconcile with below processFailure method
   */
  static getMessages(promiseObject) {
    if (promiseObject) {
      return get(promiseObject, 'responseJSON.messages') || get(promiseObject, 'messages');
    } else {
      return [];
    }
  }

  /**
   * Takes a response with error data and wraps the logic into a simple and standard structure
   * (possibly `EcomError`?)
   * @todo - implement this method appropriately
   */
  static processFailure (response) {
    if (response) {
      // check response attributes, like `messages` and `error` and encapsulate them
      return response.messages;
    }
    // if there's no response, we return a default error object
    return [];
  }

  /**
   * Perform a basic validation do determine whether or not the response data is valid
   * @param  {object} response       response object from service
   * @param  {string} responseStatus response text from jqXHR
   * @param  {object} $xhr           jQuery Ajax jqXHR promise
   * @return {void}                just checks and throws an error if it fails
   * @todo  make this much better than it is
   */
  static validate (response, responseStatus/*, $xhr*/) {
    // check conditions like `hasMessages` and `hasError`
    const hasResponse = exists(response);
    const hasMessages = ServiceFactory.checkMessages(response);
    const statusSuccess = ServiceFactory.statusWhitelist(responseStatus);
    const hasFailed = (!hasResponse || hasMessages) && statusSuccess;
    // ServiceFactory.logger.log('validate() will hasFailed?', hasFailed, responseStatus, response);
    if (hasFailed) {
      ServiceFactory.logger.log('ServiceFactory.validate() hasFailed!', hasFailed);
      throw ServiceFactory.processFailure(response);
    }
  }

  /**
   * MAIN REASON THIS EXISTS IS TO LET MESSAGES THROUGH ...
   * @todo : handle the disconnect between the above and here better
   * @todo : compare to the other "validation" going on / available in this file
   * @param  {object}  response       ajax response object
   * @param  {string}  responseStatus ajax response status text
   * @return {Boolean}                either throws or doesn't
   */
  static baseValidate (response, responseStatus) {
    const hasResponse = exists(response);
    const statusSuccess = ['success', 'abort'].includes(responseStatus) ? true : false;
    const hasFailed = (!hasResponse) && statusSuccess;
    // ServiceFactory.logger.log('baseValidate() will hasFailed?', hasFailed);
    if (hasFailed) {
      ServiceFactory.logger.error('ServiceFactory.baseValidate() has failed processFailure()');
      throw ServiceFactory.processFailure(response);
    }
  }

  /**
   * 'Promisify' a request. The `validate` function should throw an error
   * if the successful request (status 200) returned invalid data or error messages.
   * This allow us to handle every error in a consistent way, using `Promise.catch`
   * while keeping our success handlers (on `Promise.then`) clean and focused on the
   * expected data.
   * @param {object} request : this is the call to our EnterpriseServices method
   * @param {function} validate : function we validate response data with
   */
  static wrapRequest (request, validate) {
    return new Promise((resolve, reject) => {
      /**
       * Return the promise on the original EnterpiseService call
       * @param  {function} (response, responseStatus, $xhr) promise callback - arguments from jQuery
       * @return {promise}            the original will be fulfilled or rejected
       */
      request.then((response, responseStatus, $xhr) => {
        // ServiceFactory.logger.log('001 request.then try/catch:', responseStatus, response, $xhr);
        try {
          validate(response, responseStatus, $xhr);
          // ServiceFactory.logger.log('002 request.then()');
          resolve(response);
        } catch (e) {
          // ServiceFactory.logger.warn('003 wrapRequest catch (e)', e);
          reject(e);
        }
      }).fail(($xhr, textStatus, err) => {
        // ServiceFactory.logger.warn('004', textStatus, err);
        // problem: jQuery ajax will .fail() on an "abort"
        // reject(ServiceFactory.processFailure(err));
        reject(err);
        // @todo maybe do this? problem, sending ANY data back may be an issue
        // if (err === 'abort') {
        //   resolve({});
        // } else {
        //   reject(err);
        // }
      });
    });
  }

  /**
   * Create a Service. This method wraps a request and allow us to
   * define default validation, success and failure handling on the service layer.
   * @see `wrapRequest`
   * Important Note: Keep in mind that the request function can only receive one argument.
   *                 For multiple arguments, pass an object/array and destructure it on the function.
   * 2017-10-29 - making this return ...args seems to allow multiple arguments (????)
   */
  static createService (params) {
    const {request, validate, onSuccess, onFailure} = {...defaultServiceParams, ...params};
    return (...args) => ServiceFactory.wrapRequest(request(...args), validate)
                                   .then(onSuccess)
                                   .catch(onFailure);
  }

  /**
   * Experimental, not to be taken too seriously ... yet ...
   * @param  {function} init   function to check the results of to create a final service
   * @param  {*} initInput  arguments to pass to the init function
   * @param  {object} params object passed into createService
   * @return {Promise}        returns a promise to fullfil or an outright rejection
   * @todo do we even need this thing? it is not being used as of 2017-11-14
   */
  static createOrRejectService (init, initInput, params) {
    let chainInit = new Promise((resolve, reject) => {
      let result = init(initInput);
      if (result) {
        resolve(result)
      } else {
        reject(result)
      }
    });
    chainInit
      .then((result) => {
        return ServiceFactory.creatService(Object.assign(params, result));
      })
      .catch((result) => {
        return Promise.reject(result);
      })
  }
}

const defaultServiceParams = {
  //request: undefined,
  //onSuccess: undefined,
  //onFailure: undefined,
  validate: ServiceFactory.validate
};

ServiceFactory.isDebug = true;
ServiceFactory.logger = new Debugger(window._isDebug, ServiceFactory, true, 'ServiceFactory');

export default ServiceFactory;
