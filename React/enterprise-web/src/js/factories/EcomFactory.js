/**
 * @module EcomFactory
 * Note that Factories support a number of features rarely used:
 * - Call a method multiple itmes using setStateData() and return it later
 * - toModel() will return the object as a given model object
 */
import BaseFactory from './BaseFactory';
import EcomError from '../classes/EcomError';
import { combine } from '../utilities/util-object';
import { Debugger } from '../utilities/util-debug';

/**
 * @Class EcomFactory
 * @extends BaseFactory
 * This class is just a convenience wrapper constructor. We may want to add
 * some static methods at this level or the level below. For extensibility,
 * really.
 */
export default class EcomFactory extends BaseFactory {
  /**
   * @param name {*} fields { string name }
   * @param debug {*} field { boolean debug setting }
   * @param methods {*} methods or properties function toState, function toServer
   * @method collect {function}
   * @property stateData {object}
   *
   * @property collected {object}
   */
  // constructor({ name = 'Factory', debug = false, ...methodsOrProps }) {
  constructor({ name = 'Factory', debug = false, ...methods }) {
    super(name, debug);
    // Object.keys(methods).forEach((key) => {
    //   this[key] = methods[key];
    // });
    Object.assign(this, methods)
    this._collected = null;
    this.resetState();
  }

  /**
   * Call this prior to using a Factory if you want to gaurantee fully fresh data.
   */
  resetState() {
    this._collected = null;
    this._state = {};
  }

  /**
   * Used to collect data while using a Factory. Typically this is data from other Factors, used as {derivedData} input
   * @returns {object} this._collected
   */
  get collected() {
    return this._collected;
  }

  /**
   * We use a collected property to store the data we pull from each step in a factory so it's easily returned.
   * @param {Object} obj
   */
  collect(obj) {
    if (this.collected === null) {
      this.logger.log('_collected is null, starting with object copy.');
      this._collected = combine({}, obj);
    } else {
      this._collected = combine(this._collected, obj);
    }
  }

  /**
   * Calls this.collect()
   * @param {object} obj
   */
  set collected(obj) {
    this.logger.warn('Directly setting this.collected calls this.collect(). Please use collect() instead.');
    this.collect(obj);
  }

  /**
   * If we set the stateData directly, it works like an object assign (is this convenient, or confusing?)
   * @param {object} obj
   */
  set stateData(obj) {
    this.logger.log('calling setter for stateData', obj);
    this._state = Object.assign(this._state, obj);
  }

  /**
   * Getter for the state data.
   * @returns {*}
   */
  get stateData() {
    return this._state;
  }

  /**
   * Copy an object into the private _state
   * @param {Object} obj to copy to _state
   * backwards compatibility, just using setter above, but this is confusing so this protects and is more like React's setState
   * @todo: @gbov2: get rid of this
   */
  setStateData(obj) {
    this.stateData = obj; // does this call the setter?
  }
}

/**
 * Take server data and return it into a format appropriate for our State Tree.
 * @param args
 * @returns {*[]}
 * @todo: @gbov2: throw new EcomError | MessageLogger
 */
EcomFactory.prototype.toState = function(...args) {
  Debugger.use('enterprise.log').error(this.name, 'calling toState() method that is not implemented.');
  return args;
};

/**
 * Take objects and combine to a format appropriate to be posted to the server.
 * @param args
 * @returns {*[]}
 * @todo: @gbov2: throw new EcomError | MessageLogger
 */
EcomFactory.prototype.toServer = function(...args) {
  Debugger.use('enterprise.log').warn(this.name, 'toServer() method not implemented.');
  return args;
};

EcomFactory.prototype.models = [];
/**
 * Take generic object description and map to a class used by the Factory
 * @param {string} objectKey of a type, mapped to an object the factory can use
 * @param {object} data rest arguments for data
 * @return {object} described object
 */
EcomFactory.prototype.toModel = function(objectKey, ...data) {
  let type = this.models.find((el) => {
    return el.name === objectKey;
  });
  let result;
  if (type && type.model) {
    result = new type.model(...data);
  } else if (type) {
    throw new EcomError(`toModel() found objectKey ${objectKey} but it does not have a model.`, ...data);
  }
  return result;
};

/**
 * @todo: @gbov2: figuring out if this is the best way
 * @param {string} extendedInfo Extended information to pass to error.
 * Originally, factories took a service endpoint to map to models, and when the callbacks
 * failed, this was triggered.
 * @deprecated
 */
EcomFactory.serviceFail = function(extendedInfo = ''){
  console.warn('Deprecated EcomFactory.serviceFail() called');
  // throw new EcomError(`${this.name} no service passed toState()`, `EcomFactory.serviceFail() :: ${extendedInfo}`);
};
