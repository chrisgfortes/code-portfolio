import EcomFactory from './EcomFactory';
import { Enterprise } from '../modules/Enterprise';

const enterprise = Enterprise.global;

/**
 * ECR-13850 - use this and ResInitFactory for reservation flow profile flags
 * ReservationStatusFactory
 * @todo: Maybe this should be called ReservationSessionStatusFactory?
 * overall flags about the reservation status/state, users, type of res, etc.
 * we build this up as the user progresses and set flags in the State Tree
 *
 * @todo: @gbov2: Considering reorganizing these flags already.
 * Hierarchy?
 * Moving some down to more complex, purpose-specific factories
 * Perhaps these flags should just be overall topics without other "homes"?
 * Or, if they're needed for something else...?
 *
 * @instance EcomFactory
 * @type {EcomFactory}
 */
export default new EcomFactory({
  name: 'ReservationStatusFactory',
  debug: false,

  toState(response, data) {
    this.resetState();

    this.logger.log(response, data);

    let cros = this.get(response, 'cros');
    let reservationSession = this.get(response, 'reservationSession');

    const chargeTypeData = this.get(reservationSession, 'chargeType') || this.get(cros, 'charge_type');

    // i am honestly not sure (yet) if these must be separate, cannot find where window.globalCID is set
    // the main reason these are treated differently from contract_number and such coming back from the services
    // is because we may need to prompt the user on which CID they wish to use. See CorporateFactory and
    // MultipleCID.js modal for more info. Can see this in action in CorporateActions.js
    let hasOrIsGlobalPrepopulatedCid = this.exists(enterprise.prepopulatedCid) && this.get(enterprise, 'prepopulatedCid');
    let hasOrIsWindowGlobalCid = this.exists(window.globalCID) && this.get(window, 'globalCID');

    // contract flags etc.
    const responseHasContract = !!this.get(response, 'reservationSession.contract_details');
    const sessionContract = this.get(response, 'reservationSession.contract_details');
    const marketMessageSession = responseHasContract ? this.get(sessionContract, 'marketing_message_indicator') : null;

    // ECR-14250 ?
    const marketMessageAccount = this.get(reservationSession, 'profile.basic_profile.customer_details.marketing_message_indicator');
    const marketMessageDeepLink = this.get(reservationSession, 'deep_link_reservation.reservation.contract_details.marketing_message_indicator');

    const hideMarketingMessage = marketMessageSession === false || marketMessageDeepLink === false || marketMessageAccount === false;
    const loyaltySignUpEligible = !!(!sessionContract || this.get(sessionContract, 'loyalty_sign_up_eligible') === true || this.get(sessionContract, 'loyalty_sign_up_eligible') === void 0);

    // for data that is copied from ReservationInitiateRequest
    const reservationsInitiateRequest = this.get(reservationSession, 'reservationsInitiateRequest');

    this.setStateData({
      currentStep: this.get(data, 'currentStep'),
      chargeType: chargeTypeData,

      // general purpose
      hasCrosResponse: this.exists(cros), // this isn't robust enough to be useful
      reservationStatus: this.get(response, 'cros.reservation_status') || this.get(response, 'reservationSession.reservation_status'),

      hasReservationSession: this.exists(reservationSession),

      hasReservationInitiate: !!reservationsInitiateRequest,
      locations: {
        hasPickupLocation: !!this.get(reservationsInitiateRequest, 'pickupLocation'),
        hasReturnLocation: !!this.get(reservationsInitiateRequest, 'returnLocation')
      },

      // for locations
      hasLocationDetailsFromPage: this.exists(enterprise.locationDetail),

      // carclass factory
      hasCarClassSelected: !!this.get(reservationSession, 'selectedCarClassDetails'),

      // for deeplink factory
      // Note this only identifies a v2 DEEP LINK — v1 deep links have no property given
      hasDeepLinkReservation: !!this.get(reservationSession, 'deep_link_reservation'),

      // for CorporateFactory, see comments there and above on usage
      prepopulatedCid: hasOrIsGlobalPrepopulatedCid,
      globalCid: hasOrIsWindowGlobalCid,
      hasGlobalOrPrepopulatedCid: !!hasOrIsGlobalPrepopulatedCid || !!hasOrIsWindowGlobalCid,

      hasContract: responseHasContract,

      hasMarketingMessage: !hideMarketingMessage,
      hasLoyaltySignUpEligible: !loyaltySignUpEligible,

      hasDriverInfo: !!this.get(response, 'reservationSession.driverInformation') || !!this.get(cros, 'driver_info'),

      hasCommitRequest: !!this.get(reservationSession, 'commitRequest'),

      modify: {
        // rebookCancel comes from GMA and means reservations/modify/init has been created
        isModifyInProgress: !!this.get(reservationSession, 'rebookCancel'),
        // cros says a modify WILL BE a cancel rebook
        willModifyCancelRebook: !!this.get(cros, 'cancel_rebook'),
        // this changes at the end of a modify and tells us that a modify was a cancel rebook
        wasThisModifyCancelRebook: !!this.get(reservationSession, 'was_cancel_rebook'),
        // In Flight Modify has nothing to do with flying. It's when a user goes back  to edit something
        isInFlightModify: !!this.get(reservationSession, 'inflightModification')
      },

      // used for modal label logic on terms (?)
      licenseeName: this.get(cros, 'licensee_name'),

      // for pricing factory
      paymentProcessor: this.get(cros, 'prepay_payment_processor'),

      isPickupModifyBlocked: !!this.get(cros, 'block_modify_pickup_location'),
      isDropoffModifyBlocked: !!this.get(cros, 'block_modify_dropoff_location'),

      collectNewCardInModify: !!this.get(cros, 'collect_new_payment_card_in_modify'),

      userAssociatedWithRes: !!this.get(reservationSession, 'userAssociatedWithRes'),

      isUserLoggedIn: !!this.get(reservationSession, 'loggedIn'),

      // for user factory
      // @todo: @gbov2: here we have a field that may need to be overwritten elsewhere ... hmmmm (not good)
      // ECR-14250
      isLoyaltyUser: this.exists(this.get(reservationSession, 'profile.basic_profile'))
    });

    this.logger.log(this.stateData);
    return this.stateData;
  }
});
