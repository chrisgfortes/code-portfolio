import EcomFactory from '../EcomFactory';
import CarDetailsFactory from './CarDetailsFactory';

/**
 * @module CarSelectFactory
 * Transform Car Select data to and from service and state formats
 * @type {EcomFactory}
 * @param {Object} describing this factory
 */
export default new EcomFactory({
  name: 'CarSelectFactory',
  debug: false,

  toState(response, data) {
    this.resetState();

    /**
     * reservationSession.selectedCarClassDetails
     * cros.car_class_details
     * these overlap significantly.
     * we should determine the differences, if any
     * and which we should really be using
     */
    const reservationSession = this.get(response, 'reservationSession');
    const cros = this.get(response, 'cros');

    const chargeType = this.get(data, 'chargeType');

    const selectedCarClassDetails = CarDetailsFactory.toState(this.get(reservationSession, 'selectedCarClassDetails'));
    const carClassDetail = this.get(cros, 'car_class_details');

    const upgrades = [this.get(reservationSession, 'upgrades')].filter(this.exists)
                                                               .map(CarDetailsFactory.toState);
    const hasUpgrades = upgrades.length > 0; // this could be a reservation status flag but seemed trivial for that ...

    const extrasCollection = this.exists(carClassDetail) && this.exists(carClassDetail.extras) && this.get(carClassDetail, 'extras');

    const protectionsRequired = this.get(cros, 'protections_required_at_counter');
    const protectionsRequiredText = this.get(cros, 'protections_required_at_counter_text');
    const hasRequiredProtections = this.exists(protectionsRequired) && (protectionsRequired.length > 0);
    // suggestion
    // let rates = this.get(carClassDetail, 'vehicleRates')
    // let rateType = loop over keys and each pay type (PAYLATER, PAYNOW) etc., is a new RateType() class

    this.setStateData({
      chargeType,
      selectedCarClassDetails,
      hasUpgrades,
      upgrades,
      // rates
      extrasCollection,
      requiredProtections: {
        protections: protectionsRequired,
        text: protectionsRequiredText
      },
      hasRequiredProtections
    });
    this.logger.log('state data:', this.stateData);
    return this.stateData;
  }
});
