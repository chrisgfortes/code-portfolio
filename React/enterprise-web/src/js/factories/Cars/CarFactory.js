import EcomFactory from '../EcomFactory';
import { Car, CarProperty } from '../../classes/Cars';
import { exists } from '../../utilities/util-predicates';

/**
 * @module CarFactory
 * Transform car related data to and from service and state formats
 * @type {EcomFactory}
 * @param {Object} describing this factory
 */
export default new EcomFactory({
  name: 'CarFactory',
  debug: false,

  toState(raw) {
    const rawModel = _.pickBy({
      code: raw.code,
      name: raw.name,
      status: raw.status,
      description: raw.description,
      models: raw.make_model_or_similar_text,
      charges: raw.charges, // @todo - turn into an object with `CarCharge`s
      filters: raw.filters, // @todo - turn into an object with `CarProperty`s AND handle 'null' and null values
      category: new CarProperty(raw.category.code, raw.category.name), // not used yet, but could be used on Location Search filters
      features: (raw.features_shorts || []).map(f => new CarProperty(f.code, f.description)), // list of `CarProperty`s
      images: raw.images,
      priceDifferences: raw.price_differences,
      luggageCapacity: raw.luggage_capacity,
      isTnCRequired: raw.terms_and_conditions_required,
      isPreferred: raw.preferred_vehicle,
      redemptionDaysMax: raw.eplus_max_redemption_days,
      redemptionPointsRate: raw.redemption_points,
      truckUrl: raw.truck_url,
      availabilityPhoneNumber: raw.call_for_availability_phone_number
    }, exists);
    return new Car(rawModel);
  }
});
