import EcomFactory from '../EcomFactory';
import { CarDetails } from '../../classes/Cars';
import { exists } from '../../utilities/util-predicates';

/**
 * @module CarDetailsFactory
 * Transform car details data to and from service and state formats
 * @type {EcomFactory}
 * @param {Object} describing this factory
 */
export default new EcomFactory({
  name: 'CarDetailsFactory',
  debug: false,

  toState(raw = {}) {
    const rawModel = _.pickBy({
      code: raw.code,
      name: raw.name,
      status: raw.status,
      models: raw.make_model_or_similar_text,
      images: raw.images,
      priceDifferences: raw.price_differences,
      mileageInfo: raw.mileage_info,
      redemptionDaysCount: raw.redemption_day_count,
      redemptionDaysMax: raw.eplus_max_redemption_days,
      redemptionPointsRate: raw.redemption_points,
      redemptionPointsUsed: raw.eplus_points_used,
      vehicleRates: raw.vehicle_rates,
      extras: raw.extras
    }, exists);
    return new CarDetails(rawModel);
  }
});
