import EcomFactory from './EcomFactory';
import { SERVICE_ENDPOINTS } from '../constants';

/**
 * @module AdditionalInformationFactory
 * Transform Additional Information related data to and from service and state formats
 * @type {EcomFactory}
 * @param {Object} describing the factory
 */
export default new EcomFactory({
  name: 'AdditionalInformationFactory',
  debug: false,

  currentSessionToStateFormat(response, data) {
    this.resetState();
    this.logger.log('incoming data', data);
    let session = this.get(response, 'reservationSession');
    let initInfo = this.get(data, 'resInitRequestData.additionalInformation'); // from reservationInitiateRequest
    let additionalInformationAllData = this.get(session, 'additionalInformationAll');
    let additionalInformationPostRateData = this.get(session, 'additionalInformationPostRate');
    let addtionalInformationPreRateData = this.get(session, 'additionalInformationPreRate');
    let additionalInformationSavedData = this.get(session, 'additionalInformationSaved');

    // @todo: @gbov2: format for State Tree
    this.setStateData({
      initInfo,
      additionalInfoIds: (this.get(session, 'contract_details.additional_information') || []).map(info => info.id),
      additionalInformationAllData,
      additionalInformationPostRateData,
      addtionalInformationPreRateData,
      additionalInformationSavedData
    })

  },

  toState(response, data) {
    let { service } = data;

    if (this.exists(service)) {

      switch (service) {
        case SERVICE_ENDPOINTS.SESSION_CURRENT:
          // this.logger.log(response, data);
          this.currentSessionToStateFormat(response, data);
          break;
        default:
          console.warn(`no matching SERVICE_ENDPOINT in ${this.name}`);
      }

    } else {
      EcomFactory.serviceFail(this.name);
    }
    return this.stateData;
  }
})
