import EcomFactory from './EcomFactory';
import { combine } from '../utilities/util-object';
import { SERVICE_ENDPOINTS } from '../constants';

/**
 * @module LegalTermsFactory
 * Transform session related data to and from service and state formats
 * @type {EcomFactory}
 * @param {Object} describing this factory
 * Take apart or put together data bits for the session service
 */
export default new EcomFactory({
  name: 'LegalTermsFactory',
  debug: true,

  currentSessionToStateFormat(response, data) {
  },

  // format data
  toState(response, data) {
    let { service } = data;

    if (this.exists(service)) {

      switch (service) {
        case SERVICE_ENDPOINTS.SESSION_CURRENT:
          // this.logger.log(response, data);
          this.currentSessionToStateFormat(response, data);
          break;
        default:
          console.warn(`no matching SERVICE_ENDPOINT in ${this.name}`);
      }

    } else {
      EcomFactory.serviceFail(this.name);
    }
    return this.stateData;
  }
});
