import EcomFactory from '../EcomFactory';
import { Personal } from '../../classes/User';
import { exists } from '../../utilities/util-predicates';

// IS THIS NECESSARY??

/**
 * @module PersonalFactory
 * Transform personal data to and from service and state formats
 * @type {EcomFactory}
 * @param {Object} describing this factory
 */
export default new EcomFactory({
  name: 'PersonalFactory',
  debug: false,

  toState(raw = {}) {
    this.logger.log('toState:', raw.phoneNumber);
    const rawModel = _.pickBy({
      firstName: raw.firstName,
      lastName: raw.lastName,
      email: raw.email,
      phoneNumber: raw.phoneNumber,
      requestPromotion: raw.requestPromotion,
      airlineCode: raw.airlineCode,
      flightNumber: raw.flightNumber,
      terms: raw.terms
    }, exists);
    return new Personal(rawModel);
  }
});
