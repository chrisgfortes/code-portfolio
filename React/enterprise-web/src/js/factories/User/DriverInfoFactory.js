import { DriverInfo } from '../../classes/User';

import EcomFactory from '../EcomFactory';
import { exists } from '../../utilities/util-predicates';
import { validateMaskedInfo } from '../../utilities/util-validate';
import { PROFILE } from "../../constants/";

function getExpeditedRegionIssue (expedited) { // @todo - This shouldn't be here!!! Move this logic to Expedited Model
  switch (expedited.countryIssue.country_code) {
    case 'ES': return 'ESP'; //Custom rules for submitting issue region for ES
    case 'GB': return 'DVLA'; //Custom rules for submitting issue region for GB
    default: return expedited.regionIssue;
  }
}

/**
 * Returns the basic DriverInfo model
 *
 * @param      {object}      Driver information from or bound for service
 * @return     {DriverInfo}  The driver information model.
 */
export function getDriverInfoModel (obj) {
  return new DriverInfo(obj);
}

/**
 * @module DriverInfoFactory
 * Creates server data from Personal and Expedited data models
 * @type {EcomFactory}
 * @param {Object} describing this factory
 */
export default new EcomFactory({
  name: 'DriverInfoFactory',
  debug: false,

  toServer(personal, expedited, isFedexReservation) {
    const validPhone = validateMaskedInfo(personal.phoneNumber);
    const addresses = [expedited.address, expedited.addressTwo].filter(exists);
    const isExpeditedEnabled = ['noMatch', 'driver', 'edit'].includes(expedited.render); // @todo - This shouldn't be here!!! Move this logic to Expedited Model

    return {
      first_name: personal.firstName,
      last_name: personal.lastName,
      email_address: isFedexReservation ? personal.email : validateMaskedInfo(personal.email),
      phone: validPhone ? {
        phone_number: validPhone,
        phone_type: 'HOME'
      } : null,
      address: isExpeditedEnabled ? _.pickBy({
        street_addresses: addresses.filter(validateMaskedInfo), // @todo need to be sure this is an array or we have issues
        city: validateMaskedInfo(expedited.city),
        country_subdivision_code: validateMaskedInfo(expedited.regionResidence),
        country_code: validateMaskedInfo(expedited.countryResidence.country_code),
        postal: validateMaskedInfo(expedited.postal),
        address_type: 'HOME'
      }, exists) : null,
      drivers_license: isExpeditedEnabled ? _.pickBy({ // #driversLicense
        license_number: validateMaskedInfo(expedited.license),
        country_subdivision_code: validateMaskedInfo(getExpeditedRegionIssue(expedited)),
        country_code: validateMaskedInfo(expedited.countryIssue.country_code),
        birth_date: validateMaskedInfo(expedited.dateOfBirth),
        license_expiry: validateMaskedInfo(expedited.licenseExpiryDate),
        license_issue: validateMaskedInfo(expedited.licenseIssueDate)
      }, exists) : null,
      request_email_promotions: personal.requestPromotion,
      //Will change in the future depends on authenticated vs unauthenticated state
      source_code: PROFILE.SOURCE_CODE_RNT
    };
  }
});
