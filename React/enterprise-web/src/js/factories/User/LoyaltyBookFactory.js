import EcomFactory from '../EcomFactory';
import { LoyaltyBook } from '../../classes/User';
import { exists } from '../../utilities/util-predicates';

// IS THIS NECESSARY??

/**
 * @module LoyaltyBookFactory
 * Transform loyalty book data to and from service and state formats
 * @type {EcomFactory}
 * @param {Object} describing this factory
 */
export default new EcomFactory({
  name: 'LoyaltyBookFactory',
  debug: false,

  toState(raw = {}) {
    const rawModel = _.pickBy({
      loyalty_brand: raw.loyalty_brand,
      membership_id: raw.membership_id,
      last_name: raw.last_name,
    }, exists);
    return new LoyaltyBook(rawModel);
  }
});
