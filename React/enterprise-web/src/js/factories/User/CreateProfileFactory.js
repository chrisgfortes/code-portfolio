/**
 * @todo       this was a quick hack, please convert to proper models
 * @todo       combine with ExpeditedFactory.toServer to use same models
 */
import EcomFactory from '../EcomFactory';

export default new EcomFactory({
  name: 'CreateProfileFactory',
  debug: false,

  /**
   * Format data for submission to create profile endpoint
   *
   * @param      {object}  dataObj  The parameters
   * @return     {object}  create profile object model
   */
  toServer(dataObj = {}) {
    let {
      profile,
      enroll
    } = dataObj;

    let addresses = [profile.streetAddress];
    let phones = [];

    if (profile.phoneNumber) {
      phones.push({
        phone_type: profile.phoneNumberType || 'HOME',
        phone_number: profile.phoneNumber.replace(/[- )(]/g, '')
      });
    }

    if (profile.alternativePhoneNumber) {
      phones.push({
        phone_type: profile.alternativePhoneNumberType || 'HOME',
        phone_number: profile.alternativePhoneNumber.replace(/[- )(]/g, '')
      });
    }

    if (profile.additionalStreetAddress) {
      addresses.push(profile.additionalStreetAddress);
    }

    let data = {
      first_name: profile.firstName,
      last_name: profile.lastName,
      email: profile.email,
      phones,
      address: {
        street_addresses: addresses,
        city: profile.city,
        country_subdivision_code: profile.subdivision,
        country_subdivision_name: null,
        country_code: profile.country,
        country_name: null,
        postal: profile.postal,
        address_type: 'HOME'
      },
      drivers_license: { // #driversLicense
        license_number: profile.licenseNumber,
        country_subdivision_code: profile.licenseRegion,
        country_code: profile.licenseCountry,
        license_issue_date: profile.licenseIssue,
        license_expiration_date: profile.licenseExpiry
      },
      password: profile.password,
      date_of_birth: profile.birthDate,
      preference: {
        email_preference: {
          special_offers: profile.specialOffers,
          partner_offers: false,
          rental_receipts: false
        }
      },
      terms_and_conditions: {
        accept_decline: profile.acceptTerms || profile.terms
      }
    };

    data.preference.source_code = enterprise.selectors ? enterprise.selectors : 'EPLUSECOM';

    if (profile.cid) {
      data.corporate_account = profile.cid;
    }
    if (enroll.useMemberid) {
      data.use_member_number_as_username = true;
    }
    return data;
  }
})
