
import UserStateModelFactory from './UserStateModelFactory';
import UserStateViewFactory from './UserStateViewFactory';

let model = UserStateModelFactory();
let view = UserStateViewFactory();

// planning on branching this away from that over time
// to break up model and view
// currently StateTree imports the members of model and view (2 things)
const UserModelFactory = {
  model,
  view
}

export default UserModelFactory;
