
import { UserStateModel } from '../../classes/User';

export default function () {
  let userStateModel = new UserStateModel();
  // by default the model has no Profile
  return Object.assign({}, userStateModel);
}
