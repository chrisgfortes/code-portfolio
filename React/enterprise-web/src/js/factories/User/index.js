
/**
 * REMEMBER THERE IS A GENERIC USERFACTORY IN FACTORIES ROOT
 */

export ProfileFactory, {
  getClearProfile
} from './ProfileFactory';
export UserModelFactory from './UserModelFactory';

export PersonalFactory from './PersonalFactory';
export SimplePersonalFactory from './SimplePersonalFactory';
export LoyaltyBookFactory from './LoyaltyBookFactory';

export LoginFactory, { getLoginBaseModel } from './LoginFactory';

export CreateLoginFactory from './CreateLoginFactory';
export CreateProfileFactory from './CreateProfileFactory';

export DriverInfoFactory from './DriverInfoFactory';

export AccountFactory from './AccountFactory';

export ExpeditedFactory, {
  getClearExpedited,
  getPersonalMapper,
  getProfileMapper
} from './ExpeditedFactory';
