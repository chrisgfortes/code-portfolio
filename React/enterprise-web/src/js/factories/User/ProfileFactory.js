import EcomFactory from '../EcomFactory';
import { Profile, BasicProfile } from '../../classes/User';
// import { exists } from '../../utilities/util-predicates';

// this may be the dumbest function i have ever written
function getOne(obj, fieldName, altFieldName) {
  return _.get(obj, fieldName, _.get(obj, altFieldName));
}

function makeBasicProfile (profile) {
  let basicProfile = getOne(profile, 'basicProfile', 'basic_profile');
  if (basicProfile) {
    return new BasicProfile(basicProfile);
  } else return void 0;
}

/**
 * Used by UserFactory to make profiles
 * @see UserFactory
 */

export function getClearProfile(){
  return {
    licenseProfile: {
      $set: {
        country_subdivision_name: null
      }
    }
  }
}

export default new EcomFactory({
  name: 'ProfileFactory',
  debug: true,

  /**
   * pull out of reservationSession.profile:
   * - addressProfile: {}
   * - basicProfile: {}
   * - contactProfile: {}
   * - licenseProfile: {}
   * - loyaltyData: {}
   * - paymentProfile: {}
   * - preference: {}
   */
  toState (obj) {
    let result = {};
    if (obj) {

      // let rawModel = _.pickBy({
      let rawModel = {
        basicProfile: makeBasicProfile(obj),
        addressProfile: getOne(obj, 'addressProfile', 'address_profile'),
        contactProfile: getOne(obj, 'contactProfile', 'contact_profile'),
        licenseProfile: getOne(obj, 'licenseProfile', 'license_profile'),
        // loyaltyData: getOne(obj, 'loyaltyData', 'loyalty_data'),
        paymentProfile: getOne(obj, 'paymentProfile', 'payment_profile'),
        preference: obj.preference
      };
      // }, exists);

      result = new Profile(rawModel);

      this.logger.warn('toState()', result);
    } else {
      // we may return false if there's no profile (e.g. no user logged in)
      result = false;
    }
    return result;
  }
});
