import EcomFactory from '../EcomFactory';
import { SimplePersonal } from '../../classes/User';
import { exists } from '../../utilities/util-predicates';

/**
 * @module SimplePersonalFactory
 * Creates a simplified personal info from driver info data
 * @type {EcomFactory}
 * @param {Object} describing this factory
 */
export default new EcomFactory({
  name: 'SimplePersonalFactory',
  debug: false,

  toState(raw = {}) {
    const rawModel = _.pickBy({
      name: raw.first_name + ' ' + raw.last_name,
      email: raw.email_address,
      phone: raw.phone ? raw.phone.phone_number : '',
      address: raw.address,
      age: raw.age
    }, exists);
    return new SimplePersonal(rawModel);
  }
});