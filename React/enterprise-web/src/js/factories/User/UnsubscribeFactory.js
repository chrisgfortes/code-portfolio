import EcomFactory from '../EcomFactory';

export default new EcomFactory({
  name: 'UnsubscribeFactory',
  debug: false,

  /**
   * Format data for submission to login endpoint
   * @see LoginController.login
   *
   * @param      {object}  params  The parameters
   * @return     {object}  login object model
   */
  toServer(params) {
    return {
      first_name: "Dev",
      last_name: "Tester",
      email: "testeriso123@gmail.com",
      opt_in: "false",
      country_of_residence: "US",
      email_source_code: "EGOS",
      language: "en_US"
    };
  }
})
