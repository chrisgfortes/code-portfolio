/**
 * @module ExpeditedFactory
 * This toServer method is basically doing the same thing as CreateProfileFactory.
 * Should probably combine.
 * @todo       Convert toServer to use the same thing as CreateProfileFactory class model
 */
import EcomFactory from '../EcomFactory';
import { PAGEFLOW } from '../../constants';
// import { exists } from '../../utilities/util-predicates';

// @todo use a profile class
export function getClearExpedited () {
  return {
    render: {
      $set: 'search'
    },
    profile: {
      $set: null
    },
    countryIssue: {
      promoted_deals: [],
      country_sub_division_type: {
        $set: null
      },
      default_email_opt_in: {
        $set: null
      },
      disable_one_way_rental: {
        $set: null
      },
      enable_as_issuing_country: {
        $set: null
      },
      european_address_flag: {
        $set: null
      },
      house_number: {
        $set: null
      },
      license_issued_by: {
        $set: null
      },
      postal_code_type: {
        $set: null
      },
      prepay_enabled: {
        $set: null
      },
      street_address_two: {
        $set: null
      }
    },
    countryResidence: {
      promoted_deals:[],
      country_sub_division_type: {
        $set: null
      },
      default_email_opt_in: {
        $set: null
      },
      disable_one_way_rental: {
        $set: null
      },
      enable_as_issuing_country: {
        $set: null
      },
      european_address_flag: {
        $set: null
      },
      house_number:{
        $set: null
      },
      license_issued_by:{
        $set: null
      },
      prepay_enabled: {
        $set: null
      },
      street_address_two: {
        $set: null
      }
    },
    address: {
      $set: null
    },
    addressTwo: {
      $set: null
    },
    city: {
      $set: null
    },
    postal: {
      $set: null
    },
    dateOfBirth: {
      $set: null
    },
    licenseIssueDate: {
      $set: null
    },
    licenseExpiryDate: {
      $set: null
    },
    password: {
      $set: null
    },
    passwordConfirm: {
      $set: null
    },
    terms: {
      $set: false
    }
  }
}

// @todo use personal class
export function getPersonalMapper(personalField, profile) {
  return {
    firstName: {
      $set: profile.basic_profile.first_name
    },
    lastName: {
      $set: profile.basic_profile.last_name
    },
    phoneNumber: {
      $set: (personalField.phoneNumber || profile.contact_profile.phones[0].phone_number)
    },
    email: {
      $set: (personalField.email || profile.contact_profile.email)
    }
  }
}

// @todo use profile class
export function getProfileMapper(profile) {
  return {
    regionIssue: {
      $set: profile.license_profile.country_subdivision_code
    },
    regionResidence: {
      $set: _.get(profile, 'address_profile.country_subdivision_code')
    },
    address: {
      $set: _.get(profile, 'address_profile.street_addresses[0]')
    },
    addressTwo: {
      $set: _.get(profile, 'address_profile.street_addresses[1]')
    },
    city: {
      $set: _.get(profile, 'address_profile.city')
    },
    postal: {
      $set: _.get(profile, 'address_profile.postal')
    },
    dateOfBirth: {
      $set: profile.license_profile.birth_date
    },
    licenseIssueDate: {
      $set: profile.license_profile.license_issue
    },
    licenseExpiryDate: {
      $set: profile.license_profile.license_expiry
    }
  }
}

/**
 * @module ExpeditedFactory
 * Transform Expedited data to and from service and state formats
 * @type {EcomFactory}
 * @param {Object} describing this factory
 * @todo: use a user model/class (?)
 */
export default new EcomFactory({
  name: 'ExpeditedFactory',
  debug: true,

  /**
   * Format state data for server submission
   * @see        ExpeditedController.enroll
   * @param      {object}  model   the models
   */
  toServer(model = { expedited: {}, personal: {}, enroll: {} }) {
    let {
      expedited,
      personal,
      enroll
    } = model;

    let addresses = [expedited.address];
    let phones = [];
    const homeValue = PAGEFLOW.HOME.toUpperCase();
    const expeditedValue = PAGEFLOW.EXPEDITED.toUpperCase();

    if (personal.phoneNumber) {
      phones.push({
        phone_type: homeValue,
        phone_number: personal.phoneNumber.replace(/[- )(]/g, '')
      });
    }

    if (expedited.addressTwo) {
      addresses.push(expedited.addressTwo);
    }

    let data = {
      // @todo: ECR-13133 untested by GBO commented for 2.1.2
      request_origin_channel: expeditedValue,
      first_name: personal.firstName,
      last_name: personal.lastName,
      email: personal.email,
      phones,
      address: {
        street_addresses: addresses,
        city: expedited.city,
        country_subdivision_code: expedited.regionResidence,
        country_code: expedited.countryResidence.country_code,
        postal: expedited.postal,
        address_type: homeValue
      },
      drivers_license: { // #driversLicense
        license_number: expedited.license,
        country_subdivision_code: expedited.regionIssue,
        country_code: expedited.countryIssue.country_code,
        license_issue_date: expedited.licenseIssueDate,
        license_expiration_date: expedited.licenseExpiryDate
      },
      password: expedited.password,
      date_of_birth: expedited.dateOfBirth,
      'preference': {
        email_preference: {
          special_offers: personal.requestPromotion === true || personal.requestPromotion === 'true',
          partner_offers: false,
          rental_receipts: false
        }
      },
      terms_and_conditions: {
        accept_decline: expedited.terms
      }
    };

    if (enroll.useMemberid) {
      data.use_member_number_as_username = true;
    }

    //Add this so EWT knows we are enrolling on verification
    data.route = 'commit';

    this.logger.log('expedited enroll data: ', data);

    return data;
  }
});
