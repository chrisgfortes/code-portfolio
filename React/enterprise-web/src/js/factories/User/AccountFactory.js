/**
 * This file is a hack and should be changed to follow correct
 * EcomFactory patterns...
 * There wasn't time ECR-14250 :(
 */
import {
  PROFILE,
  EXPEDITED
} from '../../constants/';

const AccountFactory = {
  toServer: {
    // @see AccountController
    // @todo use a real model

    updateLicense(params) {
      let authority;
      let data = {
        loyalty_number: params.account,
        drivers_license: { // #driversLicense
          license_number: params.licenseNumber,
          license_issue_date: params.licenseIssue,
          //country_subdivision_name: null,
          country_code: params.licenseCountry,
          //country_name: null,
          license_expiration_date: params.licenseExpiry,
          do_not_rent_indicator: false
        }
      };
      if (params.licenseCountry === 'GB') {
        authority = EXPEDITED.LICENSE.REGIONS.DVLA;
        data.drivers_license.issuing_authority = authority
      } else {
        data.drivers_license.country_subdivision_code = params.licenseRegion
      }
      return data;
    },
    // @see AccountController
    // @todo use a real model
    updateContact(params) {
      let phones = [];

      if (params.phoneNumber) {
        phones.push({
          phone_type: params.phoneNumberType,
          phone_number: params.phoneNumber
        });
      }

      if (params.alternativePhoneNumber) {
        phones.push({
          phone_type: params.alternativePhoneNumberType,
          phone_number: params.alternativePhoneNumber
        });
      }

      let streetAddresses = [];
      if (params.address) {
        streetAddresses[0] = params.address;
      }

      if (params.addressTwo) {
        streetAddresses[1] = params.addressTwo;
      }

      return {
        loyalty_number: params.account,
        address: {
          street_addresses: streetAddresses,
          city: params.city,
          country_code: params.countryResidence,
          country_subdivision_code: params.regionResidence,
          postal: params.postal,
          address_type: "HOME" // @todo - This is provisional until we add address type on screen
        },
        contact: {
          email: params.email,
          phones: phones
        },
        preference: {
          email_preference: {
            special_offers: params.specialOffers
          },
          source_code: PROFILE.SOURCE_CODE_EP
        }
      };
    }
  }
};

export default AccountFactory;
