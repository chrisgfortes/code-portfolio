import EcomFactory from '../EcomFactory';
// import { exists } from '../utilities/util-predicates';

/**
 * Gets the login base model for a Baobab update
 *
 * @param      {object}  params  The parameters
 * @return     {Object}  The login base model.
 */
export function getLoginBaseModel(params) {
  return {
    password: {
      $set: params ? params.password : null
    },
    username: {
      $set: params ? params.username : null
    },
    rememberMe: {
      $set: params ? params.rememberMe : null
    },
    brand: {
      $set: params ? params.brand : null
    }
  };

}

export default new EcomFactory({
  name: 'LoginFactory',
  debug: false,

  /**
   * Format data for submission to login endpoint
   * @see LoginController.login
   *
   * @param      {object}  params  The parameters
   * @return     {object}  login object model
   */
  toServer(params) {
    let data = {
      username: params.username,
      password: params.password
    };
    if (params.rememberMe) {
      data.remember_credentials = params.rememberMe;
    }
    if (params.terms) {
      data.accept_decline_version = params.terms;
    }
    if (params.newPassword) {
      data.new_password = params.newPassword;
    }
    return data;
  }
})
