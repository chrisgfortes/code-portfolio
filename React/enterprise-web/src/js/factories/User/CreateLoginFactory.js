import EcomFactory from '../EcomFactory';

export default new EcomFactory({
  name: 'CreateLoginFactory',
  debug: false,

  /**
   * Format data for submission to create login endpoint
   *
   * @param      {object}  createPassword  The parameters
   * @return     {object}  create login object model
   */
  toServer(createPassword) {
    return {
      "loyalty_number": createPassword.userName,
      "last_name": createPassword.lastName,
      "email": createPassword.email,
      "password": createPassword.password,
      "terms_and_conditions": {
        "accept_decline": createPassword.acceptTerms,
        "accept_decline_version": createPassword.termsVersion
      }
    }
  }
})
