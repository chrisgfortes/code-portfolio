/**
 * @module ServiceController
 * For controlling services beyond basic service layer tasks and rules.
 */
// import ErrorActions from '../actions/ErrorActions';

import EcomError from '../classes/EcomError';

import ServiceFactory from '../factories/ServiceFactory';
import { Debugger } from '../utilities/util-debug';

const ServiceController = {
  /**
   * @deprecated we're using the validate and baseValidate methods in ServiceFactory instead
   */
  // validateResponse(response) {
  //   let result = false;
  //   if (ServiceFactory.checkServiceResponse(response)) {
  //     result = true;
  //   }
  //   // @todo: @gboV2 need to handle messages coming back consistently ...
  //   // more of this needs to be at another layer / NOT session controller
  //   if (!ServiceFactory.checkNoMessages(response)) {
  //     result = false;
  //   }
  //   return result;
  // },
  // /**
  //  * WARNING THIS CAN BOMB IN DIFFICULT WAYS
  //  * @deprecated should replace with catchService, preferrably
  //  */
  // serviceFailed(mess, response) {
  //   if (!ServiceFactory.checkNoMessages(response)) {
  //   // Trigger Custom Event that globally cleans up the UI?
  //     // Debugger.use('enterprise.log').error(mess, ServiceFactory.getMessages(response));
  //     throw new EcomError(mess, 'ServiceController :: serviceFailed()'); // ??
  //   }
  // },

  /**
   * @function serviceCleanup
   * @param {object|string} err error object or string error description
   * @param {array} components array of components to handle
   */
  // serviceCleanup (err, components) {
  //   console.log('serviceCleanup(err, components)', err, components);
  //   ErrorActions.stopLoading();
  //   console.log('serviceCleanup() typeof err', typeof err);
  //   // components.forEach( component => ErrorActions.setErrorsForComponent(err, component) )
  // },

  // keeping this simple for now...
  trap (message, err, fatal = false) {
    console.error(message, err);
    if (fatal) {
      console.error('Fatal error.');
      throw new Error(err);
    }
    // throw new Error(err);
  },

  /**
   * @function catchService
   * Generic handler for .catch() on Ajx services
   * @param  {string} message message sent from specific catch handler
   * @param  {string} err     error string from fail()
   * @return {EcomError|void}
   */
  // catchService (message, err, components = []) {
  //   // setTimeout(() => {
  //   //   console.error(message, err)
  //   //   // throw new EcomError(message, err);
  //   // })
  //   // if (!ServiceFactory.statusWhitelist(err)) { // @todo: do something better here
  //     // console.error('catchService(message, err, components)', message, err, components);
  //     // throw new EcomError(message + ' ' + err, ':: catchService()')
  //   // } else {
  //   //   console.warn(message, err);
  //   // }
  // },
  /**
   * called by onFailure method of services
   * @param {string} source string message indicating scope of failure (e.g. LocationSearchService);
   * @param {string} err string message from triggered error
   * @param {array} components array of components to set errors for
   * @return {EcomError} throws error which will break the promise
   * Why catchService and serviceOnFailure? so we know which layer in the stack of errors an error was thrown
   */
  serviceOnFailure (source, err, components = []) {
    if (!ServiceFactory.statusWhitelist(err)) {
      // console.error('serviceOnFailure(source, err, components)', source, err, components);
      console.error(source, err);
      // throw new EcomError(err, source);
    }
  }
};

export default ServiceController;
