import ModifyActions from '../actions/ModifyActions';
import VerificationActions from '../actions/VerificationActions';
import ReservationActions from '../actions/ReservationActions';

import ServiceController from './ServiceController';

import CancelModifyService from '../services/CancelModifyService';

import { debug } from '../utilities/util-debug';

/************************
 * SERVICE INTERACTIONS *
 ************************/
const ServiceData = {
  /**
   * Call available fields endpoint
   * @function   getAvailableFields
   * @memberof   ModifyController
   * @return     {Promise}  Promise wrapped ajax
   */
  callGetAvailableFields () {
    return CancelModifyService.getAvailableFields()
      .then((response) => {
        VerificationActions.setAvailableFields(response.showfields);
      })
      .catch((err) => {
        ServiceController.trap('ModifyController.callGetAvailableFields().catch()', err);
      })
  },
  modifyConfirmation () {
    ReservationActions.setLoadingClassName('loading');

    CancelModifyService.initiateModify()
      .then(() => {
        ModifyActions.toggleConfrimationModal(false);
        ReservationActions.setLoadingClassName(null);
      })
      .catch( err => ServiceController.trap('ModifyController.modifyConfirmation().catch()', err))
  },
  abandonModify () {
    ReservationActions.setLoadingClassName('loading');
    CancelModifyService.abandonModify();
  }
};

const Rules = {
  /**
   * Determines if in flight modify is in progress.
   * @function IsInFlightModify
   * @memberOf ModifyController
   * @return     {boolean}  True if in flight modify, False otherwise.
   */
  isInFlightModify() {
    let flags = ReservationActions.get.reservationStatusFlags();
    return flags.isInFlightModify;
  },
  /**
   * Determines if modify in progress.
   * @function   isModifyInProgress
   * @memberOf ModifyController
   * @return     {boolean}  True if modify in progress, False otherwise.
   */
  isModifyInProgress() {
    let flags = ReservationActions.get.reservationStatusFlags();
    return flags.isModifyInProgress;
  },
  /**
   * Determines if both modify (inflight or otherwise) is currently in progress.
   * Note that this may never even happen but still need to test for this condition sometimes
   * @function   isBothModifyInProgress
   * @memberof   ModifyController
   * @return     {boolean}  True if any modify in progress, False otherwise.
   */
  isBothModifyInProgress() {
    return Rules.isModifyInProgress() && Rules.isInFlightModify();
  }
};

/**************************
 * COMPONENT INTERACTIONS *
 **************************/
const Components = {
  toggleConfirmationModal (bool) {
    ModifyActions.toggleConfrimationModal(bool);
  },
  toggleCancelReservationModal (bool) {
    ModifyActions.toggleCancelReservationModal(bool);
  },
  togglePrepayTerms (bool) {
    ModifyActions.togglePrepayTerms(bool);
  },
  toggleRetrieveAndCancel (bool) {
    ModifyActions.toggleRetrieveAndCancel(bool);
  }
};

const ModifyController = debug({
  isDebug: false,
  logger: {},
  name: 'ModifyController',
  ...ServiceData,
  ...Rules,
  ...Components,

  /**
   * Called by RouterController when modify step loads
   * @memberof   ModifyController
   * @returns    {void} sets values in state tree
   */
  routeSetup() {
    VerificationActions.setModifyInformation();
  }
});

module.exports = ModifyController;
