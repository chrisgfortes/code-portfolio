/**
 * To handle logging of server-side and application events.
 * Emulates CROS messages interface to some degree.
 * @Todo: Handle usage of Debugger better. Should it use global window._isDebug or Prod/Dev flag?
 */
// @todo remove these as the interface should be factories, this is carry over before classes existed in ecom
import Message from '../classes/Message';
import MessagesList from '../classes/MessagesList';

import { Debugger } from '../utilities/util-debug';
import Analytics from '../modules/Analytics';

const Logger = new Debugger(true, {isDebug: true}, true, 'MessageLogController');

/**
 * @module MessageLogControler
 * There's gonna be more here ultimately. This isn't even where the Classes should live,
 * but we don't currently have a class set up presently.
 *
 * Also, our errors / messages / logs / debugging set up is going to be *very* different
 * from this.
 * @todo Integrate Analytics
 * @todo Add settings
 * @todo Add debug settings
 * @todo Move classes elsewhere and add all of the above :-)
 * @todo no idea what the defaults should be longer term
 *
 * Related: ErrorActions, ErrorMiddleware, Error, MessageActions
 *
 */

/** MessageLogger
 * @Class MessageLogger
 * @extends    MessagesList
 * All we're doing is decorating MessagesList and adding some functionality
 * Why? Because MessagesList was part of MessageLogger originally, this just
 * keeps it working. (ECR-13850)
 */
class MessageLogger extends MessagesList {
  constructor (...args) {
    super(...args)
  }

  track () {
    if (this.tracking) {
      MessageLogger.debug('We are tracking these log instances...', [{errors: this.messages}]);
      Analytics.trigger('html', 'errors', [{errors: this.messages}]);
    }
  }

  get tracking () {
    return enterprise.settings.messageLogTracking;
  }

  static log () {
    Logger.log(...arguments);
  }
  static info () {
    Logger.info(...arguments);
  }
  static warn () {
    Logger.warn(...arguments);
  }
  static debug () {
    Logger.debug(...arguments);
  }
}

module.exports = {
  MessageLogger: MessageLogger,
  Message: Message
};


