/* global enterprise, _ */
/**
 * @module RedirectController
 *
 * This controls:
 * (1) the Global Gateway (IP / Region / Domain) Redirects
 * (2) This also controls the Browser / Client-side browser detection code, which may no
 *  longer be relied upon as it appears they use Akamai more and more for this.
 *  Note: Why is the browser and Global Gateway code in the same place? Because both rely on
 *    the HEAD Request we issue to inspect Akamai headers. An enhancement might be to segement
 *    this code further but rely on the data collection as an outside action (?)
 * (3) This also fires the browser language code lookup. This is always used.
 *
 * Example values for Akamai-Edgescape HTTP headers:
 *  georegion=263,country_code=US,region_code=MA,city=OXFORD,dma=506,pmsa=9240,msa=1122,areacode=508,county=WORCESTER,fips=25027,lat=42.1190,long=-71.858,timezone=EST,zip=01540,continent=NA,throughput=vhigh,bw=5000,asnum=20115
 *  georegion=2,country_code=GB,region_code=EN,city=LONDON,dma=,pmsa=,msa=,areacode=,county=,fips=,lat=51.50,long=-0.12,timezone=GMT,zip=,continent=EU,throughput=vhigh,bw=5000,asnum=29302
 *  georegion=155,country_code=NL,region_code=,city=AMSTERDAM,dma=,pmsa=,msa=,areacode=,county=,fips=,lat=52.35,long=4.92,timezone=GMT+1,zip=,continent=EU,throughput=vhigh,bw=5000,asnum=205016
 *  georegion=208,country_code=SE,region_code=AB,city=STOCKHOLM,dma=,pmsa=,msa=,areacode=,county=,fips=,lat=59.33,long=18.05,timezone=GMT+1,zip=,continent=EU,throughput=vhigh,bw=5000,asnum=205016
 *  georegion=295,country_code=CA,region_code=ON,city=TORONTO,dma=,pmsa=,msa=,areacode=,county=,fips=,lat=43.67,long=-79.42,timezone=EST,zip=M3H+M3M+M4B+M4C+M4E+M4G+M4H+M4J+M4K+M4L+M4M+M4N+M4P+M4R+M4S+M4T+M4V+M4W+M4X+M4Y+M5A+M5B+M5C+M5E+M5G+M5H+M5J+M5K+M5L+M5M+M5N+M5P+M5R+M5S+M5T+M5V+M5W+M5X+M6A+M6B+M6C+M6E+M6G+M6H+M6J+M6K+M6L+M6M+M6N+M6P+M6R+M6S+M7A+M7Y+M9M+M9N+M9P+M9W,continent=NA,throughput=vhigh,bw=5000,asnum=36351
 *  georegion=77,country_code=FR,region_code=,city=PARIS,dma=,pmsa=,msa=,areacode=,county=,fips=,lat=48.87,long=2.33,timezone=GMT+1,zip=,continent=EU,throughput=vhigh,bw=5000,asnum=9009
 *
 * There is a localStorage hack (see below) to test the basic logic, but it does bypass the checking of the
 *  headers from Akamai (hard codes them instead of using what is fetched)
 *
 * Global Gateway leverages the values we get back from AEM and compares with Akamai's Edgescape headers
 * Same values from AEM are:
 *  enterprise.domainRedirect.domainToUrlMap
 *    {
 *      // see below debugOverrideDomainMap
 *    }
 */
import RedirectActions from '../actions/RedirectActions';
import LocaleController from './LocaleController';
import { GLOBAL } from '../constants';
import RedirectService from '../services/RedirectService';

import Cookie from '../utilities/util-cookies';
import Browser from '../utilities/util-env-browser';
import Storage from '../utilities/util-localStorage';
import { debug } from '../utilities/util-debug';

let logger;

// which tasks to use debug methods for
// const debugTasks = ['debugOverrideHeaders', 'debugOverrideDomainMap'];
const debugTasks = ['debugOverrideHeaders'];

/**
 * For debugging only.
 * Set debugging for sample Akamai Header for debugging purposes
 * this.headers[this.headerStr.locale] = 'Akamai-Edgescape:georegion=255,country_code=US,region_code=IL,city=WESTERNSPRINGS,dma=602,pmsa=1600,msa=1602,areacode=708,county=COOK,fips=17031,lat=41.8114,long=-87.9019,timezone=CST,zip=60558,continent=NA,throughput=vhigh,bw=5000,asnum=33491';
 * Pull Akamai-Device-Characteristics from localStorage for testing when headers are not present.
 * To set this up, use:
 *
 *  > localStorage.setItem('Akamai-Edgescape', 'georegion=2,country_code=GB,region_code=EN,city=LONDON,dma=,pmsa=,msa=,areacode=,county=,fips=,lat=51.50,long=-0.12,timezone=GMT,zip=,continent=EU,throughput=vhigh,bw=5000,asnum=29302')
 *  and
 *  > localStorage.setItem('debugging', true);
 *
 * Then, when the domains do not match, the code will take the correct action.
 * @function   debugOverrideHeaders
 * @memberOf RedirectController
 * @private
 */
function debugOverrideHeaders (headerType) {
  let overHeaders = (Storage.supported && Storage.LocalStorage.get(headerType));
  if (overHeaders) {
    RedirectController.headers[headerType] = overHeaders;
    overHeaders = null;
  }
}

/**
 * For debugging only
 * Note: include this function name in debugTasks
 * @function   debugOverrideDomainMap
 * @memberof   RedirectController
 * @return     {Object}  object to replace enterprise.domainRedirect.domainToUrlMap for testing
 * This is ONLY USED when debugTasks array above includes the method name
 * @private
 */
function debugOverrideDomainMap () {
  return {
    "co-ca": "www.enterprise.ca",
    "co-de": "www.enterprise.de",
    "co-es": "www.enterprise.es",
    "co-fr": "www.enterprise.fr",
    "co-ie": "www.enterprise.ie",
    "co-uk": "www.enterprise.co.uk",
    // "co-uk": "localhost:4502/content/co-uk/en/home.html",
    "ecom": "www.enterprise.com",
    "franchisee_at": "www.enterpriserentacar.at",
    "franchisee_be": "www.enterpriserentacar.be",
    "franchisee_bg": "www.enterpriserentacar.bg",
    "franchisee_ch": "www.enterprise.ch",
    "franchisee_cz": "www.enterpriserentacar.cz",
    "franchisee_dk": "www.enterprise.dk",
    "franchisee_gr": "www.enterprise.gr",
    "franchisee_hr": "www.enterprise.hr",
    "franchisee_hu": "www.enterprise.hu",
    "franchisee_is": "www.enterpriserentacar.is",
    "franchisee_it": "www.enterpriserentacar.it",
    "franchisee_nl": "www.enterprise.nl",
    "franchisee_no": "www.enterprise.no",
    "franchisee_pl": "www.enterpriserentacar.pl",
    "franchisee_pt": "www.enterprise.pt",
    "franchisee_se": "www.enterprise.se",
    "franchisee_tr": "www.enterprise.com.tr"
  }
}

const RedirectController = {
  // references to the promises saved in enterprise.environment.cdn.requests
  requests: {
    lang: null,
    domainHead: null,
    browser: null
  },
  // results of if statements
  envState: {
    localeRedirects: false,   // are locale / global gateway redirects enabled?
    cookieDomain: false,      // do we have a cookie for a domain redirect?
    browserRedirect: false,  // are browser-based redirects set to true?
    cookieBrowser: false,      // is a cookie-based browser override set?
    browserServerSideDetection: false, // do we feature server-side detection?
    browserClientSideRedirect: false,  // do we do a client-side redirect?
    browserClientSideDetection: false      // do we do client-side detection?
  },
  /**
   * Logic for Redirects
   * Global Gateway / Location Detect + Browser Detection
   * (1) run getBrowserPreferredLanguages request
   * (2) run HEAD request
   * (3) when promises are fulfilled, execute location redirect logic model settings + browser detection logic
   * No Location Detection necessary but Browser Detection Active
   * (1) only run HEAD request
   * (2) when promise is done, execute browser logic
   *
   * See the bottom of this doc for information on testing the server-side detection prior to it being implemented.
   */
  initiate () {
    logger = debug({isDebug: enterprise.settings.debugging, name: 'RedirectController'}).logger;
    // expose some of this
    enterprise.environment.cdn.requests = this.requests;
    enterprise.environment.cdn.requestsSettings = this.envState;
    enterprise.environment.cdn.headers = this.headers;

    // test conditions and set up environment variables
    this.conditionalSetup();

    // locale detection only
    if (this.envState.cookieDomain) {
      logger.log('this.envState.cookieDomain lookup using', this.envState.cookieDomain);
      let redirectUrl = enterprise.domainRedirect.domainToUrlMap[this.envState.cookieDomain];
      if (redirectUrl) {
        logger.warn('found "enterprise.domainRedirect.domainToUrlMap" mapping for ', redirectUrl);
        RedirectActions.goDomainRedirect(redirectUrl);
      }
    } else if (!this.envState.cookieDomain && this.envState.localeRedirects) {
      logger.log('01 calling GET request getBrowserPreferredLanguages().');
      this.getPreferredLanguage();
    } else {
      logger.log('Failed to get global gateway locale and related data.');
      this.requests.lang = Promise.reject(false);
    }

    if (!this.envState.cookieDomain && this.envState.localeRedirects ||
        !this.envState.cookieBrowser && this.envState.browserRedirect && this.envState.browserServerSideDetection) {
      logger.log('001 Getting HEAD request for Akamai headers.');
      this.getHeadRequest();
    } else {
      logger.warn('0000 else ... ?')
      if (!this.envState.cookieBrowser && !this.envState.browserServerSideDetection && this.envState.browserRedirect
          && this.envState.browserClientSideDetection) {
        logger.warn('0010 ... successful HEAD request.');
        this.requests.browser = Promise.resolve(true);
      } else {
        logger.warn('0020');
        this.requests.domainHead = Promise.reject(false);
      }
    }

    // watch responses and take action
    this.handleResponses();
  },
  /**
   * Http header data is stored here while working.
   */
  headers: {},
  /**
   * Strings for the headers we pull
   */
  headerStr: {
    // you can test these by adding a header to the responses via Charles or a HTTP Header Modification tool (see above)
    'locale': GLOBAL.HTTP_AKAMAI_EDGE,
    'device': GLOBAL.HTTP_DEVICES
  },
  debugging: enterprise.settings.debugging,
  conditionalSetup () {
    // global gateway / language redirect settings
    let cookieDomain = Cookie.get('defaultDomain');
    let disableRedirect = Cookie.get('disableRedirect') === 'true';
    let start = !disableRedirect && !enterprise.disableIpRedirect && enterprise.currentView === 'home';

    // browser redirection settings
    let serverSideDetection = enterprise.settings.browserServerSideDetection; // are we doing server-side detection
    let clientSideRedirect = enterprise.settings.browserClientSideRedirect; // client side detection
    let clientSideDetection = enterprise.settings.browserClientSideDetection; // client side detection
    let cookieBrowser = (Cookie.get('cookieBrowser')) ? true : false;
    let browserRedirect = !cookieBrowser && enterprise.settings.browserRedirect && (enterprise.currentView !== enterprise.settings.viewBrowserNotice); // overall feature;

    // logger.warn('browserRedirect: ', browserRedirect);

    // save settings
    this.envState.localeRedirects = start;
    this.envState.cookieDomain = cookieDomain;

    this.envState.browserRedirect = browserRedirect;
    this.envState.cookieBrowser = cookieBrowser;
    this.envState.browserServerSideDetection = serverSideDetection;
    this.envState.browserClientSideRedirect = clientSideRedirect;
    this.envState.browserClientSideDetection = clientSideDetection;
  },
  /**
   * Here we set up collections of promises to be checked with Promise.all
   */
  handleResponses () {
    // these do not depend on one another, so it doesn't matter what order they finish in
    let localePromises = [
      enterprise.environment.cdn.requests.lang,
      enterprise.environment.cdn.requests.domainHead
    ];
    this.redirect.localeDomain(this, localePromises);

    let browserPromises = [
      enterprise.environment.cdn.requests.domainHead
    ];
    this.redirect.browser(this, browserPromises);
  },
  homeRedirect() {
    RedirectActions.goHome();
  },
  // the redirects we have collect information and sometimes need to wait for
  // that information to be collected. here we expose the state of that
  // information lookup.
  redirect: {
    // Global Gateway via Akamai
    localeDomain: (scope, proms) => {
      return Promise.all(proms)
        .then( ()=> {
          logger.warn('9999 getBrowserPreferredLanguages() + Akamai HEAD promises resolved.');
          scope.localRedirectSettings(scope.headers[scope.headerStr.locale]);
        })
        .catch( (err) => {
          if (err !== false) {
            console.warn('RedirectController.redirect.localeDomain() detection promises issue.', err);
          }
        });
    },
    // Browser Detection via Akamai
    browser: (scope, proms) => {
      return Promise.all(proms)
        .then( () => {
          logger.warn('9999 Akamai headers for Browser Detection resolved.');
          if (scope.envState.browserServerSideDetection) {
            let h = scope.headers[scope.headerStr.device];
            if (h && h !== null) {
              let parsedObject = scope.parseBrowserHeaders(h);
              logger.log('Parsed Browser Headers for Detection:', parsedObject);
              enterprise.environment.browser = Browser.extend(parsedObject);
            } else {
              logger.log(`Not using device ${scope.headerStr.device} headers because they do not exist.`);
            }
          }
          if (scope.envState.browserRedirect) {
            let safeBrowser = enterprise.environment.browser.judgement();
            if (!safeBrowser && scope.envState.browserClientSideRedirect) {
              logger.warn('Browser failed detection and will be redirected.');
              RedirectActions.goBrowserRedirect(enterprise.settings.browserUpgradeUrl, false);
            } else if (!safeBrowser) {
              logger.log('Browser failed detection tests and should be redirected. But not today.');
            } else {
              logger.log('Browser detection completed.');
            }
          } else {
            logger.warn('Browser redirect disabled by cookie or setting.');
          }
        })
        .catch( (err) => {
          if (err !== false) {
            console.warn(' .... browser detection promises issue.', err);
          }
        });
    }
  },
  parseBrowserHeaders (h) {
    let parsed;
    let temp;
    let output = {};
    // mobile_browser=Chrome,brand_name=Chrome,device_os=Mac OS X,marketing_name=Chrome 53,model_name=53,mobile_browser_version=53
    if (h && h !== null) {
      parsed = h.split(',');
      parsed.forEach(function(i){
        temp = i.split('=');
        output[temp[0]] = temp[1];
      });
    } else {
      console.warn('No Akamai Device Headers despite enterprise.settings.browserServerSideDetection = true.');
    }
    logger.warn('parseBrowserHeaders:', output);
    return {
      rawname: output.mobile_browser || '',
      version: output.mobile_browser_version || '',
      device: output.model_name || '',
      deviceVendor: output.brand_name || '',
      osName: output.device_os || '',
      versionRelease: output.marketing_name || '',
      userAgent: navigator.userAgent
    };
  },
  getPreferredLanguage () {
    logger.log('02 calling getBrowserPreferredLanguages()');
    // reference this.handleResponses() for full callback logic
    this.requests.lang = RedirectService.getBrowserPreferredLanguages()
      .then((languages)=> {
        logger.log('03 set.preferredLanguage(languages)', languages);
        RedirectActions.set.preferredLanguage(languages)
      });
  },
  getHeadRequest () {
    logger.log('002 calling RedirectService.getAkamaiHeaders()');
    // reference this.handleResponses() for full callback logic
    this.requests.domainHead = RedirectService.getAkamaiHeaders()
      .then((response, textStatus, request)=> {
        logger.warn('003 getAkamaiHeaders() returned.');
        this.headers[this.headerStr.locale] = request.getResponseHeader(this.headerStr.locale);
        this.headers[this.headerStr.device] = request.getResponseHeader(this.headerStr.device);

        if (enterprise.settings.debugging && debugTasks.length > 0) {
          logger.warn('004 [DEBUGGING] Overridding header values...');
          debugOverrideHeaders(this.headerStr.locale);
          debugOverrideHeaders(this.headerStr.device);
          if (debugTasks.includes('debugOverrideDomainMap')) {
            enterprise.domainRedirect.domainToUrlMap = debugOverrideDomainMap();
          }
        }
        logger.groupCollapsed();
        logger.log('005 Global Gateway headers set to:\n', this.headers[this.headerStr.locale]);
        logger.log('005 Akamai Device Detection Debugging Set To:\n', this.headers[this.headerStr.device]);
        logger.groupEnd();
      });
  },
  // once we have all the data we need, then we redirect to the right place
  localRedirectSettings (akamaiHeaders) {
    let match;
    let akamaiCountryCode;
    let akamaiCountryDomain;
    let currentHost = enterprise.currentDomain;

    if (!akamaiHeaders){
      logger.log('Locale and Global Gateway completed without redirect.');
      return false;
    }

    logger.log('Global gateway using these akamaiHeaders:', akamaiHeaders);

    match = akamaiHeaders.match(/country_code=(.*?),/);

    if (match && match.length === 2) {
      akamaiCountryCode = match[1];
      logger.log('matching!', match, akamaiCountryCode);
    }

    akamaiCountryDomain = enterprise.domainRedirect.countryCodeToDomainMap[akamaiCountryCode];

    if (akamaiCountryCode && akamaiCountryDomain) {
      logger.group();
      logger.warn('A redirect based on Global Gateway modal will open:');
      logger.log('currentHost:', currentHost);
      logger.log('akamaiCountryCode: (from Akamai)', akamaiCountryCode);
      logger.log('akamaiCountryDomain: (from mapping)', akamaiCountryDomain);
      logger.log('Modal will redirect to:', enterprise.domainRedirect.domainToUrlMap[akamaiCountryDomain]);
      logger.groupEnd();
      if (currentHost !== akamaiCountryDomain) {
        RedirectActions.set.type('ip');
        RedirectActions.set.country(akamaiCountryDomain);
        let redirectDomain = RedirectActions.get.country();
        let browserPreferredLang = RedirectActions.get.preferredLanguage();
        RedirectActions.set.destinationLang(this.determineDestinationLang(redirectDomain, browserPreferredLang));
        RedirectActions.set.modal(true); // opens RedirectModal
      }
    }
  },
  determineDestinationLang () {
    return LocaleController.determineDestinationLang();
  }
};

export default RedirectController;
