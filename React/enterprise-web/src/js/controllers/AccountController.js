import ErrorActions from '../actions/ErrorActions';
import AccountActions from '../actions/AccountActions';
import KeyRentalFactsActions from '../actions/KeyRentalFactsActions';
import EnrollActions from '../actions/EnrollActions';
import RedirectActions from '../actions/RedirectActions';

import {PARTNERS, PAGEFLOW} from '../constants/';

import { AccountFactory } from '../factories/User';

import { MessageLogger } from './MessageLogController';

import AccountService from '../services/AccountService';

import { debug } from '../utilities/util-debug';

/************************
 * SERVICE INTERACTIONS *
 ************************/
const ServiceData = {
  updateLicense: (params) => {
    let data = AccountFactory.toServer.updateLicense(params);

    return AccountService.updateProfile.license(data)
      .then((response) => {
        ErrorActions.clearErrorsForComponent('accountDriver');
        if (response.license_profile) {
          AccountActions.setAccountEditModal(null);
          AccountService.getSession();
        } else {
          ErrorActions.setErrorsForComponent(response, 'accountDriver');
          if (response.messages && response.messages.length === 0) {
            let error = new MessageLogger();
            let logMessage = enterprise.settings.frontEndMessageLogCodes.accountEditDriverUnhandled;
            error.addMessage(logMessage);
            console.error('EditDriver.js: Unhandled error on driver details save.', logMessage);
            error.track();
          }
        }
      });
  },
  updateContact: (params) => {
    let data = AccountFactory.toServer.updateContact(params);

    return AccountService.updateProfile.contact(data)
      .then((response) => {
        ErrorActions.clearErrorsForComponent('accountContact');
        // this one is weird, we always get messages back?
        if (response.messages && response.messages.length > 0) {
          for (let i = 0, len = response.messages.length; i < len; i++) {
            if (response.messages[i].priority === 'ERROR') {
              ErrorActions.setErrorsForComponent(response, 'accountContact');
              break;
            }
            if (response.messages[i].priority === 'WARN') {
              AccountActions.setAccountEditModal(null);
              AccountService.getSession();
              break;
            } else if (response.messages[i].priority === 'INFO') {
              AccountActions.setAccountEditModal(null);
              AccountService.getSession();
              break;
            } else {
              let error = new MessageLogger();
              error.addMessages(response.messages);
              console.error('EditContact.js: Unhandled message. Messages:', response.messages);
              error.track();
              break;
            }
          }
        } else {
          AccountActions.setAccountEditModal(null);
          AccountService.getSession();
        }
      });
  },
  getCountries: () => {
    const pickupCountryCode = EnrollActions.getCountryCode();
    return AccountService.getCountries()
      .then((response) => {
        const countries = Components.orderByCountries(response.countries);
        AccountActions.setCountries(countries);
        KeyRentalFactsActions.setDisputesContactInfo(countries, pickupCountryCode);
        return response;
      });
  },
  getIssueCountrySubdivisions: (countryCode) => {
    return AccountService.getIssueCountrySubdivisions(countryCode)
      .then((response) => {
        const regions = Components.orderByRegions(response.regions);
        //@todo - Remove this method `setIssueCountrySubdivisions` we aren't using more account.issueCountrySubdivisions
        AccountActions.setIssueCountrySubdivisions(regions);
        AccountActions.setSubdivisions(regions);
      });
  },
  unsubscribe: () => {

  }
};

/**************************
 * COMPONENT INTERACTIONS *
 **************************/
const Components = {
  cleanEditModalErrors(component){
    const componentName = (
      _.isString(component) ?
        component :
        _.get(component, 'component')
    );

    if (!!componentName) {
      ErrorActions.clearErrorsForComponent('loginWidget');
      ErrorActions.clearErrorsForComponent(componentName);
    }
  },
  setIssueCountrySubdivisions: (params) => {
    //@todo - Remove this method `setIssueCountrySubdivisions` we aren't using more account.issueCountrySubdivisions
    AccountActions.setIssueCountrySubdivisions(params);
    AccountActions.setSubdivisions(params);
  },
  cleanFormData: (form) => {
    return AccountActions.cleanFormData(form);
  },
  setAccountEditModal: (params) => {
    AccountActions.setAccountEditModal(params);
  },
  setAccountTab: (tab, redirect) => {
    AccountActions.setAccountTab(tab);
    if(redirect) RedirectActions.goToHash(tab);
  },
  redirectToHome: () => {
    RedirectActions.goHome();
  },
  hashRouter: (loggedIn) => {
    let hash = location.hash.replace('#', '');
    if (enterprise.currentView === PAGEFLOW.ACCOUNT) {
      if (!loggedIn && hash !== PARTNERS.BRAND.EMERALD_CLUB.toLowerCase()) {
        RedirectActions.goToHash('#ep');
      }

      if (loggedIn && (hash === PARTNERS.BRAND.EMERALD_CLUB.toLowerCase() || hash === PARTNERS.BRAND.ENTERPRISE_PLUS.toLowerCase())) {
        RedirectActions.goToHash('#settings');
      }

      const map = [{
        hash: 'reservation',
        signup: false
      },{
        hash: 'reward',
        signup: false
      },{
        hash: 'ec',
        signup: true
      },{
        hash: 'ep',
        signup: true
      }];

      const goTo = map.filter((item) => (item.hash === hash));

      if(goTo.length){
        AccountController.setAccountTab(goTo[0].hash);
        $(".signup")[(goTo[0].signup ? 'show' : 'hide')]();
      }else{
        AccountController.setAccountTab('settings');
        $(".signup").hide();
      }
    }
  },
  getSupportNumbers: (support) => {
    let supportPhones = _.get(support, "support_phone_numbers");
    let phoneForEP;
    let extraDefault = "1-866-507-6222"; //@todo move this elsewhere
    if (supportPhones) {
      phoneForEP = supportPhones.find(i => {
        return i.phone_type === "EPLUS";
      });

      if (phoneForEP) {
        phoneForEP = phoneForEP.phone_number;
      } else {
        phoneForEP =
          supportPhones.length > 0
            ? supportPhones[0].phone_number
            : extraDefault;
      }
    } else {
      phoneForEP = extraDefault;
    }

    return phoneForEP;
  },
  rewards: {
    rentalsToMaintain: (nextTierRentalGoal, rentalsToDate) => {
      return Math.max(nextTierRentalGoal - rentalsToDate, 0);
    },
    daysToMaintain: (nextTierDayGoal, rentalDaysToDate) => {
      return Math.max(nextTierDayGoal - rentalDaysToDate, 0);
    },
    rentalPercentageComplete: (rentalsToDate, nextTierRentalGoal) => {
      return ((rentalsToDate / nextTierRentalGoal) * 100).toFixed(2);
    },
    rentalsDashOffset: (magicNumber, rentalPercentageComplete) => {
      return Math.max(magicNumber - Math.round(parseFloat((magicNumber * rentalPercentageComplete) / 100)), 0)
    },
    daysPercentageComplete: (nextTierDayGoal, rentalDaysToDate) => {
      return nextTierDayGoal <= 0 ? 0 : ((rentalDaysToDate / nextTierDayGoal) * 100).toFixed(2);
    },
    daysDashOffset: (magicNumber, daysPercentageComplete) => {
      return Math.max(magicNumber - Math.round(parseFloat((magicNumber * daysPercentageComplete) / 100)), 0)
    }
  },
  goToReservationStep: (step) => {
    RedirectActions.goToReservationStep(step);
  },
  orderByCountries(countries, type = 'country_name', order = 'asc') {
    return _.orderBy(countries, [type], [order]);
  },
  orderByRegions(regions, type = 'country_subdivision_name', order = 'asc') {
    return _.orderBy(regions, [type], [order]);
  },
  getLabelBasedOnCountry(mapper, country) {
    return mapper.find((item) => item.countries.includes(country)) || {};
  },
  getSubdivisionLabelBasedOnCountry(countryCode){
    const subdivisionI18n = [
      { countries: ['US'], label: i18n('resflowreview_0066') },
      { countries: ['CA'], label: i18n('resflowreview_0067') },
      { countries: ['GB', 'IE', 'ES', 'FR'], label: i18n('resflowreview_0071') },
      { countries: ['DE'], label: i18n('resflowreview_0071') }
    ]

    const translate = AccountController.getLabelBasedOnCountry(subdivisionI18n, countryCode);
    return _.get(translate, 'label') || i18n('resflowreview_0066');
  },
  clearProfile(){
    AccountActions.setProfile();
  },
  getPersonalProfile(){
    return AccountActions.get.personalModel();
  }
};

const AccountController = debug({
  isDebug: false,
  logger: {},
  name: 'AccountController',
  ...ServiceData,
  ...Components
});

export default AccountController;
