import ExistingReservationActions from '../actions/ExistingReservationActions';
import ReservationActions from "../actions/ReservationActions";
import LoginActions from '../actions/LoginActions';
import ReceiptActions from '../actions/ReceiptActions';
import CorporateActions from "../actions/CorporateActions";
import ErrorActions from "../actions/ErrorActions";

import ReceiptController from "./ReceiptController";
import { errorTriggers } from "./ErrorsController";
import ServiceController from './ServiceController';

import ReceiptService from '../services/ReceiptService';
import ExistingReservationService from '../services/ExistingReservationService';

import { debug } from '../utilities/util-debug';

/************************
 * SERVICE INTERACTIONS *
 ************************/
const ServiceData = {
  checkForMyTrips() {
    ServiceData.getMyTripsWithLoggedInUser().then(() => {
      if (ExistingReservationActions.get.reservationHasLength()) {
        ExistingReservationActions.set.reservationSearchVisibility(false);
      }
      ExistingReservationActions.set.loadingState(false);
    });
  },
  getExistingReservation(confirmation, firstName, lastName, callback) {
    const employeeNumber = ExistingReservationActions.get.employeeNumber();
    const requisitionNumber = ExistingReservationActions.get.fedexRequisitionNumber();

    const thisCall = () => {
      ServiceData.getExistingReservation(
        confirmation,
        firstName,
        lastName,
        callback
      );
    };

    const data = {
      firstName: firstName,
      lastName: lastName,
      confirmationNumber: confirmation,
      requisitionNumber: requisitionNumber,
      employeeID: employeeNumber,
      enableNorthAmericanPrepayRates: !enterprise.hidePrepay
    };

    ExistingReservationActions.set.loadingState(true);

    return ExistingReservationService
      .getReservation(data)
        .then(res => {
          ExistingReservationActions.set.loadingState(false);
          ExistingReservationActions.set.existingReservations([res]);
          if (callback) callback();
        })
        .catch((errorMessages) => {
          ExistingReservationActions.set.loadingState(false);
          ReservationActions.setLoadingClassName(null);

          errorTriggers(errorMessages, false, {
            "CROS_RES_FEDEX_RETRIEVE_MISSING_PARAMETERS": () => {
              LoginActions.setCallbackAfterAuthenticate(thisCall);
              CorporateActions.showModalInState("fedexRentalLookup");
            },
            "default": () => {
              ErrorActions.setErrorsForComponent(errorMessages, "existingReservations");
            }
          });

          if (callback) callback();
          ServiceController.trap('ExistingReservationController.getExistingReservation().catch()', errorMessages);
        });
  },
  getExistingReservationDetails(confirmation, firstName, lastName, modify, requisitionNumber, employeeNumber) {
    let data = {
      firstName: firstName,
      lastName: lastName,
      requisitionNumber: requisitionNumber,
      employeeID: employeeNumber
    };
    if (modify) {
      data.modify = true;
    }
    ReservationActions.setLoadingClassName("loading");
    ExistingReservationActions.set.loadingState(true);
    ErrorActions.clearErrorsForComponent("existingReservations");

    const reservationDetailsResult =
    ExistingReservationService
      .reservationDetails(
        confirmation,
        data
      )
      .then(res => {
        ExistingReservationActions.set.loadingState(false);
        ExistingReservationActions.set.existingReservations([res]);
      })
      .catch((errorMessages) => {
        ExistingReservationActions.set.loadingState(false);
        ReservationActions.setLoadingClassName(null);

        errorTriggers(errorMessages, false, {
          "default": () => {
            ErrorActions.setErrorsForComponent(errorMessages, "existingReservations");
          }
        });
        ServiceController.trap('ExistingReservationController.getExistingReservationDetails().catch()', errorMessages);
      });

    return reservationDetailsResult;
  },
  getMyTripsWithLoggedInUser() {
    const myTripsResult = ExistingReservationService.getMyTrips()
      .then(res => {
        if (res.messages && !res.hasOwnProperty("trip_summaries")) {
          ExistingReservationActions.set.myTrips({ error: true });
        } else if (res) {
          ExistingReservationActions.set.myTrips(res);
        }
      })
      .catch((err) => {
        ExistingReservationActions.set.loadingState(false);
        ReservationActions.setLoadingClassName(null);
        ExistingReservationActions.set.myTrips({
          error: true
        });
        ServiceController.trap('ExistingReservationController.getMyTripsWithLoggedInUser().catch()', err);
      });

    return myTripsResult;
  },
  getReceipt(reservation){
    ReceiptService
      .getReceipt(reservation.invoice_number)
      .then((response) => {
        if (response.past_trip_detail) {
          ReceiptController.setReceipt(response.past_trip_detail);
        }else{
          ReceiptActions.setReceiptLoading(false);
        }
      })
      .catch((err) => {
        ReceiptActions.toggleModal(false);
        ReceiptActions.setReceiptLoading(false);
        ServiceController.trap('ExistingReservationController.getReceipt().catch()', err);
      });
  },
  getTripsOfType(type, more = false) {
    const startRecord = ExistingReservationActions.get.startRecordOfType(type);
    ExistingReservationActions.set.myTripsLoadingOfType("loading", type);

    const queryRequest = {
      startRecord: startRecord,
      ecordCount: 5
    };

    const myTripsResult =
      ExistingReservationService
        .getMyTrips(type.toLowerCase(), queryRequest)
        .then((res) => {
          if (res.messages && !res.hasOwnProperty("trip_summaries")) {
            ExistingReservationActions.set.myTripsLoadingOfType("error", type);
          } else if (res && res.trip_summaries.length) {
            if (res.hasOwnProperty("more_records_available")) {
              ExistingReservationActions.set.moreMyTripsOfType(
                !!res.more_records_available,
                res.tripsType
              );
            }
            ExistingReservationActions.set.startRecordOfType(
              startRecord + res.trip_summaries.length + 1,
              res.tripsType
            );
            ExistingReservationActions.set.myTripsOfType(
              res.trip_summaries,
              res.tripsType,
              more
            );
            ExistingReservationActions.set.myTripsLoadingOfType(
              "has-data",
              res.tripsType
            );
          } else if (res && !res.trip_summaries.length) {
            ExistingReservationActions.set.myTripsLoadingOfType(
              "empty-data",
              type
            );
          } else {
            ExistingReservationActions.set.myTripsLoadingOfType("error", type);
          }
        })
        .catch(error => ServiceController.trap('ExistingReservationController.getTripsOfType().catch()', error));

    return myTripsResult;
  }
};

/************************
 * BUSINESS MODEL RULES *
 ************************/
const Rules = {};

/**************************
 * COMPONENT INTERACTIONS *
 **************************/
const Components = {
  getString(type, tripsType) {
    let str = i18n('reservationwidget_0024').concat(" ");
    if (type === "myTrips") {
      switch (tripsType) {
        case "NO_TRIPS_AVAILABLE":
          str = i18n('reservationwidget_0025');
          break;
        case "CURRENT":
          str.concat(i18n('resflowviewmodifycancel_1021')).concat(":");
          break;
        case "PAST":
          str.concat(i18n('resflowviewmodifycancel_1022')).concat(":");
          break;
      }
    } else if (type === "error") {
      str = i18n('resflowviewmodifycancel_0014');
    } else {
      if (tripsType === "NO_TRIPS_AVAILABLE") {
        str = i18n('resflowviewmodifycancel_0014');
      }
    }
    return str;
  },
  callSetReservationSearchVisibility(bool) {
    ExistingReservationActions.set.reservationSearchVisibility(bool);
  },
  callGetSupportPhoneNumberOfType(type) {
    return ExistingReservationActions.get.supportPhoneNumberOfType(type);
  },
  viewReceipt(e, reservation) {
    e.preventDefault();
    ServiceData.getReceipt(reservation);
    ReceiptActions.toggleModal(true);
  },
  openRentalDetailsModal() {
    ExistingReservationActions.set.rentalDetailsModal(true);
  },
  cancelClick(reservation) {
    if (reservation.driver_info) {
      ServiceData.getExistingReservationDetails(
        reservation.confirmation_number,
        reservation.driver_info.first_name,
        reservation.driver_info.last_name,
        null, // callback
        ExistingReservationActions.get.fedexRequisitionNumber(),
        ExistingReservationActions.get.employeeNumber()
      );
    } else {
      ServiceData.getExistingReservationDetails(
        reservation.confirmation_number,
        reservation.customer_first_name,
        reservation.customer_last_name,
        null, // callback
        ExistingReservationActions.get.fedexRequisitionNumber(),
        ExistingReservationActions.get.employeeNumber()
      );
    }
  },
  detailsClick(reservation) {
    if (reservation.driver_info) {
      ServiceData.getExistingReservationDetails(
        reservation.confirmation_number,
        reservation.driver_info.first_name,
        reservation.driver_info.last_name,
        null, // callback
        ExistingReservationActions.get.fedexRequisitionNumber(),
        ExistingReservationActions.get.employeeNumber()
      );
    } else {
      ServiceData.getExistingReservationDetails(
        reservation.confirmation_number,
        reservation.customer_first_name,
        reservation.customer_last_name,
        null, // callback
        ExistingReservationActions.get.fedexRequisitionNumber(),
        ExistingReservationActions.get.employeeNumber()
      );
    }
  },
  resetMyTrips () {
    ExistingReservationActions.update.resetMyTrips();
  },
  getAllMyTrips(typeOfTrip) {
    const tripsType = (typeOfTrip ? [typeOfTrip] : ["UPCOMING", "CURRENT", "PAST"]);
    _.forEach(tripsType, (type) => {
      ServiceData.getTripsOfType(type);
    });
  },
  getReservationHasLength() {
    return ExistingReservationActions.get.reservationHasLength();
  },
  setReservationSearchVisibility(bool){
    ExistingReservationActions.set.reservationSearchVisibility(bool);
  },
  setLoadingState(bool){
    ExistingReservationActions.set.loadingState(bool);
  }
}

const ExistingReservationController = debug({
  isDebug: false,
  logger: {},
  name: 'ExistingReservationController',
  ...ServiceData,
  ...Rules,
  ...Components
});

export default ExistingReservationController;
