import PricingController from './PricingController';
import ReservationActions from '../actions/ReservationActions';
import ModifyActions from '../actions/ModifyActions';
import CancellationActions from '../actions/CancellationActions';
import CancelModifyService from '../services/CancelModifyService';
import ServiceController from '../controllers/ServiceController';
import { debug } from '../utilities/util-debug';

/************************
 * SERVICE INTERACTIONS *
 ************************/
const ServiceData = {
  getReservationEligibility() {
    CancelModifyService.getReservationEligibility()
      .then((response) => {
        this.logger.log('getReservationEligibility() service callback - sets cancel details');
        ReservationActions.setEligibility(response.reservationEligibility);
        let responseDetails = _.get(response.cancellationDetails, 'cancel_fee_details');
        let details = CancellationController.getAppliedFees(responseDetails);
        CancellationController.setCancellationDetails(details);
        ReservationActions.setPaymentInformation(response.payment);
        ModifyActions.setCancelRebook(response.cancelRebook);
      })
      .catch((err) => {
        ReservationActions.setLoadingClassName(null);
        ServiceController.trap('CancellationController.getReservationEligibility().catch()', err);
      })
  },

  cancelReservation ( confirmationNumber ) {
    const isRetrieveAndCancel = CancellationActions.getRetrieveAndCancel();

    ReservationActions.setLoadingClassName('loading');
    CancelModifyService
      .cancelReservation( confirmationNumber, isRetrieveAndCancel )
        .then(() => {
          ReservationActions.setLoadingClassName(null);
        })
        .catch((err) => {
          ReservationActions.setLoadingClassName(null);
          ServiceController.trap('CancellationController.cancelReservation().catch()', err)
        });
  }
};

/**************************
 * COMPONENT INTERACTIONS *
 **************************/
const Components = {
  /**
   * @deprecated by PricingFactory.getAppliedCancellationFees()
   * @see PricingFactory.getAppliedCancellationFees()
   * @param cancelFeeDetails
   * @returns {object}
   */
  getAppliedFees (cancelFeeDetails) {
    let appliedDetails;
    if (cancelFeeDetails) {
      for (let i = 0; i < cancelFeeDetails.length; i++) {
        if (cancelFeeDetails[i].fee_apply) {
          appliedDetails = cancelFeeDetails[i];
        }
      }
    }
    this.logger.debug('getAppliedFees()', cancelFeeDetails, appliedDetails);
    return appliedDetails;
  },

  // hack to prevent errors in near term. longer term the
  // (a) original_amount_payment and (b) original_amount_view should be used
  // @todo: remove this. the total value is passed as a property on cancelDetails down to
  //  the cancellation modal, but with the new values above that we get from
  //  GBO now (a) + (b) above, it will not be necessary in the longer term.
  // This call came up around ECR-12619 and ECR-12615
  // This change could be done at any point, really, but it created a number
  //  of small regressions so we need time to test.
  augmentCancelDetailsWithTotal (cancelDetails) {
    this.logger.log('augmentCancelDetailWithTotal', cancelDetails);
    if (cancelDetails && !cancelDetails.total) {
      // cancelDetails.total = _.get(this.state.session, 'paymentInformation.amount');
      ReservationActions.setCancelTotal(PricingController.getOriginalPaymentAmount());
    }
  },
  getCancelDetails (cancelDetails) {
    // let originalPayment = _.get(cancelDetails, 'original_amount_payment') || null;
    // let originalView = _.get(cancelDetails, 'original_amount_view') || null;
    // let isSameRegion = PricingController.isSamePricingRegion(originalPayment, originalView);
    // let isSameRegion = PricingController.isSamePricingRegion(cancelDetails);
    return cancelDetails;
  },

  setModifyCancelModal: CancellationActions.setModifyCancelModal.bind(CancellationActions),

  setCancellationDetails (details) {
    this.logger.warn('setCancellationDetails()', details);
    if (details) {
      // ReservationStateTree.select(ReservationCursors.cancellationDetails).set(details);
      CancellationActions.setCancelDetail(details);
      let paymentTotal = PricingController.getOriginalPaymentAmount();
      if (paymentTotal) {
        ReservationActions.setCancelTotal(paymentTotal);
      } else {
        this.logger.log('setCancellationDetails() found no paymentInformation.');
      }
    }
  },
  hasCancelFee (cancelDetails) {
    let hasCancelFee = false;
    let feeView = _.get(cancelDetails, 'fee_amount_payment.amount') || false;
    if (feeView) {
      hasCancelFee = true;
    }
    return hasCancelFee;
  },
  getCancelFee (cancelDetails) {
    let cancelFee;
    if (CancellationController.hasCancelFee(cancelDetails)) {
      cancelFee = _.get(cancelDetails, 'fee_amount_view.format') || PRICING.VALUE_ZERO;
    }
    return cancelFee;
  },
  hasRefund (cancelDetails) {
    let result = false;
    let refundAmount = _.get(cancelDetails, 'refund_amount_view.format') || false;
    if (refundAmount) {
      result = true;
    }
    return result;
  },
  getRefundAmount (cancelDetails) {
    let refund;
    if (CancellationController.hasRefund(cancelDetails)) {
      refund = _.get(cancelDetails, 'refund_amount_view.format') || PRICING.VALUE_ZERO;
    }
    return refund;
  },
};


const CancellationController = debug({
  isDebug: false,
  logger: {},
  name: 'CancellationController',
  ...ServiceData,
  ...Components
});

module.exports = CancellationController;
