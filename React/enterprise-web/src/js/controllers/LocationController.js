/**
 * @module LocationController
 * For general location "ENTITY" control code. Think about this as Location "Meta"...
 * IF this code is about locations *themselves* use this.
 * For Location Search specific code, please use LocationSearchController
 * tl;dr
 * LocationController for Location Entities
 * LocationSearchController for Location Search activities
 */
import ReservationActions from '../actions/ReservationActions';
import DateTimeActions from '../actions/DateTimeActions';
import LocationActions from '../actions/LocationActions';

import { LOCATION_SEARCH } from '../constants';

import ReservationCursors from '../cursors/ReservationCursors';

import LocationModel, {
  LocationSearchFactory,
  LocationHoursFactory,
  LocationDetailsFactory,
  LocationCoordinatesModelFactory
} from '../factories/Location';

import ServiceController from './ServiceController';

import LocationSearchService from'../services/LocationSearchService';
import LocationInputService from '../services/LocationInputService';

import { debug } from '../utilities/util-debug';
const logger = debug({isDebug: false, name: 'LocationController'}).logger;

/**
 * @namespace Rules
 * @memberOf LocationController
 * @type {Object}
 */
const Rules = {
  /**
   * We call location details only when we select a specific location; cities are not specific
   * @param  {object|string} location object: to pull type from when you don't know which property it's using
   *                                  string: type string
   * @return {boolean}
   * Why do we need something like this for something as mundane as a location type?
   * Because I found that we use location.locationType and location.type interchangeably
   * on the site ... until we normalize that we need this.
   */
  isLocationTypeCity (location) {
    // console.assert(location, 'NO LOCATION PASSED TO isLocationTypeCity()');
    let typeFor = (typeof location === 'object') ? LocationModel.getLocationType(location) : location;
    // logger.groupCollapsed('isLocationTypeCity()');
    // logger.warn('isLocationTypeCity()', typeFor);
    // logger.groupEnd();
    return (typeFor === LOCATION_SEARCH.VALUES.CITY || typeFor === LOCATION_SEARCH.STRICT_TYPE.CITY) ? true : false;
  },

  /**
   * Specify if the location is a country or not
   * @param  {object|string} location object: to pull type from when you don't know which property it's using
   *                                  string: type string
   * @return {boolean}
   */
  isLocationTypeCountry (location) {
    let typeFor = (typeof location === 'object') ? LocationModel.getLocationType(location) : location;
    return (typeFor === LOCATION_SEARCH.STRICT_TYPE.COUNTRY) ? true : false;
  },

  /**
   * Specify if the location is a country or city
   * @param  {object|string} location object: to pull type from when you don't know which property it's using
   *                                  string: type string
   * @return {boolean}
   */
  isLocationTypeCityOrCountry (location) {
    return (this.isLocationTypeCity(location) || this.isLocationTypeCountry(location)) ? true : false;
  },

  /**
   * @function isLocationHasType
   * @memberOf LocationController
   * @param  {*}  location - random "thing" to determine if it has a type or not
   * @return {Boolean}          true or false, checked to see if it has a type property
   */
  isLocationHasType (location) {
    return (LocationModel.getLocationType(location)) ? true : false;
  },

  /**
   * Like location type for city or airport, types are defined pretty inconsistently, so we consolidate
   * and check in one place, using one technique
   * @param  {object|string}  location object: location object
   *                                   string: location type string
   * @return {Boolean}          true or false
   */
  isLocationTypeMyLocation (location = {}) {
    let typeFor = (typeof location === 'object') ? LocationModel.getLocationType(location) : location;
    // logger.log('isLocationTypeMyLocation()', typeFor);
    return (typeFor === LOCATION_SEARCH.STRICT_TYPE.MYLOCATION) ? true : false;
  },

  /**
   * @function isTypeCityOrMyLocation
   * @memberOf LocationController
   * @param  {string}  locationString string location to test against
   * @return {Boolean}                true or false
   */
  isTypeCityOrMyLocation (locationString) {
    let typeFor = LocationController.isLocationTypeCityOrCountry(locationString) || LocationController.isLocationTypeMyLocation(locationString);
    // logger.log('isTypeCityOrMyLocation(locationString)', typeFor);
    return typeFor;
  }
}
/**
 * @namespace ServicesData
 * @type {Object}
 */
const ServicesData = {

  /**
   * Fetch location data from SOLR endpoints
   * Used by Booking Widget + Location search > LocationInput (and others?)
   * @memberOf LocationController
   * @function getLocations
   * @param {string} query to send to server
   * @param {string} type mostly LOCATION_SEARCH.PICKUP or LOCATION_SEARCH.DROPOFF
   */
  getLocations (query, type) {
    // logger.log('getLocations(query, type)', query, type);
    LocationInputService.fetchLocations(query)
                        .then((responseData) => LocationSearchFactory.toState(responseData, { type }) )
                        .then((locationData) => {
                          // logger.log('locationData() from factory:', locationData);
                          let {
                            hasResults,
                            results,
                            firstResult
                          } = locationData;
                          ReservationActions.setNoResults(type, !hasResults);
                          LocationActions.saveGetLocations(results);
                          if (enterprise.branchID && hasResults && _.get(results, 'branches.length') > 0) {
                            // logger.log('firstResult data:', firstResult, _.get(results, 'branches.length'));
                            LocationActions.setGlobalLocationDetail(firstResult);
                          }
                        })
                        .catch((err) => {
                          // @todo make sure we do something more robust here via serviceFactory
                          if (err === 'abort') {
                            return true;
                          }
                          ServiceController.trap('LocationController.getLocations().catch():', err);
                        })
  },

  /**
   * Get location detail data from server.
   * @memberOf LocationController
   * @param  {string} locationKey  id, locationId, peopleSoftId of the location
   * @param  {string} type  pickup, dropoff etc.
   * @param  {boolean} returnPromise should we return the promise?
   * @return {void|object}                sets location data
   */
  getLocationDetails(locationKey, type) {
    // logger.warn('getLocationDetails() 02')

    if (!locationKey) {
      return false;
    }

    let input = {
      source: 'LocationController',
      type
    }

    return LocationSearchService.fetchLocationDetails(locationKey, type)
                                .then( (response) => LocationDetailsFactory.toState(response, input) )
                                .then(({ hasPolicies, location, policies, type, dropoffAfterHour }) => {
                                  if (hasPolicies) {
                                    ReservationActions.setPolicies(type, policies);
                                  }
                                  ReservationActions.setAfterHour(LOCATION_SEARCH.DROPOFF, dropoffAfterHour);
                                  ReservationActions.setLocationDetails(location, type);
                                })
                                .catch((err)=>{
                                  ServiceController.trap('Issue calling getLocationDetails()', err);
                                })
  },

  /**
   * @memberOf LocationController
   * @function getClosedHoursForLocation
   * @param  {string} type                        pickup|dropoff|etc
   * @param  {string} peopleSoftId                location id
   * @param  {object} reservationsInitiateRequest initate request data
   * @return {object}                             jquery ajax xhr promise
   * CONVERTED TO NEW SERVICE SET UP; uses toState toServer
   * @todo copmare with LocationSearchService.getDateTimeValidity()
   */
  getClosedHoursForLocation (type, peopleSoftId, reservationsInitiateRequest) {
    // usually end up here from setLocation or setLocationForAll
    let {
      locationId,
      locationObj,
      date
    } = DateTimeActions.selectLocationHoursObjects(type, peopleSoftId);

    // logger.info(locationId, locationObj, date);

    // all this work to just get to and from dates...
    let dataFormat = LocationHoursFactory.toServer({
      reservationsInitiateRequest,
      date,
      type
    });

    if (!!locationId && !!date && (peopleSoftId || !LocationController.isLocationTypeCityOrCountry(locationObj))) {
      let data = dataFormat;
      /********************** SERVICE CALL *****************************************************************************/
      return LocationSearchService.fetchClosedHoursForLocation(type, locationId, data)
                                .then((response) => LocationHoursFactory.toState(response, { type }) )
                                .then((hoursData) => {
                                  let {
                                    closedDates,
                                    data
                                  } = hoursData;
                                  DateTimeActions.setClosedDates(closedDates, type); // datetimeactions
                                  ReservationActions.setLocationHours(data, type);
                                  // move to action
                                  if (ReservationStateTree.select(ReservationCursors[type + 'Date']).get() !== null) {
                                    ReservationActions.setClosedTimesForLocationAndDate(type);
                                  }
                                })
                                .catch((err) => {
                                  ServiceController.trap('Issue calling getClosedHoursForLocation()', err);
                                })
    } else {
      let mess = 'Missing location parameters on getClosedHoursForLocation.';
      // logger.warn(mess);
      return Promise.resolve(mess);
    }
  },

  /**
   * @function getAgeRangesForLocation
   * @memberOf LocationController
   * @param  {string} peoplesoftIdOrCountryCode id value for search
   * @param  {string} locationType              type such as CITY, BRANCH, AIRPORT
   * @return {void}                           sets values
   * @todo : clean up the then() resolve here
   */
  getAgeRangesForLocation (peoplesoftIdOrCountryCode, locationType) {
    let branchSearch = !LocationController.isLocationTypeCityOrCountry(locationType);
    LocationSearchService.fetchAgeRangesForLocation(peoplesoftIdOrCountryCode, branchSearch)
                        .then((res) => {
                          ReservationActions.setAgeRange(res);
                          let selected = res.filter((age) => {
                            return age.selected
                          });
                          let lastAge = res[res.length - 1];
                          let age = ReservationStateTree.select(ReservationCursors.reservationSession).get('renter_age');
                          if (!age) {
                            age = ReservationStateTree.select(ReservationCursors.reservationSession).select('reservationsInitiateRequest').get('renter_age');
                          }

                          if (age && lastAge) {
                            //Double check if the selected age is in range of the new age ranges from EHI
                            //Want to maintain selected age if we can
                            let selectedInRange = res.filter((ageRangeAge) => {
                              return (age == ageRangeAge.value); //Either choose exact match or find the next highest
                            });

                            if (!selectedInRange.length) {
                              for (let i = 0; i < res.length; i++) {
                                let ageRangeAge = res[i];
                                if (ageRangeAge.value > age) {
                                  selectedInRange.push(res[i - 1]);
                                  break;
                                }
                              }
                              if (!selectedInRange.length && age > lastAge.value) {
                                selectedInRange.push(lastAge);
                              }
                            }
                            if (selectedInRange.length) {
                              ReservationActions.setAge(selectedInRange[0].value);
                              ReservationActions.setAgeLabel(selectedInRange[0].label.replace('#{age_to}', i18n('reservationwidget_0015')).replace('#{age_or_older}', i18n('reservationwidget_0042')));
                            } else {
                              ReservationActions.setAgeLabel(selected[0].label.replace('#{age_to}', i18n('reservationwidget_0015')).replace('#{age_or_older}', i18n('reservationwidget_0042')));
                              ReservationActions.setAge(selected[0].value);
                            }
                          } else if (selected && selected.length) {
                            ReservationActions.setAge(selected[0].value);
                          }
                        })
      .catch((err) => {
        ServiceController.trap('LocationController.getAgeRangesForLocation().catch()', err);
      })
  },

  /**
   * @memberOf LocationController
   * @function getDateTimeValidity
   * @param  {string} type dropoff | pickup (?)
   * @return {*}      open / closed data
   * @todo  Compare with LocationController.getClosedHoursForLocation()
   * @todo : should this use DateTime somethings ?
   */
  getDateTimeValidity (type) {
    // logger.warn('getDateTimeValidity()', type);

    ReservationStateTree.options.autoCommit = false;
    let ret;

    const {
      date,
      time,
      sameLocation,
      location
    } = LocationActions.getLocationDateTimeData(type)

    let data = { // would normally pass this through a factory but it seemed like a bit much ...
      date,
      time,
      location
    }

    if (sameLocation) {
      // logger.log('sameLocation TRUE');
      const {
        date,
        time
      } = LocationActions.getLocationDateTimeData(LOCATION_SEARCH.DROPOFF)

      let dropoffData = {
        date,
        time,
        location
      }

      //@todo this could be reworked
      ret = LocationSearchService.getDateTimeValidity(data, LOCATION_SEARCH.PICKUP)
        .then((res) => DateTimeActions.parseDateTimeValidity(res, type))
        .then(() => LocationSearchService.getDateTimeValidity(dropoffData, LOCATION_SEARCH.DROPOFF)
          .then((res) => DateTimeActions.parseDateTimeValidity(res, LOCATION_SEARCH.DROPOFF)))
        .then(() => {
          ReservationStateTree.commit();
          ReservationStateTree.options.autoCommit = true;
        })
        .catch((err) => {
          ServiceController.trap('LocationController.getDateTimeValidity().catch()', err);
        })
    } else {
      // logger.log('sameLocation FALSE');
      ret = LocationSearchService.getDateTimeValidity(data, type)
        .then((res) => DateTimeActions.parseDateTimeValidity(res, type))
        .then(() => {
          ReservationStateTree.commit();
          ReservationStateTree.options.autoCommit = true;
        })
        .catch((err) => {
          ServiceController.trap('LocationController.getDateTimeValidity().catch()', err);
        })
    }
    return ret;
  },

  /**
   * @memberOf LocationController
   * @function getSelectedLocationNameByType
   * @param  {object} location
   * @return {string} location name based on location type
   */
  getSelectedLocationNameByType(location) {
    let locationName = LocationModel.getLocationName(location);
    if (location.type === LOCATION_SEARCH.STRICT_TYPE.COUNTRY) {
      locationName = locationName+` (${i18n('leadforms_0008').toLowerCase() || 'country'})`
    }
    return locationName
  },

  /**
   * Assign coordinates to a location object model
   * When a location doesn't have coordinates, this will give them to it
   * @memberof   LocationController
   * @param      {string}  type       The type, pickup|dropoff|etc.
   * @param      {number}  lat        The lat
   * @param      {number}  longitude  The longitude
   */
  setLocationPosition (type, lat, longitude) {
    let data = LocationCoordinatesModelFactory(lat, longitude)
    LocationActions.setLocationObjCoordinates(type, data);
  }
}

/**
 * @namespace Deprecated
 * Include below to do copmarison testing (then remove)
 * @type {Object}
 */
// const Deprecated = {
// }

/**
 * @namespace Components
 * @type {Object}
 */
const Components = {
  clearLocations (type) {
    const results = LocationSearchFactory.getLocationSearchModel(null, type);
    LocationActions.saveGetLocations(results);
  }
}

/**
 * @module LocationController
 * @type {Object}
 */
const LocationController = {

  // ...Deprecated,

  ...Rules,
  ...ServicesData,
  ...Components
}

module.exports = LocationController;
