/* globals ReservationStateTree */
/**
 * @module ReservationFlowModelController
 * @description This is used for methods that cross between components and site sections.
 * There's some thought that should go into this, as there probably methods here that maybe belong uniquely in other
 * places such as Location Search
 * @todo : maybe, like BookingWidgetModelController, rename to ReservationFlowController?
 *
 * *******************************************************************************************
 *     PLEASE DO NOT IMPORT BOOKINGWIDGETMODELCONTROLLER !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * *******************************************************************************************
 *
 */
import SessionService from '../services/SessionService';

import AccountActions from '../actions/AccountActions';
import ReservationActions from '../actions/ReservationActions';
import ErrorActions from '../actions/ErrorActions';
import ModifyActions from '../actions/ModifyActions';
import RedirectActions from '../actions/RedirectActions';
import LocationActions from '../actions/LocationActions';
import DeepLinkActions from '../actions/DeepLinkActions';
import LocationSearchActions from '../actions/LocationSearchActions';

import { PRICING, SERVICE_ENDPOINTS, LOCATION_SEARCH } from '../constants';

import ReservationCursors from '../cursors/ReservationCursors';

import LocationFactory from '../factories/LocationFactory';
import LocationModel from '../factories/Location';

import { Personal, LoyaltyBook } from '../classes/User';

import ResInitiateController from './ResInitiateController';
import LocationController from './LocationController';
import LocationSearchController from './LocationSearchController';
import DateTimeController from './DateTimeController';
import ServiceController from './ServiceController';
import CorporateController from './CorporateController';

import DomManager, { DomQueryElement } from '../modules/outerDomManager';

import EnterpriseServices from '../services/EnterpriseServices';

import LocationInputService from '../services/LocationInputService';

import { debug } from '../utilities/util-debug';
const logger = debug({isDebug: false, name: 'ReservationFlowModelController'}).logger;

const TYPES = LOCATION_SEARCH.STRICT_TYPE;

/**
 * @namespace Rules: Business rules in the ReservationFlowModelController
 * @type {Object}
 */
const Rules = {

}

/**
 * @namespace ServiceData: services, actions called by ReservationFlowModelController
 * @type {Object}
 */
const ServiceData = {

  /**
   * @function getFedexLocations
   * Fetch location data from SOLR endpoints
   * Used by FedexLocationInput (and others?)
   * @uses LocationFactory - Uses LocationFactory.toState
   * @uses LocationInputService - Uses LocationInputService.fetchFedexLocations
   * @param {string} query
   * @param {string} type - pickup dropoff etc.
   * @param {string} locationType - branch, city, airport etc.
   * @todo move to LocationController ECR-14192
   */
  getFedexLocations (query, type, locationType) {
    logger.log('query, locationType:', query, locationType);
    LocationInputService.fetchFedexLocations(query, type, locationType)
      .then((responseData) => {
        let factoryInput = {
          service: SERVICE_ENDPOINTS.LOCATION_SEARCH_FEDEX,
          type,
          locationType
        };
        let locationData = LocationFactory.toState(responseData, factoryInput);
        // logger.log('locationData:', locationData);
        let {
          hasResults,
          results
        } = locationData;
        if (!hasResults) {
          // logger.log('no results');
          ReservationActions.setNoResults(type, !hasResults);
        } else {
          // logger.log('fedex data');
          ReservationActions.setNoResults(type, !hasResults);
          LocationSearchActions.saveGetLocations(results);
          // ReservationActions.updateModel(results);
        }
      }).catch((err) => {
        ServiceController.trap('ReservationFlowModelController.getFedexLocations().catch()', err);
      })
  },

  /**
   * @function submit
   * @memberOf ReservationFlowModelController
   * @param  {*} incomingData data to carry forwards
   * @param  {String} source       Source, usually "BookingWidgetModelController" or "ReservationFlowModelController"
   * @return {Promise}             Promise wrapped ajax
   */
  submit(callback = {}, source = 'ReservationFlowModelController') {
    return ResInitiateController.submit(callback, source)
  }
}

/**
 * @namespace Components: Component interactions used by ReservationFlowModelController
 * @type {Object}
 */
const Components = {

  /**
   * @function setCurrentView
   * @memberOf ReservationFlowModelController
   * set current view flag on State tree for overall app
   * @param {string} type currently displayed view
   */
  setCurrentView (type) {
    // LocationSearchActions.setCurrentView(type);

    ReservationStateTree.select(ReservationCursors.currentView).set(type);
  },

  /**
   * @function setErrorsForComponent
   * @memberOf ReservationFlowModelController
   * @param {object} error     error object
   * @param {string} component name used in state tree
   */
  setErrorsForComponent (error, component) {
    ErrorActions.setErrorsForComponent(error, component);
  },

  /**
   * @function clearErrorsForComponent
   * @memberOf ReservationFlowModelController
   * @param  {string} component name
   * @return {void}
   */
  clearErrorsForComponent(component){
    ErrorActions.clearErrorsForComponent(component);
  },

  /**
   * @function setBookingWidgetExpanded
   * @memberOf ReservationFlowModelController
   * @todo: should this be in BookingWidgetModelController
   * @param {boolean} bool true/false
   */
  setBookingWidgetExpanded(bool){
    ReservationActions.setBookingWidgetExpanded(bool);
  },

  /**
   * @function clearDriverInformation
   * @memberOf ReservationFlowModelController
   * @todo : probably a misleading name, but that's what it was called on `DeepLinkErrors`...rename it properly!
   * @return {void}
   */
  clearDriverInformation () {
    AccountActions.setLoyaltyBasicsFromSession(new LoyaltyBook());
    // @todo - Create an Action for this
    ReservationStateTree.select(ReservationCursors.personal).set(new Personal());
  },

  setFedexRequisitionNumber(targetValue){
    ReservationActions.setFedexRequisitionNumber(targetValue);
  },

  setEmployeeNumber(targetValue){
    ReservationActions.setEmployeeNumber(targetValue);
  }
}

/**
 * @namespace Deprecated: test with or without these
 * @type {Object}
 */
const Deprecated = {
  /**
   * @todo : is this really deprecated ?????
   * @function callGoToReservationStep
   * @memberOf ReservationFlowModelController
   * @todo: Use RedirectActions directly, please
   * @deprecated callGoToReservationStep
   * @param  {string} hash step in the resflow to go to
   * @return {void}
   */
  callGoToReservationStep (hash) {
    // logger.warn('called deprecated method callGoToReservationStep(). Please update.', hash);
    RedirectActions.goToReservationStep(hash);
  },

  /**
   * @function setGlobalLocationDetail
   * @memberOf ReservationFlowModelController
   * @see LocationActions.setGlobalLocationDetail
   * @deprecated See LocationActions instead
   * @todo : is this really deprecated ?????
   */
  setGlobalLocationDetail(resultItem) {
    LocationActions.setGlobalLocationDetail(resultItem);
  },

  /**
   * @memberOf ReservationFlowModelController
   * @function getDateTimeValidity
   * @deprecated Use LocationController.getDateTimeValidity() instead
   * @todo : is this really deprecated ?????
   */
  getDateTimeValidity (type) {
    console.warn('**************** Calling Deprecated Method in ReservationFlowModelController.getDateTimeValidity()');
    return LocationController.getDateTimeValidity(type);
  },

  /**
   * @function setDate
   * @memberOf ReservationFlowModelController
   * Reconcile this with the above, setDateFromSession method
   * @todo  ALSO NOTE there's overlap with BookingWidgetModelController.setDate() - uncertain about differences
   * This looks more fleshed out to me, but the other could have slightly different needs
   * @todo ECR-14192 - this overlapped directly with DateTimeController.setDate() so removed extra code, should this be combined?
   * @param date
   * @param type
   */
  setDate (date, type) {
    DateTimeController.setDate(date, type);

    logger.warn('setDate() calling getDateTimeValidity()');
    LocationController.getDateTimeValidity(type)
      .then(() => {
        ReservationActions.changeView(LocationSearchController.getViewFromInvalidType());
      });
  },
  /**
   * @function setLocationForAllFromSession
   * @memberOf ReservationFlowModelController
   * @param location
   * @param {boolean} getDetailLocationData should we fetch location details
   * @todo: @GBOv2 this will likely go away...
   * @see BookingWidgetModelController
   * @see LocationSearchController
   */
  setLocationForAllFromSession(location, getDetailLocationData = true) {
    // logger.log('setLocationForAllFromSession()');
    // logger.log('setLocationForAllFromSession()', location, getDetailLocationData);
    ReservationActions.setLocationForAll(location);
    if (!LocationController.isLocationTypeCityOrCountry(location) && getDetailLocationData) {
      let locationId = LocationModel.getLocationId(location);
      LocationController.getLocationDetails(locationId, LOCATION_SEARCH.PICKUP);
      LocationController.getClosedHoursForLocation(LOCATION_SEARCH.PICKUP).then(ReservationActions.setClosedTimesForLocationAndDate(LOCATION_SEARCH.PICKUP));
      LocationController.getClosedHoursForLocation(LOCATION_SEARCH.DROPOFF).then(ReservationActions.setClosedTimesForLocationAndDate(LOCATION_SEARCH.DROPOFF));
      LocationController.getAgeRangesForLocation(locationId, location);
    }
  },

  /**
   * @function setLocationFromSession
   * @memberOf ReservationFlowModelController
   * @param {object} location
   * @param {string} type
   * @param {json} reservationsInitiateRequest data
   * @param {boolean} getDetailLocationData should we fetch detailed data
   * @todo: @GBOv2 normalize this with LocationSearchController, BookingWidgetModelController etc.
   * Should be able to get the data from session here, and then pass it to a generic handler
   * @see BookingWidgetModelController
   * @see LocationSearchController
   */
  setLocationFromSession(location, type, reservationsInitiateRequest, getDetailLocationData = true) {
    logger.log('setLocationFromSession()', location, type, getDetailLocationData);
    const selectedLocation = ReservationStateTree.select(ReservationCursors.reservationSteps).get(type + 'Location');
    if (!selectedLocation || selectedLocation.locationName === null) {
      ReservationActions.setLocation(location, type);
      logger.log('location from session has a location type?', LocationController.isLocationHasType(location));
      // if (location.locationType !== TYPES.CITY && getDetailLocationData) {
      if (LocationController.isLocationHasType(location) && !LocationController.isLocationTypeCityOrCountry(location) && getDetailLocationData) {
        logger.log('locationType !== "CITY" call getLocationDetails()');
        logger.log('............... calling LocationController.getLocationDetails(location.key, type)', location.key, type);

        let locationId = LocationModel.getLocationId(location);
        LocationController.getLocationDetails(locationId, type);
        LocationController.getClosedHoursForLocation(type, false, reservationsInitiateRequest).then(ReservationActions.setClosedTimesForLocationAndDate(type));
        LocationController.getAgeRangesForLocation(locationId, location);

      } else {
        // logger.log('not getting location details')
      }
    } else {
      // logger.log('not setting location for ', type);
    }
  },

  /**
   * @function setTime
   * @memberOf ReservationFlowModelController
   * used when a location details exists
   * @param type
   * @param fieldName
   * @param time
   */
  setTime (type, fieldName, time) {
    // logger.log('setTime()', type);
    // BookingWidgetModelController.setTime(type, fieldName, time);
    DateTimeController.setTime(type, fieldName, time);

    ReservationStateTree.commit();
    logger.log('setTime() calling getDateTimeValidity()');
    LocationController.getDateTimeValidity(type)
      .then(() => {
        ReservationActions.changeView(LocationSearchController.getViewFromInvalidType());
      });
  },

  /**
   * @function setLocationSelectType
   * @memberOf ReservationFlowModelController
   * Sets the type of location search we are viewing. This is called by ReservationRouter.componentIsReady / onReady
   * @uses ReservationActions.setLocationSearchType there's a ReservationStateTree.data.view.locationSelect
   */
  setLocationSelectType () {
    const locations = LocationActions.getLocationSelectTypes();
    const dropoffType = locations.dropoffType;
    const pickupType = locations.pickupType;
    if (LocationController.isTypeCityOrMyLocation(pickupType)) {
      ReservationActions.setLocationSearchType(LOCATION_SEARCH.PICKUP);
    } else if (LocationController.isTypeCityOrMyLocation(dropoffType)) {
      ReservationActions.setLocationSearchType(LOCATION_SEARCH.DROPOFF);
    } else {
      ReservationActions.setLocationSearchType(LOCATION_SEARCH.PICKUP);
    }
  },

  /**
   * @function setLocationFromSelectedLocationOnMap
   * @memberOf ReservationFlowModelController
   * @param {string} type pickup / dropoff etc.
   * @todo : use an action
   * @todo : use class factory!!
   * @todo : move to LocationController or LocationSearchController
   */
  setLocationFromSelectedLocationOnMap (type) {
    logger.log('setLocationFromSelectedLocationOnMap()');
    logger.warn('THIS SHOULD USE A CLASS FACTORY!!');
    LocationSearchActions.setSelectionActive(false);
    let locationDetails = ReservationStateTree.select(ReservationCursors[type + 'Target']).get().details;
    let location = {
      key: locationDetails.peopleSoftId,
      locationName: locationDetails.locationNameTranslation,
      locationType: TYPES.BRANCH,
      lat: locationDetails.latitude,
      longitude: locationDetails.longitude
    };
    LocationSearchController.setLocation(location, type);
    // ReservationFlowModelController.setLocation(location, type);
  },

  initiateOnLoadSearch: LocationSearchController.initiateOnLoadSearch.bind(LocationSearchController),

  changeDateTime (type) {
    logger.warn('BAD REQUIRE HACK HERE FOR RECURSIVE DEPENDENCY???')
    const LocationSearchController = require('./LocationSearchController');

    ReservationActions.showDatePickerModalOfType(null);
    LocationSearchController.cleanUpScrollOffset('adjust-modal');
    ReservationFlowModelController.setLocationFromSelectedLocationOnMap(type);
  }

} // Deprecated

/**
 * @constructs ReservationFlowModelController
 * @type {Object}
 */
const ReservationFlowModelController = {
  name: 'ReservationFlowModelController',

  ...Deprecated,

  ...Rules,
  ...Components,
  ...ServiceData,

  /**
   * @function setSameLocation
   * @memberOf ReservationFlowModelController
   * @param {boolean} bool
   */
  setSameLocation(bool){
    LocationSearchActions.setSameLocationWithDates(bool);
  },

  /**
   * @function setDateFromSession
   * @memberOf ReservationFlowModelController
   * @param {moment} date object from Moment.js
   * @param {string} type e.g. pickup, dropoff etc.
   * @todo: @gbov2 reconcile this with setDate() below and BookingWidgetModelController setDate()
   * @see BookingWidgetModelController
   */
  setDateFromSession(date, type) {
    ReservationActions.setViewDate(date.clone(), type);
    ReservationActions.setDate(date, type);
  },


  /**
   * @function setTimeFromSession
   * @memberOf ReservationFlowModelController
   * used when location details do not exist
   * @param type
   * @param variable
   * @param time
   * @todo: Reconcile this with ReservationFlowModelController setTime
   * @see BookingWidgetModelController
   * @see setTime
   */
  setTimeFromSession(type, variable, time) {
    ReservationActions.setTime(type, variable, time);
  },

  contractDetailsFromPrepopuldatedCidSession(initReqAdditionalData, sessionAllAdditionalData) {
    logger.log('contractDetailsFromPrepopuldatedCidSession', initReqAdditionalData, sessionAllAdditionalData);
    return CorporateController.getContractDetails(enterprise.prepopulatedCid)
      .then((contractDetails) => {
        logger.log('CorporateController.getContractDetails() called!!');
        ReservationActions.setPrepopulatedCoupon(contractDetails);

        // @todo - move this logic to FedexController!!
        if (enterprise.b2b === 'fedex') {
          if (Array.isArray(contractDetails.additional_information) && enterprise.fedexSequenceIds) {
            let mapping = contractDetails.additional_information.reduce(function(result, source) {
              let sequenceId = '' + source.sequence;
              result[enterprise.fedexSequenceIds[sequenceId]] = source.id;
              return result;
            }, {});
            // todo save CID?
            ReservationActions.setFedexSequenceMapping(mapping);

            // Prepopulate Employee Number Field
            // const session = response.reservationSession;
            const initiateRequestAddInfo = initReqAdditionalData; //_.get(session, 'reservationsInitiateRequest.additional_information');
            const addFields = _.union(sessionAllAdditionalData, initiateRequestAddInfo);
            const employeeNumberPrepop = addFields.find(i => i.id === mapping.employeeId && i.value);
            if (employeeNumberPrepop) {
              ReservationActions.setEmployeeNumber(employeeNumberPrepop.value);
            }
            const costCenterPrepop = addFields.find(i => i.id === mapping.costCenter && i.value);
            if (costCenterPrepop) {
              ReservationActions.setCostCenter(costCenterPrepop.value);
            }
            // same as line 781 - not fedex specific
            // CorporateActions.setContractDetails(res.contract_details);
          } else {
            console.error('Something went wrong loading FEDEX info.');
          }
        } else if (contractDetails.additional_information &&
                   contractDetails.additional_information.length > 0) {
          // same as line 774 - not fedex specific
          // CorporateActions.setContractDetails(res.contract_details);
        }
        return contractDetails;
      });
  },

  getResflowStep () {
    // @todo - move the access to `ReservationStateTree` to an action
    return ReservationStateTree.select(ReservationCursors.componentToRender).get();
  },
  getIsModifiedFlag () {
    let result = false;
    // @todo: this will (maybe?) only apply for AIRPORT
    //  Home City is cancel-rebook, so there's sometimes no existingReservationConfirmationNumber
    //  --> #modify this is not set
    //  --> #confirmed this is set
    let existing = ReservationStateTree.select(ReservationCursors.existingReservationConfirmationNumber).get();
    if (existing && existing !== null) {
      result = true;
    }
    return result;
  },
  /**
   * @function setViewDate
   * @param {date} date date object
   * @param {string} type pickup, dropoff, etc.
   * @todo : reconcile with DateTimeController.setViewDate()
   */
  setViewDate (date, type) {
    const sameLocation = ReservationStateTree.select(ReservationCursors.sameLocation).get();
    const viewType = type;

    if (sameLocation) {
      type = LOCATION_SEARCH.PICKUP;
    }
    const location = ReservationStateTree.select(ReservationCursors[type + 'Target']).get();

    ReservationActions.setViewDate(date, viewType);
    LocationController.getClosedHoursForLocation(viewType, location.details.peopleSoftId);
  },
  getAgeRangeFromSessionAndLocation () {
    const session = ReservationStateTree.get().model.reservationSession;
    if (session && session.pickup_location && session.pickup_location.id) {
      let location = session.pickup_location;
      let locationId = location.id;
      LocationController.getAgeRangesForLocation(locationId, location);
    }
  },
  getLastCompleteStepHash() {
    return ReservationActions.getLastCompleteStepHash();
  },
  clearLocationsForAll () {
    ReservationActions.clearLocationsForAll();
  },
  clearLocation (type) {
    ReservationActions.clearLocation(type);
  },
  clearLocationResults(type) {
    logger.log('clearLocationResults()');
    ReservationActions.clearLocationResults(type);
  },
  clearFedexResults (type) {
    LocationSearchActions.clearFedexResults(type);
  },

  blockLocationsRequest (bool) {
    logger.warn('blockLocationsRequest()');
    LocationInputService.blockLocationsRequest = bool;
  },
  setClearLocationAttributes (type, searchAttribute, locationType, locationFilter, targetDetails, mapTarget) {
    LocationSearchActions.setSearchAttribute(type, searchAttribute);
    LocationSearchActions.setLocationType(type, locationType);
    LocationSearchActions.setLocationFilter(type, locationFilter);
    LocationSearchActions.setMapTargetDetails(type, targetDetails);
    LocationSearchActions.setMapTarget(type, mapTarget);
  },
  setSelectionActive (bool) {
    LocationSearchActions.setSelectionActive(bool);
  },
  setMyLocation (type, lat, lang) {
    LocationSearchActions.setMyLocation(type, lat, lang);
  },
  abortLocationsRequest () {
    LocationInputService.getLocationsRequest.abort();
  },
  setLocationView (type) {
    LocationSearchActions.setLocationView(type);
  },

  determineRateLabel (type, quantity) {
    switch (type) {
      case PRICING.HOURLY:
        return i18n('resflowviewmodifycancel_0004', {'number': quantity + ' - '});
      case PRICING.DAILY:
        return i18n('resflowviewmodifycancel_0005', {'number': quantity + ' - '});
      case PRICING.WEEKLY:
        return i18n('resflowviewmodifycancel_0006', {'number': quantity + ' - '});
      case PRICING.MONTHLY:
        return i18n('resflowviewmodifycancel_0007', {'number': quantity + ' - '});
      default:
        return '';
    }
  },
  getRateType (itemRateType, itemRateFormat) {
    const rateTypeMap = {
      [PRICING.HOURLY]: i18n('resflowcarselect_0071'),
      [PRICING.DAILY]: i18n('resflowcarselect_0072'),
      [PRICING.WEEKLY]: i18n('resflowcarselect_0073'),
      [PRICING.MONTHLY]: i18n('resflowcarselect_0074'),
      [PRICING.RENTAL]: i18n('resflowextras_9008')
    };
    return rateTypeMap[itemRateType] ? rateTypeMap[itemRateType].replace('#{rate}', itemRateFormat) : '';
  },
  setPriceLineItemRowName (item, lowerCase, isReviewPage, pickupCountryCode) {
    const {isSalesTax, getSalesTaxLabel, isAirportFee, getAirportFeeLabel} = ReservationFlowModelController;
    return [item].map(item => isReviewPage && isSalesTax(item) && getSalesTaxLabel(item, pickupCountryCode) || item)
                 .map(item => isReviewPage && isAirportFee(item) && getAirportFeeLabel(item, pickupCountryCode) || item)
                 .map(item => lowerCase ? item.toLowerCase() : item)
                 .pop();
  },
  setPriceLineItemMidCol (midCol, item) {
    let middleCol = midCol && item.rate_amount_view ? ReservationFlowModelController.determineRateLabel(item.rate_type, item.rate_quantity) +
      ReservationFlowModelController.getRateType(item.rate_type, item.rate_amount_view.format) : '';
    if (item.description === 'DISCOUNT') {
      middleCol = '';
    }
    return middleCol;
  },
  setPriceLineItemCost (item) {
    let cost = item.total_amount_view ? item.total_amount_view.format : '0';
    if (item.category === 'INCLUDED' || item.status === 'INCLUDED') {
      cost = i18n('reservationnav_0018');
    }
    return cost;
  },
  isSalesTax (item) {
    const lacSalesTaxLabel = i18n('LAC_legal_taxesandfees_0003') || 'Sales Tax';
    return item.toLowerCase().includes(lacSalesTaxLabel.toLowerCase());
  },
  getSalesTaxLabel (item, pickupCountryCode) {
    return i18n({key:'LAC_legal_taxesandfees_0005', countryCode: pickupCountryCode, replacements: {tax: item}})
  },
  isAirportFee (item) {
    const airportLACLabel = i18n('LAC_legal_taxesandfees_0002') || 'Airport Fee';
    return item.toLowerCase().includes(airportLACLabel.toLowerCase());
  },
  getAirportFeeLabel (item, pickupCountryCode) {
    return i18n({key:'LAC_legal_taxesandfees_0004', countryCode: pickupCountryCode, replacements: {airportFee: item}})
  },
  showInflightModifyModal () {
    ModifyActions.toggleInflightModifyModal(true);
  },
  setDeeplinkModal(state) {
    DeepLinkActions.setModal(state);
  },
  showPolicy (field, state) {
    ReservationActions.showPolicy(field, state);
  },
  showPolicyModal(state) {
    ReservationActions.showPolicyModal(state);
  },
  onClickElement(element, className, method) {
    DomManager.trigger('onClick', element, className, method);
  },
  offClickElement(element, className, method) {
    DomManager.trigger('offClick', element, className, method);
  },
  callReviewBtn() {
    DomManager.trigger('callReviewBtn');
  },
  getQueryElement(element) {
    return DomQueryElement.$getQueryElement(element);
  },
  newReservationFromSession () {
    ReservationActions.setLoadingClassName('loading');
    return EnterpriseServices.GET(SERVICE_ENDPOINTS.NEW_RES_FROM_SESSION, {
      callback: () => {
        // A handler in the EnterpriseServices controller will treat the response, which should
        //   lead to a hash redirection. Since we need the ReservationStateTree to be fully
        //   reinitialized for the new reservation I'm forcing this reload, which only happens after
        //   the forementioned redirection treatment.
        // @todo Create a special marker in the Backend that tells it's a "hard" redirection so we
        //   handle this globally instead of locally.
        ReservationActions.setLoadingClassName('loading');
        window.location.reload();
      }
    });
  },
  reuseRenterInfo () {
    ReservationFlowModelController.redirectToLanding({
      removeUnauthParams: false,
      removeDatetimeAndLocation: true
    });
  },
  reuseTripInfo () {
    const contractAdditionalInfo = ReservationStateTree.select(ReservationCursors.contractAdditionalInfo).get() || [];
    const fedexSequence = contractAdditionalInfo.reduce((result, src) => {
      result[enterprise.fedexSequenceIds[src.sequence]] = src.id;
      return result;
    }, {});
    const infoToKeep = fedexSequence && fedexSequence.costCenter ? [fedexSequence.costCenter] : [];
    ReservationFlowModelController.redirectToLanding({
      removeDatetimeAndLocation: false,
      removeUnauthParams: true,
      removeCID: true,
      additionalInfoToKeep: infoToKeep
    });
  },

  enterNewInfo () {
    ReservationFlowModelController.redirectToLanding({
      removeCID: true,
      removeDatetimeAndLocation: true
    });
  },

  redirectToLanding (options = {}) {
    const loyaltyBrand = ReservationStateTree.select(ReservationCursors.loyaltyBrand).get();
    const isFedexReservation = ReservationStateTree.select(ReservationCursors.isFedexReservation).get();
    let target;
    if (enterprise.lookAndFeelForwardMap) {
      if (loyaltyBrand) {
        target = enterprise.lookAndFeelForwardMap[loyaltyBrand];
      } else if (isFedexReservation) {
        target = enterprise.lookAndFeelForwardMap['fedex.com'];
      }
    }
    SessionService
      .clearSession(options)
      .then(() => {
        if (target) {
          RedirectActions.go(target);
        } else {
          // @todo normally shouldn't fall here, but if needed to then we should check if the home
          //   screen won't be calling again the clearSession() and clean everything up.
          RedirectActions.goHome();
        }
      });
  }
};

module.exports = ReservationFlowModelController;
