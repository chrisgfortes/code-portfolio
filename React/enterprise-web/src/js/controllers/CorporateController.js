/**
 * @module CorporateController
 * For Business, Contract, Promotional deals etc.
 */

import CorporateActions from '../actions/CorporateActions';

import ErrorsController from './ErrorsController';

import { ContractDetailsFactory } from '../factories/Corporate';

import CorporateService from '../services/CorporateService';

// import { LOCATION_SEARCH, PAGEFLOW, PRICING } from '../constants/';
import { PRICING } from '../constants/';
import { debug } from '../utilities/util-debug';
import { hasProtocol } from '../utilities/util-url';

/**
 * @namespace ServiceData
 * @type {Object}
 */
const ServiceData = {
  getContractDetails (contract) {
    return CorporateService.contractDetails(contract)
                           // .then((response) => ContractDetailsFactory.toState(response) ) // fixes, but not nice
                           .then(ContractDetailsFactory.toState.bind(ContractDetailsFactory))
                           // .then(ContractDetailsFactory.toState) // breaks EcomFactory.logger ...
                           .catch(error => {
                             console.error(error);
                             throw error;
                           })
  }
};

/**
 * @namespace Rules
 * @type {Object}
 */
const Rules = {
  isBusinessPricing (purpose) {
    return purpose === PRICING.BUSINESS;
  },
  getSpecialMessage (contractDetails) {
    return _.get(contractDetails, 'special_account_message.text') || '';
  },
  hasContractBenefits (contractDetails) {
    return !!_.get(contractDetails, 'contract_has_additional_benefits');
  },
  doesAcceptBilling (contractDetails) {
    return !!_.get(contractDetails, 'contract_accepts_billing');
  },
  isContractTypeCorporate (contractType) {
    return contractType === 'corporate'; // @todo use constant
  },
  isCodeNotApplicable (codeApplicable) {
    return !codeApplicable;
  },
  isContractTypePromo (contractType) {
    return contractType === 'promotion'; // @todo use constant
  },
  showBillingInfo (purpose, contractDetails) {
    return contractDetails
        && CorporateController.isBusinessPricing(purpose)
        && CorporateController.hasContractBenefits(contractDetails)
        && CorporateController.doesAcceptBilling(contractDetails);
  },
  showCodeBannerRemove (contractData, confirmationNumber) {
    const allowAccountRemoval = contractData && contractData.allow_account_removal_indicator;
    let result = (!confirmationNumber && allowAccountRemoval);
    return result;
  }
}

const Components = {
  setCorporateState(state) {
    CorporateActions.setCorporateState(state);
  },
  setModal(bool) {
    CorporateActions.setModal(bool);
  },
  showModalInState(state) {
    CorporateActions.showModalInState(state);
  },

  // code banner
  /**
   * @function pickContractData
   * Determine which contract data to leverage in the codebanner
   * @memberof  CorporateController
   * @param      {object}  contractDetails      The contract details
   * @param      {object}  deepLinkReservation  The deep link reservation
   * @return     {object}  correct contract details object
   */
  pickContractData (contractDetails, deepLinkReservation) {
    let deeplinkContract = _.get(deepLinkReservation, 'reservation.contract_details');
    deeplinkContract = ContractDetailsFactory.toState(deeplinkContract);
    return contractDetails || deeplinkContract;
  },
  /**
   * Used by Codebanner
   * @function getPromoCode
   * @memberof CorporateController
   * @param      {object}  resInitiateRequest  The resource initiate request
   * @return     {string}  The promo code.
   */
  getPromoCode (resInitiateRequest) {
    return _.get(resInitiateRequest, 'contract_number') || '';
  },
  /**
   * Gets the contract type.
   * @function     getContractType
   * @member           Of CorporateController
   * @param      {object}  contractData  The contract data
   * @return     {string}  The contract type.
   */
  getContractType (contractData) {
    return (contractData && contractData.contract_type || '').toLowerCase();
  },
  /**
   * Gets the custom links. Used by CodeBanner
   * @function   getCustomLinks
   * @memberof   CorporateController
   * @param      {object}  contractData  The contract data
   * @return     {object}  The custom links.
   */
  getCustomLinks (contractData) {
    return (contractData && contractData.custom_links || [])
      .filter(link => hasProtocol(link.url)); // Accepts links starting with 'http:' or 'https:'
  },
  /**
   * Gets the contract image. Used by CodeBanner
   * @function     getContractImage
   * @memberof   CorporateController
   * @param      {object}  contractData  The contract data
   * @return     {object}  The contract image.
   */
  getContractImage (contractData) {
    return contractData && contractData.contract_image;
  },
  /**
   * @function   transformImagePath
   * @memberof   CorporateController
   * @param      {object}  contractImage  The contract image
   * @return     {string}  string with paths for website renderings
   */
  transformImagePath (contractImage) {
    return (contractImage &&
      contractImage.path
        .replace('{quality}', contractImage.supported_qualities[2])
        .replace('{width}', contractImage.supported_widths[8])
    )
  },
  /**
   * @function   selectCodeBannerData
   * @memberof   CorporateController
   * @todo this could ALMOST be a factory model there's so much going on here...
   * @param      {object}  contractDetails      The contract details
   * @param      {object}  deepLinkReservation  The deep link reservation
   * @param      {object}  resInitiateRequest   The resource initiate request
   * @param      {string}  confirmationNumber   The confirmation number
   * @return     {Object}  object with properies used to render the CodeBanner
   */
  selectCodeBannerData (contractDetails, deepLinkReservation, resInitiateRequest, confirmationNumber) {
    const contractData = Components.pickContractData(contractDetails, deepLinkReservation);
    const contractImage = Components.getContractImage(contractData);
    return {
      contractData,
      promoCode: Components.getPromoCode(resInitiateRequest),
      contractType: Components.getContractType(contractData),
      customLinks: Components.getCustomLinks(contractData),
      contractImage,
      contractImagePath: Components.transformImagePath(contractImage),
      showRemove: Rules.showCodeBannerRemove(contractData, confirmationNumber)
    }
  }
  // end code banner
}

const CorporateController = debug({
  isDebug: true,
  logger: {},
  name: 'CorporateController',

  ...ServiceData,
  ...Rules,
  ...Components,

  errorTriggers: ErrorsController.errorTriggers,

  // working to resolve circular dependencies
  setLockedCIDModal: CorporateActions.setLockedCIDModal,
  checkForExistingCorporateCode: CorporateActions.checkForExistingCorporateCode,
  setAdditionalInfoFromArray: CorporateActions.setAdditionalInfoFromArray.bind(CorporateActions),
  matchAdditionalSequences: CorporateActions.matchAdditionalSequences.bind(CorporateActions),
  setError: CorporateActions.setError.bind(CorporateActions)

});

module.exports = CorporateController;
