import RedirectActions from '../actions/RedirectActions';
import ReservationActions from '../actions/ReservationActions';
import ConfirmedActions from '../actions/ConfirmedActions';
import VerificationActions from '../actions/VerificationActions';

import ProfileController from '../controllers/ProfileController';

import SessionService from '../services/SessionService';
import { SimplePersonalFactory } from '../factories/User';

import {PAGEFLOW} from '../constants/';

import { debug } from '../utilities/util-debug';
const logger = debug({isDebug: false, name: 'ConfirmedController'}).logger;

/**
 * @namespace  {object} ServiceData
 * @memberOf ConfirmedController
 * @todo: build this namespace out
 */
const ServiceData = {};
/**
 * @namespace  {object} Rules
 * @memberOf ConfirmedController
 * @todo: build this namespace out
 */
const Rules = {
  showThirdPartyEmail( sessionContractDetails ){
    return sessionContractDetails && sessionContractDetails.third_party_email_notify;
  }
};
/**
 * @namespace  {object} Components
 * @memberOf ConfirmedController
 * @todo: build this namespace out
 */
const Components = {};

const ConfirmedController = {
  ...ServiceData,
  ...Rules,
  ...Components,

  allowModify( eligibility, chargeType ) {
    return eligibility && eligibility.modify_reservation && !(enterprise.hideRedemptionModify && chargeType === 'REDEMPTION')
  },
  viewDifferentReservation( profile ) {
    if (profile) {
      RedirectActions.goDomainRedirect(enterprise.aem.path + '/account.html#reservation', true);
    } else {
      SessionService.clearSession();
      RedirectActions.goDomainRedirect(enterprise.aem.path + '.html', true);
    }
  },
  getConfirmationHeader() {
    return ConfirmedActions.getConfirmationHeader();
  },
  getSummary (){
    return ConfirmedActions.getSummary();
  },
  getDetailDate(pickupTime) {
    return ConfirmedActions.getDetailDate(pickupTime).date;
  },
  getLocationDetails(){
    return ConfirmedActions.getLocationDetails();
  },
  getPersonalInformation(driverInformation, age){
    logger.warn('getPersonalInformation()', driverInformation, age);
    if (!driverInformation || !age) {
      return {};
    }
    return SimplePersonalFactory.toState({ ...driverInformation, age});
  },
  getConfirmationThanks () {
    return ConfirmedActions.getConfirmationThanks();
  },
  getLocationHoursByDay (type, date, format, bool) {
    ConfirmedActions.getLocationHoursByDay(type, date, format, bool);
  },
  // LEFT "FUNCTION" in here for my editor, sorry guys :)
  getRequestInfo : function ( viewCarClassDetailsStatus, equipmentExtras, insuranceExtras,
      componentToRender, sessionPickupLocationWithDetail, thanks ) {

    let vehicleOnRequestHeader;
    let onRequestExtra = [];
    let onRequestVehicle = viewCarClassDetailsStatus && viewCarClassDetailsStatus.indexOf('ON_REQUEST') > -1;

    const contact = enterprise.i18nReservation.resflowconfirmation_0036;
    const phoneNumber = sessionPickupLocationWithDetail.phones.filter((number) => number.phone_type === 'OFFICE');


    if (equipmentExtras) {
      onRequestExtra = equipmentExtras.filter(function (extras) {
        if (extras) {
          return (extras.selected && (extras.allocation.indexOf('ON_REQUEST') > -1));
        }
      });
    }

    if (insuranceExtras) {
      onRequestExtra = onRequestExtra || insuranceExtras.filter(function (extras) {
          if (extras) {
            return (extras.selected && (extras.allocation.indexOf('ON_REQUEST') > -1));
          }
        });
    }

    if ((onRequestVehicle && onRequestExtra.length > 0) && componentToRender !== PAGEFLOW.CANCELLED) {
      thanks.header = enterprise.i18nReservation.resflowconfirmation_0035 || 'We have your reservation details and will call you shortly to confirm your rental selections.';
      vehicleOnRequestHeader =
        (<div>
          <div>
            <h3>{i18n('resflowcarselect_0151')}</h3>
            <p className='vehicle-desc'>{i18n('resflowcarselect_0153')} {i18n('resflowcarselect_0154')}</p>
            <p>{contact.replace('[phone number]', phoneNumber[0].phone_number)}</p>
          </div>
          <div>
            <h3>{i18n('resflowextras_0023')}</h3>
            <p className='vehicle-desc'>{i18n('resflowextras_0024')} {i18n('resflowcarselect_0154')}</p>
            <p>{contact.replace('[phone number]', phoneNumber[0].phone_number)}</p>
          </div>
        </div>);
    }
    else if (onRequestExtra.length > 0 && componentToRender !== PAGEFLOW.CANCELLED) {
      thanks.header = enterprise.i18nReservation.resflowconfirmation_0035 || 'We have your reservation details and will call you shortly to confirm your rental selections.';
      vehicleOnRequestHeader =
        (<div>
          <h3>{i18n('resflowextras_0023')}</h3>
          <p className='vehicle-desc'>{i18n('resflowextras_0024')} {i18n('resflowcarselect_0154')}</p>
          <p>{contact.replace('[phone number]', phoneNumber[0].phone_number)}</p>
        </div>);
    }
    else if (onRequestVehicle && componentToRender !== PAGEFLOW.CANCELLED) {
      thanks.header = enterprise.i18nReservation.resflowconfirmation_0035 || 'We have your reservation details and will call you shortly to confirm your rental selections.';
      vehicleOnRequestHeader =
        (<div>
          <h3>{i18n('resflowcarselect_0151')}</h3>
          <p className='vehicle-desc'>{i18n('resflowcarselect_0153')} {i18n('resflowcarselect_0154')}</p>
          <p>{contact.replace('[phone number]', phoneNumber[0].phone_number)}</p>
        </div>);
    }
    return { vehicleOnRequestHeader, onRequestExtra, onRequestVehicle };
  },
  generateURL ( driverInformation, supportLinks, confirmationNumber ) {
     return supportLinks.prefill_url
      .replace('{confirmationNumber}', confirmationNumber)
      .replace('{firstName}', driverInformation.first_name)
      .replace('{lastName}', driverInformation.last_name)
      .replace('{language}', enterprise.i18nlocale);
  },
  setLoadingClassName (){
    ReservationActions.setLoadingClassName('loading');
  },
  // @todo: move to PaymentModelController
  setAddCardSuccess(bool) {
    VerificationActions.setAddCardSuccess(bool);
  },
  // @todo: move to PaymentModelController
  setRegisterCardSuccess(bool) {
    VerificationActions.setRegisterCardSuccess(bool);
  },
  showBanner (doNotRent, signature, expedited, enroll) {
    return (!_.isEmpty(enroll.profile) || !!doNotRent || !!expedited.dnr || !!signature);
  }
};

module.exports = ConfirmedController;
