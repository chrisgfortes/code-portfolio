/**
 * @module ErrorsController
 * Used to consolidate error codes from the server, etc.
 */
import ErrorActions from '../actions/ErrorActions';
import CorporateActions from '../actions/CorporateActions';

import { RESERVATION } from '../constants';

// import ReservationCursors from '../cursors/ReservationCursors';
// import ReservationStateTree from '../stateTrees/ReservationStateTree';

/**
 * @memberOf ErrorsController
 * @function hasAccountConflict
 * @param  {array}  messages messages array from service
 * @return {Boolean}
 * @see  VerificationController.callLinkAccount()
 */
export function hasAccountConflict (messages) {
  return (messages[0] && messages[0].code === 'CROS_RES_CONTRACT_CONFLICT_EXISTS') ? true : false;
}

const SPECIAL_ERROR_MAP = RESERVATION.SPECIAL_ERROR_MAP;

/**
 * @function errorsTriggers
 * @memberOf ErrorsController
 * @param  {Array}   messages        messages from services
 * @param  {Boolean} isDeepLinkError
 * @param  {Object}  customTriggers  override/import values
 * @return {void}
 */
export function errorTriggers (messages = [], isDeepLinkError = false, customTriggers = {}) {
  if (messages && messages.length) {
    if (!isDeepLinkError) {
      ErrorActions.setErrorsForComponent(messages, 'bookingWidget');
    }
    // console.log('checking CorporateController.errorTriggers() for LocationSearchController', LocationSearchController);
    // @todo: ECR-14192 the location search error condition here is not handled here but in the submit
    // this was changed due to circular dependencies issues
    const defaultTriggers = {
      'CROS_LOGIN_SYSTEM_ERROR'                       : (message) => { CorporateActions.showModalInState('authenticate'); },
      'CROS_RES_PRE_RATE_ADDITIONAL_FIELD_REQUIRED'   : (message) => { CorporateActions.showModalInState('preRate'); },
      'CROS_EMP_ID_MANDATORY_FOR_FEDEX_CONTRACT'      : (message) => { CorporateActions.showModalInState('preRate'); CorporateActions.setError(messages); },
      'CROS_RES_FEDEX_INVALID_EMPLOYEE_ID_FORMAT'     : (message) => { CorporateActions.showModalInState('preRate'); CorporateActions.setError(messages); },
      'CROS_CONTRACT_PIN_REQUIRED'                    : (message) => { CorporateActions.showModalInState('pin'); },
      'CROS_CONTRACT_PIN_INVALID'                     : (message) => { CorporateActions.setError(messages); },
      'CROS_TRAVEL_PURPOSE_NOT_SPECIFIED'             : (message) => { CorporateActions.showModalInState('purpose'); },
      'CROS_RES_INVALID_ADDITIONAL_FIELD'             : (message) => { CorporateActions.setError(messages); },
      // 'CROS_RES_PICKUP_DATE_BEFORE_LEAD_TIME'         : (message) => { CorporateActions.setError(messages); },
      'CROS_BUSINESS_LEISURE_CONTRACT_NOT_ON_PROFILE' : (message) => { CorporateActions.setError(message); CorporateActions.showModalInState('warning'); },
      'CROS_CONTRACT_NOT_ON_PROFILE'                  : (message) => { CorporateActions.setError(message); CorporateActions.showModalInState('warning'); },
      // 'CROS_PREPAY_RESERVATION_OPERATION_NOT_VALID'   : (message) => { LocationSearchController.initiateOnLoadSearch(LOCATION_SEARCH.PICKUP); RedirectActions.goToReservationStep(PAGEFLOW.HASHPICKUP); },
      'CROS_RES_ONE_WAY_NOT_ALLOWED_FOR_CUSTOMER'     : (message) => { CorporateActions.showModalInState('oneWayNotAllowed'); },
      'PRICING_463'                                   : () => {CorporateActions.setModal(false);},
      'default'                                       : (message) => {CorporateActions.clearAdditionalInfo(); CorporateActions.setModal(false); }
    };

    const triggers = _.assign({}, defaultTriggers, customTriggers);

    messages.forEach(message => { (triggers[message.code] || triggers['default'])(message.message); });
  } else {
    CorporateActions.setModal(false);
  }
}

/**
 * @memberOf ErrorsController
 * @function hasDNCCodes
 * @param  {array}  messages messages array from service
 * @return {Boolean}          do we have these codes, or not?
 */
export function hasDNCCodes (messages) {
  return (messages[0] && (messages[0].code === 'RENTAL003442' || messages[0].code === 'RENTAL003443')) ? true : false;
}

/**
 * @memberOf ErrorsController
 * @param  {object} fieldErrors various fields with errors
 * @param  {string} eCode       codes back from CROS
 * @return {object}             returns updated fieldErrors object
 * @see VerificationController.getFieldErrors()
 */
export function getVerificationErrorCodes(fieldErrors, eCode) {
  switch (eCode) {
    case 'CROS_RES_EMPTY_EMAIL_ID':
    case 'CROS_RES_INVALID_EMAIL_ID':
    case 'RNTR00606':
    case 'RNTR00304':
      fieldErrors.email = true;
      break;
    case 'CROS_RES_EMPTY_FIRST_NAME':
    case 'RENTAL000073':
    case 'RNTR00001':
    case 'RNTR00906':
      fieldErrors.firstName = true;
      break;
    case 'CROS_RES_EMPTY_LAST_NAME':
    case 'RENTAL000074':
    case 'RENTAL000083':
    case 'RNTR00003':
    case 'missingLastNameForExpedited':
    case 'RNTR00907':
      fieldErrors.lastName = true;
      break;
    case 'CROS_RES_EMPTY_PHONE_NUMBER':
    case 'CROS_RES_INVALID_PHONE_NUMBER':
    case 'RNTR00200':
    case 'RNTR00203':
      fieldErrors.phoneNumber = true;
      break;
    default:
  }
  return fieldErrors;
}

/**
 * ErrorsController module
 * @module     {Object} ErrorsController
 * @type       {Object}
 */
const ErrorsController = {
  errorTriggers,
  // actually used by ErrorActions but here for info/availability
  specialErrorMap: SPECIAL_ERROR_MAP
}

export default ErrorsController
