import ReservationStateTree from '../stateTrees/ReservationStateTree';
/**
 * NOTE
 * THIS HAS NOT EVEN BEEN TESTED OR MUCH LESS USED SO IT IS AN EXPERIMENTAL IDEA
 * LET THE BUYER BEWARE
 *
 * Use Like:
 *
 * StateController.lock()
 *   .then(() => {
 *     // perform tree select / set / updates
 *   })
 *   .commit() // commit
 *   .trap((e) => {
 *     // error recovery
 *   })
 */
const DEFAULTERR = {};

const StateController = {
  e: null,
  setup () {
    this.e = DEFAULTERR;
  },
  cleanup () {
    this.setup();
  },
  lock () {
    this.setup();
    ReservationStateTree.options.autoCommit = false;
    return this;
  },
  then (execute) {
    try {
      execute();
      return this;
    } catch (e) {
      this.e = e;
      return this;
    }
  },
  commit () {
    // @todo could there be an exception here? no idea...
    ReservationStateTree.commit();
    ReservationStateTree.options.autoCommit = true;
    return this;
  },
  trap (exception) {
    this.cleanup();
    exception(this.e);
    return false;
  }
}

export default StateController;
