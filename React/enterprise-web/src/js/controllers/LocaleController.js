/* global moment */
/**
 * @module LocaleController
 * For date and time manipulation and formatting in a centralized place.
 * @todo: Import and properly handle moment.js locales and formatting
*/
import { Enterprise } from '../modules/Enterprise';

import { Debugger, debug } from '../utilities/util-debug';
const logger = debug({isDebug: false, name: 'LocaleController'}).logger;

let enterprise = Enterprise.global;

/**
 * @function localeSetup
 * This sets up the locale information for the entire site, mostly around
 * date and time formatting using moment.js.
 * @return {void} sets a locale for moment.js based on code we get back from AEM.
 */
function localeSetup() {
  // sometimes we migh get a code back like 'no' for 'nb'. here we say, use 'nb'
  let remappedCountryCodes = {
    'no': 'nb'
  }

  function checkExceptions(langCode) {
    let result;
    if (remappedCountryCodes[langCode]) {
      result = remappedCountryCodes[langCode]
    } else {
      result = langCode;
    }
    return result;
  }

  function checkLocales (langCode) {
    let localesArray = moment.locales();
    let result = false;
    if (localesArray.indexOf(langCode) > -1) {
      result = true;
    }
    return result;
  }

  function parseLocaleCode(langCodes) {
    let resultCode;
    let langCodeWork = langCodes.replace('_', '-'); // swap '_' for '-';
    let hasBaseCode = false;
    let baseLangWork = ''; // used for two digit code test
    let hasBaseLang = false;

    hasBaseCode = checkLocales(langCodeWork);
    if (hasBaseCode) {
      resultCode = langCodeWork;
    } else {
      // 2 digit code test, which most of our locales are
      baseLangWork = langCodeWork.substring(0, 2);
      hasBaseLang = checkLocales(baseLangWork);
      if (hasBaseLang) {
        resultCode = baseLangWork;
      } else {
        // sadly this leaves no room for exceptions that are the full language code (e.g. no-no-ny)
        let exceptionLang = checkExceptions(baseLangWork);
        resultCode = exceptionLang;
      }
    }
    return resultCode;
  }

  Debugger.use('enterprise.log').log('Boot Startup: 01: LocaleController.init(). Run "moment.locale()" and "moment.localeData()" for derived settings.');
  // Everything is based on language
  const language = enterprise && enterprise.language ? enterprise.language : '';
  //Formats are based on domain
  let domainLocale = enterprise && enterprise.i18nlocale ? enterprise.i18nlocale : '';

  let parsedDomainLocale = '';
  if (domainLocale.length == 2) {
    parsedDomainLocale = checkExceptions(domainLocale);
  } else {
    parsedDomainLocale = parseLocaleCode(domainLocale);
  }
  domainLocale = parsedDomainLocale;

  if (language && domainLocale && enterprise.i18nUnits) {
    // Sets document language as per Accessibility Guideline 3.1.1
    document.querySelector('html').setAttribute('lang', language);

    const localeData = moment().locale(domainLocale).localeData();
    const longDateFormat = (
      // fetch longDateFormat items
      ['LT', 'LTS', 'L', 'LL', 'LLL', 'LLLL']
      .reduce((out, item) => {
        out[item] = (
          item === 'LT' ?
          // default to env config
          enterprise.i18nUnits.timeformat :
          localeData.longDateFormat(item)
        );

        return out;
      }, {})
    );

    logger.log('LocaleController:', language, domainLocale, longDateFormat);

    moment.updateLocale(domainLocale, {
      longDateFormat: longDateFormat,
      week: {
        dow: parseInt(enterprise.i18nUnits.dayofweek, 10),
        doy: parseInt(enterprise.i18nUnits.dayofyear, 10)
      }
    });
    logger.log('moment().locale()', moment().locale());
  }
}

/**
 * @function getEnrollmentIssueAuthories
 * @param  {string} countryCode country code
 * @return {string}             string for issuing authority
 */
export function getEnrollmentIssueAuthories(countryCode) {
  let issuingAuthorityLabel = null;
  switch (countryCode) {
    case 'US':
      issuingAuthorityLabel = i18n('eplusenrollment_0057');
      break;
    case 'CA':
      issuingAuthorityLabel = i18n('eplusenrollment_0058');
      break;
    case 'GB':
    case 'IE':
    case 'ES':
    case 'FR':
    case 'DE':
    default :
      issuingAuthorityLabel = i18n('resflowreview_0071');
  }
  return issuingAuthorityLabel;
}

/**
 * @memberOf LocaleController
 * @module LocaleController
 * Handle locale-specific exceptional rules etc.
 * @type {Object}
 */
const LocaleController = {
  init: () => localeSetup(),

  /**
   * Used by Global Gateway redirect code
   * @method     determineDestinationLang
   * @member           Of LocaleController
   * @return     {string}  string language code value
   * @see        RedirectController
   */
  determineDestinationLang(redirectDomain, browserPreferredLang) {
    //Defaults to i18nlocale
    let lang = enterprise.i18nlocale;
    let supportedLangOnDestination = enterprise.languagesByDomain[redirectDomain];

    //If destination lang exists, uses the first one as its the default
    if (supportedLangOnDestination && supportedLangOnDestination.length) {
      lang = supportedLangOnDestination[0];
    }

    //If browser has a preferred lang and it exists as one of our available lang, uses that instead
    if (browserPreferredLang && browserPreferredLang.preferredLanguages && browserPreferredLang.preferredLanguages.length) {
      for (let i = 0; i < browserPreferredLang.preferredLanguages.length; i++) {
        let language = browserPreferredLang.preferredLanguages[i].replace('-', '_').toLowerCase();
        if (_.has(enterprise.i18nGlobalGatewayJson, language)) {
          lang = language;
          break;
        }
      }
    }
    return lang;
  },

  getEnrollmentIssueAuthories: (countryCode) => getEnrollmentIssueAuthories(countryCode),
  /**
   * @function setCADViewCurrencyCodeByAddress
   * @memberOf LocaleController
   * @param {object} pickupLocationObj location object
   * @param {object} data              submit data object
   * rules added as a result of ECR-11805
   */
  setCADViewCurrencyCodeByAddress(pickupLocationObj, data) {
    if (
       enterprise.countryCode === 'CA' || // .ca domains OR
        (enterprise.countryCode === 'US' &&
          (pickupLocationObj.countryCode === 'CA' || //.com domain AND pickup location in canada
           pickupLocationObj.details && pickupLocationObj.details.address && pickupLocationObj.details.address.country_code === 'CA')
        )
      ) {
      data.view_currency_code = 'CAD';
    }
    return data;
  }
};

LocaleController.init();

module.exports = LocaleController;
