/**
 * Module to consolidate payment-centric code out of Account and Verification.
 * Works with PaymentServices.js and PaymentMethodsActions.js
 * As is convention our foo"ModelController" handles more complex logic, routes data, etc. and
 * Serves as an abstraction away from services, models, and views.
 *
 * This consolidation is a work in progress -- and incomplete as of 2016-10-03 / ECR-22
 * Note: There are tickets in Sprint 2 of 1.8 that will cover more of this.
 *
 * Update: 2017 April - much of the above consolidation is still pending
 *
 **/
import PaymentMethodActions from '../actions/PaymentMethodActions';
import AccountActions from '../actions/AccountActions';
import VerificationActions from '../actions/VerificationActions';
import ExpeditedActions from '../actions/ExpeditedActions';
import ErrorActions from '../actions/ErrorActions';
import ReservationActions from '../actions/ReservationActions';
import LoginActions from '../actions/LoginActions';

import PaymentServices from '../services/PaymentServices';
import AccountService from '../services/AccountService';

import ServiceController from './ServiceController';
import ProfileController from './ProfileController';
import SessionController from './SessionController';

import { PROFILE, PRICING } from '../constants'
import { debug } from '../utilities/util-debug';
import { not } from '../utilities/util-predicates';
import Settings from '../settings';

import ReservationStateTree from '../stateTrees/ReservationStateTree';
import ReservationCursors from '../cursors/ReservationCursors';

/************************
 * SERVICE INTERACTIONS *
 ************************/
const ServiceData = {
  initPrepay() {
    //Nullifying all previous records
    if (PaymentMethodActions.getPangui() !== 'PANGUI') {
      PaymentModelController.registerCardStatus(false);
      VerificationActions.clearThreeDS();
    }

    VerificationActions.setAddCardSuccess(false);
    PaymentMethodActions.setPrepayCardRegSuccess();

    return PaymentServices.initPrepay()
                          .then((response) => {
                            if (!response.card_submission_key || !response.payment_context_data) {
                              ErrorActions.setErrorsForComponent(response, 'verification');
                            }
                            return response;
                          });
  },
  /**
   * Some payment processors issue secondary call backs. For FAREOFFICE on the EU side, there's sometimes a 3DS callback
   * and this watching task is needed for doing intermediary steps.
   * This isn't (yet) necessary on PREPAY NA with Pan GUI that I'm aware of.
   * Building the process here to mirror what was done on the Verification set up however, expecting as much, not
   *  to mention that EU FAREOFFICE is going to have to work on the profile page as well.
   * TBD if we will need the same precise set up for EU Add Payment in Profile or not...
   * @TODO: NEW INFO: EU FAREOFFICE WILL NEVER LIVE ON PROFILE PAGE. This listener may be moot.
   */
  initPayWatching () {
    console.debug('PaymentModelController setting listener on registerCardSuccess');
    let cardSuccess = PaymentMethodActions.getRegisterCardSuccess();
  },
  /**
   * Trigger a service call to add the payment method.
   * @returns {*|Promise}
   */
  addPaymentProfile () {
    let payload = PaymentMethodActions.getPayload();
    return PaymentServices.postPaymentProfile(payload)
                          .then((response) => {
                            ErrorActions.clearErrorsForComponent('paymentMethods');
                            PaymentMethodActions.addCardSuccess(true);
                            return response;
                          });
  },
  /**
   * @function switchPayment
   * @memberOf PaymentModelController
   * @return {Promise} Promise wrapped ajax
   */
  switchPayment () {
    let context = 'PaymentModelController.switchPayment()';
    ReservationActions.setLoadingClassName('loading');
    // @todo - This code is a duplicate of `SessionController.callGetSession()`...refactor it to avoid duplicates!
    return PaymentServices.switchPayment()
                          .then( SessionController.formatDataForStateTree )
                          .then( SessionController.sendSessionDataToState )
                          .then( () => ReservationActions.setLoadingClassName(null) )
                          .catch( err => ServiceController.trap(context, err) )
  },
  postPaymentUpdate(close) {
    const payment = PaymentMethodActions.getModifyPayment();
    let payload = {
      payment: {
        payment_method: payment
      }
    };
    // EMA-11514 - Generating first_six and last_four in case of billing code without those. This just makes the BE
    //             happy and _should not_ affect the usage
    if ((!payment.first_six || !payment.last_four)
      && payment.masked_number
      && payment.payment_type === Settings.paymentTypes.businessAccount) {
      payload.payment.payment_method.first_six = payment.masked_number.substr(0, 6);
      payload.payment.payment_method.last_four = payment.masked_number.slice(-4);
    }

    // EMA-11514 - Generating an expiration date if none is set for a billing code
    if (!payment.expiration_date && payment.payment_type === Settings.paymentTypes.businessAccount) {
      payload.payment.payment_method.expiration_date = moment().format('YYYY-MM');
    }
    PaymentServices.postPaymentUpdate(payload)
      .then((response) => {
        ErrorActions.clearErrorsForComponent('paymentMethods');

        if (_.get(response, 'payment_profile.payment_methods')) {
          PaymentMethodActions.updateProfilePaymentMethods(_.get(response, 'payment_profile.payment_methods'));
        }

        if(close) close();
        AccountActions.setAccountEditModal(null);
        PaymentMethodActions.setLoadingState(false);
      });
  },
  getPrepayToken() {
    return PaymentServices.getPrepayToken()
                          .then((response) => {
                            if (!response.card_submission_key || !response.payment_context_data) {
                              ErrorActions.setErrorsForComponent(response, 'paymentMethods');
                            }
                            return response;
                          });
  },
  registerCardStatus (status, callback = () => {}) {
    const session = ReservationActions.getSession();

    if (status && (!session || session.prepayCardRegistrationSuccessful)) {
      callback(false);
      return;
    }

    return PaymentServices.registerCardStatus(status)
                          .then((response) => {
                            if (response.status === 'OK') {
                              callback(true);
                            } else {
                              ErrorActions.setErrorsForComponent(response, 'verification');
                              callback(false);
                            }
                          });
  },
  clearRegisterCardStatus () {
    return PaymentServices.clearRegisterCardStatus()
                          .then((response) => {
                            if (response.status !== 'OK') {
                              ErrorActions.setErrorsForComponent(response, 'verification');
                            }
                          });
  },
  ThreeDSListener () {
    let cardSuccess = ReservationStateTree.select(ReservationCursors.registerCardSuccess);

    cardSuccess.on('update', ()=> {
      if (cardSuccess) {
        console.debug('ThreeDSListener');
        // only in EU is 3ds a thing
        if (PaymentMethodActions.getPangui() !== 'PANGUI') {
          PaymentModelController.ThreeDSCheck();
        }
      }
    });
  },
  ThreeDSCheck () {
    return PaymentServices.ThreeDSCheck()
      .then((response) => {
        if (response.prepay3_dsdata && response.prepay3_dsdata.perform3_ds) {
          VerificationActions.setThreeDS({
            url: response.prepay3_dsdata.acs_url,
            paReq: response.prepay3_dsdata.pa_req
          });
          VerificationActions.setPrepayModal('threeDS');
        } else if (response.prepay3_dsdata && !response.prepay3_dsdata.perform3_ds) {
          VerificationActions.setAddCardSuccess(true);
          PaymentMethodActions.setThreeDSToken();
          PaymentMethodActions.setCardModal();
        }
      });
  }
};


/************************
 * BUSINESS MODEL RULES *
 ************************/
const Rules = {
  isCreditCardMethod (paymentMethod) {
    return paymentMethod.payment_type === enterprise.settings.paymentTypes.creditCard;
  },
  isBusinessAccountMethod (paymentMethod) {
    return paymentMethod.payment_type === enterprise.settings.paymentTypes.businessAccount;
  },
  filterCreditCards (paymentMethods = []) {
    return paymentMethods.filter(PaymentModelController.isCreditCardMethod);
  },
  hasReachedCreditCardsLimit (creditCards = []) {
    return creditCards.length >= enterprise.settings.maxCreditCards;
  },
  canUsePaymentMethods (brand) {
    return !ProfileController.isEmeraldClubMember(brand);
  },
  canSavePaymentMethods (paymentMethods, sessionBrand) {
    return PaymentModelController.canUsePaymentMethods(sessionBrand) && !PaymentModelController.hasReachedCreditCardsLimit(PaymentModelController.filterCreditCards(paymentMethods));
  },
  isPrepay () {
    let chargeType = PaymentMethodActions.getChargeType();
    return chargeType === PRICING.PREPAY;
  },
  isBusinessAccount ( item ) {
    return item.payment_type === PRICING.BUSINESS_ACCOUNT_APPLICANT;
  }
};


/**************************
 * COMPONENT INTERACTIONS *
 **************************/
const Components = {
  maskCC ( last_four, billing ) {
    return billing ? '••••••••'+last_four : '••••••••••••'+last_four;
  },
  /**
   * Remove the event listener on the 'update' event for the registerCardSuccess property on paymentMethods.
   * Cleanup task when the AddPayment component is unmounted.
   */
  stopwatching () {
    let cardSuccess = PaymentMethodActions.getRegisterCardSuccess();
    cardSuccess.off('update');
    let errorState = PaymentMethodActions.getPaymentErrors();
    if (!errorState) {
      PaymentMethodActions.setErrors(null);
    }
  },
  /**
   * Perform a clean start on a set of card status flags.
   */
  reset () {
    PaymentMethodActions.resetCardStatus();
  },
  /**
   * Once we hear back from the third party payment processor, we handle the data we got back.
   * Typically called by default.js --> enterprise.cardSuccess() via the payment processor's POST BACK to our
   * JSP file ./userprofile/POST.jsp
   * @param {object} data
   * @param {string} processorKeyName
   */
  processResponse (data, processorKeyName) {
    // keep original values around for debugging etc.
    enterprise._globals.push({
      name: processorKeyName,
      value: data
    });

    // We get back a string "false" for debitCardIndicator
    if(data.debitCardIndicator !== "false") {
      if(enterprise.currentView === 'reserve') {
        VerificationActions.setPrepayModal('debitCard');
      } else {
        console.log('setting account modal');
        AccountActions.setAccountEditModal('debitCard');
      }

      return;
    }

    PaymentMethodActions.saveProcessorID(data.paymentMediaReferenceIdentifier);
    PaymentMethodActions.saveProcessorResponse(this.formatPanguiResponseData(data));

    const successCallback = (paymentReferenceID) => {
      VerificationActions.setPaymentReferenceID(paymentReferenceID);
      VerificationActions.setPrepayModal(null);
      VerificationActions.setShowPaymentList(true);
    };
    // @todo: ECR-22 We may need to do this only after a secondary callback (?)
    if (!ReservationActions.getProfile()) {
      VerificationActions.setPaymentReferenceID(data.paymentMediaReferenceIdentifier);
      VerificationActions.setAddCardSuccess(true);
      VerificationActions.setPrepayModal(null);
      if (enterprise.currentView === 'reserve') {
        VerificationActions.setReviewPageCardDetails(data);
      }
    } else {
      // Check if we need to save payment for use later
      if (enterprise.currentView === 'reserve') {
        if (PaymentMethodActions.getSavePaymentForLater() === false) {
          VerificationActions.setAddCardSuccess(true);
          successCallback(data.paymentMediaReferenceIdentifier);
          VerificationActions.setReviewPageCardDetails(data);
        } else {
          if (PaymentMethodActions.getIsExpedited() && !LoginActions.isUserLoggedIn()) {
            successCallback(data.paymentMediaReferenceIdentifier);
            ExpeditedActions.set.input('modal', 'ep');
          } else {
            this.registerRoundTrip(data.paymentMediaReferenceIdentifier, successCallback);
          }
        }
      } else {
        this.registerRoundTrip(data.paymentMediaReferenceIdentifier, successCallback);
      }
    }
  },
  // we need a bunch of data from this big old response they send back. some of it must be manipulated
  /**
   * Manipulate the response data before we do something with it.
   * @param {object} data
   * @returns {object}
   */
  formatPanguiResponseData (data) {
    let returnData = this.handleConditions(data); // see comment on method
    returnData = {
      payment_method: {
        'payment_service_context_reference_identifier': '',
        'payment_reference_id': returnData.paymentMediaReferenceIdentifier,
        'payment_type': enterprise.settings.paymentTypes.creditCard,
        'alias': enterprise.settings.paymentTypeMap[returnData.paymentMediaBrandType],
        'use_type': 'Business',
        'expiration_date':  moment(returnData.expirationMonthYearText, 'MMYYYY').format('YYYY-MM')
      }
    };
    return returnData;
  },
  /**
   * Reset modals and everything.
   */
  handleCascadeFailure (mess) {
    PaymentMethodActions.resetCardStatus();
    ErrorActions.setErrorsForComponent(mess, 'paymentMethods');
    AccountActions.setModifyPayment(null);
    AccountActions.setPangui(false);
    AccountActions.setAccountEditModal(null);
  },
  /**
   * Further changes may be needed for handling particular codes coming back from Pan GUI etc.
   * There may even be exceptions or other state information that needs to be handled.
   * @param {object} data
   * @returns {object}
   */
  handleConditions (data) {
    // @todo: add validation for Debit etc.
    return data;
  },
  /**
   * Similar to default.js:prepay() enterprise.threeDSSuccess()
   * The "round trip" is just that we heard back from the third party.
   * After we do, we post that response data to our server as it came back in an IFRAME so that
   * we don't have to deal with the PCI compliance involved...
   */
  registerRoundTrip (paymentReferenceID, callback) {
    PaymentMethodActions.setLoadingState(true);
    this.addPaymentProfile().then((data) => {
      PaymentMethodActions.setLoadingState(false);

      if (callback) {
        callback(paymentReferenceID);
      }

      this.updateProfileByResponse(data);
    });
  },
  /**
   * Once we have some data, save it to the user's profile. How we do that is up to our actions...
   * @param {object} response
   */
  updateProfileByResponse (response) {
    // console.debug('updatePaymentProfile action');
    if (response.payment_methods) {
      PaymentMethodActions.updateProfilePaymentMethods(response.payment_methods);
    }
  },
  /**
   * Get the name displayed on a page for a Credit Card.
   * @param item {object}
   * @param profile {object}
   * @param alt default string content
   * @todo change this to be used on PaymentModelController not used by Payments.js
   * @todo change so this is not using profile in this manner
   */
  getCardLabel (item, profile, alt = i18n('resflowcorporate_0203')) {
    let paymentLabel = '';
    if (item.alias) {
      paymentLabel = item.alias;
    } else if (item.card_type) {
      paymentLabel = item.card_type.replace(/_/g, ' ');
    } else if (profile && profile.basic_profile.customer_details) {
      paymentLabel = profile.basic_profile.customer_details.contract_name;
    } else {
      paymentLabel = alt;
    }
    return paymentLabel;
  },

  /**
   * Trigger a service call to update a payment method
   * @returns {*|Promise}
   * @todo: After ECR-22: Sprint 2
   */
  updatePaymentProfile () {
    // return PaymentServices.postPaymentUpdate();
  },
  setComponentErrors (warningString) {
    ErrorActions.setErrorsForComponent({
      messages: warningString
    }, 'paymentMethods');
  },
  cleanError () {
    PaymentMethodActions.setErrors(null);
  },
  setLoadingState(bool) {
    PaymentMethodActions.setLoadingState(bool);
  },
  getRedirectUrl(response) {
    let redirectUrl = window.location.href.replace(/[^/]*$/, '') + 'success.html';
    // hard coded for testing localhost, etc.
    // this used to map to Google.com for testing purposes, but FareOffice blocks that now based on whitelist rules
    // developers need to remap a remote url such as below to localhost if they're running SSL locally
    // let redirectUrl = 'https://enterprise-xqa1-aem.enterprise.co.uk/content/co-uk/en/success.html';
    // @todo: need to change initPrepay() to use the url returned from the service

    let formId = enterprise.fareOfficeFormId ? enterprise.fareOfficeFormId : '2';
    let baseUrl = enterprise.prepayUrl.path ? enterprise.prepayUrl.path : null;
    let redirect = baseUrl + '?returnUrl=' + encodeURIComponent(redirectUrl)
      + '&cardSubmissionKey=' + response.card_submission_key + '&formId=' + formId;
    return redirect;
  },
  setUpdateExpirationDate(apiDate) {
    AccountActions.setUpdateExpirationDate(apiDate);
  },
  setEditModalNickname(value) {
    AccountActions.setEditModalNickname(value);
  },
  updateExpirationDate (dateParts) {
    let momentDate = moment(dateParts.join('-'), 'YYYY-MM'); //Added Format string otherwise this won't work in IE
    let apiDate = momentDate.format('YYYY-MM');
    PaymentModelController.setUpdateExpirationDate(apiDate);
  },
  getDateParts (modifyPayment) {
    let output;
    if (modifyPayment.expiration_date) {
      let parts = modifyPayment.expiration_date.split('-');
      // in format [year, month]
      output = [parseInt(parts[0], 10), parseInt(parts[1], 10)];
    } else {
      output = [null, null];
    }

    return output;
  },
  resolveUrl () {
    let retVal;
    if (!window.location.origin) {
      retVal = window.location.protocol + '//' + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
    } else {
      retVal = window.location.origin;
    }
    return retVal;
  },
  resolveLang (country = false) {
    let mapping = enterprise.settings.panguiLangMap;
    let lang = enterprise.language;
    let retString = '';
    let langMap = mapping[lang] ? mapping[lang] : mapping.en;
    if (country) {
      retString = langMap.countryCode;
    } else {
      retString = langMap.lang;
    }
    return retString;
  },
  resolveName (profile) {
    let usersName = '';
    if (profile && profile.basic_profile && profile.basic_profile.first_name !== '' && profile.basic_profile.last_name !== '') {
      usersName = profile.basic_profile.first_name + ' ' + profile.basic_profile.last_name;
    }
    return usersName;
  },
  setSavePaymentForLater(value) {
    VerificationActions.setSavePaymentForLater(value)
  },
  removePayment(reference) {
    return AccountService.removePayment(reference)
      .then((response) => {
        if (response.payment_methods) {
          return {message:'success', response: response};
        }
      });
  },
  setRegisterCardSuccess(bool) {
    PaymentMethodActions.setRegisterCardSuccess(bool);
  },
  selectRegisterCardSuccess() {
    return PaymentMethodActions.selectRegisterCardSuccess();
  },
  // moved from VerificationServices
  setPrepayError (messages, component) {
    PaymentMethodActions.setPrepayError(messages, component)
  },
  // moved from VerificationServices
  clearPrepayError () {
    PaymentMethodActions.clearPrepayError();
  },
  setPaymentReferenceID(id) {
    VerificationActions.setPaymentReferenceID(id);
  },
  setCardModifyConfirmationModal(bool) {
    VerificationActions.setCardModifyConfirmationModal(bool);
  },
  getReviewCardDetails(cardData) {
    let cardType = enterprise.settings.paymentTypeMap[cardData.paymentMediaBrandType];
    let cardCC = '************'+cardData.lastFourDigits;
    let cardExpires = moment(cardData.expirationMonthYearText, 'MMYYYY').format('YYYY-MM');
    return {cardType, cardCC, cardExpires};
  },
  setShowPaymentList(bool) {
    VerificationActions.setShowPaymentList(bool);
  },
  paymentMethodsAvailable(brand, isNAPrepayEnabled, profile) {
    return this.canUsePaymentMethods(brand)
      && isNAPrepayEnabled
      && _.get(profile, 'payment_profile.payment_methods.length')
      && ProfileController.getLoyaltyType() !== PROFILE.EMERALD_CLUB; //make sure not to populate EC with CC info
  },
  getFilteredPayments (profile) {
    let payments = {};
    let filteredPayments = (profile && payments && payments.payment_methods) || [];
    if (profile) {
      payments = profile.payment_profile;
    }
    // Make preferred payment appear filteredPayment and alphabetical afterwords
    if (profile && payments.payment_methods && payments.payment_methods.length > 0) {
      // Hide credit cards

      if (enterprise.hideCreditCard) {
        filteredPayments = payments.payment_methods.filter(not(Rules.isCreditCardMethod));
      } else {
        let creditCards = payments.payment_methods.filter(Rules.isCreditCardMethod);
        let billingAccounts = payments.payment_methods.filter(not(Rules.isCreditCardMethod));

        const sortPreferred = (a, b) => (a.preferred ? -1 : 0) - (b.preferred ? -1 : 0);
        const sortExpired = (a, b) => {
          if (moment(a.expiration_date, 'YYYY-DD').isBefore(moment())) {
            //a is expired, so b comes before
            return 1;
          } else if (moment(b.expiration_date, 'YYYY-DD').isBefore(moment())) {
            return -1;
          } else {
            return 0;
          }
        };

        creditCards.sort(sortPreferred);
        billingAccounts.sort(sortPreferred);

        filteredPayments = billingAccounts.concat(creditCards);
        filteredPayments.sort(sortExpired); //we sort all the list, even preferred gets placed at the bottom - ECR-369
      }
    }
    return filteredPayments;
  },};

const PaymentModelController = debug({
  isDebug: false,
  logger: {},
  name: 'PaymentModelController',
  ...ServiceData,
  ...Rules,
  ...Components
});

module.exports = PaymentModelController;
