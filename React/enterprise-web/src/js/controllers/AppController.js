import AppActions from '../actions/AppActions';
import { Enterprise } from '../modules/Enterprise';

import { GLOBAL } from '../constants';

import { debug } from '../utilities/util-debug';
const logger = debug({isDebug: false, name: 'AppController'}).logger;

let enterprise = Enterprise.global;

/**
 * Logic and Flow Control specific to overall application status/config/conditions/views
 * **** PLEASE DO NOT ADD MANY OTHER CONTROLLERS ETC HERE ****
 * **** CONCERNED ABOUT RECURSIVE DEPENDENCIES            ****
 * **** THIS SHOULD BE *USED BY* OTHER DEPENDENCIES NOT   ****
 * **** USE OTHER DEPENDENCIES                            ****
 * @module     {object} AppController
 */

/**
 * @namespace  {Object} AppController.ServiceData
 */
const ServiceData = { }
/**
 * @namespace  {Object} AppController.Rules
 */
const Rules = {
  dataIsRedirect(data) {
    if (data && data.type === GLOBAL.HREF) {
      return true;
    }
    return false;
  }
};

/**
 * @namespace  {Object} AppController.Components
 */
const Components = {
  /**
   * @function   getSetCurrentView
   * @memberof   SessionController
   */
  getSetCurrentView () {
    logger.log('getSetCurrentView()');
    if (enterprise.currentView !== void 0) {
      AppActions.setCurrentView(enterprise.currentView);
    }
  }
}

export default {
  ...ServiceData,
  ...Rules,
  ...Components
}
