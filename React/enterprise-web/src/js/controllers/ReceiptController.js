/**
 * Formerly was ReceiptHelpers inside actions/ReceiptActions.
 * Strictly speaking, this isn't what I would have a "controller" doing but
 * it's certainly no Action file.
 */
import SupportActions from '../actions/SupportActions';
import PriceActions from '../actions/PriceActions';
import PricingController from '../controllers/PricingController';
import ReceiptActions from '../actions/ReceiptActions';
import { debug } from '../utilities/util-debug';

/************************
 * SERVICE INTERACTIONS *
 ************************/
const ServiceData = {}

/************************
 * BUSINESS MODEL RULES *
 ************************/
const Rules = {};

/**************************
 * COMPONENT INTERACTIONS *
 **************************/
const Components = {
  formatReceiptData(receipt) {
    receipt.contactUsPhoneNumber = SupportActions.getSupportPhoneNumberOfType('CONTACT_US').phone_number;
    receipt.formattedPickupDate = ReceiptController.formatDate(receipt.pickup_time);
    receipt.formattedDropoffDate = ReceiptController.formatDate(receipt.return_time);
    receipt.formattedPickupTime = ReceiptController.formatTime(receipt.pickup_time);
    receipt.formattedDropoffTime = ReceiptController.formatTime(receipt.return_time);
    receipt.headerDate = ReceiptController.formatHeaderDate(receipt.pickup_time);
  },
  formatDate(date) {
    return moment(date).format('DD MMM YYYY');
  },
  formatTime(date) {
    return moment(date).format('LT');
  },
  formatHeaderDate(date) {
    return moment(date).format('MMM DD, YYYY');
  },
  formatPrice(price) {
    return PriceActions.formatRatePrice(price);
  },
  formatInformationSections(receipt) {
    receipt.renterInformation = [
      {
        title: enterprise.i18nReservation.receiptsmodal_0017,
        value: [(receipt.customer_first_name + ' ' + receipt.customer_last_name)]
      },
      {
        title: enterprise.i18nReservation.receiptsmodal_0028,
        value: [receipt.membership_number]
      },
      {
        title: enterprise.i18nReservation.receiptsmodal_0018,
        value: [receipt.contract_name ? receipt.contract_name : null]
      },
      {
        title: enterprise.i18nReservation.receiptsmodal_0019,
        value: [
          receipt.customer_address.street_addresses[0],
          receipt.customer_address.city + ', ' +
          (receipt.customer_address.country_subdivision_code ? receipt.customer_address.country_subdivision_code : '') +
          ' ' + receipt.customer_address.postal
        ]
      }
    ];

    receipt.vehicleDetails = [
      {
        title: enterprise.i18nReservation.receiptsmodal_0020,
        value: [receipt.vehicle_details.vehicle_class_driven]
      },
      {
        title: enterprise.i18nReservation.receiptsmodal_0021,
        value: [receipt.vehicle_details.vehicle_class_charged]
      },
      {
        title: enterprise.i18nReservation.receiptsmodal_0022,
        value: [receipt.vehicle_details.make + ' ' + receipt.vehicle_details.model]
      },
      {
        title: enterprise.i18nReservation.receiptsmodal_0023,
        value: [receipt.vehicle_details.license_plate]
      }
    ];

    receipt.distance = [
      {
        title: enterprise.i18nReservation.receiptsmodal_0024,
        value: [receipt.vehicle_details.starting_odometer + ' ' + receipt.vehicle_details.distance_unit]
      },
      {
        title: enterprise.i18nReservation.receiptsmodal_0025,
        value: [receipt.vehicle_details.ending_odometer + ' ' + receipt.vehicle_details.distance_unit]
      },
      {
        title: enterprise.i18nReservation.receiptsmodal_0026,
        value: [receipt.vehicle_details.distance_traveled + ' ' + receipt.vehicle_details.distance_unit]
      }
    ];
  },
  formatVehicleSection(lineItems) {
    return lineItems
      .filter((item) => item.category === 'RNT')
      .map((item) => {
        return {
          title: item.description,
          price: ReceiptController.formatPrice(item),
          total: item.total_amount_view.format
        }
      });
  },
  formatPaymentSection(lineItems) {
    return lineItems
      .map((item) => {
        return {
          title: item.description,
          price: ReceiptController.formatPrice(item),
          total: item.total_amount_view.format
        }
      });
  },
  formatTotalSection(total) {
    ReceiptController.logger.log('formatTotalSection()', total);
    let handledTotal = PricingController.getPastReceiptPrices(total);
    return [{
      title: enterprise.i18nReservation.receiptsmodal_0004,
      total: handledTotal
    }]
  },
  toggleModal(option) {
    ReceiptActions.toggleModal(option);
  },
  setReceipt(receipt) {
    ReceiptController.formatReceiptData(receipt);

    receipt.vehicleSection = ReceiptController.formatVehicleSection(receipt.price_summary.payment_line_items);
    receipt.paymentSection = ReceiptController.formatPaymentSection(receipt.price_summary.payment_line_items);
    receipt.totalSection = ReceiptController.formatTotalSection(receipt.price_summary);
    ReceiptController.formatInformationSections(receipt);

    ReceiptActions.setReceiptDetails(receipt);
    ReceiptActions.setReceiptLoading(false);
  }
}

const ReceiptController = debug({
  isDebug: false,
  logger: {},
  name: 'ReceiptController',
  ...ServiceData,
  ...Rules,
  ...Components
});

export default ReceiptController;
