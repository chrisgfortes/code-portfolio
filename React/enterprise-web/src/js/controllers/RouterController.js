import ReservationActions from '../actions/ReservationActions';

import ReservationRouter from '../classes/ReservationRouter';

import ExpeditedController from './ExpeditedController';
import SessionController from './SessionController';
import CarSelectController from './CarSelectController';
import ReservationFlowModelController from './ReservationFlowModelController';
import ModifyController from './ModifyController';
import LocationSearchController from './LocationSearchController';
import VerificationController from './VerificationController';

import ReservationCursors from '../cursors/ReservationCursors';

import ReservationStateTree from '../stateTrees';

const routeRules = {
  baoTree: ReservationStateTree,
  loadingCursor: ReservationCursors.loadingClass,
  componentToRenderCursor: ReservationCursors.componentToRender,
  defaultRoute: 'book',
  routes: {
    book: {
      onHashChange: function () {
        ExpeditedController.cleanUp()
          .then(() => SessionController.callGetSession(true, 'book'))
          .then(() => ReservationActions.initialDataReady(true));
      },
      waitOn: ReservationCursors.initialDataReady,
      loading: 'loading',
      ready: false,
      component: 'BookingWidget'
    },
    dateTime: {
      onHashChange: function () {
        ExpeditedController.cleanUp()
          .then(() => SessionController.callGetSession(true, 'dateTime', true))
          .then(() => ReservationActions.setHasDataReady('dateTime', true));
      },
      waitOn: ReservationCursors.dateTimeDataReady,
      loading: 'loading',
      ready: false,
      component: 'DateTimeWidget'
    },
    location: {
      onHashChange: function () {
        ExpeditedController.cleanUp()
          .then(() => SessionController.callGetSession(true, 'location'))
          .then(() => LocationSearchController.routeSetup())
          .then(() => ReservationActions.setHasDataReady('locationSelect', true));
      },
      waitOn: ReservationCursors.locationDataReady,
      loading: 'loading',
      ready: false,
      onReady: function () {
        ReservationFlowModelController.setLocationSelectType(); //Used just to get the props correct for locationSearch
      },
      component: 'LocationSearch'
    },
    'location/pickup': {
      onHashChange: function () {
        ExpeditedController.cleanUp()
          .then(() => SessionController.callGetSession(true, 'location/pickup')) //I think the use of deferreds here makes this really clear
          .then(() => ReservationActions.setHasDataReady('locationSelect', true));
      },
      waitOn: ReservationCursors.locationDataReady,
      loading: 'loading',
      ready: false,
      onReady: function () {
        ReservationActions.setLocationSearchType('pickup');
      },
      component: 'PickupLocationSearch'
    },
    'location/dropoff': {
      onHashChange: function () {
        ExpeditedController.cleanUp()
          .then(() => SessionController.callGetSession(true, 'location/dropoff')) //I think the use of deferreds here makes this really clear
          .then(() => ReservationActions.setHasDataReady('locationSelect', true));
      },
      waitOn: ReservationCursors.locationDataReady,
      loading: 'loading',
      ready: false,
      onReady: function () {
        ReservationActions.setLocationSearchType('dropoff');//Used just to get the props correct for locationSearch
      },
      component: 'DropoffLocationSearch'
    },
    cars: {
      onHashChange: function () {
        ExpeditedController.cleanUp()
          .then(() => SessionController.callGetSession(true, 'cars', false))
          .then(() => CarSelectController.retrieveVehicles())
          .then(() => {
            ReservationActions.setHasDataReady('carSelect', true);
            let session = ReservationStateTree.select(ReservationCursors.reservationSession).get();
            let modify = ReservationStateTree.select(ReservationCursors.modify).get();
            if ((modify || session.inflightModification) && session.chargeType === 'REDEMPTION') {
              CarSelectController.whenRedemptionVehicleIsSelected();
            }
          });
      },
      waitOn: ReservationCursors.carSelectDataReady,
      loading: 'loading',
      ready: false,
      component: 'CarSelect'
    },
    extras: {
      onHashChange: function () {
        ExpeditedController.cleanUp()
          .then(() => CarSelectController.whenAvailableUpgradesRetrieved())
          .then(() => SessionController.callGetSession(true, 'extras', false))
          .then(() => ReservationActions.setHasDataReady('extras', true));
      },
      waitOn: ReservationCursors.extrasDataReady,
      loading: 'loading',
      ready: false,
      component: 'Addons'
    },
    commit: {
      onHashChange: function () {
        ExpeditedController.cleanUp()
          .then(() => SessionController.callGetSession(true, 'commit', false))
          .then(() => ReservationFlowModelController.getAgeRangeFromSessionAndLocation())
          .then(() => {
            VerificationController.routeSetup();
            ReservationActions.setHasDataReady('verification', true);
          });
      },
      waitOn: ReservationCursors.verificationDataReady,
      loading: 'loading',
      ready: false,
      component: 'Verification'
    },
    modify: {
      onHashChange: function () {
        ExpeditedController.cleanUp()
          .then(() => SessionController.callGetSession(true))
          .then(() => ModifyController.callGetAvailableFields())
          .then(() => ReservationFlowModelController.getAgeRangeFromSessionAndLocation())
          .then(() => ModifyController.routeSetup())
          .then(() => ReservationActions.initialDataReady(true));
      },
      waitOn: ReservationCursors.initialDataReady,
      loading: 'loading',
      ready: false,
      component: 'Modify'
    },
    confirmed: {
      onHashChange: function () {
        ExpeditedController.cleanUp()
          .then(() => SessionController.callGetSession(true))
          .then(() => ReservationFlowModelController.getAgeRangeFromSessionAndLocation())
          .then(() => ReservationActions.initialDataReady(true));
      },
      waitOn: ReservationCursors.initialDataReady,
      loading: 'loading',
      ready: false,
      component: 'Confirmed'
    },
    modified: {
      onHashChange: function () {
        ExpeditedController.cleanUp()
          .then(() => SessionController.callGetSession(true))
          .then(() => ReservationFlowModelController.getAgeRangeFromSessionAndLocation())
          .then(() => ReservationActions.initialDataReady(true));
      },
      waitOn: ReservationCursors.initialDataReady,
      loading: 'loading',
      ready: false,
      component: 'Modified'
    },
    details: {
      onHashChange: function () {
        ExpeditedController.cleanUp()
          .then(() => SessionController.callGetSession(true))
          .then(() => ReservationFlowModelController.getAgeRangeFromSessionAndLocation())
          .then(() => ReservationActions.initialDataReady(true));
      },
      waitOn: ReservationCursors.initialDataReady,
      loading: 'loading',
      ready: false,
      component: 'Details'
    },
    cancelled: {
      onHashChange: function () {
        ExpeditedController.cleanUp()
          .then(() => SessionController.callGetSession(true))
          .then(() => ReservationActions.initialDataReady(true));
      },
      waitOn: ReservationCursors.initialDataReady,
      loading: 'loading',
      ready: false,
      component: 'Cancelled'
    },
    timedout: {
      onHashChange: function () {
        ExpeditedController.cleanUp()
          .then(() => ReservationActions.initialDataReady(true));
      },
      waitOn: ReservationCursors.initialDataReady,
      loading: 'loading',
      ready: false,
      component: 'TimedOut'
    }
  }
};

export default {
  reservationFlow() {
    const router = new ReservationRouter(routeRules);
    router.init();
    return router;
  }
}

