import ReservationActions from '../actions/ReservationActions';
import ResInitiateActions from '../actions/ResInitiateActions';
import ErrorActions from '../actions/ErrorActions';

import { PARTNERS, PAGEFLOW, LOCATION_SEARCH } from '../constants';

import ReservationCursors from '../cursors/ReservationCursors';

import CorporateController from './CorporateController';
import LocaleController from './LocaleController';
import LocationController from './LocationController';
import { MessageLogger } from './MessageLogController';
import ServiceController from './ServiceController';

import LocationModel from '../factories/Location';
import ResInitRequestFactory from '../factories/ResInitRequestFactory';

import BookingWidgetService from '../services/BookingWidgetService';

import { Enterprise } from '../modules/Enterprise';

import Cookie from '../utilities/util-cookies';
import { debug } from '../utilities/util-debug';
const logger = debug({isDebug: false, name: 'ResInitiateController'}).logger;
let enterprise = Enterprise.global;

/**
 * @namespace Rules
 * @memberOf ResInitiateController
 * @type {object}
 */
const Rules = {
  isInsideResFlow () {
    return (enterprise.currentView === PAGEFLOW.RESERVATION_FLOW) ? true : false;
  },
  hasInitCid () {
    return ResInitiateActions.getCid();
  }
}

/**
 * @namespace ServiceData
 * @memberOf ResInitiateController
 * @type {Object}
 */
const ServiceData = {
  /**
   * @memberOf ResInitiateController
   * @function submit
   * @param  {*} incomingData misc data parsed here
   * @param {string} source Source controller for some exception behaviors
   * @return {void}
   */
  submit(incomingData, source = '') {
    const isBookingInit = (source === 'BookingWidgetModelController') ? true : false;
    // components to set errors on
    const BOOKING = PAGEFLOW.COMPONENT_BOOKING;
    const LOCATION = PAGEFLOW.COMPONENT_LOCATIONSEARCH;

    if (incomingData && incomingData.loyaltyBrand) {
      ReservationActions.setLoyaltyBrand(incomingData.loyaltyBrand);
    }

    logger.warn('submit()');

    let submitData = ResInitiateActions.getInitData();
    const {
      contractDetails,
      costCenterValue,
      employeeNumber,
      dropoffDate,
      dropoffLocationObj,
      pickupDate,
      pickupLocationObj,
      queryStringData,
      sameLocation,
      fedexSequence
    } = submitData;

    // 01 build some current Additional Information Data
    if (isBookingInit) {
      ResInitiateController.setAdditionalCorporateInfo(employeeNumber, fedexSequence, costCenterValue);
      ResInitiateController.setMatchingAdditionalSequences(employeeNumber, contractDetails, queryStringData);
    }

    // 02 Get the current Additional Information Data
    let additionalInformation = ResInitiateActions.getAdditionalInfo();
    // 03 add to the submitted data set
    submitData.additionalInformation = additionalInformation;

    // ************* POST DATA FORMATTING  *************
    let data = ResInitRequestFactory.toServer(submitData);
    // udpate currency based on addresses and set CAD exceptions
    data = LocaleController.setCADViewCurrencyCodeByAddress(pickupLocationObj, data);

    if (isBookingInit) {
      let doSubmit = true;
      doSubmit = ResInitiateController.bookingStopTraps(sameLocation, pickupLocationObj, dropoffLocationObj, pickupDate, dropoffDate)
      if (!doSubmit) {
        return false;
      }
      ResInitiateController.setBookingCookieData(sameLocation, pickupLocationObj, dropoffLocationObj, pickupDate, dropoffDate);
    }

    ReservationActions.closeNoVehicleAvailabilityModal();
    ReservationActions.setLoadingClassName('loading');
    // this is dumb, but the logic to go back and forth was near impossible
    ErrorActions.clearErrorsForComponent(LOCATION);
    ErrorActions.clearErrorsForComponent(BOOKING);

    // Quickfix for when this function is called with a callback as an argument.
    const callback = (incomingData && typeof incomingData === 'function' ? incomingData : null);
    return BookingWidgetService.submit(data, callback)
      .then((response) => {
        logger.log('calling booking submit promise resolution')
        ReservationActions.setBookingWidgetLoadingState(false);
        ReservationActions.clearCarClassDetails();
        if (response.messages) {

          ReservationActions.setLoadingClassName(null);
          ErrorActions.setErrorsForComponent(response, LOCATION);
          ErrorActions.setErrorsForComponent(response, BOOKING);

          CorporateController.errorTriggers(response.messages, false)
            // CorporateController.errorTriggers(response.messages, false, {
            //   'CROS_PREPAY_RESERVATION_OPERATION_NOT_VALID'   : (message) => { LocationSearchController.initiateOnLoadSearch(LOCATION_SEARCH.PICKUP); RedirectActions.goToReservationStep(PAGEFLOW.HASHPICKUP); },
            // });
          let error = ReservationStateTree.select(ReservationCursors.corporate).get('error');
          if (error) {
            if (response.messages[0].message !== error) {
              CorporateController.setError(null);
            }
          }
        } else {
          CorporateController.setError(null);
          CorporateController.setModal(false);
        }
        if (callback && typeof callback === 'function') {
          callback(response);
        }
        return response;
      })
      .catch((err) => {
        ServiceController.trap('ResInitiateController.submit().catch()', err);
      });
  }
} // ServiceData

const ResInitiateController = {
  ...Rules,
  ...ServiceData,
  /**
   * @function setAdditionalCorporateInfo
   * @memberOf BookingWidgetModelController
   * NOTE: NOT NECESSARY FOR ReservationFlowModelController variation of submit
   * @param {[type]} employeeNumber  [description]
   * @param {[type]} fedexSequence   [description]
   * @param {[type]} costCenterValue [description]
   */
  setAdditionalCorporateInfo(employeeNumber, fedexSequence, costCenterValue) {
    if (employeeNumber) {
      CorporateController.setAdditionalInfoFromArray([{
        id: fedexSequence.employeeId,
        value: employeeNumber
      }, {
        id: fedexSequence.costCenter,
        value: costCenterValue
      }]);
    }
  },

  /**
   * @function setMatchingAdditionalSequences
   * @memberOf BookingWidgetModelController
   * NOTE: NOT NECESSARY FOR ReservationFlowModelController variation of submit
   * @param {string} employeeNumber  employee number
   * @param {object} contractDetails contract details object
   * @param {object} queryStringData query string data
   * @returns {void} sets values
   */
  setMatchingAdditionalSequences(employeeNumber, contractDetails, queryStringData) {
    if (!employeeNumber && contractDetails) {
      if (contractDetails.contract_number === PARTNERS.CONTRACTS.USAA) {
        CorporateController.matchAdditionalSequences(queryStringData, PARTNERS.CONTRACT_SPECIAL_SEQUENCE.USAA, contractDetails.additional_information);
      }
    }
  },

  /**
   * @function bookingStopTraps
   * @memberOf BookingWidgetModelController
   * NOTE: NOT NECESSARY FOR ReservationFlowModelController variation of submit
   * @param  {boolean} sameLocation      same direction or not
   * @param  {object} pickupLocationObj  location object
   * @param  {object} dropoffLocationObj location object
   * @param  {object} pickupDate         pickup date time
   * @param  {object} dropoffDate        dropoff date time
   * @return {boolean}                   if this returns false, we do not submit
   */
  bookingStopTraps(sameLocation, pickupLocationObj, dropoffLocationObj, pickupDate, dropoffDate) {
    if (!pickupLocationObj.myLocation && !dropoffLocationObj.myLocation) {
      // use LocationModel factory tools here
      if ((!pickupLocationObj.locationId && !pickupLocationObj.locationName) ||
         (!sameLocation && (!dropoffLocationObj.locationId && !dropoffLocationObj.locationName))) {
        let error = new MessageLogger();
        error.addMessage(enterprise.settings.frontEndMessageLogCodes.invalidLocation);
        ErrorActions.setErrorsForComponent(error, 'bookingWidget');
        return false;
      } else {
        // do we have a city and a non city?
        if ((LocationController.isLocationTypeCityOrCountry(pickupLocationObj) && !LocationController.isLocationTypeCityOrCountry(dropoffLocationObj)) ||
            (LocationController.isLocationTypeCityOrCountry(dropoffLocationObj) && !LocationController.isLocationTypeCityOrCountry(pickupLocationObj))) {
          if (!dropoffDate) {
            ReservationActions.changeView('dropoffCalendar', false);
            ReservationActions.setInvalidDate(true);
            return false;
          } else if (!pickupDate) {
            ReservationActions.changeView('pickupCalendar', false);
            ReservationActions.setInvalidDate(true);
            return false;
          }
        }
        ReservationActions.setBookingWidgetLoadingState(true);
        return true;
      }
    } else {
      return true;
    }
  },

  setBookingCookieData(sameLocation, pickupLocationObj, dropoffLocationObj) {
    if (sameLocation && enterprise.b2b !== PARTNERS.B2BS.FEDEX) {
      if (LocationModel.getLocationId(pickupLocationObj) && LocationModel.getLocationName(pickupLocationObj)) {
        Cookie.cookieLocation(pickupLocationObj, LOCATION_SEARCH.PICKUP);
      }
    } else {
      if ( LocationModel.getLocationName(pickupLocationObj) && LocationModel.getLocationId(pickupLocationObj) &&
          LocationModel.getLocationId(dropoffLocationObj) && LocationModel.getLocationName(dropoffLocationObj) ) {
        Cookie.cookieLocation(pickupLocationObj, LOCATION_SEARCH.PICKUP);
        Cookie.cookieLocation(dropoffLocationObj, LOCATION_SEARCH.DROPOFF);
      }
    }
  }
}

export default ResInitiateController;

