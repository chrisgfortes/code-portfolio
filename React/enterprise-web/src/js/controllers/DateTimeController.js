
import ReservationActions from '../actions/ReservationActions';
import DateTimeActions from '../actions/DateTimeActions';
import LocationActions from '../actions/LocationActions';
import ReservationCursors from '../cursors/ReservationCursors';
// import BookingWidgetService from '../services/BookingWidgetService';
import LocationController from './LocationController';
// import BookingWidgetModelController from '../controllers/BookingWidgetModelController';
import { LOCATION_SEARCH } from '../constants';
import { debug } from '../utilities/util-debug';

const logger = debug({name: 'DateTimeController', isDebug: false}).logger;

const DateTimeController = {
  /**
   * Reconcile this with ReservationFlowModelController.setDate() - uncertain about differences
   * @memberOf DateTimeController
   * @param date
   * @param type
   * @see ReservationFlowModelController
   */
  setDate (date, type) {
    logger.warn('setDate()', date, type);
    ReservationActions.setDate(date, type);
    //ReservationActions.setTime(type, 'value', null);
    ReservationActions.setClosedTimesForLocationAndDate(type);

    const pickupDate = DateTimeActions.getPickupDate();
    const dropoffDate = DateTimeActions.getDropoffDate();
    const invalidDate = DateTimeActions.getInvalidDate();
    const location = LocationActions.getLocation(type);


    //make sure to set right viewDate for dropoff calendar so always relative to pickup
    if (type === LOCATION_SEARCH.PICKUP) {
      if (pickupDate && !dropoffDate) {
        ReservationActions.setViewDate(date.clone(), LOCATION_SEARCH.DROPOFF);
        // BookingWidgetService.getClosedHoursForLocation(LOCATION_SEARCH.DROPOFF);
        // BookingWidgetModelController.getClosedHoursForLocation(LOCATION_SEARCH.DROPOFF);
        LocationController.getClosedHoursForLocation(LOCATION_SEARCH.DROPOFF);
      }
    }

    //if selected pickup Date is after dropOff Date clear the opossite type that is being set
    if (pickupDate && dropoffDate && pickupDate.isAfter(dropoffDate)) {
      ReservationActions.clearDateTime(type === LOCATION_SEARCH.PICKUP ? LOCATION_SEARCH.DROPOFF : LOCATION_SEARCH.PICKUP);
      logger.log('00');
      ReservationActions.setViewDate(date.clone(), type === LOCATION_SEARCH.PICKUP ? LOCATION_SEARCH.DROPOFF : LOCATION_SEARCH.PICKUP);
      logger.warn('01');
      // logger.warn('!!!change this to BookingWidgetModelController!!!');

      // BookingWidgetService.getClosedHoursForLocation(type === LOCATION_SEARCH.PICKUP ? LOCATION_SEARCH.DROPOFF : LOCATION_SEARCH.PICKUP);
      // BookingWidgetModelController.getClosedHoursForLocation(type === LOCATION_SEARCH.PICKUP ? LOCATION_SEARCH.DROPOFF : LOCATION_SEARCH.PICKUP);
      LocationController.getClosedHoursForLocation(type === LOCATION_SEARCH.PICKUP ? LOCATION_SEARCH.DROPOFF : LOCATION_SEARCH.PICKUP, _.get(location, 'details.peopleSoftId'));

      if (invalidDate) {
        // @todo replace 'dropoff' && 'pickup' with their constants
        ReservationActions.changeView(type === LOCATION_SEARCH.PICKUP ? 'dropoffCalendar' : 'pickupCalendar', false);
      }
    } else {
      ReservationActions.changeView(null, false);
    }

    if (!invalidDate) {
      ReservationActions.changeView(null, false);
    }
  },

  setViewDate (date, type) {
    ReservationActions.setViewDate(date, type);
    // BookingWidgetService.getClosedHoursForLocation(type);
    // BookingWidgetModelController.getClosedHoursForLocation(type);
    LocationController.getClosedHoursForLocation(type);
  },

  /**
   * From BookingWidgetModelController
   * @param {string} type     pickup / dropoff / etc
   * @param {string} fieldName name of field to set
   * @param {[type]} time     [description]
   */
  setTime (type, fieldName, time) {
    ReservationActions.setTime(type, fieldName, time);
    const pickupDate = ReservationStateTree.select(ReservationCursors.pickupDate).get();
    const dropoffDate = ReservationStateTree.select(ReservationCursors.dropoffDate).get();

    if (pickupDate && dropoffDate && dropoffDate.isSame(pickupDate) &&
        Object.is(type, LOCATION_SEARCH.PICKUP)) {
      ReservationActions.setTime(LOCATION_SEARCH.DROPOFF, 'value', null);
    }
    /*
     var nextView = '';
     if (type == LOCATION_SEARCH.DROPOFF && !pickupDate) {
     nextView = 'pickupCalendar';
     } else if (type == LOCATION_SEARCH.DROPOFF && pickupDate) {
     nextView = 'age';
     } else if (type == LOCATION_SEARCH.PICKUP && dropoffDate) {
     nextView = 'age';
     } else if (type == LOCATION_SEARCH.PICKUP && !dropoffDate) {
     nextView = 'dropoffCalendar';
     }
     ReservationActions.changeView(nextView);
     */
  }
}

module.exports = DateTimeController;
