import AccountService from '../services/AccountService';
import ErrorActions from '../actions/ErrorActions';
import AccountActions from '../actions/AccountActions';
import { PARTNERS, PROFILE } from '../constants';
import { debug } from '../utilities/util-debug';

/************************
 * SERVICE INTERACTIONS *
 ************************/
const ServiceData = {
  updateProfilePassword(params) {
    return AccountService.updateProfile.password(params)
      .then((errorResponse) => {
        if (errorResponse.length) {
          ErrorActions.setErrorsForComponent(errorResponse, 'resetPassword');
        }
        return errorResponse;
      });
  },
  updatePassword (params){
    return AccountService.updateProfile.password(params)
      .then((errorResponse) => {
        if (errorResponse.length) {
          ErrorActions.setErrorsForComponent(errorResponse, 'accountPassword');
        } else {
          AccountActions.setAccountEditModal(null);
          AccountService.getSession();
        }
      })
  }
};

/**
 * @namespace  {Rules} ProfileController.Rules
 * @type       {Object}
 */
const Rules = {
  isLoggedIn (session = {}) {
    if (session.loggedIn) {
      return true;
    } else {
      return AccountActions.get.loggedIn();
    }
  },
  isUserDNR () {
    return AccountActions.get.dnrFlag();
  },
  hasCid () {
    return AccountActions.get.basicProfileCid();
  },
  isEnterprisePlusMember (sessionBrand) {
    return sessionBrand === PARTNERS.BRAND.ENTERPRISE_PLUS;
  },
  isEmeraldClubMember (sessionBrand) {
    return sessionBrand === PARTNERS.BRAND.EMERALD_CLUB;
  },
  isSignatureProfile ( profile ) {
    return (profile && profile.basic_profile.loyalty_data.loyalty_program === PROFILE.SIGNATURE);
  },
  isDoNotRentProfile (profile) {
    return (profile && profile.license_profile.do_not_rent_indicator);
  },
  isOptionalDate(licenseDate) {
    return licenseDate === PROFILE.LICENSE_ISSUE_OPTIONAL;
  },
  isMandatoryDate(licenseDate) {
    return licenseDate === PROFILE.LICENSE_ISSUE_MANDATORY;
  }
}

/**************************
 * COMPONENT INTERACTIONS *
 **************************/
const Components = {
  getLoyaltyType() {
    return AccountActions.get.loyaltyType();
  },
  getCidNumber() {
    return AccountActions.get.basicProfileCidNumber();
  },
  setModifyPayment (payment) {
    AccountActions.setModifyPayment(payment);
  },
  setAccountEditModal (type) {
    AccountActions.setAccountEditModal(type);
  },
  setPangui(bool) {
    AccountActions.setPangui(bool);
  },
  enableLicenseIssueDate(expedited) {
    const licenseIssueDate = expedited.countryIssue.license_issue_date;
    const isEqualLicenseIssueWithOptional = ProfileController.isOptionalDate(licenseIssueDate);
    const isEqualLicenseIssueWithMandatory = ProfileController.isMandatoryDate(licenseIssueDate);
    return (expedited.countryIssue && (isEqualLicenseIssueWithOptional || isEqualLicenseIssueWithMandatory));
  },
  enableLicenseExpiryDate(expedited) {
    const licenseExpiryDate = expedited.countryIssue.license_expiry_date;
    const isEqualLicenseExpiryDateWithOptional = ProfileController.isOptionalDate(licenseExpiryDate);
    const isEqualLicenseExpiryDateWithMandatory = ProfileController.isMandatoryDate(licenseExpiryDate);
    return (expedited.countryIssue && (isEqualLicenseExpiryDateWithOptional || isEqualLicenseExpiryDateWithMandatory));
  }
};

const ProfileController = debug({
  isDebug: false,
  logger: {},
  name: 'ProfileController',
  ...ServiceData,
  ...Rules,
  ...Components
});

module.exports = ProfileController;
