/**
 * @module Session Controller
 *
 * @todo: @GBOv2 work in major, serious, dramatic progress ...
 * Main Controller for current-session and other session services.
 *
 * Controllers touch Actions, Services, "Factories",
 * and get touched by Components.
 */
import ModifyActions from '../actions/ModifyActions';
import ReservationActions from '../actions/ReservationActions';
import AccountActions from '../actions/AccountActions';
import CarSelectActions from '../actions/CarSelectActions';
import ExtrasActions from '../actions/ExtrasActions';
import DeepLinkActions from '../actions/DeepLinkActions';
import LoginActions from '../actions/LoginActions';
import MessageActions from '../actions/MessageActions';
import CorporateActions from '../actions/CorporateActions';
import LocationSearchActions from '../actions/LocationSearchActions';
import VerificationActions from '../actions/VerificationActions';
import ExpeditedActions from '../actions/ExpeditedActions';
import KeyRentalFactsActions from '../actions/KeyRentalFactsActions';

// import EcomError from '../classes/EcomError'; // only in the try/catch

import { GLOBAL, PAGEFLOW, SERVICE_ENDPOINTS, PARTNERS, PRICING } from '../constants';

import SessionFactory from '../factories/SessionFactory';
import { LoyaltyBookFactory } from '../factories/User';

import AppController from './AppController';
import FedexController from './FedexController';
import CarSelectController from './CarSelectController';
import ExtrasController from './ExtrasController';
import USAAController from './USAAController';
import DeepLinkController from './DeepLinkController';

import ReservationFlowModelController from './ReservationFlowModelController';
import CancellationController from './CancellationController';
import ServiceController from './ServiceController';

import DomManager, { DomFlags } from '../modules/outerDomManager';
import Analytics from '../modules/Analytics';
import TimeStamp from '../modules/TimeStamp';
import { Enterprise } from '../modules/Enterprise';

import SessionService from '../services/SessionService';

import {debug} from '../utilities/util-debug';
// import { get } from '../utilities/util-object';
import {exists} from '../utilities/util-predicates';

const logger = debug({isDebug: false, name: 'SessionController'}).logger;

let enterprise = Enterprise.global;
// shortcuts
const {
  setLocationForAllFromSession,
  setLocationFromSession,
  setDateFromSession,
  setTimeFromSession,
  getResflowStep
} = ReservationFlowModelController;

let flags = {
  getDetailLocationData: true, // getSession() sets this to be default
  setTree: true, // false per getSession()
  hash: false, // false per getSession()
  inResFlow: false
};

const SessionController = {
  name: 'SessionController',

  /**
   * Pull back for use flags needed for session services.
   */
  getDerivedDataFlags() {
    let flowStep = getResflowStep();
    // debugger;
    return {
      service: SERVICE_ENDPOINTS.SESSION_CURRENT,
      currentStep: flowStep, // this is useless on page load
      getDetailLocationData: flags.getDetailLocationData,
      setTree: flags.setTree,
      hash: flags.hash
    }
  },

  /**
   * STEP 03 send Factory Formatted data to Various Controllers and Actions to set in State Tree
   * @function sendSessionDataToState
   * @memberOf SessionController
   * @param {object} factoryData formatted by SessionController.sendSessionToState (SessionFactory.toState())
   */

  sendSessionDataToState(factoryData) {
    let {
      analyticsFields,
      reservationStatusFlags,
      resInitRequestData,
      additionalInfo,
      corporateData,
      deepLinkData,
      expediteData,
      location,
      carData,
      pricingData,
      messagesData,
      userData, // ECR-14250
      session // @todo one day we will remove this
    } = factoryData;

    logger.log('sendSessionDataToState(factoryData)', factoryData);

    /************** Expose ReservationStatusFlags Data ***************************************************************/
    // Object.assign(Enterprise.global.environment, {reservationStatusFlags});
    SessionController.exposeReservationStatusFlags(reservationStatusFlags);

    // if setTree was in SessionService getSession callback
    if (flags.setTree) {

      if (userData) {
        logger.log('userData.profile:', userData.profile);
        AccountActions.set.profileModel(userData.profile);
      }

      LoginActions.set.loggedInState(reservationStatusFlags.isUserLoggedIn);

      // 00 || 24 Deal With Deeplink MADNESS
      // @todo: @GBOv2: Must be reworked ... please see DeepLinkFactory
      // ALSO ARE TWO OF THESE CURSORS USED HERE UNDEFINED?????
      // updates only if the ReservationStateTree is not set or if we have different errors
      /************** Deep Link Data Bits ****************************************************************************/
      if (deepLinkData && DeepLinkController.shouldSessionSaveDeepLinkData(deepLinkData.deepLinkResMessages)) {
        logger.log('----------------------- running old DEEP LINK code from setStateTreeWithSession()');
        DeepLinkActions.setDeepLinkInfo(deepLinkData.deepLinkInfo);
        DeepLinkActions.setDeepLinkReservation(deepLinkData.deepLinkSource);
        if (deepLinkData.deepLinkDriverInfo) {
          logger.warn('personalFromDriver() — DEEP LINK DATA', deepLinkData.deepLinkDriverInfo);
          ReservationActions.set.personalFromDriver(deepLinkData.deepLinkDriverInfo);
        }
        if (deepLinkData.deepLinkResMessages) {
          DeepLinkActions.setDeepLinkErrors(deepLinkData.deepLinkResMessages);
        }
      } else {
        logger.log('-------------- ELSE NOT RUNNING OLD DEEP LINK CODE from setStateTreeWithSession()');
      }

      // 01 actions from sessioncontroller set state
      /************** ReservationInitiateRequest Data ****************************************************************/
      // if reservationInitiateData exists
      if (reservationStatusFlags.hasReservationInitiate) {
        ReservationActions.setAge(resInitRequestData.renterAge) // 02.1
        // more reservationInitiateRequest data to come
      }

      /************** Location and ReservationInitiateRequest Data ***************************************************/
      if (location && reservationStatusFlags.hasLocationDetailsFromPage && location.locationForAll) { // –-> BRANCH (and region) PAGES enterprise.locationDetail
        // @todo: @GBOV2: this has not solved the problem: we set locations in too many places
        setLocationForAllFromSession(location.locationForAll, flags.getDetailLocationData);
      } else if (!reservationStatusFlags.hasLocationDetailsFromPage && location // --> RESERVATION FLOW
              && reservationStatusFlags.hasReservationInitiate) {
        if (location.pickupLocation) {
          // @todo: @gbov2 i am not happy this passes a whole chunk of data down ...
          setLocationFromSession(location.pickupLocation, 'pickup', resInitRequestData.reservationsInitiateRequest, flags.getDetailLocationData)
        }
        if (location.dropoffLocation) {
          // @todo: @gbov2 i am not happy this passes a whole chunk of data down ...
          setLocationFromSession(location.dropoffLocation, 'dropoff', resInitRequestData.reservationsInitiateRequest, flags.getDetailLocationData);
        }
        // 02
        if (location.pickupLocation && location.initiatePickupLocationDateTime) {
          setDateFromSession(location.initiatePickupLocationDateTime, 'pickup');
          setTimeFromSession('pickup', 'value', location.initiatePickupLocationDateTime);
          ReservationActions.setBookingWidgetExpanded(true);
        }
        if (location.dropoffLocation && location.initiateDropoffLocationDateTime) {
          setDateFromSession(location.initiateDropoffLocationDateTime, 'dropoff');
          setTimeFromSession('dropoff', 'value', location.initiateDropoffLocationDateTime);
        }
        // 05
        if (deepLinkData.deepLinkSource) {
          LocationSearchActions.setSameLocationFromSession(deepLinkData.deepLinkSameLocation);
        } else {
          LocationSearchActions.setSameLocationFromSession(resInitRequestData.sameLocation);
        }
      }

      /************** Selected Car Class Details Data ****************************************************************/
      if (exists(carData) && reservationStatusFlags.hasCarClassSelected) {
        // 03
        ReservationActions.setCarClassDetails(carData.selectedCarClassDetails, reservationStatusFlags.chargeType);
        CorporateActions.setPromoCodeApplicable(
          CarSelectController.isAtPromotionalRate(carData.selectedCarClassDetails) || 
          CarSelectController.isAtContractRate(carData.selectedCarClassDetails));
      } else if (carData && !reservationStatusFlags.hasCarClassSelected) {
        ReservationActions.clearCarClassDetails();
      }
      if (exists(carData)) {
        // 06
        if (resInitRequestData.bookingWidgetSelectedCarClass) {
          CarSelectActions.setPreSelectedCarClass(resInitRequestData.bookingWidgetSelectedCarClass);
        }
        // 07
        if (carData.hasUpgrades) {
          CarSelectActions.setUpgradedStatus(false);
          CarSelectActions.setAvailableUpgrades(carData.upgrades);
        }
        // 21
        if (carData.hasRequiredProtections) {
          ExtrasActions.set.protectionsRequiredAtCounter(carData.requiredProtections.protections,
            carData.requiredProtections.text);
        }
        // 12
        if (carData && exists(carData.extrasCollection)) {
          ExtrasController.extras(carData.extrasCollection, carData.chargeType);
        }
      }

      /************** B2B | Corporate | Contract | Coupon Data *******************************************************/
      // this was moved to outside of setTree due to redundant code in SessionService (28)
      // if (exists(corporateData.contractNumber) && !reservationStatusFlags.prepopulatedCid) {
      //   ReservationActions.setCoupon(corporateData.contractNumber); // 04
      // }
      // 08
      if (exists(additionalInfo) && additionalInfo.initInfo) {
        CorporateActions.setAdditionalInfoFromArray(additionalInfo.initInfo);
      }

      // 18
      if (exists(corporateData)) {
        if(corporateData.contractDetails) {
          logger.log('contract details?', corporateData.contractDetails);
          CorporateActions.setContractDetails(corporateData.contractDetails); // from cros
        } else {
          CorporateActions.removeContractDetails();
        }
      }
      // 22
      if (exists(pricingData) && pricingData.availablePayTypes) {
        CarSelectActions.setAvailablePayTypes(pricingData.availablePayTypes);
      }

      /************** Driver and Profile User Info *******************************************************************/
      /************************ note additional Loyalty fields (a) are set outside of the setTree flag below *********/
      // 11 && 19
      if (reservationStatusFlags.hasDriverInfo) {
        // @todo: break this up please!!!
        // 11 this sets fname, lname, phone number, email, by Object.assign() to state tree
        ReservationActions.set.personalFromDriver(userData.driverData); // sets Personal info compared with driverInformation
        // 19
        ReservationActions.setFromCrosDriverInfo(userData.driverSourceData);
      }

      /************** Legal Terms Data *******************************************************************************/
      // key facts
      // policies
      // terms
      // 15
      if (exists(reservationStatusFlags.licenseeName) && exists(location.pickupBrand)) {
        KeyRentalFactsActions.setKeyFactsBrand(reservationStatusFlags.licenseeName || PARTNERS.ENTERPRISE_LABEL);
      }

      /************** Payment and Price Information ******************************************************************/
      // 16
      if (exists(pricingData)) {
        ReservationActions.setPaymentProcessor(pricingData.paymentProcessor);
        ReservationActions.setCollectNewModifyPaymentCard(pricingData.collectNewCardInModify);
      }
      // 14
      if (reservationStatusFlags.collectNewCardInModify) {
        VerificationActions.setPaymentReferenceID(null);
      }

      /************** Misc Reservation Data **************************************************************************/
      // 09
      if (reservationStatusFlags.hasCommitRequest) {
        CorporateActions.setCommitRequestData(corporateData.commitData);
      }
      // 20
      ModifyActions.toggleModifyFlag(reservationStatusFlags.modify.isModifyInProgress);
      if (!ModifyActions.hasCancelRebook()) {
        ModifyActions.setCancelRebook(reservationStatusFlags.modify.willModifyCancelRebook);
      }
      // 13
      ReservationActions.setBlockModifyPickupLocation(reservationStatusFlags.isPickupModifyBlocked);
      // 13.1
      ReservationActions.setBlockModifyDropoffLocation(reservationStatusFlags.isDropoffModifyBlocked);
      // 17
      if (exists(messagesData) && messagesData.messages) {
        MessageActions.setMessages(messagesData.messages);
      }
      // 23
      if (exists(expediteData)) {
        ReservationActions.setExpediteEligibility(expediteData.expediteEligible);
      }
    } // end if setTree

    /************** Inflight Modify **********************************************************************************/
    /****** Inflight Modify just means that a reservation in progress of being created is beind modified *************/
    if (reservationStatusFlags.modify.isInFlightModify && !ExpeditedActions.get.restartStatus()) {
      ReservationActions.toggleInflightModify(true);
    } else if (!reservationStatusFlags.modify.isInFlightModify) {
      ReservationActions.toggleInflightModify(false)
    }

    /************** Loyalty Data *************************************************************************************/
    /******************* for more, see (a) above *********************************************************************/
    // 27 this needs work, this isn't even referenced; seems we go to profile.basic_profile.loyalty_data or something
    // ECR-14250

    if (exists(userData) && exists(userData.loyalty)) {
      logger.log('setLoyaltyBasicsFromSession()', {
        last_name: userData.loyalty.loyaltyLastName,
        membership_id: userData.loyalty.loyaltyID,
        loyalty_brand: userData.loyalty.loyaltyBrand
      });
      AccountActions.setLoyaltyBasicsFromSession(LoyaltyBookFactory.toState({
        last_name: userData.loyalty.loyaltyLastName,
        membership_id: userData.loyalty.loyaltyID,
        loyalty_brand: userData.loyalty.loyaltyBrand
      }));
    }

    /************** More Corporate Data and Adjustments **************************************************************/
    // 04 + 28 + 35
    if (exists(corporateData)) {
      if (corporateData.contractNumber && (!reservationStatusFlags.prepopulatedCid)) {
        ReservationActions.setCoupon(corporateData.contractNumber);
      }
      // 28
      if (corporateData.globalCid) {
        ReservationActions.setCoupon(corporateData.globalCid);
      }
      // 36.1
      if (corporateData.prepopulatedCid) {
        ReservationActions.setCoupon(corporateData.prepopulatedCid);
      }
      // 29
      if (FedexController.isFedexFlow(corporateData.contractNumber)) {
        DomManager.trigger('fedexPage');
      } else {
        DomManager.trigger('fedexResetDefaults');
      }
    }
    // 30
    if (exists(pricingData) && reservationStatusFlags.chargeType === PRICING.REDEMPTION && pricingData.carRedemptionDays) {
      ReservationActions.setRedemptionDays(pricingData.carRedemptionDays);
    }

    /************** Marketing and Loyalty Flags DO NOT MARKET ********************************************************/
    // 31
    ReservationActions.setMarketingFlag(!reservationStatusFlags.hasMarketingMessage);
    DomManager.trigger('marketingAndLoyalty', !reservationStatusFlags.hasMarketingMessage, reservationStatusFlags.hasLoyaltySignUpEligible);
    if (!reservationStatusFlags.hasMarketingMessage && !enterprise.aem.homePath) {
      DomManager.trigger('resetHeaderBook');
    }

    /************** Pricing: Cancellation Fee Details Set ************************************************************/
    // 32
    if (exists(pricingData) && pricingData.hasCancellationDetails) {
      CancellationController.setCancellationDetails(pricingData.appliedCancellationFees);
    }

    // 33
    // @todo: REMOVE THIS !!!!!!!
    /************** Set Session Catch All Fallback Crutch We Use Cuz We Are Lazy *************************************/
    logger.log('setting reservationSession cursor');
    ReservationActions.setReservationSession(session);

    /************** Expedited Data Carry-Over ************************************************************************/
    // 34 this was always at the end of the getSession() call. But it's just a callback.
    // @todo: Figure out another way to do this because this is lame.
    ExpeditedActions.set.expeditedFields();

      // 36 and friends from initiatePrepopulatedCid
      // @TODO: move this to another Controller method
    if (exists(corporateData) && corporateData.prepopulatedCid) {
      // don't call this directly from here
      ReservationFlowModelController.contractDetailsFromPrepopuldatedCidSession(
        additionalInfo.initInfo,
        additionalInfo.additionalInformationAllData)
          .then((contractDetailData) => {
            logger.log('contractDetails callback from Promise?', contractDetailData);
            // if (ServiceController.validateResponse($xhr)) {
            logger.log('\tuse this corporate data!', contractDetailData);
            // @todo: use a corporate factory here!!!
            CorporateActions.setContractDetails(contractDetailData);
            // } else {
              // logger.error('Issue getting Contract Details in SessionController');
              // ServiceController.serviceFailed('Issue getting Contract Details in SessionController. responseStatus = ' + responseStatus, $xhr);
          })
          .catch((err) => {
            ServiceController.trap('SessionController failed calling ReservationFlowModelController.contractDetailsFromPrepopuldatedCidSession()', err);
          });
    }
    // end 36 callback

    /************** Analytics Callback *******************************************************************************/
    Analytics.ready(analyticsFields, enterprise.analytics);

  },

  /**
   * STEP 02 Get and Format the data we get back from the Session Service response data.
   * @function formatDataForStateTree
   * @memberOf SessionController
   * @param {Object} jsonData
   * @returns {Object} ready for state tree
   */
  formatDataForStateTree (jsonData) {
    logger.warn('01 formatDataForStateTree()', jsonData);
    AppController.getSetCurrentView();

    let derivedData = SessionController.getDerivedDataFlags();

    if (AppController.dataIsRedirect(jsonData)) {
      logger.log('SessionController sees href code and not session data:', jsonData.type, jsonData.code);
      return SessionFactory.getSessionTemplate(jsonData, derivedData);
    } else {
      let factoryData = SessionFactory.toState(jsonData, derivedData);
      logger.warn('SessionFactory extracted data from current-session:', factoryData);
      return factoryData;
    }
  },

  /**
   * STEP 1 passes the request to SessionService.fetchSession() and handles the response.
   * @memberOf SessionController
   * @function callGetSession
   * @interface SessionService
   * @param {boolean} setTree
   * @param {boolean} hash
   * @param {boolean} getDetailLocationData
   * @param {function} callback
   * @returns {*|Object}
   */
  callGetSession(setTree = true, hash = false, getDetailLocationData = true, callback = () => { }) {
    // pull out some arguments that we may need
    flags.getDetailLocationData = getDetailLocationData;
    flags.setTree = setTree;
    flags.hash = hash;

    // logger.log('flags?', flags);
    let context = 'SessionController.callGetSession()';

    return SessionService.fetchSession(setTree, hash, getDetailLocationData, callback)
            .then( TimeStamp.mark('getSession():callback') )
            .then( SessionController.formatDataForStateTree )
            .then( SessionController.sendSessionDataToState )
            .catch( err => ServiceController.trap(context, err) )
  },

  /**
   * @memberOf SessionController
   * @function clearAndGetNewSession
   * In several places we need to trigger an overall refresh, but with options passed
   * @param {Object} options
   */
  clearAndGetNewSession(options) {
    SessionController.clearSession(options)
      .then(() => {
        SessionController.callGetSession();
      });
  },

  /**
   * Called to update session state.
   * @function sendUpdateDataToState
   * @memberOf SessionController
   * @param {object} factoryData
   * this is a hack replacement for setStateTreeWithCros from the old SessionService
   */
  sendUpdateDataToState(factoryData) {
    let {
      reservationStatusFlags,
      pricingData,
      messagesData,
      session
    } = factoryData;
    logger.log('sendUpdateDataToState()', factoryData);
    SessionController.exposeReservationStatusFlags(reservationStatusFlags);
    // stinks we have to keep relying on this
    // should really move where this is ...
    ReservationActions.setReservationSession(session);
    ReservationActions.setCollectNewModifyPaymentCard(pricingData.collectNewCardInModify);
    if (messagesData && messagesData.messages) {
      MessageActions.setMessages(messagesData.messages);
    }
    if (!ModifyActions.hasCancelRebook()) {
      ModifyActions.setCancelRebook(reservationStatusFlags.modify.willModifyCancelRebook);
    }
    ReservationActions.setBlockModifyPickupLocation(reservationStatusFlags.isPickupModifyBlocked);
    ReservationActions.setBlockModifyDropoffLocation(reservationStatusFlags.isDropoffModifyBlocked);
    if (exists(pricingData.availablePayTypes)) {
      CarSelectActions.setAvailablePayTypes(pricingData.availablePayTypes);
    }
    ExpeditedActions.set.expeditedFields();

    return factoryData;
  },

  /**
   * Called to expose status flags to global environment
   * @function exposeReservationStatusFlags
   * @memberOf SessionController
   * @param {object} obj from ReservationStatusFactory
   */
  exposeReservationStatusFlags (obj) {
    ReservationActions.set.reservationStatusFlags(obj);
    Object.assign(Enterprise.global.environment, {reservationStatusFlags : obj});
  },

  /**
   * Updates the session by calling SessionService.updateSession()
   * @memberOf SessionController
   * @function updateSession
   * @param {function} callback fired after XHR promise returned
   * @returns {Promise} XHR promise
   */
  updateSession(callback) {
    let context = 'SessionController.updateSession()';
    return SessionService.fetchUpdated(callback)
      .then((response) => {
        if (response.reservationSession) {
          ReservationActions.setReservationSession(response.reservationSession)
        }

        let derivedData = Object.assign(SessionController.getDerivedDataFlags(), {
          service: SERVICE_ENDPOINTS.SESSION_UPDATE + '/update'
        });

        return SessionFactory.toState(response, derivedData);
      })
      .then(SessionController.sendUpdateDataToState)
      .catch(err => ServiceController.trap(context, err));
  },

  /**
   * Clears the renter's session by calling SessionService.clearSession()
   * @function clearSession
   * @memberOf SessionController
   * @returns {XHR Promise}
   */
  clearSession(...args) {
    logger.log('clearSession()', ...args);
    return SessionService.clearSession(...args);
  },

  /**
   * Determine if the session should refresh based on (mostly) Corporate rules
   * @function sessnNeedesRefresh
   * @memberOf SessionController
   * @param {string} stat
   * @param {string} contractName
   * @returns {boolean}
   */
  sessionNeedsRefresh (stat, contractName, currentStep, hasSelectedCar) {
    let result = false;
    if (stat === GLOBAL.RESERVATION_STATUS_OP && ( // there is a reservation &&
        (FedexController.isFedexLandingPage()) // user is fedex landing
        || (enterprise.currentView === PAGEFLOW.RESERVATION_FLOW && window.location.hash === PAGEFLOW.HASHBOOK) // user on #book
        || (USAAController.isUSAALandingPage(contractName)) // user is usaa landing
      )
    ) {
      logger.log('\t001 sessionNeedsRefresh');
      result = true;
      // @todo figure out why this if statement matters
      // failure to have selected car class?
    } else if ((stat === GLOBAL.RESERVATION_STATUS_OP || GLOBAL.RESERVATION_STATUS_CN)
      && hasSelectedCar
      && DomFlags.$ecLoyaltyPage // ec unauth
      && (currentStep !== PAGEFLOW.CONFIRMED
      && currentStep !== PAGEFLOW.CANCELLED)
    ) {
      logger.log('\t002 sessionNeedsRefresh');
      result = true;
    }
    logger.log('sessionNeedsRefresh:', stat, contractName, currentStep, hasSelectedCar);
    logger.log('\tRESULT:', result);
    return result;
  },

  /**
   * Sets SessionService flag
   * @function setResFlow
   * @memberOf SessionController
   * @param {bool} tf - sets flag true or false
   * @todo: @GBOv2: this is set in reservation.js and needs to be done another way.
   */
  setResFlow(tf) {
    SessionService.inResFlow = tf;
    flags.inResFlow = tf;
  }

};

export default SessionController;

