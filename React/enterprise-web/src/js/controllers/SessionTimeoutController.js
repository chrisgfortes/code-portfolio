/* globals ReservationStateTree */
import ReservationCursors from '../cursors/ReservationCursors';

let SessionTimeoutController = {
  UPDATE_DELAY: 1000,

  init (sessionTimeout) {

    // Initialize FE session control. Since the final countdown modal is defaulted to count
    //  to 5 minutes, we reduce it from the total session timeout.
    // sessionTimeout = 350; // testing only!!
    // console.warn('^^^^^^^^^ *********** using sessionTimeout: ', sessionTimeout, '*********** ^^^^^^^^^');
    let timeoutToModal = sessionTimeout - 5 * 60;
    ReservationStateTree
      .select(ReservationCursors.sessionTimeoutDuration)
      .set(timeoutToModal >= 0 ? timeoutToModal : 0);

    if (this.timer) {
      clearInterval(this.timer);
    }

    this.sessionTimeoutSecs = ReservationStateTree
      .select(ReservationCursors.sessionTimeoutDuration)
      .get();

    if (this.sessionTimeoutSecs) {
      // console.log('(re?)intializing session timeout with ' + this.sessionTimeoutSecs + 's');

      this.start = Date.now();
      this.elapsedSecs = 0;
      this.timer = setInterval(this._tick.bind(this), this.UPDATE_DELAY);
    }
  },

  _tick () {
    // console.log('tick: ' + this.elapsedSecs);

    let elapsed = new Date() - this.start;
    this.elapsedSecs = Math.round(elapsed / this.UPDATE_DELAY);
    if (this.elapsedSecs > this.sessionTimeoutSecs) {
      this._timeOut();
    }
  },

  _timeOut () {
    clearInterval(this.timer);

    ReservationStateTree
      .select(ReservationCursors.showTimeoutModal)
      .set(true);
  },

};

module.exports = SessionTimeoutController;
