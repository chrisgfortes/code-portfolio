import ReservationStateTree from '../stateTrees/ReservationStateTree';
import ReservationFlowModelController from './ReservationFlowModelController';
import ReservationCursors from '../cursors/ReservationCursors';

const DecoupledModelController = {
  setDate: function (date, type) {
    let details = ReservationStateTree.select(ReservationCursors[type + 'Target']).get().details;
    ReservationStateTree.select(ReservationCursors[type + 'Decoupled']).set('date', date);
    ReservationFlowModelController.loadDetailsPage(details, type);
  },
  setViewDate: function (date, type) {
    ReservationStateTree.select(ReservationCursors[type + 'Decoupled']).set('viewDate', date);
  }
};


module.exports = DecoupledModelController;
