import ReservationActions from '../actions/ReservationActions';
import ReservationStateTree from '../stateTrees/ReservationStateTree';
import ReservationCursors from '../cursors/ReservationCursors';
/**
 * @module DeepLinkController
 * For handling rules around deeplinking data and routing
 * @type {Object}
 * @todo: add Rules, ServiceData, etc.
 */
const DeepLinkController = {

  /**
   * Determines if deep link reservation.
   * NOTE THIS IS FUNDAMENTALLY FLAWED because:
   *  - for v1 deep links we have no obvious properties that indicate it
   *  - for v2 deep links we do get the reservationSession.deep_link_reservation properties
   * @function   isDeepLinkReservation
   * @memberOf  DeepLinkController
   * @todo: add more tests to better identify this condition (may have to set sessionStorage flag on deeplink.html)
   */
  isDeepLinkReservation() {
    const reservationStatusFlags = ReservationActions.get.reservationStatusFlags();
    return _.get(reservationStatusFlags, 'hasDeepLinkReservation');
  },

  /**
   * Used by SessionController.sendSessionDataToState().
   * @function   shouldSessionSaveDeepLinkData
   * @memberof   DeepLinkController
   * @todo: @gbov2: I don't fully understand what this is doing, but it was copied from the old SessionService
   * @todo: change to use actions
   * @param {object} deepLinkMessages
   * @returns {boolean}
   */
  shouldSessionSaveDeepLinkData(deepLinkMessages) {
    let result = false;
    if ((!ReservationStateTree.select(ReservationCursors.deepLinkInfo).get() ||
      !ReservationStateTree.select(ReservationCursors.deepLinkReservation).get()) ||
      (ReservationStateTree.select(ReservationCursors.deepLinkErrors).get() !== deepLinkMessages)
    ) {
      result = true;
    }
    return result;
  }
};

export default DeepLinkController;
