import LoginActions from '../actions/LoginActions';
import AccountActions from '../actions/AccountActions';
import ErrorActions from '../actions/ErrorActions';
import ExpeditedActions from '../actions/ExpeditedActions';
import ChangePasswordActions from '../actions/ChangePasswordActions';
import RedirectActions from '../actions/RedirectActions';
import ReservationActions from '../actions/ReservationActions';
import CorporateActions from '../actions/CorporateActions';

import AccountController from './AccountController';
import ServiceController from './ServiceController';
import CarSelectController from './CarSelectController';
import SessionController from './SessionController';
import ExpeditedController from './ExpeditedController';
import ProfileController from './ProfileController';

import { ProfileFactory, LoginFactory, getLoginBaseModel } from '../factories/User';

import AccountService from '../services/AccountService';

import ReservationCursors from '../cursors/ReservationCursors';
import { debug } from '../utilities/util-debug';
import DomManager from '../modules/outerDomManager';
import { PAGEFLOW } from '../constants';

const LoginController = debug({
  isDebug: false,
  logger: {},
  name: 'LoginController',
  // rules
  whenUserLogsIn () {
    return new Promise((resolve, reject) => {
      if (LoginActions.isUserLoggedIn()) {
        resolve();
      }
      const loginCursor = LoginActions.getLoggedInCursor();
      const loginListener = ({data}) => {
        if (!data.previousData && data.data) {
          resolve();
          loginCursor.off(loginListener);
        }
      };
      loginCursor.on('update', loginListener);
    });
  },
  // service methods
  // ECR-14250 toState / toServer factories
  login (params) {

    LoginActions.setLoginCredentials(getLoginBaseModel(params));
    let data = LoginFactory.toServer(params);

    // ECR-11124
    // eslint-disable-next-line no-eq-null
    if (params.getSession == null) { // yes, this is intended to be ==
      params.getSession = true;
    }

    ErrorActions.clearErrorsForComponent('existingReservations');
    ErrorActions.clearErrorsForComponent('loginWidget');
    ErrorActions.clearErrorsForComponent('changePassword');

    // ECR-14250 FACTORIES! MODELS! OH MY
    return AccountService.login(params.brand, params, data)
      .then( response => {
        LoginController.logger.log('login completed ... ', data, params, response);
        if (response.profile) {
          let profile = ProfileFactory.toState(response.profile);
          LoginController.logger.log('00');

          // LoginActions.set.loggedInState(true); // why bother waiting?

          if (!params.retrieve) {
            LoginController.logger.log('01');
            const session = ReservationActions.getSession();
            ExpeditedActions.set.input('previousChargeType', session.chargeType);
            if (params.getSession) {
              LoginController.logger.log('02');
              // TODO - This call to getSession doesn't seems to be necessary in most login scenarios.
              //        Each `login` call should be analised and `params.getSession` should be set to `false`
              //        whenever this is the case. If we end up noticing this is the case for every scenario,
              //        we can get rid of this call :)
              AccountActions.setProfile(profile); // i think we want this ASAP anyways
              SessionController.callGetSession();
            } else {
              LoginController.logger.log('03');
              AccountActions.setProfile(profile);
            }
          }

          if (profile.basicProfile) {
            LoginController.logger.log('04');
            ExpeditedActions.set.input('modal', false);
            // const session = ReservationActions.getSession();

            LoginController.logger.warn('.......... DNR? ..... ', ProfileController.isUserDNR());

            if (ProfileController.isUserDNR()) {
              ExpeditedActions.set.input('modal', 'dnr');
            }

            if (ExpeditedController.isSessionExpedited()) {
              ExpeditedActions.set.input('render', 'edit');
            }
            if (ExpeditedController.needsExpeditedCIDModal()) {
              ExpeditedActions.set.input('modal', 'cid');
            }
            if (ReservationActions.getCurrentHash() === PAGEFLOW.CARS) {
              ReservationActions.setLoadingClassName('loading');
              CarSelectController.retrieveVehicles().then(() => ReservationActions.setLoadingClassName(''));
            }
            /* Not sure this is ever called
            if (params.retrieve) {
              ExistingReservationService.getExistingReservation(session.confirmationNumber,
                session.driverInformation.first_name,
                session.driverInformation.last_name,
                () => SessionController.callGetSession(response) );
            }*/
          }
          return response;
        } else {
          if (response.messages
            // ECR-14251 Corporate ErrorsController
            && response.messages.some(message => message.code === 'CROS_LOGIN_TERMS_AND_CONDITIONS_ACCEPT_VERSION_MISMATCH')) {
            CorporateActions.setModal(false);
            LoginActions.setLoginModal('terms');
          }
          ErrorActions.setErrorsForComponent(response, 'loginWidget', params);
          if (params.newPassword) {
            ErrorActions.setErrorsForComponent(response, 'changePassword', params);
          }
          return response;
        }
      })
      .catch((err) => {
        LoginController.logger.log('XXXX');
        // return {'data': 'error', 'errors': response}; // @todo ECR-13850 what is this?
        ServiceController.trap('LoginController.login().catch():', err);
      });
  },
  logout () {
    LoginController.setLogoutModal(false);
    ErrorActions.clearErrorsForComponent('loginWidget');
    AccountService.logout()  // ECR-14250
      .then((response) => {
        if (!response.messages) {
          LoginController.logger.log('01');
          //clear out coupon code
          ReservationActions.setCoupon(null);
          LoginActions.setLoginWidgetActive(false);
          if (ReservationActions.getCurrentHash() === 'cars') {
            ReservationActions.setLoadingClassName('loading');
            CarSelectController.retrieveVehicles().then(() => ReservationActions.setLoadingClassName(''));
          }
          LoginActions.set.loggedInState(false);
          AccountController.hashRouter(false); // ECR-14250 found it wasn't always resetting the tabs
          SessionController.callGetSession();
        } else {
          LoginController.logger.log('02', response);
          ErrorActions.setErrorsForComponent(response, 'loginWidget');
        }
      })
      .catch((err) => {
        LoginController.logger.log('XXXX');
        ServiceController.trap('LoginController.logout().catch()', err);
      });
  },
  setAccountTab(tab) {
    AccountActions.setAccountTab(tab);
  },
  setLoginModal(state) {
    LoginActions.setLoginModal(state);
  },
  setLogoutModal (bool) {
    LoginActions.setLogoutModal(bool);
  },
  triggerLoginWidget(profile) {
    $(document).trigger('authenticated', profile);
    setTimeout(function () {
      LoginActions.setLoginWidgetActive(false);
    }, 3000);
  },
  toggleMenu () {
    DomManager.trigger('loginToggleMenu');
  },
  getForgotPassword (supportLinks) {
    window.open(supportLinks ? supportLinks.forgot_password_url : 'https://legacy.enterprise.com/car_rental/enterprisePlusForgotPassword.do');
  },
  getDNRMessage (domain) {
    const phoneMap = {
      "com": i18n('dnr_2005'),
      "ca": i18n('dnr_2006'),
      "uk": i18n('dnr_2007'),
      "gb": i18n('dnr_2007'),
      "ie": i18n('dnr_2008'),
      "fr": i18n('dnr_2010'),
      "es": i18n('dnr_2011'),
      "de": i18n('dnr_2009')
    };
    return i18n('dnr_0002', {phone: phoneMap[domain]});
  },
  getCallbackAfterAuthenticate() {
    return LoginActions.getCallbackAfterAuthenticate();
  },
  setCallbackAfterAuthenticate (data) {
    return LoginActions.setCallbackAfterAuthenticate(data);
  },
  ValidationRules: {
    rules: {
      length: null,
      blacklist: null,
      oneLetter: null,
      oneNumber: null,
      passwordMatch: null
    },
    changePassword (password, confirmPassword) {
      let value = password.value;
      if (value) {
        let blacklist = ['password', 'passwort', 'contraseña', 'mot de passe', 'motdepasse', 'senha', 'wachtwoord', 'palavra-passe'];
        let lowerCaseValue = value.toLowerCase();
        this.rules.blacklist = !blacklist.some((str) => lowerCaseValue.indexOf(str) > -1);
        //this.rules.space = !/\s/g.test(value);
        this.rules.length = value.length > 7;
        this.rules.oneNumber = /.*\d+.*/.test(value);
        this.rules.oneLetter = /.*[a-zA-Z]+.*/.test(value);
        //this.rules.email = !/(.+)@(.+){2,}\.(.+){2,}/.test(value);
        this.rules.passwordMatch = password.value === confirmPassword.value;
      } else {
        for (let rule in this.rules) {
          if (this.rules.hasOwnProperty(rule)) {
            this.rules[rule] = null;
          }
        }
      }
      return this.rules;
    },
    changePasswordConfirm (password, confirmPassword) {
      this.rules.passwordMatch = password.value === confirmPassword.value;
      return this.rules;
    }
  },
  setChangePasswordModal (bool) {
    ChangePasswordActions.toggleModal(bool);
  },
  getSavePaymentForLater () {
    return LoginActions.getSavePaymentForLater();
  },
  redirectToHome(){
    RedirectActions.go(_.get(enterprise, 'aem.path').concat('.html'));
  }
});

module.exports = LoginController;
