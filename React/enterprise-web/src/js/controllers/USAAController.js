
import { PARTNERS, PAGEFLOW } from '../constants';
import { debug } from '../utilities/util-debug';
import { Enterprise } from '../modules/Enterprise';
let enterprise = Enterprise.global;

/**
 * @module USAA Specific Business Rules
 * @type {Object}
 */
const USAAController = debug({
  name: 'USAAController',
  isDebug: true,
  isUSAALandingPage(contractName) {
    return (enterprise.currentView === PAGEFLOW.LANDING_PAGE && PARTNERS.B2BS.USAA === contractName)
  }
});

export default USAAController;
