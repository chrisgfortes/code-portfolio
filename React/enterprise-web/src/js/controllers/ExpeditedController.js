import ExpeditedActions from '../actions/ExpeditedActions';
import ProfileActions from '../actions/ProfileActions';
import ReservationActions from '../actions/ReservationActions';
import ResInitiateActions from '../actions/ResInitiateActions';
import ErrorActions from '../actions/ErrorActions';
import AccountActions from '../actions/AccountActions';

import { EXPEDITED, PROFILE, PAGEFLOW, REQUEST } from '../constants';

import EnrollmentController from './EnrollmentController';
import SessionController from './SessionController';
import ServiceController from './ServiceController';
import ProfileController from './ProfileController';
import PaymentModelController from './PaymentModelController';

import { ExpeditedFactory, ProfileFactory } from '../factories/User';

import DomManager from '../modules/outerDomManager';
import Analytics from '../modules/Analytics';

import ExpeditedService from '../services/ExpeditedService';

import ScrollUtil from '../utilities/util-scroll';
import { debug } from '../utilities/util-debug';
const logger = debug({isDebug: false, name: 'ExpeditedController'}).logger;

/************************
 * SERVICE INTERACTIONS *
 ************************/
const ServiceData = {
  search() {
    let createPassword = ExpeditedActions.get.createdPasswordModel();
    let expeditedModel = ExpeditedActions.get.expeditedModel();
    let personalModel = ExpeditedActions.get.personalModel();

    logger.log('createPassword', createPassword);
    logger.log('expeditedModel', expeditedModel);
    logger.log('personalModel', personalModel);

    // logger.log('...licenseRegion:', createPassword.licenseRegion);
    // logger.log('...regionIssue:', expeditedModel.regionIssue);

    let data = {};
    // @todo: use model ECR-14250
    if (createPassword.setPassword) { // #driversLicense
      data = {
        driver_license_number: createPassword.licenseNumber,
        country: createPassword.licenseCountry,
        last_name: createPassword.lastName
      };
      if (createPassword.licenseRegion && createPassword.licenseRegion !== EXPEDITED.LICENSE.REGIONS.DVLA) {
        data.country_subdivision = createPassword.licenseRegion;
      } else {
        // DVLA temp hack
        data.issuing_authority = createPassword.licenseRegion;
      }
    } else {
      data = {
        driver_license_number: expeditedModel.license,
        country: expeditedModel.countryIssue.country_code,
        last_name: personalModel.lastName
      };
      if (expeditedModel.regionIssue && expeditedModel.regionIssue !== EXPEDITED.LICENSE.REGIONS.DVLA) {
        data.country_subdivision = expeditedModel.regionIssue;
      } else {
        // DVLA temp hack
        data.issuing_authority = expeditedModel.regionIssue;
      }
    }

    return ExpeditedService
      .searchProfile(data)
      .then((response) => {
        logger.log('searchProfile()', response);
        ExpeditedActions.set.clearProfile();
        ProfileActions.set.clearProfile();

        let stateProfile;
        if (response.profile) {
          stateProfile = ProfileFactory.toState(response.profile)
        } else if (response.messages.length === 0) {
          ExpeditedActions.set.input('render', 'noMatch');
        }

        if (createPassword.setPassword && stateProfile) {
          if (stateProfile.contact_profile && !stateProfile.contact_profile.email) {
            EnrollmentController.setEmailModal(true);
          } else {
            EnrollmentController.setCreatePasswordDetails('email', stateProfile.contact_profile.email);
            EnrollmentController.createLogin();
          }
          return stateProfile;
        } else {
          ErrorActions.clearErrorsForComponent('verification');
          if (stateProfile) {
            AccountActions.setProfile(stateProfile); // i think we want this ASAP anyways
            // AccountActions.setProfile(stateProfile); // i think we want this ASAP anyways
            let searchResponse = response;

            if (ProfileController.isUserDNR()) {
              ExpeditedActions.set.input('modal', 'dnr');
              ExpeditedActions.set.input('dnr', true);
            }

            const session = ReservationActions.getSession();

            if (stateProfile.basic_profile) {
              if (Rules.needsExpeditedCIDModal()) {
                ExpeditedActions.set.input('modal', 'cid');
              }
            }

            ExpeditedActions.set.input('previousChargeType', session.chargeType);

            SessionController
              .updateSession()
              .then((factoryData) => {
                // @todo change this
                let loyalty = _.get(factoryData, 'session.profile.basic_profile.loyalty_data.loyalty_program');
                let render = null;

                if (searchResponse.branch_enrolled) {
                  loyalty = 'Branch-Enrolled';
                }

                if (loyalty) {
                  switch (loyalty) {
                    case PROFILE.NON_LOYALTY:
                      render = 'driver';
                      break;
                    case PROFILE.BRANCH_ENROLLED:
                      render = 'branch';
                      break;
                    case PROFILE.ENTERPRISE_PLUS:
                    case PROFILE.EMERALD_CLUB:
                      render = 'profile';
                      break;
                    default:
                      render = 'noMatch';
                      break;
                  }
                }

                ExpeditedActions.set.input('render', render);
                ScrollUtil.scrollTo('#expedited-profile');

                window._analytics.renter = factoryData.analyticsFields.renter;
                //Triggering Analytics on Expedited Success
                Analytics.trigger('html', 'analytics-expedited');
                return factoryData;
              })
              .catch((err) => ServiceController.trap('ExpeditedController.search()::updateSession()', err));
          }
        }
      })
      .catch((error) => {
        // temp ECR-13850
        if (error.length && error[0].code !== 'RNTR00907') ExpeditedActions.set.input('render', 'noMatch');
        Analytics.trigger('html', 'analytics-expedited');
        ServiceController.trap('ExpeditedController.search().catch()', error);
      });
  },
  enroll() {
    const expedited = ExpeditedActions.get.expeditedModel();
    const personal = ExpeditedActions.get.personalModel();
    const enroll = ExpeditedActions.get.enrollModel();

    let data = ExpeditedFactory.toServer({ expedited, personal, enroll });

    return ExpeditedService
      .enroll(data)
      .then(response => {
        if (response && response.profile) {
          let profile = ProfileFactory.toState(response.profile);
          AccountActions.set.profileModel(profile); // ECR-15109 profile missing is causing an issue. this is reset immedidatley, but we get it back, let's use it
          return SessionController.updateSession(() => {
            ExpeditedActions.set.enroll(profile);
          })
          .then(() => profile);
        } else {
          ErrorActions.setErrorsForComponent(response, 'verification');
          if (response.messages) {
            for (let k in response.messages) {
              if (response.messages[k].code === REQUEST.MESSAGES.CODES.DUPLICATE_ID && response.messages[k].code != REQUEST.MESSAGES.CODES.DUPLICATE_ACC) {
                ExpeditedActions.set.input('modal', 'duplicateID');
              } else if (response.messages[k].code !== REQUEST.MESSAGES.CODES.DUPLICATE_ID && response.messages[k].code === REQUEST.MESSAGES.CODES.DUPLICATE_ACC) {
                ExpeditedActions.set.input('modal', 'duplicateAcc');
              }
            }
          }

          return response;
        }
      })
      // @todo ECR-13850 do something real here
      .catch((err) => {
        console.error('ExpeditedController.enroll().catch()', err);
      });
  }
}

/************************
 * BUSINESS MODEL RULES *
 ************************/
const Rules = {
  isSessionExpedited () {
    return ExpeditedActions.get.sessionExpedited();
  },
  needsExpeditedCIDModal() {
    const loggedIn = ProfileController.isLoggedIn();
    const sessionCID = ResInitiateActions.getCid().toLowerCase();
    const userHasCID = ProfileController.hasCid();
    const userCID = userHasCID && ProfileController.getCidNumber().toLowerCase(); // wut?
    const isPrepay = PaymentModelController.isPrepay();
    const currentStep = ExpeditedActions.get.currentStep();
    const isDeepLink = ExpeditedActions.get.isDeepLink();
    if (
      !(loggedIn && isDeepLink) && currentStep !== PAGEFLOW.CONFIRMED &&
        userHasCID &&
      (!sessionCID ||
        isPrepay ||
        sessionCID !== userCID
      )
    ) {
      return true;
    } else {
      return false;
    }
  },
  isExecutiveProfile(loyaltyMemberType){
    return (loyaltyMemberType === PROFILE.EXECUTIVE);
  },
  isSignatureProfile(loyaltyMemberType){
    return (loyaltyMemberType === PROFILE.SIGNATURE);
  }
}

/**************************
 * COMPONENT INTERACTIONS *
 **************************/
const Components = {
  setInput(field, state) {
    ExpeditedActions.set.input(field, state);
  },
  clearProfile(clearLicense) {
    if (clearLicense) {
      ExpeditedActions.set.clearLicense();
    }
    ExpeditedActions.set.clearProfile();
  },
  setExpeditedCoupon(cid) {
    ReservationActions.setExpeditedCoupon(cid);
  },
  setExpeditedHeadingFocus(expeditedHeading) {
    DomManager.trigger('setExpeditedHeadingFocus', expeditedHeading);
  },
  expediteSearch() {
    return ExpeditedController.search();
  },
  cleanUp() {
    return new Promise(function (resolve) {
      ExpeditedActions.set.input('modal', false);
      resolve();
    });
  },
  isNotExecutiveOrSignatureProfile(loyaltyMemberType){
    const isNotExecutive = !Rules.isExecutiveProfile(loyaltyMemberType);
    const isNotSignature = !Rules.isSignatureProfile(loyaltyMemberType);
    return (isNotExecutive && isNotSignature);
  },
  getLocationByExpedited(expedited) {
    const regionIssue = _.get(expedited, 'regionIssue');
    const countryName = expedited.countryIssue.country_name;
    return (`${regionIssue ? regionIssue.concat(',') : ''} ${countryName}`);
  },
  getCountryInformation(profile) {
    const countrySubdivision = _.get(profile, 'license_profile.country_subdivision_code');
    const countryCode = profile.license_profile.country_code;
    return `${countrySubdivision ? countrySubdivision.concat(',') : ''} ${countryCode}`;
  },
  maskField(fieldValue, qtdShowing, qtdBullets = 4) {
    if (!enterprise.utilities.isMaskedField(fieldValue)) {
      const maskStyle = (_.range(qtdBullets).fill(PROFILE.MASK_CHAR)).join('');
      return (fieldValue ? maskStyle.concat(fieldValue.substring(qtdShowing)) : "");
    }

    return fieldValue;
  }
}

const ExpeditedController = {
  ...ServiceData,
  ...Rules,
  ...Components
};

export default ExpeditedController;
