/**
 * @module BookingWidgetModelController
 * @todo: maybe rename to BookingWidgetController (?)
 *
 * *******************************************************************************************
 *     PLEASE DO NOT IMPORT RESERVATIONFLOWMODELCONTROLLER !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * *******************************************************************************************
 *
 */
import ReservationActions from '../actions/ReservationActions';
import ModifyActions from '../actions/ModifyActions';

import { LOCATION_SEARCH } from '../constants';

import ResInitiateController from './ResInitiateController';
import CorporateController from './CorporateController';
import LocationController from './LocationController';
import DateTimeController from './DateTimeController';
import FedexController from './FedexController';

import LocationModel from '../factories/Location';

import { debug } from '../utilities/util-debug';
const logger = debug({isDebug: false, name: 'BookingWidgetModelController'}).logger;

/**
 * @namespace Rules: Rules used by BookingWidgetModelController
 * @type {Object}
 */
const Rules = {
  isFedex: (() => FedexController.isFedexFlow())()
}

/**
 * @namespace ServiceData: Used by the BookingWidgetModelController
 * @type {Object}
 */
const ServiceData = {

  /**
   * @todo @gbov2 normalize this with ReservationFlowModelController and LocationSearchController set location methods
   * This is called when a BookingWidget user selects a location from the LocationDropdown search results for a
   * round trip (default)
   * @memberOf BookingWidgetModelController
   * @param {SelectedInputLocation} location selected from input
   * @see ReservationFlowModelController
   */
  setLocationForAll(location) {
    logger.warn('setLocationForAll()', location);

    ReservationActions.setBookingWidgetExpanded(true);
    ReservationActions.setLocationForAll(location);
    ReservationActions.clearDateTime();
    ReservationActions.clearPolicies(LOCATION_SEARCH.BOTH);

    // when we have a specific location, we get detailed data
    if (!LocationController.isLocationTypeCityOrCountry(location)) {
      let locationId = LocationModel.getLocationId(location);
      // watch network panel, you'll see the 4 requests
      LocationController.getLocationDetails(locationId, LOCATION_SEARCH.BOTH);
      LocationController.getClosedHoursForLocation(LOCATION_SEARCH.PICKUP);
      LocationController.getClosedHoursForLocation(LOCATION_SEARCH.DROPOFF);
      LocationController.getAgeRangesForLocation(locationId, location);
    } else {
      console.assert(location.countryCode, 'location.countryCode is not defined');
      LocationController.getAgeRangesForLocation(location.countryCode, location);
    }
    ReservationActions.changeView(null, false);
  },

  /**
   * Called from Location Search dropdown on the Booking Widget
   * @memberOf BookingWidgetModelController
   * @param location
   * @param type
   * @see LocationSearchController
   */
  setLocation (location, type) {
    logger.warn('setLocation() FOR BOOKINGWIDGET!!!!!!', type, location)
    ReservationActions.setLocation(location, type);
    ReservationActions.clearPolicies(type);
    // with a specific branch, airport etc. as opposed to a regional location, get details
    if (!LocationController.isLocationTypeCityOrCountry(location)) {
      let locationId = LocationModel.getLocationId(location);
      LocationController.getLocationDetails(locationId, type);
      LocationController.getClosedHoursForLocation(type);
      LocationController.getAgeRangesForLocation(locationId, location);
    }
    ReservationActions.changeView(null, false);
  },

  /**
   * @function getAges
   * @param  {Array}  ageRange array of ages
   * @return {Array}          either ages set by incoming service, or domain default
   */
  getAges (ageRange = []) {
    return (ageRange && ageRange.length > 0) ? ageRange : enterprise.age;
  },

  /**
   * @function setAdditionalCorporateInfo
   * @memberOf BookingWidgetModelController
   * NOTE: NOT NECESSARY FOR ReservationFlowModelController variation of submit
   * @param {[type]} employeeNumber  [description]
   * @param {[type]} fedexSequence   [description]
   * @param {[type]} costCenterValue [description]
   */
  setAdditionalCorporateInfo(employeeNumber, fedexSequence, costCenterValue) {
    if (employeeNumber) {
      CorporateController.setAdditionalInfoFromArray([{
        id: fedexSequence.employeeId,
        value: employeeNumber
      }, {
        id: fedexSequence.costCenter,
        value: costCenterValue
      }]);
    }
  },

  /**
   * @function submit
   * @memberOf BookingWidgetModelController
   * @param  {*} incomingData data to carry forwards
   * @param  {String} source       Source, usually "BookingWidgetModelController" or "ReservationFlowModelController"
   * @return {Promise}             Promise wrapped ajax
   */
  submit(incomingData, source = 'BookingWidgetModelController') {
    return ResInitiateController.submit(incomingData, source)
  }
}

/**
 * @namespace Components work done by BookingWidgetModelController
 * @type {Object}
 */
const Components = {

  /**
   * Called by BookingWidget > LocationSearch to trigger selected location settings
   * ********* User Called *********
   * @return {void} sets flags in state tree and kicks off some service calls
   */
  selectLocationFromInput(selectedLocation, type, sameLocationFlag) {
    if (BookingWidgetModelController.isFedex) {
      BookingWidgetModelController.setAlphaId(selectedLocation, type);
    } else if (sameLocationFlag) {
      BookingWidgetModelController.setLocationForAll(selectedLocation);
    } else {
      BookingWidgetModelController.setLocation(selectedLocation, type);
    }
  },

  /**
   * Used by LandingPageMap which is sort of a one off weird case
   * @param  {*} deepLinkReservation
   * @return {*}
   */
  loadMap (deepLinkReservation) {
    let suffix = 'enterprise';
    let latlng = new google.maps.LatLng(deepLinkReservation.pickup_location.gps.latitude, deepLinkReservation.pickup_location.gps.longitude);
    let mapOptions = {
      zoom: 10,
      center: latlng,
      draggable: false,
      scrollwheel: false,
      streetViewControl: false,
      mapTypeControl: false,
      disableDefaultUI: true,
      animation: google.maps.Animation.DROP
    };
    let map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    new google.maps.Marker({
      position: latlng,
      map: map,
      title: deepLinkReservation.pickup_location.name,
      icon: '/etc/designs/ecom/dist/img/icons/png/ENT-mappin-single-' + suffix + '.png'
    });

    google.maps.event.addListenerOnce(map, 'idle', ()=> {
      google.maps.event.trigger(map, 'resize');
      map.setCenter(latlng);
    });
  },

  /**
   * @function getCouponDetails
   * Called by Components/BookingWidget (and DateTimeWidget ?)
   * @memberOf BookingWidgetModelController
   * @param  {object} deepLinkReservation deepLink reservaiton data
   * @param  {object} profile             profile data
   * @param  {string} coupon              coupon code
   * @param  {object} contractDetails     contract object
   * @param  {string} prepopulatedCoupon  preset string
   * @return {object}                     object showing placeholder input text and if it can be edited
   */
  getCouponDetails (deepLinkReservation, profile, coupon, contractDetails, prepopulatedCoupon) {
    let placeholder;
    let profileCID;
    // ECR-14251
    const deepLinkRes = deepLinkReservation && deepLinkReservation.reservation;
    const deepLinkContractName = deepLinkRes && deepLinkRes.contract_details && deepLinkRes.contract_details.contract_name;
    const sessionProfileDetails = profile && profile.basic_profile.customer_details;
    let profileAllowRemoval;
    let contractAllowRemoval;

    if (sessionProfileDetails && sessionProfileDetails.contract_number) {
      // CID from Profile
      placeholder = null;

      if ((coupon && coupon.toUpperCase()) === sessionProfileDetails.contract_number.toUpperCase()) {
        profileCID = true;
        placeholder = sessionProfileDetails.contract_name;
        profileAllowRemoval = sessionProfileDetails.allow_account_removal_indicator;
      }
    }

    if (coupon && contractDetails && contractDetails.contract_number) {
      // CID from Session
      if (coupon && coupon.toUpperCase() === contractDetails.contract_number.toUpperCase()) {
        placeholder = contractDetails.contract_name;
        contractAllowRemoval = contractDetails.allow_account_removal_indicator;
      }
    } else if (deepLinkContractName) {
      // CID from Deeplink
      placeholder = deepLinkContractName;
      contractAllowRemoval = deepLinkRes.contract_details.allow_account_removal_indicator;
    } else if (coupon && prepopulatedCoupon && prepopulatedCoupon.contract_number) {
      // Prepopulated CID
      placeholder = prepopulatedCoupon.contract_name;
      contractAllowRemoval = prepopulatedCoupon.allow_account_removal_indicator;
    }
    const isRemovable = (profileCID && profileAllowRemoval) || (!profileCID && contractAllowRemoval);
    return {placeholder, isRemovable};
  },

  /**
   * Set a location flag for Fedex location selection by alphaID
   * @param {object} location object
   * @param {string} type     pickup, dropoff, etc.
   */
  setAlphaId (location, type) {
    ReservationActions.setLocation(location, type);
    ReservationActions.changeView(null, false);
  },

  // ReservationActions, "aliased" here in case we ever need to get in the middle
  // @todo: remove these?
  setBookingWidgetExpanded: ReservationActions.setBookingWidgetExpanded,
  setLoyaltyBrand: ReservationActions.setLoyaltyBrand,
  closeNoVehicleAvailabilityModal: ReservationActions.closeNoVehicleAvailabilityModal,
  closeDNRModal: ReservationActions.closeDNRModal,
  setCoupon: ReservationActions.setCoupon,
  setCouponLabel: ReservationActions.setCouponLabel,
  setEmployeeNumber: ReservationActions.setEmployeeNumber,

  // membershipIdChange: ReservationActions.set.membershipId, // wrong
  // should be right
  setMembershipId: ReservationActions.set.membershipId,
  // should be right
  loyaltyLastNameChange: ReservationActions.set.loyaltyLastName,

  setAge: ReservationActions.setAge,
  showAgePolicyModal: ReservationActions.showAgePolicyModal,
  setBookingWidgetUpdatedError: ReservationActions.setBookingWidgetUpdatedError,
  clearEmployeeNumberError: ReservationActions.clearEmployeeNumberError,

  // CorporateController // figure out circular dependencies
  setLockedCIDModal: CorporateController.setLockedCIDModal,
  checkForExistingCorporateCode: CorporateController.checkForExistingCorporateCode,

  // modify!!
  toggleInflightModifyModal: ModifyActions.toggleInflightModifyModal,

  toggleInflightModify: ReservationActions.toggleInflightModify,

  setDate: DateTimeController.setDate.bind(DateTimeController),
  setTime: DateTimeController.setTime.bind(DateTimeController)
}

/**
 * @namespace Deprecated : can test with our without by including below
 * @type {Object}
 */
// const Deprecated = {
//   /**
//    * @deprecated use LocationController instead
//    */
//   getClosedHoursForLocation(type, peopleSoftId, reservationsInitiateRequest) {
//     logger.warn('getClosedHoursforLocation() DEPRECATED use LocationController INSTEAD');
//     LocationController.getClosedHoursForLocation(type, peopleSoftId, reservationsInitiateRequest);
//   },

// }

/**
 * @namespace RightPlace for the HashModal feature
 * Should we be moving this?
 * @type {Object}
 */
const RightPlace = {
  /**
   * Used by the "Right Place" hash model process
   * @memberOf BookingWidgetModelController
   * @param {string} type type of hash being used
   */
  setRightPlaceType (type) {
    ReservationActions.setRightPlaceType(type);
  },
  /**
   * Used by the "Right Place" hash model process
   * @param {object} object reference
   */
  setRightPlaceRefer (object) {
    ReservationActions.setRightPlaceRefer(object);
  },
  /**
   * Used by the "Right Place" hash model process
   * @memberOf BookingWidgetModelController
   * @function setRightPlaceModal
   * @param {boolean} bool to set the right place flag
   */
  setRightPlaceModal (bool) {
    ReservationActions.setRightPlaceModal(bool);
  },
  /**
   * Used by "Right Place" hash model process
   * @function closeRightPlaceModal
   */
  closeRightPlaceModal() {
    ReservationActions.closeRightPlaceModal();
  }
}

const BookingWidgetModelController = {
  name: 'BookingWidgetModelController',

  ...Rules,
  ...Components,
  ...ServiceData,

  ...RightPlace//,
  // ...Deprecated

};

module.exports = BookingWidgetModelController;
