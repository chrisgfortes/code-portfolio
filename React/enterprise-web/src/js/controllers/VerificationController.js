/**
 * @module VerficationController
 * @todo: make ... smaller ...
 */
import ExpeditedActions from '../actions/ExpeditedActions';
import CorporateActions from '../actions/CorporateActions';
import ModifyActions from '../actions/ModifyActions';
import VerificationActions from '../actions/VerificationActions';
import ErrorActions from '../actions/ErrorActions';
import ReservationActions from '../actions/ReservationActions';
import PaymentMethodActions from '../actions/PaymentMethodActions';
import AccountActions from '../actions/AccountActions';

import { LogisticsInfoFactory } from '../factories/Corporate';
import { DriverInfoFactory } from '../factories/User';

import PricingController from './PricingController';
import FedexController from './FedexController';
import EnrollmentController from './EnrollmentController';
import ReservationFlowModelController from './ReservationFlowModelController';
import ExistingReservationController from "./ExistingReservationController";
import PaymentModelController from './PaymentModelController';
import ExpeditedController from './ExpeditedController';
import AccountController from './AccountController';

// import ExpeditedService from '../services/ExpeditedService';
import CarSelectService from '../services/CarSelectService';
import AccountService from '../services/AccountService';
import VerificationServices from '../services/VerificationServices';

import FocusManager from '../modules/FocusManager';

import { getVerificationErrorCodes,
         hasAccountConflict,
         hasDNCCodes } from "./ErrorsController";
import { getVerificationPostalLabel,
         getVerificationSubdivisionLabel } from "./LocaleController";

import EcomError from '../classes/EcomError';

import Images from '../utilities/util-images';
import Scroll from '../utilities/util-scroll';
import ObjectUtil from '../utilities/util-object';
import Storage from '../utilities/util-localStorage';
import { LOCALES, PRICING, PROFILE, PARTNERS, PAGEFLOW } from "../constants/";
import { debug } from '../utilities/util-debug';

const logger = debug({isDebug: false, name: 'VerificationController'}).logger;

/**
 * @namespace ServiceData
 * @type {Object}
 * @todo : invert services handling from services files
 */
const ServiceData = {

  /**
   * @function callGetCountries
   * @memberOf VerificationController
   * @return {Promise} returns the jquery ajax promise
   * @see components/Verification/ResidenceInformation
   */
  callGetCountries (stateProfile) {
    logger.log('callGetCountries()');
    return AccountService.getCountries()
      .then(() => {
        //Two tier hierarchy defaults, domain CountryCode then profile CountryCode
        let selectedCountry = enterprise.countryCode;
        if (stateProfile) {
          selectedCountry = stateProfile.address_profile.country_code
        }
        VerificationController.setPersonalFields('countryCode', selectedCountry);
        VerificationController.setPersonalFields('issueCountry', selectedCountry);
        VerificationController.callGetSubdivisions(selectedCountry);
      })
  },

  /**
   * @function getEnrollResponse
   * @memberOf VerificationController
   * @return {object} response from enrollment service
   * @todo  TEST THIS REFACTOR
   * @todo  THERE MUST BE MORE TO THIS, THAN THIS
   */
  getEnrollResponse () {
    logger.log('getEnrollResponse()');
    return ExpeditedController.enroll();
  },

  /**
   * @function getPrepayPolicy
   * @memberOf VerificationController
   * @return {void} sets policies t&c
   * @todo: legal factory
   */
  getPrepayPolicy () {
    logger.log('getPrepayPolicy()');
    VerificationServices.fetchPrepayPolicy()
      .then((response) => {
        if (response && response.prepay_terms_and_conditions) {
          VerificationActions.setPrepayPolicy(response.prepay_terms_and_conditions)
        } else {
          throw new EcomError('getPrepayPolicy() returned no prepay terms and conditions.', 'VerificationController');
        }
      })
  },

  /**
   * @function getTaxesFeesDisclaimer
   * @memberOf VerifcationController
   * @return {void} sets taxes and fees disclaimers
   * @todo: legal factory
   */
  getTaxesFeesDisclaimer () {
    logger.log('getTaxesFeesDisclaimer()');
    VerificationServices.fetchTaxesFeesDisclaimer()
      .then((response) => {
        if (response && response.taxes_fees_and_surcharges) {
          VerificationActions.setTaxesFeesDisclaimer(response.taxes_fees_and_surcharges)
        } else {
          throw new EcomError('getTaxesFeesDisclaimer() returned no taxes and fees surcharges.', 'VerificationController');
        }
      })
  },

  /**
   * @function callLinkAccount
   * @memberOf VerificationController
   * @param  {Boolean} ignore if we should ignore
   * @return {void}         sets conflicts etc.
   * @todo toServer User factory
   * Note: This uses the ErrorController hasAccountConflict because that's what we're looking
   * for in this instance, as opposed to using Factory model here
   */
  callLinkAccount (ignore = false) {
    logger.log('callLinkAccount()');
    const session = ReservationActions.getSession();
    const userId = _.get(session, 'profile.basic_profile.loyalty_data.id');

    if (!userId) {
      return false;
    }

    let data = {};

    if (ignore === true) {
      data.ignoreConflicts = true;
    }
    // end factory model

    VerificationServices.linkAccount(data)
      .then((response) => {
        if (response.additional_data) {
          VerificationActions.setAssociateAccountStatus('cleared');
        } else if (response.messages) {
          if (hasAccountConflict(response.messages)) {
            VerificationActions.setConflictAccountModal(true);
          } else {
            ErrorActions.setErrorsForComponent(response, 'verification');
          }
        } else if (response.error) {
          throw new EcomError('callLinkAccount() returned an error.', 'VerificationController');
        }
      })
  },

  /**
   * @function callSubmitSave
   * @memberOf VerificationController
   * @param  {function}  afterCommitCB
   * @param  {Boolean}  modify
   * @return {void}                       kicks off a process
   * @see  components/Verification/Submit.js
   */
  callSubmitSave ( modify ) {
    logger.log('callSubmitSave()');
    const isFedexReservation = VerificationController.isFedexReservation();
    if (isFedexReservation) {
      let data = ServiceData.getSubmitData();
      const verification = VerificationActions.getVerification();

      if (verification.privacyChecked) {
        data.privacyChecked = verification.privacyChecked;
      }

      return VerificationServices.submitSave(data)
        .then(() => {
          logger.log('after callSubmitSave call to commit');
          return VerificationController.commit(modify);
        })
        .catch(() => {
          throw new EcomError('Issue saving Fedex Data for callSubmitSave().', 'VerificationController');
        })
    } else {
      logger.log('callSubmitSave... commit call');
      return VerificationController.commit(modify);
    }
  },

  /**
   * @function commit
   * @memberOf VerificationController
   * @param  {Boolean} isModify is this a modify or not
   * @return {Promise}
   */
  commit (isModify = false) {
    logger.log('commit()');
    let data = ServiceData.getSubmitData();
    logger.log('data: ', data);
    if (isModify) {
      delete data.billing_account_type;
      delete data.billing_account;
    }
    ErrorActions.clearErrorsForComponent('verification');

    return VerificationServices.submit(data, isModify)
            .then((response) => {
              logger.log('submit().then()', response);
              if (response.messages) {
                if (hasDNCCodes(response.messages)) {
                  VerificationActions.setDNCModal(true);
                } else {
                  logger.log('else on messages');
                  ErrorActions.setErrorsForComponent(response.messages, 'verification');
                }
              } else {
                logger.log('else no messages');
                VerificationActions.setAddCardSuccess(false);
                VerificationActions.clearThreeDS();
                ExpeditedActions.set.clearProfile();
                VerificationActions.clearPreviousErrors();
              }
            })
            .catch((err) => {
              logger.error('error ... ', err);
              throw new EcomError('Error thrown on submit()' + err, 'VerificationServices');
            })
  },

  /**
   * @function commitWithClearedDNC
   * @memberOf VerificationController
   * @return {void} starts process
   */
  commitWithClearedDNC() {
    let data = ServiceData.getSubmitData();

    data.delivery = {};
    data.collections = {};

    VerificationServices.commitWithClearedDNC(data)
      .then(() => {
        ErrorActions.clearErrorsForComponent('verification');
        VerificationController.closeConflictAccountModal();
      })
      .catch((err) => {
        throw new EcomError('Issue with commitWithClearedDNC()' + err, 'VerificationController');
      });
  },

  /**
   * @function upgradeVehicle
   * @memberOf VerificationController
   * @param  {string} code car class code
   * @return {void}      sets upgrade
   */
  upgradeVehicle ( code ) {
    logger.log('upgradeVehicle()');
    CarSelectService.upgradeVehicle( code )
      .then(() => {
        AccountService.getSession();
      })
      .catch(() => {
        throw new EcomError('upgradeVehicle call failed.', 'VerificationController');
      })
  },

  /**
   * @function callGetSubdivisions
   * @memberOf VerificationController
   * @param  {string} country country code
   * @return {void}         calls service
   * @todo : should call ProfileController method that in turn calls this service
   */
  callGetSubdivisions ( country ) {
    logger.log('callGetSubdivisions()');
    AccountService.getSubdivisions( country );
  },

  // @todo: this has a callback need in one place
  getDisclaimer () {
    logger.log('getDisclaimer()');
    return VerificationServices.fetchDisclaimer()
      .then((response) => {
        if (response && response.privacy_policy) {
          VerificationActions.setPrivacyPolicy(response.privacy_policy);
          return response;
        } else {
          return Promise.reject('- No policy returned.');
        }
      })
      .catch((message) => {
        throw new EcomError('getDisclaimer() failed to successfully fetchDisclaimer().' + message, 'VerificationController');
      })
  },

  /**
   * @function callGetIssueSubdivisions
   * @param  {string} country country code
   * @return {void}         sets values in state
   */
  callGetIssueSubdivisions ( country ) {
    AccountService.getIssueCountrySubdivisions( country );
  },

  // @todo: invert with service
  getRes (confirmationNumber, firstName, lastName) {
    ExistingReservationController.getExistingReservationDetails(confirmationNumber, firstName, lastName);
  },

  // @todo - finish the data models used here and clean this function, it still has almost 100 lines!!!
  getSubmitData () {
    const model = VerificationActions.getModelTree(),
      personal = AccountActions.get.personalModel(),
      expedited = model.expedited,
      purpose = model.purpose,
      additionalInfo = model.additionalInfo,
      delivery = model.delivery,
      collection = model.collection,
      billingAuthorized = model.billingAuthorized,
      paymentType = model.paymentType,
      paymentID = model.paymentID,
      paymentReferenceID = model.paymentReferenceID,
      session = ReservationActions.getSession(),
      profile = AccountActions.get.profileModel(),
      corporateSession = session.contract_details, // @todo use contractDetails cursor!
      isFedexReservation = session.fedexReservation,
      sessionPickupLocation = session.pickup_location,
      sessionDropoffLocation = session.return_location,
      isPrepaySelected = session.prepaySelected,
      verification = VerificationActions.getVerification(),
      threeDSToken = verification.threeDS.token;

    const phoneNumber = personal.phoneNumber || _.get(profile, 'contact_profile.phones[0].phone_number');

    let data = {
      driver_info: DriverInfoFactory.toServer({...personal, phoneNumber}, expedited, isFedexReservation),
      airline_information: (personal.airlineCode || personal.flightNumber) ? {
        code: personal.airlineCode,
        flight_number: personal.flightNumber
      } : null,
      travel_purpose: purpose || null,
      delivery: delivery.enabled ? LogisticsInfoFactory.toServer(delivery, sessionPickupLocation.address.country_code) : null,
      collection: collection.enabled ? LogisticsInfoFactory.toServer(collection, sessionDropoffLocation.address.country_code) : null,
      prepay3_dspa_res: threeDSToken || null
    };

    if (additionalInfo.length > 0) {
      data.additional_information = additionalInfo;

      if (isFedexReservation) {
        const mapping = corporateSession.additional_information.reduce((obj, info) => (
          {...obj, ...{[enterprise.fedexSequenceIds['' + info.sequence]]: info.id}}
        ), {});

        const infoIDsToValue = additionalInfo.reduce((obj, info) => (
          {...obj, ...{[info.id]: info.value}}
        ), {});

        data.employeeId = infoIDsToValue[mapping.employeeId];
        data.requisitionNo = infoIDsToValue[mapping.requisitionNo];
      }
    }

    // @todo - this whole if block could get some love...and refactoring
    if (corporateSession) {
      if ( _.get(corporateSession, 'contract_has_additional_benefits') === false ) {
        if (corporateSession.contract_billing_account) {
          data.billing_account_type = 'EXISTING';
        } else {
          //Pay at counter, do nothing
        }
      } else if (corporateSession.contract_accepts_billing) { //Non-blacklist
        if (corporateSession.contract_billing_account) {
          if (billingAuthorized === 'true') {
            data.billing_account_type = 'EXISTING';
          } else if (paymentType === 'CREDIT_CARD') {
            data.payment_ids = [paymentID];
          } else {
            //Pay at counter, do nothing
          }
        } else if (billingAuthorized === 'true') {
          if (paymentType === 'EXISTING') {
            data.payment_ids = [paymentID];
          } else if (paymentType === 'CUSTOM') {
            data.billing_account_type = 'CUSTOM';
            data.billing_account = paymentID;
          } else {
            //Pay at counter, do nothing
          }
        } else if (paymentType === 'CREDIT_CARD') {
          data.payment_ids = [paymentID];
        } else {
          //Pay at counter, do nothing
        }
      } else {
        //Pay at counter, do nothing
      }
    }

    if(isPrepaySelected && PaymentMethodActions.getPangui() === 'PANGUI') {
      data.payment_ids = [paymentReferenceID];
    }

    ObjectUtil.deleteNull(data, ['driver_info.request_email_promotions']);

    return data;
  }

} // ServiceData

const Rules = {
  /**
   * @function isFedexReservation
   * @memberOf VerificationController
   * @return {Boolean} true or false from the STATE TREE
   * Why here, and not just calling this directly? Mainly to be as obvious as possible...
   */
  isFedexReservation () {
    return FedexController.isFedexReservation();
  },
  hasContractWithPromotionsEnabled ( contract ) {
    return !contract || contract.marketing_message_indicator;
  },
  isExpediteSearchFilled ( expedited, personal ) {
    return !!(expedited.license
    && personal.lastName
    && expedited.countryIssue
    && (expedited.countryIssue.enable_country_sub_division ? expedited.regionIssue : false));
  },
  isMissingPayment ( verification, prepay, modify, collectNewModifyPaymentCard, paymentProcessor, paymentReferenceID ) {
    const failedThreeDS = verification.threeDS.url && !verification.threeDS.token;
    if (!prepay || (modify && !collectNewModifyPaymentCard)) {
      return false;
    }
    if (paymentProcessor === PRICING.PAYMENT_PROCESSOR_NA) {
      if (!paymentReferenceID) {
        return true;
      }
    } else {
      return (!verification.registerCardSuccess || failedThreeDS);
    }
  },
  enforceExpeditedValidation ( modify, expedited, expediteSearchFilled ) {
    return !modify
    && expediteSearchFilled
    && (expedited.render === 'noMatch' || expedited.render === 'driver' || expedited.render === 'branch' || expedited.render === 'edit');
  },
  ecUnauthAssociated ( driverInfoIdentifier, driverInfoLoyalty ) {
    return ((driverInfoIdentifier) ? true : false) && (driverInfoLoyalty === PARTNERS.EMERALD_CLUB_LONG);
  },
  onlyShowEP ( sessionContractDetails ) {
    return (sessionContractDetails && sessionContractDetails.contract_type !== PRICING.PROMOTION);
  },
  showExpeditedBanner ( resEnrollment, userLoggedIn, expedited, modify, ecUnauthAssociated ) {
    return (resEnrollment && !userLoggedIn && !expedited && !modify && !ecUnauthAssociated);
  },
  showExpedited ( userLoggedIn, expedited, modify, ecUnauthAssociated, expediteEligible ) {
    return (!(userLoggedIn && expedited.render !== PROFILE.EDIT) && !modify && !ecUnauthAssociated && (expediteEligible !== false));
  },
  showFlightInfo ( sessionPickupLocation ) {
    return sessionPickupLocation && (sessionPickupLocation.location_type === 'airport');
  },
  showAssociateAccountStatus ( modify, userLoggedIn, profile, userAssociatedWithRes, associateAccountStatus ) {
    return modify && userLoggedIn && profile && !userAssociatedWithRes && (associateAccountStatus !== 'cleared');
  },
  showKeyRentalFacts ( keyRentalFacts ) {
    return !!(keyRentalFacts && keyRentalFacts.length);
  },
  showRedemption ( chargeType ) {
    return chargeType === PRICING.REDEMPTION;
  },
  showECContent ( sessionBrand ) {
    return sessionBrand === PARTNERS.BRAND.EMERALD_CLUB;
  },
  showPageFlowCancelled ( sessionBrand ) {
    return sessionBrand === PARTNERS.BRAND.EMERALD_CLUB;
  },
  showAdditionalInfo ( showPersonal, modify, additionalInformationSaved ) {
    return !!((showPersonal || !modify) && additionalInformationSaved && additionalInformationSaved.length > 0);
  },
  showContractDetails ( showPersonal, contractDetails ) {
    return !!(showPersonal && contractDetails);
  },
  showAvailableUpgrades ( selectedCar, existingReservationConfirmationNumber, upgrades ) {
    return !!selectedCar.vehicleRates
        && !existingReservationConfirmationNumber
        && upgrades.length > 0
        && Object.keys(upgrades[0].priceDifferences).length > 0;
  },
  showAdditionalInfoOnConfirm ( showAdditionalInfo, componentToRender ) {
    return showAdditionalInfo && (componentToRender === PAGEFLOW.CONFIRMED);
  },
  showModifyContent ( modify, modifyPriceDifferenceZero ) {
    return (modify && !modifyPriceDifferenceZero);
  },
  showAirlineInfo ( showPersonal, airlineInformation ) {
    return !!(showPersonal && airlineInformation);
  },
  showRequestExtras ( selectedCar, requestExtras ) {
    return (selectedCar.status && selectedCar.status.indexOf(PRICING.EXTRAS_ON_REQUEST) > -1) || requestExtras.length;
  },
  showBilling ( contractDetails ) {
    return contractDetails ? contractDetails.contract_accepts_billing : false;
  },
  showSpecialMessage (corporateSpecialMessage, currentHash) {
    return !!corporateSpecialMessage
        && currentHash === PAGEFLOW.CONFIRMED_CONFIRMED;
  },
  showTripPurpose (codeApplicable, showPersonal, sessionContractDetails) {
    return !!(codeApplicable && showPersonal && sessionContractDetails);
  },
  showBillingAccount (modify, billingAccount, sessionContractDetails) {
    return (modify && billingAccount && sessionContractDetails && sessionContractDetails.contract_billing_account);
  },
  showDeliveryCollection (modify, vehicleLogistics) {
    return (modify && vehicleLogistics);
  },
  showSwitchPay (modify, pricingDifference, showPersonal) {
    return !!(!modify && pricingDifference.PREPAY && !showPersonal);
  }
} // Rules

/**
 * @namespace Components
 * @type {Object}
 */
const Components = {
  /**
   * used by Taxes and Fees
   * @return {[type]} [description]
   */
  callSetInput ( type, obj ) {
    ExpeditedActions.set.input( type, obj );
  },
  determineRetrieveAuth ( loggedIn, messages ) {
    return loggedIn && (messages || []).some( m => m.code === 'CROS_RES_NEW_LOGIN_FOR_FULL_ACCESS' );
  },
  formatSummaryMessage ( isModified, amount ) {
    const lang = {
      baselineSummaryMessage : i18n('prepay_1005') || 'Your credit card will be charged the following amount:',
      modifiedSummaryMessage : i18n('prepay_0072') || 'You will be refunded the original amount paid and your credit card will be charged the following amount: '
    }
    let mess = '';
    if (isModified) {
      mess = lang.modifiedSummaryMessage;
    } else {
      mess = lang.baselineSummaryMessage;
    }
    return (<span className="charged"> {mess} {amount} </span>);
  },
  formatPolicyHtml ( policy ) {
    const rentalPoliciesMark = '#{rentalPolicies}';
    const rentalPoliciesCssClass = 'open-rental-policies';
    const rentalPoliciesLabel = i18n('prepay_0075') || 'Rental Policies';
    const rentalPoliciesLink = `<a href="#" class="${rentalPoliciesCssClass}">${rentalPoliciesLabel}</a>`;
    return policy && policy.replace(rentalPoliciesMark, rentalPoliciesLink);
  },

  // @todo: car model should be used here via car factory of some type
  getCarSummary (selectedCar, ageLabel, sessionPickupTime, sessionDropoffTime, sessionPickupLocation, sessionDropoffLocation, contractDetails) {
    return selectedCar ? {
      vehicleType: selectedCar.name,
      vehicleMake: selectedCar.models,
      image: Images.getUrl(selectedCar.images.SideProfile.path, 352, 'high'),
      age: ageLabel,
      code: contractDetails ? contractDetails.contract_name : '',
      contract: contractDetails,
      time: {
        pickup: moment(sessionPickupTime),
        dropoff: moment(sessionDropoffTime)
      },
      location: {
        pickup: sessionPickupLocation.name,
        dropoff: sessionDropoffLocation.name
      }
    } : {
      vehicleType: null,
      vehicleMake: null,
      age: 0,
      code: null,
      time: {
        pickup: null,
        dropoff: null
      },
      location: {
        pickup: null,
        dropoff: null
      },
      extras: null
    }
  },
  setDefaultCountry ( account, profile ) {
    logger.log('setDefaultCountry()', account, profile);
    let countryIssue = (
      profile ?
        profile.license_profile.country_code :
        enterprise.countryCode
    );
    let countryResidence = (
      _.get(profile, 'address_profile.country_code') ?
        _.get(profile, 'address_profile.country_code') :
        enterprise.countryCode
    );
    let issueFlag = false;
    let residenceFlag = false;

    EnrollmentController.getIssueCountrySubdivisions(countryIssue);
    EnrollmentController.getSubdivisions(countryResidence);

    if (account.countries) {
      for (let i = 0, len = account.countries.length; i < len; i++) {
        if (account.countries[i].country_code === countryIssue) {
          ExpeditedActions.set.input('countryIssue', account.countries[i]);
          issueFlag = true;
        }
        if (account.countries[i].country_code === countryResidence) {
          ExpeditedActions.set.input('countryResidence', account.countries[i]);
          residenceFlag = true;
        }

        if (issueFlag && residenceFlag) {
          break;
        }
      }
    }
  },
  getEstimatedTotalValue ( pricing, contractDetails, selectedCar, chargeType ) {
    let estimatedTotalValue = '';
    if (_.get(contractDetails, 'contract_type') === 'CORPORATE' && _.get(selectedCar, `vehicleRates.${chargeType}.price_summary.estimated_total_view.amount`) === '0.00'
    ) {
      estimatedTotalValue = PricingController.getNetRate();
    } else {
      estimatedTotalValue = pricing ? pricing.estimated_total_view : PricingController.getNetRate(true);
      estimatedTotalValue = PricingController.getSplitCurrencyAmount(estimatedTotalValue);
    }
    return estimatedTotalValue;
  },
  getFees ( savings, pricing ) {
    let fees = pricing && pricing.fee_line_items ? pricing.fee_line_items : [];
    if(savings.length) {
      fees = fees.concat(savings);
    }
    return fees;
  },
  getFieldErrors ( errors ) {
    // Map error codes from CROS to the correspondent form fields
    let fieldErrors = {
      email: false,
      firstName: false,
      phoneNumber: false,
      lastName: false
    };

    if (errors && errors.length > 0) {
      errors.forEach( e => {
        getVerificationErrorCodes(fieldErrors, e.code);
      });
    }

    return fieldErrors;
  },
  getForeignFeesDestinationAmountText ( prepay, pricing ) {
    const estimatedTotalDestinationValue = pricing ? pricing.estimated_total_payment.format : PricingController.getNetRate(true);
    const estimatedTotalDestinationPriceCode = pricing ? pricing.estimated_total_payment.code : PricingController.getNetRate(true);
    let foreignFees = '';
    let showDestinationAmount = '';
    if (prepay && (_.get(pricing, 'estimated_total_payment.code') !== _.get(pricing, 'estimated_total_view.code'))) {
      foreignFees = i18n('prepay_0034') || "Your bank's foreign transaction fees may apply";
    }
    if (prepay && enterprise.currentDomain === LOCALES.DOMAIN_USA && _.get(pricing, 'estimated_total_payment.code') !== LOCALES.USA_PAY_CODE) {
      foreignFees = i18n('prepay_0034') || "Your bank's foreign transaction fees may apply";
      if (_.get(pricing, 'estimated_total_payment.code') !== _.get(pricing, 'estimated_total_view.code')) {
        showDestinationAmount = i18n('prepay_0032', {'currency': '(' + estimatedTotalDestinationValue + ')'}) ||
          `You will be charged in your destination's currency CAD ${estimatedTotalDestinationValue}`;
      }
    } else if (prepay && enterprise.currentDomain === LOCALES.DOMAIN_CANADA && _.get(pricing, 'estimated_total_payment.code') !== LOCALES.CANADA_PAY_CODE) {
      foreignFees = i18n('prepay_0034') || "Your bank's foreign transaction fees may apply";
      if (_.get(pricing, 'estimated_total_payment.code') !== _.get(pricing, 'estimated_total_view.code')) {
        showDestinationAmount = i18n('prepay_0033', {'currency': '(' + estimatedTotalDestinationValue + ')'}) ||
          `You will be charged in your destination's currency USD ${estimatedTotalDestinationValue}`;
      }
    } else if (_.get(pricing, 'estimated_total_payment.code') !== _.get(pricing, 'estimated_total_view.code')) {
      showDestinationAmount = i18n('reservationnav_0042', {'currencyCode': estimatedTotalDestinationPriceCode, 'currency': estimatedTotalDestinationValue}) ||
        `You will be charged in ${estimatedTotalDestinationPriceCode} (${estimatedTotalDestinationValue})`;
    }
    return {foreignFees, showDestinationAmount};
  },
  getIsUnpaidNotRefund ( modifyPriceDifferenceCurrency ) {
    return modifyPriceDifferenceCurrency.integralPart > 0;
  },
  getLabels ( paymentInformation, currentComponent, prepay ) {
    const maskedNumber = paymentInformation ? '(' + PaymentModelController.maskCC(paymentInformation.card_details.number) + ')' : '';
    const prepayLabel = currentComponent === 'Confirmed' ? enterprise.i18nReservation.prepay_1004.replace(/#{cardNumber}/, maskedNumber) : enterprise.i18nReservation.prepay_1005;
    const paymentMethodLabel = prepay ? prepayLabel : enterprise.i18nReservation.resflowreview_0110;
    const totalLabel = prepay ? enterprise.i18nReservation.resflowreview_0118 : enterprise.i18nReservation.resflowreview_0079;
    return { prepayLabel, paymentMethodLabel, totalLabel }
  },
  getRequestExtras ( selectedCar, chargeType ) {
    return selectedCar.vehicleRates[chargeType].extras.equipment.filter(function (extra) {
      if (extra) {
        return (extra.selected_quantity > 0 && extra.allocation.indexOf(PRICING.EXTRAS_ON_REQUEST) > -1);
      }
    });
  },
  getRedemptionChargeType ( chargeType ) {
    return (chargeType === PRICING.REDEMPTION) ? PRICING.PAYLATER : chargeType;
  },
  getCouponInfoHeading ( contractDetails, codeApplicable ) {
    let couponInfoHeading = enterprise.i18nReservation.reservationwidget_0039;
    let codeApplicableMsg = '';
    let contractType = false;
    if (contractDetails && contractDetails.contract_type) {
      contractType = contractDetails.contract_type.toLowerCase();
      if (contractType === 'corporate') {
        couponInfoHeading = enterprise.i18nReservation.resflowcorporate_4015;
        codeApplicableMsg = '(' + (codeApplicable ? i18n('resflowcarselect_8011') : i18n('promotionemail_0021')) + ')';
      } else if (contractType === 'promotion') {
        couponInfoHeading = enterprise.i18nReservation.resflowcorporate_4016;
        codeApplicableMsg = '(' + (codeApplicable ? i18n('promotionemail_0003') : i18n('promotionemail_0019')) + ')';
      }
    }
    return { couponInfoHeading, contractType, codeApplicableMsg }
  },

  getCost ( chargeType, selectedCar ) {
    const chargeTypeRedemption = this.getRedemptionChargeType( chargeType );
    return _.get(selectedCar, `vehicleRates.${chargeTypeRedemption}.price_summary.estimated_total_view`)
        || null;
  },

  // @todo: corporate controller? maybe not as this is directly content related? could be split, seems to be begging for factory method
  getCorporateSummary ( corporateSession, contractDetails, billingAuthorized, purpose, cost ) {
    let corporateSummary = false;
    if (corporateSession && contractDetails.contract_has_additional_benefits !== false && corporateSession.contract_accepts_billing &&
        corporateSession.contract_billing_account && billingAuthorized === 'true' && purpose === PRICING.PURPOSE_BUSINESS) {
      corporateSummary = i18n('resflowcorporate_0067').replace('#{account}', corporateSession.contract_name).replace('#{total}', cost);
    }
    return corporateSummary;
  },

  getDefaultCountry ( account, profile ) {
    if (account.countries && account.countries.length < 1) {
      AccountController.getCountries().then(() => {
        // need updated account model not from state
        let accountModel = AccountActions.get.accountModel();
        VerificationController.setDefaultCountry( accountModel, profile );
      });
    } else {
      VerificationController.setDefaultCountry( account, profile );
    }
  },

  getResidenceLabels ( personal, countryData ) {
    let originCountry = personal.countryCode || enterprise.countryCode,
      postalLabel = null,
      subdivisionLabel = null,
      enableIssuingAuthority = countryData && countryData.enable_country_sub_division;

    if (originCountry) {
      subdivisionLabel = getVerificationSubdivisionLabel(originCountry);
      postalLabel = getVerificationPostalLabel(originCountry);
    }

    return { originCountry, postalLabel, subdivisionLabel, enableIssuingAuthority }
  },
  getNetRate ( contractDetails, selectedCar, chargeType ) {
    return (
      _.get(contractDetails, 'contract_type') === PRICING.CONTRACT_CORPORATE &&
      _.get(selectedCar, `vehicleRates.${chargeType}.price_summary.estimated_total_view.amount`) === PRICING.VALUE_ZERO
    );
  },
  getPaidAmountCurrency ( paymentInformation ) {
    const paidAmount = _.get(paymentInformation, 'amount') || false;
    return !!paidAmount && PricingController.getSplitCurrencyAmount(paidAmount);
  },
  /**
   * corporate only?
   * @todo : pick this apart and do someething better;
   * @param  {[type]}  corporateSession           [description]
   * @param  {[type]}  contractDetails            [description]
   * @param  {Boolean} hasDifferentPaymentAndView [description]
   * @param  {[type]}  amount                     [description]
   * @param  {[type]}  billingAuthorized          [description]
   * @param  {[type]}  purpose                    [description]
   * @return {[type]}                             [description]
   */
  getSubLabel ( corporateSession, contractDetails, hasDifferentPaymentAndView, amount, billingAuthorized, purpose ) {
    const chargedOnPickUp = <span>{i18n('expedited_00178')}<strong> {amount} </strong>{i18n('expedited_0018c')}</span>;
    const estimatedTotal = <span>{i18n('resflowreview_0110')}<strong> {amount} </strong></span>;
    const paymentToBeDetermined = i18n('resflowcorporate_9050');
    const accountBilled = (accountName) => i18n('resflowcorporate_0048', {accountName});
    let subLabel = null;
    if (corporateSession) {
      if (_.has(contractDetails, 'contract_has_additional_benefits') && contractDetails.contract_has_additional_benefits === false) {
        if (corporateSession.contract_billing_account) {
          subLabel = accountBilled(corporateSession.contract_name);
        } else if (hasDifferentPaymentAndView) {
          subLabel = estimatedTotal;
        } else {
          subLabel = chargedOnPickUp;
        }
      } else if (corporateSession.contract_accepts_billing) {
        if (corporateSession.contract_billing_account) {
          if (billingAuthorized === 'true' && purpose === PRICING.PURPOSE_BUSINESS) {
            subLabel = accountBilled(corporateSession.contract_name);
          } else if (billingAuthorized === 'false' || purpose === PRICING.PURPOSE_LEISURE) {
            if (hasDifferentPaymentAndView) {
              subLabel = estimatedTotal;
            } else {
              subLabel = chargedOnPickUp;
            }
          } else {
            subLabel = paymentToBeDetermined;
          }
        } else if (billingAuthorized === 'true' && purpose === PRICING.PURPOSE_BUSINESS) {
          //subLabel = enterprise.i18nReservation.resflowcorporate_0058;
          subLabel = chargedOnPickUp;
        } else if (billingAuthorized === 'false' || purpose === PRICING.PURPOSE_LEISURE) {
          if (hasDifferentPaymentAndView) {
            subLabel = estimatedTotal;
          } else {
            subLabel = chargedOnPickUp;
          }
        } else {
          subLabel = paymentToBeDetermined;
        }
      } else if (hasDifferentPaymentAndView) {
        subLabel = estimatedTotal;
      } else {
        subLabel = chargedOnPickUp;
      }
    }
    return subLabel;
  },
  getSummaryText ( prepay, modify, amount, hasDifferentPaymentAndView ) {
    return prepay ? this.formatSummaryMessage(modify, amount) :
      hasDifferentPaymentAndView ? <span><span>{i18n('resflowreview_0110')}</span><strong> {amount} </strong></span> :
        <span>{i18n('expedited_00178')}<strong> {amount} </strong>{i18n('expedited_0018c')}</span>;
  },
  getTripPurposeCondition ( modify, contractDetails, deliveryAllowed, collectionAllowed, additionalInformationPostRate ) {
    const billing = this.showBilling( contractDetails );
    const contractHasBenefit = _.has(contractDetails, 'contract_has_additional_benefits') && contractDetails.contract_has_additional_benefits;
    const additionalInfo = additionalInformationPostRate.length > 0;
    return !modify && contractHasBenefit && contractDetails && (deliveryAllowed || collectionAllowed || billing || additionalInfo);
  },
  getUpgradeMessage ( upgrades ) {
    const nextUpgrade = upgrades.name;
    const differenceAmount = _.get(upgrades, 'priceDifferences.UPGRADE.difference_amount_view.format') || '';
    const upgradeAmount = differenceAmount.replace(' ', '') || i18n('resflowextras_0020');
    return i18n('resflowreview_0321', {nextUpgrade, upgradeAmount});
  },
  getWarningMessages ( personal, sessionPickupLocation, prepay, verification ) {
    let warningMessages = [];
    if (!personal.firstName || !personal.lastName || !personal.email || !personal.phoneNumber) {
      warningMessages.push({
        defaultMessage: i18n('expedited_0052')
      });
    }
    if (!!sessionPickupLocation.multi_terminal && personal.airlineCode !== false) {
      warningMessages.push({
        defaultMessage: (i18n('multipleterminals_0003') || 'Please provide an airline name so we know which terminal you will be coming from')
      });
    }
    if (prepay && !verification.prepayChecked) {
      warningMessages.push({
        defaultMessage: (i18n('prepay_0068') || 'You must accept the Prepay terms and conditions before submission')
      });
    }
    return warningMessages;
  },
  onNoFlight () {
    VerificationActions.setPersonalFields('airlineCode', 'WALK IN');
  },
  onNotListed () {
    VerificationActions.setPersonalFields('airlineCode', 'OTHER');
  },
  onAddFlight () {
    VerificationActions.setPersonalFields('airlineCode', null);
  },
  maskedDateExpression () {
    return enterprise.settings.regularex.maskedDate;
  },
  requireAuthentication ( deepLink, loggedIn ) {
    let requireAuth = false;

    if (!loggedIn && deepLink && deepLink.deepLinkFlow && deepLink.error) {
      for (let i = 0; i < deepLink.messages.length; i++) {
        let code = deepLink.messages[i].code;
        if (code === 'CROS_RES_LOGIN_FOR_FULL_ACCESS') {
          requireAuth = true;
        }
      }
    }
    return {
      requireAuth
    };
  },
  setLocationSearchType ( type ) {
    VerificationActions.setLocationSearchType( type );
  },
  setPriceLineItemRowName (desc, lowerCase, isReviewPage, countryCode) {
    return ReservationFlowModelController.setPriceLineItemRowName(desc, lowerCase, isReviewPage, countryCode);
  },
  setPriceLineItemMidCol (midCol, item) {
    return ReservationFlowModelController.setPriceLineItemMidCol(midCol, item);
  },
  setPriceLineItemCost (item) {
    return ReservationFlowModelController.setPriceLineItemCost(item);
  },
  showPolicy ( code, type ) {
    VerificationActions.showPolicy( code, type );
  },
  setErrorActions ( messages, type ) {
    ErrorActions.setErrorsForComponent({
      messages: messages
    }, type);
  },
  /**
   * Sets the personal fields.
   * @memberOf VerificationController
   * @function setPersonalFields
   * @param      {string}  variable  The property
   * @param      {*}  value     The value bound for the property
   * @todo unless this is really being done one field at a time, we should be using a factory
   */
  setPersonalFields ( variable, value ) {
    logger.warn('setPersonalFields(variable, value)', variable, `'${value}'`);
    let val = FedexController.filterReusedData(variable, value);
    logger.warn('....after FedexController.filterReusedData()', val);
    VerificationActions.setPersonalFields( variable, val );
  },
  setRequestPromotions ( checked ) {
    VerificationActions.setRequestPromotions(checked);
  },
  setPrivacyChecked ( bool ) {
    VerificationActions.setPrivacyChecked( bool );
  },
  setPrepayChecked ( bool ) {
    VerificationActions.setPrepayChecked( bool );
  },
  setVerificationErrors(errors) {
    VerificationActions.setVerificationErrors(errors);
  },
  setOverallPolicy (bool) {
    VerificationActions.setOverallPolicy(bool);
  },
  setEmailPreference (profile, expedited) {
    let checked = (
      profile && profile.preference ?
        profile.preference.email_preference.special_offers :
        expedited.countryIssue.default_email_opt_in
    );
    if (checked === false) { //Passing null if by default the request promotions is unchecked
      checked = null;
    }
    return checked;
  },
  shouldShowPromotions ( profile, contract ) {
    return !_.get(profile, 'preference.email_preference.special_offers') &&
      this.hasContractWithPromotionsEnabled( contract ) &&
      !enterprise.hideEmailExtras;
  },
  skipToDetails (e) {
    e.preventDefault();
    let foundEl = FocusManager.focusByFirstFoundElement('#focus-required-text-label, #focus-pexpedited-banner-login');
    if (foundEl === null) {
      FocusManager.byEnabledElement(document.getElementById('personal-information').getElementsByTagName('input'))
    }
  },
  togglePrepayTerms ( bool ) {
    ModifyActions.togglePrepayTerms( bool );
  },
  toggleInflightModify ( bool ) {
    logger.warn('VerificationController.toggleInflightModify(bool)', bool);
    ReservationActions.toggleInflightModify( bool );
  }
} // Components

/**
 * @namespace Modals
 * @type {Object}
 */
const Modals = {
  closeConflictAccountModal() {
    VerificationActions.setConflictAccountModal( false );
  },
  closeKeyFactsModal( cursor, bool ) {
    VerificationActions.setModalCursor( cursor, bool );
  },
  callSetModal (type, event) {
    logger.warn('callSetModal(type)', type, event);
    VerificationActions.setModal( type );
  },
  setCannotModifyModal ( bool ) {
    VerificationActions.setCannotModifyModal( bool );
  },
  //all of these functions really could be one function that is passed which modal to set 'business layer'
  setLockedCIDModal( bool ) {
    CorporateActions.setLockedCIDModal( bool );
  },
  showPolicyModal ( type ) {
    VerificationActions.showPolicyModal( type )
  },
  setPolicyModal ( bool ) {
    VerificationActions.setPolicyModal(bool);
  },
  setReservationPolicyModalContent ( item ) {
    VerificationActions.setReservationPolicyModalContent(item);
  },
  setBasicPrepayModal () {
    VerificationActions.setBasicPrepayModal();
  },
  setPrepayModal(state) {
    VerificationActions.setPrepayModal(state);
  },
  learnMore (e) {
    e.preventDefault();
    Storage.SessionStorage.set('scrollOffset', Scroll.getTopScroll());
    VerificationActions.setDestinationPriceInfoModal(true);
  },
  cleanupScrollOffset() {
    Scroll.scrollToOffset(Storage.SessionStorage.get('scrollOffset'));
    Storage.SessionStorage.remove('scrollOffset');
  },
  destinationScrollReset () {
    Scroll.scrollToOffset(Storage.SessionStorage.get('scrollOffset'));
    Storage.SessionStorage.remove('scrollOffset');
  },
  setDestinationPriceInfoModal(bool) {
    VerificationActions.setDestinationPriceInfoModal(bool);
  },
  onSurcharge () {
    Storage.SessionStorage.set('scrollOffset', Scroll.getTopScroll());
    VerificationActions.setModal('taxesFees');
  },
  callCannotModify() {
    VerificationActions.setCannotModifyModal(true);
  },
  confirmConflictAccount() {
    VerificationController.callLinkAccount(true);
    VerificationActions.setAssociateAccountStatus('cleared');
    VerificationController.closeConflictAccountModal();
  }
} // Modals

const VerificationController = {
  ...ServiceData,
  ...Rules,
  ...Components,
  ...Modals,

  /**
   * Called by RouterController when commit step loads
   * @memberOf VerificationController
   */
  routeSetup() {
    let session = ReservationActions.getSession();
    // @todo use profile not session ReservationActions.getProfile()
    if (session.profile || session.commitRequest) {
      VerificationActions.setPersonalInformation();
      VerificationActions.setOtherInformation();
    }
  }

};

export default VerificationController;
