import CarSelectService from '../services/CarSelectService';

import { CarFactory,
         CarDetailsFactory } from '../factories';

import CarSelectActions from '../actions/CarSelectActions';
import RedemptionActions from '../actions/RedemptionActions';
import CorporateActions from '../actions/CorporateActions';
import RedirectActions from '../actions/RedirectActions';
import ReservationActions from '../actions/ReservationActions';
import ModifyActions from '../actions/ModifyActions';

import PricingController from './PricingController';
import SessionController from './SessionController';

import { debug } from '../utilities/util-debug';
import { PRICING, VEHICLES } from '../constants';
import { exists, not, and } from '../utilities/util-predicates';
import { partition, flatPartition } from '../utilities/util-array';
import { trimNumber } from '../utilities/util-strings';

const { isCurrencyPayType,
        isRedemptionPayType,
        isPrepayPayType } = PricingController;

const rateTypeLabels = {
  'HOURLY'      : i18n('resflowcarselect_0011'),
  'DAILY'       : i18n('resflowcarselect_0012'),
  'DAILY_PROMO' : i18n('resflowcarselect_0012'),
  'WEEKLY'      : i18n('resflowcarselect_0013'),
  'MONTHLY'     : i18n('resflowcarselect_0014')
};

const rateQuantitiesLabels = {
  'HOURLY'  : (quantity) => i18n('resflowcarselect_0067', {'number': quantity}),
  'DAILY'   : (quantity) => i18n('resflowcarselect_0068', {'number': quantity}),
  'WEEKLY'  : (quantity) => i18n('resflowcarselect_0069', {'number': quantity}),
  'MONTHLY' : (quantity) => i18n('resflowcarselect_0070', {'number': quantity})
};

/************************
 * SERVICE INTERACTIONS *
 ************************/
const ServiceData = {
  retrieveVehicles () {
    const { isAvailableAtPromotionOrContract } = CarSelectController;
    CarSelectService.getVehicles()
                    .then( availableVehicles => availableVehicles.map(CarFactory.toState))
                    .then( availableVehicles => {
                      const promotionCars = availableVehicles.filter(isAvailableAtPromotionOrContract);
                      CorporateActions.setPromoCodeApplicable(promotionCars.length > 0);
                      CarSelectController.setAvailableVehicles(availableVehicles);
                      CarSelectActions.carSelectDataReady(true);
                    })
                    .catch( err => console.error('CarSelectController.retrieveVehicles()catch()', err) );
  },
  whenCarHasDetails (car) {
    if (car.details) {
      return Promise.resolve(car);
    }
    return CarSelectService.getVehicleDetails(car)
                           .then( CarDetailsFactory.toState )
                           .then( details => {
                             CarSelectActions.setVehicleDetails(details, car.code);
                             return CarSelectActions.getVehicleByCode(car.code);
                           })
                           .catch( err => console.error('CarSelectController.whenCarHasDetails().catch()', err) );
  },
  whenAvailableUpgradesRetrieved () {
    return CarSelectService.getAvailableUpgrades()
                           .then( upgrades => upgrades.map(CarDetailsFactory.toState))
                           .then( upgrades => CarSelectController.handleUpgrades(upgrades))
                           .catch( err => console.error('CarSelectController.whenAvailableUpgradesRetrieved().catch()', err) );
  },
  whenUpgradedVehicle (carCode) {
    return CarSelectService.upgradeVehicle(carCode)
                           .then(() => {
                             SessionController.callGetSession()
                                              .then(() => {
                                                CarSelectActions.setAvailableUpgrades([]);
                                                CarSelectActions.setUpgradedStatus(true);
                                              });
                           })
                           .catch( err => console.error('CarSelectController.whenUpgradedVehicle().catch()', err) );
  },
  whenRedemptionVehicleIsCleared () {
    return CarSelectService.clearRedemptionVehicle()
                           // @todo - GMA should return an object with the same session structure
                           //         from current-session, so then we can pass it to SessionFactory here
                           //.then((session) => {
                           //  CarSelectActions.clearCarStep();
                           //  ReservationActions.setReservationSession(session);
                           //})
                           .then(() => CarSelectActions.clearCarStep())
                           .then(() => SessionController.callGetSession())
                           .catch( err => console.error('CarSelectController.whenRedemptionVehicleIsCleared().catch()', err) );
  },
  whenRedemptionVehicleIsSelected (days) {
    const carClass = CarSelectActions.getSelectedCarCode() ||
                     CarSelectActions.getSessionSelectedCar().code;
    days = days || RedemptionActions.getDays();

    RedemptionActions.toggleRedemptionModalLoading(true);
    RedemptionActions.toggleRedemptionModal(true);
    CarSelectService.selectRedemptionVehicle({carClass, days})
                    // @todo - GMA should return an object with the same session structure
                    //         from current-session, so then we can pass it to SessionFactory here
                    //.then((session) => ReservationActions.setReservationSession(session))
                    .then(() => SessionController.callGetSession())
                    .catch(() => RedemptionActions.toggleRedemptionModal(false))
                    .then(() => RedemptionActions.toggleRedemptionModalLoading(false))
                    .catch( err => console.error('CarSelectController.whenRedemptionVehicleIsSelected().catch()', err) );
  },
  whenVehicleIsSelected (payType) {
    const { whenRedemptionVehicleIsSelected } = CarSelectController;
    const carClass = CarSelectActions.getSelectedCarCode();
    const paymentType = payType || CarSelectActions.getPayType();
    const isPrepay = isPrepayPayType(paymentType);

    if (isRedemptionPayType(paymentType)) {
      return whenRedemptionVehicleIsSelected();
    }

    ReservationActions.setLoadingClassName('loading');
    return CarSelectService.selectVehicle({carClass, isPrepay})
                           .then(() => ReservationActions.setLoadingClassName(null))
                           .catch( err => console.error('CarSelectController.whenVehicleIsSelected().catch()', err) );
  }
};

/************************
 * BUSINESS MODEL RULES *
 ************************/
const Rules = {
  isPrepayAllowed () {
    return !enterprise.hidePrepay // ECR-11783
        && !CorporateActions.getContractDetails(); // ECR-10703
  },
  isPrepayAllowedForCar (car) {
    return CarSelectController.isPrepayAllowed()
        && car.charges.PREPAY;
  },
  isNAPrepayAllowed () {
    return CarSelectController.isPrepayAllowed()
        && enterprise.prepayType === 'noModal';
  },
  getPayTypeForCar (car) {
    const {isPrepayAllowedForCar} = CarSelectController;
    return isPrepayAllowedForCar(car) ? PRICING.PREPAY : PRICING.PAYLATER;
  },
  isVan (car) {
    return _.get(car, 'filters.CLASS.filter_code') === '500';
  },
  isTruck (car) {
    return !!car.truckUrl;
  },
  isSoldOut (car) {
    return car.status === VEHICLES.STATUS.SOLD_OUT;
  },
  isRestricted (car) {
    return VEHICLES.RESTRICTED_STATUSES.includes(car.status);
  },
  isOnRequest (car) {
    return VEHICLES.ON_REQUEST_STATUSES.includes(car.status);
  },
  isUnavailable (car) {
    const {isSoldOut, isRestricted} = CarSelectController;
    return isSoldOut(car)
        || isRestricted(car);
  },
  isOnRequestPrepay (payType, car) {
    const { isOnRequest } = CarSelectController;
    return isOnRequest(car)
        && isPrepayPayType(payType);
  },
  hasUncertainAvailability (payType, car) {
    const {isRestricted, isOnRequestPrepay} = CarSelectController;
    return isRestricted(car)
        || isOnRequestPrepay(payType, car);
  },
  isAtRetail (car) {
    return VEHICLES.AT_RETAIL_STATUSES.includes(car.status);
  },
  isAtContractRate (car) {
    return VEHICLES.AT_CONTRACT_RATE_STATUSES.includes(car.status);
  },
  isAtPromotionalRate (car) {
    return VEHICLES.AT_PROMOTIONAL_RATE_STATUSES.includes(car.status);
  },
  isAvailable (car) {
    return VEHICLES.AVAILABLE_STATUSES.includes(car.status);
  },
  isAvailableAtRetail (car) {
    const {isUnavailable, isAtRetail} = CarSelectController;
    return !isUnavailable(car)
        && isAtRetail(car)
  },
  isAvailableAtPromotionOrContract (car) {
    return [ VEHICLES.STATUS.AVAILABLE_AT_PROMOTIONAL_RATE,
             VEHICLES.STATUS.AVAILABLE_AT_CONTRACT_RATE
           ].includes(car.status);
  },
  isPreferred (car) {
    return car.isPreferred;
  },
  hasAnyCurrencyCharge (car) {
    return Object.keys(car.charges || {})
                 .some(isCurrencyPayType);
  },
  getTransmission (car) {
    // @todo: ECR-13130 covering for bad data. Do this better next time
    // also see Car.js
    const transmission = _.get(car, 'filters.TRANSMISSION.description');
    return (transmission === 'null') ? '' : transmission;
  },
  getPassengersCapacity (car) {
    // @todo: ECR-13130 covering for bad data. Do this better next time
    // also see Car.js
    const passengers = _.get(car, 'filters.PASSENGERS.description');
    return (passengers === 'null') ? '' : passengers;
  },
  getCarClass (car) {
    // @todo: ECR-13130 covering for bad data. Do this better next time
    // also see Car.js
    const carClass = _.get(car, 'filters.CLASS.description');
    return (carClass === 'null') ? '' : carClass;
  },
  hasDetails (car) {
    return _.has(car, 'details');
  },
  hasPriceSummary (car, chargeType) {
    return _.has(car, `details.vehicleRates.${chargeType}.price_summary`)
  },
  getPriceSummary (car, chargeType) {
    return _.get(car, `details.vehicleRates.${chargeType}.price_summary`) || {}
  },
  hasRates (charge) {
    return _.get(charge, 'rates.length') > 0;
  }
};

/**************************
 * COMPONENT INTERACTIONS *
 **************************/
const Components = {
  setAvailableVehicles (availableVehicles = []) {
    if (availableVehicles.length > 0) {
      const {filters, chargeTypes} = availableVehicles.reduce(({filters, chargeTypes}, car) => ({
        filters: _.merge(filters, CarSelectController.getCarFilters(car)),
        chargeTypes: _.merge(chargeTypes, CarSelectController.getCarChargeTypes(car))
      }), {filters:{}, chargeTypes:{}});

      // Set paytypes and filters
      CarSelectController.setAvailablePayTypes(chargeTypes);
      CarSelectActions.setAvailableFilters(filters);

      // set available vehicles
      CarSelectActions.setAvailableVehicles(availableVehicles);
      CarSelectActions.setFilteredVehicles(availableVehicles);

      CarSelectController.setDefaultPayType(chargeTypes);
      CarSelectController.setDefaultFilters(filters);
    }
  },
  handleUpgrades (upgrades) {
    const upgraded = CarSelectActions.getUpgradedStatus();
    if (upgrades.length > 0 && upgrades[0].code) {
      CarSelectActions.setAvailableUpgrades(upgrades);
      CarSelectActions.setUpgradedStatus(false);
    } else if(upgrades.length === 0 && upgraded){
      CarSelectActions.setUpgradedStatus(false);
    }
  },
  getCarFilters (car) {
    let filters = Object.assign({}, ...VEHICLES.FILTERS.map( f => ({[f]: {}}) ));
    if (car.filters) {
      VEHICLES.FILTERS.filter( f => car.filters[f] && car.filters[f].description !== 'null')
                      .forEach( f => filters[f][car.filters[f].description] = true)
    }
    return filters;
  },
  getCarChargeTypes (car) {
    return Object.keys(car.charges)
                 .reduce((obj, chargeType) => Object.assign(obj, {[chargeType]: true}), {});
  },
  setDefaultPayType (availablePayTypes) {
    const isModify = ReservationActions.getIsModify();
    const chargeType = ReservationActions.getChargeType();

    // CHANGE PAYTYPE HERE FOR MODIFY
    if (isModify && chargeType) {
      CarSelectActions.updatePayType(chargeType);
    } else if (enterprise.defaultPaymentOption) {
      if (enterprise.defaultPaymentOption == 'paynow') {
        if (availablePayTypes.PREPAY) {
          CarSelectActions.updatePayType(PRICING.PREPAY);
        } else {
          CarSelectActions.updatePayType(PRICING.PAYLATER);
        }
      }
      // If only one pay type available, set default pay type to that option
    } else if (Object.keys(availablePayTypes).length == 1) {
      let payTypes = Object.keys(availablePayTypes);
      CarSelectActions.updatePayType(payTypes[0]);
    }
  },
  setDefaultFilters (filters) {
    // Check if there are any preselected cars
    let preSelectedName = CarSelectActions.getPreSelectedCarClass();

    if (preSelectedName) {
      const isPreSelectedClass = filters[VEHICLES.FILTER.CATEGORY].hasOwnProperty(preSelectedName);
      if (isPreSelectedClass) {
        CarSelectActions.setMetaPreSelectedCarClass(preSelectedName);
        CarSelectActions.setPreSelectedCarClass(null);
        CarSelectActions.addToFilter(VEHICLES.FILTER.CATEGORY, preSelectedName);
        CarSelectController.updateFilteredCars();
      } else {
        CarSelectController.setPreSelectedCarFilter(preSelectedName);
      }
    } else if (enterprise.transmission) {
      CarSelectController.setPreSelectedCarTransmission(filters);
    }
  },
  setPreSelectedCarFilter (preSelectedName) {
    const { isSoldOut } = CarSelectController;
    // Need to redefine the list of filters
    const filteredCars = CarSelectActions.getAvailableVehicles()
                                         .filter(car => car.name === preSelectedName);
    const hasAvailableFilteredCars = filteredCars.filter(not(isSoldOut)).length > 0;

    if (hasAvailableFilteredCars) {
      // Don't mess with the filteredVehicles since it's used elsewhere
      CarSelectActions.setFilteredVehicles(filteredCars);
    } else {
      // ECR-10656 -- Reset filter and show special message (see PreFilteredBand.js)
      CarSelectActions.setBandState('none');
    }
  },
  setPreSelectedCarTransmission (filters) {
    const { isSoldOut, clearAllFilters } = CarSelectController;
    const transmissionFilter = filters[VEHICLES.FILTER.TRANSMISSION];
    let transmissionType = enterprise.transmission;

    // @todo this should not be here
    let manualMap = ['Schaltgetriebe', 'Manuelle', 'Schalter'];
    if (enterprise.transmission == 'Manual') {
      transmissionType = manualMap.find( manual => transmissionFilter.hasOwnProperty(manual) ) || transmissionType;
    }

    if (transmissionFilter.hasOwnProperty(transmissionType) &&
        Object.keys(transmissionFilter).length > 1) {
      CarSelectActions.setFilter(VEHICLES.FILTER.TRANSMISSION, [transmissionType]);
      CarSelectController.updateFilteredCars();

      let filteredCars = CarSelectActions.getFilteredVehicles() || [];

      const hasAvailableFilteredCars = filteredCars.filter(not(isSoldOut)).length > 0;

      if (hasAvailableFilteredCars) {
        CarSelectActions.setTransmissionBandState('default');
      } else {
        clearAllFilters();
        CarSelectActions.setTransmissionBandState('empty');
      }
    } else {
      CarSelectActions.setTransmissionBandState(null);
    }
  },
  setAvailablePayTypes (availablePayTypes) {
    const { isNAPrepayAllowed } = CarSelectController;
    const isNAPrepayEnabled = isNAPrepayAllowed() && availablePayTypes.PREPAY;
    CarSelectActions.setAvailablePayTypes(availablePayTypes);
    CarSelectActions.setIsNAPrepayEnabled(isNAPrepayEnabled);
  },
  partitionVehiclesByAvailability (vehicles) {
    const {isSoldOut} = CarSelectController;
    return partition(not(isSoldOut), vehicles);
  },
  sortAvailableVehicles (availableVehicles, chargeType, deepLinkReservation, selectedCar) {
    const {isAvailable, isVan, isPreferred, compareCarsByTotalPrice} = CarSelectController;
    let vehicles = flatPartition(isAvailable, flatPartition(not(isVan), availableVehicles));
    // Float up preferred cars
    let [preferredVehicles, otherVehicles] = partition(isPreferred, vehicles);
    vehicles = [...preferredVehicles.sort(compareCarsByTotalPrice(chargeType)), ...otherVehicles];
    // Float up a car prepopped via deeplink
    if (_.get(deepLinkReservation, 'stepStatus.CAR_SELECT') === 'PREPOPPED') {
      const floatedCarCode = _.get(deepLinkReservation, 'reservation.car_class_details.code');
      const floatedCarCodeIndex = vehicles.findIndex(el => el.code === floatedCarCode);
      if (floatedCarCodeIndex > -1 && !selectedCar) {
        vehicles.splice(0, 0, vehicles.splice(floatedCarCodeIndex, 1)[0]);
        vehicles[0].prepopped = true;
      }
    }
    return vehicles;
  },
  sortUnavailableVehicles (unavailableVehicles) {
    const {isVan} = CarSelectController;
    return flatPartition(not(isVan), unavailableVehicles);
  },
  compareCarsByTotalPrice (chargeType) {
    const getTotalPrice = (car) => _.get(car, `charges.${chargeType}.total_price_payment.amount`, 0);
    return (a, b) => getTotalPrice(a) - getTotalPrice(b);
  },
  hasPreferredVehicles (vehicles) {
    const {isPreferred} = CarSelectController;
    return vehicles.some(isPreferred);
  },
  getDisplayingVehiclesAmount ({filteredVehicles, availableVehicles}) {
    return (filteredVehicles || availableVehicles || []).filter(exists).length;
  },
  hasAnyFilterSelected (selectedFilters) {
    return Object.values(selectedFilters)
                 .some(filter => filter.length > 0);
  },
  clearAllFilters () {
    const clearedFilters = Object.assign({}, ...VEHICLES.FILTERS.map( f => ({[f]: []})) );
    CarSelectActions.setAllFilters(clearedFilters);
    CarSelectController.updateFilteredCars();
  },
  updateFilteredCars () {
    const { getCarClass, getPassengersCapacity, getTransmission } = CarSelectController;
    // Need to redefine the list of filters
    let cars = CarSelectActions.getAvailableVehicles();
    let filters = CarSelectActions.getSelectedFilters();

    const hasSelectedCategory = car => filters[VEHICLES.FILTER.CATEGORY].length === 0
                                    || filters[VEHICLES.FILTER.CATEGORY].includes(getCarClass(car));
    const hasSelectedPassengerCapacity = car => filters[VEHICLES.FILTER.PASSENGERS].length === 0
                                             || filters[VEHICLES.FILTER.PASSENGERS].includes(getPassengersCapacity(car));
    const hasSelectedTransmission = car => filters[VEHICLES.FILTER.TRANSMISSION].length === 0
                                        || getTransmission(car) === null
                                        || filters[VEHICLES.FILTER.TRANSMISSION].includes(getTransmission(car));

    const filteredCars = cars.filter(and(hasSelectedCategory,
                                         hasSelectedPassengerCapacity,
                                         hasSelectedTransmission));

    CarSelectActions.setTransmissionBandState(null);
    CarSelectActions.setFilteredVehicles(filteredCars);
  },
  toggleFilterOption (name, value, isChecked) {
    const filterAction = isChecked ? 'addToFilter' : 'removeFromFilter';
    CarSelectActions[filterAction](name, value);
    CarSelectController.updateFilteredCars();
  },
  getAvailablePaytypes (availablePayTypes, hasVehicles) {
    const additionalPayTypeConditions = {
      [PRICING.PREPAY]: CarSelectController.isPrepayAllowed(),
      [PRICING.REDEMPTION]: !enterprise.hideRedemption
    }
    const isPayTypeAvailable = (type) => (availablePayTypes[type]
                                      && (additionalPayTypeConditions[type] || true));

    if (hasVehicles) {
      const payTypes = Object.keys(availablePayTypes)
                             .reduce((obj, type) => (
                               Object.assign(obj, {[type]: isPayTypeAvailable(type)})
                             ), {})
      return payTypes;
    }

    return {};
  },
  getOfficeLocationNumber (locationPhones = []) {
    const officePhone = locationPhones.find(phone => phone.phone_type === 'OFFICE') || {}
    return officePhone.phone_number || '';
  },
  getPhoneNumber (car, locationPhones, isFedexReservation) {
    const {getOfficeLocationNumber} = CarSelectController;
    if (isFedexReservation) { // fedex does the opposite of everyone else ECR-10671
      return getOfficeLocationNumber(locationPhones)
          || car.availabilityPhoneNumber;
    }
    return car.availabilityPhoneNumber
        || getOfficeLocationNumber(locationPhones);
  },
  getTransmissionSetting(transmissionBandState, settings){
    const transmission = enterprise.transmission.toLowerCase();
    const shouldShowTransmissionBand = transmission !== '' && !!transmissionBandState;
    if (!shouldShowTransmissionBand) {
      return false;
    }
    return settings[transmissionBandState]
        || settings[transmission]
        || settings.manual;
  },
  getPreFilterSettingRules(settings, bandState, shouldHide){
    let setting = settings.default;

    if (settings[bandState]) {
      setting = settings[bandState];
    } else if (bandState == 'cleared' || shouldHide) {
      setting = false;
    }

    return setting;
  },
  getMetaFilteredSetting(settings, bandState, selectedFilters){
    const selectedFiltersCount = Array.concat(...Object.values(selectedFilters))
                                      .filter(filterItem => filterItem)
                                      .length;
    const hasMoreThanOneFilter = (selectedFiltersCount > 1);
    return CarSelectController.getPreFilterSettingRules(settings, bandState, hasMoreThanOneFilter);
  },
  getPreFilteredSetting(settings, bandState, selectedFilters){
    const hasFilters = Object.values(selectedFilters)
                             .some(f => f.length > 0);
    return CarSelectController.getPreFilterSettingRules(settings, bandState, hasFilters);
  },
  vanModalControls: {
    close () {
      CarSelectActions.setVanModal(false);
    },
    confirm () {
      CarSelectController.confirmSelection();
      CarSelectActions.setVanModalConfirm(true);
      CarSelectController.vanModalControls.close();
    }
  },
  limitedVehicleModalControls: {
    close () {
      CarSelectActions.toggleRequestModal(false);
    },
    confirm () {
      CarSelectActions.setRequestModalConfirm(true);
      CarSelectController.confirmSelection();
      CarSelectController.limitedVehicleModalControls.close();
    }
  },
  redemptionModalControls: {
    confirm () {
      CarSelectController.confirmFromRedemptionModal();
      RedemptionActions.toggleRedemptionModal(false);
    },
    cancel () {
      // @todo - we don't seem to unset the modal loading... check that!
      RedemptionActions.toggleRedemptionModalLoading(true);
      CarSelectController.whenRedemptionVehicleIsCleared()
                         .then(() => RedemptionActions.toggleRedemptionModal(false))
    }
  },
  pointsToggle: {
    // @todo - this timeout logic seems to be related with the
    //         service layer (wait some time before making another request)...
    //         it should be handled by `CarSelectService`.
    createControls () {
      const time = 500;
      let timeout = null;
      const setDays = () => {
        clearTimeout(timeout);
        timeout = setTimeout(CarSelectController.whenRedemptionVehicleIsSelected, time);
      };
      const plus = () => {
        RedemptionActions.addDay();
        setDays();
      }
      const minus = () => {
        RedemptionActions.subtractDay();
        setDays();
      }
      return { setDays, plus, minus };
    }
  },
  confirmFromRedemptionModal () {
    if (ReservationActions.getCurrentHash() !== 'commit') {
      RedirectActions.goToReservationStep('extras');
    }
  },
  confirmSelection () {
    if (ModifyActions.isInflightModify()) {
      ModifyActions.toggleInflightModifyModal(true); //Which is in CarSelect.js
    } else {
      return CarSelectController.whenVehicleIsSelected();
    }
  },
  selectVehicle (car, chargeType, onSelecting = () => {}) {
    const {isOnRequest} = CarSelectController;
    CarSelectActions.selectVehicle(car.code, car.name, car);
    onSelecting();
    if (isOnRequest(car)) {
      CarSelectActions.setRequestModalOrigin(true, 'vehicle');
      return false;
    }
    CarSelectController.whenVehicleIsSelected(chargeType);
  },
  // @todo - take a look on that method and try to unify with `selectVehicle`
  onSelectVehicle (car, isNAPrepayEnabled, payType) {
    const { isPrepayAllowed, isOnRequest } = CarSelectController;
    const carClass = car.code;
    const carName = car.name;

    if (car.isTnCRequired && car.description) {
      CarSelectActions.selectVehicle(carClass, carName, car);
      CarSelectActions.setVanModal(true, car.description, payType); // Is in CarSelect.js
      return false;
    }

    if (isOnRequest(car)) {
      CarSelectActions.selectVehicle(carClass, carName, car);
      CarSelectActions.setRequestModalOrigin(true, 'vehicle');
      return false;
    }

    //TODO: Put Code Here To Show REDEMPTION Modal
    if (isRedemptionPayType(payType)) {
      CarSelectActions.selectVehicle(carClass, carName, car);
      CarSelectController.setDaysFromPreviousCar();
      RedemptionActions.setDays(car.redemptionDaysMax);
      CarSelectController.whenRedemptionVehicleIsSelected(car.redemptionDaysMax);
    } else if (isNAPrepayEnabled) {
      CarSelectActions.selectVehicle(carClass, carName, car);
      CarSelectController.whenVehicleIsSelected(payType);
    } else if (!isPrepayAllowed() || !car.charges.PREPAY) {
      CarSelectActions.selectVehicle(carClass, carName, car);
      CarSelectController.whenVehicleIsSelected(PRICING.PAYLATER);
    } else {
      CarSelectActions.preSelectVehicle(car);
      CarSelectActions.toggleVehiclePriceModal(true);
    }
  },
  onSelectingInVehiclePrice: CarSelectActions.toggleVehiclePriceModal.bind(null, false),
  onSelectingInRateComparison:CarSelectActions.toggleRateComparisonModal.bind(null, false),
  onConfirmingInNotAvailableModal: CarSelectActions.closeNotAvailableModal,
  setDaysFromPreviousCar () {
    const car = CarSelectActions.getSessionSelectedCar()
    if (exists(car)) {
      RedemptionActions.setPreviousDays(car.redemptionDaysCount);
    }
  },
  getPaymentItemPriceLabel (status, format) {
    return status === 'INCLUDED' ? i18n('reservationnav_0018')
                                 : format;
  },
  getPaymentTotalPriceLabel (isIncluded, format) {
    return isIncluded ? i18n('reservationnav_0018')
                      : format;
  },
  getRateTypeLabel (rateType) {
    return rateTypeLabels[rateType] || ''
  },
  getRateQuantitiesLabel (rateType, quantity) {
    return (rateQuantitiesLabels[rateType] || (() => ''))(quantity);
  },
  getChargeConfig (charge) {
    const {hasRates, getRateTypeLabel} = CarSelectController;
    const firstRate = _.get(charge, 'rates[0]') || {};
    const totalPrice = _.get(charge, 'total_price_view') || null;
    return {
      hasCharge       : !!charge,
      hasRates        : hasRates(charge),
      rateType        : getRateTypeLabel(firstRate.unit_rate_type),
      unitPrice       : _.get(firstRate, 'unit_amount_view.format') || '',
      totalPrice      : _.get(totalPrice, 'format') || '',
      totalPriceParts : totalPrice ? PricingController.getSplitCurrencyAmount(totalPrice) : null
    };
  },
  getCurrencyChargesConfig (car) {
    const {getChargeConfig} = CarSelectController;
    const configs = PRICING.CURRENCY_TYPES.map(chargeType => (
      {[chargeType] : getChargeConfig(car.charges[chargeType])}
    ));
    return Object.assign({}, ...configs);
  },
  getFeeItemSummaries (priceSummary = {}) {
    const { getPaymentItemPriceLabel } = CarSelectController;
    return (priceSummary.fee_line_items || []).map( (item, index) => ({
      code   : (item.description || index).toLowerCase(),
      header : (item.description || '').toLowerCase(),
      total  : getPaymentItemPriceLabel(item.status, item.total_amount_view.format)
    }));
  },
  getVehicleItemSummaries (priceSummary = {}) {
    const { getRateQuantitiesLabel, getPaymentItemPriceLabel } = CarSelectController;
    return (priceSummary.vehicle_line_items || []).map( item => ({
      code   : item.rate_type,
      header : getRateQuantitiesLabel(item.rate_type, trimNumber(item.rate_quantity)), // @todo - This SHOULD NOT be here...Ideally, we should create model classes for vehicle rates, price summary and payment line items and call `trimNumber` on the factory layer!
      total  : getPaymentItemPriceLabel(item.status, item.total_amount_view.format)
    }));
  },
  getPaymentItemSummaries (priceSummary = {}, paymentItemType) {
    const { getPaymentItemPriceLabel } = CarSelectController;
    return (priceSummary[paymentItemType] || []).map( item => ({
      code   : item.code,
      header : item.description,
      total  : getPaymentItemPriceLabel(item.status, item.total_amount_view.format)
    }));
  },
  getFeesTotal (priceSummary = {}) {
    const { getPaymentTotalPriceLabel } = CarSelectController;
    const totalTaxesAndFees = priceSummary.estimated_total_taxes_and_fees_view || {};
    // @todo - checkout if `estimated_total_taxes_and_fees_view` can have a status
    return getPaymentTotalPriceLabel(totalTaxesAndFees.amount == 0, (totalTaxesAndFees.format || ''))
  },
  getTotalPrice (priceSummary = {}) {
    return PricingController.getSafeSplitCurrencyAmount(priceSummary.estimated_total_view)
  },
  mergeSummaries (itemSummaries) {
    return Object.entries(itemSummaries)
                 .reduce( (summary, [charge, items]) => {
                   items.map(item => [item.code, { header: item.header, [charge]: item.total}])
                        .reduce( (summ, [key, value]) => {
                          summ[key] = Object.assign({}, summ[key], value);
                          return summ
                        }, summary);
                   return summary;
                 }, {});
  },
  getChargeSummaryConfigs (car, chargeType) {
    const { hasDetails, hasPriceSummary, getPriceSummary,
            getFeeItemSummaries, getVehicleItemSummaries,
            getPaymentItemSummaries, getFeesTotal, getTotalPrice} = CarSelectController;
    const priceSummary = getPriceSummary(car, chargeType);
    return {
      hasDetails   : hasDetails(car),
      hasSummary   : hasPriceSummary(car, chargeType),
      feeItems     : getFeeItemSummaries(priceSummary),
      vehicleItems : getVehicleItemSummaries(priceSummary),
      savingItems  : getPaymentItemSummaries(priceSummary, 'savings_line_items'),
      extraItems   : getPaymentItemSummaries(priceSummary, 'extra_line_items'),
      feesTotal    : getFeesTotal(priceSummary),
      totalPrice   : getTotalPrice(priceSummary)
    }
  },
  getCurrencyChargesSummaryConfigs (car) {
    const {getChargeSummaryConfigs, mergeSummaries} = Components;
    let allConfigs = PRICING.CURRENCY_TYPES.map(charge => [charge, getChargeSummaryConfigs(car, charge)])
                                           .reduce( (obj, [charge, configs]) => (
                                             _.merge(obj, ...Object.entries(configs).map(([key, value]) => ({[key]: {[charge]: value}})))
                                           ), {} );
    allConfigs.vehicleItems = mergeSummaries(allConfigs.vehicleItems);
    allConfigs.extraItems   = mergeSummaries(allConfigs.extraItems);
    allConfigs.savingItems  = mergeSummaries(allConfigs.savingItems);
    allConfigs.feeItems     = mergeSummaries(allConfigs.feeItems);
    return allConfigs;
  },
  getSavingsAmount (car, payType) {
    return _.get(car, `priceDifferences.${payType}.difference_amount_view.format`);
  },
  showTaxesAndFees (car) {
    CarSelectActions.setDetailsCar(car.code);
    CarSelectActions.toggleTaxesAndFeesModal(true);
  },
  showRateComparison (car) {
    CarSelectActions.setCarRateCompare(car);
    CarSelectActions.setDetailsCar(car.code);
    CarSelectActions.toggleRateComparisonModal(true);
  }
};

const CarSelectController = debug({
  isDebug: false,
  logger: {},
  name: 'CarSelectController',
  ...ServiceData,
  ...Rules,
  ...Components
});

module.exports = CarSelectController;
