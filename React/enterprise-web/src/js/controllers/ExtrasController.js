import ExtrasService from '../services/ExtrasService';
import CarSelectActions from '../actions/CarSelectActions';
import ExtrasActions from '../actions/ExtrasActions';
import SessionController from '../controllers/SessionController';
import PricingController from './PricingController';
import CarSelectController from './CarSelectController';

import SimpleExtraFactory from '../factories/SimpleExtraFactory';

import Images from '../utilities/util-images';
import { debug } from '../utilities/util-debug';
import { PAGEFLOW, PROFILE, PRICING, EXTRAS } from '../constants'

/************************
 * SERVICE INTERACTIONS *
 ************************/
const ServiceData = {
  submitExtras() {
    Promise
      .all(ServiceData.submit('update'))
      .then(() => ServiceData.submit('route'));

    Components.closeExtrasProtectionModal();
  },
  submit: function (action) {
    let equipment = ExtrasActions.get.cursorBySelect('equipmentExtras'),
      insurance = ExtrasActions.get.cursorBySelect('insuranceExtras'),
      modify = ExtrasActions.get.cursorBySelect('modify'),
      updateQueue = ExtrasActions.get.cursorBySelect('updateQueue'),
      vehicleRequest = ExtrasActions.get.cursorBySelect('requestModal', 'origin'),
      currentQueueLength = updateQueue.length,
      allExtras = equipment.concat(insurance),
      extrasReq = [],
      currentRequest = Components.currentExtrasRequest;

    ExtrasActions.set.blockSubmit(true);

    if (action === 'route') {
      let requestModal = ExtrasActions.get.cursorBySelect('requestModal')

      let onRequest = equipment.filter(function (extras) {
        if (extras) {
          return (extras.selected && (extras.allocation.indexOf('ON_REQUEST') > -1));
        }
      });

      onRequest = onRequest || insurance.filter(function (extras) {
        if (extras) {
          return (extras.selected && (extras.allocation.indexOf('ON_REQUEST') > -1));
        }
      });

      if (onRequest.length && !requestModal.confirm) {
        CarSelectActions.setRequestModalOrigin(true, 'vehicle_extras');
        ExtrasActions.set.blockSubmit(false);
        return false;
      }

      if (onRequest.length && vehicleRequest === 'vehicle') {
        CarSelectActions.setRequestModalOrigin(true, 'extras');
        ExtrasActions.set.blockSubmit(false);
        return false;
      }

      if (!modify) ExtrasActions.set.reservationStepCommit(true);

      ExtrasService.finishExtras();
    } else {
      ExtrasActions.push.updateQueue(currentQueueLength);
      for (let i = 0; i < allExtras.length; i++) {
        let extra = allExtras[i];

        if (extra.status !== PAGEFLOW.NOT_WEB_BOOKABLE) {
          extrasReq.push({
            'code': extra.code,
            'quantity': extra.selected_quantity
          });
        }
      }

      if (currentRequest.length) {
        let current =
          currentRequest[currentRequest.length - 1]
            .then(() => {
              currentRequest.shift();
              return ServiceData.selectCurrentExtras(extrasReq);
            });

        currentRequest.push(current);
      } else {
        currentRequest.push(ServiceData.selectCurrentExtras(extrasReq));
      }
    }

    return currentRequest;
  },
  selectCurrentExtras: function (currentExtra) {
    return ExtrasService
      .updateSelectExtras(currentExtra)
      .then(() => {
        // Get queue and its current length
        let updateQueue = ExtrasActions.get.updateQueue();
        let queueLength = updateQueue.length;

        // Splice item off queue, since our request was successful
        const valueToItemQueue = (updateQueue.splice(queueLength - 1, 1) && updateQueue);
        ExtrasActions.set.updateQueue(valueToItemQueue);

        // Get queue and its new length again to check against
        updateQueue = ExtrasActions.get.updateQueue();
        const newQueueLength = updateQueue.length;

        // If our queue is empty, its safe to proceed
        if (!newQueueLength) {
          SessionController
            .callGetSession(true, 'extras')
            .then(() => ExtrasActions.set.blockSubmit(false));
        }
      })
      .catch((err) => {
        ExtrasActions.set.blockSubmit(false);
        console.error('ExtrasController.selectCurrentExtras().catch()', err);
      });
  }
}

/************************
 * BUSINESS MODEL RULES *
 ************************/
const Rules = {
  augmentExtraWithFlags(extra) {
    extra.isIncluded = extra.status === PAGEFLOW.INCLUDED;
    extra.notWebBookable = extra.status === PAGEFLOW.NOT_WEB_BOOKABLE;
    extra.isMandatory = extra.status === PROFILE.LICENSE_ISSUE_MANDATORY;
  },
  isBookable(status) {
    return !(
      status === PAGEFLOW.INCLUDED ||
      status === PROFILE.LICENSE_ISSUE_MANDATORY ||
      status === PAGEFLOW.NOT_WEB_BOOKABLE
    );
  },
  getExtrasChargeType(chargeType) {
    return chargeType === PRICING.REDEMPTION ? PRICING.PAYLATER : chargeType;
  },
  isIncluded(extra){
    return (extra.status === EXTRAS.STATUS.INCLUDED)
  },
  isMandatory(extra){
    return (extra.status === EXTRAS.STATUS.MANDATORY);
  },
  buildExtrasByStatus(type, extras, includedExtras = [], mandatoryExtras = []) {
    extras.forEach(extra => {
      if (ExtrasController.isIncluded(extra)) {
        let included = SimpleExtraFactory.toState(type, extra);
        includedExtras.push(included);
      }

      if (ExtrasController.isMandatory(extra)) {
        let mandatory = SimpleExtraFactory.toState(type, extra);
        mandatoryExtras.push(mandatory);
      }
    });

    return {
      includedExtras: includedExtras,
      mandatoryExtras: mandatoryExtras
    }
  },
  rateTypeMap: {
    HOURLY: i18n('resflowcarselect_0071'),
    DAILY: i18n('resflowcarselect_0072'),
    WEEKLY: i18n('resflowcarselect_0073'),
    MONTHLY: i18n('resflowcarselect_0074'),
    RENTAL: i18n('resflowextras_9008')
  }
}

/**************************
 * COMPONENT INTERACTIONS *
 **************************/
const Components = {
  currentExtrasRequest: [],
  openExtraDetailsModal(extra) {
    ExtrasActions.set.highlightedExtra(extra);
    ExtrasActions.set.showExtraDetailsModal(true);
  },
  reviewButtonClicked_Handler(blockSubmit) {
    if (!blockSubmit && !Components.shouldRemindProtectionExtras()) {
      ServiceData.submitExtras();
    }
  },
  upgrade_Handler(code, currentCarClass) {
    CarSelectActions.setPreviousCarClass(currentCarClass);
    CarSelectController.whenUpgradedVehicle(code);
  },
  clearHighlightedExtra() {
    ExtrasActions.set.highlightedExtra(null);
  },
  openExclusionExtrasModal(code, event) {
    event.preventDefault();
    ExtrasActions.set.exclusionExtrasModal(true, code);
  },
  closeExclusionExtrasModal() {
    ExtrasActions.set.exclusionExtrasModal(false);
  },
  addRemoveExtraClicked_Handler(e, index, currentCursor, select) {
    e.preventDefault();
    let action = e.currentTarget.getAttribute('data-action');
    Components.addRemoveExtra(action, index, currentCursor, select);
  },
  upgradeBannerContent(nextUpgrade, chargeType, selectedCarClassDetails) {
    let imgPath = null;
    let upgradeText = null;
    let code = null;
    if (nextUpgrade && nextUpgrade.vehicleRates) {
      const upgradeAmount = _.get(nextUpgrade, 'priceDifferences.UPGRADE.difference_amount_view.format') || i18n('resflowextras_0020');
      const totalAmount = _.get(nextUpgrade, `vehicleRates.${chargeType}.price_summary.estimated_total_view.format`) || PricingController.getNetRate(true);
      upgradeText = i18n('resflowextras_0016', { nextUpgrade: nextUpgrade.name, upgradeAmount, totalAmount });
      imgPath = Images.getUrl(nextUpgrade.images.ThreeQuarter.path, 176, 'high');
      code = nextUpgrade.code;
    } else {
      imgPath = Images.getUrl(selectedCarClassDetails.images.ThreeQuarter.path, 176, 'high');
    }
    return {
      imgPath,
      upgradeText,
      code
    };
  },
  numericStepperClicked_Handler(e, index, currentCursor, maxQuantity) {
    const action = e.currentTarget.getAttribute('data-action');
    Components.updateExtraQuantity(action, index, currentCursor, maxQuantity);
  },
  addRemoveExtra (action, index, cursor, selected) {
    let currentExtra = ExtrasActions.set.selectCursorByIndex(cursor, index);
    let numSelected = currentExtra.get('selected_quantity') || 0;

    ExtrasActions.set.selectedExtra(currentExtra, selected);
    return Components.setExtraQuantity(action, currentExtra, numSelected);
  },
  updateExtraQuantity (action, index, cursor, maxQuantity) {
    const currentExtra = ExtrasActions.set.selectCursorByIndex(cursor, index);
    const selectedQuantity = currentExtra.get('selected_quantity');
    if (action === 'add') {
      if (selectedQuantity < maxQuantity) {
        Components.setExtraQuantity('add', currentExtra, selectedQuantity);
      }
    } else if (action === 'subtract') {
      if (selectedQuantity <= maxQuantity && selectedQuantity > 0) {
        Components.setExtraQuantity('remove', currentExtra, selectedQuantity);
      }
    }
  },
  setExtraQuantity (action, extra, numSelected) {
    const newVal = (action === 'add') ? numSelected + 1 : numSelected - 1;
    extra.set('selected_quantity', newVal);
    // If our quantity is zero after being set, mark this extra as de-selected on the UI
    if (extra.get('selected_quantity') === 0) ExtrasActions.set.selectedExtra(extra, false);
    // Make call to update extras
    return ServiceData.submit();
  },
  shouldRemindProtectionExtras: () => {
    const reqAtCounter = ExtrasActions.get.protectionsRequired();
    const insuranceExtras = ExtrasActions.get.insuranceExtras();
    const result = (reqAtCounter.length ? (
      insuranceExtras.some((extra) => (reqAtCounter.indexOf(extra.code) > -1 && !extra.selected && !extra.isIncluded))
    ) : false);
    ExtrasActions.set.showExtrasProtectionModal(result);
    return result;
  },
  closeExtrasProtectionModal: () => {
    ExtrasActions.set.showExtrasProtectionModal(false);
  },
  formatAvailableExtras: function(extras) {
    return extras.map(extra => {
      let isIncluded = !Rules.isBookable(extra.status);
      extra.isBookable = !isIncluded;

      if (extra.selected_quantity > 0 && !isIncluded) {
        extra.selected = true;
      } else {
        extra.selected = false;
      }

      Rules.augmentExtraWithFlags(extra);
      Components.formatPricing(extra, extra.isIncluded);
      return extra;
    });
  },
  formatPricing(extra, isIncluded) {
    extra.formattedRateAmount = isIncluded && "--";
    extra.formattedRateAmount =
      !isIncluded &&
      extra.rate_amount_view &&
      parseFloat(extra.rate_amount_view.amount) &&
      extra.rate_amount_view
        ? Rules.rateTypeMap[extra.rate_type].replace(
            "#{rate}",
            extra.rate_amount_view.format
          )
        : "--";

    if (extra.max_amount_view !== null) {
      extra.formattedMaxAmount = isIncluded && "--";
      extra.formattedMaxAmount =
        !isIncluded &&
        extra.max_amount_view &&
        parseFloat(extra.max_amount_view.amount)
          ? enterprise.i18nReservation.resflowextras_0008.replace(
              "#{price}",
              extra.max_amount_view.format
            )
          : "--";
    } else {
      extra.formattedMaxAmount = "--";
    }
    return extra;
  },
  extras: function (extras, chargeType) {
    let extrasObj = extras[Rules.getExtrasChargeType(chargeType)];
    if (extrasObj) {
      let fuel = extrasObj.fuel;
      let equipment = extrasObj.equipment.concat(fuel); // Combine equipment and fuel extras
      let insurance = extrasObj.insurance;
      let extrasByStatus;

      ExtrasActions.set.addExtras('equipment', Components.formatAvailableExtras(equipment));
      ExtrasActions.set.addExtras('fuel', Components.formatAvailableExtras(fuel));
      ExtrasActions.set.addExtras('insurance', Components.formatAvailableExtras(insurance));

      extrasByStatus = Rules.buildExtrasByStatus(
        'equipment',
        equipment
      );

      extrasByStatus = Rules.buildExtrasByStatus(
        'insurance',
        insurance,
        extrasByStatus.includedExtras,
        extrasByStatus.mandatoryExtras
      );

      ExtrasActions.set.addExtras('includedExtras', extrasByStatus.includedExtras);
      ExtrasActions.set.addExtras('mandatoryExtras', extrasByStatus.mandatoryExtras);
    }
  },
  hasUpgradeBanner(chargeType, selectedCarClassDetails, upgrades, upgraded) {
    const pricingDetails = _.get(selectedCarClassDetails, 'vehicleRates');
    return (pricingDetails && chargeType !== PRICING.REDEMPTION && (upgrades.length > 0 || upgraded));
  }
}

const ExtrasController = debug({
  isDebug: false,
  logger: {},
  name: 'ExtrasController',
  ...ServiceData,
  ...Rules,
  ...Components
});

export default ExtrasController;
