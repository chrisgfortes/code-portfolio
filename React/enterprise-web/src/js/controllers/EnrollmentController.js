import EnrollActions from '../actions/EnrollActions';
import LoginActions from '../actions/LoginActions';
import ErrorActions from '../actions/ErrorActions';
import AccountActions from '../actions/AccountActions';
import KeyRentalFactsActions from '../actions/KeyRentalFactsActions';
import AccountService from '../services/AccountService';
import { debug } from '../utilities/util-debug';
import DomManager from '../modules/outerDomManager';
import { getEnrollmentIssueAuthories } from './LocaleController';
import { GLOBAL } from '../constants';

import {
  CreateLoginFactory,
  CreateProfileFactory,
  ProfileFactory
} from '../factories/User';

/************************
 * SERVICE INTERACTIONS *
 ************************/
const ServiceData = {
  // @todo consolidate getCountries calls in single place!!
  getCountries () {
    const pickupCountryCode = EnrollActions.getCountryCode();
    if (EnrollActions.getCountriesList().length < 1) {
      AccountService.getCountries().then((response)=> {
        AccountActions.setCountries(response.countries);
        KeyRentalFactsActions.setDisputesContactInfo(response.countries, pickupCountryCode);
        EnrollmentController.getSubdivisions(enterprise.countryCode);
        EnrollmentController.getIssueCountrySubdivisions(enterprise.countryCode);
        let countries = EnrollActions.getCountriesList();
        if (countries) {
          for (let i = 0, len = countries.length; i < len; i++) {
            if (countries[i].country_code === enterprise.countryCode) {
              EnrollmentController.setIssueCountry(countries[i]);
              EnrollmentController.setOriginCountry(countries[i]);
              return;
            }
          }
        }
      });
    }
  },
  getCountriesBasic () {
    const pickupCountryCode = EnrollActions.getCountryCode();
    return AccountService.getCountries().then((response)=> {
      AccountActions.setCountries(response.countries);
      KeyRentalFactsActions.setDisputesContactInfo(response.countries, pickupCountryCode);
      return response.countries;
    });
  },
  getIssueCountrySubdivisions(country) {
    AccountService.getIssueCountrySubdivisions(country)
      .then((response) => {
        AccountActions.setIssueCountrySubdivisions(response.regions);
      });
  },
  getSubdivisions(country) {
    return AccountService.getSubdivisions(country)
      .then((response) => {
        AccountActions.setSubdivisions(response.regions);
        return response.regions;
      });
  },
  createLogin() {
    const createPassword = AccountActions.get.createPassword();
    const data = CreateLoginFactory.toServer(createPassword);
    AccountService.createLogin(data, createPassword)
      .then((response) => {
        ErrorActions.clearErrorsForComponent('createPassword');
        if (response && response.profile) {
          EnrollmentController.setCreatePasswordDetails('loading', false);
          EnrollActions.setSuccessProfile(response);
          EnrollActions.setEnrollSuccess(true);
        }
      })
      .catch(() => {
        EnrollmentController.setCreatePasswordDetails('loading', false);
      });
  },
  createProfile() {
    const profile = EnrollActions.getEnrollProfile();
    const enroll = EnrollActions.getEnroll();
    let profileData = {
      profile, enroll
    }
    let data = CreateProfileFactory.toServer(profileData);
    return AccountService.createProfile(data)
        .then((response) => {
          ErrorActions.clearErrorsForComponent('enroll');
          if (response && response.profile) {
            return ProfileFactory.toState(response.profile);
          }
        })
      .catch((response) => {
        if (response) {
          response.forEach((message, index) => {
            if (message.code === 'RNTR64054') {
              EnrollActions.setEnrollModal('duplicateID');
            } else if (message.code === 'RNTR64050') {
              EnrollActions.setEnrollModal('duplicateAcc');
            }
          });
        }
        ErrorActions.setErrorsForComponent(response, 'enroll');
        console.error('EnrollmentController.createProfile().catch()', response);
        return 'error';
      });
  },
  searchProfileMember() {
    const createPassword = AccountActions.get.createPassword();
    const data = {
      "last_name": createPassword.lastName,
      "loyalty_number": createPassword.userName
    };

    AccountService.searchProfile( data )
      .then((response) => {
        if (response.profile) {
          EnrollmentController.setCreatePasswordDetails('profileId', response.profile.basic_profile.individual_id);
          if(response.profile.contact_profile.email) {
            EnrollmentController.setCreatePasswordDetails('email', response.profile.contact_profile.email);
            this.createLogin();
          } else {
            EnrollmentController.setEmailModal(true);
            EnrollmentController.setCreatePasswordDetails('loading', false);
          }
        }
      })
      .catch(() => {
        EnrollmentController.setCreatePasswordDetails('loading', false);
      });
  },
  getTerms () {
    AccountService.getTerms()
      .then((response) => {
        if (response && response.terms_and_conditions) {
          LoginActions.setTerms(response);
        } else {
          console.error('EnrollmentController.getTerms() "else"')
        }
      })
      .catch( err => console.error('EnrollmentController.getTerms()', err) );
  }
};

/**************************
 * COMPONENT INTERACTIONS *
 **************************/
const Components = {
  callCreatePassword(bool, terms, value) {
    EnrollmentController.setCreatePasswordDetails('loading', bool);
    EnrollmentController.setCreatePasswordDetails('termsVersion', terms);
    EnrollmentController.setCreatePasswordDetails('acceptTerms', value);
    EnrollmentController.setCreatePassword(true);
    EnrollmentController.searchProfileMember();
  },
  callEnrollBtn() {
    DomManager.trigger('callExpediteEnrollBtn');
  },
  callReviewBtn() {
    DomManager.trigger('callReviewBtn');
  },
  getIssueAuthorityLabel (country) {
    let issuingAuthorityLabel = null;
    issuingAuthorityLabel = getEnrollmentIssueAuthories(country.country_code);
    return issuingAuthorityLabel;
  },
  getSubdivisionLabel ( originCountry ) {
    let subdivisionLabel = null;
    let country = originCountry.country_code ? originCountry.country_code : originCountry;

    switch (country) {
      case 'US':
        subdivisionLabel = i18n('resflowreview_0066');
        break;
      case 'CA':
        subdivisionLabel = i18n('resflowreview_0067');
        break;
      default :
        subdivisionLabel = i18n('eplusenrollment_0056');
    }
    return subdivisionLabel;
  },
  getExpeditedEnrollFieldErrors(expedited, errors) {
    // Field errors
    let fieldErrors = {
      birthDateError: false,
      licenseExpiryError: false,
      licenseIssueError: false,
      addressError: false,
      zipCodeError: false,
      cityError: false,
      stateError: false
    };
    // Map client-side validation errors to the correspondent form fields
    if (expedited.errors) {
      expedited.errors.map(item => {
        switch (item) {
          case i18n('resflowreview_0037'):
            fieldErrors.licenseExpiryError = true;
            break;
          case i18n('resflowreview_0720'):
            fieldErrors.licenseIssueError = true;
            break;
          case i18n('eplusenrollment_0031'):
            fieldErrors.birthDateError = true;
            break;
          case i18n('resflowreview_0063'):
            fieldErrors.addressError = true;
            break;
          case i18n('resflowreview_0065'):
            fieldErrors.cityError = true;
            break;
          case i18n('resflowreview_0068'):
            fieldErrors.zipCodeError = true;
            break;
          case i18n('eplusenrollment_0056'):
          case i18n('resflowreview_0066'):
          case i18n('resflowreview_0067'):
            fieldErrors.stateError = true;
            break;
          default:
        }
      });
    }
    // Map error codes from CROS to the correspondent form fields
    if (errors) {
      errors.map(item => {
        switch (item.code) {
          case 'RNTR00907': {
            // This is the only element outside the RenderInformation form whose error is triggered
            //   by the Expedited action, so we cheat a little bit...
            const lastNameField = document.getElementById('lastName');
            lastNameField.focus();
            lastNameField.classList.add('invalid');
            break;
          }
          case 'RNTR00406':
            fieldErrors.licenseExpiryError = true;
            break;
          case 'RNTR00409' || 'RNTR00408':
            fieldErrors.licenseIssueError = true;
            break;
          case 'RNTR00005':
            fieldErrors.birthDateError = true;
            break;
          case 'RNTR64027':
            fieldErrors.zipCodeError = true;
            break;
          default:
        }
      });
    }
    return fieldErrors;
  },
  setProfileDetails(attr, value) {
    EnrollActions.setProfileDetails(attr, value);
  },
  setCreatePasswordDetails(attr, value) {
    EnrollActions.setCreatePasswordDetails(attr, value);
  },
  mergeProfileDetails(data) {
    EnrollActions.mergeProfileDetails(data);
  },
  setIssueCountry(country) {
    EnrollActions.setIssueCountry(country);
  },
  setCreatePassword(bool) {
    EnrollActions.setCreatePassword(bool);
  },
  setEnrollModal(type) {
    EnrollActions.setEnrollModal(type);
  },
  setEnrollSection (field) {
    EnrollActions.setEnrollSection(field);
  },
  setEnrollSuccess(bool) {
    EnrollActions.setEnrollSuccess(bool);
  },
  setSuccessProfile(data) {
    EnrollActions.setSuccessProfile(data);
  },
  setEmailModal(bool) {
    EnrollActions.setEmailModal(bool);
  },
  setEnrollContactDetails(country, value, subdivision) {
    EnrollmentController.setOriginCountry(country);
    EnrollmentController.getSubdivisions(value);
    EnrollmentController.setProfileDetails('country', value);
    EnrollmentController.setProfileDetails('subdivision', subdivision);
  },
  setEnrollLicenseDetails(country, value, regionValue) {
    EnrollmentController.setIssueCountry(country);
    EnrollmentController.getIssueCountrySubdivisions(value);
    EnrollmentController.setProfileDetails('licenseCountry', value);
    EnrollmentController.setProfileDetails('licenseRegion', regionValue);
    EnrollmentController.setProfileDetails('licenseIssue', regionValue);
  },
  setEnrollSectionDetails(sectionName, value) {
    EnrollmentController.setEnrollSection(sectionName);
    EnrollmentController.mergeProfileDetails(value);
  },
  setMemberid(bool) {
    EnrollActions.setMemberid(bool);
  },
  setOriginCountry(country) {
    EnrollActions.setOriginCountry(country);
  }
};

const Rules = {
  isOptionalDate(licenseDate) {
    return licenseDate === GLOBAL.LICENSE.OPTIONAL;
  },
  isMandatoryDate(licenseDate) {
    return licenseDate === GLOBAL.LICENSE.MANDATORY;
  }
}

const EnrollmentController = debug({
  isDebug: false,
  logger: {},
  name: 'EnrollmentController',
  ...ServiceData,
  ...Components,
  ...Rules
});

module.exports = EnrollmentController;
