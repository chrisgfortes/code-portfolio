/**
 * @module LocationSearchController
 * For Location Search specific code. Code for flow control and data services on the
 * Location Search page.
 * For general location "ENTITY" control code, code that is about locations themselves,
 * please use LocationController
 * tl;dr
 * LocationController for Location Entities
 * LocationSearchController for Location Search activities
 *
 * *******************************************************************************************
 *     PLEASE DO NOT IMPORT RESERVATIONFLOWMODELCONTROLLER !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * *******************************************************************************************
 *
 *
 */
import AppActions from '../actions/AppActions';
import ReservationActions from '../actions/ReservationActions';
import RedirectActions from '../actions/RedirectActions';
import ModifyActions from '../actions/ModifyActions';
import LocationSearchActions from '../actions/LocationSearchActions';
import DateTimeActions from '../actions/DateTimeActions';

import ReservationCursors from '../cursors/ReservationCursors';

import ServiceController from './ServiceController';
import BookingWidgetModelController from './BookingWidgetModelController';

import LocationController from './LocationController';

import LocationFactory from '../factories/LocationFactory';
import LocationModel, { MapFactory, LocationMapSearchFactory } from '../factories/Location';

import Analytics from '../modules/Analytics';

import LocationSearchService from'../services/LocationSearchService';
import KeyRentalFactsService from '../services/KeyRentalFactsService';

import Scroll from '../utilities/util-scroll';
import { PARTNERS, LOCATION_SEARCH } from '../constants';

import { debug } from '../utilities/util-debug';
const logger = debug({isDebug: false, name: 'LocationSearchController'}).logger;

const { VALIDITY } = LOCATION_SEARCH;
const TYPES = LOCATION_SEARCH.STRICT_TYPE;

/**
 * @namespace Rules: Business Rules used by LocationSearchController
 * @type {Object}
 */
const Rules = {
  /**
   * @function isExternalBrand
   * @memberOf LocationSearchController
   * @todo should this be elsewehere?
   * @param  {string}  brand brand name
   * @return {Boolean}       truth value
   */
  isExternalBrand ( brand ) {
    return brand === PARTNERS.ALAMO || brand === PARTNERS.NATIONAL;
  },
  /**
   * @function isSameLocation
   * @memberOf LocationSearchController
   * @return {boolean} truth value
   */
  isSameLocation: () => {
    return LocationSearchActions.isUsingSameLocation()
  }
}

/**
 * @namespace ServiceData: services and data interactions called by LocationSearchController
 * @type {Object}
 */
const ServiceData = {
  /**
   * @function callGetWeeksHoursForLocation
   * @memberOf LocationSearchController
   * @param  {strign} type     pickup, dropoff etc.
   * @param  {date} date     date object
   * @param  {object} location location object
   * @return {objecct}          jquery xhr ajax promise
   */
  callGetWeeksHoursForLocation (type, date, location) {
    logger.log('callGetWeeksHoursForLocation()', type, date, location);
    return LocationSearchService.getWeeksHoursForLocation(type, date, location);
  },

  /**
   * @function getResults
   * @memberOf LocationSearchController
   * @param  {object}  props   [description]
   * @param  {Boolean} results what type of results to return
   * @return {*}
   * @todo  this is run every time Map.render() runs unfortunately...
   */
  getResults (props, results=false) {
    // logger.warn('getResults(props, results)', props, results);
    return LocationSearchActions.getResults(props, results);
  },

  /**
   * @function selectQuery
   * @memberOf LocationSearchController
   * @param  {object} selected location object
   * @param  {string} type     direction pickup, dropoff etc.
   * @return {void}            sets map location properties
   * WARNING sometimes the selected.type is the type here, which is WRONG WRONG WRONG
   * @todo ECR-14192 WAIT! THIS CALLS setMaptResults but is getting a kinda sort LocationDetail back
   *       The ONLY REASON our page works is because Location Spatial Search still runs after this.
   *       --- this seems to serve the purpose of getting additional, selected location details, e.g. lat/longitude
   */
  selectQuery(selected, type) {
    logger.warn('000', type);
    // logger.log('selectQuery()', LocationController.isLocationTypeCityOrCountry(selected), LocationController.isLocationTypeMyLocation(selected))
    if (!LocationController.isLocationTypeCityOrCountry(selected) && !LocationController.isLocationTypeMyLocation(selected)) {

      // logger.warn('selectQuery(selected) with ', selected, type);
      let data = LocationModel.getLocationId(selected);
      // logger.warn('getLocationId()', data);
      return LocationSearchService.fetchSelectQuery(data)
        .then((response) => {
          logger.log('001');

          // logger.warn('using results data?', response[0] && response[0].longitude)

          // sometimes when we come into the map page we have a defined location selected, but not a coordinate system
          // when the selected object comes into here and has not lat or longitude we have to use the results' lat/long
          let coords = {
            longitude: selected.longitude || response[0] && response[0].longitude,
            lat: selected.lat || response[0] && response[0].latitude
          };
          if (Rules.isSameLocation()) {
            logger.log('001.1');
            LocationController.setLocationPosition(LOCATION_SEARCH.PICKUP, coords.lat, coords.longitude);
            LocationController.setLocationPosition(LOCATION_SEARCH.DROPOFF, coords.lat, coords.longitude);
          } else {
            logger.log('001.2');
            LocationController.setLocationPosition(type, coords.lat, coords.longitude);
          }
          logger.log('setMapTarget()', coords);
          LocationSearchActions.setMapTarget(type, coords);
          // logger.warn('selectQuery ... setMapResults()', response);
          // LocationSearchActions.setMapResults(type, response);
          return response[0];
        })
        // @todo: handle this right
        .catch((err) => {
          ServiceController.trap('LocationSearchController.selectQuery().catch()', err);
          return Promise.reject(false);
        });
    } else {
      logger.warn('resolving selectQuery() early!');
      return Promise.resolve(true);
    }
  },

  /**
   * Called from Location Search dropdown on Location Search Step
   * @todo: @gbov2: normalize this with BookingWidgetModelController and LocationSearchController for set location
   * THIS IS CALLED NOT WHEN THE DROP-DOWN IS SELECTED BUT WHEN THE ITEM IS CHOSEN IN THE LOCATION SEARCH PAGE
   * NEW METHOD MOVED FROM RESERVATIONFLOWMODELCONTROLLER
   * For the callback from BookingWidget, see BookingWidgetModelController
   * @function setLocation
   * @memberOf LocationSearchController
   * @param location
   * @param type
   * @see BookingWidgetModelController
   */
  setLocation (location, type) {
    logger.warn('setLocation(location, type)', location, type);
    ReservationActions.setLocation(location, type);

    const sameLocation = ReservationStateTree.select(ReservationCursors.sameLocation).get();
    const dropoffLocation = ReservationStateTree.select(ReservationCursors.dropoffLocationObj).get();
    const hasCityDropoffLocation = [TYPES.CITY, TYPES.MYLOCATION].includes(dropoffLocation.locationType);
    const hasCountryDropoffLocation = [TYPES.COUNTRY, TYPES.MYLOCATION].includes(dropoffLocation.locationType);
    const shouldSelectDropoffLocation = !sameLocation && type === LOCATION_SEARCH.PICKUP && (hasCityDropoffLocation || hasCountryDropoffLocation);

    logger.groupCollapsed('setLocation()');
    logger.log(!sameLocation);
    logger.log(type === LOCATION_SEARCH.PICKUP);
    logger.log(hasCityDropoffLocation);
    logger.log(hasCountryDropoffLocation);
    logger.groupEnd()

    if (sameLocation) {
      logger.log('x1');
      ReservationActions.setLocationForAll(location);
    }
    const doNothing = () => {};
    const moveToDropoffStepIfNecessary = (e) => {
      logger.log('x2');
      if (e.code !== '#cars') {
        logger.warn('goto dropoff?');
        LocationSearchController.initiateOnLoadSearch(LOCATION_SEARCH.DROPOFF);
        // @todo use constants from pageflow
        RedirectActions.goToReservationStep('location/dropoff');
      }
    };
    const retrieveTermsAndConditionIfPossible = (e) => {
      logger.log('x3');
      if (e.code === '#cars') {
        KeyRentalFactsService.getTermsAndConditions();
      }
    };

    logger.log('shouldSelectDropoffLocation?', shouldSelectDropoffLocation);

    const onSubmit = shouldSelectDropoffLocation ? moveToDropoffStepIfNecessary : doNothing;

    LocationController.getLocationDetails(LocationModel.getLocationId(location), type, true)
      .then(() => {
        logger.log('x4');
        ReservationActions.setLoadingClassName('loading');
        // @todo: would like to call BookingWidgetService here
        // this used to be ReservationFlowModelController which set errors differently
        BookingWidgetModelController.submit(onSubmit, 'LocationSearchController')
                                    .then(retrieveTermsAndConditionIfPossible);
      });
  },

  /**
   * @memberOf LocationSearchController
   */
  initiateOnLoadSearch (type) {
    logger.log('0001');
    // Components.setLoading();
    // LocationSearchController.isLocationPopulated = Promise.resolve();
    LocationSearchController.isLocationPopulated = new Promise((resolve, reject) => {
      // logger.warn('initiateOnLoadSearch()');
      const locationCursor = ReservationStateTree.select(ReservationCursors.model).select(type).select('location').get();
      // logger.warn('locationCursor', locationCursor);
      // note: if a location has been selected already some coords may not be defined
      const selected = LocationFactory.getSimpleLocationModel(locationCursor);
      logger.warn('initiateOnLoadSearch is calling selectQuery()', selected);
      LocationSearchController.selectQuery(selected, type)
        .then(() => {
          resolve(true);
        })
        .catch(err => {
          console.error('LocationSearchController.selectQuery failed', err);
          reject(err);
        });
    });
  },

  /**
   * @function doSpatialSearch   * @memberOf LocationSearchController
   * Called by Map.js .searchThisArea()
   * @param {object} mapBoundsModel map bounds object from MapFactory
   * @param  {object} options options.type, options.radius, options.mustPassDateTime
   * @return {void}         sets map options
   */
  doSpatialSearch (mapBoundsModel, options) {
    logger.warn('00 doSpatialSearch(mapBoundsModel, options)', mapBoundsModel, options);
    // @todo - add parameter validation(?)
    const {center} = mapBoundsModel;
    const {type, radius, mustPassDateTime} = options;

    const isUsingSameLocation = LocationSearchController.isSameLocation();
    let pickupDate;
    let pickupTime;
    let dropoffDate;
    let dropoffTime;

    if (mustPassDateTime) {
      if (isUsingSameLocation || type === LOCATION_SEARCH.PICKUP) {
        pickupDate = ReservationStateTree.select(ReservationCursors.pickupDate).get();
        pickupTime = ReservationStateTree.select(ReservationCursors.pickupTimeValue).get();
      }
      if (isUsingSameLocation || type === LOCATION_SEARCH.DROPOFF) {
        dropoffDate = ReservationStateTree.select(ReservationCursors.dropoffDate).get();
        dropoffTime = ReservationStateTree.select(ReservationCursors.dropoffTimeValue).get();
      }
    }

    let parameters = LocationMapSearchFactory.toServer({
      pickupDate,
      pickupTime,
      dropoffDate,
      dropoffTime,
      radius
    });
    const query = center.latitude + '/' + center.longitude;
    LocationSearchService.spatialSearch(query, parameters)
      .then((res) => {
        const uglyCenter = {
          lat: center.latitude,
          longitude: center.longitude
        };
        // @todo: models here not passed through Factory/Model/Classes
        LocationSearchActions.setMapTarget(type, uglyCenter);
        LocationSearchActions.setSearchRadius(type, res.radiusUsedInKilometers);
        logger.warn('setMapResults()', res.locationsResult);
        LocationSearchActions.setMapResults(type, res.locationsResult);
        Analytics.trackLocations(res.locationsResult);
      }
    );
  },

  /**
   * Used by Location Search screen to get Country Branches
   * @param  {string} Country Code
   * @param {object} mapBound gives latitude & longitude
   * @param {string} type tells if it's pickup/dropoff
   * @return {Promise} Promise wrapped ajax
   */
  getCountryBranches(country, mapBound, type) {
    const {center} = mapBound;
    const Center = {
      lat: center.latitude,
      longitude: center.longitude
    };
    const isUsingSameLocation = LocationSearchController.isSameLocation();
    let pickupDate;
    let pickupTime;
    let dropoffDate;
    let dropoffTime;
    if (isUsingSameLocation || type === LOCATION_SEARCH.PICKUP) {
      pickupDate = ReservationStateTree.select(ReservationCursors.pickupDate).get();
      pickupTime = ReservationStateTree.select(ReservationCursors.pickupTimeValue).get();
    }
    if (isUsingSameLocation || type === LOCATION_SEARCH.DROPOFF) {
      dropoffDate = ReservationStateTree.select(ReservationCursors.dropoffDate).get();
      dropoffTime = ReservationStateTree.select(ReservationCursors.dropoffTimeValue).get();
    }
    let parameters = LocationMapSearchFactory.toServer({
      pickupDate,
      pickupTime,
      dropoffDate,
      dropoffTime
    });
    LocationSearchService.getCountryBranches(country, parameters).then((res) => {
      LocationSearchActions.setMapTarget(type, Center);
      logger.warn('setMapResults()', res.locationsResult);
      LocationSearchActions.setMapResults(type, res);
      Analytics.trackLocations(res.locationsResult);
    });
  }

}

/**
 * @namespace Components: methods used by LocationSearchController
 * @type {Object}
 */
const Components = {
  callDomainRedirect (url) {
    RedirectActions.goDomainRedirect(url);
  },

  initiateLocationSearchListeners (type) {
    const mapCursor = ReservationStateTree.select(ReservationCursors[type + 'MapModel']);
    const locationFiltersCursor = ReservationStateTree.select(ReservationCursors.locationFilters);
    mapCursor.on('update', LocationSearchController.updateDisplayResults.bind(this, type));
    locationFiltersCursor.on('update', LocationSearchController.updateDisplayResults.bind(this, type));
    return [mapCursor, locationFiltersCursor];
  },
  releaseLocationSearchListeners (listenerCursors) {
    //listenerCursors.forEach( cursor => cursor.release());
    listenerCursors.forEach(cursor => cursor.off());
  },
  setMapTarget (props, coords) {
    LocationSearchActions.setMapTarget(props, coords);
  },
  getLocationCenter(location) {
    logger.warn('getLocationCenter()', location, location.lat, location.longitude);
    if (!LocationController.isLocationTypeCountry(location)) {
      return new Promise((resolve, reject) => {
        try {
          // this LatLng can come back with lat() and lng() as NaN if the input is not correct
          let position = new google.maps.LatLng(location.lat, location.longitude);
          if (position.lat() && position.lng()) {
            resolve(position);
          } else {
            reject(false);
          }
        } catch(e) {
          reject(e);
        }
      });
    } else {
      return new Promise((resolve, reject) => {
        try {
          new google.maps.Geocoder().geocode({'address': location.locationName}, function(results, status){
            if (status == google.maps.GeocoderStatus.OK) {
              resolve(results[0].geometry.location)
            }
          });
        } catch (e) {
          reject(e);
        }
      });
    }
  },
  setLoading (className = 'loading') {
    AppActions.setLoadingClassName(className);
  },

  /******* USER SELECTION ******************************************************************************/
  /**
   * Select Location from LocationInput BUT VIA LOCATION SEARCH
   * @param  {object} selected location object selected
   * @param  {string} type     pickpup / dropoff / etc.
   * @return {void}            just sets values
   */
  selectLocation ( selected, type ) {
    logger.warn('\tTHIS IS WHEN A USER SELECTS SOMETHING FROM THE LOCATION INPUT DROPDOWN - RENAME SET LOCATION?')
    logger.warn('\tLocationSearchController.selectLocation() via LOCATION SEARCH COMPONENTS');
    LocationSearchController.selectQuery(selected);

    ReservationActions.setLocation(selected, type);
    ReservationActions.clearLocationResults(type);
  },
  // still todo below this line
  getWeeksData (type, date, target, deepLink, details) {
    let id = null;

    if (details) {
      id = details.peopleSoftId;
    } else if (deepLink) {
      id = deepLink.reservation.pickup_location.id;
    } else {
      id = target.details.peopleSoftId;
    }
    DateTimeActions.setSelectedDate(type, date);
    LocationSearchController.callGetWeeksHoursForLocation(type, date, id);
  },
  getHoursForDate (deepLinkReservation, mapOfType, dateOfType, type) {
    logger.log('getHoursForDate()');
    let date = moment().startOf('week');
    if (enterprise.locationDetail) {
      LocationSearchController.callGetWeeksHoursForLocation(LOCATION_SEARCH.PICKUP, date, enterprise.locationDetail.locationId);

    } else if (deepLinkReservation && deepLinkReservation.reservation.pickup_location) {
      LocationSearchController.callGetWeeksHoursForLocation(LOCATION_SEARCH.PICKUP, date, deepLinkReservation.reservation.pickup_location.id);

    } else {
      date = dateOfType.clone().startOf('week');
      LocationSearchController.callGetWeeksHoursForLocation(type, date, mapOfType.target.details.peopleSoftId);
    }
    return date;
  },

  /******* FILTERS  ***********************************************************************************/
  filterResults (results, type) {
    return LocationSearchController.filterOpenForMyTimes(results, type); // @todo - compound other filtering methods as they get implemented
  },
  filterOpenForMyTimes (results, type) {
    let filteredResults = results;

    const isFiltering = LocationSearchActions.getOpenForMyTimesFilter();
    if (isFiltering) {
      filteredResults = results.filter(result => LocationSearchController.isLocationOpen(result, type));
    }
    return filteredResults;
  },
  getActiveFilters () {
    // @todo - refactor this method when other filters are implemented
    let activeFilters = [];
    const isFilteringOpenForMyTimes = LocationSearchActions.getOpenForMyTimesFilter();
    if (isFilteringOpenForMyTimes) {
      activeFilters.push(i18n('locationsearch_0020') || 'Open for my times');
    }
    return activeFilters;
  },
  clearFilters () {
    LocationSearchActions.setOpenForMyTimesFilter(false);
  },

  /******* MODALS **************************************************************************************/
  showDatePickerModalOfTypeAndView (type) {
    let formattedType = (type == 'sameLocation') ? LOCATION_SEARCH.PICKUP : type;
    let location = ReservationStateTree.select(ReservationCursors[formattedType + 'Target']).get();

    //TODO: Add additional logic for opening pickup or dropoff section
    ReservationActions.backupCurrentDateTime();
    ReservationActions.showDatePickerModalOfType(type);
    if (type == 'sameLocation') {
      LocationController.getClosedHoursForLocation(LOCATION_SEARCH.PICKUP, location.details.peopleSoftId)
        .then(() => LocationController.getClosedHoursForLocation(LOCATION_SEARCH.DROPOFF, location.details.peopleSoftId))
        .then(() => ReservationActions.changeView(LocationSearchController.getViewFromInvalidType()));
    } else {
      LocationController.getClosedHoursForLocation(formattedType, location.details.peopleSoftId)
        .then(() => ReservationActions.changeView(LocationSearchController.getViewFromInvalidType()));
    }
  },
  getViewFromInvalidType () {
    const pickupInvalidTime = ReservationStateTree.select(ReservationCursors.pickupInvalidTime).get();
    const dropoffInvalidTime = ReservationStateTree.select(ReservationCursors.dropoffInvalidTime).get();
    const pickupInvalidDate = ReservationStateTree.select(ReservationCursors.pickupInvalidDate).get();
    const dropoffInvalidDate = ReservationStateTree.select(ReservationCursors.dropoffInvalidDate).get();

    if (pickupInvalidTime && dropoffInvalidTime && pickupInvalidDate && dropoffInvalidDate) {
      return 'pickupCalendar';
    } else if (pickupInvalidDate) {
      return 'pickupCalendar';
    } else if (pickupInvalidTime) {
      return 'pickupTime';
    } else if (dropoffInvalidDate) {
      return 'dropoffCalendar';
    } else if (dropoffInvalidTime) {
      return 'dropoffTime';
    }

    //Check if user hasn't left a any date times empty
    const pickupTime = ReservationStateTree.select(ReservationCursors.pickupTime).get();
    const dropoffTime = ReservationStateTree.select(ReservationCursors.dropoffTime).get();
    const pickupDate = ReservationStateTree.select(ReservationCursors.pickupDate).get();
    const dropoffDate = ReservationStateTree.select(ReservationCursors.dropoffDate).get();

    if (!pickupTime && !dropoffTime && !pickupDate && !dropoffDate) {
      return 'pickupCalendar';
    } else if (!pickupDate) {
      return 'pickupCalendar';
    } else if (!pickupTime.value) {
      return 'pickupTime';
    } else if (!dropoffDate) {
      return 'dropoffCalendar';
    } else if (!dropoffTime.value) {
      return 'dropoffTime';
    }
  },
  closeDateTimeErrorModal (type) {
    //var formattedType = (type == 'sameLocation') ? LOCATION_SEARCH.PICKUP : type;
    ReservationActions.revertDateTime();
    let sameLocation = ReservationStateTree.select(ReservationCursors.sameLocation).get();

    LocationSearchController.cleanUpScrollOffset('adjust-modal');

    let preventDetails = ReservationStateTree.select(ReservationCursors.locationSelect).select('preventDetails').get();

    if (preventDetails) {
      // if we do not do this, the Map.dockMapBasedOnScroll() does not work
      LocationSearchActions.clearDetailsByType(type);
    }

    if (sameLocation) {
      return LocationSearchService.getDateTimeValidity(LOCATION_SEARCH.PICKUP)
        .then(() => LocationSearchService.getDateTimeValidity(LOCATION_SEARCH.DROPOFF))
        .then(() => ReservationActions.showDatePickerModalOfType(null));
    } else {
      return LocationSearchService.getDateTimeValidity(type)
        .then(() => ReservationActions.showDatePickerModalOfType(null));
    }
  },
  closeAfterHoursModal () {
    LocationSearchActions.setAfterHoursModal(false);
  },

  /**
   * @function loadAfterHoursModal
   * @memberOf LocationSearchController
   * @param  {locationId} id location id of location
   * @return {void}    sets values
   */
  loadAfterHoursModal ( id ) {
    LocationSearchService.getAfterHoursPolicy(id)
      .then((response) => {
        if (response.location.policies && response.location.policies.length > 0) {
          for (let i = 0, len = response.location.policies.length; i < len; i++) {
            if (response.location.policies[i].code === 'AFHR') {
              LocationSearchActions.setAfterHoursMessage(response.location.policies[i].policy_text);
            }
          }
        }
        return response;
      })
      .then(() => {
        LocationSearchActions.setAfterHoursModal(true);
      })
      .catch((err) => {
        ServiceController.trap('LocationSearchController.loadAfterHoursModal().catch()', err);
      })
  },

  /******* DETAILS / RESULTS ETC  **********************************************************************/

  /**
   * @function getMapBoundsForSearch
   * @memberOf LocationSearchController
   * @param  {GoogleMap} gMap Google Maps Instance
   * @return {object}    bounds model for spatial search config
   */
  getMapBoundsModel(gMap) {
    return MapFactory.getMapBoundsModel(gMap);
  },

  //Get each line of hours info for the week
  //@todo: this shouldn't return markup
  getLineItems(weekHours) {
    let hours = weekHours.hours || [];
    let closed = weekHours.closed;
    let open24Hours = weekHours.open24Hours;
    let lineItems = [];

    if (open24Hours) {
      lineItems.push(
        <span className="location-hour" key={0}>
        {enterprise.i18nReservation.resflowlocations_0098}
      </span>
      );
    } else if (hours.length > 0 && !closed) {
      hours.map((hour, index) => {
        let operatingTime;
        if (hour) {
          operatingTime = moment(hour.open, 'H:mm').format('LT') + ' - ' + moment(hour.close, 'H:mm').format('LT');
        } else {
          operatingTime = enterprise.i18nReservation.locations_0020;
        }

        lineItems.push(
          <span className="location-hour" key={index}>
          {operatingTime}
        </span>
        );
      });
    } else {
      lineItems.push(
        <span className="location-hour" key={0}>
        {enterprise.i18nReservation.locations_0020}
      </span>
      );
    }
    return lineItems;
  },
  /**
   * @function loadDetailsPage
   * @memberOf LocationSearchController
   * @todo : this should be broken up, does too much
   * @param  {[type]}  details        [description]
   * @param  {[type]}  type           [description]
   * @param  {[type]}  showErrorModal [description]
   * @param  {Boolean} preventDetails [description]
   * @return {[type]}                 [description]
   */
  loadDetailsPage (details, type, showErrorModal, preventDetails = false) {
    logger.warn('loadDetailsPage()', details, type, showErrorModal, preventDetails);
    const target = ReservationStateTree.select(ReservationCursors[type + 'Map']).get().target;
    const pickupDate = ReservationStateTree.select(ReservationCursors.pickupDate).get();
    const sameLocation = ReservationStateTree.select(ReservationCursors.sameLocation).get();

    LocationSearchActions.setPreventDetails(preventDetails);

    if (sameLocation) {
      DateTimeActions.setSameLocationInvalidDateTime(false);
    } else {
      DateTimeActions.setInvalidDateTime(type, false);
    }

    LocationController.getLocationDetails(details.peopleSoftId, type);

    LocationSearchActions.setMapTarget(type, {
      longitude: details.longitude,
      lat: details.latitude
    });

    if (sameLocation) {
      LocationSearchActions.setMapTargetDetails(LOCATION_SEARCH.PICKUP, details);
      LocationSearchActions.setMapTargetDetails(LOCATION_SEARCH.DROPOFF, details);
    } else {
      LocationSearchActions.setMapTargetDetails(type, details);
    }

    if (showErrorModal) {
      ReservationActions.setLoadingClassName('loading');
      LocationController.getDateTimeValidity(type).then(() => {
        let renderType = LocationSearchController.shouldRenderDateTimeError(type);
        if (renderType) {
          LocationSearchActions.setSelectionActive(false);
          LocationSearchController.showDatePickerModalOfTypeAndView(renderType);
          ReservationActions.setLoadingClassName('');
        } else {
          let target = ReservationStateTree.select(ReservationCursors[type + 'Map']).get().target.details;
          let location = LocationFactory.getSimpleLocationModel(target, { locationType: TYPES.BRANCH });
          if (ModifyActions.isInflightModify()) {
            ModifyActions.toggleInflightModifyModal(true);
            ReservationActions.setLoadingClassName('');
          } else {
            LocationSearchController.setLocation(location, type);
          }
        }
      });
    } else {
      logger.warn('calling LocationController.getDateTimeValidity() instead!!!!!');
      LocationController.getDateTimeValidity(type);
    }

    //Set default selected date to be pickup date per wires
    DateTimeActions.setSelectedDate(type, pickupDate);
    LocationSearchService.getWeeksHoursForLocation(type, target.selectedDate, details.peopleSoftId);
  },
  setDetailsPage (props, bool) {
    LocationSearchActions.setDetailsPage(props, bool);
  },
  getExoticTypeForLocation (location) {
    const commonAttributes = ['ENTERPRISE', 'ONE_WAYS', 'COUNTER'];
    const hasExoticType = location.attributes.length > commonAttributes.length;
    const discardCommonAttributes = (attr) => !commonAttributes.includes(attr);
    return hasExoticType && location.attributes.filter(discardCommonAttributes).join('');
  },
  unloadDetailsPage (type) {
    LocationSearchActions.setMapTargetDetails(type, null);
  },
  updateDisplayResults (type) {
    const results = ReservationStateTree.select(ReservationCursors[type + 'MapModel']).get().results; // @todo - create cursor for results
    ReservationStateTree.select(ReservationCursors.locationDisplayResults).set(LocationSearchController.filterResults(results, type));
  },
  // NOTE: THIS IS AN ACTION
  clearDisplayResults () {
    ReservationStateTree.select(ReservationCursors.locationDisplayResults).set(null);
  },
  // NOTE SAME AS NEXT
  clearAllResults ( type ) {
    LocationSearchActions.clearMapResults();
    LocationSearchActions.setLocationFilter(type, 'all');
    LocationSearchActions.setLocationType(type, null);
  },
  // NOTE: SAME AS ABOVE
  clearAll (type) {
    LocationSearchActions.clearMapResults();
    LocationSearchActions.setLocationFilter(type, 'all');
    LocationSearchActions.setLocationType(type, null);
  },
  setHighlightLocation (index, pin) {
    LocationSearchActions.setHighlightLocation(index);
    if (pin) {
      LocationSearchActions.setPinHighlightLocation(index);
    }
  },
  setSelectionActive (bool) {
    LocationSearchActions.setSelectionActive(bool);
  },

  /******* OPEN CLOSED VALIDITY **********************************************************************/
  /**
   * @function selectLocationAndCheckTimeValidity
   * @memberOf LocationSearchController
   * @param  {object} location location object
   * @param  {string} type     pickup, dropoff etc.
   * @return {void}          sets flags and fires loadDetailsPage
   * @uses LocationSearchController.loadDetailsPage()
   */
  selectLocationAndCheckTimeValidity(location, type) {
    logger.warn('selectLocationAndCheckTimeValidity(location, type)', location, type);
    LocationSearchController.setSelectionActive(true);
    // LocationSearchController.loadDetailsPage(location, type, true);
    LocationSearchController.loadDetailsPage(location, type, true, false);
  },
  getLocationValidity (location, type) {
    return _.get(location, [type + 'Validity']);
  },
  getLocationValidities (location, type) {
    const isUsingSameLocation = LocationSearchController.isSameLocation();
    if (isUsingSameLocation) {
      return [LOCATION_SEARCH.PICKUP, LOCATION_SEARCH.DROPOFF].map(type => LocationSearchController.getLocationValidity(location, type));
    } else {
      return [LocationSearchController.getLocationValidity(location, type)];
    }
  },
  getLocationDateTimeDetails (location, type) {
    let details = {};
    const isUsingSameLocation = LocationSearchController.isSameLocation();
    if (isUsingSameLocation || type === LOCATION_SEARCH.PICKUP) {
      const pickupValidity = LocationSearchController.getLocationValidity(location, LOCATION_SEARCH.PICKUP);
      const pickupDate = ReservationStateTree.select(ReservationCursors.pickupDate).get();
      details.pickup = {
        isClosed: !LocationSearchController.isValidityOpen(pickupValidity),
        date: pickupDate && pickupDate.format(enterprise.i18nUnits.shortdateformat),
        hours: _.get(pickupValidity, 'locationHours.STANDARD.hours') || []
      };
    }
    if (isUsingSameLocation || type === LOCATION_SEARCH.DROPOFF) {
      const dropoffValidity = LocationSearchController.getLocationValidity(location, LOCATION_SEARCH.DROPOFF);
      const dropoffDate = ReservationStateTree.select(ReservationCursors.dropoffDate).get();
      details.dropoff = {
        isClosed: !LocationSearchController.isValidityOpen(dropoffValidity),
        date: dropoffDate && dropoffDate.format(enterprise.i18nUnits.shortdateformat),
        hours: _.get(dropoffValidity, 'locationHours.STANDARD.hours') || []
      };
    }
    return details;
  },
  isValidityOpen (validity) {
    if (validity) {
      const openValidityTypes = ['VALID_STANDARD_HOURS', 'VALID_AFTER_HOURS'];
      return openValidityTypes.includes(validity.validityType);
    } else {
      // logger.log('no validity available.');
      return;
    }
  },
  isLocationOpenForType (location, type) {
    return LocationSearchController.isValidityOpen(LocationSearchController.getLocationValidity(location, type));
  },
  isLocationOpen (location, type) {
    return LocationSearchController.getLocationValidities(location, type).every(LocationSearchController.isValidityOpen)
  },
  updateOpenForMyTimes (isFiltering) {
    LocationSearchActions.setOpenForMyTimesFilter(isFiltering);
  },
  getLocationStatus (location, type) {
    let closedTypes = [];
    const isUsingSameLocation = LocationSearchController.isSameLocation();
    if ((isUsingSameLocation || type === LOCATION_SEARCH.PICKUP) && !LocationSearchController.isLocationOpenForType(location, LOCATION_SEARCH.PICKUP)) {
      closedTypes.push(VALIDITY.PICKUP_INVALID);
    }
    if ((isUsingSameLocation || type === LOCATION_SEARCH.DROPOFF) && !LocationSearchController.isLocationOpenForType(location, LOCATION_SEARCH.DROPOFF)) {
      closedTypes.push(VALIDITY.DROPOFF_INVALID);
    }
    switch (closedTypes.length) {
      case 0 :
        return VALIDITY.OPEN;
      case 1 :
        return closedTypes[0];
      default:
        return VALIDITY.BOTH_CLOSED;
    }
  },
  hasAfterHoursDropoff (location) {
    const hasValidity = LocationSearchController.getLocationValidity(location, LOCATION_SEARCH.DROPOFF);
    return hasValidity && hasValidity.validityType === 'VALID_AFTER_HOURS';
  },

  /******* UI  **************************************************************************************/
  mobileTabSelect ( type, tab ) {
    LocationSearchActions.setMapTargetDetails(type, null);
    LocationSearchActions.setMapMobileTab(type, tab);
    LocationSearchActions.setShowMobileMap(type, tab === 'map');
  },
  hasErrorsOfType (propScope) {
    let retVal = false;

    logger.debug(propScope.invalidTime);
    logger.debug(propScope.invalidDate);

    if (propScope.invalidTime === true || propScope.invalidDate === true) {
      retVal = true;
    }
    return retVal;
  },
  shouldRenderDateTimeError (type) {
    let invalidObj = {
      pickupInvalidTime: ReservationStateTree.select(ReservationCursors.pickupInvalidTime).get(),
      dropoffInvalidTime: ReservationStateTree.select(ReservationCursors.dropoffInvalidTime).get(),
      pickupInvalidDate: ReservationStateTree.select(ReservationCursors.pickupInvalidDate).get(),
      dropoffInvalidDate: ReservationStateTree.select(ReservationCursors.dropoffInvalidDate).get()
    };
    let sameLocation = ReservationStateTree.select(ReservationCursors.sameLocation).get();

    if (sameLocation) {
      if (invalidObj.pickupInvalidTime || invalidObj.pickupInvalidDate || invalidObj.dropoffInvalidTime || invalidObj.dropoffInvalidDate) {
        return 'sameLocation';
      } else {
        return false;
      }
    } else if (invalidObj[type + 'InvalidTime'] || invalidObj[type + 'InvalidDate']) {
      return type;
    } else {
      return false;
    }
  },
  cacheScrollOffset (key) {
    let cachedScrollOffset = Scroll.getTopScroll();
    LocationSearchActions.cacheScrollOffset(key, cachedScrollOffset);
  },
  cleanUpScrollOffset (key) {
    // let scrollOffset = ReservationStateTree.select(ReservationCursors.locationSelect).select('scrollOffset').get();
    const scrollOffset = LocationSearchActions.getScrollOffset(key);
    if (scrollOffset && scrollOffset >= 0) {
      Scroll.scrollToOffset(scrollOffset);
      LocationSearchActions.cacheScrollOffset(key, null);
    }
  }
}

/**
 * LocationSearchController
 * @type {Object}
 */
const LocationSearchController = {
  ...Rules,
  ...ServiceData,
  ...Components,

  /**
   * @memberOf LoctionSearchController
   * Set up the location route in RouterController
   * Currently just a test for data availability on this route
   * @return     {boolean}  either rejects the promise or returns true
   */
  routeSetup() {
    let hasPickup = LocationSearchActions.get.hasPickupLocation();
    if (hasPickup) return true;
    else {
      RedirectActions.goToReservationStep('book');
      return Promise.reject(new Error('Location data unavailable, redirecting to #book.'));
    }
  },

  // Promise object for initiateOnLoadSearch()
  isLocationPopulated: {},

  /**
   * @function _dtmTracking
   * @memberOf LocationSearchController
   * DTM / Analytics
   * @param  {string} id      id of location
   * @param  {string} objType type of object
   * @return {string}         string identifier sent to dtm
   * @todo move to Analytics?
   * @todo figure out how to not call isUsingSameLocation() so much or something ...
   */
  _dtmTracking (id, objType) {
    // logger.log('_dtmTracking() sameLocation?', sameLocation);
    // const resType = LocationSearchController.isSameLocation() ? 'rt' : ReservationActions.getLocationSearchType();
    // logger.log(LocationController);
    const resType = LocationSearchController.isSameLocation() ? 'rt' : ReservationActions.getLocationSearchType();
    return objType+'.location_EN_'+id+'_'+resType;
  }

};

module.exports = LocationSearchController;
