import ReservationCursors from '../cursors/ReservationCursors';
import DeepLinkController from './DeepLinkController';
import ModifyController from './ModifyController';
import { PARTNERS, PAGEFLOW } from '../constants';
import { isMaskedField } from '../utilities/util-validate';
import { debug } from '../utilities/util-debug';
import { Enterprise } from '../modules/Enterprise';
let enterprise = Enterprise.global;

let enterpriseB2bFlag = (enterprise.b2b && enterprise.b2b.toLowerCase());

/**
 * @module Fedex Specific Business Rules
 * @type {Object}
 * @todo: Add Rules, ServiceData, etc.
 */
const FedexController = debug({
  name: 'FedexController',
  isDebug: false,

  /**
   * Determines if fedex reservation is re-used information not modify.
   * (1) when it's not a modify but has data recreated for re-using info via the buttons
   * (2) re-used info is provided by Deep Link properties
   * (3) we don't want to persist some information we may have because GBO can't handle it
   * @todo fix GMA's deep link persistence layer to clear masked data
   * @function   isFedexReusedInfoNotModify
   * @memberof   FedexController
   * @return  {boolean}
   */
  isFedexReusedInfoNotModify() {
    this.logger.log('isFedexReservation()', FedexController.isFedexReservation());
    this.logger.log('DeepLinkController.isDeepLinkReservation()', DeepLinkController.isDeepLinkReservation());
    this.logger.log('!ModifyController.isBothModifyInProgress()', !ModifyController.isBothModifyInProgress());
    return !ModifyController.isBothModifyInProgress() && DeepLinkController.isDeepLinkReservation() && FedexController.isFedexReservation();
  },

  /**
   * Filter out data we don't want to persist in Fedex
   *
   * @param      {string}  variable  The variable
   * @param      {string}  val       The value
   * @return     {string}  cleaned values
   */
  filterReusedData(variable, val) {
    let result;
    if (variable === 'phoneNumber' && (FedexController.isFedexReusedInfoNotModify() && isMaskedField(val))) {
      result = '';
    } else {
      result = val;
    }
    return result;
  },

  /**
   * @function isFedexReservation
   * @memberOf FedexController
   * @return {Boolean} true or false from the state tree
   */
  isFedexReservation() {
    return ReservationStateTree.select(ReservationCursors.isFedexReservation).get();
  },

  /**
   * Is this the fedex landing page?
   * @todo: we have at LEAST 3 others of these
   * @returns {boolean}
   */
  isFedexLandingPage() {
    return (enterprise.currentView === PAGEFLOW.LANDING_PAGE && enterpriseB2bFlag === PARTNERS.B2BS.FEDEX)
  },
  /**
   * Test various conditions to see if this is a Fedex Flow
   * @returns {boolean}
   */
  isFedexFlow(contractNumber) {
    return (contractNumber === PARTNERS.CONTRACTS.FEDEX || enterpriseB2bFlag === PARTNERS.B2BS.FEDEX)
  }
});

export default FedexController;
