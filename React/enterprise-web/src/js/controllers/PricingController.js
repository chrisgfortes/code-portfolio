/* global _, ReservationStateTree */
/**
 * PricingController
 * Consolidate Business Logic and Rules Related to Pricing.
 * @module PricingController
 * @type {Object}
 * @requires global enterprise, global lodash, global ReservationStateTree Constants
 * @todo: remove lodash/underscore dependency
 */
import ReservationCursors from '../cursors/ReservationCursors';
import ReservationActions from '../actions/ReservationActions';
import CarSelectActions from '../actions/CarSelectActions';
import {PRICING} from '../constants';
import { debug } from '../utilities/util-debug';

/************************
 * SERVICE INTERACTIONS *
 ************************/
const ServiceData = {

};

/************************
 * BUSINESS MODEL RULES *
 ************************/
const Rules = {
  isCorporate (payType, contractType) {
    return payType === PRICING.PAYLATER && contractType === PRICING.CONTRACT_CORPORATE;
  },
  isPromotion (payType, contractType) {
    return payType === PRICING.PAYLATER && contractType === PRICING.PROMOTION;
  },
  isPayTypeSelected (payType, selectedType) {
    return payType === selectedType
        || (payType === PRICING.CURRENCY && PRICING.CURRENCY_TYPES.includes(selectedType));
  },
  isCurrencyPayType (payType) {
    return payType === PRICING.CURRENCY || PRICING.CURRENCY_TYPES.includes(payType)
  },
  isPrepayPayType (payType) {
    return payType === PRICING.PREPAY;
  },
  isRedemptionPayType (payType) {
    return payType === PRICING.REDEMPTION;
  },
  isRedemptionSaving (paymentItem) {
    return paymentItem.category === PRICING.EPLUS_REDEMPTION_SAVINGS;
  },
  canChangePayType(isModifying, payTypes) {
    const hasCurrencyType = PRICING.CURRENCY_TYPES.some(type => payTypes[type]);
    const hasRedemptionType = payTypes[PRICING.REDEMPTION];
    return !isModifying && hasCurrencyType && hasRedemptionType;
  }
};

/**************************
 * COMPONENT INTERACTIONS *
 **************************/
const Components = {
// frequently this does not matter, as the _payment and _view are set correctly by
  // the back end (GMA / GBO)
  isSamePricingRegion (cancelDetails) {
    let result = false;

    let sourceOne = _.get(cancelDetails, 'original_amount_payment') || null;
    let sourceTwo = _.get(cancelDetails, 'original_amount_view') || null;

    if (sourceOne && sourceTwo) {
      let primary = _.get(sourceOne, 'code');
      let secondary = _.get(sourceTwo, 'code');
      if (primary === secondary) {
        result = true;
      }
    }
    return result;
  },

  /**
   * We get, on cancellation details, the original locale's payment and view
   * amounts for different currencies. Here we normalize which amount is used,
   * paymentInformation, or original
   */
  getOriginalPaymentAmount () {
    let result = null;
    let sessionPrepay = ReservationStateTree.select(ReservationCursors.reservationSession).get('prepaySelected');
    let paymentInformation = ReservationStateTree.select(ReservationCursors.paymentInformation).get();
    let cancelData = ReservationStateTree.select(ReservationCursors.cancellationDetails).get();

    PricingController.logger.log('getOriginalPaymentAmount()', cancelData);

    if (sessionPrepay) {
      if (cancelData) {
        PricingController.logger.log('...00');
        result = _.get(cancelData, 'original_amount_view');
      } else {
        let payData = _.get(paymentInformation, 'amount') || paymentInformation;
        PricingController.logger.log('01');
        result = payData;
      }
    }
    return result;
  },

  getCurrencySymbol (currencyAmount, priceSummary) {
    return _.get(currencyAmount, 'symbol')
      || _.get(priceSummary, 'currency_symbol')
      || enterprise.primaryCurrencySymbol;
  },
  // sketchy at best ECR-12408 - we do not get a "format" (i.e. formatted value)
  // this is specific to priceDifferences, I believe -- in other cases you can't use
  // priceSummary.currency_code etc.
  getAmountWithSymbol (currencyAmount, priceSummary) {
    const symbol = PricingController.getCurrencySymbol(currencyAmount, priceSummary);

    // this.logger.debug('getAmountWithSymbol()', result); // array we need to do something with
    return currencyAmount.format.replace(currencyAmount.symbol, symbol);
  },

  /**
   * @todo: Combine with getNetRate() below. This is a half-way measure because when we found that
   * sometimes it's a string, sometimes it's a translation ( resflowcorporate_0107 ) it was alarming
   * Kevin O insists it should be a key, which I'm inclined to believe.
   */
  returnNetRate () {
    PricingController.logger.debug('returnNetRate()');
    // return i18n('resflowcorporate_0107') || PRICING.PRICING_NET_RATE;
    return i18n('resflowcorporate_0107');
  },

  /**
   * Found sometimes it's a hard coded string, sometimes it's a translation.
   * What we don't know yet: There may be a reason for that. Dave E doesn't recall.
   * @todo pull all rules about Net Price.
   * @param {bool} useLang use the translation or not.
   * @returns {*}
   */
  getNetRate (useLang) {
    let result;

    /**
     * ECR-12752
     * We have a business rule where, if the user is cross-domain, cannot modify, and there
     * is no pricing data, it was showing Net Rate but should show 0.00
     */
    let eligibility = ReservationStateTree.select(ReservationCursors.reservationEligibility);
    let reasons = eligibility.select('blocked_reasons').get();
    let modify = eligibility.select('modify_reservation').get();
    let paymentInfo = ReservationStateTree.select(ReservationCursors.paymentInformation).get();

    // this.logger.debug('getNetRate()', (Array.isArray(reasons) && reasons.length > 0), !modify, paymentInfo);

    // eligibility reasons will actually be an array of things like "Reserved on another domain"
    if ((Array.isArray(reasons) && reasons.length > 0) && !modify && paymentInfo === null) {
      const defaultAmount = ReservationActions.getDefaultAmount();
      result = PricingController.getAmountWithSymbol(defaultAmount, {});
    } else {
      result = useLang ? PricingController.returnNetRate() : PRICING.PRICING_NET_RATE;
    }
    PricingController.logger.debug('Net Rate?', result);
    return result;
  },

  /**
   * getPastPriceReceiptPrices()
   * ECR-12408 - Secret rates and a null GET invoice/past_trip_detail.price_summary.total_charged_amount
   * Choice was made that when this value isn't returned from GBO we display a zero 0.00 value
   * @param total
   * @returns {*}
   */
  getPastReceiptPrices (total) {
    PricingController.logger.debug('getPastReceiptPrices()', total);
    const defaultAmount = ReservationActions.getDefaultAmount();
    return _.get(total, 'total_charged_amount.format', PricingController.getAmountWithSymbol(defaultAmount, total))
  },

  /**
   * ECR-13057 - This method receives a Currency Amount Info and returns its `format` field split
   * into symbol, integral and decimal parts and decimal mark.
   * Notice this method expects `amountInfo` to be an object containing at least `formatted_amount` field as a string
   */
  getSplitCurrencyAmount (amountInfo) {
    const {symbol, formatted_amount: formattedAmount, decimal_separator: decimalDelimiter } = amountInfo;
    const [integralPart, decimalPart] = formattedAmount.split(decimalDelimiter);
    return {symbol, integralPart, decimalDelimiter, decimalPart};
  },
  getDefaultSplitCurrencyAmount () {
    return {
      symbol: '',
      integralPart: PricingController.getNetRate(),
      decimalDelimiter: '',
      decimalPart: ''
    };
  },
  getSafeSplitCurrencyAmount (amountInfo) {
    const { getSplitCurrencyAmount, getDefaultSplitCurrencyAmount } = PricingController;
    return amountInfo ? getSplitCurrencyAmount(amountInfo)
      : getDefaultSplitCurrencyAmount();
  },
  getEstimatedTotalValue (sessionContractDetails, selectedCar, pricing, chargeType) {
    let totalPrice = pricing ? pricing.estimated_total_view.format : PricingController.getNetRate(true);
    totalPrice.replace(' ', '');
    if (
      _.get(sessionContractDetails, 'contract_type') === PRICING.CONTRACT_CORPORATE &&
      _.get(selectedCar, `vehicleRates.${chargeType}.price_summary.estimated_total_view.amount`) === PRICING.VALUE_ZERO
    ) {
      totalPrice = PricingController.getNetRate();
    }
    return totalPrice;
  },
  getEstimatedTotalDestinationValue (pricing) {
    let totalPrice = pricing ? pricing.estimated_total_payment.format : PricingController.getNetRate(true);
    totalPrice.replace(' ', '');
    return totalPrice;
  },
  getRedemptionSavings ( chargeType, pricing ) {
    const { isRedemptionSaving } = PricingController;
    return chargeType === PRICING.REDEMPTION ? pricing.payment_line_items.find(isRedemptionSaving) : null;
  },
  getCouponHeading (contractType, sessionContractDetails) {
    let couponInfoHeading = i18n('reservationwidget_0039');
    if (sessionContractDetails && sessionContractDetails.contract_type) {
      if (contractType === PRICING.CORPORATE) {
        couponInfoHeading = i18n('resflowcorporate_4015');
      } else if (contractType === PRICING.PROMOTION_PROMOTION) {
        couponInfoHeading = i18n('resflowcorporate_4016');
      }
    }
    return couponInfoHeading;
  },
  getUnpaidRefundAmount (pricingDifference, cancelled, prepay, cancellationDetails) {
    let unpaidRefundAmount = _.get(pricingDifference, 'UNPAID_REFUND_AMOUNT.difference_amount_view.format', ' ').replace(' ', '') || false;
    // Canceled a Prepay reservation
    if (cancelled && prepay) {
      // ECR-11929 - requirement to show the same value as in the Thanks header, which is rendered in
      //   ConfirmedActions.getConfirmationThanks.
      unpaidRefundAmount = _.get(cancellationDetails, 'refund_amount.format');
    } else if (_.get(pricingDifference, 'UNPAID_REFUND_AMOUNT.difference_amount_view.amount') < 0) {
      unpaidRefundAmount = unpaidRefundAmount.replace('-', ''); //we remove the minus sign to avoid confusion to the user
    }
    return unpaidRefundAmount;
  },
  getPaymentMethodLabel (prepay, componentToRender, unpaidRefundAmount, wasCancelRebook, maskedNumber, pricingDifference, modify, cancelled) {
    let prepayLabel = '';
    const ReservationFlowModelController = require('./ReservationFlowModelController');
    if (componentToRender === 'Confirmed') {
      if (unpaidRefundAmount) { //unpaid refund only exis
        this.logger.log('001');
        prepayLabel = i18n('prepay_0070').replace(/#{cardNumber}/, maskedNumber) || `Your credit card ${maskedNumber} will be charged the following amount after rental is completed:`;
      } else if (wasCancelRebook) {
        this.logger.log('002 - this.props.modify: ', modify, ReservationFlowModelController.getIsModifiedFlag());
        prepayLabel = i18n('prepay_0072') || 'You will be refunded the original amount paid and your credit card will be charged the following amount:';
      } else {
        this.logger.log('003 - this.props.modify: ', modify, ReservationFlowModelController.getIsModifiedFlag());
        prepayLabel = i18n('prepay_1004').replace(/#{cardNumber}/, maskedNumber) || `Your credit card ${maskedNumber} was charged the following amount:`;
      }
    } else {
      prepayLabel = i18n('prepay_1005') || 'Your credit card will be charged the following amount:';
    }

    let paymentMethodLabel = (prepay ? prepayLabel : i18n('resflowreview_0110'));
    // Canceled a Prepay reservation
    if (cancelled && prepay) {
      paymentMethodLabel = i18n('prepay_0058') || 'Your credit card will be refunded the following amount:';
    } else if (_.get(pricingDifference, 'UNPAID_REFUND_AMOUNT.difference_amount_view.amount') < 0) {
      paymentMethodLabel = i18n('prepay_0058') || 'Your credit card will be refunded the following amount';
    }
    return paymentMethodLabel;
  },
  selectPayType: CarSelectActions.updatePayType
};

const PricingController = debug({
  isDebug: false,
  logger: {},
  name: 'PricingController',
  ...ServiceData,
  ...Rules,
  ...Components
});

export default PricingController;
//module.exports = PricingController;
