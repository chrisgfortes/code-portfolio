import ComplexType from '../ComplexType';
import AmountInfo from './AmountInfo';
import { PRICING } from '../../constants';

const protoRate = {
  amountView: new AmountInfo(), //total_price_view -- AmountInfo
  amountPayment: new AmountInfo(),
  type: PRICING.DAILY,
  quantity: 0
}

export default class Rate extends ComplexType {
  constructor (obj = {}) {
    super();
    return Object.assign({}, protoRate, obj);
  }
}