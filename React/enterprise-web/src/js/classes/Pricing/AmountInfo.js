import Base from '../Base'

const protoAmount = {
  code: '',
  symbol: '',
  amount: 0.0,
  format: '',
  formattedAmount: '', //formatted_amount
  decimalDelimiter: '' //decimal_separator
}

export default class AmountInfo extends Base {
  constructor (obj = {}) {
    super();
    return Object.assign({}, protoAmount, obj);
  }
}