import ComplexType from '../ComplexType';

class PaymentMethodsCollection {
  constructor (arr) {
    this.paymentMethods = arr;
  }
  get payment_methods () {   // eslint-disable-line camelcase
    return this.paymentMethods;
  }
  set payment_methods (v) {  // eslint-disable-line camelcase
    this.paymentMethods = v;
  }
}

let paymentMethods = new PaymentMethodsCollection();

const protoPaymentModel = {
  payment_profile: {
    ...paymentMethods
    // payment_methods: []
  }
}

export default class PaymentProfile extends ComplexType {
  constructor(obj = {}) {
    super();
    Object.assign(this, protoPaymentModel, obj);
  }
}
