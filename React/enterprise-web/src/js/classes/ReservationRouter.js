import ReservationActions from '../actions/ReservationActions';
import AppActions from '../actions/AppActions';
import ErrorActions from '../actions/ErrorActions';
import { Debugger } from '../utilities/util-debug';

/*
 {
 baoTree: tree,                                          // Tree you'll be using paths to
 loadingCursor: ['view', 'loadingClass'],                // Path to Cursor of the loading className on tree
 componentToRenderCursor: ['view', 'componentToRender'], // Path to Cursor of string of which component to render
 routes: {
 locationSelect: {
 onHashChange: function() {
 ReservationFlowModelController.getReservationSession(); // Immediately get session on hashChange
 // Or other data sequentially using deferredes
 // Or ReservationActions.initialDataReady(true) and start lazy loading some stuff
 },
 waitOn: ReservationCursors.initialDataReady, // Path to a data ready cursor set after that in this instance would get set after successful session ajax call
 loading: 'loading',       // What class to put on container while waiting for data (for above cursor to be true,
 ready: true/false,        // A happy Boolean for anyone to use.
 onReady: fn,              // Any middleware where you want to pass props or do other synchronous stuff right before component mounts to avoid rerenders
 // Useful for instance in setting LocationSearch type (pickup / dropoff)
 component: LocationSearch // Router will automatically set correct component type on tree when it is ready
 // Switch defined in react component for instance in ReservationFlow.js
 }
 }
 }
 */

let ReservationRouter = function (rules) {
  this.routeRules = rules.routes;
  this.baoTree = rules.baoTree;
  this.currentHash = null;
  this.defaultRoute = rules.defaultRoute;

  // debugging only
  this.isDebug = false;
  this.name = 'ReservationRouter';
  this.logger = new Debugger(window._isDebug, this, true, this.name);

  this.matchPageTitleToStep = function (hash) {
    let newTitle;
    switch (hash) {
      case 'dateTime': newTitle = i18n('reservationnav_0001'); break;
      case 'location': newTitle = i18n('reservationnav_0003'); break;
      case 'cars': newTitle = i18n('reservationnav_0010'); break;
      case 'extras': newTitle = i18n('reservationnav_0012'); break;
      case 'commit': newTitle = i18n('resflowreview_150'); break;
      case 'confirmed': newTitle = i18n('resflowconfirmation_0001') + ' ' + i18n('resflowconfirmation_0033'); break;
      case 'cancelled': newTitle = i18n('resflowconfirmation_0001') + ' ' + i18n('resflowviewmodifycancel_0057'); break;
      default: break;
    }
    if (newTitle) {
      document.title = enterprise.utilities.capitalizeAll(newTitle);
    }
  };

  this.init = function () {
    this.initHashChangeHandler();
  };

  this.initHashChangeHandler = function () {
    window.onhashchange = () => {
      let hash = location.hash.replace(/\?.*$/, '');

      hash = hash.replace('#', '');

      this.logger.warn('onhashchange()', hash);

      this.matchPageTitleToStep(hash);

      this.currentHash = hash;

      if (enterprise.currentView === 'reserve' && hash) {
        $('body').addClass('resflow-' + hash);
      }

      ReservationActions.setCurrentHash(hash);

      if (!this.routeRules.hasOwnProperty(hash)) {
        location.hash = this.defaultRoute;
      } else {
        //Set loading className if one exists otherwise fallback to 'loading'
        AppActions.setLoadingClassName(this.routeRules[hash].loading ? this.routeRules[hash].loading : 'loading');

        if (this.routeRules[hash].waitOn) { // Set up listener for when data is ready
          this.baoTree.select(this.routeRules[hash].waitOn)
            .once('update', () => this.componentIsReady());
        }

        if (this.routeRules[hash].onHashChange) { // Hash change immediately gets called after the data ready handler is set
          this.routeRules[hash].onHashChange();
        }

        if (!this.routeRules[hash].waitOn) { // If the data didn't have a dataReady handler and
          this.componentIsReady();        // the hashChange logic has been called, the component is ready
        }

        $('html, body').animate({scrollTop: '0px'});
      }
      ErrorActions.clearErrorsForComponent('viewCarSelect');
    };

    window.onhashchange();
  };

  this.componentIsReady = function () {
    this.logger.log('this.componentIsReady()', this.currentHash);
    if (this.routeRules[this.currentHash].onReady) {
      this.routeRules[this.currentHash].onReady();
    }
    this.routeRules[this.currentHash].ready = true;
    this.mountComponent(this.routeRules[this.currentHash].component);
    ReservationActions.setLoadingClassName(null);
  };

  this.mountComponent = function (component) {
    this.logger.log('mountComponent: ', component);
    ReservationActions.setComponentToRender(component);
  };
};

module.exports = ReservationRouter;
