import { get } from '../../utilities/util-object';
import LocationModel from '../../factories/Location';

/**
 * Used for Location Submission to SERVICE_ENDPOINTS.RESERVATION_INIT (& possibly others)
 * BookingWidgetService and ReservationFlowModelController
 * @class ServiceInitLocationModel
 * @param {Object} locationObject a location object with distinct fields
 * @param {Date} dateTime     date time object
 * @todo : GRAB THESE PROPERTIES SAFELY VIA FACTORY
 */
export function ServiceInitLocationModel(locationObject, locationType, locationDate) {
  return {
    'id': get(locationObject, 'locationId'),
    'name': LocationModel.getLocationName(locationObject),
    'latitude': get(locationObject, 'lat'),
    'longitude': get(locationObject, 'longitude'),
    'type': locationType,
    locationType,
    'dateTime': locationDate,
    'countryCode': get(locationObject, 'countryCode')
  }
}

