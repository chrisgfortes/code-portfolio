/**
 * This is used when a user selects from LocationDropDown via
 *   LocationResult or LocationCity
 * @param {object} location generic object usually passed
 *                          through LocationFactory
 */
export function SelectedInputLocation(location) {
  // pretty insane right here
  // @todo: normalize this
  // @todo this assumes it HAS an ID
  this.key = location.id
  this.id = location.id
  this.locationId = location.id
  this.peopleSoftId = location.id

  // @todo this assumes it HAS a locationName
  this.locationName = location.locationName

  // @todo this assumes it HAS a locationType
  this.type = location.type
  this.locationType = location.type

  this.longitude = location.longitude
  this.lat = location.lat
  this.countryCode = location.countryCode
  this.airportCode = location.airportCode
}
