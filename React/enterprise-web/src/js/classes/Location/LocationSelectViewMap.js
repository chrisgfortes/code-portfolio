
import ComplexType from '../ComplexType';

const protoLocationViewMap = {
  detailsPage: false,
  searchRadius: 1,
  showMobileMap: true,
  showMobileFilter: false,
  mobileTab: 'list',
  filter: 'all',
  locationType: 'all',
  searchAttribute: 'all',
  initialAirportSearch: false,
  target: {
    selectedDate: moment(),
    details: null,
    weeklyHours: [],
    longitude: null,
    lat: null,
    southwest: {
      longitude: null,
      lat: null
    },
    northeast: {
      longitude: null,
      lat: null
    }
  },
  zoom: null
}

/**
 * For the Location Select View Map
 * @class LocationSelectViewMap
 */
export class LocationSelectViewMap extends ComplexType {
  constructor(obj = {}) {
    super();
    return Object.assign({}, protoLocationViewMap, obj);
  }
}
