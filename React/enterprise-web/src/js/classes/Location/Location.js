/**
 * @module Location
 */
import ComplexType from '../ComplexType';
import { LOCATION_SEARCH } from '../../constants';

/**
 * @todo : should this be factory-based?
 */

/**
 * Baseline Location Search Results Object
 * @type {Object}
 * @todo : break into distinct file
 */
const protoLocationResults = {
  fedex: [],
  [LOCATION_SEARCH.TYPES.AIRPORTS]: [],
  [LOCATION_SEARCH.TYPES.CITIES]: [],
  [LOCATION_SEARCH.TYPES.EXACT_MATCH_COUNTRIES]: [],
  [LOCATION_SEARCH.TYPES.PARTIAL_MATCH_COUNTRIES]: [],
  [LOCATION_SEARCH.TYPES.BRANCHES]: [],
  [LOCATION_SEARCH.TYPES.PORTS]: [],
  [LOCATION_SEARCH.TYPES.TRAINS]: []
};

/**
 * Location Results template
 * @param {Object} obj Object to override property values
 * @requires protoLocationResults
 */
export function LocationResults (obj = {}) {
  return Object.assign({}, protoLocationResults, obj);
}

/**
 * Baseline Location Object template
 * @type {Object}
 * @todo : sometimes you need results and details, sometimes you don't
 */
const protoBaseLocation = {
  myLocation: false,
  locationId: null,
  locationName: null,
  locationType: null,
  lat: null,
  longitude: null,
  countryCode: null,
  airportCode: null,
  costCenter: null,
  results: new LocationResults(),
  details: {}
};

/**
 * Base Location Object in State Tree
 * @param {Object} obj Object to override property values
 * @requires protoBaseLocation
 */
export function BaseLocation(obj = {}) {
  return Object.assign({}, protoBaseLocation, obj)
}

/**
 * Used for View State Tree values
 * @param {number} offset offset value. Dropoff gets +2 days
 * @return {moment} moment.js date object with offset
 */
export function ProtoLocationViewDate(offset = 1) {
  return moment().add(offset, 'days');
}

/**
 * Baseline date for locations
 * Can be passed an offset like in the ReservationStateTree dropoff gets +2 days
 * @type {Object}
 */
export function ProtoLocationDate(offset = 1) {
  return {
    // momentDate: moment().add(offset, 'days'),
    momentDate: ProtoLocationViewDate(offset),
    closedDates: null,
    previousDate: null
  }
}

/**
 * Baseline full LocationState Object
 * @type {Object}
 */
const protoLocationState = {
  location: new BaseLocation(),
  date: new ProtoLocationDate(),
  time: {
    array: [],
    value: moment().hour(12).minute(0).second(0),
    closed: [],
    previousTime: null
  },
  locationHours: null,
  map: {
    results: false
  },
  afterHour: {
    time: [],
    allowed: null,
    phone: null,
    message: null
  },
  policies: null,
  noResults: false
};

/**
 * @class Location
 * @extends ComplexType
 * @requires protoLocationState
 * Typically this is only used for State Tree as it's the whole set of nested objects.
 */
export class Location extends ComplexType {
  /**
   * Location Class Constructor
   * @param  {Object} obj Object to override properties
   * @return {Object}     Location object with hierarchy of predfined fields.
   */
  constructor(obj = {}) {
    super()
    return Object.assign({}, protoLocationState, obj);
  }
}



