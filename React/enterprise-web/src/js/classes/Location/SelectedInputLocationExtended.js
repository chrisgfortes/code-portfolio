import {SelectedInputLocation} from './SelectedInputLocation';
/**
 * @class SelectedInputLocationExtended
 * @extends {SelectedInputLocation}
 * Usually this applies to a FEDEX location that includes the costCenter property.
 */
export class SelectedInputLocationExtended extends SelectedInputLocation {
  constructor(obj, extendedProps = { costCenter: void 0 }) {
    super(obj)
    this.costCenter = extendedProps.costCenter;
  }
}
