/* eslint no-undefined: "warn" */
import { get } from '../../utilities/util-object';

/**
 * SimpleLocation is used in various places as well as within a larger Location object.
 * Usually used for a selected Location
 * @constructor SimpleLocation
 * @param {object} obj to describe the fields
 *  - key / locationId
 *  - locationName
 *  - locationType
 *  - lat
 *  - longitude
 *  - countryCode
 */
export function SimpleLocation(obj) {

  // so I'd prefer to use a getter or setter and handle both scenarios
  // but we're still not 100% sold on Baobab liking objects being passed
  // around inside and it certainly wouldn't persist or be send to a service
  let lat = get(obj, 'lat') || get(obj, 'latitude');
  this.lat = lat;
  this.latitude = lat;

  this.key = get(obj, 'key'); // locationId
  this.locationName = get(obj, 'locationName'); // input should use getLocationName
  this.locationType = get(obj, 'locationType'); // input should use getLocationType
  this.longitude = get(obj, 'longitude');
  this.countryCode= get(obj, 'countryCode');
}

// SimpleLocation.prototype = Something.prototype || new Something;
// set defaults for fields
SimpleLocation.prototype = {
  key: undefined, // locationId
  locationName: "", // generally speaking when output to an AEM page, it's an empty string if not set
  locationType: undefined,
  lat: undefined,
  latitude: undefined,
  longitude: undefined,
  countryCode: "" // generally speaking when output to an AEM page, it's an empty string if not set
};
