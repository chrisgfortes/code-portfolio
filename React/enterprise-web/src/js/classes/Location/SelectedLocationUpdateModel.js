
/**
 * Used for Location Selection in Location Search dropdown.
 * Passed to update() method in State tree.
 * @param {string} id           locationId, peoplesoftId
 * @param {string} locationName name of location
 * @param {string} locationType CITY, AIRPORT, etc.
 * @param {string} lat          latitude position
 * @param {string} longitude    longitude position
 * @param {string} countryCode  country of the location
 * @param {string} airportCode  code for airport
 * @param {string} costCenter   (need an example of this...)
 * @todo : can we leverage one of the other Location objects?
 */
export function SelectedLocationUpdateModel({
  id, locationName, locationType, type, lat, longitude, countryCode, airportCode, costCenter
}) {
  return {
    // 'key': { // do we need a key here?
    //   $set: id
    // },
    'locationId': {
      $set: id
    },
    'locationName': {
      $set: locationName
    },
    // having both a type and a locationType is a HACK to get around inconsistent field usage
    // in the near term. longer term we should settle on one.
    'type': {
      $set: type
    },
    'locationType': {
      $set: locationType
    },
    'lat': {
      $set: lat
    },
    'longitude': {
      $set: longitude
    },
    'countryCode': {
      $set: countryCode
    },
    'airportCode': {
      $set: airportCode
    },
    'costCenter': {
      $set: costCenter
    }
  }
}
