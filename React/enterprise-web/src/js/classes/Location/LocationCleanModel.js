import ComplexType from '../ComplexType';

/**
 * @class LocationCleanModel
 * Use to clear out the pickup/dropoff objects.
 * This is way too close to SelectedLocationUpdateModel in my book.
 * Well, ok, it's too close to them all.
 */
export class LocationCleanModel extends ComplexType {
  /**
   * Constructor for LocationCleanModel
   * @param  {String} direction string direction, pickup, dropoff, etc.
   * @param  {LocationResults} resultSet [description]
   * @return {LocationCleanModel}
   */
  constructor(direction = 'pickup', resultSet) {
    super();
    this[direction] = {
      location: {
        $set: {
          locationId: null,
          locationName: null,
          locationType: null,
          lat: null,
          longitude: null,
          results: resultSet
        }
      }
    }
  }
}
