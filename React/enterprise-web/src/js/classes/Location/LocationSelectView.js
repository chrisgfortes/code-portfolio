
import ComplexType from '../ComplexType';

const protoLocationView = {
  afterHours: {
    modal: false
  },
  dataReady: null,
  showType: null,
  showDatePickerModalOfType: null,
  errors: null,
  highlightLocation: null,
  highlightPinLocation: null,
  displayResults: null,
  filters: {
    openForMyTimes: false
  },
  preventDetails: false,
  selectionActive: false,
  scrollOffset: null // may no longer be used
}
/**
 * For the Location Select View State
 * @class LocationSelectView
 */
export class LocationSelectView extends ComplexType {
  constructor(obj = {}) {
    super();
    return Object.assign({}, protoLocationView, obj);
  }
}
