import ComplexType from '../ComplexType';

const protoLocationViewState = {
  policies: {
    modal: false,
    showPolicy: null
  },
  date: moment().add(1, 'days'),
  tempDate: null,
  location: {
    loading: false,
    error: false
  },
  calendar: {
    loading: false,
    error: false
  },
  time: {
    loading: false,
    error: false,
    initial: true
  }
}

/**
 * For the Location Search View State
 * @class LocationViewState
 */
export class LocationViewState extends ComplexType {
  constructor(obj = {}) {
    super();
    return Object.assign({}, protoLocationViewState, obj);
  }
}
