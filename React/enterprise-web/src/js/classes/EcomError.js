/**
 * Throw Real Errors, my friend.
 * of type ...
 * Most ideas came from here ...
 * https://github.com/onury/custom-error-test/blob/master/the-one/CustomError.js
 */
const setProto = Object.setPrototypeOf;

export default function EcomError(message, source) {
  let err;
  let sourceMessage = `${message} :: ${source}`;
  if (setProto) {
    err = new Error(sourceMessage);
    setProto(err, EcomError.prototype);
  } else {
    err = this;
  }

  Object.defineProperty(err, 'name', {
    enumerable: false,
    writable: false,
    value: 'EcomError'
  });

  if (!setProto) {
    Object.defineProperty(err, 'message', {
      enumerable: false,
      writable: true,
      value: sourceMessage
    });
    if (Error.captureStackTrace) {
      Error.captureStackTrace(err, EcomError);
    } else {
      Object.defineProperty(err, 'stack', {
        enumerable: false,
        writable: false,
        value: (new Error(sourceMessage)).stack
      });
    }
  }
  Object.defineProperty(err, 'source', {
    enumerable: false,
    writable: true,
    value: source
  });
  return err;
}
if (setProto) {
  setProto(EcomError.prototype, Error.prototype);
} else {
  EcomError.prototype = Object.create(Error.prototype, {
    constructor: { value: EcomError }
  });
}
