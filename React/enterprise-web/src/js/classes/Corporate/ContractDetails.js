/**
 * @module ContractDetails
 * This is used on the ContractDetails response as well as the profile.basicProfile
 *  when it is a corporate EP account. Example: J82V6MG/enterprise1
 */
import ComplexType from '../ComplexType';
import { exists } from '../../utilities/util-predicates';

const protoContractDetails = {
  contract_number: null,
  contract_type: null,
  contract_name: null,
  contract_accepts_billing: true,
  additional_information: [],
  restrictions: {
    rental_minimum_length: 0,
    rental_maximum_length: 0,
    black_out_dates: [],
    participating_countries: [],
    participating_states: [],
    participating_cities: []
  },
  contract_has_additional_benefits: false,
  marketing_message_indicator: false, // marketing / do not market / show + hide the page wrapper etc. see ReservationStatus Flags/Factory
  allow_account_removal_indicator: true,
  loyalty_sign_up_eligible: true,
  upgrade_offer_indicator: false,
  custom_links: [],
  frequent_flyer_eligible_indicator: true,
  allow_one_way: true,
  authentication_type: null,
  non_enriched_contract_name: null,
  mob_descriptions: [],
  mob_short_descriptions: [],
  additional_info_before_rates: false,
  third_party_email_notify: false,
  email_opt_in: false,
  disable_membership_programs: []
}

/**
 * Class for corporate details.
 * @class      CorporateDetails (ContractDetails)
 */
export default class ContractDetails extends ComplexType {
  /**
   * Constructs the ContractDetails object.
   * This is used on the ContractDetails response as well as the profile.basicProfile
   *  when it is a corporate EP account. Example: J82V6MG/enterprise1
   * @param      {object}  obj     The incoming data object
   */
  constructor (obj) {
    super();
    // Obj must sometimes be set to nothing so we return nothing!!
    if (exists(obj)) {
      Object.assign(this, protoContractDetails, obj);
    } else return void 0;
  }
  // get contractName () {
  //   return this.contractName;
  // }
}
