import ComplexType from '../ComplexType';

const protoLogisticsInfo = {
  enabled: false,
  streetAddress: null,
  city: null,
  postal: null,
  phone: null,
  comments: null
};

export default class LogisticsInfo extends ComplexType {
  constructor (obj = {}) {
    super();
    return Object.assign({}, protoLogisticsInfo, obj);
  }
}