import Base from './Base';
import { Debugger } from '../utilities/util-debug';

/**
 * @class FieldTypeBase
 * Also known as FType Classes. These are single entry fields.
 * @extends Base
 */
export default class FieldTypeBase extends Base {
  constructor(name = 'FieldTypeBase', debug = false) {
    super()
    this.name = name;
    this.isDebug = debug;
    this.logger = new Debugger(window._isDebug, this, true, this.name);
  }
}
