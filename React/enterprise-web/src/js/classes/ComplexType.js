import Base from './Base';

// let ComplexType = Object.create(Base);

class ComplexType extends Base {
  constructor() {
    super();
    // console.log('ComplexType constructor fired.');
  }
}

export default ComplexType;

// export class LocationTemplate extends Base {
//   constructor(args = {}) {
//     super(args);
//     this._private = args;
//     Object.keys(this._private).forEach((key) => {
//       Object.defineProperty(this, key, {
//         get() {
//           console.log('getting...', key);
//           return this._private[key];
//         },
//         set(val) {
//           console.log('setting...', key);
//           this._private[key] = val;
//         }
//       })
//     });
//     this.source = 'Constructor';
//   }
// }

// export class Location extends LocationTemplate {
//   constructor(){
//     super(protoLocation);
//   }
// }

