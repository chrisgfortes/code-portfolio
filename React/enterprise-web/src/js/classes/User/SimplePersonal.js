import ComplexType from '../ComplexType';

const protoSimplePersonal = {
  name: null,
  email: null,
  phone: null,
  address: null,
  age: null
};

export default class SimplePersonal extends ComplexType {
  constructor (obj = {}) {
    super();
    return Object.assign(this, protoSimplePersonal, obj);
  }
}
