import ComplexType from '../ComplexType';

const protoUserStateView = {
  account: {
    pangui: null,
    tab: null,
    editModal: null,
    modifyPayment: null,
    countries: [],
    subdivisions: [],
    issueCountrySubdivisions: [],
    contact: {
      errors: null
    },
    driver: {
      errors: null
    },
    password: {
      errors: null
    },
    editModalErrors: null
  },
  payment: {
    addCardSuccess: false,
    registerCardSuccess: false,
    cardModal: null,
    id: null,
    responseData: {},
    pangui: false,
    errors: null,
    submitting: false,
    fareoffice: false,
    context: null
  },
  enroll: {
    success: false,
    successID: null,
    modal: false,
    section: 'account',
    errors: null,
    issueCountry: null,
    originCountry: null,
    useMemberid: false,
    profile: {} // PROFILE OBJECT????
  }
};

/**
 * Default User State Modelfor ReservationStateTree
 * @class UserStateView
 */
export default class UserStateView extends ComplexType {
  constructor(obj = {}) {
    super();
    return Object.assign({}, protoUserStateView, obj);
  }
}
