import LoyaltyData from './LoyaltyData';
import { ContractDetails } from '../Corporate';

const protoBasicProfile = {
  first_name: '',
  individual_id: '',
  last_name: '',
  // loyalty_data: {}, cannot have this here or it confuses getters and setters
  user_name: '',
  // customer_details: null,
  town_of_birth: null
}

export default class BasicProfile {
  constructor (obj = {}) {
    // since we modify obj we make a copy!!!
    let basicCopy = Object.assign({}, obj);
    this.loyaltyData = new LoyaltyData(basicCopy.loyalty_data ? basicCopy.loyalty_data : null);
    this.customerDetails = new ContractDetails(basicCopy.customer_details ? basicCopy.customer_details : null);
    delete basicCopy.loyalty_data; // delete original value, we use the getter below
    delete basicCopy.customer_details; // delete the original value, we use the getter below

    Object.assign(this, protoBasicProfile, basicCopy);
  }
  get loyalty_data () { // eslint-disable-line camelcase
    return this.loyaltyData;
  }
  get customer_details () { // eslint-disable-line camelcase
    return this.customerDetails;
  }
  set loyalty_data (v) { // eslint-disable-line camelcase
    this.loyaltyData = v;
  }
  set customer_details (v) { // eslint-disable-line camelcase
    this.customerDetails = v;
  }
}
