
const protoLoyaltyData = {
  activity_to_next_tier: {},
  id: '',
  loyalty_member_ship_type: '',
  loyalty_number: '',
  loyalty_program: '',
  loyalty_program_code: '',
  loyalty_tier: '',
  points_to_date: 0,
  rental_days_to_date: 0,
  start_date: ''
}

export default class LoyaltyData {
  constructor(obj) {
    if (obj !== null) {
      Object.assign(this, protoLoyaltyData, obj);
    } else return void 0;
  }
}
