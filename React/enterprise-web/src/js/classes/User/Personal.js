import ComplexType from '../ComplexType';

const protoPersonal = {
  firstName: null,
  lastName: null,
  email: null,
  phoneNumber: null,
  requestPromotion: null, // pretty sure this should be null
  airlineCode: null,
  flightNumber: null,
  terms: 'false'
}

export default class Personal extends ComplexType {
  constructor (obj = {}) {
    super();
    return Object.assign(this, protoPersonal, obj);
  }
}
