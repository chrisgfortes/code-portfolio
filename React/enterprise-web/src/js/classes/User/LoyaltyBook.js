import ComplexType from '../ComplexType';

/**
 * @todo the loyalty data is pulled in UserFactory and ResInitRequestFactory as well
 */

const protoLoyaltyBook = {
  loyalty_brand: null, // @todo - rename to loyaltyBrand
  membership_id: null, // @todo - rename to membershipId
  last_name: null // @todo - rename to lastName
};

export default class LoyaltyBook extends ComplexType {
  constructor (obj = {}) {
    super();
    // console.warn('LoyaltyBook constructor', obj);
    return Object.assign(this, protoLoyaltyBook, obj);
  }
  // @todo - swap this field name with the actual above
  get loyaltyBrand () {
    return this.loyalty_brand;
  }
  // @todo - swap this field name with the actual above
  get membershipId () {
    return this.membership_id;
  }
  // @todo - swap this field name with the actual above
  get lastName () {
    return this.last_name;
  }
}
