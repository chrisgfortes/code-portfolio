import ComplexType from '../ComplexType';

const protoExpeditedState = {
  expediteEligibility: null,
  expedited: {
    dnr: null,
    modal: null,
    render: 'search',
    countryIssue: null,
    regionIssue: null,
    license: null,
    countryResidence: null,
    regionResidence: null,
    address: null,
    addressTwo: null,
    city: null,
    postal: null,
    dateOfBirth: null,
    licenseIssueDate: null,
    licenseExpiryDate: null,
    password: null,
    passwordConfirm: null,
    previousChargeType: null,
    terms: null,
    restart: false,
    errors: null
  }
}

export class ExpeditedModelState extends ComplexType {
  constructor (obj = {}) {
    super();
    return Object.assign({}, protoExpeditedState, obj);
  }
}
