import ComplexType from '../ComplexType';

import Personal from './Personal';
import LoyaltyBook from './LoyaltyBook';
import DriverInfo from './DriverInfo';

/**
 * Legacy ReservationStateTree.data.model profile objects
 * @type {Object}
 */
const protoUserStateModel = {
  // legacy model
  employeeNumber: null,
  // legacy model
  login: {
    password: null,
    username: null,
    brand: null,
    rememberMe: null
  },
  // legacy model
  createPassword: {
    setPassword: false,
    setEmailModal: false,
    errors: null,
    loading: null,
    licenseCountry: null
  },
  // @todo usually construction of combined objects should be in factories
  loyaltyBook: new LoyaltyBook(),
  // @todo usually construction of combined objects should be in factories
  personal: new Personal(),

  driverInfo: new DriverInfo()
  // legacy model
  // driverInfo: {
  //   individual_indentifier: null,
  //   loyalty_program_type: null,
  //   payment_profile: {
  //     payment_methods: []
  //   }
  // }
};

/**
 * Default User State Modelfor ReservationStateTree
 * @class UserStateModel
 */
export default class UserStateModel extends ComplexType {
  constructor(obj = {}) {
    super();
    return Object.assign({}, protoUserStateModel, obj);
  }
}
