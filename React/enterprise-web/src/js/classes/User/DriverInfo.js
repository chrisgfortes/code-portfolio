import ComplexType from '../ComplexType';

import { PaymentProfile } from '../Payment';

let paymentProfile = new PaymentProfile();

const protoDriverInfo = {
  first_name: null,
  last_name: null,
  email_address: null,
  phone: {
    phone_number: null,
    phone_type: null,
    priority: null
  },
  individual_indentifier: null,
  loyalty_program_type: null,
  ...paymentProfile,

  // not sure all these are always present ... bottom group here came from toServer in DriverInfoFactory
  // address model?
  address: {
    street_addresses: null,
    city: null,
    country_subdivision_code: null,
    country_code: null,
    postal: null,
    address_type: null
  },
  // license model?
  drivers_license: { // #driversLicense
    license_number: null,
    country_subdivision_code: null,
    country_code: null,
    birth_date: null,
    license_expiry: null,
    license_issue: null
  },
  request_email_promotions: false,
  source_code: null
}

export default class DriverInfo extends ComplexType {
  constructor(obj = {}) {
    super();
    return Object.assign(this, protoDriverInfo, obj);
  }
}

