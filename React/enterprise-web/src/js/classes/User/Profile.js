import ComplexType from '../ComplexType';

/**
 * @todo : Originally I started to use this for basicProfile only.
 * This is based on the GBO Common Profile Response
 */

const protoProfile = {
  addressProfile: {},
  basicProfile: {},
  contactProfile: {},
  licenseProfile: {},
  paymentProfile: {}, // use Payment Profile like on DriverInformation
  preference: {}
}

/**
 * Class for profile.
 * This is used for the user profile when they are logged in.
 * @see       ProfileFactory
 * @class      Profile (name)
 */
export default class Profile extends ComplexType {
  constructor(obj = {}) {
    super();
    // debugger;
    Object.assign(this, protoProfile, obj);
  }
  get basic_profile () { // eslint-disable-line camelcase
    // console.warn('get basic_profile .... please update this reference'); uncomment to see getter goodness in action
    return this.basicProfile;
  }
  set basic_profile (v) { // eslint-disable-line camelcase
    this.basicProfile = v;
  }
  get contact_profile () { // eslint-disable-line camelcase
    return this.contactProfile;
  }
  set contact_profile (v) { // eslint-disable-line camelcase
    this.contactProfile = v;
  }
  get license_profile () { // eslint-disable-line camelcase
    return this.licenseProfile;
  }
  set license_profile (v) { // eslint-disable-line camelcase
    this.licenseProfile = v;
  }
  get payment_profile () { // eslint-disable-line camelcase
    return this.paymentProfile;
  }
  set payment_profile (v) { // eslint-disable-line camelcase
    this.paymentProfile = v;
  }
  get address_profile() { // eslint-disable-line camelcase
    return this.addressProfile;
  }
  set address_profile(v) { // eslint-disable-line camelcase
    this.addressProfile = v;
  }
  // does this work?
  // get dnr () {
  //   return this.licenseProfile && this.licenseProfile.do_not_rent_indicator;
  // }
  // this was an idea for convenience but had trouble
  // get loyaltyData () {
  //   return this.basicProfile.loyalty_data;
  // }
  // get loyalty_data () { // eslint-disable-line camelcase
  //   return this.loyaltyData;
  // }
}
