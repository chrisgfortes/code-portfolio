/**
 * @see old MessageLogController
 * Message
 * Goes into the MessageLogger array.
 */

const protoMessage = {
  code: null,
  defaultMessage: null,
  displayAs: null,
  i18nKey: null,
  locale: null,
  message: null,
  parameters: [],
  priority: null,
  techMessage: null,
  type: null
}

export default class Message {
  constructor (obj) {
    Object.assign(this, protoMessage, obj);
  }
  get tech_message () {  // eslint-disable-line camelcase
    return this.techMessage;
  }
  set tech_message (v) {  // eslint-disable-line camelcase
    this.techMessage = v;
  }
  get display_as () { // eslint-disable-line camelcase
    return this.displayAs;
  }
  set display_as (v) { // eslint-disable-line camelcase
    this.displayAs = v;
  }
  getMessage () {
    return i18n(this.i18nKey);
  }
}

// @see old MessageLogController
// class Message {
//   constructor ({
//     code,
//     defaultMessage,
//     displayAs = 'ALERT', // defaults tbd
//     message,
//     tech_message = null, // eslint-disable-line camelcase
//     parameters = [],
//     priority = 'ERROR',  // defaults tbd
//     type = 'GLOBAL',      // defaults tbd
//     i18nKey // see ECR-11700 comments below
//   }) {
//     this.code = code; // is this gonna blow up in AEM / clientlibs?
//     this.defaultMessage = defaultMessage || message;
//     this.displayAs = displayAs;
//     this.message = message;
//     this.tech_message = tech_message;   // eslint-disable-line camelcase
//     this.locale = enterprise.i18nlocale;
//     this.parameters = parameters;
//     this.priority = priority;
//     this.type = type; // is this gonna blow up in AEM / clientlibs?
//     this.i18nKey = i18nKey; // see ECR-11700 comments below
//   }
//   // workaround for bug in ECR-11700 (see below)
//   getMessage () {
//     return i18n(this.i18nKey);
//   }
// }
