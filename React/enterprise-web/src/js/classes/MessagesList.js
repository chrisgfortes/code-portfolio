/**
 * MessagesList is just like an array of messages that we get back from the services
 * @requires   Message
 * @class      MessagesList MessagesList
 */
import Message from './Message';

export default class MessagesList {
  /**
   * A MessagesList is just an array of Messages
   *
   * @param      {Array}  messages  The messages array
   */
  constructor (messages = [], namespace = 'LEGACY') {
    let mess = ('messages' in messages) ? messages.messages : messages;
    this.messages = mess;
    this.namespace = namespace;
  }
  /**
   * @method MessagesList.addMessage
   * @param      {object}  messageObj    the message object item
   */
  addMessage (messageObj) {
    let message = new Message(messageObj);
    // bug in ECR-11700
    // https://github.com/yui/yuicompressor/issues/206
    // comes back undefined in AEM/YUI Compressor since .message is a getter on FE_PICKUP_LOCATION_INVALID
    // console.dir(message.message);
    if (message.message === void 0) {
      message.message = message.getMessage();
    }
    this.messages.push(message);
  }
  /**
   * Adds more than one messages array to another messages array
   * @method     MessagesList.addMessages
   * @param      {array}  items   the messages array to combine
   */
  addMessages (items) {
    this.messages = this.messages.concat(items);
  }
  /**
   * A stub to make a new Message
   * @method     MessagesList.makeMessage
   * @param      {string}  strCode     The string code
   * @param      {string}  strMessage  The string message
   */
  makeMessage (strCode, strMessage) {
    let message = new Message({
      code: strCode,
      defaultMessage: strMessage,
      message: strMessage
    });
    this.messages.push(message);
  }
  /**
   * Add a single message to the messages list
   * @method     MessagesList.add
   * @param      {object}  message    the message object
   */
  add (message) {
    this.message.push(message);
  }
}

