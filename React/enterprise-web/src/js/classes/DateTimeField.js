/**
 * @class DateTimeField
 * ******  ******  ******  ******  ******  ******  ******  ******  ******  ******  ******  ******
 * ****** NOTE: This is for handling our baseline GMA/GBO DATE TIME FIELD FORMAT, not for  ******
 * ****** *** creation or maniupulation of Moment.js date time fields or objects.  ******  ******
 * ******  ******  ******  ******  ******  ******  ******  ******  ******  ******  ******  ******
 * @requires moment.js
 * @todo  Extend with better Moment.js handling
 */
import FieldTypeBase from './FieldTypeBase';
// import EcomError from './EcomError';

export default class DateTimeField extends FieldTypeBase {
  /**
   * @constructor DateTimeField
   * @param  {string|object} date - date string or object
   * @param  {string|object} time - time string or object
   * @return {DateTimeField}
   * @todo Integrate with MOMENT.JS
   */
  constructor(date, time) {
    super('DateTimeField', true);
    if (!date) {
      this.logger.warn('DateTimeFied is missing date.', 'DateTimeField');
    }
    this.date = date || null;
    if (!time) {
      this.logger.warn('DateTimeFied is missing time.', 'DateTimeField');
    }
    this.time = time || null;
  }
  get dateTime () {
    if (DateTimeField.isMoment(this.date) && DateTimeField.isMoment(this.time)) {
      return this.date.format('YYYY-MM-DD') + this.time.format('THH:mm');
    } else {
      this.logger.warn('DateTimeField is not a Moment.js object', 'DateTimeField.js');
      return null;
    }
  }
  get datetime () {
    return this.dateTime;
  }
}

DateTimeField.isMoment = function (prop) {
  return moment.isMoment(prop) ? true : false;
}

DateTimeField.prototype.get = function () {
  return this.dateTime;
}
