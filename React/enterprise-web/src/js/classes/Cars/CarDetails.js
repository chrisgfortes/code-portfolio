import ComplexType from '../ComplexType';
import CarProperty from './CarProperty';
import { VEHICLES } from '../../constants';


const protoCarDetails = {
  code: '',
  name: '',
  status: VEHICLES.STATUS.SOLD_OUT,
  models: '', // make_model_or_similar_text
  priceDifferences: {}, // price_differences -- check for `pricingDifference` to see where this is used too
  images: {},
  mileageInfo: null, // mileage_info -- could possibly be another class
  redemptionDaysCount: 0, // redemption_day_count
  redemptionDaysMax: 0, // eplus_max_redemption_days
  redemptionPointsRate: 0, // redemption_points
  redemptionPointsUsed: 0, // eplus_points_used
  vehicleRates: {}, // vehicle_rates
  extras: {} // @todo - make some model for the extra object?
}

export default class CarDetails extends ComplexType {
  constructor (obj = {}) {
    super();
    return Object.assign({}, protoCarDetails, obj);
  }
}
