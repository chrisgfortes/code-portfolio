import ComplexType from '../ComplexType';
import { PRICING, VEHICLES } from '../../constants';

const protoCarModelState = {
  payType: PRICING.PAYLATER,
  isNAPrepayEnabled: false,
  availablePayTypes: {},
  availableVehicles: [],
  filteredVehicles: [],
  selectedCar: null,
  selectedCarCode: '',
  filters: {
    [VEHICLES.FILTER.CATEGORY]     : {},
    [VEHICLES.FILTER.PASSENGERS]   : {},
    [VEHICLES.FILTER.TRANSMISSION] : {}
  },
  selectedFilters: {
    [VEHICLES.FILTER.CATEGORY]     : [],
    [VEHICLES.FILTER.PASSENGERS]   : [],
    [VEHICLES.FILTER.TRANSMISSION] : []
  },
  preSelectedCarClass: null,
  preSelectedCarMetaClass: null,
  filterBandState: 'default',
  transmissionBandState: null
}

/**
 * For the Car Select portion of the ReservationStateTree
 * @class CarSelect
 */
export default class CarSelect extends ComplexType {
  constructor(obj = {}) {
    super();
    return Object.assign({}, protoCarModelState, obj);
  }
}