import ComplexType from '../ComplexType';
import AmountInfo from '../Pricing/AmountInfo';

const protoCarCharge = {
  totalPriceView: new AmountInfo(), //total_price_view
  totalPricePayment: new AmountInfo(), //total_price_payment
  rates: [] // list of `Pricing/Rate`
}

export default class CarCharge extends ComplexType {
  constructor (obj = {}) {
    super();
    return Object.assign({}, protoCarCharge, obj);
  }
}