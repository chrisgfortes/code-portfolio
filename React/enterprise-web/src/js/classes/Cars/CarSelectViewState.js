import ComplexType from '../ComplexType';

const protoCarViewState = {
  selectedCar: '',
  selectedCarDetails: null,
  dataReady: null,
  errors: null,
  taxesAndFees: {
    modal: false
  },
  modal: false,
  detailsCar: null,
  carRateCompare: {},
  rateComparison: {
    modal: false
  },
  vanModal: {
    modal: false,
    confirm: false,
    description: null
  },
  requestModal: {
    modal: false,
    confirm: false,
    origin: ''
  },
  redemption: {
    modal: false,
    days: 1,
    previousDays: null,
    loading: false,
    payLaterPrice: null
  },
  notAvailable: {
    modal: true
  },
  vehiclePriceModal: {
    modal: false
  },
  preSelectedVehicle: false
};

/**
 * For the Car Select View State
 * @class CarSelectViewState
 */
export default class CarSelectViewState extends ComplexType {
  constructor(obj = {}) {
    super();
    return Object.assign({}, protoCarViewState, obj);
  }
}