import ComplexType from '../ComplexType';
import CarProperty from './CarProperty';
import { VEHICLES } from '../../constants';


const protoCar = {
  code: '',
  name: '',
  status: VEHICLES.STATUS.SOLD_OUT,
  description: '',
  models: '', //make_model_or_similar_text
  charges: {}, // object with `CarCharge`s
  filters: {}, // object with `CarProperty`s
  category: new CarProperty('', ''), // not used yet, but could be used on Location Search filters
  features: [], // list of `CarProperty`s
  images: {},
  luggageCapacity: 0, //luggage_capacity
  priceDifferences: {}, // price_differences -- check for `pricingDifference` to see where this is used too
  isTnCRequired: false, //terms_and_conditions_required
  isPreferred: false, // preferred_vehicle
  redemptionDaysMax: 0, // eplus_max_redemption_days
  redemptionPointsRate: 0, // redemption_points
  truckUrl: null, //truck_url
  availabilityPhoneNumber: ''//call_for_availability_phone_number
  //details: undefined // `CarDetails` object
}

/**
 * For general purpose car "stuff"
 * @todo Determine if this is needed ... if it should be combined with the above, etc.
 */
export default class Car extends ComplexType {
  constructor (obj = {}) {
    super();
    return Object.assign({}, protoCar, obj);
  }
}