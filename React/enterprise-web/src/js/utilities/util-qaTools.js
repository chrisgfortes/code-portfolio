/**
 * TODO:
 * Create a floating panel to toggle features
 */

class QATools {
  constructor () {
    this.active = false;
  }

  init () {
    this.toggle();
  }

  toggle (active) {
    if (active === undefined) {
      this.active = !this.active;
    } else {
      this.active = active;
    }

    // namespace class to style the UI
    document.body.classList.toggle('qatools', this.active);

    // focus visualization on console
    if (this.active) {
      document.addEventListener('keyup', this.keyListener);
    } else {
      document.removeEventListener('keyup', this.keyListener);
    }
  }

  keyListener (e) {
    // TAB
    if (e.keyCode === 9) {
      console.debug('focus', document.activeElement);
    }
  }
}

module.exports = QATools;
