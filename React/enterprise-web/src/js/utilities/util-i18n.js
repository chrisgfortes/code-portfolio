/**
 * Fetch a key and replace strings when necessary
 * @param  String key             Key to search inside enterprise.i18nReservation
 * @param  Object replacements    An Object mapping string replacements
 * @return String
 *
 * Usage:
 *    i18n('MVT_0012')                       -> `Pay in #{currency}`
 *    i18n('MVT_0012', { currency: 'EUR' })  -> `Pay in EUR`
 *    i18n('UNEXISTING_KEY')                 -> `` (empty string)
 *    i18n('')                               -> `` (empty string)
 *    i18n(undefined) i18n(null)             -> false and console.error
 *    i18n([]), i18n({}), i18n(123)          -> false and console.error
 */
import { Debugger } from './util-debug';

// set this false to turn off for local dev
// unlike other isDebug flags in the code this should
// be committed as true
const localDebug = {
  isDebug: false
};
const debugState = (!process || process.env.NODE_ENV !== 'test');
const logger = new Debugger(debugState, localDebug, true, 'util-i18n');

module.exports = function (...args) {
  let settings = {};
  if ((typeof args[0]).toLowerCase() === 'object') {
    settings = args[0];
  }
  const key = settings.key || args[0];
  const replacements = settings.replacements || args[1] || {};
  const countrySuffix = settings.countryCode ? `_${settings.countryCode}` : '';

  // @TODO change to leverage util-debug.js
  switch (true) {
    case (typeof key !== 'string'):
      logger.error('type ' + typeof key + ' of "key" not supported, must be string');
      return false;
    case (key.length && typeof enterprise.i18nReservation[key] !== 'string'):
      logger.warn(key, 'not found');
      break;
    case (!key.length):
      logger.warn('no key provided');
      break;
  }
  const i18nKeys = _.get(enterprise, 'i18nReservation') || {};
  const initialString = i18nKeys[key+countrySuffix] || i18nKeys[key] || '';

  let finalString = Object.keys(replacements).reduce((str, replaceKey) => {
    return str.replace(new RegExp(`#{${replaceKey}}`, 'g'), replacements[replaceKey]);
  }, initialString);

  if (_.get(enterprise, 'qatools.active')) {
    return <span data-i18nkey={key}>{finalString}</span>;
  }

  return finalString;
};
