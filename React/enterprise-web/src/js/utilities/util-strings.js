
/**
 * The `trimNumber` function trims the 0's from a string-formatted number
 * both from the left side and right side when the number has a decimal part
 * @param  {String} numberString     the number to be trimmed
 * @param  {String} decimalDelimiter the decimal delimiter in case we have to trim locale-formatted numbers
 * @return {String}                  the trimmed number
 */
export function trimNumber(numberString, decimalDelimiter = '.') {
  let ret = numberString.replace(/^0+(\d)/, '$1');
  if (ret.includes(decimalDelimiter)) {
    ret = ret.replace(/\.?0+$/, '');
  }
  return ret
}