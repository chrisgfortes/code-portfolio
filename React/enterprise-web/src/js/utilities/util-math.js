var MathUtil = {
  //Hash sieving for uniqueArray O(2n) in worst case compared to O(n^2) for classic
  uniqueArray: function (array) {
    let a = [], b, i = array.length;
    while (i--) {
      b = array[i];
      if (a.indexOf(b) == -1) {
        a.push(b);
      }
    }
    return a;
  },
  calculateSphericalDistance: function (first, second) {
    let rad = function (x) {
      return x * Math.PI / 180;
    };

    //Haversine Formula
    const R = 6378137; // Earth’s mean radius in meter
    let dLat = rad(second.lat() - first.lat()),
      dLong = rad(second.lng() - first.lng());

    let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(rad(first.lat())) * Math.cos(rad(second.lat())) *
      Math.sin(dLong / 2) * Math.sin(dLong / 2);

    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    //Returning results in meters based on earth's curvature
    return R * c;

  }
};

module.exports = MathUtil;
