/**
 * @todo Remove jQuery dependency
 * @type {{scrollTo: ((id?, timing?)), getTopScroll: (()), down: ((scrollOffset?))}}
 */
const scrollUtil = {
  scrollTo (id, timing = 200, isModal) {
    let element = $(id);
    if (element.length) {
      if (isModal) {
        $(element)[0].scrollIntoView();
      } else if (timing === 0) {
        $('html, body').scrollTop(element.offset().top);
      } else {
        $('html, body').animate({
          scrollTop: element.offset().top
        }, timing);
      }
    }
  },
  getTopScroll () {
    return window.pageYOffset || document.documentElement.scrollTop;
  },
  scrollToOffset(scrollOffset) {
    window.scrollTo(0, scrollOffset);
  },
  scrollTop (element, type) {
    $(element).animate({scrollTop: 0}, type);
  }
};

module.exports = scrollUtil;
