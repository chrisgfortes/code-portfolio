/**
 * @todo add feature tests / modernizr etc.
 */
const LocalStorage = {
  get (key) {
    return localStorage.getItem(key)
  },
  set (key, val) {
    localStorage.setItem(key, JSON.stringify(val));
  },
  clear () {
    localStorage.clear();
  }
};

function supported (kindof = 'session') {
  if (kindof === 'session') {
    return window.sessionStorage;
  } else {
    return window.localStorage;
  }
}

const SessionStorage = {
  get (key) {
    if (supported('session')) {
      return sessionStorage.getItem(key);
    }
  },
  set (key, val) {
    if (supported('session')) {
      sessionStorage.setItem(key, JSON.stringify(val));
    }
  },
  remove (key) {
    if (supported('session')) {
      sessionStorage.removeItem(key);
    }
  },
  clear () {
    if (supported('session')) {
      sessionStorage.clear();
    }
  }
};

export default {
  LocalStorage,
  SessionStorage,
  supported
}
