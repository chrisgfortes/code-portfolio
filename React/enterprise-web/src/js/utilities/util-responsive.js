/**
 * How big might this get? Should we move to src/js/modules ?
 */
const ResponsiveUtil = {
  isMobile (size = '(max-width: 768px)') {
    return window.matchMedia(size).matches;
  }
};

module.exports = ResponsiveUtil;
