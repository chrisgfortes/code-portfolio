
/**
 * @memberOf Cookie
 * @param  {string} locationToCookie path to the cookie
 * @param  {string} type             pickup|dropoff etc.
 * @return {*}                  location data
 */
function cookieLocation(locationToCookie, type) {
  let locations = Cookie.get(type + 'SearchedLocations'),
    locationArray = [],
    tempArray = [],
    hasLocation = false;

  locationToCookie = $.extend(true, {}, locationToCookie);
  delete locationToCookie.details;

  if (locations) {
    locationArray = locations.split('&&');
    if (locationArray.length == 3) {
      locationArray.pop();
    }

    try {
      tempArray = $.map(locationArray, function(location) {
        return JSON.parse(location);
      });
    } catch (error) {
      console.warn('location cookie error');
      Cookie.remove(type + 'SearchedLocations');
    }

    $.each(tempArray, function(i, location) {
      if (locationToCookie.locationId == location.locationId) {
        hasLocation = true;
      }
    });
  }

  if (!hasLocation) {
    locationArray.unshift(JSON.stringify(locationToCookie));
    Cookie.set(type + 'SearchedLocations', locationArray.join('&&'));
  }
}

const Cookie = {
  get(key) {
    const regEx = new RegExp('(?:(?:^|.*;\\s*)' + key + '\\s*\\=\\s*([^;]*).*$)|^.*$');
    const val = document.cookie.match(regEx);

    if (key && val && val[1]) {
      return unescape(val[1]);
    }

    return null;
  },
  setDays(key, val, days) {
    let date;
    let expires;
    if (days) {
      date = new Date();
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
      expires = date.toGMTString();
    } else {
      expires = '';
    }
    this.set(key, val, {
      expires
    });
  },
  set(key, val, newOptions = {}) {
    const options = Object.assign({
      path: '/'
    }, newOptions);
    const mappedOptions = Object.keys(options)
      .map(optionKey => {
        let oKey = optionKey;

        if (oKey === 'maxAge') {
          oKey = 'max-age';
        }

        return `${oKey}=${options[optionKey]}`;
      })
      .join(';');

    console.log(`${key}=${val};${mappedOptions}`);
    document.cookie = `${key}=${val};${mappedOptions}`;

    return Cookie.get(key);
  },
  remove(key) {
    document.cookie = key + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  },
  cookieLocation
};

module.exports = Cookie;
