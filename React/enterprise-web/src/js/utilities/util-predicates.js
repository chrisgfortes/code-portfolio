/*
Predicate Utils contains functions that help to manipulate and compound predicate functions(*)

(*) - functions that, given a value, test some conditions and return a boolean
*/

export function not (predicate) {
  return (value) => !predicate(value);
}

export function and (...predicates) {
  return (value) => predicates.map(p => p(value))
                              .reduce((p1, p2) => p1 && p2);
}

export function or (...predicates) {
  return (value) => predicates.map(p => p(value))
                              .reduce((p1, p2) => p1 || p2);
}

export function exists (value) {
  return value !== null && value !== void 0;
}
