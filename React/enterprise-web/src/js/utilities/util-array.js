/**
 * Array Utils contains functions that help to manipulate arrays
 * (Some functions here are heavily inspired by Haskell's List library: https://hackage.haskell.org/package/base-4.10.0.0/docs/Data-List.html)
 */

/**
 * The intersperse function takes an element and an array
 * and 'intersperses' that element between the elements of the array.
 *
 * ex: `intersperse([1,2,3], '-')` ==> `[1, '-', 2, '-', 3]`
 */
export function intersperse (arr, value) {
  return arr.reduce((result, el, index) => result.concat(index === 0 ? [el] : [value, el]), [])
}

/**
 * The `partition` function takes a predicate and a list,
 * returning the pair of lists of elements which do and
 * do not satisfy the predicate, respectively.
 *
 * ex: `partition(n => n > 3, [1,9,2,8,3,7])` ==> `[[9,8,7], [1,2,3]]`
 */
export function partition (predicate, arr) {
  return arr.reduce(([yes, no], el) => (
    predicate(el) ? [yes.concat(el), no]
                  : [yes, no.concat(el)]
  ), [[], []]);
}

/**
 * The `flatPartition` function applies `partition` to
 * the arguments and then flatten the result into a single array.
 * @see `partition`
 *
 * ex: `flatPartition(n => n > 3, [1,9,2,8,3,7])` ==> `[9,8,7,1,2,3]`
 */
export function flatPartition (predicate, arr) {
  return Array.concat(...partition(predicate, arr));
}