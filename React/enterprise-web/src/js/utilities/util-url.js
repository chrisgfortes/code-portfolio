const Url = {
  hasProtocol (url) {
    return /^http(s)?:/.test(url)
  },
  getParameters () {
    let parameters = {};
    window.location.search.substring(1).split('&').forEach(function (item) {
      let k = item.split('=')[0].toLowerCase();
      let v = item.split('=')[1];
      v = v && decodeURIComponent(v);
      (k in parameters) ? parameters[k].push(v) : parameters[k] = [v];
    });
    return parameters;
  }
  //getParametersHash() {
  //    var parameters = {};
  //    window.location.hash.replace(/ *\#[^)]*\? */g, "").split("&").forEach(function (item) {
  //        var k = item.split("=")[0], v = item.split("=")[1];
  //        v = v && decodeURIComponent(v);
  //        (k in parameters) ? parameters[k].push(v) : parameters[k] = [v]
  //    });
  //    return parameters;
  //}
};

module.exports = Url;
