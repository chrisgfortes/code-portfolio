// Copy from enterprise.utilities.isMaskedField -- should this really be inside `enterprise.utilities`?
import { GLOBAL } from '../constants';

export function isMaskedField (val) {
  const maskRegex = new RegExp('[' + GLOBAL.V1_MASK_CHAR + GLOBAL.V2_MASK_CHAR + ']');
  return maskRegex.test(val);
}

export function validateMaskedInfo (info) {
  return info && !isMaskedField(info) ? info : null;
}