/**
 * Escapes all keypresses but ENTER and SPACEBAR. Used for adding keyboard interaction with non-
 * standard elements (anything but buttons and links).
 *
 * Example:
 *   $('#fake-button').on('click keypress', function(event){
 *     if(a11yClick(event) === true){
 *       // do stuff
 *     }
 *   });
 */

const a11yClick = function (param) {
  let isItA11yClick = function (event) {
    const SPACEBAR = 32;
    const ENTER = 13;

    if (!event) {
      return undefined;
    }

    if (event.type === 'click') {
      return true;
    }

    if (event.type === 'keypress') {
      let code = event.charCode || event.keyCode;
      if (code === SPACEBAR) { // prevent scrolling the page
        event.preventDefault();
      }
      if ((code === SPACEBAR) || (code === ENTER)) {
        return true;
      }
    } else {
      return false;
    }
  };

  if (typeof param === 'function') {
    return function (event) {
      if (isItA11yClick(event)) {
        param.call(this, event);
      }
    };
  }

  return isItA11yClick(param);
};

module.exports = a11yClick;
