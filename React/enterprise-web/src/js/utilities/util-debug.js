/**
 * @TODO: make honor enterprise.settings.debugging
 * @TODO: set a better pattern for instantiation (debug export is a start)
 * @todo  console.assert and console.group don't work, I don't think
 * */
const global = window || {};
global._isDebug = __CONSOLE__;
// console.log('window._isDebug', window._isDebug);
const consoleApi = 'assert,count,debug,dir,dirxml,error,exception,group,' +
  'groupCollapsed,groupEnd,info,log,markTimeline,profile,profileEnd,' +
  'time,timeEnd,timeStamp,trace,warn';
let con = (global.console = global.console || {});
const noop = function() {};

function _addItem (item) {
  Debugger.g.loggers.push(item);
}
function _enable (scope) {
  for (let m in console) {
    if (typeof console[m] === 'function') {
      if (scope.scopeName) {
        scope.debug[m] = console[m].bind(global.console, scope.scopeName + ': ');
      } else if (scope.showScope) {
        scope.debug[m] = console[m].bind(global.console, scope.toString() + ': ');
      } else {
        scope.debug[m] = console[m].bind(global.console);
      }
    }
  }
  if (scope.isDebug === false) {
    scope.isDebug = true;
  } else if (scope._isDebug === false) {
    scope._isDebug = false;
  }
}
function _disable (scope) {
  if (scope.isDebug === true) {
    scope.isDebug = false;
  } else if (scope._isDebug === true) {
    scope._isDebug = false;
  }
  for (let m in console) {
    if (typeof console[m] === 'function') {
      scope.debug[m] = function () {};
    }
  }
}

/**
 * Debugger
 * @constructor
 * @param {bool} globalDebugState
 * @param {object} objectScope
 * @param {bool} showScope
 * @param {string} scopeName
 */
class Debugger {
  constructor (globalDebugState, objectScope, showScope, scopeName) {
    (function () {
      function c () {}
      for (let d = consoleApi.split(','), a; a = d.pop();) {
        con[a] = con[a] || c;
      }
    }(global));
    this.debug = {};
    this.showScope = (showScope !== void 0) ? showScope : false;
    this.scopeName = (scopeName !== void 0) ? scopeName : false;
    this.onoff = true;
    this.objectScope = objectScope;
    if (!global.console) return function() {};

    // console.groupCollapsed(scopeName);
    //   console.log('passed', globalDebugState);
    //   console.log('global', global._isDebug);
    //   console.log(this.objectScope.isDebug);
    // console.groupEnd();

    if (globalDebugState && (this.objectScope.isDebug || this.objectScope._isDebug)) {
      this.onoff = true;
      _enable(this);
    } else {
      this.onoff = false;
      _disable(this);
    }
    _addItem({
      namedAs: scopeName,
      obj: this
    });
    return this.debug;
  }

  pause () {
    _disable(this);
  }

  play () {
    _enable(this);
  }
}
Debugger.g = {
  defaultConsole: {},
  state: true,
  loggers: []
};
Debugger.g.pause = function() {
  for (let d = consoleApi.split(','), a; a = d.pop();) {
    Debugger.g.defaultConsole[a] = con[a];
    con[a] = noop;
  }
  Debugger.g.state = false;
};
Debugger.g.play = function() {
  for (let d = consoleApi.split(','), a; a = d.pop();) {
    if (Debugger.g.defaultConsole.hasOwnProperty(a)) {
      con[a] = Debugger.g.defaultConsole[a];
      Debugger.g.defaultConsole[a] = noop;
    }
  }
  Debugger.g.state = true;
};
Debugger.getByName = function (n, getLogger = false) {
  let retVal;
  let obj = Debugger.g.loggers.find((el) => {
    return el.namedAs === n;
  });
  if (getLogger && obj && obj.obj.debug) {
    retVal = obj.obj.debug;
  } else if (obj && obj.obj) {
    retVal = obj.obj;
  } else { // really do we ever want this?
    retVal = obj;
  }
  return retVal;
};
// this allows adhoc creation of debuggers, or accessing another debugger, by name.
Debugger.named = function (n, debugOnly = true) {
  let retObj;
  let inst = Debugger.getByName(n, debugOnly);
  if (inst) {
    retObj = inst;
  } else {
    let adhoc = {
      namedAs: n,
      obj: new Debugger(true, {isDebug:true}, true, n)
    };
    retObj = adhoc.obj;
  }
  return retObj;
};

// pure convenience method alias for Debugger.named
// return the full Debugger instance,
// though in truth if it cannot find one, it will make one
Debugger.get = function (n){
  return Debugger.named(n, false);
};
// pure convenience method alias for Debugger.named
// use an existing Debugger instance,
// though in truth if it cannot find one it will make one
Debugger.use = function (n) {
  return Debugger.named(n, true);
};

module.exports = {
  Debugger: Debugger,
  /**
   * Debug factory wrapper for Debugger
   * @param {(string|object)} [nameOf] string for debugger. if object, should have a .name property
   * @param {object} [scopeOf] object scope of debugger, ignored if first argument is an object
   * @returns {Debugger}
   */
  debug(nameOf, scopeOf) {
    let name;
    let scopeFor;
    if (typeof nameOf === 'string' && typeof scopeOf === 'object') {
      // console.log('001')
      name = nameOf;
      scopeFor = scopeOf;
    } else if (typeof nameOf === 'object') {
      // console.log('002')
      scopeFor = nameOf;
      name = scopeFor.name;
    } else {
      console.warn('003 Mismatched util-debug.js arguments.');
      return new Debugger(window._isDebug, {isDebug:false}, true, 'Debugger');
    }
    if (typeof scopeFor.isDebug === void 0 && typeof scopeFor._isDebug === void 0) {
      console.warn('004');
      scopeFor._isDebug = false;
    }
    return Object.assign({}, scopeFor, {logger: new Debugger(window._isDebug, scopeFor, true, name)})
  }
};
