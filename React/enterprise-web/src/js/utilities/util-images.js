var Images = {
  getUrl: function (url, width, quality) {
    url = url.replace('{width}', width);
    url = url.replace('{quality}', quality);
    return url;
  }
};

module.exports = Images;
