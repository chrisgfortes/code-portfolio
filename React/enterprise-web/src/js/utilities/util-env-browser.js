/* global Modernizr, UAParser, _ */
/**
 * Please do not use this for anything but it's intended purpose. Hoping, sincerely, one day this goes away.
 * Purpose: ECR-10878 defines that we must detect older devices and browsers.
 *  This helps do that.
 *  Extending this so that we can use the headers from http://edc.edgesuite.net/
 *
 *  Does a couple things:
 *   - Allows for a client-side detection process
 *   - Allows for an external source of detection
 *   - Allows running a series of Modernizr tests to determine modern browser support
 *   - Makes logical decisions based on properties derived from tests
 *   - Runs some code based only on feature flags
 *
 */

// no this shouldn't be a const, we change it based on conditions
let profile = {
  names: {
    'msie': ['msie', 'ie', 'internet explorer'],
    'chrome': ['chrome', 'chromium'],
    'safari': ['safari'],
    'firefox': ['firefox', 'firebird', 'seamonkey', 'camino', 'chimera', 'k-meleon', 'brave', 'mozilla', 'phoenix']
  },
  engines: { // engine not returned by edgesuite
    known: ['blink', 'edgehtml', 'gecko', 'trident'], // blink not returned by UAParser
    needswork: ['webkit', 'presto'],
    unknown: []
  },
  ua: []
};

class Browser {
  constructor (ua = {
    rawname: null,
    version: null,
    versionRelease: null,
    engine: null,
    device: null,
    deviceClass: null,
    deviceVendor: null,
    osName: null,
    osVersion: null,
    userAgent: null
  }, settings = {}) {
    this.rawname = ua.name;
    this.version = ua.version;
    this.versionRelease = ua.versionRelease;
    this.engine = ua.engine;
    this.device = ua.device;
    this.deviceClass = ua.deviceClass;
    this.deviceVendor = ua.deviceVendor;
    this.osName = ua.osName;
    this.osVersion = ua.osVersion;

    // this.properties = {}; // @todo: groupd all the above under this specifically as an object e.g. BrowserProperties

    // qualitative properties // try to be optimistic with positive defaults
    this.html5 = true;  // does our device support basic HTML5 stuffs
    this.ok = true;     // basic tests
    this.pass = true;   // this is set by decision code not detection code
    this.message = '';  // if something failed, give a message

    this.isEdgeMSIE = false; // is edge considered IE?
    this.isIEMobileMSIE = false; // is IE Mobile considered IE?

    this.settings = settings; // save incoming settings
    this.tests = (this.settings.html5List || []);
    // pull the ones we want
    this.browserServerSideDetection = settings.browserServerSideDetection; // are we doing server-side detection?
    this.browserClientSideRedirect = settings.browserClientSideRedirect; // are we doing a client-side redirect?
    this.browserClientSideDetection = settings.browserClientSideDetection; // are we doing detection on the client?
    this.browserRedirect = settings.browserRedirect; // globally, are we using a browser redirect?
  }

  get name () {
    return this.rawname && this.rawname.toLowerCase();
  }

  set name (v) {
    this.rawname = v;
  }

  getProp (v) {
    return this[v] && this[v].toLowerCase();
  }

  checkCommonNames (nameStr) {
    let retVal = false;
    let namesForThis = profile.names[nameStr];
    if (namesForThis.indexOf(this.name) !== -1) {
      retVal = true;
    }
    return retVal;
  }

  get isIE () {
    let isIE = false;
    let namesForIE = profile.names.ie;
    if (this.isEdgeMSIE) namesForIE.push('edge');
    if (this.isIEMobileMSIE) namesForIE.push('iemobile');
    if (this.checkCommonNames('msie')) {
      isIE = true;
      this.rawname = 'msie';
      // make sure we are using the name in our thresholds
    }
    return isIE;
  }

  get isIEDesktop () {
    let ie = false;
    if (this.isIE && this.name !== 'iemobile' && this.getProp('osName').indexOf('windows phone') < 0) {
      ie = true;
    }
    return ie;
  }

  get isChromeDesktop () {
    let c = false;
    if (this.getProp('name') === 'chrome' && (!this.isIOS && !this.isAndroid) && this.checkCommonNames('chrome')) {
      c = true;
      this.rawname = 'chrome';
    }
    return c;
  }

  get isFirefoxDesktop () {
    let f = false;
    if (this.getProp('name') === 'firefox' && (!this.isIOS && !this.isAndroid) && this.checkCommonNames('firefox')) {
      f = true;
      this.rawname = 'firefox';
    }
    return f;
  }

  get isSafariDesktop () {
    let s = false;
    if (!this.isIOS && this.getProp('name') === 'safari' && this.checkCommonNames('safari')) {
      s = true;
      this.rawname = 'safari';
    }
    return s;
  }

  get isMac () {
    let m = false;
    // sometimes we have 'mac os x', 'mac os'
    if (this.getProp('osName').indexOf('mac os') > -1) {
      m = true;
    }
    return m;
  }

  get isIOS () {
    let i = false;
    let os = this.getProp('osName');
    try {
      if (os.indexOf('ios') > -1 || os.indexOf('iphone os') > -1) {
        i = true;
      }
    } catch (e) {  }
    return i;
  }

  get isAndroid () {
    let a = false;
    try {
      if (this.getProp('osName').toLowerCase() === 'android') {
        a = true;
      }
    } catch (e) {  }
    return a;
  }

  get isSafeBrowser () {
    return this.ok && this.pass;
  }

  // A DOM element has been added by the basepage inside conditional comments.
  // this test checks if the conditional comment hidden element is found by the browser.
  // if the browser found it, then it is IE. ALSO, WILL BE IE9 OR LESS as IE10 DROPPED SUPPORT
  // FOR CONDITIONAL COMMENTS.
  doubleCheckIE () {
    let ie = false;
    let domTest = document.getElementById('msie');
    // THESE ARE RISKY :)
    // IE11+
    //    typeof navigator.maxTouchPoints !="undefined"
    // 10 or older
    //    document.all
    // 10 OR NEWER
    //    ("onpropertychange" in document) && (!!window.matchMedia)
    // 9 or older
    //    document.all && !window.atob
    // 9 OR NEWER
    //    ("onpropertychange" in document) && (!!window.innerWidth)
    // 8 or older
    //    document.all && !document.addEventListener
    // 8 OR NEWER
    //    ("onpropertychange" in document) && (!!window.XDomainRequest)
    //    document.documentMode
    if (domTest) {
      ie = true;
      this.name('msie');
      this.failsTests('The browser sees an IE9 only element.');
    }
    return ie;
  }

  html5Compliance () {
    let i = 0;
    let limit = this.tests.length;
    for (i; i < limit; i++) {
      if (Modernizr[this.tests[i]] === false) {
        this.html5 = false;
        this.failsTests('Fails HTML5 test on ' + this.tests[i]);
        break;
      }
    }
    if (enterprise.settings.debugging) {
      console.debug('Testing html5Compliance()', this.message);
    }
  }

  failsTests (message) {
    this.message = message;
    this.ok = false;
    this.pass = false;
    return false;
  }

  cookie () {
    return true;
  }

  judgement () {
    if (this.settings.debugging) {
      console.groupCollapsed('Browser detection profile settings:');
      console.log('browserServerSideDetection: ', this.browserServerSideDetection);
      console.log('browserClientSideRedirect:', this.browserClientSideRedirect);
      console.log('browserClientSideDetection:', this.browserClientSideDetection);
      console.log('browserRedirect:', this.settings.browserRedirect);
      console.log('allowUnknownBrowsers: ', this.settings.allowUnknown);
      console.log('enforce HTML5 Compliance: ', this.settings.featureTests);
      console.groupCollapsed('Thresholds we are testing against:');
      Object.keys(this.settings.browserThresholds).forEach( (key)=> {
        console.log(key, ':', this.settings.browserThresholds[key]);
      });
      console.groupEnd();
      console.debug('Sniffing results:');
      console.dir(this);
      console.log(navigator.userAgent);
      console.groupEnd();
    }

    let thresholds = this.settings.browserThresholds;
    let workingName;
    if (this.isIEDesktop) {
      // @todo: make better;
      if (this.doubleCheckIE()) {
        workingName = 'msie';
      }
      workingName = 'msie';
    } else if (this.isChromeDesktop) {
      workingName = 'chrome';
    } else if (this.isFirefoxDesktop) {
      workingName = 'firefox';
    } else if (this.isSafariDesktop) {
      workingName = 'safari';
    // } else if (this.isAndroid) {
    //   workingName = 'android';
    //   this.version = this.osVersion;
    // } else if (this.isIOS) {
    //   workingName = 'ios';
    //   this.version = this.osVersion;
    } else {
      workingName = void 0; // this either a browser we don't know, or a browser we don't care about
      // also, there's a good chance they're browsers that are something we know but don't support officially
    }

    let versionByName;
    if (workingName !== void 0) {
      versionByName = thresholds[workingName];
      if (parseFloat(this.version) < versionByName) {
        console.warn('known browser failed version tests! Abort! Abort!', workingName, '(threshold)', versionByName, '(this.version)', this.version);
        this.failsTests('Fails threshold test for ' + workingName + ' defined as version ' + versionByName + ' (current version: ' + this.version + ')');
      } else {
        // console.debug('Known browser passed.');
      }
    } else { // everyone and everything else, including mobile that doesn't come back as Android or iOS
      let tested = false;
      if (this.settings.allowUnknown) {
        // maybe we should hit them with he html5Compliance() tests regardless?
        if (this.settings.featureTests) {
          // console.debug('Not a common browser. Testing feature support.', this);
          tested = true;
          this.html5Compliance();
        }
      } else {
        this.failsTests('Not a common browser. Blocking outright.', this);
      }
      if (this.settings.featureTests && !tested) {
        this.html5Compliance();
      }
    }
    if (this.settings.debugging && !this.isSafeBrowser) {
      console.warn('Fail message: ', this.message);
    }
    return this.isSafeBrowser; // in this case, a safe browser can just be an unknown browser
  }
}

// our actual module that we are exporting
module.exports = (function () {

  let clientSide;
  let settings;
  let browser;

  // FROM THE DOCS ABOVE
  // # Possible 'device.type':
  // console, mobile, tablet, smarttv, wearable, embedded
  function resolveDevice (ba) {
    // console.info('@todo: change the resolveDevice() method');
    // return (ba.device.type) ? ba.device.type : ((ba.os.name === 'Windows') ? 'pc' : 'unknown')
    let str = ba.device.type;
    if (!ba.device.type) {
      if (ba.os.name === 'Windows' || ba.os.name === 'Linux') {
        str = 'pc';
      } else {
        str = 'unknown';
      }
    }
    return str;
  }

  function clientSideDetect (cs) {
    let thisBrowser;
    if (cs) {
      // Read the docs! RTFM!!
      // https://github.com/faisalman/ua-parser-js
      let b = new UAParser();
      let ba = b.getResult();

      // this is a stub of data to be used to create our Browser object.
      thisBrowser = {
        name: ba.browser.name,
        version: ba.browser.major,
        versionRelease: ba.browser.version,
        engine: ba.engine.name,
        device: ba.device.model || resolveDevice(ba),
        deviceClass: ba.device.type || 'computer', // tablet mobile, etc.
        deviceVendor: ba.device.vendor || ba.os.name,
        osName: ba.os.name,
        osVersion: ba.os.version,
        userAgent: b.getUA()
      };
    }
    return thisBrowser;
  }

  function mapBrowserValues (curBrow, headBrow) {
    Object.keys(headBrow).forEach( (key)=> {
      curBrow[key] = headBrow[key];
    });
    return curBrow;
  }

  return {
    // default
    init: function (entSettings) {
      settings = entSettings;
      clientSide = settings.browserClientSideDetection;
      clientSide = (enterprise.currentView === enterprise.settings.viewBrowserNotice) ? false : clientSide;
      browser = clientSideDetect(clientSide);

      // if (settings.debugging && clientSide) {
      //   console.group('browser by clientside parser');
      //   Object.keys(browser).forEach(function (key) {
      //     console.log(key, ':', browser[key]);
      //   });
      //   console.groupEnd();
      // }
      return new Browser(browser, settings);
    },
    extend: function (headerData) {
      let currentBrowser = enterprise.environment.browser;
      currentBrowser = mapBrowserValues(currentBrowser, headerData);
      if (enterprise.settings.debugging) {
        console.debug('Using server-detected browser values.', currentBrowser);
      }
      return currentBrowser;
    },
    // create your own!
    modify: function (userAgentData, override = false) {
      return new Browser(userAgentData, override);
    }
  };
}());
