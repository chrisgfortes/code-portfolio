var viewCursor = ReservationStateTree.select('view');

var Resize = {
  listen() {
    var delay;

    this.setBreakpoint();

    window.onresize = () => {
      clearTimeout(delay);
      delay = setTimeout(this.setBreakpoint, 100);
    };
  },
  setBreakpoint() {
    //Embed media breakpoint within CSS under the Content attribute within body:after
    //Allowing Javascript to detect media queries
    let defaultBreakpoint = 'default',
      cssBreakpointString = window.getComputedStyle(document.body, ':after').getPropertyValue('content');

    viewCursor.set('currentViewBreakpoint', cssBreakpointString || defaultBreakpoint);
  }
};

module.exports = Resize;
