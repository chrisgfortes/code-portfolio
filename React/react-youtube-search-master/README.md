# VideoTube - React Youtube Search

Study application developed with react, and consuming youtube search API

## Used stacks
- React
- Babel
- Webpack
- Node
- Sass

# Install and use

1. Clone this repository to your computer
2. Go to the project folder using terminal
3. Execute the following commands
```
$ npm install
$ npm start
```

Open your browser and type [http://localhost:8080](http://localhost:8080)