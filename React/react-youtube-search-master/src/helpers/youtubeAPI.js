import axios from 'axios';
import qs from 'qs';

export default {
  search: function(term) {
        let URL = {
            base: 'https://www.googleapis.com/youtube/v3/search?',
            query: {
                part: 'snippet',
                q: term,
                type: 'video',
                key: 'AIzaSyD-oDco5bSrsQUwmJcwuVgOFXyaFfo6AW4',
                maxResults: 10
            }
        }

        let request = URL.base.concat(qs.stringify(URL.query));

        return axios.get(request).then(function(resp){
            return resp.data.items;
        }).catch(function(err){
            console.error(err);
        });
    }
}