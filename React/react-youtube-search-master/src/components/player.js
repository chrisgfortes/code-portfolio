import React, { Component } from 'react';
import {IntlProvider, FormattedDate} from 'react-intl';
import _ from 'lodash';

class Player extends Component {
	constructor(props){
		super(props);
	}

	iframe(video){
		if (video){
			let base = (`https://www.youtube.com/embed/${video.id.videoId}?enablejsapi=1`),
				url = ( video.hasOwnProperty('autoplay') ? base.concat('&autoplay=1') : base )

			return (
				<iframe
					className="iframe row-md-height row-sm-height row-xs-height"
					src={url}
					frameBorder="0"
					allowFullScreen
				/>
			)
		}
	}

	description(video){
		if(video){
			let description = (
					video.snippet.description ?
						<span className="description">{video.snippet.description}</span> :
						""
					);

			return (
				<div className="infoVideo">
					<h1>{video.snippet.title}</h1>
					<span className="channel">{video.snippet.channelTitle}</span>
					<span className="date">
						<IntlProvider locale="en">
					        <FormattedDate
					        	className="date"
								value={video.snippet.publishedAt}
								day="numeric"
			                    month="long"
			                    year="numeric"
							/>
					    </IntlProvider>
					</span>

					{description}
				</div>
			)
		}
	}

	render() {
		let video = this.props.videos,
			iframe = this.iframe(video),
			description = this.description(video);

		return (
			<div>
				{iframe}
				{description}
			</div>
		)
  	}
}

export default Player;