/* Libs */
import React, { Component } from 'react';

/* Components */
import Loading from '../helpers/loading';
import youtube from '../helpers/youtubeAPI';
import Search from './search';
import Player from './player';
import List from './list';

class App extends Component {
	constructor(props) {
		super(props);

		this.state = {
			videos: null,
			videoSelected: null,
			loading: false,
            term: ''
		};

        this.search = this.search.bind(this)
    }

    componentWillMount(){
    	this.search();
    }

    updateSearch(){
    	this.search()
    }

    search(query = 'Imagine Dragons'){
    	this.setState({
			loading: true
		});

    	youtube
    		.search(query)
    		.then((res) => {
    			this.setState({
    				videoSelected: res[0],
					videos: res,
					term: query,
					loading: false
				});
    		})
    }

    play(video){
    	video.autoplay = true;

    	this.setState({
			videoSelected: video
		});
    }

	render() {
		return (
			<div className="container">
				<div className="row">
					<div className="col-md-2 col-sm-3 col-xs-3">
						<h1>Video<span>Tube</span></h1>
					</div>
					<div className="col-md-6 col-sm-7 col-xs-7">
				    	<Search searchTerm={term => this.search(term)}/>
					</div>
					<div className="col-md-4 col-sm-2 col-xs-2 github">
						<a href="https://github.com/chrisgfortes" target="_blank" className="user hidden-xs hidden-sm">@chrisgfortes | </a>
						<a href="https://github.com/chrisgfortes/react-youtube-search"
							target="_blank"
							className="repository"
							title="Github repository"
						>
							<img
								src="./images/github.svg"
								width="25"
							/>
						</a>
					</div>
				</div>

				{(this.state.loading ?
					<Loading /> :
					<div className="row">
						<div className="col-md-8">
							<Player videos={this.state.videoSelected}/>
						</div>

						<div className="col-md-4 col-xs-12">
							<List videos={this.state.videos} onPlay={video => this.play(video)}/>
						</div>
					</div>
				)}
			</div>
		);
  	}
}

export default App;