import React, { Component } from 'react';

class Search extends Component {
	constructor(props){
		super(props);

		this.state = {
			term: ''
		};
	}

	onInputChange(term){
		this.setState({ term });
	}

	handleSubmit(event){
		event.preventDefault();
        this.props.searchTerm(this.state.term);
	}

	render() {
		return (
			<form onSubmit={this.handleSubmit.bind(this)}>
				<label className="form-group">
				    <input type="text" value={this.state.term} onChange={event => this.onInputChange(event.target.value)} className="form-control" placeholder="Pesquise seu video aqui" />
				</label>
			</form>
		)
  	}
}

export default Search;