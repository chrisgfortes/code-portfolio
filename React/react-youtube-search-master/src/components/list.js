import React, { Component } from 'react';
import Search from './search';
import _ from 'lodash';

class List extends Component {
	constructor(props) {
	    super(props);
	}

	onItemClick(video, event){
		event.preventDefault();
        this.props.onPlay(video, true);
        this.clearStylePlay(event);
	}

	clearStylePlay(currTarg){
		let elm = document.querySelectorAll('.btnMedia'),
			currentTarget = currTarg.target.classList;

		_.forEach(elm, (curr, idx) => {
			let current = curr.classList;

			if (current.contains('pause')) {
				current.remove('pause');
				current.add('play');
			}
		});

		currentTarget.remove('play');
		currentTarget.add('pause');
	}

	render() {
		let suggestions = _.map(this.props.videos, (item) => {
			return (
		        <li className="col-md-12 col-xs-6" key={item.id.videoId}>
					<a ref="btn" className='btnMedia play' href={'/watch?' + _.replace(item.etag, /\"/g, '')} onClick={this.onItemClick.bind(this, item)}>
						<img
							src={item.snippet.thumbnails.medium.url}
							alt={item.snippet.title}
							width={item.snippet.thumbnails.width}
							height={item.snippet.thumbnails.height}
						/>
					</a>
				</li>
		    );
		});

		return (
			<div className="suggested">
				<h1	className="visible-xs">Suggested videos</h1>
				<ul className="row playlist">
					{suggestions}
	    		</ul>
    		</div>
		)
  	}
}

export default List;